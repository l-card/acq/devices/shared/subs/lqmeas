#ifndef LQTDEFS_H
#define LQTDEFS_H

#include <QtGlobal>
#include <QString>
#ifdef Q_OS_WIN
#include <QTextCodec>
#include "TextCodecs.h"
#endif



#ifdef Q_OS_WIN
    #define QSTRING_FROM_CSTR(str) LQMeas::TextCodecs::cp1251().toUnicode(str)
    #define QSTRING_TO_CSTR(str)   LQMeas::TextCodecs::cp1251().fromUnicode(str)
#else
    #define QSTRING_FROM_CSTR(str) QString(str)
    #define QSTRING_TO_CSTR(str)   str.toUtf8()
#endif

#if QT_VERSION < QT_VERSION_CHECK(5, 7, 0)
    #define qAsConst(v)            v
#endif


#define LQREAL_IS_NULL(val) qFuzzyIsNull(val)
#define LQREAL_IS_EQUAL(val1, val2) ((qFuzzyIsNull(val1) && qFuzzyIsNull(val2)) || qFuzzyCompare(val1, val2))


#endif // LQTDEFS_H
