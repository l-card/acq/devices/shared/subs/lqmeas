#ifndef ENUMCFGKEY_H
#define ENUMCFGKEY_H

#include <QString>

namespace LQMeas {
    /* вспомогательный класс для сопостовления строки коду перечисления для сохранения
     * в конфигурационном файле. в отличие от EnumNamedValue не предназначен для
     * вывода в интерфейс строки и не предусматривает первода.
     *
     * Для работы необходимо объявить статический массив стркут, сопоставляющих
     * пары key и value. Последний элемент должен быть с пустой строкой и
     * значением по умолчанию.
     *
     * Для сопостовления используются статические методы getKey и getValue, котым
     * передается объявленный массив пар значений */
    template <typename EnumValueType> struct EnumConfigKey {
        static const QString &defaultKey() {
            static const QString str;
            return str;
        }

        const QString key;
        EnumValueType value;

        static QString getKey(const EnumConfigKey *keys, const EnumValueType &val) {
            QString ret;
            bool fnd = false;
            for ( ;!fnd && !keys->key.isEmpty(); ++keys) {
                if (keys->value == val) {
                    fnd = true;
                    ret = keys->key;
                }
            }
            return ret;
        }

        static EnumValueType getValue(const EnumConfigKey *keys, const QString &key, bool *valid = nullptr) {
            EnumValueType ret;
            bool fnd = false;
            for ( ;!fnd && !keys->key.isEmpty(); ++keys) {
                ret = keys->value;
                if (keys->key == key) {
                    fnd = true;
                }
            }

            if (!fnd) {
                ret = keys->value;
            }

            if (valid)
                *valid = fnd;
            return ret;
        }
    };
}

#endif // ENUMCFGKEY_H
