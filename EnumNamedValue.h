#ifndef ENUMVALUE_H
#define ENUMVALUE_H

#include <QString>
#include <QStringList>

namespace LQMeas {
    /* Сопоставлет некое значение кода из перечисления и строки, описывающей
       этот код. Для возможности перевода строки на лету, она создается
       не при создании объекта а при обращении к методу получения строки */
    template <typename EnumIdType> class EnumNamedValue {
    public:
        virtual EnumIdType id() const = 0;
        virtual QString displayName() const = 0;
        virtual ~EnumNamedValue() {}

        bool operator==(const EnumNamedValue& type) const {
            return id() == type.id();
        }

        bool operator==(const EnumIdType& val) const {
            return id() == val;
        }

        bool operator!=(const EnumNamedValue& type) const {
            return id() != type.id();
        }

        bool operator!=(const EnumIdType& val) const {
            return id() != val;
        }

        static QStringList namesList(const QList<const EnumNamedValue *> &enumVals) {
            QStringList ret;
            for (const EnumNamedValue * val : enumVals) {
                ret.append(val->displayName());
            }
            return ret;
        }

        static const EnumNamedValue *getByID(const QList<const EnumNamedValue *> &enumVals,
                                                 const EnumIdType &id,
                                                 const EnumNamedValue *defaultItem = nullptr) {
            const auto &it {std::find_if(enumVals.cbegin(), enumVals.cend(),
                                         [&id](const EnumNamedValue *enumVal){return enumVal->id() == id;})};
            return it != enumVals.cend() ? *it : defaultItem;
        }

        static const EnumNamedValue *getByName(const QList<const EnumNamedValue *> &enumVals,
                                               const QString &name,
                                               const EnumNamedValue *defaultItem = nullptr) {
            const auto &it {std::find_if(enumVals.cbegin(), enumVals.cend(),
                                         [&name](const EnumNamedValue *enumVal){return enumVal->displayName() == name;})};
            return it != enumVals.cend() ? *it : defaultItem;
        }
    };
}

#endif // ENUMVALUE_H
