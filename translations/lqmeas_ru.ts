<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>Device</name>
    <message>
        <location filename="../devs/LTR/modules/LTRModuleFrameReceiver.cpp" line="123"/>
        <source>Data receive error</source>
        <translation>Ошибка приема данных</translation>
    </message>
    <message>
        <location filename="../ifaces/in/adc/DevAdc.cpp" line="25"/>
        <source>Data acquisition started successfully</source>
        <translation>Сбор данных запущен успешно</translation>
    </message>
    <message>
        <location filename="../ifaces/in/adc/DevAdc.cpp" line="27"/>
        <source>Data acquisition start error</source>
        <translation>Ошибка запуска сбора данных</translation>
    </message>
    <message>
        <location filename="../ifaces/in/adc/DevAdc.cpp" line="39"/>
        <source>Data acquisition stoped successfully</source>
        <translation>Сбор данных остановлен успешно</translation>
    </message>
    <message>
        <location filename="../ifaces/in/adc/DevAdc.cpp" line="41"/>
        <source>Data acquisition stop error</source>
        <translation>Ошибка останова сбора данных</translation>
    </message>
    <message>
        <location filename="../ifaces/out/SyncModes/cycle/DevOutSyncModeImplCycle.cpp" line="95"/>
        <source>Output cycle generation setup done successfully</source>
        <translation>Установка циклического сигнала для генерации выполнена успешно</translation>
    </message>
    <message>
        <location filename="../ifaces/out/SyncModes/cycle/DevOutSyncModeImplCycle.cpp" line="97"/>
        <source>Output cycle generation setup error</source>
        <translation>Ошибка установки циклического сигнала для генерации</translation>
    </message>
    <message>
        <location filename="../ifaces/out/SyncModes/cycle/DevOutSyncModeImplCycle.cpp" line="104"/>
        <source>Output cycle generation started successfully</source>
        <translation>Запуск генерации циклического сигнала выполнен успешно</translation>
    </message>
    <message>
        <location filename="../ifaces/out/SyncModes/cycle/DevOutSyncModeImplCycle.cpp" line="106"/>
        <source>Output cycle generation start error</source>
        <translation>Ошибка запуска генерации циклического сигнала</translation>
    </message>
    <message>
        <location filename="../ifaces/out/SyncModes/cycle/DevOutSyncModeImplCycle.cpp" line="121"/>
        <source>Output cycle generation stopped successfully</source>
        <translation>Останов генерации циклического сигнала выполнен успешно</translation>
    </message>
    <message>
        <location filename="../ifaces/out/SyncModes/cycle/DevOutSyncModeImplCycle.cpp" line="123"/>
        <source>Output cycle generation stop error</source>
        <translation>Ошибка останова генерации циклического сигнала</translation>
    </message>
    <message>
        <location filename="../ifaces/out/SyncModes/cycle/DevOutSyncModeImplCycle.cpp" line="132"/>
        <source>Output cycle generation signal updated successfully</source>
        <translation>Генерируемые выходные сигналы обновлены успешно</translation>
    </message>
    <message>
        <location filename="../ifaces/out/SyncModes/cycle/DevOutSyncModeImplCycle.cpp" line="134"/>
        <source>Output cycle generation signal update error</source>
        <translation>Ошибка обновления генерируемых выходных сигналов обновлены успешно</translation>
    </message>
</context>
<context>
    <name>LQMeas::AnalogSignalConst</name>
    <message>
        <location filename="../ifaces/out/Signals/Analog/Const/AnalogSignalConst.h" line="14"/>
        <source>Constant</source>
        <translation>Пост. значение</translation>
    </message>
</context>
<context>
    <name>LQMeas::AnalogSignalMultiPoint</name>
    <message>
        <location filename="../ifaces/out/Signals/Analog/MultiPoint/AnalogSignalMultiPoint.h" line="14"/>
        <source>Multiple Point Based</source>
        <translation>По точкам</translation>
    </message>
</context>
<context>
    <name>LQMeas::AnalogSignalMultiPointConnectors</name>
    <message>
        <location filename="../ifaces/out/Signals/Analog/MultiPoint/AnalogSignalMultiPointConnector.cpp" line="7"/>
        <source>Step</source>
        <translation>Ступень</translation>
    </message>
    <message>
        <location filename="../ifaces/out/Signals/Analog/MultiPoint/AnalogSignalMultiPointConnector.cpp" line="23"/>
        <source>Linear</source>
        <translation>Линейное</translation>
    </message>
</context>
<context>
    <name>LQMeas::AnalogSignalPulse</name>
    <message>
        <location filename="../ifaces/out/Signals/Analog/Pulse/AnalogSignalPulse.h" line="13"/>
        <source>Pulse</source>
        <translation>Импульс</translation>
    </message>
</context>
<context>
    <name>LQMeas::AnalogSignalRamp</name>
    <message>
        <location filename="../ifaces/out/Signals/Analog/Ramp/AnalogSignalRamp.h" line="13"/>
        <source>Ramp</source>
        <translation>Пила</translation>
    </message>
</context>
<context>
    <name>LQMeas::AnalogSignalSin</name>
    <message>
        <location filename="../ifaces/out/Signals/Analog/Sin/AnalogSignalSin.h" line="13"/>
        <source>Sinus</source>
        <translation>Синус</translation>
    </message>
</context>
<context>
    <name>LQMeas::AnalogSignalTimeSeq</name>
    <message>
        <location filename="../ifaces/out/Signals/Analog/TimeSeq/AnalogSignalTimeSeq.h" line="14"/>
        <source>Time sequence of signals</source>
        <translation>Временная последовательность сигналов</translation>
    </message>
</context>
<context>
    <name>LQMeas::DevAdcTarePoints</name>
    <message>
        <location filename="../ifaces/in/adc/tare/DevAdcTarePoint.cpp" line="7"/>
        <source>Zero</source>
        <translation>Ноль</translation>
    </message>
    <message>
        <location filename="../ifaces/in/adc/tare/DevAdcTarePoint.cpp" line="15"/>
        <source>Scale</source>
        <translation>Шкала</translation>
    </message>
</context>
<context>
    <name>LQMeas::DevInCntrConfig</name>
    <message>
        <location filename="../ifaces/in/cntr/DevInCntrInfo.cpp" line="11"/>
        <source>Counter %1</source>
        <translation>Счетчик %1</translation>
    </message>
</context>
<context>
    <name>LQMeas::DevOutSyncStreamThread</name>
    <message>
        <location filename="../ifaces/out/SyncModes/stream/DevOutSyncStreamThread.cpp" line="141"/>
        <source>Output data stream stopped successfully</source>
        <translation>Поток вывода данных остановлен</translation>
    </message>
    <message>
        <location filename="../ifaces/out/SyncModes/stream/DevOutSyncStreamThread.cpp" line="143"/>
        <source>Output data stream stop error</source>
        <translation>Ошибка останова потока вывода данных</translation>
    </message>
</context>
<context>
    <name>LQMeas::Device</name>
    <message>
        <location filename="../devs/DeviceFrameSender.h" line="72"/>
        <source>Data send error</source>
        <translation>Ошибка передачи данных</translation>
    </message>
    <message>
        <location filename="../devs/Device.cpp" line="62"/>
        <source>Device opened successfully</source>
        <translation>Связь с устройством установлена успешно</translation>
    </message>
    <message>
        <location filename="../devs/Device.cpp" line="64"/>
        <source>Device open error</source>
        <translation>Ошибка открытия устройства</translation>
    </message>
    <message>
        <location filename="../devs/Device.cpp" line="79"/>
        <source>Device closed successfully</source>
        <translation>Связь с устройством закрыта успешно</translation>
    </message>
    <message>
        <location filename="../devs/Device.cpp" line="81"/>
        <source>Device close error</source>
        <translation>Ошибка закрытия устройства</translation>
    </message>
    <message>
        <location filename="../devs/DeviceConfig.cpp" line="99"/>
        <source>Found configuration for invalid device type: expected %1, found %2</source>
        <translation>Найденная конфигурация предназначена для другого типа устройства: ожидалось %1, найдено %2</translation>
    </message>
    <message>
        <location filename="../devs/DeviceConfig.cpp" line="114"/>
        <source>Load config (device %1)</source>
        <translation>Загрузка конфигурации (устройство %1)</translation>
    </message>
    <message>
        <location filename="../devs/Device.cpp" line="120"/>
        <source>Device configuration done successfully</source>
        <translation>Конфигурация устройства выполнена успешно</translation>
    </message>
    <message>
        <location filename="../devs/Device.cpp" line="123"/>
        <source>Device configuration error</source>
        <translation>Ошибка конфигурации устройства</translation>
    </message>
    <message>
        <location filename="../devs/DeviceFrameReceiver.h" line="63"/>
        <source>Data receive error</source>
        <translation>Ошибка приема данных</translation>
    </message>
</context>
<context>
    <name>LQMeas::DeviceInterfaces</name>
    <message>
        <location filename="../devs/DeviceInterface.cpp" line="7"/>
        <source>Unknown</source>
        <translation>Неизвестно</translation>
    </message>
    <message>
        <location filename="../devs/DeviceInterface.cpp" line="15"/>
        <source>USB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../devs/DeviceInterface.cpp" line="23"/>
        <source>PCI</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../devs/DeviceInterface.cpp" line="31"/>
        <source>PCI-Express</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../devs/DeviceInterface.cpp" line="39"/>
        <source>Ethernet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../devs/DeviceInterface.cpp" line="47"/>
        <source>LTR module</source>
        <translation>модуль LTR</translation>
    </message>
    <message>
        <location filename="../devs/DeviceInterface.cpp" line="55"/>
        <source>Local</source>
        <translation>Локальный</translation>
    </message>
</context>
<context>
    <name>LQMeas::DeviceRefMethods</name>
    <message>
        <location filename="../devs/DeviceRefMethod.cpp" line="7"/>
        <source>Unknown</source>
        <translation>Неизвестный</translation>
    </message>
    <message>
        <location filename="../devs/DeviceRefMethod.cpp" line="15"/>
        <source>Serial Number</source>
        <translation>Серийный номер</translation>
    </message>
    <message>
        <location filename="../devs/DeviceRefMethod.cpp" line="23"/>
        <source>IP Address</source>
        <translation>IP адрес</translation>
    </message>
    <message>
        <location filename="../devs/DeviceRefMethod.cpp" line="31"/>
        <source>Network name</source>
        <translation>Сетевое имя</translation>
    </message>
    <message>
        <location filename="../devs/DeviceRefMethod.cpp" line="39"/>
        <source>Crate slot</source>
        <translation>Слот крейта</translation>
    </message>
</context>
<context>
    <name>LQMeas::DeviceTypeStdSeries</name>
    <message>
        <location filename="../devs/DeviceTypeStdSeries.cpp" line="7"/>
        <location filename="../devs/DeviceTypeStdSeries.cpp" line="8"/>
        <source>Unspecified</source>
        <translation>Не определена</translation>
    </message>
    <message>
        <location filename="../devs/DeviceTypeStdSeries.cpp" line="16"/>
        <source>E Series</source>
        <translation>E серия</translation>
    </message>
    <message>
        <location filename="../devs/DeviceTypeStdSeries.cpp" line="17"/>
        <source>External ADC/DAC modules (E Series)</source>
        <translation>Внешние модули АЦП/ЦАП (E серия)</translation>
    </message>
    <message>
        <location filename="../devs/DeviceTypeStdSeries.cpp" line="25"/>
        <source>L Series</source>
        <translation>L серия</translation>
    </message>
    <message>
        <location filename="../devs/DeviceTypeStdSeries.cpp" line="26"/>
        <source>PCI plug-in ADC/DAC boards (L Series)</source>
        <translation>Встраиваемые в ПК платы АЦП/ЦАП (L серия)</translation>
    </message>
    <message>
        <location filename="../devs/DeviceTypeStdSeries.cpp" line="34"/>
        <source>LTR</source>
        <translation>LTR</translation>
    </message>
    <message>
        <location filename="../devs/DeviceTypeStdSeries.cpp" line="35"/>
        <source>LTR Rack System</source>
        <translation>Крейтовая система LTR</translation>
    </message>
    <message>
        <location filename="../devs/DeviceTypeStdSeries.cpp" line="43"/>
        <source>L-ViMS</source>
        <translation>L-ViMS</translation>
    </message>
    <message>
        <location filename="../devs/DeviceTypeStdSeries.cpp" line="44"/>
        <source>L-ViMS System</source>
        <translation>Система L-ViMS</translation>
    </message>
    <message>
        <location filename="../devs/DeviceTypeStdSeries.cpp" line="52"/>
        <source>VIB-4</source>
        <translation>ВИБ-4</translation>
    </message>
    <message>
        <location filename="../devs/DeviceTypeStdSeries.cpp" line="53"/>
        <source>VIB-4 System</source>
        <translation>Система ВИБ-4</translation>
    </message>
</context>
<context>
    <name>LQMeas::DiscreteSignalConst</name>
    <message>
        <location filename="../ifaces/out/Signals/Discrete/Const/DiscreteSignalConst.h" line="15"/>
        <source>Constant</source>
        <translation>Пост. значение</translation>
    </message>
</context>
<context>
    <name>LQMeas::DiscreteSignalMultiPoint</name>
    <message>
        <location filename="../ifaces/out/Signals/Discrete/MultiPoint/DiscreteSignalMultiPoint.h" line="14"/>
        <source>Multiple Point Based</source>
        <translation>По точкам</translation>
    </message>
</context>
<context>
    <name>LQMeas::DiscreteSignalPulse</name>
    <message>
        <location filename="../ifaces/out/Signals/Discrete/Pulse/DiscreteSignalPulse.h" line="13"/>
        <source>Pulse</source>
        <translation>Импульс</translation>
    </message>
</context>
<context>
    <name>LQMeas::DiscreteSignalSinglePulseBurst</name>
    <message>
        <location filename="../ifaces/out/Signals/Discrete/SinglePulseBurst/DiscreteSignalSinglePulseBurst.h" line="13"/>
        <source>Pulse Burst</source>
        <translation>Пачка импульсов</translation>
    </message>
</context>
<context>
    <name>LQMeas::E502</name>
    <message>
        <source>IP</source>
        <translation type="vanished">IP</translation>
    </message>
    <message>
        <source>Net Name</source>
        <oldsource>NetSvc</oldsource>
        <translation type="vanished">Сет. имя</translation>
    </message>
</context>
<context>
    <name>LQMeas::E502NetworkBrowser</name>
    <message>
        <source>Cannot start network browse for E502 modules</source>
        <translation type="vanished">Не удалось запустить поиск модулей E502 в сети</translation>
    </message>
    <message>
        <source>Error occured during wait for new browse event for E502 modules</source>
        <translation type="vanished">Во время ожидания событий при поиске модулей E502 в сети произошла ошибка</translation>
    </message>
</context>
<context>
    <name>LQMeas::LPW25</name>
    <message>
        <location filename="../sensors/LPW25/LPW25TypeInfo.cpp" line="106"/>
        <source>Channel 1</source>
        <translation>Канал 1</translation>
    </message>
    <message>
        <location filename="../sensors/LPW25/LPW25TypeInfo.cpp" line="118"/>
        <source>Channel 2</source>
        <translation>Канал 2</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTR11</name>
    <message>
        <location filename="../devs/LTR/modules/LTR11/LTR11.cpp" line="131"/>
        <source>Process data error</source>
        <translation>Ошибка обработки данных</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTR114</name>
    <message>
        <location filename="../devs/LTR/modules/LTR114/LTR114.cpp" line="128"/>
        <source>Process data error</source>
        <translation>Ошибка обработки данных</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTR12</name>
    <message>
        <location filename="../devs/LTR/modules/LTR12/LTR12.cpp" line="146"/>
        <source>Process data error</source>
        <translation>Ошибка обработки данных</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTR210</name>
    <message>
        <location filename="../devs/LTR/modules/LTR210/LTR210.cpp" line="229"/>
        <location filename="../devs/LTR/modules/LTR210/LTR210.cpp" line="263"/>
        <source>Process data error</source>
        <translation>Ошибка обработки данных</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTR212</name>
    <message>
        <location filename="../devs/LTR/modules/LTR212/LTR212.cpp" line="359"/>
        <source>Process data error</source>
        <translation>Ошибка обработки данных</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTR212AdcTareCoefs</name>
    <message>
        <location filename="../devs/LTR/modules/LTR212/LTR212AdcTareCoefs.cpp" line="10"/>
        <source>DAC Value</source>
        <translation>Значение ЦАП</translation>
    </message>
    <message>
        <location filename="../devs/LTR/modules/LTR212/LTR212AdcTareCoefs.cpp" line="20"/>
        <source>Offset Code</source>
        <oldsource>Offset Ceof</oldsource>
        <translation>Код смещения</translation>
    </message>
    <message>
        <location filename="../devs/LTR/modules/LTR212/LTR212AdcTareCoefs.cpp" line="31"/>
        <source>Scale Ceof</source>
        <translation>Коэф. шкалы</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTR212AdcTareOps</name>
    <message>
        <location filename="../devs/LTR/modules/LTR212/LTR212AdcTareOps.cpp" line="7"/>
        <source>External Tare</source>
        <translation>Внешняя тарировка</translation>
    </message>
    <message>
        <location filename="../devs/LTR/modules/LTR212/LTR212AdcTareOps.cpp" line="16"/>
        <source>Fabric Coefficients</source>
        <oldsource>Fabric Coefs</oldsource>
        <translation>Заводские коэффициенты</translation>
    </message>
    <message>
        <location filename="../devs/LTR/modules/LTR212/LTR212AdcTareOps.cpp" line="25"/>
        <source>Reset</source>
        <translation>Сброс</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTR216</name>
    <message>
        <location filename="../devs/LTR/modules/LTR216/LTR216AdcConfig.cpp" line="68"/>
        <source>All measurements</source>
        <translation>Все измерения</translation>
    </message>
    <message>
        <location filename="../devs/LTR/modules/LTR216/LTR216AdcConfig.cpp" line="77"/>
        <source>Without open circuit check</source>
        <translation>Без проверки обрыва</translation>
    </message>
    <message>
        <location filename="../devs/LTR/modules/LTR216/LTR216AdcConfig.cpp" line="87"/>
        <source>Without background table</source>
        <translation>Без фоновой таблицы</translation>
    </message>
    <message>
        <location filename="../devs/LTR/modules/LTR216/LTR216AdcConfig.cpp" line="97"/>
        <source>Unbalance only</source>
        <translation>Только разбаланс</translation>
    </message>
    <message>
        <location filename="../devs/LTR/modules/LTR216/LTR216.cpp" line="129"/>
        <source>Process data error</source>
        <translation>Ошибка обработки данных</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTR216AdcTareCoefs</name>
    <message>
        <location filename="../devs/LTR/modules/LTR216/LTR216AdcTareCoefs.cpp" line="9"/>
        <source>DAC Value</source>
        <translation>Значение ЦАП</translation>
    </message>
    <message>
        <location filename="../devs/LTR/modules/LTR216/LTR216AdcTareCoefs.cpp" line="19"/>
        <source>Offset Ceof</source>
        <oldsource>Offset Ceofficient</oldsource>
        <translation>Коэф. смещения</translation>
    </message>
    <message>
        <location filename="../devs/LTR/modules/LTR216/LTR216AdcTareCoefs.cpp" line="29"/>
        <source>Scale Ceof</source>
        <oldsource>Scale Ceofficient</oldsource>
        <translation>Коэф. шкалы</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTR216AdcTareOps</name>
    <message>
        <location filename="../devs/LTR/modules/LTR216/LTR216AdcTareOps.cpp" line="7"/>
        <source>External Tare</source>
        <translation>Внешняя тарировка</translation>
    </message>
    <message>
        <location filename="../devs/LTR/modules/LTR216/LTR216AdcTareOps.cpp" line="16"/>
        <source>Reset</source>
        <translation>Сброс</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTR22</name>
    <message>
        <location filename="../devs/LTR/modules/LTR22/LTR22.cpp" line="140"/>
        <source>Process data error</source>
        <translation>Ошибка обработки данных</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTR24</name>
    <message>
        <location filename="../devs/LTR/modules/LTR24/LTR24.cpp" line="158"/>
        <source>Process data error</source>
        <translation>Ошибка обработки данных</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTR25</name>
    <message>
        <location filename="../devs/LTR/modules/LTR25/LTR25.cpp" line="142"/>
        <source>Process data error</source>
        <translation>Ошибка обработки данных</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTR27</name>
    <message>
        <location filename="../devs/LTR/modules/LTR27/LTR27.cpp" line="157"/>
        <source>Process data error</source>
        <translation>Ошибка обработки данных</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTR34</name>
    <message>
        <location filename="../devs/LTR/modules/LTR34/LTR34.cpp" line="170"/>
        <source>Prepare data error</source>
        <translation>Ошибка подготовки данных</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTR35</name>
    <message>
        <location filename="../devs/LTR/modules/LTR35/LTR35.cpp" line="333"/>
        <source>Prepare data error</source>
        <translation>Ошибка подготовки данных</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTR51</name>
    <message>
        <location filename="../devs/LTR/modules/LTR51/LTR51.cpp" line="180"/>
        <source>Process data error</source>
        <translation>Ошибка обработки данных</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTRAvrUpdater</name>
    <message>
        <location filename="../devs/LTR/updaters/avr/LTRAvrUpdater.cpp" line="95"/>
        <location filename="../devs/LTR/updaters/avr/LTRAvrUpdater.cpp" line="249"/>
        <source>Open module</source>
        <translation>Установка соединения с модулем</translation>
    </message>
    <message>
        <location filename="../devs/LTR/updaters/avr/LTRAvrUpdater.cpp" line="105"/>
        <source>Read module information</source>
        <translation>Чтение информации о модуле</translation>
    </message>
    <message>
        <location filename="../devs/LTR/updaters/avr/LTRAvrUpdater.cpp" line="198"/>
        <source>Erase module flash</source>
        <translation>Стирание flash-памяти модуля</translation>
    </message>
    <message>
        <location filename="../devs/LTR/updaters/avr/LTRAvrUpdater.cpp" line="203"/>
        <location filename="../devs/LTR/updaters/avr/LTRAvrUpdater.cpp" line="318"/>
        <source>Write firmware to module flash</source>
        <translation>Запись прошивки во flash-память модуля</translation>
    </message>
    <message>
        <location filename="../devs/LTR/updaters/avr/LTRAvrUpdater.cpp" line="213"/>
        <source>Read firmware from module flash</source>
        <translation>Чтение прошивки из flash-памяти модуля</translation>
    </message>
    <message>
        <location filename="../devs/LTR/updaters/avr/LTRAvrUpdater.cpp" line="221"/>
        <source>Verify firmware</source>
        <translation>Верификация прошивки</translation>
    </message>
    <message>
        <location filename="../devs/LTR/updaters/avr/LTRAvrUpdater.cpp" line="230"/>
        <source>Update finish</source>
        <translation>Завершение обновления</translation>
    </message>
    <message>
        <location filename="../devs/LTR/updaters/avr/LTRAvrUpdater.cpp" line="256"/>
        <source>Setup low speed mode for module interface</source>
        <translation>Установка низкой скорости интерфейса модуля</translation>
    </message>
    <message>
        <location filename="../devs/LTR/updaters/avr/LTRAvrUpdater.cpp" line="261"/>
        <source>Write module AVR Fuses</source>
        <translation>Запись AVR Fuses модуля</translation>
    </message>
    <message>
        <location filename="../devs/LTR/updaters/avr/LTRAvrUpdater.cpp" line="272"/>
        <source>Restart crate</source>
        <translation>Перезапуск крейта</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTRAvrUpdaterErrors</name>
    <message>
        <location filename="../devs/LTR/updaters/avr/LTRAvrUpdaterErrors.cpp" line="5"/>
        <source>Cannot open firmware file (%1)</source>
        <translation>Не удалось открыть файл с прошивкой (%1)</translation>
    </message>
    <message>
        <location filename="../devs/LTR/updaters/avr/LTRAvrUpdaterErrors.cpp" line="9"/>
        <source>Unsupported updated LTR module ID (0x%1)</source>
        <translation>Неподдерживаемый идентификатор обновляемого модуля LTR (0x%1)</translation>
    </message>
    <message>
        <location filename="../devs/LTR/updaters/avr/LTRAvrUpdaterErrors.cpp" line="13"/>
        <source>Invalid firmware file size (%1 bytes)</source>
        <translation>Некорректный размер файла прошивки (%1 байт)</translation>
    </message>
    <message>
        <location filename="../devs/LTR/updaters/avr/LTRAvrUpdaterErrors.cpp" line="17"/>
        <source>Firmware verifycation error. Byte %1: write 0x%2, read 0x%3</source>
        <translation>Ошибка верификации прошивки. Байт %1: записано 0x%2, прочитано 0x%3</translation>
    </message>
    <message>
        <location filename="../devs/LTR/updaters/avr/LTRAvrUpdaterErrors.cpp" line="22"/>
        <source>Written AVR fuse verifycation error. Write 0x%1, read 0x%2</source>
        <translation>Ошибка верификации записанных AVR fuse. Записано 0x%1, прочитано 0x%2</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTRModule</name>
    <message>
        <location filename="../devs/LTR/modules/LTRModule.h" line="30"/>
        <source>Slot %1</source>
        <translation>Слот %1</translation>
    </message>
    <message>
        <location filename="../devs/LTR/LTRModuleRef.cpp" line="22"/>
        <source>Slot</source>
        <translation>Слот</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTRService</name>
    <message>
        <location filename="../devs/LTR/service/LTRServiceRef.cpp" line="38"/>
        <source>ltrd Service</source>
        <translation>Служба ltrd</translation>
    </message>
    <message>
        <location filename="../devs/LTR/service/LTRServiceRef.cpp" line="41"/>
        <source>local</source>
        <translation>Локальная</translation>
    </message>
</context>
<context>
    <name>LQMeas::LTRSyncMarkTypes</name>
    <message>
        <location filename="../devs/LTR/marks/LTRSyncMarks.cpp" line="8"/>
        <source>START</source>
        <translation>СТАРТ</translation>
    </message>
    <message>
        <location filename="../devs/LTR/marks/LTRSyncMarks.cpp" line="17"/>
        <source>SECOND</source>
        <translation>СЕКУНДА</translation>
    </message>
</context>
<context>
    <name>LQMeas::Log</name>
    <message>
        <location filename="../log/Log.cpp" line="14"/>
        <source>Critical Errors</source>
        <translation>Критические ошибки</translation>
    </message>
    <message>
        <location filename="../log/Log.cpp" line="15"/>
        <source>Errors</source>
        <translation>Ошибки</translation>
    </message>
    <message>
        <location filename="../log/Log.cpp" line="16"/>
        <source>Warnings</source>
        <translation>Предупреждения</translation>
    </message>
    <message>
        <location filename="../log/Log.cpp" line="17"/>
        <source>Info</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../log/Log.cpp" line="18"/>
        <source>Details</source>
        <translation>Детали</translation>
    </message>
    <message>
        <location filename="../log/Log.cpp" line="19"/>
        <source>Debug 1</source>
        <translation>Отладка 1</translation>
    </message>
    <message>
        <location filename="../log/Log.cpp" line="20"/>
        <source>Debug 2</source>
        <translation>Отладка 2</translation>
    </message>
    <message>
        <location filename="../log/Log.cpp" line="21"/>
        <source>Debug 3</source>
        <translation>Отладка 3</translation>
    </message>
</context>
<context>
    <name>LQMeas::LogModel</name>
    <message>
        <location filename="../log/LogModel.cpp" line="89"/>
        <source>Record time</source>
        <translation>Время записи</translation>
    </message>
    <message>
        <location filename="../log/LogModel.cpp" line="92"/>
        <source>Level</source>
        <translation>Уровень</translation>
    </message>
    <message>
        <location filename="../log/LogModel.cpp" line="95"/>
        <source>Text</source>
        <translation>Текст</translation>
    </message>
</context>
<context>
    <name>LQMeas::LogPanel</name>
    <message>
        <location filename="../log/LogPanel.cpp" line="50"/>
        <source>Ctrl+D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../log/LogPanel.cpp" line="141"/>
        <source>Log</source>
        <translation>Журнал</translation>
    </message>
    <message>
        <location filename="../log/LogPanel.cpp" line="142"/>
        <source>Show log level</source>
        <translation>Уровень отображения журнала</translation>
    </message>
    <message>
        <location filename="../log/LogPanel.cpp" line="143"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="../log/LogPanel.cpp" line="144"/>
        <source>Select All</source>
        <translation>Выделить все</translation>
    </message>
    <message>
        <location filename="../log/LogPanel.cpp" line="145"/>
        <source>Recalculate rows size</source>
        <translation>Пересчитать размер строк</translation>
    </message>
</context>
<context>
    <name>LQMeas::StdErrors</name>
    <message>
        <location filename="../StdErrors.cpp" line="10"/>
        <source>This configuration is not supported by device</source>
        <translation>Данная конфигурация не поддерживается устройством</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="14"/>
        <source>Data sending in progress. No free space for new data</source>
        <translation>Идет передача данных. Нет свободного места для новых данных</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="18"/>
        <source>Stream completion timeout</source>
        <translation>Ну удалось дождаться остановки потока данных</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="22"/>
        <source>Received less data than requested</source>
        <translation>Принято меньше данных, чем запрашивалось</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="26"/>
        <source>Invalid data acquisition parameters</source>
        <translation>Неверные параметры сбора данных</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="30"/>
        <source>Feature is not implemented!</source>
        <translation>Данная возможность не реализована!</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="34"/>
        <source>Output signal requires too many points</source>
        <translation>Выходной сигнал требует слишком большого числа точек</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="38"/>
        <source>Output generation already running</source>
        <translation>Генерация выходного сигнала уже запущена</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="42"/>
        <source>Invalid configuration type</source>
        <translation>Неверный тип конфигурации</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="46"/>
        <source>Data processing error</source>
        <translation>Ошибка обработки данных</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="50"/>
        <source>Unsupported synchronous output generation mode</source>
        <translation>Неподдерживаемый режим генерации синхронного вывода</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="54"/>
        <source>Specified device was diconnected</source>
        <translation>Указанное устройство было отключено</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="58"/>
        <source>No one channel is enabled</source>
        <translation>Не разрешено ни одного канала</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="62"/>
        <source>Specified signal type is not supported in current mode</source>
        <translation>Указанный тип сигнала не поддерживается в текущем режиме работы</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="66"/>
        <source>TEDS Node is found but TEDS chip is not supported by software (family 0x%1)</source>
        <oldsource>TEDS Node is found but TEDS chip is not supported by software (family 0x%1</oldsource>
        <translation>Узел TEDS успешно найден, но его микросхема TEDS не поддерживается программным обеспечением (семейство 0x%1)</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="70"/>
        <source>Specified device channel is not in TEDS data access mode</source>
        <translation>Указанный канал устройства не переведен в режим доступа к данным TEDS</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="74"/>
        <source>Specified invalid tare operation</source>
        <translation>Указана неверная тарировочная операция</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="78"/>
        <source>Specified invalid tare point</source>
        <translation>Указана неверная тарировочная точка</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="82"/>
        <source>No module data was received</source>
        <translation>Не было принято данных от модуля</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="86"/>
        <source>Unknown unit type (%1)</source>
        <translation>Неизвестная единица измерения (%1)</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="90"/>
        <source>Invalid operation for current output generation state</source>
        <translation>Недопустимая операция для текущего состояния генерации сигналов</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="94"/>
        <source>Generation start waiting timeout</source>
        <translation>Истекло время ожидания запуска генерации</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="98"/>
        <source>Generated signal update waiting timeout</source>
        <translation>Истекло время ожидания обновления сигналов генерации</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="102"/>
        <source>Requested feature is not supported</source>
        <translation>Запрашиваемая возможность не поддерживается</translation>
    </message>
    <message>
        <location filename="../StdErrors.cpp" line="106"/>
        <source>No valid device connection</source>
        <translation>Соединение с устройством не установлено</translation>
    </message>
</context>
<context>
    <name>LQMeas::Units::Groups</name>
    <message>
        <location filename="../units/std/Digital.cpp" line="8"/>
        <source>Digital</source>
        <translation>Цифровая величина</translation>
    </message>
    <message>
        <location filename="../units/std/Digital.cpp" line="27"/>
        <source>digital code</source>
        <translation>Цифровой код</translation>
    </message>
    <message>
        <location filename="../units/std/Digital.cpp" line="28"/>
        <source>digital codes</source>
        <translation>Цифровые коды</translation>
    </message>
    <message>
        <location filename="../units/std/Special.cpp" line="8"/>
        <source>Special</source>
        <translation>Специального назначения</translation>
    </message>
    <message>
        <location filename="../units/std/Special.cpp" line="27"/>
        <source>Unknown</source>
        <translation>Неизвестно</translation>
    </message>
    <message>
        <location filename="../units/std/Special.cpp" line="39"/>
        <source>Unitless</source>
        <translation>Безразмерный</translation>
    </message>
    <message>
        <location filename="../units/std/Information.cpp" line="10"/>
        <source>Information</source>
        <translation>Количество информации</translation>
    </message>
    <message>
        <location filename="../units/std/Information.cpp" line="41"/>
        <location filename="../units/std/Information.cpp" line="42"/>
        <source>bit</source>
        <translation>бит</translation>
    </message>
    <message>
        <location filename="../units/std/Information.cpp" line="43"/>
        <source>bits</source>
        <translation>биты</translation>
    </message>
    <message>
        <location filename="../units/std/Information.cpp" line="80"/>
        <source>B</source>
        <translation>Б</translation>
    </message>
    <message>
        <location filename="../units/std/Information.cpp" line="81"/>
        <source>byte</source>
        <translation>байт</translation>
    </message>
    <message>
        <location filename="../units/std/Information.cpp" line="82"/>
        <source>bytes</source>
        <translation>байты</translation>
    </message>
    <message>
        <location filename="../units/std/Discrete.cpp" line="8"/>
        <source>Discrete</source>
        <translation>Дискретная величина</translation>
    </message>
    <message>
        <location filename="../units/std/Discrete.cpp" line="28"/>
        <location filename="../units/std/Discrete.cpp" line="29"/>
        <source>logical level</source>
        <translation>логический уровень</translation>
    </message>
    <message>
        <location filename="../units/std/Relative.cpp" line="8"/>
        <source>Relative</source>
        <translation>Относительная величина</translation>
    </message>
    <message>
        <location filename="../units/std/Relative.cpp" line="29"/>
        <source>ratio</source>
        <translation>отношение</translation>
    </message>
    <message>
        <location filename="../units/std/Relative.cpp" line="40"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../units/std/Relative.cpp" line="41"/>
        <source>percent</source>
        <translation>процент</translation>
    </message>
    <message>
        <location filename="../units/std/Relative.cpp" line="42"/>
        <source>percents</source>
        <translation>проценты</translation>
    </message>
    <message>
        <location filename="../units/std/Relative.cpp" line="53"/>
        <source>dB</source>
        <translation>дБ</translation>
    </message>
    <message>
        <location filename="../units/std/Relative.cpp" line="54"/>
        <source>decibel</source>
        <translation>децибел</translation>
    </message>
    <message>
        <location filename="../units/std/Relative.cpp" line="55"/>
        <source>decibels</source>
        <translation>децибелы</translation>
    </message>
    <message>
        <location filename="../units/std/Angle.cpp" line="8"/>
        <source>Angle</source>
        <translation>Угол</translation>
    </message>
    <message>
        <location filename="../units/std/Angle.cpp" line="33"/>
        <source>°</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../units/std/Angle.cpp" line="35"/>
        <source>degree</source>
        <translation>градус</translation>
    </message>
    <message>
        <location filename="../units/std/Angle.cpp" line="36"/>
        <source>degrees</source>
        <translation>градусы</translation>
    </message>
    <message>
        <location filename="../units/std/Angle.cpp" line="46"/>
        <source>rad</source>
        <translation>рад</translation>
    </message>
    <message>
        <location filename="../units/std/Angle.cpp" line="47"/>
        <source>radian</source>
        <translation>радиан</translation>
    </message>
    <message>
        <location filename="../units/std/Angle.cpp" line="48"/>
        <source>radians</source>
        <translation>радианы</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="10"/>
        <source>Pressure</source>
        <translation>Давление</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="50"/>
        <source>Pa</source>
        <translation>Па</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="51"/>
        <source>pascal</source>
        <translation>паскаль</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="52"/>
        <source>pascals</source>
        <translation>паскали</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="62"/>
        <source>hPa</source>
        <translation>гПа</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="63"/>
        <source>hectopascal</source>
        <translation>гектопаскаль</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="64"/>
        <source>hectopascals</source>
        <translation>гектопаскали</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="74"/>
        <source>kPa</source>
        <translation>кПа</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="75"/>
        <source>kilopascal</source>
        <translation>килопаскаль</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="76"/>
        <source>kilopascals</source>
        <translation>килопаскали</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="86"/>
        <source>MPa</source>
        <translation>МПа</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="87"/>
        <source>megapascal</source>
        <translation>мегапаскаль</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="88"/>
        <source>megapascals</source>
        <translation>мегапаскали</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="98"/>
        <location filename="../units/std/Pressure.cpp" line="99"/>
        <source>bar</source>
        <translation>бар</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="100"/>
        <source>bars</source>
        <translation>бары</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="110"/>
        <source>mbar</source>
        <translation>мбар</translation>
    </message>
    <message>
        <source>millibar</source>
        <translation type="vanished">миллибар</translation>
    </message>
    <message>
        <source>millibars</source>
        <translation type="vanished">миллибары</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="111"/>
        <source>milibar</source>
        <translation>миллибар</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="112"/>
        <source>milibars</source>
        <translation>миллибары</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="122"/>
        <source>at</source>
        <translation>ат</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="123"/>
        <source>technical atmosphere</source>
        <translation>техническая атмосфера</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="124"/>
        <source>technical atmospheres</source>
        <translation>технические атмосферы</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="134"/>
        <source>ata</source>
        <translation>ата</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="135"/>
        <source>technical atmosphere (absolute)</source>
        <translation>техническая атмосфера (абсолютное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="136"/>
        <source>technical atmospheres (absolute)</source>
        <translation>технические атмосферы (абсолютное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="146"/>
        <source>atg</source>
        <translation>ати</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="147"/>
        <source>technical atmosphere (gauge)</source>
        <translation>техническая атмосфера (избыточное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="148"/>
        <source>technical atmospheres (gauge)</source>
        <translation>технические атмосферы (избыточное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="158"/>
        <source>atm</source>
        <translation>атм</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="159"/>
        <source>standard atmosphere</source>
        <translation>физическая атмосфера</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="160"/>
        <source>standard atmospheres</source>
        <translation>физические атмосферы</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="170"/>
        <source>kgf/cm²</source>
        <translation>кгс/см²</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="171"/>
        <source>kgf/cm^2</source>
        <translation>кгс/см^2</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="172"/>
        <source>kilogram-force per square centimeter</source>
        <translation>килограмм-сила на квадратный сантиметр</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="173"/>
        <source>kilograms-force per square centimeter</source>
        <translation>килограммы-сила на квадратный сантиметр</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="183"/>
        <source>kgf/m²</source>
        <translation>кгс/м²</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="184"/>
        <source>kgf/m^2</source>
        <translation>кгс/м^2</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="185"/>
        <source>kilogram-force per square meter</source>
        <translation>килограмм-сила на квадратный метр</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="186"/>
        <source>kilograms-force per square meter</source>
        <translation>килограммы-сила на квадратный метр</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="196"/>
        <source>psi</source>
        <translation>psi</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="197"/>
        <source>pound-force per square inch</source>
        <translation>фунт-сила на квадратный дюйм</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="198"/>
        <source>pounds-force per square inch</source>
        <translation>фунты-сила на квадратный дюйм</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="208"/>
        <source>psia</source>
        <translation>psia</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="209"/>
        <source>pound-force per square inch (absolute)</source>
        <translation>фунт-сила на квадратный дюйм (абсолютное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="210"/>
        <source>pounds-force per square inch (absolute)</source>
        <translation>фунты-сила на квадратный дюйм (абсолютное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="220"/>
        <source>psig</source>
        <translation>psig</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="221"/>
        <source>pound-force per square inch (gauge)</source>
        <translation>фунт-сила на квадратный дюйм (избыточное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="222"/>
        <source>pounds-force per square inch (gauge)</source>
        <translation>фунты-сила на квадратный дюйм (избыточное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="232"/>
        <source>mm Hg</source>
        <translation>мм рт. ст.</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="233"/>
        <source>millimetre of mercury</source>
        <translation>миллиметр ртутного столба</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="234"/>
        <source>millimetres of mercury</source>
        <translation>миллиметры ртутного столба</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="244"/>
        <source>mmwg</source>
        <translation>мм вод. ст.</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="245"/>
        <source>millimetre of water</source>
        <translation>миллиметр водяного столба</translation>
    </message>
    <message>
        <location filename="../units/std/Pressure.cpp" line="246"/>
        <source>millimeters, water gauge</source>
        <translation>миллиметры водяного столба</translation>
    </message>
    <message>
        <location filename="../units/std/Acceleration.cpp" line="9"/>
        <source>Acceleration</source>
        <translation>Ускорение</translation>
    </message>
    <message>
        <location filename="../units/std/Acceleration.cpp" line="34"/>
        <source>m/s²</source>
        <translation>м/c²</translation>
    </message>
    <message>
        <location filename="../units/std/Acceleration.cpp" line="35"/>
        <source>m/s^2</source>
        <translation>м/c^2</translation>
    </message>
    <message>
        <location filename="../units/std/Acceleration.cpp" line="36"/>
        <source>metre per second squared</source>
        <translation>метр на секунду в квадрате</translation>
    </message>
    <message>
        <location filename="../units/std/Acceleration.cpp" line="37"/>
        <source>metres per second squared</source>
        <translation>метры на секунду в квадрате</translation>
    </message>
    <message>
        <location filename="../units/std/Acceleration.cpp" line="47"/>
        <source>mm/s²</source>
        <translation>мм/c²</translation>
    </message>
    <message>
        <location filename="../units/std/Acceleration.cpp" line="48"/>
        <source>mm/s^2</source>
        <translation>мм/c^2</translation>
    </message>
    <message>
        <location filename="../units/std/Acceleration.cpp" line="49"/>
        <source>millimetre per second squared</source>
        <translation>миллиметр на секунду в квадрате</translation>
    </message>
    <message>
        <location filename="../units/std/Acceleration.cpp" line="50"/>
        <source>millimetres per second squared</source>
        <translation>миллиметры на секунду в квадрате</translation>
    </message>
    <message>
        <location filename="../units/std/Mass.cpp" line="9"/>
        <source>Mass</source>
        <translation>Масса</translation>
    </message>
    <message>
        <location filename="../units/std/Mass.cpp" line="36"/>
        <location filename="../units/std/Acceleration.cpp" line="60"/>
        <source>g</source>
        <translation>g</translation>
    </message>
    <message>
        <location filename="../units/std/Mass.cpp" line="37"/>
        <source>gram</source>
        <translation>грамм</translation>
    </message>
    <message>
        <location filename="../units/std/Mass.cpp" line="38"/>
        <source>grams</source>
        <translation>граммы</translation>
    </message>
    <message>
        <location filename="../units/std/Mass.cpp" line="48"/>
        <source>mg</source>
        <translation>мг</translation>
    </message>
    <message>
        <source>miligram</source>
        <translation type="vanished">миллиграмм</translation>
    </message>
    <message>
        <source>miligrams</source>
        <translation type="vanished">миллиграммы</translation>
    </message>
    <message>
        <location filename="../units/std/Mass.cpp" line="49"/>
        <source>milligram</source>
        <translation>миллиграмм</translation>
    </message>
    <message>
        <location filename="../units/std/Mass.cpp" line="50"/>
        <source>milligrams</source>
        <translation>миллиграммы</translation>
    </message>
    <message>
        <location filename="../units/std/Mass.cpp" line="60"/>
        <source>µg</source>
        <translation>мкг</translation>
    </message>
    <message>
        <location filename="../units/std/Mass.cpp" line="61"/>
        <source>microgram</source>
        <translation>микрограмм</translation>
    </message>
    <message>
        <location filename="../units/std/Mass.cpp" line="62"/>
        <source>micrograms</source>
        <translation>микрограммы</translation>
    </message>
    <message>
        <location filename="../units/std/Mass.cpp" line="72"/>
        <source>kg</source>
        <translation>кг</translation>
    </message>
    <message>
        <location filename="../units/std/Mass.cpp" line="73"/>
        <source>kilogram</source>
        <translation>килограмм</translation>
    </message>
    <message>
        <location filename="../units/std/Mass.cpp" line="74"/>
        <source>kilograms</source>
        <translation>килограммы</translation>
    </message>
    <message>
        <location filename="../units/std/Mass.cpp" line="84"/>
        <source>t</source>
        <translation>т</translation>
    </message>
    <message>
        <location filename="../units/std/Mass.cpp" line="85"/>
        <source>tonne</source>
        <translation>тонна</translation>
    </message>
    <message>
        <location filename="../units/std/Mass.cpp" line="86"/>
        <source>tonns</source>
        <translation>тонны</translation>
    </message>
    <message>
        <location filename="../units/std/Acceleration.cpp" line="61"/>
        <source>gravity</source>
        <translation>ускорение свободного падения</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="10"/>
        <source>Velocity</source>
        <translation>Скорость</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="38"/>
        <source>m/s</source>
        <translation>м/с</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="39"/>
        <source>metre per second</source>
        <translation>метр в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="40"/>
        <source>metres per second</source>
        <translation>метры в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="50"/>
        <source>cm/s</source>
        <translation>см/с</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="51"/>
        <source>centimetre per second</source>
        <translation>сантиметр в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="52"/>
        <source>centimetres per second</source>
        <translation>сантиметры в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="62"/>
        <source>mm/s</source>
        <translation>мм/с</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="63"/>
        <source>millimetre per second</source>
        <translation>миллиметр в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="64"/>
        <source>millimetres per second</source>
        <translation>миллиметры в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="74"/>
        <source>µm/s</source>
        <translation>мкм/c</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="75"/>
        <source>um/s</source>
        <translation>мкм/c</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="76"/>
        <source>micrometre per second</source>
        <translation>микрометр в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="77"/>
        <source>micrometres per second</source>
        <translation>микрометры в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="87"/>
        <source>in/s</source>
        <translation>дюйм/с</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="88"/>
        <location filename="../units/std/Velocity.cpp" line="89"/>
        <source>inch per second</source>
        <translation>дюйм в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="99"/>
        <source>km/h</source>
        <translation>км/ч</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="100"/>
        <source>kilometre per hour</source>
        <translation>километр в час</translation>
    </message>
    <message>
        <location filename="../units/std/Velocity.cpp" line="101"/>
        <source>kilometres per hour</source>
        <translation>километры в час</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="9"/>
        <source>Displacement</source>
        <translation>Перемещение</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="vanished">м</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="39"/>
        <source>m</source>
        <comment>metre</comment>
        <translation>м</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="40"/>
        <source>metre</source>
        <translation>метр</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="41"/>
        <source>metres</source>
        <translation>метры</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="51"/>
        <source>cm</source>
        <translation>см</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="52"/>
        <source>centimetre</source>
        <translation>сантиметр</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="53"/>
        <source>centimetres</source>
        <translation>сантиметры</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="63"/>
        <source>mm</source>
        <translation>мм</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="64"/>
        <source>millimetre</source>
        <translation>миллиметр</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="65"/>
        <source>millimetres</source>
        <translation>миллиметры</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="77"/>
        <source>µm</source>
        <translation>мкм</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="78"/>
        <source>um</source>
        <translation>мкм</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="79"/>
        <source>micrometre</source>
        <translation>микрометр</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="80"/>
        <source>micrometres</source>
        <translation>микрометры</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="90"/>
        <source>nm</source>
        <translation>нм</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="91"/>
        <source>nanometre</source>
        <translation>нанометр</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="92"/>
        <source>nanometres</source>
        <translation>нанометры</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="102"/>
        <source>km</source>
        <translation>км</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="103"/>
        <source>kilometre</source>
        <translation>километр</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="104"/>
        <source>kilometres</source>
        <translation>километры</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="114"/>
        <source>″</source>
        <translation>″</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="115"/>
        <source>inch</source>
        <translation>дюйм</translation>
    </message>
    <message>
        <location filename="../units/std/Displacement.cpp" line="116"/>
        <source>inches</source>
        <translation>дюймы</translation>
    </message>
    <message>
        <location filename="../units/std/RotationalSpeed.cpp" line="11"/>
        <source>Rotational Speed</source>
        <translation>Частота вращения</translation>
    </message>
    <message>
        <location filename="../units/std/RotationalSpeed.cpp" line="37"/>
        <source>rpm</source>
        <translation>об/мин</translation>
    </message>
    <message>
        <location filename="../units/std/RotationalSpeed.cpp" line="38"/>
        <location filename="../units/std/RotationalSpeed.cpp" line="39"/>
        <source>revolutions per minute</source>
        <translation>обороты в минуты</translation>
    </message>
    <message>
        <location filename="../units/std/RotationalSpeed.cpp" line="49"/>
        <source>rps</source>
        <translation>об/с</translation>
    </message>
    <message>
        <location filename="../units/std/RotationalSpeed.cpp" line="50"/>
        <location filename="../units/std/RotationalSpeed.cpp" line="51"/>
        <source>revolutions per second</source>
        <translation>обороты в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/RotationalSpeed.cpp" line="61"/>
        <source>rad/s</source>
        <translation>рад/с</translation>
    </message>
    <message>
        <location filename="../units/std/RotationalSpeed.cpp" line="62"/>
        <source>radian per second</source>
        <translation>радиан в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/RotationalSpeed.cpp" line="63"/>
        <source>radians per second</source>
        <translation>радианы в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Frequency.cpp" line="8"/>
        <source>Frequency</source>
        <translation>Частота</translation>
    </message>
    <message>
        <location filename="../units/std/Frequency.cpp" line="33"/>
        <source>Hz</source>
        <translation>Гц</translation>
    </message>
    <message>
        <location filename="../units/std/Frequency.cpp" line="34"/>
        <source>hertz</source>
        <translation>герц</translation>
    </message>
    <message>
        <location filename="../units/std/Frequency.cpp" line="44"/>
        <source>kHz</source>
        <translation>кГц</translation>
    </message>
    <message>
        <location filename="../units/std/Frequency.cpp" line="45"/>
        <source>kiloherz</source>
        <translation>килогерц</translation>
    </message>
    <message>
        <location filename="../units/std/Frequency.cpp" line="55"/>
        <source>MHz</source>
        <translation>МГц</translation>
    </message>
    <message>
        <location filename="../units/std/Frequency.cpp" line="56"/>
        <source>megaherz</source>
        <translation>мегагерц</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="8"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="40"/>
        <source>s</source>
        <translation>с</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="41"/>
        <source>second</source>
        <translation>секунда</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="42"/>
        <source>seconds</source>
        <translation>секунды</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="53"/>
        <source>ms</source>
        <translation>мс</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="54"/>
        <source>millisecond</source>
        <translation>миллисекунда</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="55"/>
        <source>milliseconds</source>
        <translation>миллисекунды</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="65"/>
        <source>µs</source>
        <translation>мкс</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="66"/>
        <source>us</source>
        <translation>мкс</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="67"/>
        <source>microsecond</source>
        <translation>микросекунда</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="68"/>
        <source>microseconds</source>
        <translation>микросекунды</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="78"/>
        <source>ns</source>
        <translation>нс</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="79"/>
        <source>nanosecond</source>
        <translation>наносекунда</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="80"/>
        <source>nanoseconds</source>
        <translation>наносекунды</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="90"/>
        <source>min</source>
        <translation>мин</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="91"/>
        <source>minute</source>
        <translation>минута</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="92"/>
        <source>minutes</source>
        <translation>минуты</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="102"/>
        <source>h</source>
        <translation>ч</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="103"/>
        <source>hour</source>
        <translation>час</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="104"/>
        <source>hours</source>
        <translation>часы</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="114"/>
        <source>d</source>
        <translation>д</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="115"/>
        <source>day</source>
        <translation>день</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="116"/>
        <source>days</source>
        <translation>дни</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="126"/>
        <source>w</source>
        <translation>нед</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="127"/>
        <source>week</source>
        <translation>неделя</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="128"/>
        <source>weeks</source>
        <translation>недели</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="138"/>
        <source>M</source>
        <comment>month</comment>
        <translation>мес</translation>
    </message>
    <message>
        <source>M</source>
        <translation type="vanished">мес</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="139"/>
        <source>month</source>
        <translation>месяц</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="140"/>
        <source>months</source>
        <translation>месяцы</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="150"/>
        <source>Y</source>
        <translation>г</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="151"/>
        <source>year</source>
        <translation>год</translation>
    </message>
    <message>
        <location filename="../units/std/Time.cpp" line="152"/>
        <source>years</source>
        <translation>годы</translation>
    </message>
    <message>
        <location filename="../units/std/Temperature.cpp" line="9"/>
        <source>Temperature</source>
        <translation>Температура</translation>
    </message>
    <message>
        <location filename="../units/std/Temperature.cpp" line="37"/>
        <source>K</source>
        <translation>К</translation>
    </message>
    <message>
        <location filename="../units/std/Temperature.cpp" line="38"/>
        <source>kelvin</source>
        <translation>кельвин</translation>
    </message>
    <message>
        <location filename="../units/std/Temperature.cpp" line="39"/>
        <source>kelvins</source>
        <translation>кельвины</translation>
    </message>
    <message>
        <location filename="../units/std/Temperature.cpp" line="50"/>
        <source>°C</source>
        <translation>°C</translation>
    </message>
    <message>
        <location filename="../units/std/Temperature.cpp" line="52"/>
        <source>degree Celsius</source>
        <translation>градус Цельсия</translation>
    </message>
    <message>
        <location filename="../units/std/Temperature.cpp" line="53"/>
        <source>degrees Celsius</source>
        <translation>градусы Цельсия</translation>
    </message>
    <message>
        <location filename="../units/std/Temperature.cpp" line="64"/>
        <source>°F</source>
        <translation>°F</translation>
    </message>
    <message>
        <location filename="../units/std/Temperature.cpp" line="65"/>
        <source>F</source>
        <translation>F</translation>
    </message>
    <message>
        <location filename="../units/std/Temperature.cpp" line="66"/>
        <source>degree Fahrenheit</source>
        <translation>градус Фаренгейта</translation>
    </message>
    <message>
        <location filename="../units/std/Temperature.cpp" line="67"/>
        <source>degrees Fahrenheit</source>
        <translation>градусы Фаренгейта</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="9"/>
        <source>Resistance</source>
        <translation>Сопротивление</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="38"/>
        <source>Ω</source>
        <translation>Ом</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="39"/>
        <source>Ohm</source>
        <translation>Ом</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="40"/>
        <source>ohm</source>
        <translation>ом</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="41"/>
        <source>ohms</source>
        <translation>омы</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="51"/>
        <source>kΩ</source>
        <translation>кОм</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="52"/>
        <source>kOhm</source>
        <translation>кОм</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="53"/>
        <source>kiloohm</source>
        <translation>килоом</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="54"/>
        <source>kiloohms</source>
        <translation>килоомы</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="64"/>
        <source>MΩ</source>
        <translation>МОм</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="65"/>
        <source>MOhm</source>
        <translation>МОм</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="66"/>
        <source>megaohm</source>
        <translation>магаом</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="67"/>
        <source>megaohms</source>
        <translation>мегаомы</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="77"/>
        <source>GΩ</source>
        <translation>ГОм</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="78"/>
        <source>GOhm</source>
        <translation>ГОм</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="79"/>
        <source>gigaohm</source>
        <translation>гигаом</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="80"/>
        <source>gigaohms</source>
        <translation>гигаомы</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="90"/>
        <source>mΩ</source>
        <translation>мОм</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="91"/>
        <source>mOhm</source>
        <translation>мОм</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="92"/>
        <source>milliohm</source>
        <translation>миллиом</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="93"/>
        <source>milliohms</source>
        <translation>миллиомы</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="103"/>
        <source>µΩ</source>
        <translation>мкОм</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="104"/>
        <source>uOhm</source>
        <translation>мкОм</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="105"/>
        <source>microohm</source>
        <translation>микроом</translation>
    </message>
    <message>
        <location filename="../units/std/Resistance.cpp" line="106"/>
        <source>microohms</source>
        <translation>микроомы</translation>
    </message>
    <message>
        <location filename="../units/std/Current.cpp" line="9"/>
        <source>Current</source>
        <translation>Ток</translation>
    </message>
    <message>
        <location filename="../units/std/Current.cpp" line="36"/>
        <source>A</source>
        <translation>А</translation>
    </message>
    <message>
        <location filename="../units/std/Current.cpp" line="37"/>
        <source>ampere</source>
        <translation>ампер</translation>
    </message>
    <message>
        <location filename="../units/std/Current.cpp" line="38"/>
        <source>amperes</source>
        <translation>амперы</translation>
    </message>
    <message>
        <location filename="../units/std/Current.cpp" line="49"/>
        <source>milliampere</source>
        <translation>миллиампер</translation>
    </message>
    <message>
        <location filename="../units/std/Current.cpp" line="50"/>
        <source>milliamperes</source>
        <translation>миллиамперы</translation>
    </message>
    <message>
        <location filename="../units/std/Current.cpp" line="61"/>
        <source>uA</source>
        <translation>мкА</translation>
    </message>
    <message>
        <location filename="../units/std/Current.cpp" line="62"/>
        <source>microampere</source>
        <translation>микроампер</translation>
    </message>
    <message>
        <location filename="../units/std/Current.cpp" line="63"/>
        <source>microamperes</source>
        <translation>микроамперы</translation>
    </message>
    <message>
        <location filename="../units/std/Current.cpp" line="74"/>
        <source>kiloampere</source>
        <translation>килоампер</translation>
    </message>
    <message>
        <location filename="../units/std/Current.cpp" line="75"/>
        <source>kiloamperes</source>
        <translation>килоамперы</translation>
    </message>
    <message>
        <source>amper</source>
        <translation type="vanished">ампер</translation>
    </message>
    <message>
        <source>ampers</source>
        <translation type="vanished">амперы</translation>
    </message>
    <message>
        <location filename="../units/std/Current.cpp" line="48"/>
        <source>mA</source>
        <translation>мА</translation>
    </message>
    <message>
        <source>milliamper</source>
        <translation type="vanished">миллиампер</translation>
    </message>
    <message>
        <source>milliampers</source>
        <translation type="vanished">миллиамперы</translation>
    </message>
    <message>
        <location filename="../units/std/Current.cpp" line="60"/>
        <source>µA</source>
        <translation>мкА</translation>
    </message>
    <message>
        <source>microamper</source>
        <translation type="vanished">микроампер</translation>
    </message>
    <message>
        <source>microampers</source>
        <translation type="vanished">микроамперы</translation>
    </message>
    <message>
        <location filename="../units/std/Current.cpp" line="73"/>
        <source>kA</source>
        <translation>кА</translation>
    </message>
    <message>
        <source>kiloamper</source>
        <translation type="vanished">килоампер</translation>
    </message>
    <message>
        <source>kiloampers</source>
        <translation type="vanished">килоамперы</translation>
    </message>
    <message>
        <location filename="../units/std/Voltage.cpp" line="7"/>
        <source>Voltage</source>
        <translation>Напряжение</translation>
    </message>
    <message>
        <location filename="../units/std/Voltage.cpp" line="34"/>
        <source>V</source>
        <translation>В</translation>
    </message>
    <message>
        <location filename="../units/std/Voltage.cpp" line="35"/>
        <source>volt</source>
        <translation>вольт</translation>
    </message>
    <message>
        <location filename="../units/std/Voltage.cpp" line="36"/>
        <source>volts</source>
        <translation>вольты</translation>
    </message>
    <message>
        <location filename="../units/std/Voltage.cpp" line="46"/>
        <source>mV</source>
        <translation>мВ</translation>
    </message>
    <message>
        <location filename="../units/std/Voltage.cpp" line="47"/>
        <source>millivolt</source>
        <translation>милливольт</translation>
    </message>
    <message>
        <location filename="../units/std/Voltage.cpp" line="48"/>
        <source>millivolts</source>
        <translation>милливольты</translation>
    </message>
    <message>
        <location filename="../units/std/Voltage.cpp" line="58"/>
        <source>µV</source>
        <translation>мкВ</translation>
    </message>
    <message>
        <location filename="../units/std/Voltage.cpp" line="59"/>
        <source>uV</source>
        <translation>мкВ</translation>
    </message>
    <message>
        <location filename="../units/std/Voltage.cpp" line="60"/>
        <source>microvolt</source>
        <translation>микровольты</translation>
    </message>
    <message>
        <location filename="../units/std/Voltage.cpp" line="61"/>
        <source>microvolts</source>
        <translation>микровольты</translation>
    </message>
    <message>
        <location filename="../units/std/Voltage.cpp" line="71"/>
        <source>kV</source>
        <translation>кВ</translation>
    </message>
    <message>
        <location filename="../units/std/Voltage.cpp" line="72"/>
        <source>kilovolt</source>
        <translation>киловольт</translation>
    </message>
    <message>
        <location filename="../units/std/Voltage.cpp" line="73"/>
        <source>kilovolts</source>
        <translation>киловольты</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="12"/>
        <source>Pressure impulse</source>
        <translation>Импульс давления</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="61"/>
        <source>Pa·s</source>
        <translation>Па·с</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="62"/>
        <source>pascal-second</source>
        <oldsource>pascal - second</oldsource>
        <translation>паскаль-секунда</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="63"/>
        <source>pascal-seconds</source>
        <oldsource>pascal - seconds</oldsource>
        <translation>паскаль-секунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="73"/>
        <source>Pa·ms</source>
        <translation>Па·мс</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="74"/>
        <source>pascal-millisecond</source>
        <oldsource>pascal - millisecond</oldsource>
        <translation>паскаль-миллисекунда</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="75"/>
        <source>pascal-milliseconds</source>
        <oldsource>pascal - milliseconds</oldsource>
        <translation>паскаль-миллисекунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="84"/>
        <source>Pa·min</source>
        <translation>Па·мин</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="85"/>
        <source>pascal-minute</source>
        <oldsource>pascal - minute</oldsource>
        <translation>паскаль-минута</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="86"/>
        <source>pascal-minutes</source>
        <oldsource>pascal - minutes</oldsource>
        <translation>паскаль-минуты</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="96"/>
        <source>bar·s</source>
        <translation>бар·с</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="97"/>
        <source>bar-second</source>
        <oldsource>bar - second</oldsource>
        <translation>бар-секунда</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="98"/>
        <source>bar-seconds</source>
        <oldsource>bar - seconds</oldsource>
        <translation>бар-секунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="108"/>
        <source>bar·ms</source>
        <translation>бар·мс</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="109"/>
        <source>bar-millisecond</source>
        <oldsource>bar - millisecond</oldsource>
        <translation>паскаль-миллисекунда</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="110"/>
        <source>bar-milliseconds</source>
        <oldsource>bar - milliseconds</oldsource>
        <translation>паскаль-миллисекунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="119"/>
        <source>bar·min</source>
        <translation>бар·мин</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="120"/>
        <source>bar-minute</source>
        <oldsource>bar - minute</oldsource>
        <translation>бар-минута</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="121"/>
        <source>bar-minutes</source>
        <oldsource>bar - minutes</oldsource>
        <translation>бар-минуты</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="131"/>
        <source>at·s</source>
        <translation>ат·с</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="132"/>
        <source>technical atmosphere-second</source>
        <oldsource>technical atmosphere - second</oldsource>
        <translation>техническая атмосфера - секунда</translation>
    </message>
    <message>
        <source>technical atmosphere- seconds</source>
        <oldsource>technical atmosphere - seconds</oldsource>
        <translation type="vanished">техническая атмосфера - секунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="143"/>
        <source>at·ms</source>
        <translation>ат·мс</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="144"/>
        <source>technical atmosphere-millisecond</source>
        <oldsource>technical atmosphere - millisecond</oldsource>
        <translation>техническая атмосфера - миллисекунда</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="145"/>
        <source>technical atmosphere-milliseconds</source>
        <oldsource>technical atmosphere - milliseconds</oldsource>
        <translation>техническая атмосфера - миллисекунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="154"/>
        <source>at·min</source>
        <translation>ат·мин</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="155"/>
        <source>technical atmosphere-minute</source>
        <oldsource>technical atmosphere - minute</oldsource>
        <translation>техническая атмосфера - минута</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="156"/>
        <source>technical atmosphere-minutes</source>
        <oldsource>technical atmosphere - minutes</oldsource>
        <translation>техническая атмосфера - минуты</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="166"/>
        <source>ata·s</source>
        <translation>ата·с</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="167"/>
        <source>technical atmosphere-second (absolute)</source>
        <translation>техническая атмосфера - секунда (абсолютное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="168"/>
        <source>technical atmosphere-seconds (absolute)</source>
        <translation>техническая атмосфера - секунды (абсолютное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="178"/>
        <source>ata·ms</source>
        <translation>ата·мс</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="179"/>
        <source>technical atmosphere-millisecond (absolute)</source>
        <translation>техническая атмосфера - миллисекунда (абсолютное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="180"/>
        <source>technical atmosphere-milliseconds (absolute)</source>
        <translation>техническая атмосфера - миллисекунды (абсолютное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="189"/>
        <source>ata·min</source>
        <translation>ата·мин</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="190"/>
        <source>technical atmosphere-minute (absolute)</source>
        <translation>техническая атмосфера - минута (абсолютное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="191"/>
        <source>technical atmosphere-minutes (absolute)</source>
        <translation>техническая атмосфера - минуты (абсолютное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="201"/>
        <source>atg·s</source>
        <translation>ати·с</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="202"/>
        <source>technical atmosphere-second (gauge)</source>
        <translation>техническая атмосфера - секунда (избыточное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="203"/>
        <source>technical atmosphere-seconds (gauge)</source>
        <translation>техническая атмосфера - секунды (избыточное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="213"/>
        <source>atg·ms</source>
        <translation>ата·мс</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="214"/>
        <source>technical atmosphere-millisecond (gauge)</source>
        <translation>техническая атмосфера - миллисекунда (избыточное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="215"/>
        <source>technical atmosphere-milliseconds (gauge)</source>
        <translation>техническая атмосфера - миллисекунды (избыточное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="224"/>
        <source>atg·min</source>
        <translation>ата·мин</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="225"/>
        <source>technical atmosphere-minute (gauge)</source>
        <translation>техническая атмосфера - минута (избыточное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="226"/>
        <source>technical atmosphere-minutes (gauge)</source>
        <translation>техническая атмосфера - минуты (избыточное давление)</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="237"/>
        <source>atm·s</source>
        <translation>атм·с</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="261"/>
        <source>standard atmosphere-minute</source>
        <translation>физическая атмосфера-минута</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="262"/>
        <source>standard atmosphere-minutes</source>
        <translation>физическая атмосфера-минуты</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="273"/>
        <source>millimetre of mercury-second</source>
        <translation>миллиметр ртутного столба-секунда</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="274"/>
        <source>millimetre of mercury-seconds</source>
        <translation>миллиметр ртутного столба-секунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="285"/>
        <source>millimetre of mercury-millisecond</source>
        <translation>миллиметр ртутного столба-миллисекунда</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="286"/>
        <source>millimetre of mercury-milliseconds</source>
        <translation>миллиметр ртутного столба-миллисекунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="296"/>
        <source>millimetre of mercury-minute</source>
        <translation>миллиметр ртутного столба-минута</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="297"/>
        <source>millimetre of mercury-minutes</source>
        <translation>миллиметр ртутного столба-минуты</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="309"/>
        <source>kgf/cm^2·s</source>
        <translation>кгс/см^2·c</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="310"/>
        <source>kilogram-force per square centimeter-second</source>
        <translation>килограмм-сила на квадратный сантиметр-секунда</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="311"/>
        <source>kilogram-force per square centimeter-seconds</source>
        <translation>килограмм-сила на квадратный сантиметр-секунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="322"/>
        <source>kgf/cm^2·ms</source>
        <translation>кгс/см^2·мc</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="323"/>
        <source>kilogram-force per square centimeter-millisecond</source>
        <translation>килограмм-сила на квадратный сантиметр-миллисекунда</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="324"/>
        <source>kilogram-force per square centimeter-milliseconds</source>
        <translation>килограмм-сила на квадратный сантиметр-миллисекунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="336"/>
        <source>kgf/cm^2·min</source>
        <translation>кгс/см^2·мин</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="337"/>
        <source>kilogram-force per square centimeter-minute</source>
        <translation>килограмм-сила на квадратный сантиметр-минута</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="338"/>
        <source>kilogram-force per square centimeter-minutes</source>
        <translation>килограмм-сила на квадратный сантиметр-минуты</translation>
    </message>
    <message>
        <source>standard atmosphere - second</source>
        <oldsource>standard atmosphere-second</oldsource>
        <translation type="vanished">физическая атмосфера - секунда</translation>
    </message>
    <message>
        <source>standard atmosphere - seconds</source>
        <oldsource>standard atmosphere-seconds</oldsource>
        <translation type="vanished">физическая атмосфера - секунды</translation>
    </message>
    <message>
        <source>standard atmosphere - millisecond</source>
        <oldsource>standard atmosphere-millisecond</oldsource>
        <translation type="vanished">физическая атмосфера - миллисекунда</translation>
    </message>
    <message>
        <source>standard atmosphere - milliseconds</source>
        <oldsource>standard atmosphere-milliseconds</oldsource>
        <translation type="vanished">физическая атмосфера - миллисекунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="249"/>
        <source>atm·ms</source>
        <translation>атм·мс</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="133"/>
        <source>technical atmosphere-seconds</source>
        <translation>техническая атмосфера-секунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="238"/>
        <source>standard atmosphere-second</source>
        <translation>физическая атмосфера-секунда</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="239"/>
        <source>standard atmosphere-seconds</source>
        <translation>физическая атмосфера-секунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="250"/>
        <source>standard atmosphere-millisecond</source>
        <translation>физическая атмосфера-миллисекунда</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="251"/>
        <source>standard atmosphere-milliseconds</source>
        <translation>физическая атмосфера-миллисекунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="260"/>
        <source>atm·min</source>
        <translation>атм·мин</translation>
    </message>
    <message>
        <source>standard atmosphere - minute</source>
        <translation type="vanished">физическая атмосфера-минута</translation>
    </message>
    <message>
        <source>standard atmosphere - minutes</source>
        <translation type="vanished">физическая атмосфера-минуты</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="272"/>
        <source>mm Hg·s</source>
        <translation>мм рт. ст. с</translation>
    </message>
    <message>
        <source>millimetre of mercury - second</source>
        <translation type="vanished">миллиметр ртутного столба - секунда</translation>
    </message>
    <message>
        <source>millimetre of mercury - seconds</source>
        <translation type="vanished">миллиметр ртутного столба - секунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="284"/>
        <source>mm Hg·ms</source>
        <translation>мм рт. ст. мс</translation>
    </message>
    <message>
        <source>millimetre of mercury - millisecond</source>
        <translation type="vanished">миллиметр ртутного столба - миллисекунда</translation>
    </message>
    <message>
        <source>millimetre of mercury - milliseconds</source>
        <translation type="vanished">миллиметр ртутного столба - миллисекунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="295"/>
        <source>mm Hg·min</source>
        <translation>мм рт. ст.·мин</translation>
    </message>
    <message>
        <source>millimetre of mercury - minute</source>
        <translation type="vanished">миллиметр ртутного столба - минута</translation>
    </message>
    <message>
        <source>millimetre of mercury - minutes</source>
        <translation type="vanished">миллиметр ртутного столба - минуты</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="308"/>
        <source>kgf/cm²·s</source>
        <translation>кгс/см²·c</translation>
    </message>
    <message>
        <source>kilogram-force per square centimeter - second</source>
        <translation type="vanished">килограмм-сила на квадратный сантиметр - секунда</translation>
    </message>
    <message>
        <source>kilogram-force per square centimeter - seconds</source>
        <translation type="vanished">килограмм-сила на квадратный сантиметр - секунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="321"/>
        <source>kgf/cm²·ms</source>
        <translation>кгс/см²·мc</translation>
    </message>
    <message>
        <source>kilogram-force per square centimeter - millisecond</source>
        <translation type="vanished">килограмм-сила на квадратный сантиметр - миллисекунда</translation>
    </message>
    <message>
        <source>kilogram-force per square centimeter - milliseconds</source>
        <translation type="vanished">килограмм-сила на квадратный сантиметр - миллисекунды</translation>
    </message>
    <message>
        <location filename="../units/std/PressureImpulse.cpp" line="335"/>
        <source>kgf/cm²·min</source>
        <translation>кгс/см²·мин</translation>
    </message>
    <message>
        <source>kilogram-force per square centimeter - minute</source>
        <translation type="vanished">килограмм-сила на квадратный сантиметр - минута</translation>
    </message>
    <message>
        <source>kilogram-force per square centimeter - minutes</source>
        <translation type="vanished">килограмм-сила на квадратный сантиметр - минуты</translation>
    </message>
    <message>
        <location filename="../units/std/ElectricCharge.cpp" line="10"/>
        <source>Electric Charge</source>
        <translation>Электрический заряд</translation>
    </message>
    <message>
        <location filename="../units/std/Temperature.cpp" line="51"/>
        <location filename="../units/std/ElectricCharge.cpp" line="37"/>
        <source>C</source>
        <translation>Кл</translation>
    </message>
    <message>
        <location filename="../units/std/ElectricCharge.cpp" line="38"/>
        <source>coulomb</source>
        <translation>кулон</translation>
    </message>
    <message>
        <location filename="../units/std/ElectricCharge.cpp" line="39"/>
        <source>coulombs</source>
        <translation>кулоны</translation>
    </message>
    <message>
        <location filename="../units/std/ElectricCharge.cpp" line="49"/>
        <source>pC</source>
        <translation>пКл</translation>
    </message>
    <message>
        <location filename="../units/std/ElectricCharge.cpp" line="50"/>
        <source>picocoulomb</source>
        <translation>пикокулон</translation>
    </message>
    <message>
        <location filename="../units/std/ElectricCharge.cpp" line="51"/>
        <source>picocoulombs</source>
        <translation>пикокулоны</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="10"/>
        <source>Power</source>
        <translation>Мощность</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="49"/>
        <source>W</source>
        <translation>Вт</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="50"/>
        <source>watt</source>
        <translation>ватт</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="51"/>
        <source>watts</source>
        <translation>ватты</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="61"/>
        <source>kW</source>
        <translation>кВт</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="62"/>
        <source>kilowatt</source>
        <translation>киловатт</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="63"/>
        <source>kilowatts</source>
        <translation>киловатты</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="73"/>
        <source>MW</source>
        <translation>МВт</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="74"/>
        <source>megawatt</source>
        <translation>мегаватт</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="75"/>
        <source>megawatts</source>
        <translation>мегаватты</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="85"/>
        <source>kgf⋅m/s</source>
        <translation>кгс·м/с</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="86"/>
        <source>kilogram-force-metre per second</source>
        <translation>килограмм-сила-метр в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="87"/>
        <source>kilogram-force-metres per second</source>
        <translation>килограмм-сила-метры в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="97"/>
        <source>erg/s</source>
        <translation>эрг/с</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="98"/>
        <source>erg per second</source>
        <translation>эрг в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="99"/>
        <source>ergs per second</source>
        <translation>эрг в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="109"/>
        <source>cal/s</source>
        <translation>кал/с</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="110"/>
        <source>calorie per second</source>
        <translation>калория в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="111"/>
        <source>calories per second</source>
        <translation>калории в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="121"/>
        <source>kcal/s</source>
        <translation>ккал/с</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="122"/>
        <source>kilocalorie per second</source>
        <translation>килокалория в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="123"/>
        <source>kilocalories per second</source>
        <translation>килокалории в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="133"/>
        <source>V⋅A</source>
        <translation>В⋅А</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="134"/>
        <source>volt-ampere</source>
        <translation>вольт-ампер</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="135"/>
        <source>volt-amperes</source>
        <translation>вольт-амперы</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="145"/>
        <source>kV⋅A</source>
        <translation>кВ⋅А</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="146"/>
        <source>kilovolt-ampere</source>
        <translation>киловольт-ампер</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="147"/>
        <source>kilovolt-amperes</source>
        <translation>киловольт-амперы</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="157"/>
        <source>MV⋅A</source>
        <translation>МВ⋅А</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="158"/>
        <source>megavolt-ampere</source>
        <translation>мегавольт-ампер</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="159"/>
        <source>megavolt-amperes</source>
        <translation>мегавольт-амперы</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="169"/>
        <source>var</source>
        <translation>вар</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="170"/>
        <source>volt-ampere reactive</source>
        <translation>вольт-ампер реактивный</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="171"/>
        <source>volt-amperes reactive</source>
        <translation>вольт-амперы реактивные</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="181"/>
        <source>kvar</source>
        <translation>квар</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="182"/>
        <source>kilovolt-ampere reactive</source>
        <translation>киловольт-ампер реактивный</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="183"/>
        <source>kilovolt-amperes reactive</source>
        <translation>киловольт-амперы реактивные</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="193"/>
        <source>Mvar</source>
        <translation>Мвар</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="194"/>
        <source>megvolt-ampere reactive</source>
        <translation>мегавольт-ампер реактивный</translation>
    </message>
    <message>
        <location filename="../units/std/Power.cpp" line="195"/>
        <source>megavolt-amperes reactive</source>
        <translation>мегавольт-амперы реактивные</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="10"/>
        <source>Energy</source>
        <translation>Энергия</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="47"/>
        <source>J</source>
        <translation>Дж</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="48"/>
        <source>joule</source>
        <translation>джоуль</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="49"/>
        <source>joules</source>
        <translation>джоули</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="59"/>
        <source>kJ</source>
        <translation>кДж</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="60"/>
        <source>kilojoule</source>
        <translation>килоджоуль</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="61"/>
        <source>kilojoules</source>
        <translation>килоджоули</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="71"/>
        <source>kgf·m</source>
        <translation>кгс·м</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="72"/>
        <source>kilogram-force-metre</source>
        <translation>киллограм-сила-метр</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="73"/>
        <source>kilogram-force-metres</source>
        <translation>киллограм-сила-метры</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="83"/>
        <location filename="../units/std/Energy.cpp" line="84"/>
        <source>erg</source>
        <translation>эрг</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="85"/>
        <source>ergs</source>
        <translation>эрг</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="95"/>
        <source>cal</source>
        <translation>кал</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="96"/>
        <source>calorie</source>
        <translation>калория</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="97"/>
        <source>calories</source>
        <translation>калории</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="107"/>
        <source>kcal</source>
        <translation>ккал</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="108"/>
        <source>kilocalorie</source>
        <translation>килокалория</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="109"/>
        <source>kilocalories</source>
        <translation>килокалории</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="121"/>
        <source>Wh</source>
        <translation>Вт⋅ч</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="122"/>
        <source>watt-hour</source>
        <translation>ватт-час</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="123"/>
        <source>watt-hours</source>
        <translation>ватт-часы</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="133"/>
        <source>kWh</source>
        <translation>кВт⋅ч</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="134"/>
        <source>kilowatt-hour</source>
        <translation>киловатт-час</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="135"/>
        <source>kilowatt-hours</source>
        <translation>киловатт-часы</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="145"/>
        <source>MWh</source>
        <translation>МВт⋅ч</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="146"/>
        <source>megawatt-hour</source>
        <translation>магаватт-час</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="147"/>
        <source>megawatt-hours</source>
        <translation>мегаватт-часы</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="157"/>
        <source>V⋅A⋅h</source>
        <translation>В⋅А⋅ч</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="158"/>
        <source>volt-ampere-hour</source>
        <translation>вольт-ампер-час</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="159"/>
        <source>volt-ampere-hours</source>
        <translation>вольт-ампер-часы</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="169"/>
        <source>kV⋅A⋅h</source>
        <translation>кВ⋅А⋅ч</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="170"/>
        <source>kilovolt-ampere-hour</source>
        <translation>киловольт-ампер-час</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="171"/>
        <source>kilovolt-ampere-hours</source>
        <translation>киловольт-ампер-часы</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="181"/>
        <source>MV⋅A⋅h</source>
        <translation>МВ⋅А⋅ч</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="182"/>
        <source>megavolt-ampere-hour</source>
        <translation>мегавольт-ампер-час</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="183"/>
        <source>megavolt-ampere-hours</source>
        <translation>мегавольт-ампер-часы</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="193"/>
        <source>var⋅h</source>
        <translation>вар⋅ч</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="194"/>
        <source>volt-ampere reactive-hour</source>
        <translation>вольт-ампер реактивный-час</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="195"/>
        <source>volt-ampere reactive-hours</source>
        <translation>вольт-ампер реактивный-часы</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="205"/>
        <source>kvar⋅h</source>
        <translation>квар⋅ч</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="206"/>
        <source>kilovolt-ampere reactive-hour</source>
        <translation>киловольт-ампер реактивный-час</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="207"/>
        <source>kilovolt-ampere reactive-hours</source>
        <translation>киловольт-ампер реактивный-часы</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="217"/>
        <source>Mvar⋅h</source>
        <translation>Мвар⋅ч</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="218"/>
        <source>megavolt-ampere reactive-hour</source>
        <translation>мегавольт-ампер реактивный-час</translation>
    </message>
    <message>
        <location filename="../units/std/Energy.cpp" line="219"/>
        <source>megavolt-ampere reactive-hours</source>
        <translation>мегавольт-ампер реактивный-часы</translation>
    </message>
    <message>
        <location filename="../units/std/Force.cpp" line="9"/>
        <source>Force</source>
        <translation>Сила</translation>
    </message>
    <message>
        <location filename="../units/std/Force.cpp" line="36"/>
        <source>N</source>
        <translation>Н</translation>
    </message>
    <message>
        <location filename="../units/std/Force.cpp" line="37"/>
        <source>newton</source>
        <translation>ньютон</translation>
    </message>
    <message>
        <location filename="../units/std/Force.cpp" line="38"/>
        <source>newtons</source>
        <translation>ньютоны</translation>
    </message>
    <message>
        <location filename="../units/std/Force.cpp" line="48"/>
        <source>kN</source>
        <translation>кН</translation>
    </message>
    <message>
        <location filename="../units/std/Force.cpp" line="49"/>
        <source>kilonewton</source>
        <translation>килоньютон</translation>
    </message>
    <message>
        <location filename="../units/std/Force.cpp" line="50"/>
        <source>kilonewtons</source>
        <translation>килоньютоны</translation>
    </message>
    <message>
        <location filename="../units/std/Force.cpp" line="60"/>
        <source>kgf</source>
        <translation>кгс</translation>
    </message>
    <message>
        <location filename="../units/std/Force.cpp" line="61"/>
        <source>kilogram-force</source>
        <translation>килограмм-сила</translation>
    </message>
    <message>
        <location filename="../units/std/Force.cpp" line="62"/>
        <source>kilograms-force</source>
        <translation>килограммы-сила</translation>
    </message>
    <message>
        <location filename="../units/std/Force.cpp" line="72"/>
        <source>gf</source>
        <translation>гс</translation>
    </message>
    <message>
        <location filename="../units/std/Force.cpp" line="73"/>
        <source>gram-force</source>
        <translation>грамм-сила</translation>
    </message>
    <message>
        <location filename="../units/std/Force.cpp" line="74"/>
        <source>grams-force</source>
        <translation>граммы-сила</translation>
    </message>
    <message>
        <location filename="../units/std/Force.cpp" line="84"/>
        <source>tf</source>
        <translation>тс</translation>
    </message>
    <message>
        <location filename="../units/std/Force.cpp" line="85"/>
        <source>tonne-force</source>
        <translation>тонна-сила</translation>
    </message>
    <message>
        <location filename="../units/std/Force.cpp" line="86"/>
        <source>tonns-force</source>
        <translation>тонны-сила</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="9"/>
        <source>Volume</source>
        <translation>Объем</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="36"/>
        <source>m³</source>
        <translation>м³</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="37"/>
        <source>m^3</source>
        <translation>м^3</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="38"/>
        <source>cubic metre</source>
        <translation>кубический метр</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="39"/>
        <source>cubic metres</source>
        <translation>кубические метры</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="49"/>
        <source>cm³</source>
        <translation>см³</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="50"/>
        <source>cm^3</source>
        <translation>см^3</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="51"/>
        <source>cubic centimetre</source>
        <translation>кубический сантиметр</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="52"/>
        <source>cubic centimetres</source>
        <translation>кубические сантиметры</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="62"/>
        <source>mm³</source>
        <translation>мм³</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="63"/>
        <source>mm^3</source>
        <translation>мм^3</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="64"/>
        <source>cubic millimetre</source>
        <translation>кубический миллиметр</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="65"/>
        <source>cubic millimetres</source>
        <translation>кубические миллиметры</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="75"/>
        <source>l</source>
        <translation>л</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="76"/>
        <source>litre</source>
        <translation>литр</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="77"/>
        <source>litres</source>
        <translation>литры</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="87"/>
        <source>ml</source>
        <translation>мл</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="88"/>
        <source>millilitre</source>
        <translation>миллилитр</translation>
    </message>
    <message>
        <location filename="../units/std/Volume.cpp" line="89"/>
        <source>millilitres</source>
        <translation>миллилитры</translation>
    </message>
    <message>
        <location filename="../units/std/RelativeDeformation.cpp" line="9"/>
        <source>Relative Deformation</source>
        <translation>Относительная деформация</translation>
    </message>
    <message>
        <location filename="../units/std/RelativeDeformation.cpp" line="34"/>
        <source>m/m</source>
        <translation>м/м</translation>
    </message>
    <message>
        <location filename="../units/std/RelativeDeformation.cpp" line="35"/>
        <source>metre per metre</source>
        <translation>метр на метр</translation>
    </message>
    <message>
        <location filename="../units/std/RelativeDeformation.cpp" line="36"/>
        <source>metres per metre</source>
        <translation>метры на метр</translation>
    </message>
    <message>
        <location filename="../units/std/RelativeDeformation.cpp" line="46"/>
        <source>mm/m</source>
        <translation>мм/м</translation>
    </message>
    <message>
        <location filename="../units/std/RelativeDeformation.cpp" line="47"/>
        <source>millimetre per metre</source>
        <translation>миллиметр на метр</translation>
    </message>
    <message>
        <location filename="../units/std/RelativeDeformation.cpp" line="48"/>
        <source>millimetres per metre</source>
        <translation>миллиметры на метр</translation>
    </message>
    <message>
        <location filename="../units/std/RelativeDeformation.cpp" line="58"/>
        <source>µm/m</source>
        <translation>мкм/м</translation>
    </message>
    <message>
        <location filename="../units/std/RelativeDeformation.cpp" line="59"/>
        <source>um/m</source>
        <translation>мкм/м</translation>
    </message>
    <message>
        <location filename="../units/std/RelativeDeformation.cpp" line="60"/>
        <source>micrometre per metre</source>
        <translation>микрометр на метр</translation>
    </message>
    <message>
        <location filename="../units/std/RelativeDeformation.cpp" line="61"/>
        <source>micrometres per metre</source>
        <translation>микрометры на метр</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="9"/>
        <source>Density</source>
        <translation>Плотность</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="36"/>
        <source>g/m³</source>
        <translation>г/м³</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="37"/>
        <source>g/m^3</source>
        <translation>г/м^3</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="38"/>
        <source>gram per cubic metre</source>
        <translation>грамм на кубический метр</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="39"/>
        <source>grams per cubic metre</source>
        <translation>граммы на кубический метр</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="49"/>
        <source>kg/m³</source>
        <translation>кг/м³</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="50"/>
        <source>kg/m^3</source>
        <translation>кг/м^3</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="51"/>
        <source>kilogram per cubic metre</source>
        <translation>килограмм на кубический метр</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="52"/>
        <source>kilograms per cubic metre</source>
        <translation>килограммы на кубический метр</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="62"/>
        <source>g/cm³</source>
        <translation>г/см³</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="63"/>
        <source>g/cm^3</source>
        <translation>г/см^3</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="64"/>
        <source>gram per cubic centimetre</source>
        <translation>грамм на кубический сантиметр</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="65"/>
        <source>grams per cubic centimetre</source>
        <translation>граммы на кубический сантиметр</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="75"/>
        <source>mg/cm³</source>
        <translation>мг/см³</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="76"/>
        <source>mg/cm^3</source>
        <translation>мг/см^3</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="77"/>
        <source>milligram per cubic centimetre</source>
        <translation>миллиграмм на кубический сантиметр</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="78"/>
        <source>milligrams per cubic centimetre</source>
        <translation>миллиграммы на кубический сантиметр</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="88"/>
        <source>g/l</source>
        <translation>г/л</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="89"/>
        <source>gram per litre</source>
        <translation>грамм на литр</translation>
    </message>
    <message>
        <location filename="../units/std/Density.cpp" line="90"/>
        <source>grams per litre</source>
        <translation>граммы на литр</translation>
    </message>
    <message>
        <location filename="../units/std/Area.cpp" line="10"/>
        <source>Area</source>
        <translation>Площадь</translation>
    </message>
    <message>
        <location filename="../units/std/Area.cpp" line="36"/>
        <source>m²</source>
        <translation>м²</translation>
    </message>
    <message>
        <location filename="../units/std/Area.cpp" line="37"/>
        <source>m^2</source>
        <translation>м^2</translation>
    </message>
    <message>
        <location filename="../units/std/Area.cpp" line="38"/>
        <source>square metre</source>
        <translation>квадратный метр</translation>
    </message>
    <message>
        <location filename="../units/std/Area.cpp" line="39"/>
        <source>square metres</source>
        <translation>квадратные метры</translation>
    </message>
    <message>
        <location filename="../units/std/Area.cpp" line="49"/>
        <source>cm²</source>
        <translation>см²</translation>
    </message>
    <message>
        <location filename="../units/std/Area.cpp" line="50"/>
        <source>cm^2</source>
        <translation>см^2</translation>
    </message>
    <message>
        <location filename="../units/std/Area.cpp" line="51"/>
        <source>square centimetre</source>
        <translation>квадратный сантиметр</translation>
    </message>
    <message>
        <location filename="../units/std/Area.cpp" line="52"/>
        <source>square centimetres</source>
        <translation>квадратные сантиметры</translation>
    </message>
    <message>
        <location filename="../units/std/Area.cpp" line="62"/>
        <source>mm²</source>
        <translation>мм²</translation>
    </message>
    <message>
        <location filename="../units/std/Area.cpp" line="63"/>
        <source>mm^2</source>
        <translation>мм^2</translation>
    </message>
    <message>
        <location filename="../units/std/Area.cpp" line="64"/>
        <source>square millimetre</source>
        <translation>квадратный миллиметр</translation>
    </message>
    <message>
        <location filename="../units/std/Area.cpp" line="65"/>
        <source>square millimetres</source>
        <translation>квадратные миллиметры</translation>
    </message>
    <message>
        <location filename="../units/std/Area.cpp" line="75"/>
        <source>″²</source>
        <translation>″²</translation>
    </message>
    <message>
        <location filename="../units/std/Area.cpp" line="76"/>
        <source>″^2</source>
        <translation>″^2</translation>
    </message>
    <message>
        <location filename="../units/std/Area.cpp" line="77"/>
        <source>square inch</source>
        <translation>квадратный дюйм</translation>
    </message>
    <message>
        <location filename="../units/std/Area.cpp" line="78"/>
        <source>square inches</source>
        <translation>квадратные дюймы</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="9"/>
        <source>Mass Flow Rate</source>
        <translation>Массовый расход</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="39"/>
        <source>g/s</source>
        <translation>г/с</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="40"/>
        <source>gram per second</source>
        <translation>грамм в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="41"/>
        <source>grams per second</source>
        <translation>граммы в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="52"/>
        <source>mg/s</source>
        <translation>мг/с</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="53"/>
        <source>milligram per second</source>
        <translation>миллиграмм в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="54"/>
        <source>milligrams per second</source>
        <translation>миллиграммы в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="64"/>
        <source>kg/s</source>
        <translation>кг/с</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="65"/>
        <source>kilogram per second</source>
        <translation>килограмм в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="66"/>
        <source>kilograms per second</source>
        <translation>килограммы в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="76"/>
        <source>kg/min</source>
        <translation>кг/мин</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="77"/>
        <source>kilogram per minute</source>
        <translation>килограмм в минуту</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="78"/>
        <source>kilograms per minute</source>
        <translation>килограммы в минуту</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="88"/>
        <source>kg/h</source>
        <translation>кг/ч</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="89"/>
        <source>kilogram per hour</source>
        <translation>килограмм в час</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="90"/>
        <source>kilograms per hour</source>
        <translation>килограммы в час</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="100"/>
        <source>t/s</source>
        <translation>т/с</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="101"/>
        <source>tonne per second</source>
        <translation>тонна в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="102"/>
        <source>tonns per second</source>
        <translation>тонны в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="112"/>
        <source>t/min</source>
        <translation>т/мин</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="113"/>
        <source>tonne per minute</source>
        <translation>тонна в минуту</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="114"/>
        <source>tonns per minute</source>
        <translation>тонны в минуту</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="124"/>
        <source>t/h</source>
        <translation>т/ч</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="125"/>
        <source>tonne per hour</source>
        <translation>тонна в час</translation>
    </message>
    <message>
        <location filename="../units/std/MassFlowRate.cpp" line="126"/>
        <source>tonns per hour</source>
        <translation>тонны в час</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="9"/>
        <source>Volumetric Flow Rate</source>
        <translation>Объёмный расход</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="38"/>
        <source>m³/s</source>
        <translation>м³/с</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="39"/>
        <source>m^3/s</source>
        <translation>м^3/с</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="40"/>
        <source>cubic metre per second</source>
        <translation>кубический метр в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="41"/>
        <source>cubic metres per second</source>
        <translation>кубические метры в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="51"/>
        <source>m³/min</source>
        <translation>м³/мин</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="52"/>
        <source>m^3/min</source>
        <translation>м^3/мин</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="53"/>
        <source>cubic metre per minute</source>
        <translation>кубический метр в минуту</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="54"/>
        <source>cubic metres per minute</source>
        <translation>кубические метры в минуту</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="64"/>
        <source>m³/h</source>
        <translation>м³/ч</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="65"/>
        <source>m^3/h</source>
        <translation>м^3/ч</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="66"/>
        <source>cubic metre per hour</source>
        <translation>кубический метр в час</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="67"/>
        <source>cubic metres per hour</source>
        <translation>кубические метры в час</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="77"/>
        <source>l/s</source>
        <translation>л/с</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="78"/>
        <source>litre per second</source>
        <translation>литр в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="79"/>
        <source>litres per second</source>
        <translation>литры в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="89"/>
        <source>l/min</source>
        <translation>л/мин</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="90"/>
        <source>litre per minute</source>
        <translation>литр в минуту</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="91"/>
        <source>litres per minute</source>
        <translation>литры в минуту</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="101"/>
        <source>l/h</source>
        <translation>л/ч</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="102"/>
        <source>litre per hour</source>
        <translation>литр в час</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="103"/>
        <source>litres per hour</source>
        <translation>литры в час</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="113"/>
        <source>ml/s</source>
        <translation>мл/с</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="114"/>
        <source>millilitre per second</source>
        <translation>миллилитр в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/VolumetricFlowRate.cpp" line="115"/>
        <source>millilitres per second</source>
        <translation>миллилитры в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/UnitPrefixes.h" line="31"/>
        <source>k</source>
        <translation>к</translation>
    </message>
    <message>
        <location filename="../units/std/UnitPrefixes.h" line="32"/>
        <source>kilo</source>
        <translation>кило</translation>
    </message>
    <message>
        <location filename="../units/std/UnitPrefixes.h" line="38"/>
        <source>M</source>
        <comment>mega</comment>
        <translation>М</translation>
    </message>
    <message>
        <location filename="../units/std/UnitPrefixes.h" line="39"/>
        <source>mega</source>
        <translation>мега</translation>
    </message>
    <message>
        <location filename="../units/std/UnitPrefixes.h" line="45"/>
        <source>G</source>
        <translation>Г</translation>
    </message>
    <message>
        <location filename="../units/std/UnitPrefixes.h" line="46"/>
        <source>giga</source>
        <translation>гига</translation>
    </message>
    <message>
        <location filename="../units/std/InformationSpeed.cpp" line="9"/>
        <source>Information Speed</source>
        <translation>Скорость передачи информации</translation>
    </message>
    <message>
        <location filename="../units/std/InformationSpeed.cpp" line="48"/>
        <source>bit/s</source>
        <translation>бит/с</translation>
    </message>
    <message>
        <location filename="../units/std/InformationSpeed.cpp" line="49"/>
        <source>bit per second</source>
        <translation>бит в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/InformationSpeed.cpp" line="50"/>
        <source>bits per second</source>
        <translation>биты в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/InformationSpeed.cpp" line="87"/>
        <source>B/s</source>
        <translation>Б/с</translation>
    </message>
    <message>
        <location filename="../units/std/InformationSpeed.cpp" line="88"/>
        <source>byte per second</source>
        <translation>байт в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/InformationSpeed.cpp" line="89"/>
        <source>bytes per second</source>
        <translation>байты в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/InformationSpeed.cpp" line="126"/>
        <source>words/s</source>
        <translation>слов/с</translation>
    </message>
    <message>
        <source>word/s</source>
        <translation type="vanished">слов/с</translation>
    </message>
    <message>
        <location filename="../units/std/InformationSpeed.cpp" line="127"/>
        <source>word per second</source>
        <translation>слова в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/InformationSpeed.cpp" line="128"/>
        <source>words per second</source>
        <translation>слов в секунду</translation>
    </message>
    <message>
        <location filename="../units/std/InformationSpeed.cpp" line="165"/>
        <source>B/h</source>
        <translation>Б/ч</translation>
    </message>
    <message>
        <location filename="../units/std/InformationSpeed.cpp" line="166"/>
        <source>byte per hour</source>
        <translation>байт в час</translation>
    </message>
    <message>
        <location filename="../units/std/InformationSpeed.cpp" line="167"/>
        <source>bytes per hour</source>
        <translation>байты в час</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViAcqChannelConfig</name>
    <message>
        <location filename="../devs/vi/config/acq/channel/types/ViAcqChannelTypePhaseTrigger.cpp" line="19"/>
        <source>Phase trigger</source>
        <translation>Фазоотметчик</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/channel/types/ViAcqChannelTypeRadialVibration.cpp" line="23"/>
        <source>Radial vibration</source>
        <translation>Радиальная вибрация</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/channel/types/ViAcqChannelTypeVibroAccel.cpp" line="30"/>
        <source>Vibroacceleration</source>
        <translation>Виброускорение</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/channel/types/ViAcqChannelTypeUnknown.cpp" line="20"/>
        <source>Unknown</source>
        <translation>Неизвестный тип</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/channel/types/ViAcqChannelTypeAxialPosition.cpp" line="20"/>
        <source>Axial Position</source>
        <translation>Осевое смещение</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViAcqChannelPhaseTrigPolarities</name>
    <message>
        <location filename="../devs/vi/config/acq/channel/phasetrig/ViAcqChannelPhaseTrigPolarity.cpp" line="8"/>
        <source>Projection</source>
        <translation>Выступ</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/channel/phasetrig/ViAcqChannelPhaseTrigPolarity.cpp" line="17"/>
        <source>Notch</source>
        <translation>Впадина</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViAcqChannelPositionNormalDirections</name>
    <message>
        <location filename="../devs/vi/config/acq/channel/position/ViAcqChannelPositionNormalDirection.cpp" line="9"/>
        <source>Toward Probe</source>
        <translation>К датчику</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/channel/position/ViAcqChannelPositionNormalDirection.cpp" line="18"/>
        <source>Away from Probe</source>
        <translation>От датчика</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViAcqChannelStatusCodes</name>
    <message>
        <location filename="../devs/vi/data/state/acq/ViAcqChannelStatus.cpp" line="8"/>
        <source>Unknown</source>
        <translation>Неизвестное состояние</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/acq/ViAcqChannelStatus.cpp" line="18"/>
        <source>Not ready</source>
        <translation>Нет данных</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/acq/ViAcqChannelStatus.cpp" line="28"/>
        <source>Valid data</source>
        <translation>Действительные данные</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/acq/ViAcqChannelStatus.cpp" line="38"/>
        <source>Short circuit</source>
        <translation>КЗ</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/acq/ViAcqChannelStatus.cpp" line="48"/>
        <source>Open circuit</source>
        <translation>Обрыв</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/acq/ViAcqChannelStatus.cpp" line="58"/>
        <source>Out of sensor valid range</source>
        <translation>Вне действительного диапазона датчика</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViAcqSensorConTypes</name>
    <message>
        <location filename="../devs/vi/config/acq/sensor/ViAcqSensorConType.cpp" line="8"/>
        <source>ICP</source>
        <translation>ICP</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/sensor/ViAcqSensorConType.cpp" line="19"/>
        <source>Voltage out, positive supply</source>
        <translation>Вых. по напр., полож. питание</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/sensor/ViAcqSensorConType.cpp" line="30"/>
        <source>Voltage out, negative supply</source>
        <translation>Вых. по напр., отриц. питание</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViAcqSensorConfig</name>
    <message>
        <location filename="../devs/vi/config/acq/sensor/types/proximeter/ViAcqSensorTypeProximeter.cpp" line="19"/>
        <source>Proximeter</source>
        <translation>Проксиметр</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/sensor/types/accelerometer/ViAcqSensorTypeAccelerometer.cpp" line="18"/>
        <source>Accelerometer</source>
        <translation>Акселерометр</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/sensor/types/unknown/ViAcqSensorTypeUnknown.cpp" line="17"/>
        <source>Unknown</source>
        <translation>Неизвестный тип</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViDiagnosticAdcStdChsInfo</name>
    <message>
        <location filename="../devs/vi/modules/ViDiagnosticAdcChInfo.cpp" line="12"/>
        <source>High Power Relay %1</source>
        <translation>Сильнотоковое реле %1</translation>
    </message>
    <message>
        <location filename="../devs/vi/modules/ViDiagnosticAdcChInfo.cpp" line="22"/>
        <source>Vref</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../devs/vi/modules/ViDiagnosticAdcChInfo.cpp" line="32"/>
        <source>Tmcu</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../devs/vi/modules/ViDiagnosticAdcChInfo.cpp" line="42"/>
        <source>Upow</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LQMeas::ViDinChannelConfig</name>
    <message>
        <location filename="../devs/vi/config/din/ViDinChannelConfig.cpp" line="22"/>
        <source>DIN</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LQMeas::ViDoutChannelTypes</name>
    <message>
        <location filename="../devs/vi/config/dout/ViDoutChannelType.cpp" line="8"/>
        <source>Low Power Relay</source>
        <translation>Низкотоковое реле</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/dout/ViDoutChannelType.cpp" line="9"/>
        <source>Low Power Relays</source>
        <translation>Низкотоковые реле</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/dout/ViDoutChannelType.cpp" line="18"/>
        <source>High Power Relay</source>
        <translation>Высокотоковое реле</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/dout/ViDoutChannelType.cpp" line="19"/>
        <source>High Power Relays</source>
        <translation>Высокотоковые реле</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViDoutNormalOpStates</name>
    <message>
        <location filename="../devs/vi/config/dout/ViDoutNormalOpState.cpp" line="8"/>
        <source>Normally De-Energized</source>
        <translation>Обесточено без тревоги</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/dout/ViDoutNormalOpState.cpp" line="17"/>
        <source>Normally Energized</source>
        <translation>Запитано без тревоги</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViErrors</name>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="8"/>
        <source>Device error: %1</source>
        <translation>Ошибка устройства: %1</translation>
    </message>
    <message>
        <source>Modbus register operation error</source>
        <translation type="vanished">Ошибка выполнения операции с регистрами modbus</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="19"/>
        <source>Invalid connection parameters</source>
        <translation>Неверные параметры подключения</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="23"/>
        <source>Cannot setup connection with device</source>
        <translation>Не удалось установить соединение с устройством</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="27"/>
        <source>Connection with device is not opened</source>
        <translation>Связь с устройством не была установлена</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="31"/>
        <source>Connection lost</source>
        <translation>Связь с устройством была потеряна</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="35"/>
        <source>Connection was reset by another side</source>
        <translation>Соединение сброшено устройством</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="39"/>
        <source>Invalid command response format</source>
        <translation>Неверный формат ответа на команду</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="43"/>
        <source>Invalid command response status</source>
        <translation>Неверный статус ответа на команду</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="47"/>
        <source>Insufficient command response data size</source>
        <translation>Недостаточный размер данных в ответе на команду</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="51"/>
        <source>No corresponding active configuration exchange</source>
        <translation>Нет соответствующей активной передаваемой конфигурации</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="55"/>
        <source>No valid configuration was detected in module</source>
        <translation>В модуле отсутствует действительная конфигурация</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="59"/>
        <location filename="../devs/vi/ViErrors.cpp" line="63"/>
        <source>Invalid read configuration block data size</source>
        <translation>Неверный размер данных в прочитанном конфигурационном блоке</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="67"/>
        <source>Measurements types mismatch on configuration parse</source>
        <translation>Несоответствие типов измерения при разборе конфигурации</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="71"/>
        <source>Written configuration check data mismatch</source>
        <translation>Ошибка проверки данных записанной конфигурации</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="75"/>
        <source>Livebit change is not detected</source>
        <translation>Обнаружено отсутствие изменения состояния бита жизни</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="79"/>
        <source>Stream data overflow</source>
        <translation>Переполнение потока данных</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="83"/>
        <source>Stream data receive timeout</source>
        <translation>Истек таймаут приема потока данных</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="87"/>
        <source>Data acquisition channel %1 is not enabled in module configuration</source>
        <translation>Канал сбора данных %1 не разрешен в конфигурации модуля</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="94"/>
        <source>Invalid control sequence</source>
        <translation>Неверная управляющая последовательность</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="95"/>
        <source>Invalid command signature</source>
        <translation>Неверный признак команды</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="96"/>
        <source>Invalid write sequence size</source>
        <translation>Неверный размер записываемой последовательности</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="97"/>
        <source>Invalid CRC</source>
        <translation>Ошибка проверки контрольной суммы</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="98"/>
        <source>Invalid command data size</source>
        <translation>Неверный размер данных в команде</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="99"/>
        <source>Flash write error</source>
        <translation>Ошибка записи во flash память</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="100"/>
        <source>Invalid Modbus register boundary cross</source>
        <translation>Некорректное пересечение границы областей modbus регистров</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="101"/>
        <source>Waveform is not present</source>
        <translation>Указанная выборка отсутствует</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="102"/>
        <source>Out of waveform data</source>
        <translation>Запрос вне доступных данных выборки</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="103"/>
        <source>Invalid waveform size requested</source>
        <translation>Запрошен неверный размер выборки</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="104"/>
        <source>No waveform data</source>
        <translation>Нет данных в запрошенной выборке</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="105"/>
        <source>Invalid wavefrom type</source>
        <translation>Неверный тип выборки</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="106"/>
        <source>Waveforms are not supported for this module type</source>
        <translation>Выборки не поддерживаются для данного типа модуля</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="107"/>
        <source>Invalid command code</source>
        <translation>Неверный код команды</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="108"/>
        <source>Device not ready to execute specified operation</source>
        <translation>Устройство не готово к выполнению указанной операции</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="109"/>
        <source>Specified operation is not supported in current device mode</source>
        <translation>Указанная операция недоступна в текущем режиме работы устройства</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="110"/>
        <source>Carrier board flash memory is not detected</source>
        <translation>Не была обнаружена flash-память на несущей плате</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="111"/>
        <source>Carrier board flash memory exchange error</source>
        <translation>Ошибка обмена с flash-памятью на несущей плате</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="112"/>
        <source>Mezzanine flash memory is not detected</source>
        <translation>Не обнаружена flash-память мезанина</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="113"/>
        <source>Mezzanine flash memory exchange error</source>
        <translation>Ошибка обмена с flash-памятью мезанина</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="114"/>
        <source>Unsupported data format version</source>
        <translation>Неподдерживаемая версия формата</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="115"/>
        <source>Invalid module type</source>
        <translation>Неверный тип модуля</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="116"/>
        <source>Written flash data comparsion error</source>
        <translation>Ошибка проверки записанных во flash-память данных</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="117"/>
        <source>Invalid network port number</source>
        <translation>Неверный номер сетевого порта</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="118"/>
        <source>Unsupported network port feature</source>
        <translation>Неподдерживаемая возможность сетевого порта</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="119"/>
        <source>Ethernet switch mcs is not detected</source>
        <translation>Микросхема ethernet свича не обнаружена</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="120"/>
        <source>Invalid command parameter value</source>
        <translation>Неверное значение параметра команды</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="121"/>
        <source>Specified operation is not started</source>
        <translation>Указанная операция не запущена</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="122"/>
        <source>Network port initialization error</source>
        <translation>Ошибка инициализации сетевого порта</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="123"/>
        <source>Operational configuration modification is not started</source>
        <translation>Изменение конфигурации режимов работы не было начато</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="124"/>
        <source>Operational configuration modification is not finished</source>
        <translation>Изменение конфигурации режимов работы не было закончено</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="125"/>
        <source>Invalid operational configuration identification</source>
        <translation>Неверный идентификатор конфигурации режимов работы</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="126"/>
        <source>Invalid operational configuration block number</source>
        <translation>Неверный номер блока данных конфигурации режимов работы</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="127"/>
        <source>Operational configuration size is too large</source>
        <translation>Размер конфигурации режимов работы слишком большой</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="128"/>
        <source>Invalid operational configuration CRC</source>
        <translation>Неверная контрольная сумма конфигурации режимов работы</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="129"/>
        <source>Operational configuration is not valid</source>
        <translation>Конфигурация режимов работы не действительна</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="130"/>
        <source>Invalid operational configuration data format</source>
        <translation>Неверный формат конфигурации режимов работы</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="131"/>
        <source>Unknown operational configuration measurement type</source>
        <translation>Неизвестный тип измерения в конфигурации режимов работы</translation>
    </message>
    <message>
        <location filename="../devs/vi/ViErrors.cpp" line="132"/>
        <source>Unknown error code %1</source>
        <translation>Неизвестный код ошибки %1</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViEventConfig</name>
    <message>
        <location filename="../devs/vi/config/events/ViEventConfig.cpp" line="29"/>
        <source>Event</source>
        <translation>Событие</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViLogicElementConfigAnd</name>
    <message>
        <location filename="../devs/vi/config/events/logic/types/ViLogicElementConfigAnd.cpp" line="10"/>
        <source>AND</source>
        <translation>И</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViLogicElementConfigDin</name>
    <message>
        <location filename="../devs/vi/config/events/logic/types/ViLogicElementConfigDin.cpp" line="14"/>
        <source>Discrete input state</source>
        <translation>Состояние дискретного входа</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViLogicElementConfigEvent</name>
    <message>
        <location filename="../devs/vi/config/events/logic/types/ViLogicElementConfigEvent.cpp" line="14"/>
        <source>Event state</source>
        <translation>Состояние события</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViLogicElementConfigMeasCond</name>
    <message>
        <location filename="../devs/vi/config/events/logic/types/ViLogicElementConfigMeasCond.cpp" line="17"/>
        <source>Measurement Condition</source>
        <translation>Условие для измерения</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViLogicElementConfigNot</name>
    <message>
        <location filename="../devs/vi/config/events/logic/types/ViLogicElementConfigNot.cpp" line="9"/>
        <source>NOT</source>
        <translation>НЕ</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViLogicElementConfigOr</name>
    <message>
        <location filename="../devs/vi/config/events/logic/types/ViLogicElementConfigOr.cpp" line="9"/>
        <source>OR</source>
        <translation>ИЛИ</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViMeasCondTypes</name>
    <message>
        <location filename="../devs/vi/config/acq/meas/ViMeasCondType.cpp" line="8"/>
        <source>Alert</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/ViMeasCondType.cpp" line="17"/>
        <source>Danger</source>
        <translation>Останов</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/ViMeasCondType.cpp" line="26"/>
        <source>Failure</source>
        <translation>Неисправность</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/ViMeasCondType.cpp" line="35"/>
        <source>Change Threshold</source>
        <translation>Порог отклонения</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViMeasConfig</name>
    <message>
        <location filename="../devs/vi/config/acq/meas/types/ViMeasTypeVoltageDC.cpp" line="21"/>
        <source>DC Voltage</source>
        <translation>Постоянное напр.</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/types/ViMeasTypeVibroVelocity.cpp" line="18"/>
        <source>Velocity</source>
        <translation>Скорость</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/types/ViMeasTypeVibroAccelHf.cpp" line="19"/>
        <source>Acceleration (HF)</source>
        <translation>Ускорение (ВЧ)</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/types/ViMeasTypeVibroAccel.cpp" line="19"/>
        <source>Acceleration</source>
        <translation>Ускорение</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/types/ViMeasTypeRotSpeed.cpp" line="18"/>
        <source>Rotational speed </source>
        <translation>Скорость вращения</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/types/ViMeasTypeVoltageGap.cpp" line="20"/>
        <source>Gap</source>
        <translation>Зазор</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/types/ViMeasTypeVibroDisp.cpp" line="21"/>
        <source>Displacement</source>
        <translation>Перемещение</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/types/ViMeasTypeUnknown.cpp" line="18"/>
        <source>Unknown</source>
        <translation>Неизвестное измерение</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaDisp.cpp" line="21"/>
        <source>nX Phase of Displacement</source>
        <translation>nX Фаза перемещения</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaVelocity.cpp" line="21"/>
        <source>nX Phase of Velocity</source>
        <translation>nX Фаза скорости</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaAccel.cpp" line="21"/>
        <source>nX Phase of Acceleration</source>
        <translation>nX Фаза ускорения</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpDisp.cpp" line="22"/>
        <source>nX Amplitude of Displacement</source>
        <translation>nX Амплитуда перемещения</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpVelocity.cpp" line="22"/>
        <source>nX Amplitude of Velocity</source>
        <translation>nX Амплитуда скорости</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpAccel.cpp" line="21"/>
        <source>nX Amplitude of Acceleration</source>
        <translation>nX Амплитуда ускорения</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/types/ViMeasTypeVibroSmax.cpp" line="17"/>
        <source>Max displacement from average (Smax)</source>
        <translation>Макс. смещение от среднего (Smax)</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/types/ViMeasTypePosition.cpp" line="17"/>
        <source>Position</source>
        <translation>Позиция</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViMeasParameterTypes</name>
    <message>
        <location filename="../devs/vi/config/acq/meas/ViMeasParameterType.cpp" line="8"/>
        <source>RMS</source>
        <translation>СКЗ</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/ViMeasParameterType.cpp" line="17"/>
        <source>Peak-to-peak</source>
        <translation>Размах</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/meas/ViMeasParameterType.cpp" line="26"/>
        <source>Peak</source>
        <translation>Пик</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViModule</name>
    <message>
        <location filename="../devs/vi/modules/ViModule.cpp" line="39"/>
        <source>IP: %1</source>
        <translation>IP: %1</translation>
    </message>
    <message>
        <location filename="../devs/vi/modules/ViModule.cpp" line="40"/>
        <source>Net name: %1</source>
        <translation>Сет. имя: %1</translation>
    </message>
    <message>
        <location filename="../devs/vi/devref/ViDeviceRefNetSvc.cpp" line="98"/>
        <source>Net Name</source>
        <translation>Сет. имя</translation>
    </message>
    <message>
        <location filename="../devs/vi/devref/ViDeviceRefIpAddr.cpp" line="118"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViModuleFaults</name>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="11"/>
        <source>Unknown device fault</source>
        <translation>Неизвестная неисправность</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="19"/>
        <source>Unknown device warning</source>
        <translation>Неизвестное предупреждение</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="27"/>
        <source>Clock system fault</source>
        <translation>Ошибка инициализации системы клоков</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="35"/>
        <source>SDRAM fault</source>
        <translation>Ошибка динамической памяти</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="43"/>
        <source>Eth PHY access</source>
        <translation>Ошибка доступа к Ethernet PHY</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="51"/>
        <source>Eth PHY loopback</source>
        <translation>Ошибка кольцевого теста Ethernet PHY</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="59"/>
        <source>Mezzanine flash not detected</source>
        <translation>Не обнаружена flash-память мезонина</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="67"/>
        <source>Carrier board flash not detected</source>
        <translation>Не обнаружена flash-память несущей платы</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="75"/>
        <source>Ethrentet switch IC not detected</source>
        <translation>Не обнаружена микросхема Ethernet коммутатора</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="83"/>
        <source>Ethrentet switch IC interface</source>
        <translation>Ошибка интерфейса микросхемы Ethernet коммутатора</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="91"/>
        <source>Gigabit PHY access</source>
        <translation>Ошибка доступа к Gigabit PHY</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="99"/>
        <source>Gigabit PHY loopback</source>
        <translation>Ошибка кольцевого теста Gigabit PHY</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="107"/>
        <source>Relay current out of range</source>
        <translation>Ток реле вне допустимого диапазона</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="115"/>
        <source>Over-temperature</source>
        <translation>Перегрев</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="123"/>
        <source>Diagnostic ADC LDO not ready</source>
        <translation>Ошибка готовности питания диагностического АЦП</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="131"/>
        <source>Diagnostic ADC calibration not ready</source>
        <translation>Ошибка готовности калибровки диагностического АЦП</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="139"/>
        <source>Supply voltage out of range</source>
        <translation>Напряжение питания вне допустимого диапазона</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="147"/>
        <source>Acquisition channel fault</source>
        <translation>Неисправность в измерительном канале</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="155"/>
        <source>No carrier board info</source>
        <translation>Не обнаружена информация о несущей плате</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="163"/>
        <source>Unknown device type</source>
        <translation>Неизвестный тип устройства</translation>
    </message>
    <message>
        <location filename="../devs/vi/data/state/diagnostic/ViModuleFaults.cpp" line="171"/>
        <source>No mezzanine board info</source>
        <translation>Не обнаружена информация о мезонине плате</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViModuleMbErrors</name>
    <message>
        <location filename="../devs/vi/connection/mb/ViModuleMbErrors.cpp" line="6"/>
        <source>Modbus register operation error</source>
        <translation>Ошибка выполнения операции с регистрами modbus</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViModuleOpcUaErrors</name>
    <message>
        <location filename="../devs/vi/connection/opcua/ViModuleOpcUaErrors.cpp" line="6"/>
        <source>Acquisition channel paramaeters read request failed</source>
        <translation>Запрос на получения параметров канала сбора данных завершен с ошибкой</translation>
    </message>
    <message>
        <location filename="../devs/vi/connection/opcua/ViModuleOpcUaErrors.cpp" line="10"/>
        <source>Acquisition channels raw data subscription create request failed</source>
        <translation>Запрос на создание подписки на данные канала сбора данных завершен с ошибкой</translation>
    </message>
    <message>
        <location filename="../devs/vi/connection/opcua/ViModuleOpcUaErrors.cpp" line="14"/>
        <source>Acquisition channels monitored items create request failed</source>
        <translation>Запрос на создание отслеживаемого элемента для канала сбора данных завершен с ошибкой</translation>
    </message>
    <message>
        <location filename="../devs/vi/connection/opcua/ViModuleOpcUaErrors.cpp" line="18"/>
        <source>Acquisition channels data receive error</source>
        <translation>Ошибка приема данных канала</translation>
    </message>
    <message>
        <location filename="../devs/vi/connection/opcua/ViModuleOpcUaErrors.cpp" line="22"/>
        <source>%1. Status 0x%2: %3</source>
        <translation>%1. Статус 0x%02: %3</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViSetpointConditionTypes</name>
    <message>
        <location filename="../devs/vi/config/acq/setpoint/ViSetpointConditionType.cpp" line="8"/>
        <source>Disabled</source>
        <translation>Запрещено</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/setpoint/ViSetpointConditionType.cpp" line="19"/>
        <source>Above</source>
        <translation>Выше</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/setpoint/ViSetpointConditionType.cpp" line="30"/>
        <source>Below</source>
        <translation>Ниже</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/setpoint/ViSetpointConditionType.cpp" line="41"/>
        <source>Out of Band</source>
        <translation>Вне интервала</translation>
    </message>
    <message>
        <location filename="../devs/vi/config/acq/setpoint/ViSetpointConditionType.cpp" line="52"/>
        <source>In Band</source>
        <translation>В интервале</translation>
    </message>
</context>
<context>
    <name>LQMeas::ViWaveformSrcConfig</name>
    <message>
        <location filename="../devs/vi/config/wf/ViWaveformSrcConfig.cpp" line="48"/>
        <source>Waveform source</source>
        <translation>Источник выборки</translation>
    </message>
</context>
<context>
    <name>LQMeas::lcompErrors</name>
    <message>
        <location filename="../devs/lcomp/lcompErrors.h" line="11"/>
        <source>Create instance error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devs/lcomp/lcompErrors.h" line="14"/>
        <source>Cannot open device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devs/lcomp/lcompErrors.h" line="17"/>
        <source>Cannot load device firmware</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devs/lcomp/lcompErrors.h" line="20"/>
        <source>Board test error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devs/lcomp/lcompErrors.h" line="23"/>
        <source>Cannot read board description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devs/lcomp/lcompErrors.h" line="26"/>
        <source>Cannot close device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devs/lcomp/lcompErrors.h" line="29"/>
        <source>Cannot fill ADC parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devs/lcomp/lcompErrors.h" line="32"/>
        <source>Request ADC buffer error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devs/lcomp/lcompErrors.h" line="35"/>
        <source>Cannot setup ADC parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devs/lcomp/lcompErrors.h" line="38"/>
        <source>Cannot initialize ADC start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devs/lcomp/lcompErrors.h" line="41"/>
        <source>Cannot start ADC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devs/lcomp/lcompErrors.h" line="44"/>
        <source>Cannot stop ADC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devs/lcomp/lcompErrors.h" line="47"/>
        <source>Receive buffer overflow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LQMeas::x502</name>
    <message>
        <source>Process data error</source>
        <translation type="vanished">Ошибка обработки данных</translation>
    </message>
    <message>
        <location filename="../devs/x502/x502.cpp" line="216"/>
        <source>Setup FPGA mode</source>
        <translation>Установка режима работы через ПЛИС</translation>
    </message>
    <message>
        <location filename="../devs/x502/x502.cpp" line="211"/>
        <source>Load blackfin firmware</source>
        <translation>Загрузка прошивки Blackfin</translation>
    </message>
    <message>
        <location filename="../devs/x502/x502.cpp" line="229"/>
        <source>Setup channel %1 parameters</source>
        <translation>Установка параметров канала %1</translation>
    </message>
    <message>
        <location filename="../devs/x502/x502.cpp" line="237"/>
        <source>Setup channels count</source>
        <translation>Установка количества каналов</translation>
    </message>
    <message>
        <location filename="../devs/x502/x502.cpp" line="244"/>
        <source>Setup sync mode</source>
        <translation>Установка режима генерации частоты синхронизации</translation>
    </message>
    <message>
        <location filename="../devs/x502/x502.cpp" line="251"/>
        <source>Setup sync start mode</source>
        <translation>Установка режима запуска частоты синхронизации</translation>
    </message>
    <message>
        <location filename="../devs/x502/x502.cpp" line="258"/>
        <source>Setup reference frequency</source>
        <translation>Установка опорной частоты</translation>
    </message>
    <message>
        <location filename="../devs/x502/x502.cpp" line="272"/>
        <source>Setup external reference frequency value</source>
        <translation>Установка значения внешней опорной частоты</translation>
    </message>
    <message>
        <location filename="../devs/x502/x502.cpp" line="281"/>
        <source>Setup adc frequency</source>
        <translation>Установка частоты АЦП</translation>
    </message>
    <message>
        <location filename="../devs/x502/x502.cpp" line="289"/>
        <source>Setup din frequency</source>
        <translation>Установка частоты дискретного ввода</translation>
    </message>
    <message>
        <location filename="../devs/x502/x502.cpp" line="340"/>
        <source>Setup pullup resistors</source>
        <translation>Установка конфигурации подтягивающих резисторов</translation>
    </message>
    <message>
        <location filename="../devs/x502/x502.cpp" line="349"/>
        <source>Setup output frequency</source>
        <translation>Установка частоты вывода</translation>
    </message>
    <message>
        <location filename="../devs/x502/x502.cpp" line="664"/>
        <source>Prepare data error</source>
        <translation>Ошибка подготовки данных</translation>
    </message>
    <message>
        <source>IP</source>
        <translation type="obsolete">IP</translation>
    </message>
</context>
<context>
    <name>LQMeas::x502NetworkBrowser</name>
    <message>
        <location filename="../devs/x502/net/x502NetworkBrowser.cpp" line="18"/>
        <source>Cannot start network browse for E502/E16 modules</source>
        <translation>Не удалось запустить поиск модулей E502/E16 в сети</translation>
    </message>
    <message>
        <location filename="../devs/x502/net/x502NetworkBrowser.cpp" line="27"/>
        <source>Error occured during wait for new browse event for E502/E16 modules</source>
        <translation>Во время ожидания событий при поиске модулей E502/E16 в сети произошла ошибка</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../devs/lcomp/lcompResolver.cpp" line="40"/>
        <source>Cannot load lcomp dynamic library</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViAcqChannelConfig</name>
    <message>
        <source>Unknown</source>
        <translation type="vanished">Неизвестный</translation>
    </message>
</context>
<context>
    <name>ViAcqSensorConTypes</name>
    <message>
        <source>ICP</source>
        <translation type="vanished">ICP</translation>
    </message>
</context>
<context>
    <name>ViAcqSensorConfig</name>
    <message>
        <source>Proximeter</source>
        <translation type="vanished">Проксиметр</translation>
    </message>
    <message>
        <source>Accelerometer</source>
        <translation type="vanished">Акселерометр</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">Неизвестный тип</translation>
    </message>
</context>
<context>
    <name>ViDoutChannelTypes</name>
    <message>
        <source>Low Power Relay</source>
        <translation type="vanished">Низкотоковое реле</translation>
    </message>
</context>
<context>
    <name>ViDoutNormalOpStates</name>
    <message>
        <source>Normally De-Energized</source>
        <translation type="vanished">Обесточено без тревоги</translation>
    </message>
    <message>
        <source>Normally Energized</source>
        <translation type="vanished">Запитано без тревоги</translation>
    </message>
</context>
<context>
    <name>ViMeasParameterTypes</name>
    <message>
        <source>RMS</source>
        <translation type="vanished">СКЗ</translation>
    </message>
</context>
<context>
    <name>ViModule</name>
    <message>
        <source>IP</source>
        <translation type="vanished">IP</translation>
    </message>
    <message>
        <source>Net Name</source>
        <translation type="vanished">Сет. имя</translation>
    </message>
</context>
<context>
    <name>ViSetpointConditionTypes</name>
    <message>
        <source>Disabled</source>
        <translation type="vanished">Запрещено</translation>
    </message>
</context>
<context>
    <name>x502</name>
    <message>
        <location filename="../devs/x502/net/x502DeviceRefNetSvc.cpp" line="97"/>
        <source>Net Name</source>
        <translation>Сет. имя</translation>
    </message>
    <message>
        <location filename="../devs/x502/net/x502DeviceRefIPAddr.cpp" line="116"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
</context>
</TS>
