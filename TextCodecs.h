#ifndef LQMEAS_TEXTCODECS_H
#define LQMEAS_TEXTCODECS_H

class QTextCodec;

namespace LQMeas {
    class TextCodecs {
    public:
        static QTextCodec &cp1251();
    };
}

#endif // LQMEAS_TEXTCODECS_H
