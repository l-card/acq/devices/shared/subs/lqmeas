#include "Unit.h"

LQMeas::Unit::~Unit() {

}

bool LQMeas::Unit::operator==(const LQMeas::Unit &other) const {
    return &other == this;
}

bool LQMeas::Unit::operator!=(const LQMeas::Unit &other) const {
    return &other != this;
}

