#ifndef LQMEAS_UNITGROUP_H
#define LQMEAS_UNITGROUP_H



#include <QString>
#include <QList>
#include <QObject>
#include "lqmeas/EnumNamedValue.h"


namespace LQMeas {
    class Unit;

    class UnitGroup : public EnumNamedValue<QString> {
    public:
        virtual ~UnitGroup();
        virtual QList<const Unit *> units() const = 0;
    };
}

Q_DECLARE_METATYPE(const LQMeas::UnitGroup *);

#endif // UNITSGROUP_H
