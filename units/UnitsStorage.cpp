#include "UnitsStorage.h"
#include "Unit.h"
#include "std/UnitGroups.h"
#include "std/Special.h"
#include "lqmeas/StdErrors.h"
#include <QJsonObject>


static const QLatin1String cfgkey_unit_id("UnitID");


void LQMeas::UnitsStorage::save(QJsonObject &cfgObj, const LQMeas::Unit &unit) const {
    cfgObj[cfgkey_unit_id] = unit.id();
}

const LQMeas::Unit &LQMeas::UnitsStorage::load(const QJsonObject &cfgObj, const LQMeas::Unit &defaultUnit) const {
    const LQMeas::Unit *ret;
    const QString id {cfgObj[cfgkey_unit_id].toString()};
    if (id.isEmpty()) {
        ret = &defaultUnit;
    } else {
        LQError err;
        ret = &get(id, err);
    }
    return *ret;
}

const LQMeas::Unit &LQMeas::UnitsStorage::get(const QString &id, LQError &err) const {
    const LQMeas::Unit *ret {&Units::Special::unknown()};
    const QList<const LQMeas::Unit *> &units {allUnits()};
    const auto it {std::find_if(units.constBegin(), units.constEnd(),
                                [&id](auto unit){return unit->id() == id;})};
    if (it == units.constEnd()) {
        err = StdErrors::UnknownUnit(id);
    } else {
        ret = *it;
    }
    return *ret;
}

LQMeas::UnitsStorage &LQMeas::UnitsStorage::instance() {
    static UnitsStorage storage;
    return storage;
}

QList<const LQMeas::Unit *> LQMeas::UnitsStorage::analogUnits() const {
    return Units::Groups::analogUnits();
}

QList<const LQMeas::Unit *> LQMeas::UnitsStorage::allUnits() const {
    return Units::Groups::allUnits();
}

LQMeas::UnitsStorage::UnitsStorage(QObject *parent) : QObject(parent) {

}
