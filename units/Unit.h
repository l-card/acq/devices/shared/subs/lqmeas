#ifndef MEASUNIT_H
#define MEASUNIT_H

#include <QString>
#include <QObject>
#include "lqmeas/EnumNamedValue.h"

namespace LQMeas {
    /* Класс для обозначания едениц измерения.
       Сопоставлет некий код и переводимое название еденицы измерения отображения.
       Также содержит коэффициент перевода относительно базовых единиц измерения,
       чтобы можно было перводить значения в разных единицах измерения, но заданных
       относительно одной базовой */
    class Unit : public EnumNamedValue<QString> {
    public:
        virtual ~Unit();

        bool operator==(const Unit &other) const;
        bool operator!=(const Unit &other) const;


        QString displayName() const override {return displayFullName();}

        /* обозначение единицы измерения */
        virtual QString symbol() const = 0;
        /* полноое название при выборе типа единиц */
        virtual QString displayFullName() const = 0;
        /* полное название единиц в множественном числе (если применимо) */
        virtual QString fullPluralName() const {return displayFullName();}
        /* обозначение единицы без использования юникодных символов */
        virtual QString asciiSymbol() const {return symbol();}



        /* Указывает соотношение между данной величиной и базовой, какую
           долю от базовой величины составляет данная. */
        virtual double multipler() const = 0;
        /* указывает значение базовой величины, соответствующее нулю данной */
        virtual double zeroPoint() const {return 0;}
        /* базовая единица измерения.
         * Для перевода величины из данной в базовую нужно:
           Xbase = multipler() * Xunit + zeroPoint() */
        virtual const Unit &baseUnit() const = 0;


        bool operator< (const Unit& rhs) const {
            return (baseUnit() == rhs.baseUnit()) &&  (multipler() < rhs.multipler());
        }
        bool operator> (const Unit& rhs) const {
            return (baseUnit() == rhs.baseUnit()) &&  (multipler() > rhs.multipler());
        }
        bool operator<=(const Unit& rhs) const {
            return (baseUnit() == rhs.baseUnit()) &&  (multipler() <= rhs.multipler());
        }
        bool operator>=(const Unit& rhs) const {
            return (baseUnit() == rhs.baseUnit()) &&  (multipler() >= rhs.multipler());
        }
    };
}

Q_DECLARE_METATYPE(const LQMeas::Unit *);

#endif // MEASUNIT_H

