#ifndef UNITCONVERTER_H
#define UNITCONVERTER_H

namespace LQMeas {
    class Unit;

    class UnitConverter {
    public:
        static bool canConvert(const Unit &src_unit, const Unit &dst_unit);
        static double convert(double val, const Unit &src_unit, const Unit &dst_unit);

        UnitConverter(const Unit &src_unit, const Unit &dst_unit);
        UnitConverter(const UnitConverter &conv1, const UnitConverter &conv2);

        double convert(double val) const;
        double revert(double val) const;
    private:
        double m_mult;
        double m_addit;
    };
}

#endif // UnitConverter_H
