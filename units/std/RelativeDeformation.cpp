#include "RelativeDeformation.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString RelativeDeformation::id() const {return QStringLiteral("unitgroup_reldeform");}
        QString RelativeDeformation::displayName() const {return Groups::tr("Relative Deformation");}

        QList<const Unit *> RelativeDeformation::units() const {
            static const QList<const Unit *> list {
                &m_m(),
                &mm_m(),
                &um_m(),
            };
            return list;
        }

        const RelativeDeformation &RelativeDeformation::group() {
            static const RelativeDeformation gr;
            return gr;
        }

        class RelDeformationUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return RelativeDeformation::m_m();}
        };

        const Unit &RelativeDeformation::m_m() {
            static const  class RelDeformationUnit_m_m : public RelDeformationUnit {
            public:
                QString id() const override {return QStringLiteral("reldeform_m_m");}
                QString symbol() const override {return Groups::tr("m/m");}
                QString displayFullName() const override {return Groups::tr("metre per metre");}
                QString fullPluralName() const override {return Groups::tr("metres per metre");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &RelativeDeformation::mm_m() {
            static const  class RelDeformationUnit_mm_m : public RelDeformationUnit {
            public:
                QString id() const override {return QStringLiteral("reldeform_mm_m");}
                QString symbol() const override {return Groups::tr("mm/m");}
                QString displayFullName() const override {return Groups::tr("millimetre per metre");}
                QString fullPluralName() const override {return Groups::tr("millimetres per metre");}
                double multipler() const override {return 1e-3;}
            } u;
            return u;
        }

        const Unit &RelativeDeformation::um_m() {
            static const  class RelDeformationUnit_um_m : public RelDeformationUnit {
            public:
                QString id() const override {return QStringLiteral("reldeform_um_m");}
                QString symbol() const override {return Groups::tr("µm/m");}
                QString asciiSymbol() const override {return Groups::tr("um/m");}
                QString displayFullName() const override {return Groups::tr("micrometre per metre");}
                QString fullPluralName() const override {return Groups::tr("micrometres per metre");}
                double multipler() const override {return 1e-6;}
            } u;
            return u;
        }
    }
}


