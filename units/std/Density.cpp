#include "Density.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString Density::id() const {return QStringLiteral("unitgroup_density");}
        QString Density::displayName() const {return Groups::tr("Density");}

        QList<const Unit *> Density::units() const {
            static const QList<const Unit *> list {
                &g_m3(),
                &kg_m3(),
                &g_cm3(),
                &mg_cm3(),
                &g_l()
            };
            return list;
        }

        const Density &Density::group() {
            static const Density gr;
            return gr;
        }

        class DensityUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Density::g_m3();}
        };

        const Unit &Density::g_m3() {
            static const  class Density_g_m3 : public DensityUnit {
            public:
                QString id() const override {return QStringLiteral("density_g_m3");}
                QString symbol() const override {return Groups::tr("g/m³");}
                QString asciiSymbol() const override {return Groups::tr("g/m^3");}
                QString displayFullName() const override {return Groups::tr("gram per cubic metre");}
                QString fullPluralName() const override {return Groups::tr("grams per cubic metre");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &Density::kg_m3() {
            static const  class Density_kg_m3 : public DensityUnit {
            public:
                QString id() const override {return QStringLiteral("density_kg_m3");}
                QString symbol() const override {return Groups::tr("kg/m³");}
                QString asciiSymbol() const override {return Groups::tr("kg/m^3");}
                QString displayFullName() const override {return Groups::tr("kilogram per cubic metre");}
                QString fullPluralName() const override {return Groups::tr("kilograms per cubic metre");}
                double multipler() const override {return 1e3;}
            } u;
            return u;
        }

        const Unit &Density::g_cm3() {
            static const  class Density_g_cm3 : public DensityUnit {
            public:
                QString id() const override {return QStringLiteral("density_g_cm3");}
                QString symbol() const override {return Groups::tr("g/cm³");}
                QString asciiSymbol() const override {return Groups::tr("g/cm^3");}
                QString displayFullName() const override {return Groups::tr("gram per cubic centimetre");}
                QString fullPluralName() const override {return Groups::tr("grams per cubic centimetre");}
                double multipler() const override {return 1e6;}
            } u;
            return u;
        }

        const Unit &Density::mg_cm3() {
            static const  class Density_mg_cm3 : public DensityUnit {
            public:
                QString id() const override {return QStringLiteral("density_mg_cm3");}
                QString symbol() const override {return Groups::tr("mg/cm³");}
                QString asciiSymbol() const override {return Groups::tr("mg/cm^3");}
                QString displayFullName() const override {return Groups::tr("milligram per cubic centimetre");}
                QString fullPluralName() const override {return Groups::tr("milligrams per cubic centimetre");}
                double multipler() const override {return 1e3;}
            } u;
            return u;
        }

        const Unit &Density::g_l() {
            static const  class Density_g_l : public DensityUnit {
            public:
                QString id() const override {return QStringLiteral("density_g_l");}
                QString symbol() const override {return Groups::tr("g/l");}
                QString displayFullName() const override {return Groups::tr("gram per litre");}
                QString fullPluralName() const override {return Groups::tr("grams per litre");}
                double multipler() const override {return 1e3;}
            } u;
            return u;
        }
    }
}
