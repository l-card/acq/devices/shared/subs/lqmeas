#ifndef LQMEAS_UNITS_RESISTANCE_H
#define LQMEAS_UNITS_RESISTANCE_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Resistance : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Resistance &group();

            static const Unit &ohm(); /* омы */
            static const Unit &kohm(); /* килоом */
            static const Unit &Mohm(); /* мегаом */
            static const Unit &Gohm(); /* гигаом */
            static const Unit &mohm(); /* милиом */
            static const Unit &uohm(); /* микроом */
        };
    }
}

#endif // LQMEAS_UNITS_RESISTANCE_H
