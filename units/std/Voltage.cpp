#include "Voltage.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"
namespace LQMeas {
    namespace Units {
        QString Voltage::id() const  {return QStringLiteral("unitgroup_voltage");}
        QString Voltage::displayName() const {return Groups::tr("Voltage");}

        QList<const Unit *> Voltage::units() const {
             static const QList<const Unit *> list {
                 &V(),
                 &mV(),
                 &uV(),
                 &kV()
             };
             return list;
        }

        const Voltage &Voltage::group() {
            static const Voltage gr;
            return gr;
        }


        class VoltageUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Voltage::V();}
        };

        const Unit &Voltage::V() {
            static const class VoltageUnit_V : public VoltageUnit {
            public:
                QString id() const override {return QStringLiteral("voltage_V");}
                QString symbol() const override {return Groups::tr("V");}
                QString displayFullName() const override {return Groups::tr("volt");}
                QString fullPluralName() const override {return Groups::tr("volts");}
                double multipler() const override {return 1.;}
            } u;
            return u;
        }

        const Unit &Voltage::mV() {
            static const class VoltageUnit_mV : public VoltageUnit {
            public:
                QString id() const override {return QStringLiteral("voltage_mV");}
                QString symbol() const override {return Groups::tr("mV");}
                QString displayFullName() const override {return Groups::tr("millivolt");}
                QString fullPluralName() const override {return Groups::tr("millivolts");}
                double multipler() const override {return 1e-3;}
            } u;
            return u;
        }

        const Unit &Voltage::uV() {
            static const class VoltageUnit_uV : public VoltageUnit {
            public:
                QString id() const override {return QStringLiteral("voltage_uV");}
                QString symbol() const override {return Groups::tr("µV");}
                QString asciiSymbol() const override {return Groups::tr("uV");}
                QString displayFullName() const override {return Groups::tr("microvolt");}
                QString fullPluralName() const override {return Groups::tr("microvolts");}
                double multipler() const override {return 1e-6;}
            } u;
            return u;
        }

        const Unit &Voltage::kV()   {
            static const class VoltageUnit_uV : public VoltageUnit {
            public:
                QString id() const override {return QStringLiteral("voltage_kV");}
                QString symbol() const override {return Groups::tr("kV");}
                QString displayFullName() const override {return Groups::tr("kilovolt");}
                QString fullPluralName() const override {return Groups::tr("kilovolts");}
                double multipler() const override {return 1e3;}
            } u;
            return u;
        }
    }
}
