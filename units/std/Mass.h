#ifndef LQMEAS_UNITS_MASS_H
#define LQMEAS_UNITS_MASS_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Mass : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Mass &group();


            static const Unit &g();  /* грамм */
            static const Unit &mg();  /* милиграмм */
            static const Unit &ug();  /* микрограмм */
            static const Unit &kg();  /* килограмм */
            static const Unit &t();  /* тонна */
        };
    }
}

#endif // LQMEAS_UNITS_MASS_H
