#include "Acceleration.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString Acceleration::id() const {return QStringLiteral("unitgroup_acceleration");}
        QString Acceleration::displayName() const {return Groups::tr("Acceleration");}

        QList<const Unit *> Acceleration::units() const {
            static const QList<const Unit *> list {
                &m_s2(),
                &mm_s2(),
                &g()
            };
            return list;
        }

        const Acceleration &Acceleration::group() {
            static const Acceleration gr;
            return gr;
        }

        class AccelerationUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Acceleration::m_s2();}
        };

        const Unit &Acceleration::m_s2() {
            static const class AccelerationUnit_m_s2 : public AccelerationUnit {
            public:
                QString id() const override {return QStringLiteral("accel_m_s2");}
                QString symbol() const override {return Groups::tr("m/s²");}
                QString asciiSymbol() const override {return Groups::tr("m/s^2");}
                QString displayFullName() const override {return Groups::tr("metre per second squared");}
                QString fullPluralName() const override {return Groups::tr("metres per second squared");}
                double multipler() const override {return 1.;}
            } u;
            return u;
        }

        const Unit &Acceleration::mm_s2() {
            static const class AccelerationUnit_mm_s2 : public AccelerationUnit {
            public:
                QString id() const override {return QStringLiteral("accel_mm_s2");}
                QString symbol() const override {return Groups::tr("mm/s²");}
                QString asciiSymbol() const override {return Groups::tr("mm/s^2");}
                QString displayFullName() const override {return Groups::tr("millimetre per second squared");}
                QString fullPluralName() const override {return Groups::tr("millimetres per second squared");}
                double multipler() const override {return 1e-3;}
            } u;
            return u;
        }

        const Unit &Acceleration::g() {
            static const class AccelerationUnit_g : public AccelerationUnit {
            public:
                QString id() const override {return QStringLiteral("accel_g");}
                QString symbol() const override {return Groups::tr("g");}
                QString displayFullName() const override {return Groups::tr("gravity");}
                double multipler() const override {return 9.80665;}
            } u;
            return u;
        }
    }
}
