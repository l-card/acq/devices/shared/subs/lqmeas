#ifndef LQMEAS_UNITS_ROTATIONALSPEED_H
#define LQMEAS_UNITS_ROTATIONALSPEED_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class RotationalSpeed : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const RotationalSpeed &group();

            static const Unit &rpm(); /* обороты в минуту */
            static const Unit &rps(); /* обороты в секунду */
            static const Unit &rad_s(); /* радиан в секунду */
        };
    }
}


#endif // LQMEAS_UNITS_ROTATIONALSPEED_H
