#include "Displacement.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString Displacement::id() const {return QStringLiteral("unitgroup_displacement");}
        QString Displacement::displayName() const {return Groups::tr("Displacement");}

        QList<const Unit *> Displacement::units() const {
            static const QList<const Unit *> list {
                &m(),
                &cm(),
                &mm(),
                &um(),
                &nm(),
                &km(),
                &inch()
            };
            return list;
        }

        const Displacement &Displacement::group() {
            static const Displacement gr;
            return gr;
        }


        class DisplacementUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Displacement::m();}
        };

        const Unit &Displacement::m() {
            static const  class DisplacementUnit_m : public DisplacementUnit {
            public:
                QString id() const override {return QStringLiteral("disp_m");}
                QString symbol() const override {return Groups::tr("m", "metre");}
                QString displayFullName() const override {return Groups::tr("metre");}
                QString fullPluralName() const override {return Groups::tr("metres");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &Displacement::cm() {
            static const  class DisplacementUnit_cm : public DisplacementUnit {
            public:
                QString id() const override {return QStringLiteral("disp_cm");}
                QString symbol() const override {return Groups::tr("cm");}
                QString displayFullName() const override {return Groups::tr("centimetre");}
                QString fullPluralName() const override {return Groups::tr("centimetres");}
                double multipler() const override {return 1e-2;}
            } u;
            return u;
        }

        const Unit &Displacement::mm() {
            static const  class DisplacementUnit_mm : public DisplacementUnit {
            public:
                QString id() const override {return QStringLiteral("disp_mm");}
                QString symbol() const override {return Groups::tr("mm");}
                QString displayFullName() const override {return Groups::tr("millimetre");}
                QString fullPluralName() const override {return Groups::tr("millimetres");}
                double multipler() const override {return 1e-3;}
            } u;
            return u;
        }



        const Unit &Displacement::um() {
            static const  class DisplacementUnit_um : public DisplacementUnit {
            public:
                QString id() const override {return QStringLiteral("disp_um");}
                QString symbol() const override {return Groups::tr("µm");}
                QString asciiSymbol() const override {return Groups::tr("um");}
                QString displayFullName() const override {return Groups::tr("micrometre");}
                QString fullPluralName() const override {return Groups::tr("micrometres");}
                double multipler() const override {return 1e-6;}
            } u;
            return u;
        }

        const Unit &Displacement::nm() {
            static const  class DisplacementUnit_nm : public DisplacementUnit {
            public:
                QString id() const override {return QStringLiteral("disp_nm");}
                QString symbol() const override {return Groups::tr("nm");}
                QString displayFullName() const override {return Groups::tr("nanometre");}
                QString fullPluralName() const override {return Groups::tr("nanometres");}
                double multipler() const override {return 1e-9;}
            } u;
            return u;
        }

        const Unit &Displacement::km() {
            static const  class DisplacementUnit_km : public DisplacementUnit {
            public:
                QString id() const override {return QStringLiteral("disp_km");}
                QString symbol() const override {return Groups::tr("km");}
                QString displayFullName() const override {return Groups::tr("kilometre");}
                QString fullPluralName() const override {return Groups::tr("kilometres");}
                double multipler() const override {return 1e3;}
            } u;
            return u;
        }

        const Unit &Displacement::inch() {
            static const  class DisplacementUnit_inch : public DisplacementUnit {
            public:
                QString id() const override {return QStringLiteral("disp_inch");}
                QString symbol() const override {return Groups::tr("″");}
                QString displayFullName() const override {return Groups::tr("inch");}
                QString fullPluralName() const override {return Groups::tr("inches");}
                double multipler() const override {return inch_to_m_mult;}
            } u;
            return u;
        }
    }
}
