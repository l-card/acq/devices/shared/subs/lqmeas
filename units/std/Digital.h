#ifndef LQMEAS_UNITS_DIGITAL_H
#define LQMEAS_UNITS_DIGITAL_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Digital : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Digital &group();

            static const Unit &codes(); /* коды (АЦП/ЦАП и т.п) */
        };
    }
}



#endif // DIGITAL_H
