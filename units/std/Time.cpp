#include "Time.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"

namespace LQMeas {
    namespace Units {
        QString Time::id() const {return QStringLiteral("unitgroup_time");}
        QString Time::displayName() const {return Groups::tr("Time");}

        QList<const Unit *> Time::units() const {
            static const QList<const Unit *> list {
                &s(),
                &ms(),
                &us(),
                &ns(),
                &minute(),
                &hour(),
                &day(),
                &week(),
                &month(),
                &year(),
            };
            return list;
        }

        const Time &Time::group() {
            static const Time gr;
            return gr;
        }

        class TimeUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Time::s();}
        };

        const Unit &Time::s() {
            static const class TimeUnit_s : public TimeUnit {
            public:
                QString id() const override {return QStringLiteral("time_sec");}
                QString symbol() const override {return Groups::tr("s");}
                QString displayFullName() const override {return Groups::tr("second");}
                QString fullPluralName() const override {return Groups::tr("seconds");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }


        const Unit &Time::ms() {
            static const class TimeUnit_ms : public TimeUnit {
            public:
                QString id() const override {return QStringLiteral("time_ms");}
                QString symbol() const override {return Groups::tr("ms");}
                QString displayFullName() const override {return Groups::tr("millisecond");}
                QString fullPluralName() const override {return Groups::tr("milliseconds");}
                double multipler() const override {return 1e-3;}
            } u;
            return u;
        }

        const Unit &Time::us() {
            static const class TimeUnit_us : public TimeUnit {
            public:
                QString id() const override {return QStringLiteral("time_us");}
                QString symbol() const override {return Groups::tr("µs");}
                QString asciiSymbol() const override {return Groups::tr("us");}
                QString displayFullName() const override {return Groups::tr("microsecond");}
                QString fullPluralName() const override {return Groups::tr("microseconds");}
                double multipler() const override {return 1e-6;}
            } u;
            return u;
        }

        const Unit &Time::ns() {
            static const class TimeUnit_ns : public TimeUnit {
            public:
                QString id() const override {return QStringLiteral("time_ns");}
                QString symbol() const override {return Groups::tr("ns");}
                QString displayFullName() const override {return Groups::tr("nanosecond");}
                QString fullPluralName() const override {return Groups::tr("nanoseconds");}
                double multipler() const override {return 1e-9;}
            } u;
            return u;
        }

        const Unit &Time::minute() {
            static const class TimeUnit_min : public TimeUnit {
            public:
                QString id() const override {return QStringLiteral("time_min");}
                QString symbol() const override {return Groups::tr("min");}
                QString displayFullName() const override {return Groups::tr("minute");}
                QString fullPluralName() const override {return Groups::tr("minutes");}
                double multipler() const override {return 60;}
            } u;
            return u;
        }

        const Unit &Time::hour() {
            static const class TimeUnit_hour : public TimeUnit {
            public:
                QString id() const override {return QStringLiteral("time_h");}
                QString symbol() const override {return Groups::tr("h");}
                QString displayFullName() const override {return Groups::tr("hour");}
                QString fullPluralName() const override {return Groups::tr("hours");}
                double multipler() const override {return 60*60;}
            } u;
            return u;
        }

        const Unit &Time::day()     {
            static const class TimeUnit_day : public TimeUnit {
            public:
                QString id() const override {return QStringLiteral("time_day");}
                QString symbol() const override {return Groups::tr("d");}
                QString displayFullName() const override {return Groups::tr("day");}
                QString fullPluralName() const override {return Groups::tr("days");}
                double multipler() const override {return 24*60*60;}
            } u;
            return u;
        }

        const Unit &Time::week() {
            static const class TimeUnit_week : public TimeUnit {
            public:
                QString id() const override {return QStringLiteral("time_week");}
                QString symbol() const override {return Groups::tr("w");}
                QString displayFullName() const override {return Groups::tr("week");}
                QString fullPluralName() const override {return Groups::tr("weeks");}
                double multipler() const override {return 7*day().multipler();}
            } u;
            return u;
        }

        const Unit &Time::month() {
            static const class TimeUnit_month : public TimeUnit {
            public:
                QString id() const override {return QStringLiteral("time_month");}
                QString symbol() const override {return Groups::tr("M", "month");}
                QString displayFullName() const override {return Groups::tr("month");}
                QString fullPluralName() const override {return Groups::tr("months");}
                double multipler() const override {return 30.4375 * day().multipler();}
            } u;
            return u;
        }

        const Unit &Time::year() {
            static const class TimeUnit_year : public TimeUnit {
            public:
                QString id() const override {return QStringLiteral("time_year");}
                QString symbol() const override {return Groups::tr("Y");}
                QString displayFullName() const override {return Groups::tr("year");}
                QString fullPluralName() const override {return Groups::tr("years");}
                double multipler() const override {return 365 * day().multipler();}
            } u;
            return u;
        }
    }
}
