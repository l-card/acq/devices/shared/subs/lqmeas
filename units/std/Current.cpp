#include "Current.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString Current::id() const {return QStringLiteral("unitgroup_current");}
        QString Current::displayName() const {return Groups::tr("Current");}

        QList<const Unit *> Current::units() const {
            static const QList<const Unit *> list {
                &A(),
                &mA(),
                &uA(),
                &kA()
            };
            return list;
        }

        const Current &Current::group() {
            static const Current gr;
            return gr;
        }


        class CurrentUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Current::A();}
        };

        const Unit &Current::A() {
            static const class CurrentUnit_A : public CurrentUnit {
            public:
                QString id() const override {return QStringLiteral("cur_A");}
                QString symbol() const override {return Groups::tr("A");}
                QString displayFullName() const override {return Groups::tr("ampere");}
                QString fullPluralName() const override {return Groups::tr("amperes");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &Current::mA() {
            static const class CurrentUnit_mA : public CurrentUnit {
            public:
                QString id() const override {return QStringLiteral("cur_mA");}
                QString symbol() const override {return Groups::tr("mA");}
                QString displayFullName() const override {return Groups::tr("milliampere");}
                QString fullPluralName() const override {return Groups::tr("milliamperes");}
                double multipler() const override {return 1e-3;}
            } u;
            return u;
        }

        const Unit &Current::uA() {
            static const class CurrentUnit_uA : public CurrentUnit {
            public:
                QString id() const override {return QStringLiteral("cur_uA");}
                QString symbol() const override {return Groups::tr("µA");}
                QString asciiSymbol() const override {return Groups::tr("uA");}
                QString displayFullName() const override {return Groups::tr("microampere");}
                QString fullPluralName() const override {return Groups::tr("microamperes");}
                double multipler() const override {return 1e-6;}
            } u;
            return u;
        }

        const Unit &Current::kA() {
            static const class CurrentUnit_kA : public CurrentUnit {
            public:
                QString id() const override {return QStringLiteral("cur_kA");}
                QString symbol() const override {return Groups::tr("kA");}
                QString displayFullName() const override {return Groups::tr("kiloampere");}
                QString fullPluralName() const override {return Groups::tr("kiloamperes");}
                double multipler() const override {return 1e3;}
            } u;
            return u;
        }

    }
}
