#include "PressureImpulse.h"
#include "Time.h"
#include "Pressure.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"



namespace LQMeas {
    namespace Units {
        QString PressureImpulse::id() const {return QStringLiteral("unitgroup_pressure_impulse");}
        QString PressureImpulse::displayName() const {return Groups::tr("Pressure impulse");}


        QList<const Unit *> PressureImpulse::units() const {
            static const QList<const Unit *> list {
                    &Pa_s(),
                    &Pa_ms(),
                    &Pa_minute(),
                    &bar_s(),
                    &bar_ms(),
                    &bar_minute(),
                    &at_s(),
                    &at_ms(),
                    &at_minute(),
                    &ata_s(),
                    &ata_ms(),
                    &ata_minute(),
                    &atg_s(),
                    &atg_ms(),
                    &atg_minute(),
                    &atm_s(),
                    &atm_ms(),
                    &atm_minute(),
                    &mmHg_s(),
                    &mmHg_ms(),
                    &mmHg_minute(),
                    &kgf_cm2_s(),
                    &kgf_cm2_ms(),
                    &kgf_cm2_minute()
            };
            return list;
        }

        const PressureImpulse &PressureImpulse::group() {
            static const PressureImpulse ug;
            return ug;
        }



        class PressureImpulseUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return PressureImpulse::Pa_s();}
        };

        const Unit &PressureImpulse::Pa_s()  {
            static const class PressureImpulse_Pa_s : public PressureImpulseUnit {
            public:
                QString id() const override {return QStringLiteral("press_impulse_pa_s");}
                QString symbol() const override {return Groups::tr("Pa·s");}
                QString displayFullName() const override {return Groups::tr("pascal-second");}
                QString fullPluralName() const override {return Groups::tr("pascal-seconds");}
                double multipler() const override {return Pressure::Pa().multipler() * Time::s().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::Pa_ms() {
            static const class PressureImpulse_Pa_ms : public PressureImpulseUnit {
            public:
                QString id() const override {return QStringLiteral("press_impulse_pa_ms");}
                QString symbol() const override {return Groups::tr("Pa·ms");}
                QString displayFullName() const override {return Groups::tr("pascal-millisecond");}
                QString fullPluralName() const override {return Groups::tr("pascal-milliseconds");}
                double multipler() const override {return Pressure::Pa().multipler() * Time::ms().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::Pa_minute()  {
            static const class PressureImpulse_Pa_min : public PressureImpulseUnit {
                QString id() const override {return QStringLiteral("press_impulse_pa_min");}
                QString symbol() const override {return Groups::tr("Pa·min");}
                QString displayFullName() const override {return Groups::tr("pascal-minute");}
                QString fullPluralName() const override {return Groups::tr("pascal-minutes");}
                double multipler() const override {return Pressure::Pa().multipler() * Time::minute().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::bar_s()  {
            static const class PressureImpulse_bar_s : public PressureImpulseUnit {
            public:
                QString id() const override {return QStringLiteral("press_impulse_bar_s");}
                QString symbol() const override {return Groups::tr("bar·s");}
                QString displayFullName() const override {return Groups::tr("bar-second");}
                QString fullPluralName() const override {return Groups::tr("bar-seconds");}
                double multipler() const override {return Pressure::bar().multipler() * Time::s().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::bar_ms() {
            static const class PressureImpulse_bar_ms : public PressureImpulseUnit {
            public:
                QString id() const override {return QStringLiteral("press_impulse_bar_ms");}
                QString symbol() const override {return Groups::tr("bar·ms");}
                QString displayFullName() const override {return Groups::tr("bar-millisecond");}
                QString fullPluralName() const override {return Groups::tr("bar-milliseconds");}
                double multipler() const override {return Pressure::bar().multipler() * Time::ms().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::bar_minute()  {
            static const class PressureImpulse_bar_min : public PressureImpulseUnit {
                QString id() const override {return QStringLiteral("press_impulse_bar_min");}
                QString symbol() const override {return Groups::tr("bar·min");}
                QString displayFullName() const override {return Groups::tr("bar-minute");}
                QString fullPluralName() const override {return Groups::tr("bar-minutes");}
                double multipler() const override {return Pressure::bar().multipler() * Time::minute().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::at_s()  {
            static const class PressureImpulse_at_s : public PressureImpulseUnit {
            public:
                QString id() const override {return QStringLiteral("press_impulse_at_s");}
                QString symbol() const override {return Groups::tr("at·s");}
                QString displayFullName() const override {return Groups::tr("technical atmosphere-second");}
                QString fullPluralName() const override {return Groups::tr("technical atmosphere-seconds");}
                double multipler() const override {return Pressure::at().multipler() * Time::s().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::at_ms()  {
            static const class PressureImpulse_at_ms : public PressureImpulseUnit {
            public:
                QString id() const override {return QStringLiteral("press_impulse_at_ms");}
                QString symbol() const override {return Groups::tr("at·ms");}
                QString displayFullName() const override {return Groups::tr("technical atmosphere-millisecond");}
                QString fullPluralName() const override {return Groups::tr("technical atmosphere-milliseconds");}
                double multipler() const override {return Pressure::at().multipler() * Time::ms().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::at_minute()  {
            static const class PressureImpulse_at_min : public PressureImpulseUnit {
                QString id() const override {return QStringLiteral("press_impulse_at_min");}
                QString symbol() const override {return Groups::tr("at·min");}
                QString displayFullName() const override {return Groups::tr("technical atmosphere-minute");}
                QString fullPluralName() const override {return Groups::tr("technical atmosphere-minutes");}
                double multipler() const override {return Pressure::at().multipler() * Time::minute().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::ata_s() {
            static const class PressureImpulse_ata_s : public PressureImpulseUnit {
            public:
                QString id() const override {return QStringLiteral("press_impulse_ata_s");}
                QString symbol() const override {return Groups::tr("ata·s");}
                QString displayFullName() const override {return Groups::tr("technical atmosphere-second (absolute)");}
                QString fullPluralName() const override {return Groups::tr("technical atmosphere-seconds (absolute)");}
                double multipler() const override {return Pressure::ata().multipler() * Time::s().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::ata_ms() {
            static const class PressureImpulse_ata_ms : public PressureImpulseUnit {
            public:
                QString id() const override {return QStringLiteral("press_impulse_ata_ms");}
                QString symbol() const override {return Groups::tr("ata·ms");}
                QString displayFullName() const override {return Groups::tr("technical atmosphere-millisecond (absolute)");}
                QString fullPluralName() const override {return Groups::tr("technical atmosphere-milliseconds (absolute)");}
                double multipler() const override {return Pressure::ata().multipler() * Time::ms().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::ata_minute() {
            static const class PressureImpulse_ata_min : public PressureImpulseUnit {
                QString id() const override {return QStringLiteral("press_impulse_ata_min");}
                QString symbol() const override {return Groups::tr("ata·min");}
                QString displayFullName() const override {return Groups::tr("technical atmosphere-minute (absolute)");}
                QString fullPluralName() const override {return Groups::tr("technical atmosphere-minutes (absolute)");}
                double multipler() const override {return Pressure::ata().multipler() * Time::minute().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::atg_s() {
            static const class PressureImpulse_atg_s : public PressureImpulseUnit {
            public:
                QString id() const override {return QStringLiteral("press_impulse_atg_s");}
                QString symbol() const override {return Groups::tr("atg·s");}
                QString displayFullName() const override {return Groups::tr("technical atmosphere-second (gauge)");}
                QString fullPluralName() const override {return Groups::tr("technical atmosphere-seconds (gauge)");}
                double multipler() const override {return Pressure::atg().multipler() * Time::s().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::atg_ms() {
            static const class PressureImpulse_atg_ms : public PressureImpulseUnit {
            public:
                QString id() const override {return QStringLiteral("press_impulse_atg_ms");}
                QString symbol() const override {return Groups::tr("atg·ms");}
                QString displayFullName() const override {return Groups::tr("technical atmosphere-millisecond (gauge)");}
                QString fullPluralName() const override {return Groups::tr("technical atmosphere-milliseconds (gauge)");}
                double multipler() const override {return Pressure::atg().multipler() * Time::ms().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::atg_minute() {
            static const class PressureImpulse_atg_min : public PressureImpulseUnit {
                QString id() const override {return QStringLiteral("press_impulse_atg_min");}
                QString symbol() const override {return Groups::tr("atg·min");}
                QString displayFullName() const override {return Groups::tr("technical atmosphere-minute (gauge)");}
                QString fullPluralName() const override {return Groups::tr("technical atmosphere-minutes (gauge)");}
                double multipler() const override {return Pressure::atg().multipler() * Time::minute().multipler();}
            } u;
            return u;
        }


        const Unit &PressureImpulse::atm_s()  {
            static const class PressureImpulse_atm_s : public PressureImpulseUnit {
            public:
                QString id() const override {return QStringLiteral("press_impulse_atm_s");}
                QString symbol() const override {return Groups::tr("atm·s");}
                QString displayFullName() const override {return Groups::tr("standard atmosphere-second");}
                QString fullPluralName() const override {return Groups::tr("standard atmosphere-seconds");}
                double multipler() const override {return Pressure::atm().multipler() * Time::s().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::atm_ms()  {
            static const class PressureImpulse_atm_ms : public PressureImpulseUnit {
            public:
                QString id() const override {return QStringLiteral("press_impulse_atm_ms");}
                QString symbol() const override {return Groups::tr("atm·ms");}
                QString displayFullName() const override {return Groups::tr("standard atmosphere-millisecond");}
                QString fullPluralName() const override {return Groups::tr("standard atmosphere-milliseconds");}
                double multipler() const override {return Pressure::atm().multipler() * Time::ms().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::atm_minute()  {
            static const class PressureImpulse_atm_min : public PressureImpulseUnit {
                QString id() const override {return QStringLiteral("press_impulse_atm_min");}
                QString symbol() const override {return Groups::tr("atm·min");}
                QString displayFullName() const override {return Groups::tr("standard atmosphere-minute");}
                QString fullPluralName() const override {return Groups::tr("standard atmosphere-minutes");}
                double multipler() const override {return Pressure::atm().multipler() * Time::minute().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::mmHg_s()  {
            static const class PressureImpulse_mmHg_s : public PressureImpulseUnit {
            public:
                QString id() const override {return QStringLiteral("press_impulse_mmHg_s");}
                QString symbol() const override {return Groups::tr("mm Hg·s");}
                QString displayFullName() const override {return Groups::tr("millimetre of mercury-second");}
                QString fullPluralName() const override {return Groups::tr("millimetre of mercury-seconds");}
                double multipler() const override {return Pressure::mmHg().multipler() * Time::s().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::mmHg_ms()  {
            static const class PressureImpulse_mmHg_ms : public PressureImpulseUnit {
            public:
                QString id() const override {return QStringLiteral("press_impulse_mmHg_ms");}
                QString symbol() const override {return Groups::tr("mm Hg·ms");}
                QString displayFullName() const override {return Groups::tr("millimetre of mercury-millisecond");}
                QString fullPluralName() const override {return Groups::tr("millimetre of mercury-milliseconds");}
                double multipler() const override {return Pressure::mmHg().multipler() * Time::ms().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::mmHg_minute()  {
            static const class PressureImpulse_mmHg_min : public PressureImpulseUnit {
                QString id() const override {return QStringLiteral("press_impulse_mmHg_min");}
                QString symbol() const override {return Groups::tr("mm Hg·min");}
                QString displayFullName() const override {return Groups::tr("millimetre of mercury-minute");}
                QString fullPluralName() const override {return Groups::tr("millimetre of mercury-minutes");}
                double multipler() const override {return Pressure::mmHg().multipler() * Time::minute().multipler();}
            } u;
            return u;
        }


        const Unit &PressureImpulse::kgf_cm2_s()  {
            static const class PressureImpulse_kgf_cm2_s : public PressureImpulseUnit {
            public:
                QString id() const override {return QStringLiteral("press_impulse_kgf_cm2_s");}
                QString symbol() const override {return Groups::tr("kgf/cm²·s");}
                QString asciiSymbol() const override {return Groups::tr("kgf/cm^2·s");}
                QString displayFullName() const override {return Groups::tr("kilogram-force per square centimeter-second");}
                QString fullPluralName() const override {return Groups::tr("kilogram-force per square centimeter-seconds");}
                double multipler() const override {return Pressure::kgf_cm2().multipler() * Time::s().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::kgf_cm2_ms()  {
            static const class PressureImpulse_kgf_cm2_ms : public PressureImpulseUnit {
            public:
                QString id() const override {return QStringLiteral("press_impulse_kgf_cm2_ms");}
                QString symbol() const override {return Groups::tr("kgf/cm²·ms");}
                QString asciiSymbol() const override {return Groups::tr("kgf/cm^2·ms");}
                QString displayFullName() const override {return Groups::tr("kilogram-force per square centimeter-millisecond");}
                QString fullPluralName() const override {return Groups::tr("kilogram-force per square centimeter-milliseconds");}
                double multipler() const override {return Pressure::kgf_cm2().multipler() * Time::ms().multipler();}
            } u;
            return u;
        }

        const Unit &PressureImpulse::kgf_cm2_minute() {
            static const class PressureImpulse_kgf_cm2_min : public PressureImpulseUnit {
            public:
            public:
                QString id() const override {return QStringLiteral("press_impulse_kgf_cm2_min");}
                QString symbol() const override {return Groups::tr("kgf/cm²·min");}
                QString asciiSymbol() const override {return Groups::tr("kgf/cm^2·min");}
                QString displayFullName() const override {return Groups::tr("kilogram-force per square centimeter-minute");}
                QString fullPluralName() const override {return Groups::tr("kilogram-force per square centimeter-minutes");}
                double multipler() const override {return Pressure::kgf_cm2().multipler() * Time::minute().multipler();}
            } u;
            return u;
        }
    }
}

