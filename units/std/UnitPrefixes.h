#ifndef UNITPREFIXES_H
#define UNITPREFIXES_H

#include "../Unit.h"
#include "UnitGroups.h"
#include <QStringBuilder>

namespace LQMeas {
    class UnitPrefix : public Unit {
    public:
        QString symbol() const override {return prefixSymbol() % prefixSrcUnit().symbol();}
        QString asciiSymbol() const override {return prefixAsciiSymbol() % prefixSrcUnit().asciiSymbol();}
        QString displayFullName() const override {return prefixName() % prefixSrcUnit().displayFullName();}
        QString fullPluralName() const override {return  prefixName() % prefixSrcUnit().fullPluralName();}
        double multipler() const override {return prefixMultiplier() * prefixSrcUnit().multipler();}
        double zeroPoint() const override {return prefixSrcUnit().zeroPoint();}
        const Unit &baseUnit() const override {return prefixSrcUnit().baseUnit();}

        virtual const Unit &prefixSrcUnit() const = 0;
        virtual QString prefixSymbol() const = 0;
        virtual QString prefixAsciiSymbol() const {return prefixSymbol();}
        virtual QString prefixName() const = 0;
        virtual double prefixMultiplier() const = 0;

    };


    namespace UnitPrefixes {
        class kilo : public UnitPrefix {
        public:            
            QString prefixSymbol() const override {return Units::Groups::tr("k");}
            QString prefixName() const override {return Units::Groups::tr("kilo");}
            double prefixMultiplier() const override {return 1e3;}
        };

        class Mega : public UnitPrefix {
        public:
            QString prefixSymbol() const override {return Units::Groups::tr("M", "mega");}
            QString prefixName() const override {return Units::Groups::tr("mega");}
            double prefixMultiplier() const override {return 1e6;}
        };

        class Giga : public UnitPrefix {
        public:
            QString prefixSymbol() const override {return Units::Groups::tr("G");}
            QString prefixName() const override {return Units::Groups::tr("giga");}
            double prefixMultiplier() const override {return 1e9;}
        };
    }
}
#endif // UNITPREFIXES_H
