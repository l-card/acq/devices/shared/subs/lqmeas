#ifndef LQMEAS_UNITS_AREA_H
#define LQMEAS_UNITS_AREA_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Area : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Area &group();

            static const Unit &m2();  /* метры² */
            static const Unit &cm2(); /* сантиметры² */
            static const Unit &mm2(); /* милиметры² */
            static const Unit &inch2(); /* дюйм² */
        };
    }
}

#endif // LQMEAS_UNITS_AREA_H
