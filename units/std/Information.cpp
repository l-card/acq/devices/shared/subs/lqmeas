#include "Information.h"
#include "UnitGroups.h"
#include "UnitPrefixes.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString Information::id() const {return QStringLiteral("unitgroup_information");}
        QString Information::displayName() const {return Groups::tr("Information");}

        QList<const Unit *> Information::units() const {
            static const QList<const Unit *> list {
                &bit(),
                &kbit(),
                &Mbit(),
                &Gbit(),
                &byte(),
                &kbyte(),
                &Mbyte(),
                &Gbyte(),
            };
            return list;
        }

        const Information &Information::group() {
            static const Information gr;
            return gr;
        }


        class InformationUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Information::bit();}
        };

        const Unit &Information::bit() {
            static const class InformationUnit_bit : public InformationUnit {
            public:
                QString id() const override {return QStringLiteral("inform_bit");}
                QString symbol() const override {return Groups::tr("bit");}
                QString displayFullName() const override {return Groups::tr("bit");}
                QString fullPluralName() const override {return Groups::tr("bits");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &Information::kbit() {
            static const class InformationUnit_kbit : public UnitPrefixes::kilo {
            public:
                QString id() const override {return QStringLiteral("inform_kbit");}
                const Unit &prefixSrcUnit() const override {return bit();}
            } u;
            return u;
        }

        const Unit &Information::Mbit() {
            static const class InformationUnit_Mbit : public UnitPrefixes::Mega {
            public:
                QString id() const override {return QStringLiteral("inform_Mbit");}
                const Unit &prefixSrcUnit() const override {return bit();}
            } u;
            return u;
        }

        const Unit &Information::Gbit() {
            static const class InformationUnit_Gbit : public UnitPrefixes::Giga {
            public:
                QString id() const override {return QStringLiteral("inform_Gbit");}
                const Unit &prefixSrcUnit() const override {return bit();}
            } u;
            return u;
        }

        const Unit &Information::byte() {
            static const class InformationUnit_byte : public InformationUnit {
            public:
                QString id() const override {return QStringLiteral("inform_byte");}
                QString symbol() const override {return Groups::tr("B");}
                QString displayFullName() const override {return Groups::tr("byte");}
                QString fullPluralName() const override {return Groups::tr("bytes");}
                double multipler() const override {return 8;}
            } u;
            return u;
        }

        const Unit &Information::kbyte() {
            static const class InformationUnit_kbyte : public UnitPrefixes::kilo {
            public:
                QString id() const override {return QStringLiteral("inform_kbyte");}
                const Unit &prefixSrcUnit() const override {return byte();}
            } u;
            return u;
        }

        const Unit &Information::Mbyte() {
            static const class InformationUnit_Mbyte : public UnitPrefixes::Mega {
            public:
                QString id() const override {return QStringLiteral("inform_Mbyte");}
                const Unit &prefixSrcUnit() const override {return byte();}
            } u;
            return u;
        }

        const Unit &Information::Gbyte() {
            static const class InformationUnit_Gbyte : public UnitPrefixes::Giga {
            public:
                QString id() const override {return QStringLiteral("inform_Gbyte");}
                const Unit &prefixSrcUnit() const override {return byte();}
            } u;
            return u;
        }



    }
}
