#ifndef LQMEAS_UNITS_POWER_H
#define LQMEAS_UNITS_POWER_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Power : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Power &group();


            static const Unit &W();  /* ватт */
            static const Unit &kW(); /* киловатт */
            static const Unit &MW(); /* магаватт */
            static const Unit &kgf_m_s(); /* килограмм-сила-метр в секунду */
            static const Unit &eg_s(); /* эрг в секунду */
            static const Unit &cal_s(); /* калорий в секунду */
            static const Unit &kcal_s(); /* килокалорий в секунду */

            /* В*А, Вт и вар эквивалентны, но используются для разных мощьностей: полной, активной, реактивной */
            static const Unit &va(); /* вольт-ампер */
            static const Unit &kva(); /* киловольт-ампер */
            static const Unit &Mva(); /* мегавольт-ампер */

            static const Unit &var(); /* вольт-ампер реактивный */
            static const Unit &kvar(); /* киловольт-ампер реактивный */
            static const Unit &Mvar(); /* мегавольт-ампер реактивный */
        };
    }
}


#endif // LQMEAS_UNITS_POWER_H
