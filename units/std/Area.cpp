#include "Area.h"
#include "Displacement.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString Area::id() const {return QStringLiteral("unitgroup_area");}
        QString Area::displayName() const {return Groups::tr("Area");}

        QList<const Unit *> Area::units() const {
            static const QList<const Unit *> list {
                &m2(),
                &cm2(),
                &mm2(),
                &inch2()
            };
            return list;
        }

        const Area &Area::group() {
            static const Area gr;
            return gr;
        }

        class AreaUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Area::m2();}
        };

        const Unit &Area::m2() {
            static const  class AreaUnit_m2 : public AreaUnit {
            public:
                QString id() const override {return QStringLiteral("area_m2");}
                QString symbol() const override {return Groups::tr("m²");}
                QString asciiSymbol() const override {return Groups::tr("m^2");}
                QString displayFullName() const override {return Groups::tr("square metre");}
                QString fullPluralName() const override {return Groups::tr("square metres");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &Area::cm2() {
            static const  class AreaUnit_cm2 : public AreaUnit {
            public:
                QString id() const override {return QStringLiteral("area_cm2");}
                QString symbol() const override {return Groups::tr("cm²");}
                QString asciiSymbol() const override {return Groups::tr("cm^2");}
                QString displayFullName() const override {return Groups::tr("square centimetre");}
                QString fullPluralName() const override {return Groups::tr("square centimetres");}
                double multipler() const override {return 1e-4;}
            } u;
            return u;
        }

        const Unit &Area::mm2() {
            static const  class AreaUnit_mm2 : public AreaUnit {
            public:
                QString id() const override {return QStringLiteral("area_mm2");}
                QString symbol() const override {return Groups::tr("mm²");}
                QString asciiSymbol() const override {return Groups::tr("mm^2");}
                QString displayFullName() const override {return Groups::tr("square millimetre");}
                QString fullPluralName() const override {return Groups::tr("square millimetres");}
                double multipler() const override {return 1e-6;}
            } u;
            return u;
        }

        const Unit &Area::inch2() {
            static const  class AreaUnit_inch2 : public AreaUnit {
            public:
                QString id() const override {return QStringLiteral("area_inch2");}
                QString symbol() const override {return Groups::tr("″²");}
                QString asciiSymbol() const override {return Groups::tr("″^2");}
                QString displayFullName() const override {return Groups::tr("square inch");}
                QString fullPluralName() const override {return Groups::tr("square inches");}
                double multipler() const override {return Displacement::inch_to_m_mult * Displacement::inch_to_m_mult;}
            } u;
            return u;
        }
    }
}
