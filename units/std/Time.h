#ifndef LQMEAS_UNITS_TIME_H
#define LQMEAS_UNITS_TIME_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Time : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Time &group();

            static const Unit &s();  /* секунды */
            static const Unit &ms(); /* милисекунды */
            static const Unit &us(); /* микросекунды */
            static const Unit &ns(); /* наносекунды */
            static const Unit &minute(); /* минуты */
            static const Unit &hour(); /* часы */
            static const Unit &day(); /* дни */
            static const Unit &week(); /* дни */
            static const Unit &month(); /* дни */
            static const Unit &year(); /* дни */
        };
    }
}



#endif // LQMEAS_UNITS_TIME_H
