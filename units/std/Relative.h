#ifndef LQMEAS_UNITS_RELATIVE_H
#define LQMEAS_UNITS_RELATIVE_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Relative : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Relative &group();

            static const Unit &ratio(); /* безразмерное отношение */
            static const Unit &percentage(); /* проценты */
            static const Unit &db(); /* децибелы */
        };
    }
}


#endif // LQMEAS_UNITS_RELATIVE_H
