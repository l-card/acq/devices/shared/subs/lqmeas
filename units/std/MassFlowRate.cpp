#include "MassFlowRate.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString MassFlowRate::id() const { return QStringLiteral("unitgroup_massflowrate"); }
        QString MassFlowRate::displayName() const { return Groups::tr("Mass Flow Rate");}

        QList<const Unit *> MassFlowRate::units() const {
            static const QList<const Unit *> list {
                &g_s(),
                &mg_s(),
                &kg_s(),
                &kg_minute(),
                &kg_h(),
                &t_s(),
                &t_minute(),
                &t_h()
            };
            return list;
        }

        const MassFlowRate &MassFlowRate::group() {
            static const MassFlowRate gr;
            return gr;
        }

        class MassFlowRateUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return MassFlowRate::g_s();}
        };

        const Unit &MassFlowRate::g_s() {
            static const class MassFlowRateUnit_g_s : public MassFlowRateUnit {
            public:
                QString id() const override {return QStringLiteral("massflowrate_g_s");}
                QString symbol() const override {return Groups::tr("g/s");}
                QString displayFullName() const override {return Groups::tr("gram per second");}
                QString fullPluralName() const override {return Groups::tr("grams per second");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }


        const Unit &MassFlowRate::mg_s() {
            static const class MassFlowRateUnit_mg_s : public MassFlowRateUnit {
            public:
                QString id() const override {return QStringLiteral("massflowrate_mg_s");}
                QString symbol() const override {return Groups::tr("mg/s");}
                QString displayFullName() const override {return Groups::tr("milligram per second");}
                QString fullPluralName() const override {return Groups::tr("milligrams per second");}
                double multipler() const override {return 1e-3;}
            } u;
            return u;
        }

        const Unit &MassFlowRate::kg_s() {
            static const class MassFlowRateUnit_kg_s : public MassFlowRateUnit {
            public:
                QString id() const override {return QStringLiteral("massflowrate_kg_s");}
                QString symbol() const override {return Groups::tr("kg/s");}
                QString displayFullName() const override {return Groups::tr("kilogram per second");}
                QString fullPluralName() const override {return Groups::tr("kilograms per second");}
                double multipler() const override {return 1e3;}
            } u;
            return u;
        }

        const Unit &MassFlowRate::kg_minute() {
            static const class MassFlowRateUnit_kg_minute : public MassFlowRateUnit {
            public:
                QString id() const override {return QStringLiteral("massflowrate_kg_min");}
                QString symbol() const override {return Groups::tr("kg/min");}
                QString displayFullName() const override {return Groups::tr("kilogram per minute");}
                QString fullPluralName() const override {return Groups::tr("kilograms per minute");}
                double multipler() const override {return 1e3/60;}
            } u;
            return u;
        }

        const Unit &MassFlowRate::kg_h() {
            static const class MassFlowRateUnit_kg_h : public MassFlowRateUnit {
            public:
                QString id() const override {return QStringLiteral("massflowrate_kg_h");}
                QString symbol() const override {return Groups::tr("kg/h");}
                QString displayFullName() const override {return Groups::tr("kilogram per hour");}
                QString fullPluralName() const override {return Groups::tr("kilograms per hour");}
                double multipler() const override {return 1e3/3600;}
            } u;
            return u;
        }

        const Unit &MassFlowRate::t_s() {
            static const class MassFlowRateUnit_t_s : public MassFlowRateUnit {
            public:
                QString id() const override {return QStringLiteral("massflowrate_t_s");}
                QString symbol() const override {return Groups::tr("t/s");}
                QString displayFullName() const override {return Groups::tr("tonne per second");}
                QString fullPluralName() const override {return Groups::tr("tonns per second");}
                double multipler() const override {return 1e6;}
            } u;
            return u;
        }

        const Unit &MassFlowRate::t_minute() {
            static const class MassFlowRateUnit_t_minute : public MassFlowRateUnit {
            public:
                QString id() const override {return QStringLiteral("massflowrate_t_min");}
                QString symbol() const override {return Groups::tr("t/min");}
                QString displayFullName() const override {return Groups::tr("tonne per minute");}
                QString fullPluralName() const override {return Groups::tr("tonns per minute");}
                double multipler() const override {return 1e6/60;}
            } u;
            return u;
        }

        const Unit &MassFlowRate::t_h() {
            static const class MassFlowRateUnit_t_h : public MassFlowRateUnit {
            public:
                QString id() const override {return QStringLiteral("massflowrate_t_h");}
                QString symbol() const override {return Groups::tr("t/h");}
                QString displayFullName() const override {return Groups::tr("tonne per hour");}
                QString fullPluralName() const override {return Groups::tr("tonns per hour");}
                double multipler() const override {return 1e6/3600;}
            } u;
            return u;
        }
    }
}
