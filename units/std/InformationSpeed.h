#ifndef LQMEAS_UNITS_INFORMATIONSPEED_H
#define LQMEAS_UNITS_INFORMATIONSPEED_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class InformationSpeed : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const InformationSpeed &group();

            static const Unit &bit_s();   /* биты/с */
            static const Unit &kbit_s();  /* килобиты/с */
            static const Unit &Mbit_s();  /* мегабиты/с */
            static const Unit &Gbit_s();  /* гигабиты/с */

            static const Unit &byte_s();  /* байты/с */
            static const Unit &kbyte_s(); /* килобайты/с */
            static const Unit &Mbyte_s(); /* мегабайты/с */
            static const Unit &Gbyte_s(); /* гигабайты/с */

            static const Unit &word_s();  /* слова/с */
            static const Unit &kword_s(); /* килослова/с */
            static const Unit &Mword_s(); /* мегаслова/с */
            static const Unit &Gword_s(); /* гигаслова/с */


            static const Unit &byte_h();  /* байты/ч */
            static const Unit &kbyte_h(); /* килобайты/ч */
            static const Unit &Mbyte_h(); /* мегабайты/ч */
            static const Unit &Gbyte_h(); /* гигабайты/ч */
        };
    }
}


#endif // INFORMATIONSPEED_H
