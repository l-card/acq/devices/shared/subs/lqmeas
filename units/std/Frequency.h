#ifndef LQMEAS_UNITS_FREQUENCY_H
#define LQMEAS_UNITS_FREQUENCY_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Frequency : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Frequency &group();

            static const Unit &Hz();  /* герцы */
            static const Unit &kHz(); /* килогерцы */
            static const Unit &MHz(); /* мгагерц */
        };
    }
}

#endif // LQMEAS_UNITS_FREQUENCY_H
