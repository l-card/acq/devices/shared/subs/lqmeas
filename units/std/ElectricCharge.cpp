#include "ElectricCharge.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"



namespace LQMeas {
    namespace Units {
        QString ElectricCharge::id() const {return QStringLiteral("unitgroup_electric_charge");}
        QString ElectricCharge::displayName() const {return Groups::tr("Electric Charge");}


        QList<const Unit *> ElectricCharge::units() const {
            static const QList<const Unit *> list {
                &C(),
                &pC()
            };
            return list;
        }

        const ElectricCharge &ElectricCharge::group() {
            static const ElectricCharge ug;
            return ug;
        }



        class ElectricChargeUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return ElectricCharge::C();}
        };

        const Unit &ElectricCharge::C()  {
            static const class ElectricChargeUnit_C : public ElectricChargeUnit {
            public:
                QString id() const override {return QStringLiteral("electric_charge_C");}
                QString symbol() const override {return Groups::tr("C");}
                QString displayFullName() const override {return Groups::tr("coulomb");}
                QString fullPluralName() const override {return Groups::tr("coulombs");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &ElectricCharge::pC() {
            static const class ElectricChargeUnit_pC : public ElectricChargeUnit {
            public:
                QString id() const override {return QStringLiteral("electric_charge_pC");}
                QString symbol() const override {return Groups::tr("pC");}
                QString displayFullName() const override {return Groups::tr("picocoulomb");}
                QString fullPluralName() const override {return Groups::tr("picocoulombs");}
                double multipler() const override {return 1e-12;}
            } u;
            return u;
        }
    }
}
