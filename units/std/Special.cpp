#include "Special.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"

namespace LQMeas {
    namespace Units {
        QString Special::id() const {return QStringLiteral("unitgroup_special");}
        QString Special::displayName() const {return Groups::tr("Special");}

        QList<const Unit *> Special::units() const {
            static const QList<const Unit *> list {
                &unitless()
            };
            return list;
        }

        const Special &Special::group() {
            static const Special gr;
            return gr;
        }

        const Unit &Special::unknown() {
            static const class UnitUnknown : public Unit {
            public:
                QString id() const override {return QStringLiteral("unknown");}
                QString symbol() const override {return QString{};}
                QString displayFullName() const override {return Groups::tr("Unknown");}
                double multipler() const override {return 1.;}
                const Unit &baseUnit() const override {return Special::unknown();}
            } u;
            return u;
        }

        const Unit &Special::unitless() {
            static const class UnitUnitless : public Unit {
            public:
                QString id() const override {return QStringLiteral("unitless");}
                QString symbol() const override {return QString{};}
                QString displayFullName() const override {return Groups::tr("Unitless");}
                double multipler() const override {return 1.;}
                const Unit &baseUnit() const override {return Special::unitless();}
            } u;
            return u;
        }
    }
}
