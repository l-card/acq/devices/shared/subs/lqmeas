#include "Discrete.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"

namespace LQMeas {
    namespace Units {
        QString Discrete::id() const {return QStringLiteral("unitgroup_discrete");}
        QString Discrete::displayName() const {return Groups::tr("Discrete");}

        QList<const Unit *> Discrete::units() const {
            static const QList<const Unit *> list {
                &logicalLevel()
            };
            return list;
        }

        const Discrete &Discrete::group() {
            static const Discrete gr;
            return gr;
        }


        const Unit &Discrete::logicalLevel() {
            static const class Unit_LogicalLevel : public Unit {
            public:
                QString id() const override {return QStringLiteral("discrete_log_lvl");}
                QString symbol() const override {return Groups::tr("");}
                QString displayFullName() const override {return Groups::tr("logical level");}
                QString fullPluralName() const override {return Groups::tr("logical level");}
                double multipler() const override {return 1.;}
                const Unit &baseUnit() const override {return Discrete::logicalLevel();}
            } u;
            return u;
        }
    }
}

