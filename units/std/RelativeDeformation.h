#ifndef LQMEAS_UNITS_RELATIVEDEFORMATION_H
#define LQMEAS_UNITS_RELATIVEDEFORMATION_H


#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class RelativeDeformation : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const RelativeDeformation &group();

            static const Unit &m_m();  /* м/м */
            static const Unit &mm_m(); /* мм/м */
            static const Unit &um_m(); /* мкм/м */
        };
    }
}


#endif // RELATIVEDEFORMATION_H
