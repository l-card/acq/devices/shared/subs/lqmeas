#ifndef LQMEAS_UNITS_FORCE_H
#define LQMEAS_UNITS_FORCE_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Force : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Force &group();

            static const Unit &N();  /* ньютон */
            static const Unit &kN();  /* килоньютон */
            static const Unit &kgf();  /* кгс */
            static const Unit &gf();  /* гс */
            static const Unit &tf();  /* тс */

            static constexpr double kgf_to_newton = 9.80665;
        };
    }
}

#endif // LQMEAS_UNITS_FORCE_H
