#include "Pressure.h"
#include "Force.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString Pressure::id() const {return QStringLiteral("unitgroup_pressure");}
        QString Pressure::displayName() const {return Groups::tr("Pressure");}

        QList<const Unit *> Pressure::units() const {
            static const QList<const Unit *> list{
                &Pa(),
                &hPa(),
                &kPa(),
                &MPa(),
                &bar(),
                &mbar(),
                &at(),
                &ata(),
                &atg(),
                &atm(),
                &kgf_cm2(),
                &kgf_m2(),
                &psi(),
                &psia(),
                &psig(),
                &mmHg(),
                &mmwg()
            };
            return list;
        }

        const Pressure &Pressure::group() {
            static const Pressure gr;
            return gr;
        }


        class PressureUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Pressure::Pa();}
        };

        const Unit &Pressure::Pa() {
            static const class PressureUnit_Pa : public PressureUnit {
            public:
                QString id() const override {return QStringLiteral("pres_pa");}
                QString symbol() const override {return Groups::tr("Pa");}
                QString displayFullName() const override {return Groups::tr("pascal");}
                QString fullPluralName() const override {return Groups::tr("pascals");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &Pressure::hPa()  {
            static const class PressureUnit_hPa : public PressureUnit {
            public:
                QString id() const override {return QStringLiteral("pres_hpa");}
                QString symbol() const override {return Groups::tr("hPa");}
                QString displayFullName() const override {return Groups::tr("hectopascal");}
                QString fullPluralName() const override {return Groups::tr("hectopascals");}
                double multipler() const override {return 100;}
            } u;
            return u;
        }

        const Unit &Pressure::kPa() {
            static const class PressureUnit_kPa : public PressureUnit {
            public:
                QString id() const override {return QStringLiteral("pres_kpa");}
                QString symbol() const override {return Groups::tr("kPa");}
                QString displayFullName() const override {return Groups::tr("kilopascal");}
                QString fullPluralName() const override {return Groups::tr("kilopascals");}
                double multipler() const override {return 1e3;}
            } u;
            return u;
        }

        const Unit &Pressure::MPa() {
            static const class PressureUnit_MPa : public PressureUnit {
            public:
                QString id() const override {return QStringLiteral("pres_Mpa");}
                QString symbol() const override {return Groups::tr("MPa");}
                QString displayFullName() const override {return Groups::tr("megapascal");}
                QString fullPluralName() const override {return Groups::tr("megapascals");}
                double multipler() const override {return 1e6;}
            } u;
            return u;
        }

        const Unit &Pressure::bar() {
            static const class PressureUnit_bar : public PressureUnit {
            public:
                QString id() const override {return QStringLiteral("pres_bar");}
                QString symbol() const override {return Groups::tr("bar");}
                QString displayFullName() const override {return Groups::tr("bar");}
                QString fullPluralName() const override {return Groups::tr("bars");}
                double multipler() const override {return 1e5;}
            } u;
            return u;
        }

        const Unit &Pressure::mbar() {
            static const class PressureUnit_mbar : public PressureUnit {
            public:
                QString id() const override {return QStringLiteral("pres_mbar");}
                QString symbol() const override {return Groups::tr("mbar");}
                QString displayFullName() const override {return Groups::tr("milibar");}
                QString fullPluralName() const override {return Groups::tr("milibars");}
                double multipler() const override {return 1e2;}
            } u;
            return u;
        }

        const Unit &Pressure::at() {
            static const class PressureUnit_at : public PressureUnit {
            public:
                QString id() const override {return QStringLiteral("pres_at");}
                QString symbol() const override {return Groups::tr("at");}
                QString displayFullName() const override {return Groups::tr("technical atmosphere");}
                QString fullPluralName() const override {return Groups::tr("technical atmospheres");}
                double multipler() const override {return Force::kgf_to_newton * 10e4;}
            } u;
            return u;
        }

        const Unit &Pressure::ata() {
            static const class PressureUnit_ata : public PressureUnit {
            public:
                QString id() const override {return QStringLiteral("pres_ata");}
                QString symbol() const override {return Groups::tr("ata");}
                QString displayFullName() const override {return Groups::tr("technical atmosphere (absolute)");}
                QString fullPluralName() const override {return Groups::tr("technical atmospheres (absolute)");}
                double multipler() const override {return Force::kgf_to_newton * 10e4;}
            } u;
            return u;
        }

        const Unit &Pressure::atg() {
            static const class PressureUnit_atg : public PressureUnit {
            public:
                QString id() const override {return QStringLiteral("pres_atg");}
                QString symbol() const override {return Groups::tr("atg");}
                QString displayFullName() const override {return Groups::tr("technical atmosphere (gauge)");}
                QString fullPluralName() const override {return Groups::tr("technical atmospheres (gauge)");}
                double multipler() const override {return Force::kgf_to_newton * 10e4;}
            } u;
            return u;
        }

        const Unit &Pressure::atm() {
            static const class PressureUnit_atm : public PressureUnit {
            public:
                QString id() const override {return QStringLiteral("pres_atm");}
                QString symbol() const override {return Groups::tr("atm");}
                QString displayFullName() const override {return Groups::tr("standard atmosphere");}
                QString fullPluralName() const override {return Groups::tr("standard atmospheres");}
                double multipler() const override {return atm_to_p_mul;}
            } u;
            return u;
        }

        const Unit &Pressure::kgf_cm2() {
            static const class PressureUnit_kgf_cm2 : public PressureUnit {
            public:
                QString id() const override {return QStringLiteral("pres_kgf_cm2");}
                QString symbol() const override {return Groups::tr("kgf/cm²");}
                QString asciiSymbol() const override {return Groups::tr("kgf/cm^2");}
                QString displayFullName() const override {return Groups::tr("kilogram-force per square centimeter");}
                QString fullPluralName() const override {return Groups::tr("kilograms-force per square centimeter");}
                double multipler() const override {return Force::kgf_to_newton * 10e4;}
            } u;
            return u;
        }

        const Unit &Pressure::kgf_m2() {
            static const class PressureUnit_kgf_m2 : public PressureUnit {
            public:
                QString id() const override {return QStringLiteral("pres_kgf_m2");}
                QString symbol() const override {return Groups::tr("kgf/m²");}
                QString asciiSymbol() const override {return Groups::tr("kgf/m^2");}
                QString displayFullName() const override {return Groups::tr("kilogram-force per square meter");}
                QString fullPluralName() const override {return Groups::tr("kilograms-force per square meter");}
                double multipler() const override {return Force::kgf_to_newton;}
            } u;
            return u;
        }

        const Unit &Pressure::psi() {
            static const class PressureUnit_psi : public PressureUnit {
            public:
                QString id() const override {return QStringLiteral("pres_psi");}
                QString symbol() const override {return Groups::tr("psi");}
                QString displayFullName() const override {return Groups::tr("pound-force per square inch");}
                QString fullPluralName() const override {return Groups::tr("pounds-force per square inch");}
                double multipler() const override {return psi_to_p_mul;}
            } u;
            return u;
        }

        const Unit &Pressure::psia() {
            static const class PressureUnit_psia : public PressureUnit {
            public:
                QString id() const override {return QStringLiteral("pres_psia");}
                QString symbol() const override {return Groups::tr("psia");}
                QString displayFullName() const override {return Groups::tr("pound-force per square inch (absolute)");}
                QString fullPluralName() const override {return Groups::tr("pounds-force per square inch (absolute)");}
                double multipler() const override {return psi_to_p_mul;}
            } u;
            return u;
        }

        const Unit &Pressure::psig() {
            static const class PressureUnit_psig: public PressureUnit {
            public:
                QString id() const override {return QStringLiteral("pres_psig");}
                QString symbol() const override {return Groups::tr("psig");}
                QString displayFullName() const override {return Groups::tr("pound-force per square inch (gauge)");}
                QString fullPluralName() const override {return Groups::tr("pounds-force per square inch (gauge)");}
                double multipler() const override {return psi_to_p_mul;}
            } u;
            return u;
        }

        const Unit &Pressure::mmHg() {
            static const class PressureUnit_mmHg : public PressureUnit {
            public:
                QString id() const override {return QStringLiteral("pres_mmHg");}
                QString symbol() const override {return Groups::tr("mm Hg");}
                QString displayFullName() const override {return Groups::tr("millimetre of mercury");}
                QString fullPluralName() const override {return Groups::tr("millimetres of mercury");}
                double multipler() const override {return 133.322;}
            } u;
            return u;
        }

        const Unit &Pressure::mmwg() {
            static const class PressureUnit_mmwg : public PressureUnit {
            public:
                QString id() const override {return QStringLiteral("pres_mmwg");}
                QString symbol() const override {return Groups::tr("mmwg");}
                QString displayFullName() const override {return Groups::tr("millimetre of water");}
                QString fullPluralName() const override {return Groups::tr("millimeters, water gauge");}
                double multipler() const override {return Force::kgf_to_newton;}
            } u;
            return u;
        }
    }
}
