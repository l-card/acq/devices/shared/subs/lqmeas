#include "Velocity.h"
#include "Displacement.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString Velocity::id() const {return QStringLiteral("unitgroup_velocity");}
        QString Velocity::displayName() const {return Groups::tr("Velocity");}

        QList<const Unit *> Velocity::units() const {
            static const QList<const Unit *> list {
                &m_s(),
                &cm_s(),
                &mm_s(),
                &um_s(),
                &inch_s(),
                &km_h()
            };
            return list;
        }

        const Velocity &Velocity::group() {
            static const Velocity gr;
            return gr;
        }

        class VelocityUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Velocity::m_s();}
        };

        const Unit &Velocity::m_s() {
            static const class VelocityUnit_m_s : public VelocityUnit {
            public:
                QString id() const override {return QStringLiteral("velocity_m_s");}
                QString symbol() const override {return Groups::tr("m/s");}
                QString displayFullName() const override {return Groups::tr("metre per second");}
                QString fullPluralName() const override {return Groups::tr("metres per second");}
                double multipler() const override {return 1.;}
            } u;
            return u;
        }

        const Unit &Velocity::cm_s() {
            static const class VelocityUnit_cm_s : public VelocityUnit {
            public:
                QString id() const override {return QStringLiteral("velocity_cm_s");}
                QString symbol() const override {return Groups::tr("cm/s");}
                QString displayFullName() const override {return Groups::tr("centimetre per second");}
                QString fullPluralName() const override {return Groups::tr("centimetres per second");}
                double multipler() const override {return 1e-2;}
            } u;
            return u;
        }

        const Unit &Velocity::mm_s() {
            static const class VelocityUnit_mm_s : public VelocityUnit {
            public:
                QString id() const override {return QStringLiteral("velocity_mm_s");}
                QString symbol() const override {return Groups::tr("mm/s");}
                QString displayFullName() const override {return Groups::tr("millimetre per second");}
                QString fullPluralName() const override {return Groups::tr("millimetres per second");}
                double multipler() const override {return 1e-3;}
            } u;
            return u;
        }

        const Unit &Velocity::um_s() {
            static const class VelocityUnit_um_s : public VelocityUnit {
            public:
                QString id() const override {return QStringLiteral("velocity_um_s");}
                QString symbol() const override {return Groups::tr("µm/s");}
                QString asciiSymbol() const override {return Groups::tr("um/s");}
                QString displayFullName() const override {return Groups::tr("micrometre per second");}
                QString fullPluralName() const override {return Groups::tr("micrometres per second");}
                double multipler() const override {return 1e-6;}
            } u;
            return u;
        }

        const Unit &Velocity::inch_s() {
            static const class VelocityUnit_inch_s : public VelocityUnit {
            public:
                QString id() const override {return QStringLiteral("velocity_inch_s");}
                QString symbol() const override {return Groups::tr("in/s");}
                QString displayFullName() const override {return Groups::tr("inch per second");}
                QString fullPluralName() const override {return Groups::tr("inch per second");}
                double multipler() const override {return Displacement::inch_to_m_mult;}
            } u;
            return u;
        }

        const Unit &Velocity::km_h() {
            static const class VelocityUnit_km_h : public VelocityUnit {
            public:
                QString id() const override {return QStringLiteral("velocity_km_h");}
                QString symbol() const override {return Groups::tr("km/h");}
                QString displayFullName() const override {return Groups::tr("kilometre per hour");}
                QString fullPluralName() const override {return Groups::tr("kilometres per hour");}
                double multipler() const override {return 1./3.6;}
            } u;
            return u;
        }
    }
}
