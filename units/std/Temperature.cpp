#include "Temperature.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString Temperature::id() const {return QStringLiteral("unitgroup_temperature");}
        QString Temperature::displayName() const {return Groups::tr("Temperature");}

        QList<const Unit *> Temperature::units() const {
            static const QList<const Unit *> list {
                &K(),
                &degreeC(),
                &degreeF()
            };
            return list;
        }

        const Temperature &Temperature::group() {
            static const Temperature gr;
            return gr;
        }



        class TemperatureUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Temperature::K();}
        };


        const Unit &Temperature::K() {
            static const class TemperatureUnit_K : public TemperatureUnit {
            public:
                QString id() const override {return QStringLiteral("temp_K");}
                QString symbol() const override {return Groups::tr("K");}
                QString displayFullName() const override {return Groups::tr("kelvin");}
                QString fullPluralName() const override {return Groups::tr("kelvins");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }


        const Unit &Temperature::degreeC() {
            static const class TemperatureUnit_DegreeC : public TemperatureUnit {
            public:
                QString id() const override {return QStringLiteral("temp_degreeC");}
                QString symbol() const override {return Groups::tr("°C");}
                QString asciiSymbol() const override {return Groups::tr("C");}
                QString displayFullName() const override {return Groups::tr("degree Celsius");}
                QString fullPluralName() const override {return Groups::tr("degrees Celsius");}
                double multipler() const override {return 1;}
                double zeroPoint() const override {return 273.15;}
            } u;
            return u;
        }

        const Unit &Temperature::degreeF() {
            static const class TemperatureUnit_DegreeF : public TemperatureUnit {
            public:
                QString id() const override {return QStringLiteral("temp_degreeF");}
                QString symbol() const override {return Groups::tr("°F");}
                QString asciiSymbol() const override {return Groups::tr("F");}
                QString displayFullName() const override {return Groups::tr("degree Fahrenheit");}
                QString fullPluralName() const override {return Groups::tr("degrees Fahrenheit");}
                double multipler() const override {return 5./9;}
                double zeroPoint() const override {return 459.67 * 5./9;}
            } u;
            return u;
        }
    }
}


