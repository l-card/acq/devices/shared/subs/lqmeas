#include "VolumetricFlowRate.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString VolumetricFlowRate::id() const {return QStringLiteral("unitgroup_volflowrate");}
        QString VolumetricFlowRate::displayName() const {return Groups::tr("Volumetric Flow Rate");}

        QList<const Unit *> VolumetricFlowRate::units() const {
            static const QList<const Unit *> list {
                &m3_s(),
                &m3_minute(),
                &m3_h(),
                &l_s(),
                &l_minute(),
                &l_h(),
                &ml_s()
            };
            return list;
        }

        const VolumetricFlowRate &VolumetricFlowRate::group() {
            static const VolumetricFlowRate gr;
            return gr;
        }

        class VolumeUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return VolumetricFlowRate::m3_s();}
        };

        const Unit &VolumetricFlowRate::m3_s() {
            static const  class VolumetricFlowRateUnit_m3_s : public VolumeUnit {
            public:
                QString id() const override {return QStringLiteral("volflowrate_m3_s");}
                QString symbol() const override {return Groups::tr("m³/s");}
                QString asciiSymbol() const override {return Groups::tr("m^3/s");}
                QString displayFullName() const override {return Groups::tr("cubic metre per second");}
                QString fullPluralName() const override {return Groups::tr("cubic metres per second");}
                double multipler() const override {return 1;}
            } u;
            return u;
        };

        const Unit &VolumetricFlowRate::m3_minute() {
            static const  class VolumetricFlowRateUnit_m3_min : public VolumeUnit {
            public:
                QString id() const override {return QStringLiteral("volflowrate_m3_min");}
                QString symbol() const override {return Groups::tr("m³/min");}
                QString asciiSymbol() const override {return Groups::tr("m^3/min");}
                QString displayFullName() const override {return Groups::tr("cubic metre per minute");}
                QString fullPluralName() const override {return Groups::tr("cubic metres per minute");}
                double multipler() const override {return 1./60;}
            } u;
            return u;
        }

        const Unit &VolumetricFlowRate::m3_h() {
            static const  class VolumetricFlowRateUnit_m3_h : public VolumeUnit {
            public:
                QString id() const override {return QStringLiteral("volflowrate_m3_h");}
                QString symbol() const override {return Groups::tr("m³/h");}
                QString asciiSymbol() const override {return Groups::tr("m^3/h");}
                QString displayFullName() const override {return Groups::tr("cubic metre per hour");}
                QString fullPluralName() const override {return Groups::tr("cubic metres per hour");}
                double multipler() const override {return 1./3600;}
            } u;
            return u;
        }

        const Unit &VolumetricFlowRate::l_s() {
            static const  class VolumetricFlowRateUnit_l_s : public VolumeUnit {
            public:
                QString id() const override {return QStringLiteral("volflowrate_l_s");}
                QString symbol() const override {return Groups::tr("l/s");}
                QString displayFullName() const override {return Groups::tr("litre per second");}
                QString fullPluralName() const override {return Groups::tr("litres per second");}
                double multipler() const override {return 1e-3;}
            } u;
            return u;
        };

        const Unit &VolumetricFlowRate::l_minute() {
            static const  class VolumetricFlowRateUnit_l_min : public VolumeUnit {
            public:
                QString id() const override {return QStringLiteral("volflowrate_l_min");}
                QString symbol() const override {return Groups::tr("l/min");}
                QString displayFullName() const override {return Groups::tr("litre per minute");}
                QString fullPluralName() const override {return Groups::tr("litres per minute");}
                double multipler() const override {return 1e-3/60;}
            } u;
            return u;
        };

        const Unit &VolumetricFlowRate::l_h() {
            static const  class VolumetricFlowRateUnit_l_h : public VolumeUnit {
            public:
                QString id() const override {return QStringLiteral("volflowrate_l_h");}
                QString symbol() const override {return Groups::tr("l/h");}
                QString displayFullName() const override {return Groups::tr("litre per hour");}
                QString fullPluralName() const override {return Groups::tr("litres per hour");}
                double multipler() const override {return 1e-3/3600;}
            } u;
            return u;
        }

        const Unit &VolumetricFlowRate::ml_s() {
            static const  class VolumetricFlowRateUnit_ml_s : public VolumeUnit {
            public:
                QString id() const override {return QStringLiteral("volflowrate_ml_s");}
                QString symbol() const override {return Groups::tr("ml/s");}
                QString displayFullName() const override {return Groups::tr("millilitre per second");}
                QString fullPluralName() const override {return Groups::tr("millilitres per second");}
                double multipler() const override {return 1e-6;}
            } u;
            return u;
        };
    }
}
