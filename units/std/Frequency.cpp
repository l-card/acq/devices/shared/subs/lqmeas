#include "Frequency.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"

namespace LQMeas {
    namespace Units {
        QString Frequency::id() const {return QStringLiteral("unitgroup_frequency");}
        QString Frequency::displayName() const {return Groups::tr("Frequency");}

        QList<const Unit *> Frequency::units() const {
            static const QList<const Unit *> list {
                &Hz(),
                &kHz(),
                &MHz()
            };
            return list;
        }

        const Frequency &Frequency::group() {
            static const Frequency gr;
            return gr;
        }

        class FrequencyUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Frequency::Hz();}
        };

        const Unit &Frequency::Hz() {
            static const class FrequencyUnit_Hz : public FrequencyUnit {
            public:
                QString id() const override {return QStringLiteral("freq_hz");}
                QString symbol() const override {return Groups::tr("Hz");}
                QString displayFullName() const override {return Groups::tr("hertz");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &Frequency::kHz() {
            static const class FrequencyUnit_kHz : public FrequencyUnit {
            public:
                QString id() const override {return QStringLiteral("freq_khz");}
                QString symbol() const override {return Groups::tr("kHz");}
                QString displayFullName() const override {return Groups::tr("kiloherz");}
                double multipler() const override {return 1e3;}
            } u;
            return u;
        }

        const Unit &Frequency::MHz() {
            static const class FrequencyUnit_MHz : public FrequencyUnit {
            public:
                QString id() const override {return QStringLiteral("freq_Mhz");}
                QString symbol() const override {return Groups::tr("MHz");}
                QString displayFullName() const override {return Groups::tr("megaherz");}
                double multipler() const override {return 1e6;}
            } u;
            return u;
        }
    }
}
