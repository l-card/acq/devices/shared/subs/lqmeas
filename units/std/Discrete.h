#ifndef LQMEAS_UNITS_DISCRETE_H
#define LQMEAS_UNITS_DISCRETE_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Discrete : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Discrete &group();


            static const Unit &logicalLevel(); /* логический уровень (0 или 1) */
        };
    }
}

#endif // LQMEAS_UNITS_DISCRETE_H
