#ifndef LQMEAS_UNITS_GROUPS_H
#define LQMEAS_UNITS_GROUPS_H

#include <QObject>


namespace LQMeas {
    class  UnitGroup;
    class  Unit;

    namespace Units {
        class Groups  : public QObject {
            Q_OBJECT
        public:
            static const UnitGroup &voltage();
            static const UnitGroup &current();
            static const UnitGroup &resistance();
            static const UnitGroup &power();
            static const UnitGroup &electricCharge();
            static const UnitGroup &temperature();
            static const UnitGroup &force();
            static const UnitGroup &energy();
            static const UnitGroup &time();
            static const UnitGroup &frequency();
            static const UnitGroup &rotationalSpeed();
            static const UnitGroup &displacement();
            static const UnitGroup &velocity();
            static const UnitGroup &acceleration();
            static const UnitGroup &mass();
            static const UnitGroup &pressure();
            static const UnitGroup &pressureImpulse();
            static const UnitGroup &massFlowRate();
            static const UnitGroup &volumetricFlowRate();
            static const UnitGroup &area();
            static const UnitGroup &volume();
            static const UnitGroup &density();
            static const UnitGroup &angle();
            static const UnitGroup &relative();
            static const UnitGroup &relativeDeformation();

            static const UnitGroup &discrete();
            static const UnitGroup &digital();
            static const UnitGroup &information();
            static const UnitGroup &informationSpeed();
            static const UnitGroup &special();

            static const QList<const UnitGroup *> &analogGroups();
            static const QList<const UnitGroup *> &allGroups();
            static QList<const Unit *> analogUnits();
            static QList<const Unit *> allUnits();
        };
    }
}

#endif // LQMEAS_UNITS_GROUPS_H
