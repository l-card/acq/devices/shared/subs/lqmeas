#include "RotationalSpeed.h"
#include "Frequency.h"

#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"
#include <QtMath>

namespace LQMeas {
    namespace Units {
        QString RotationalSpeed::id() const {return QStringLiteral("unitgroup_rotspeed");}
        QString RotationalSpeed::displayName() const {return Groups::tr("Rotational Speed");}

        QList<const Unit *> RotationalSpeed::units() const {
            static const QList<const Unit *> list {
                &rpm(),
                &rps(),
                &rad_s()
            };
            return list;
        }

        const RotationalSpeed &RotationalSpeed::group() {
            static const RotationalSpeed gr;
            return gr;
        }

        class RotationalSpeedUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Frequency::Hz();}
        };


        const Unit &RotationalSpeed::rpm() {
            static const class FrequencyUnit_RPM : public RotationalSpeedUnit {
            public:
                QString id() const override {return QStringLiteral("freq_rpm");}
                QString symbol() const override {return Groups::tr("rpm");}
                QString displayFullName() const override {return Groups::tr("revolutions per minute");}
                QString fullPluralName() const override {return Groups::tr("revolutions per minute");}
                double multipler() const override {return 1./60;}
            } u;
            return u;
        }

        const Unit &RotationalSpeed::rps() {
            static const class FrequencyUnit_RPS : public RotationalSpeedUnit {
            public:
                QString id() const override {return QStringLiteral("freq_rps");}
                QString symbol() const override {return Groups::tr("rps");}
                QString displayFullName() const override {return Groups::tr("revolutions per second");}
                QString fullPluralName() const override {return Groups::tr("revolutions per second");}
                double multipler() const override {return 1.;}
            } u;
            return u;
        }

        const Unit &RotationalSpeed::rad_s() {
            static const class FrequencyUnit_rad_s : public RotationalSpeedUnit {
            public:
                QString id() const override {return QStringLiteral("freq_rad_s");}
                QString symbol() const override {return Groups::tr("rad/s");}
                QString displayFullName() const override {return Groups::tr("radian per second");}
                QString fullPluralName() const override {return Groups::tr("radians per second");}
                double multipler() const override {return 1./(2. * M_PI);}
            } u;
            return u;
        }
    }
}
