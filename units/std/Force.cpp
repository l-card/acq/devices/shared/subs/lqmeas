#include "Force.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString Force::id() const {return QStringLiteral("unitgroup_force");}
        QString Force::displayName() const {return Groups::tr("Force");}

        QList<const Unit *> Force::units() const {
            static const QList<const Unit *> list {
                &N(),
                &kN(),
                &kgf(),
                &gf(),
                &tf(),
            };
            return list;
        }

        const Force &Force::group() {
            static const Force gr;
            return gr;
        }

        class ForceUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Force::N();}
        };

        const Unit &Force::N() {
            static const class ForceUnit_N : public ForceUnit {
            public:
                QString id() const override {return QStringLiteral("force_N");}
                QString symbol() const override {return Groups::tr("N");}
                QString displayFullName() const override {return Groups::tr("newton");}
                QString fullPluralName() const override {return Groups::tr("newtons");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &Force::kN() {
            static const class ForceUnit_kN : public ForceUnit {
            public:
                QString id() const override {return QStringLiteral("force_kN");}
                QString symbol() const override {return Groups::tr("kN");}
                QString displayFullName() const override {return Groups::tr("kilonewton");}
                QString fullPluralName() const override {return Groups::tr("kilonewtons");}
                double multipler() const override {return 1e3;}
            } u;
            return u;
        }

        const Unit &Force::kgf() {
            static const class ForceUnit_kgf : public ForceUnit {
            public:
                QString id() const override {return QStringLiteral("force_kgf");}
                QString symbol() const override {return Groups::tr("kgf");}
                QString displayFullName() const override {return Groups::tr("kilogram-force");}
                QString fullPluralName() const override {return Groups::tr("kilograms-force");}
                double multipler() const override {return kgf_to_newton;}
            } u;
            return u;
        }

        const Unit &Force::gf() {
            static const class ForceUnit_gf : public ForceUnit {
            public:
                QString id() const override {return QStringLiteral("force_gf");}
                QString symbol() const override {return Groups::tr("gf");}
                QString displayFullName() const override {return Groups::tr("gram-force");}
                QString fullPluralName() const override {return Groups::tr("grams-force");}
                double multipler() const override {return 1e-3 * kgf_to_newton;}
            } u;
            return u;
        }

        const Unit &Force::tf() {
            static const class ForceUnit_tf : public ForceUnit {
            public:
                QString id() const override {return QStringLiteral("force_tf");}
                QString symbol() const override {return Groups::tr("tf");}
                QString displayFullName() const override {return Groups::tr("tonne-force");}
                QString fullPluralName() const override {return Groups::tr("tonns-force");}
                double multipler() const override {return 1e3 * kgf_to_newton;}
            } u;
            return u;
        }
    }
}
