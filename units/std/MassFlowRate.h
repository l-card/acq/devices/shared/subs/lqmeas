#ifndef LQMEAS_UNITS_MASSFLOWRATE_H
#define LQMEAS_UNITS_MASSFLOWRATE_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class MassFlowRate : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const MassFlowRate &group();


            static const Unit &g_s();   /* г/с */
            static const Unit &mg_s();  /* мг/с */
            static const Unit &kg_s();  /* кг/с */
            static const Unit &kg_minute();/* кг/мин */
            static const Unit &kg_h();  /* кг/ч */
            static const Unit &t_s();   /* т/с */
            static const Unit &t_minute(); /* т/мин */
            static const Unit &t_h();   /* т/ч */

        };
    }
}

#endif // MASSFLOWRATE_H

