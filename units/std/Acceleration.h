#ifndef LQMEAS_UNITS_ACCELERATION_H
#define LQMEAS_UNITS_ACCELERATION_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Acceleration : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Acceleration &group();

            static const Unit &m_s2(); /* м/с^2 */
            static const Unit &mm_s2(); /* мм/с^2 */
            static const Unit &g();    /* g */
        };
    }
}


#endif // LQMEAS_UNITS_ACCELERATION_H
