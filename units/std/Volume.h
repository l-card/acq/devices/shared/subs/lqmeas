#ifndef LQMEAS_UNITS_VOLUME_H
#define LQMEAS_UNITS_VOLUME_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Volume : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Volume &group();


            static const Unit &m3();  /* метры³ */
            static const Unit &cm3(); /* сантиметры³ */
            static const Unit &mm3(); /* милиметры³ */
            static const Unit &l();   /* литры */
            static const Unit &ml();   /* миллилитры */

        };
    }
}


#endif // LQMEAS_UNITS_VOLUME_H
