#ifndef LQMEAS_UNITS_VELOCITY_H
#define LQMEAS_UNITS_VELOCITY_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Velocity : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Velocity &group();

            static const Unit &m_s();   /* м/c */
            static const Unit &cm_s();  /* см/с */
            static const Unit &mm_s();  /* мм/с */
            static const Unit &um_s();  /* мкм/с */
            static const Unit &inch_s();/* дюйм/с */
            static const Unit &km_h();  /* км/ч */
        };
    }
}

#endif // LQMEAS_UNITS_VELOCITY_H
