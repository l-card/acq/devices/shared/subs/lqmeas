#include "Energy.h"
#include "Force.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString Energy::id() const {return QStringLiteral("unitgroup_energy");}
        QString Energy::displayName() const {return Groups::tr("Energy");}

        QList<const Unit *> Energy::units() const {
            static const QList<const Unit *> list {
                &J(),
                &kJ(),
                &kgfm(),
                &erg(),
                &cal(),
                &kcal(),
                &Wh(),
                &kWh(),
                &MWh(),
                &vah(),
                &kvah(),
                &Mvah(),
                &varh(),
                &kvarh(),
                &Mvarh()
            };
            return list;
        }

        const Energy &Energy::group() {
            static const Energy gr;
            return gr;
        }

        class EnergyUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Energy::J();}
        };

        const Unit &Energy::J() {
            static const class EnergyUnit_J : public EnergyUnit {
            public:
                QString id() const override {return QStringLiteral("energy_J");}
                QString symbol() const override {return Groups::tr("J");}
                QString displayFullName() const override {return Groups::tr("joule");}
                QString fullPluralName() const override {return Groups::tr("joules");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &Energy::kJ() {
            static const class EnergyUnit_kJ : public EnergyUnit {
            public:
                QString id() const override {return QStringLiteral("energy_kJ");}
                QString symbol() const override {return Groups::tr("kJ");}
                QString displayFullName() const override {return Groups::tr("kilojoule");}
                QString fullPluralName() const override {return Groups::tr("kilojoules");}
                double multipler() const override {return 1e3;}
            } u;
            return u;
        }

        const Unit &Energy::kgfm() {
            static const class EnergyUnit_kgfm : public EnergyUnit {
            public:
                QString id() const override {return QStringLiteral("energy_kgfm");}
                QString symbol() const override {return Groups::tr("kgf·m");}
                QString displayFullName() const override {return Groups::tr("kilogram-force-metre");}
                QString fullPluralName() const override {return Groups::tr("kilogram-force-metres");}
                double multipler() const override {return Force::kgf_to_newton;}
            } u;
            return u;
        }

        const Unit &Energy::erg() {
            static const class EnergyUnit_erg : public EnergyUnit {
            public:
                QString id() const override {return QStringLiteral("energy_erg");}
                QString symbol() const override {return Groups::tr("erg");}
                QString displayFullName() const override {return Groups::tr("erg");}
                QString fullPluralName() const override {return Groups::tr("ergs");}
                double multipler() const override {return erg_to_j_mul;}
            } u;
            return u;
        }

        const Unit &Energy::cal() {
            static const class EnergyUnit_cal : public EnergyUnit {
            public:
                QString id() const override {return QStringLiteral("energy_cal");}
                QString symbol() const override {return Groups::tr("cal");}
                QString displayFullName() const override {return Groups::tr("calorie");}
                QString fullPluralName() const override {return Groups::tr("calories");}
                double multipler() const override {return cal_to_j_mul;}
            } u;
            return u;
        }

        const Unit &Energy::kcal() {
            static const class EnergyUnit_kcal : public EnergyUnit {
            public:
                QString id() const override {return QStringLiteral("energy_kcal");}
                QString symbol() const override {return Groups::tr("kcal");}
                QString displayFullName() const override {return Groups::tr("kilocalorie");}
                QString fullPluralName() const override {return Groups::tr("kilocalories");}
                double multipler() const override {return 1e3*cal_to_j_mul;}
            } u;
            return u;
        }



        const Unit &Energy::Wh() {
            static const class EnergyUnit_Wh : public EnergyUnit {
            public:
                QString id() const override {return QStringLiteral("energy_wh");}
                QString symbol() const override {return Groups::tr("Wh");}
                QString displayFullName() const override {return Groups::tr("watt-hour");}
                QString fullPluralName() const override {return Groups::tr("watt-hours");}
                double multipler() const override {return wh_to_j_mul;}
            } u;
            return u;
        }

        const Unit &Energy::kWh() {
            static const class EnergyUnit_kWh : public EnergyUnit {
            public:
                QString id() const override {return QStringLiteral("energy_kwh");}
                QString symbol() const override {return Groups::tr("kWh");}
                QString displayFullName() const override {return Groups::tr("kilowatt-hour");}
                QString fullPluralName() const override {return Groups::tr("kilowatt-hours");}
                double multipler() const override {return 1e3 * wh_to_j_mul;}
            } u;
            return u;
        }

        const Unit &Energy::MWh() {
            static const class EnergyUnit_MWh : public EnergyUnit {
            public:
                QString id() const override {return QStringLiteral("energy_Mwh");}
                QString symbol() const override {return Groups::tr("MWh");}
                QString displayFullName() const override {return Groups::tr("megawatt-hour");}
                QString fullPluralName() const override {return Groups::tr("megawatt-hours");}
                double multipler() const override {return 1e6 * wh_to_j_mul;}
            } u;
            return u;
        }

        const Unit &Energy::vah() {
            static const class EnergyUnit_vah : public EnergyUnit {
            public:
                QString id() const override {return QStringLiteral("energy_vah");}
                QString symbol() const override {return Groups::tr("V⋅A⋅h");}
                QString displayFullName() const override {return Groups::tr("volt-ampere-hour");}
                QString fullPluralName() const override {return Groups::tr("volt-ampere-hours");}
                double multipler() const override {return wh_to_j_mul;}
            } u;
            return u;
        }

        const Unit &Energy::kvah() {
            static const class EnergyUnit_kvah : public EnergyUnit {
            public:
                QString id() const override {return QStringLiteral("energy_kvah");}
                QString symbol() const override {return Groups::tr("kV⋅A⋅h");}
                QString displayFullName() const override {return Groups::tr("kilovolt-ampere-hour");}
                QString fullPluralName() const override {return Groups::tr("kilovolt-ampere-hours");}
                double multipler() const override {return 1e3 * wh_to_j_mul;}
            } u;
            return u;
        }

        const Unit &Energy::Mvah() {
            static const class EnergyUnit_Mvah : public EnergyUnit {
            public:
                QString id() const override {return QStringLiteral("energy_Mvah");}
                QString symbol() const override {return Groups::tr("MV⋅A⋅h");}
                QString displayFullName() const override {return Groups::tr("megavolt-ampere-hour");}
                QString fullPluralName() const override {return Groups::tr("megavolt-ampere-hours");}
                double multipler() const override {return 1e6 * wh_to_j_mul;}
            } u;
            return u;
        }

        const Unit &Energy::varh() {
            static const class EnergyUnit_varh : public EnergyUnit {
            public:
                QString id() const override {return QStringLiteral("energy_varh");}
                QString symbol() const override {return Groups::tr("var⋅h");}
                QString displayFullName() const override {return Groups::tr("volt-ampere reactive-hour");}
                QString fullPluralName() const override {return Groups::tr("volt-ampere reactive-hours");}
                double multipler() const override {return wh_to_j_mul;}
            } u;
            return u;
        }

        const Unit &Energy::kvarh() {
            static const class EnergyUnit_kvarh : public EnergyUnit {
            public:
                QString id() const override {return QStringLiteral("energy_kvarh");}
                QString symbol() const override {return Groups::tr("kvar⋅h");}
                QString displayFullName() const override {return Groups::tr("kilovolt-ampere reactive-hour");}
                QString fullPluralName() const override {return Groups::tr("kilovolt-ampere reactive-hours");}
                double multipler() const override {return 1e3 * wh_to_j_mul;}
            } u;
            return u;
        }

        const Unit &Energy::Mvarh() {
            static const class EnergyUnit_Mvarh : public EnergyUnit {
            public:
                QString id() const override {return QStringLiteral("energy_Mvarh");}
                QString symbol() const override {return Groups::tr("Mvar⋅h");}
                QString displayFullName() const override {return Groups::tr("megavolt-ampere reactive-hour");}
                QString fullPluralName() const override {return Groups::tr("megavolt-ampere reactive-hours");}
                double multipler() const override {return 1e6 * wh_to_j_mul;}
            } u;
            return u;
        }
    }
}
