#ifndef LQMEAS_UNITS_DENSITY_H
#define LQMEAS_UNITS_DENSITY_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Density : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Density &group();

            static const Unit &g_m3();    /* г/м³ */
            static const Unit &kg_m3();   /* кг/м³ */
            static const Unit &g_cm3();   /* г/см³ */
            static const Unit &mg_cm3();  /* мг/см³ */
            static const Unit &g_l();     /* г/л */
        };
    }
}

#endif // LQMEAS_UNITS_DENSITY_H
