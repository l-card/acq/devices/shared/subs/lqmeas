#ifndef LQMEAS_UNITS_SPECIAL_H
#define LQMEAS_UNITS_SPECIAL_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Special : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Special &group();

            static const Unit &unknown();
            static const Unit &unitless(); /* безразмерная величина */
        };
    }
}

#endif // LQMEAS_UNITS_SPECIAL_H
