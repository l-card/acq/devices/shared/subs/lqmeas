#ifndef LQMEAS_UNITS_VOLTAGE_H
#define LQMEAS_UNITS_VOLTAGE_H


#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Voltage : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Voltage &group();


            static const Unit &V();  /* вольты */
            static const Unit &mV(); /* миливольты */
            static const Unit &uV(); /* микровольты */
            static const Unit &kV(); /* киловольты */
        };
    }
}

#endif // LQMEAS_UNITS_VOLTAGE_H
