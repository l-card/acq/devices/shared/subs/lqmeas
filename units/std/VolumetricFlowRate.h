#ifndef LQMEAS_UNITS_VOLUMETRICFLOWRATE_H
#define LQMEAS_UNITS_VOLUMETRICFLOWRATE_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class VolumetricFlowRate : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const VolumetricFlowRate &group();


            static const Unit &m3_s();      /* м³/с */
            static const Unit &m3_minute(); /* м³/мин */
            static const Unit &m3_h();      /* м³/ч */

            static const Unit &l_s();       /* л/с */
            static const Unit &l_minute();  /* л/мин */
            static const Unit &l_h();       /* л/ч */

            static const Unit &ml_s();       /* мл/с */
        };
    }
}




#endif // VOLUMETRICFLOWRATE_H
