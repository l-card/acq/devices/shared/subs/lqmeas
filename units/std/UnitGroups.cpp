#include "UnitGroups.h"
#include "Voltage.h"
#include "Current.h"
#include "Resistance.h"
#include "Power.h"
#include "ElectricCharge.h"
#include "Temperature.h"
#include "Force.h"
#include "Energy.h"
#include "Time.h"
#include "Frequency.h"
#include "RotationalSpeed.h"
#include "Displacement.h"
#include "Velocity.h"
#include "Acceleration.h"
#include "Mass.h"
#include "Pressure.h"
#include "PressureImpulse.h"
#include "MassFlowRate.h"
#include "VolumetricFlowRate.h"
#include "Area.h"
#include "Volume.h"
#include "Density.h"
#include "Angle.h"
#include "Relative.h"
#include "RelativeDeformation.h"
#include "Discrete.h"
#include "Information.h"
#include "InformationSpeed.h"
#include "Digital.h"
#include "Special.h"


namespace LQMeas {
    const UnitGroup &Units::Groups::voltage() {
        return Voltage::group();
    }

    const UnitGroup &Units::Groups::current() {
        return Current::group();
    }

    const UnitGroup &Units::Groups::resistance() {
        return Resistance::group();
    }

    const UnitGroup &Units::Groups::power() {
        return Power::group();
    }

    const UnitGroup &Units::Groups::electricCharge() {
        return ElectricCharge::group();
    }

    const UnitGroup &Units::Groups::temperature() {
        return Temperature::group();
    }

    const UnitGroup &Units::Groups::force() {
        return Force::group();
    }

    const UnitGroup &Units::Groups::energy() {
        return Energy::group();
    }

    const UnitGroup &Units::Groups::time() {
        return Time::group();
    }

    const UnitGroup &Units::Groups::frequency() {
        return Frequency::group();
    }

    const UnitGroup &Units::Groups::rotationalSpeed() {
        return RotationalSpeed::group();
    }

    const UnitGroup &Units::Groups::displacement() {
        return Displacement::group();
    }

    const UnitGroup &Units::Groups::velocity() {
        return Velocity::group();
    }

    const UnitGroup &Units::Groups::acceleration() {
        return Acceleration::group();
    }

    const UnitGroup &Units::Groups::mass() {
        return Mass::group();
    }

    const UnitGroup &Units::Groups::pressure() {
        return Pressure::group();
    }

    const UnitGroup &Units::Groups::pressureImpulse() {
        return PressureImpulse::group();
    }

    const UnitGroup &Units::Groups::massFlowRate() {
        return MassFlowRate::group();
    }

    const UnitGroup &Units::Groups::volumetricFlowRate() {
        return VolumetricFlowRate::group();
    }

    const UnitGroup &Units::Groups::area() {
        return Area::group();
    }

    const UnitGroup &Units::Groups::volume() {
        return Volume::group();
    }

    const UnitGroup &Units::Groups::density() {
        return Density::group();
    }

    const UnitGroup &Units::Groups::angle() {
        return Angle::group();
    }

    const UnitGroup &Units::Groups::relative() {
        return Relative::group();
    }

    const UnitGroup &Units::Groups::relativeDeformation() {
        return RelativeDeformation::group();
    }

    const UnitGroup &Units::Groups::discrete() {
        return Discrete::group();
    }

    const UnitGroup &Units::Groups::information() {
        return Information::group();
    }

    const UnitGroup &Units::Groups::informationSpeed() {
        return InformationSpeed::group();
    }

    const UnitGroup &Units::Groups::digital() {
        return Digital::group();
    }

    const UnitGroup &Units::Groups::special() {
        return Special::group();
    }

    const QList<const UnitGroup *> &LQMeas::Units::Groups::analogGroups()  {
        static const QList<const UnitGroup *> groups {
            &voltage(),
            &current(),
            &resistance(),
            &power(),
            &electricCharge(),
            &temperature(),
            &force(),
            &energy(),
            &time(),
            &frequency(),
            &rotationalSpeed(),
            &displacement(),
            &velocity(),
            &acceleration(),
            &mass(),
            &pressure(),
            &pressureImpulse(),
            &massFlowRate(),
            &volumetricFlowRate(),
            &area(),
            &volume(),
            &density(),
            &angle(),
            &relative(),
            &relativeDeformation(),
        };
        return groups;
    }

    const QList<const UnitGroup *> &Units::Groups::allGroups() {
        static const QList<const UnitGroup *> groups {
            analogGroups() + QList<const UnitGroup *>{
                &discrete(),
                &digital(),
                &information(),
                &informationSpeed(),
                &special()
            }
        };
        return groups;
    }


    QList<const Unit *> Units::Groups::analogUnits() {
        QList<const Unit *> ret;
        ret.append(&Special::unitless());
        for (const LQMeas::UnitGroup *group : analogGroups()) {
            ret.append(group->units());
        }
        return ret;
    }

    QList<const Unit *> Units::Groups::allUnits() {
        QList<const Unit *> ret;
        for (const LQMeas::UnitGroup *group : allGroups()) {
            ret.append(group->units());
        }
        return ret;
    }



}
