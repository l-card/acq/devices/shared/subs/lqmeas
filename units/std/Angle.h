#ifndef ANGLE_H
#define ANGLE_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Angle : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Angle &group();

            static const Unit &degree(); /* градусы (угол) */
            static const Unit &rad(); /* радиан (угол) */

        };
    }
}



#endif // ANGLE_H
