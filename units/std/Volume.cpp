#include "Volume.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString Volume::id() const {return QStringLiteral("unitgroup_volume");}
        QString Volume::displayName() const {return Groups::tr("Volume");}

        QList<const Unit *> Volume::units() const {
            static const QList<const Unit *> list {
                &m3(),
                &cm3(),
                &mm3(),
                &l(),
                &ml(),
            };
            return list;
        }

        const Volume &Volume::group() {
            static const Volume gr;
            return gr;
        }

        class VolumeUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Volume::m3();}
        };

        const Unit &Volume::m3() {
            static const  class VolumeUnit_m3 : public VolumeUnit {
            public:
                QString id() const override {return QStringLiteral("volume_m3");}
                QString symbol() const override {return Groups::tr("m³");}
                QString asciiSymbol() const override {return Groups::tr("m^3");}
                QString displayFullName() const override {return Groups::tr("cubic metre");}
                QString fullPluralName() const override {return Groups::tr("cubic metres");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &Volume::cm3() {
            static const  class VolumeUnit_cm3 : public VolumeUnit {
            public:
                QString id() const override {return QStringLiteral("volume_cm3");}
                QString symbol() const override {return Groups::tr("cm³");}
                QString asciiSymbol() const override {return Groups::tr("cm^3");}
                QString displayFullName() const override {return Groups::tr("cubic centimetre");}
                QString fullPluralName() const override {return Groups::tr("cubic centimetres");}
                double multipler() const override {return 1e-6;}
            } u;
            return u;
        }

        const Unit &Volume::mm3() {
            static const  class VolumeUnit_mm3 : public VolumeUnit {
            public:
                QString id() const override {return QStringLiteral("volume_mm3");}
                QString symbol() const override {return Groups::tr("mm³");}
                QString asciiSymbol() const override {return Groups::tr("mm^3");}
                QString displayFullName() const override {return Groups::tr("cubic millimetre");}
                QString fullPluralName() const override {return Groups::tr("cubic millimetres");}
                double multipler() const override {return 1e-9;}
            } u;
            return u;
        }

        const Unit &Volume::l() {
            static const  class VolumeUnit_l : public VolumeUnit {
            public:
                QString id() const override {return QStringLiteral("volume_l");}
                QString symbol() const override {return Groups::tr("l");}
                QString displayFullName() const override {return Groups::tr("litre");}
                QString fullPluralName() const override {return Groups::tr("litres");}
                double multipler() const override {return 1e-3;}
            } u;
            return u;
        }

        const Unit &Volume::ml() {
            static const  class VolumeUnit_ml : public VolumeUnit {
            public:
                QString id() const override {return QStringLiteral("volume_ml");}
                QString symbol() const override {return Groups::tr("ml");}
                QString displayFullName() const override {return Groups::tr("millilitre");}
                QString fullPluralName() const override {return Groups::tr("millilitres");}
                double multipler() const override {return 1e-6;}
            } u;
            return u;
        }
    }
}
