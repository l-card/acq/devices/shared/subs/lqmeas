#ifndef LQMEAS_UNITS_TEMPERATURE_H
#define LQMEAS_UNITS_TEMPERATURE_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Temperature : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Temperature &group();


            static const Unit &K();       /* Кельвин */
            static const Unit &degreeC(); /* градусы Цельсия */
            static const Unit &degreeF(); /* градусы Фаренгейта */
        };
    }
}


#endif // LQMEAS_UNITS_TEMPERATURE_H
