#include "Mass.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString Mass::id() const { return QStringLiteral("unitgroup_mass"); }
        QString Mass::displayName() const { return Groups::tr("Mass");}

        QList<const Unit *> Mass::units() const {
            static const QList<const Unit *> list {
                &g(),
                &mg(),
                &ug(),
                &kg(),
                &t()
            };
            return list;
        }

        const Mass &Mass::group() {
            static const Mass gr;
            return gr;
        }

        class MassUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Mass::g();}
        };

        const Unit &Mass::g() {
            static const class MassUnit_g : public MassUnit {
            public:
                QString id() const override {return QStringLiteral("mass_g");}
                QString symbol() const override {return Groups::tr("g");}
                QString displayFullName() const override {return Groups::tr("gram");}
                QString fullPluralName() const override {return Groups::tr("grams");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &Mass::mg() {
            static const class MassUnit_mg : public MassUnit {
            public:
                QString id() const override {return QStringLiteral("mass_mg");}
                QString symbol() const override {return Groups::tr("mg");}
                QString displayFullName() const override {return Groups::tr("milligram");}
                QString fullPluralName() const override {return Groups::tr("milligrams");}
                double multipler() const override {return 1e-3;}
            } u;
            return u;
        }

        const Unit &Mass::ug() {
            static const class MassUnit_ug : public MassUnit {
            public:
                QString id() const override {return QStringLiteral("mass_ug");}
                QString symbol() const override {return Groups::tr("µg");}
                QString displayFullName() const override {return Groups::tr("microgram");}
                QString fullPluralName() const override {return Groups::tr("micrograms");}
                double multipler() const override {return 1e-6;}
            } u;
            return u;
        }

        const Unit &Mass::kg() {
            static const class MassUnit_kg : public MassUnit {
            public:
                QString id() const override {return QStringLiteral("mass_kg");}
                QString symbol() const override {return Groups::tr("kg");}
                QString displayFullName() const override {return Groups::tr("kilogram");}
                QString fullPluralName() const override {return Groups::tr("kilograms");}
                double multipler() const override {return 1e3;}
            } u;
            return u;
        }

        const Unit &Mass::t() {
            static const class MassUnit_t : public MassUnit {
            public:
                QString id() const override {return QStringLiteral("mass_t");}
                QString symbol() const override {return Groups::tr("t");}
                QString displayFullName() const override {return Groups::tr("tonne");}
                QString fullPluralName() const override {return Groups::tr("tonns");}
                double multipler() const override {return 1e6;}
            } u;
            return u;
        }
    }
}
