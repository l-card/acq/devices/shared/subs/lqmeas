#include "Relative.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"

namespace LQMeas {
    namespace Units {
        QString Relative::id() const {return QStringLiteral("unitgroup_relative");}
        QString Relative::displayName() const {return Groups::tr("Relative");}

        QList<const Unit *> Relative::units() const {
            static const QList<const Unit *> list {
                &ratio(),
                &percentage(),
                &db()
            };
            return list;
        }

        const Relative &Relative::group() {
            static const Relative gr;
            return gr;
        }

        const Unit &Relative::ratio() {
            static const class Unit_Ratio : public Unit {
            public:
                QString id() const override {return QStringLiteral("rel_ratio");}
                QString symbol() const override {return  QString();}
                QString displayFullName() const override {return Groups::tr("ratio");}
                double multipler() const override {return 1;}
                const Unit &baseUnit() const override {return Relative::ratio();}
            } u;
            return u;
        }

        const Unit &Relative::percentage() {
            static const class Unit_Percentage : public Unit {
            public:
                QString id() const override {return QStringLiteral("rel_percentage");}
                QString symbol() const override {return Groups::tr("%");}
                QString displayFullName() const override {return Groups::tr("percent");}
                QString fullPluralName() const override {return Groups::tr("percents");}
                double multipler() const override {return 1e-2;}
                const Unit &baseUnit() const override {return Relative::ratio();}
            } u;
            return u;
        }

        const Unit &Relative::db() {
            static const class Unit_Db : public Unit {
            public:
                QString id() const override {return QStringLiteral("rel_db");}
                QString symbol() const override {return Groups::tr("dB");}
                QString displayFullName() const override {return Groups::tr("decibel");}
                QString fullPluralName() const override {return Groups::tr("decibels");}
                double multipler() const override {return 1;}
                const Unit &baseUnit() const override {return Relative::db();}
            } u;
            return u;
        }
    }
}
