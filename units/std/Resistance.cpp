#include "Resistance.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString Resistance::id() const {return QStringLiteral("unitgroup_resistance");}
        QString Resistance::displayName() const {return Groups::tr("Resistance");}

        QList<const Unit *> Resistance::units() const {
            static const QList<const Unit *> list {
                &ohm(),
                &kohm(),
                &Mohm(),
                &Gohm(),
                &mohm(),
                &uohm()
            };
            return list;
        }

        const Resistance &Resistance::group() {
            static const Resistance gr;
            return gr;
        }

        class ResistanceUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Resistance::ohm();}
        };


        const Unit &Resistance::ohm() {
            static const class ResistanceUnit_Ohm : public ResistanceUnit {
            public:
                QString id() const override {return QStringLiteral("resistance_ohm");}
                QString symbol() const override{return Groups::tr("Ω");}
                QString asciiSymbol() const override {return Groups::tr("Ohm");}
                QString displayFullName() const override {return Groups::tr("ohm");}
                QString fullPluralName() const override {return Groups::tr("ohms");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &Resistance::kohm() {
            static const class ResistanceUnit_kohm : public ResistanceUnit {
            public:
                QString id() const override {return QStringLiteral("resistance_kohm");}
                QString symbol() const override{return Groups::tr("kΩ");}
                QString asciiSymbol() const override {return Groups::tr("kOhm");}
                QString displayFullName() const override {return Groups::tr("kiloohm");}
                QString fullPluralName() const override {return Groups::tr("kiloohms");}
                double multipler() const override {return 1e3;}
            } u;
            return u;
        }

        const Unit &Resistance::Mohm() {
            static const class ResistanceUnit_Mohm : public ResistanceUnit {
            public:
                QString id() const override {return QStringLiteral("resistance_Mohm");}
                QString symbol() const override{return Groups::tr("MΩ");}
                QString asciiSymbol() const override {return Groups::tr("MOhm");}
                QString displayFullName() const override {return Groups::tr("megaohm");}
                QString fullPluralName() const override {return Groups::tr("megaohms");}
                double multipler() const override {return 1e6;}
            } u;
            return u;
        }

        const Unit &Resistance::Gohm() {
            static const class ResistanceUnit_Gohm : public ResistanceUnit {
            public:
                QString id() const override {return QStringLiteral("resistance_Gohm");}
                QString symbol() const override{return Groups::tr("GΩ");}
                QString asciiSymbol() const override {return Groups::tr("GOhm");}
                QString displayFullName() const override {return Groups::tr("gigaohm");}
                QString fullPluralName() const override {return Groups::tr("gigaohms");}
                double multipler() const override {return 1e9;}
            } u;
            return u;
        }

        const Unit &Resistance::mohm() {
            static const class ResistanceUnit_mohm : public ResistanceUnit {
            public:
                QString id() const override {return QStringLiteral("resistance_mohm");}
                QString symbol() const override{return Groups::tr("mΩ");}
                QString asciiSymbol() const override {return Groups::tr("mOhm");}
                QString displayFullName() const override {return Groups::tr("milliohm");}
                QString fullPluralName() const override {return Groups::tr("milliohms");}
                double multipler() const override {return 1e-3;}
            } u;
            return u;
        }

        const Unit &Resistance::uohm() {
            static const class ResistanceUnit_uohm : public ResistanceUnit {
            public:
                QString id() const override {return QStringLiteral("resistance_uohm");}
                QString symbol() const override{return Groups::tr("µΩ");}
                QString asciiSymbol() const override {return Groups::tr("uOhm");}
                QString displayFullName() const override {return Groups::tr("microohm");}
                QString fullPluralName() const override {return Groups::tr("microohms");}
                double multipler() const override {return 1e-6;}
            } u;
            return u;
        }
    }
}
