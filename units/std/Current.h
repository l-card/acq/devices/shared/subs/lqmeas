#ifndef LQMEAS_UNITS_CURRENT_H
#define LQMEAS_UNITS_CURRENT_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Current : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Current &group();

            static const Unit &A();  /* амперы */
            static const Unit &mA(); /* милиамперы */
            static const Unit &uA(); /* микроамперы */
            static const Unit &kA(); /* килоамперы */
        };
    }
}


#endif // LQMEAS_UNITS_CURRENT_H
