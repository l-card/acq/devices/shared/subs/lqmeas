#ifndef LQMEAS_UNITS_PRESSURE_H
#define LQMEAS_UNITS_PRESSURE_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Pressure : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Pressure &group();


            static const Unit &Pa();  /* паскаль */
            static const Unit &hPa(); /* гектопаскаль */
            static const Unit &kPa(); /* килопаскаль */
            static const Unit &MPa(); /* мегапаскаль */

            static const Unit &bar();  /* бар */
            static const Unit &mbar(); /* миллибар */

            /*  Для at и psi есть абсолютные измерения (ata/ата, psia) -
                относительно вакуума, а также избыточные (atg/ати, psig) - относительно
                атмосферного давления. Сами единицы эквивалентны, но меряют относительно
                разного уровня. Т.к. атмосферное давление может меняться, то перевод
                автоматом не сделаешь. */
            static const Unit &at(); /* техническая атмосфера */
            static const Unit &ata(); /* техническая атмосфера (абсолютное давление) */
            static const Unit &atg(); /* техническая атмосфера (избыточное давление) */
            static const Unit &atm(); /* физическая атмосфера */

            static const Unit &kgf_cm2(); /* кгс/см^2 */
            static const Unit &kgf_m2();  /* кгс/м^2 */

            static const Unit &psi(); /* фунт на дйюм ^2 */
            static const Unit &psia(); /* фунт на дйюм ^2 (абсолютное давление)  */
            static const Unit &psig(); /* фунт на дйюм ^2 (избыточное давление)  */

            static const Unit &mmHg(); /* мм ртутного столба */
            static const Unit &mmwg(); /* мм водного столба */

            static constexpr double atm_to_p_mul = 101325;
            static constexpr double psi_to_p_mul = 6894.76;
        };
    }
}


#endif // LQMEAS_UNITS_PRESSURE_H
