#include "InformationSpeed.h"
#include "UnitGroups.h"
#include "UnitPrefixes.h"
#include "lqmeas/units/Unit.h"

namespace LQMeas {
    namespace Units {
        QString InformationSpeed::id() const {return QStringLiteral("unitgroup_informationspeed");}
        QString InformationSpeed::displayName() const {return Groups::tr("Information Speed");}

        QList<const Unit *> InformationSpeed::units() const {
            static const QList<const Unit *> list {
                &bit_s(),
                &kbit_s(),
                &Mbit_s(),
                &Gbit_s(),
                &byte_s(),
                &kbyte_s(),
                &Mbyte_s(),
                &Gbyte_s(),
                &word_s(),
                &kword_s(),
                &Mword_s(),
                &Gword_s(),
                &byte_h(),
                &kbyte_h(),
                &Mbyte_h(),
                &Gbyte_h(),
            };
            return list;
        }

        const InformationSpeed &InformationSpeed::group() {
            static const InformationSpeed gr;
            return gr;
        }


        class InformationSpeedUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return InformationSpeed::bit_s();}
        };

        const Unit &InformationSpeed::bit_s() {
            static const class InformationSpeedUnit_bit_s : public InformationSpeedUnit {
            public:
                QString id() const override {return QStringLiteral("informspeed_bit_s");}
                QString symbol() const override {return Groups::tr("bit/s");}
                QString displayFullName() const override {return Groups::tr("bit per second");}
                QString fullPluralName() const override {return Groups::tr("bits per second");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &InformationSpeed::kbit_s() {
            static const class InformationSpeedUnit_kbit_s : public UnitPrefixes::kilo {
            public:
                QString id() const override {return QStringLiteral("informspeed_kbit_s");}
                const Unit &prefixSrcUnit() const override {return bit_s();}
            } u;
            return u;
        }

        const Unit &InformationSpeed::Mbit_s() {
            static const class InformationSpeedUnit_Mbit_s : public UnitPrefixes::Mega {
            public:
                QString id() const override {return QStringLiteral("informspeed_Mbit_s");}
                const Unit &prefixSrcUnit() const override {return bit_s();}
            } u;
            return u;
        }

        const Unit &InformationSpeed::Gbit_s() {
            static const class InformationSpeedUnit_Gbit_s : public UnitPrefixes::Giga {
            public:
                QString id() const override {return QStringLiteral("informspeed_Gbit_s");}
                const Unit &prefixSrcUnit() const override {return bit_s();}
            } u;
            return u;
        }

        const Unit &InformationSpeed::byte_s() {
            static const class InformationSpeedUnit_byte_s : public InformationSpeedUnit {
            public:
                QString id() const override {return QStringLiteral("informspeed_byte_s");}
                QString symbol() const override {return Groups::tr("B/s");}
                QString displayFullName() const override {return Groups::tr("byte per second");}
                QString fullPluralName() const override {return Groups::tr("bytes per second");}
                double multipler() const override {return 8;}
            } u;
            return u;
        }

        const Unit &InformationSpeed::kbyte_s() {
            static const class InformationSpeedUnit_kbyte_s : public UnitPrefixes::kilo {
            public:
                QString id() const override {return QStringLiteral("informspeed_kbyte_s");}
                const Unit &prefixSrcUnit() const override {return byte_s();}
            } u;
            return u;
        }

        const Unit &InformationSpeed::Mbyte_s() {
            static const class InformationSpeedUnit_Mbyte_s : public UnitPrefixes::Mega {
            public:
                QString id() const override {return QStringLiteral("informspeed_Mbyte_s");}
                const Unit &prefixSrcUnit() const override {return byte_s();}
            } u;
            return u;
        }

        const Unit &InformationSpeed::Gbyte_s() {
            static const class InformationSpeedUnit_Gbyte_s : public UnitPrefixes::Giga {
            public:
                QString id() const override {return QStringLiteral("informspeed_Gbyte_s");}
                const Unit &prefixSrcUnit() const override {return byte_s();}
            } u;
            return u;
        }

        const Unit &InformationSpeed::word_s() {
            static const class InformationSpeedUnit_word_s : public InformationSpeedUnit {
            public:
                QString id() const override {return QStringLiteral("informspeed_word_s");}
                QString symbol() const override {return Groups::tr("words/s");}
                QString displayFullName() const override {return Groups::tr("word per second");}
                QString fullPluralName() const override {return Groups::tr("words per second");}
                double multipler() const override {return 8*4;}
            } u;
            return u;
        }

        const Unit &InformationSpeed::kword_s() {
            static const class InformationSpeedUnit_kword_s : public UnitPrefixes::kilo {
            public:
                QString id() const override {return QStringLiteral("informspeed_kword_s");}
                const Unit &prefixSrcUnit() const override {return word_s();}
            } u;
            return u;
        }

        const Unit &InformationSpeed::Mword_s() {
            static const class InformationSpeedUnit_Mword_s : public UnitPrefixes::Mega {
            public:
                QString id() const override {return QStringLiteral("informspeed_Mword_s");}
                const Unit &prefixSrcUnit() const override {return word_s();}
            } u;
            return u;
        }

        const Unit &InformationSpeed::Gword_s() {
            static const class InformationSpeedUnit_Gword_s : public UnitPrefixes::Giga {
            public:
                QString id() const override {return QStringLiteral("informspeed_Gword_s");}
                const Unit &prefixSrcUnit() const override {return word_s();}
            } u;
            return u;
        }

        const Unit &InformationSpeed::byte_h() {
            static const class InformationSpeedUnit_byte_h : public InformationSpeedUnit {
            public:
                QString id() const override {return QStringLiteral("informspeed_byte_h");}
                QString symbol() const override {return Groups::tr("B/h");}
                QString displayFullName() const override {return Groups::tr("byte per hour");}
                QString fullPluralName() const override {return Groups::tr("bytes per hour");}
                double multipler() const override {return 8./3600;}
            } u;
            return u;
        }

        const Unit &InformationSpeed::kbyte_h() {
            static const class InformationSpeedUnit_kbyte_h : public UnitPrefixes::kilo {
            public:
                QString id() const override {return QStringLiteral("informspeed_kbyte_h");}
                const Unit &prefixSrcUnit() const override {return byte_h();}
            } u;
            return u;
        }

        const Unit &InformationSpeed::Mbyte_h() {
            static const class InformationSpeedUnit_Mbyte_h : public UnitPrefixes::Mega {
            public:
                QString id() const override {return QStringLiteral("informspeed_Mbyte_h");}
                const Unit &prefixSrcUnit() const override {return byte_h();}
            } u;
            return u;
        }

        const Unit &InformationSpeed::Gbyte_h() {
            static const class InformationSpeedUnit_Gbyte_h : public UnitPrefixes::Giga {
            public:
                QString id() const override {return QStringLiteral("informspeed_Gbyte_h");}
                const Unit &prefixSrcUnit() const override {return byte_h();}
            } u;
            return u;
        }
    }
}
