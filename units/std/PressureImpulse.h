#ifndef LQMEAS_UNITS_PRESSUREIMPULSE_H
#define LQMEAS_UNITS_PRESSUREIMPULSE_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class PressureImpulse : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const PressureImpulse &group();


            static const Unit &Pa_s();      /* Паскаль - секунда */
            static const Unit &Pa_ms();     /* Паскаль - милисекунда */
            static const Unit &Pa_minute();    /* Паскаль - минута */

            static const Unit &bar_s();     /* бар - секунда */
            static const Unit &bar_ms();    /* бар - милисекунда */
            static const Unit &bar_minute();   /* бар - мнута */

            static const Unit &at_s();      /* техническая атмосфера - секунда*/
            static const Unit &at_ms();     /* техническая атмосфера - милисекунда*/
            static const Unit &at_minute();    /* техническая атмосфера - минута*/

            static const Unit &ata_s();      /* техническая атмосфера - секунда (абсолютное давление) */
            static const Unit &ata_ms();     /* техническая атмосфера - милисекунда (абсолютное давление)*/
            static const Unit &ata_minute(); /* техническая атмосфера - минута (абсолютное давление)*/

            static const Unit &atg_s();      /* техническая атмосфера - секунда (абсолютное давление) */
            static const Unit &atg_ms();     /* техническая атмосфера - милисекунда (абсолютное давление)*/
            static const Unit &atg_minute(); /* техническая атмосфера - минута (абсолютное давление)*/

            static const Unit &atm_s();     /* физическая атмосфера - секунда */
            static const Unit &atm_ms();    /* физическая атмосфера - милисекунда */
            static const Unit &atm_minute();   /* физическая атмосфера - минута */

            static const Unit &mmHg_s();    /* мм ртутного столба  - секунда */
            static const Unit &mmHg_ms();   /* мм ртутного столба  - милисекунда */
            static const Unit &mmHg_minute();  /* мм ртутного столба  - минута */

            static const Unit &kgf_cm2_s(); /* кгс/см² - сек */
            static const Unit &kgf_cm2_ms(); /* кгс/см² - милисекунда */
            static const Unit &kgf_cm2_minute(); /* кгс/см² - мин */
        };
    }
}

#endif // LQMEAS_UNITS_PRESSUREIMPULSE_H
