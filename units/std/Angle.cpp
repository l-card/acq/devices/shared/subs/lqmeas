#include "Angle.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"
#include <QtMath>
namespace LQMeas {
    namespace Units {
        QString Angle::id() const {return QStringLiteral("unitgroup_angle");}
        QString Angle::displayName() const {return Groups::tr("Angle");}

        QList<const Unit *> Angle::units() const {
            static const QList<const Unit *> list {
                &degree(),
                &rad()
            };
            return list;
        }

        const Angle &Angle::group() {
            static const Angle gr;
            return gr;
        }

        class AngleUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Angle::degree();}
        };


        const Unit &Angle::degree() {
            static const class AngleUnit_Degree : public AngleUnit {
            public:
                QString id() const override {return QStringLiteral("angle_degree");}
                QString symbol() const override {return Groups::tr("°");}
                QString asciiSymbol() const override {return Groups::tr("");}
                QString displayFullName() const override {return Groups::tr("degree");}
                QString fullPluralName() const override {return Groups::tr("degrees");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &Angle::rad() {
            static const class AngleUnit_Rad : public AngleUnit {
            public:
                QString id() const override {return QStringLiteral("angle_rad");}
                QString symbol() const override {return Groups::tr("rad");}
                QString displayFullName() const override {return Groups::tr("radian");}
                QString fullPluralName() const override {return Groups::tr("radians");}
                double multipler() const override {return 360./(2.*M_PI);}
            } u;
            return u;
        }
    }
}
