#include "Power.h"
#include "Energy.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"


namespace LQMeas {
    namespace Units {
        QString Power::id() const {return QStringLiteral("unitgroup_power");}
        QString Power::displayName() const {return Groups::tr("Power");}

        QList<const Unit *> Power::units() const {
            static const QList<const Unit *> list {
                &W(),
                &kW(),
                &MW(),
                &kgf_m_s(),
                &eg_s(),
                &cal_s(),
                &kcal_s(),

                &va(),
                &kva(),
                &Mva(),

                &var(),
                &kvar(),
                &Mvar()
            };
            return list;
        }

        const Power &Power::group() {
            static const Power gr;
            return gr;
        }


        class PowerUnit : public Unit {
        public:
            const Unit &baseUnit() const override {return Power::W();}
        };


        const Unit &Power::W() {
            static const class PowerUnit_W : public PowerUnit {
            public:
                QString id() const override {return QStringLiteral("power_w");}
                QString symbol() const override {return Groups::tr("W");}
                QString displayFullName() const override {return Groups::tr("watt");}
                QString fullPluralName() const override {return Groups::tr("watts");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &Power::kW() {
            static const class PowerUnit_kW : public PowerUnit {
            public:
                QString id() const override {return QStringLiteral("power_kw");}
                QString symbol() const override {return Groups::tr("kW");}
                QString displayFullName() const override {return Groups::tr("kilowatt");}
                QString fullPluralName() const override {return Groups::tr("kilowatts");}
                double multipler() const override {return 1e3;}
            } u;
            return u;
        }

        const Unit &Power::MW() {
            static const class PowerUnit_MW : public PowerUnit {
            public:
                QString id() const override {return QStringLiteral("power_Mw");}
                QString symbol() const override {return Groups::tr("MW");}
                QString displayFullName() const override {return Groups::tr("megawatt");}
                QString fullPluralName() const override {return Groups::tr("megawatts");}
                double multipler() const override {return 1e6;}
            } u;
            return u;
        }

        const Unit &Power::kgf_m_s() {
            static const class PowerUnit_kgf_m_s : public PowerUnit {
            public:
                QString id() const override {return QStringLiteral("power_kgf_m_s");}
                QString symbol() const override {return Groups::tr("kgf⋅m/s");}
                QString displayFullName() const override {return Groups::tr("kilogram-force-metre per second");}
                QString fullPluralName() const override {return Groups::tr("kilogram-force-metres per second");}
                double multipler() const override {return Energy::cal_to_j_mul * 1e3;}
            } u;
            return u;
        }

        const Unit &Power::eg_s() {
            static const class PowerUnit_eg_s : public PowerUnit {
            public:
                QString id() const override {return QStringLiteral("power_eg_s");}
                QString symbol() const override {return Groups::tr("erg/s");}
                QString displayFullName() const override {return Groups::tr("erg per second");}
                QString fullPluralName() const override {return Groups::tr("ergs per second");}
                double multipler() const override {return Energy::erg_to_j_mul;}
            } u;
            return u;
        }

        const Unit &Power::cal_s() {
            static const class PowerUnit_cal_s : public PowerUnit {
            public:
                QString id() const override {return QStringLiteral("power_cal_s");}
                QString symbol() const override {return Groups::tr("cal/s");}
                QString displayFullName() const override {return Groups::tr("calorie per second");}
                QString fullPluralName() const override {return Groups::tr("calories per second");}
                double multipler() const override {return Energy::cal_to_j_mul;}
            } u;
            return u;
        }

        const Unit &Power::kcal_s() {
            static const class PowerUnit_kcal_s : public PowerUnit {
            public:
                QString id() const override {return QStringLiteral("power_kcal_s");}
                QString symbol() const override {return Groups::tr("kcal/s");}
                QString displayFullName() const override {return Groups::tr("kilocalorie per second");}
                QString fullPluralName() const override {return Groups::tr("kilocalories per second");}
                double multipler() const override {return Energy::cal_to_j_mul * 1e3;}
            } u;
            return u;
        }

        const Unit &Power::va() {
            static const class PowerUnit_va : public PowerUnit {
            public:
                QString id() const override {return QStringLiteral("power_va");}
                QString symbol() const override {return Groups::tr("V⋅A");}
                QString displayFullName() const override {return Groups::tr("volt-ampere");}
                QString fullPluralName() const override {return Groups::tr("volt-amperes");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &Power::kva() {
            static const class PowerUnit_kva : public PowerUnit {
            public:
                QString id() const override {return QStringLiteral("power_kva");}
                QString symbol() const override {return Groups::tr("kV⋅A");}
                QString displayFullName() const override {return Groups::tr("kilovolt-ampere");}
                QString fullPluralName() const override {return Groups::tr("kilovolt-amperes");}
                double multipler() const override {return 1e3;}
            } u;
            return u;
        }

        const Unit &Power::Mva() {
            static const class PowerUnit_Mva : public PowerUnit {
            public:
                QString id() const override {return QStringLiteral("power_Mva");}
                QString symbol() const override {return Groups::tr("MV⋅A");}
                QString displayFullName() const override {return Groups::tr("megavolt-ampere");}
                QString fullPluralName() const override {return Groups::tr("megavolt-amperes");}
                double multipler() const override {return 1e6;}
            } u;
            return u;
        }

        const Unit &Power::var() {
            static const class PowerUnit_var : public PowerUnit {
            public:
                QString id() const override {return QStringLiteral("power_var");}
                QString symbol() const override {return Groups::tr("var");}
                QString displayFullName() const override {return Groups::tr("volt-ampere reactive");}
                QString fullPluralName() const override {return Groups::tr("volt-amperes reactive");}
                double multipler() const override {return 1;}
            } u;
            return u;
        }

        const Unit &Power::kvar() {
            static const class PowerUnit_kvar : public PowerUnit {
            public:
                QString id() const override {return QStringLiteral("power_kvar");}
                QString symbol() const override {return Groups::tr("kvar");}
                QString displayFullName() const override {return Groups::tr("kilovolt-ampere reactive");}
                QString fullPluralName() const override {return Groups::tr("kilovolt-amperes reactive");}
                double multipler() const override {return 1e3;}
            } u;
            return u;
        }

        const Unit &Power::Mvar() {
            static const class PowerUnit_kvar : public PowerUnit {
            public:
                QString id() const override {return QStringLiteral("power_Mvar");}
                QString symbol() const override {return Groups::tr("Mvar");}
                QString displayFullName() const override {return Groups::tr("megvolt-ampere reactive");}
                QString fullPluralName() const override {return Groups::tr("megavolt-amperes reactive");}
                double multipler() const override {return 1e6;}
            } u;
            return u;
        }

    }
}
