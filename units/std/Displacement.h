#ifndef LQMEAS_UNITS_DISPLACEMENT_H
#define LQMEAS_UNITS_DISPLACEMENT_H


#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Displacement : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Displacement &group();


            static const Unit &m();  /* метры */
            static const Unit &cm(); /* сантиметры */
            static const Unit &mm(); /* милиметры */
            static const Unit &um(); /* микрометры */
            static const Unit &nm(); /* нанометры */
            static const Unit &km(); /* километры */
            static const Unit &inch(); /* дюйм */

            static constexpr double inch_to_m_mult = 0.0254;
        };
    }
}


#endif // LQMEAS_UNITS_DISPLACEMENT_H
