#ifndef ELECTRICCHARGE_H
#define ELECTRICCHARGE_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class ElectricCharge : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const ElectricCharge &group();


            static const Unit &C();         /* кулон */
            static const Unit &pC();        /* пикакулон */
        };
    }
}


#endif // ELECTRICCHARGE_H
