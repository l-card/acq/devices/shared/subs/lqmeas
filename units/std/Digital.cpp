#include "Digital.h"
#include "UnitGroups.h"
#include "lqmeas/units/Unit.h"

namespace LQMeas {
    namespace Units {
        QString Digital::id() const {return QStringLiteral("unitgroup_digital");}
        QString Digital::displayName() const {return Groups::tr("Digital");}

        QList<const Unit *> Digital::units() const {
            static QList<const Unit *> list {
                &codes()
            };
            return list;
        }

        const Digital &Digital::group() {
            static const Digital gr;
            return gr;
        }

        const Unit &Digital::codes() {
            static const class DigitalUnit_codes : public Unit {
            public:
                QString id() const override {return QStringLiteral("digital_codes");}
                QString symbol() const override {return QString{};}
                QString displayFullName() const override {return Groups::tr("digital code");}
                QString fullPluralName() const override {return Groups::tr("digital codes");}
                double multipler() const override {return 1.;}
                const Unit &baseUnit() const override {return Digital::codes();}
            } u;
            return u;
        }
    }
}
