#ifndef LQMEAS_UNITS_ENERGY_H
#define LQMEAS_UNITS_ENERGY_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Energy : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Energy &group();

            static const Unit &J();  /* джоуль */
            static const Unit &kJ(); /* килоджоуль */
            static const Unit &kgfm(); /* кгс*м */
            static const Unit &erg(); /* эрг */
            static const Unit &cal(); /* калории */
            static const Unit &kcal(); /* килокалории */


            static const Unit &Wh(); /*  ватт в час */
            static const Unit &kWh(); /* киловатт в час */
            static const Unit &MWh(); /* мегаватт в час */

            static const Unit &vah();  /* вольт-ампер  в час */
            static const Unit &kvah(); /* киловольт-ампер  в час */
            static const Unit &Mvah(); /* мегавольт-ампер  в час */

            static const Unit &varh();  /* вар в час */
            static const Unit &kvarh(); /* киловар в час */
            static const Unit &Mvarh(); /* киловар в час */




            static constexpr double cal_to_j_mul = 4.1868;
            static constexpr double erg_to_j_mul = 1e-7;
            static constexpr double wh_to_j_mul = 3.6 * 1e3;
        };
    }
}

#endif // LQMEAS_UNITS_ENERGY_H
