#ifndef LQMEAS_UNITS_INFORMATION_H
#define LQMEAS_UNITS_INFORMATION_H

#include "lqmeas/units/UnitGroup.h"

namespace LQMeas {
    namespace Units {
        class Information : public UnitGroup {
        public:
            QString id() const override;
            QString displayName() const override;
            QList<const Unit *> units() const override;

            static const Information &group();

            static const Unit &bit(); /* биты */
            static const Unit &kbit(); /* килобиты */
            static const Unit &Mbit(); /* мегабиты */
            static const Unit &Gbit(); /* гигабиты */

            static const Unit &byte(); /* байты */
            static const Unit &kbyte(); /* килобайты */
            static const Unit &Mbyte(); /* мегабайты */
            static const Unit &Gbyte(); /* гигабайты */
        };
    }
}

#endif // LQMEAS_UNITS_INFORMATION_H
