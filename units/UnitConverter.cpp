#include "UnitConverter.h"
#include "Unit.h"

namespace LQMeas {
    bool UnitConverter::canConvert(const Unit &src_unit, const Unit &dst_unit) {
        return src_unit.baseUnit() == dst_unit.baseUnit();
    }

    double UnitConverter::convert(double val, const Unit &src_unit, const Unit &dst_unit) {
        if (!canConvert(src_unit, dst_unit))
            return qSNaN();
        return UnitConverter(src_unit, dst_unit).convert(val);
    }

    UnitConverter::UnitConverter(const Unit &src_unit, const Unit &dst_unit) {
        if (canConvert(src_unit, dst_unit)) {
            m_mult = src_unit.multipler() / dst_unit.multipler();
            m_addit = (src_unit.zeroPoint() - dst_unit.zeroPoint()) / dst_unit.multipler();
        } else {
            m_mult = 1;
            m_addit = 0;
        }
    }

    UnitConverter::UnitConverter(const UnitConverter &conv1, const UnitConverter &conv2) {
        m_mult = conv1.m_mult * conv2.m_mult;
        m_addit = conv1.m_addit * conv2.m_mult + conv2.m_addit;
    }

    double UnitConverter::convert(double val) const {
        return  val * m_mult + m_addit;
    }

    double UnitConverter::revert(double val) const {
        return  val / m_mult;
    }
}
