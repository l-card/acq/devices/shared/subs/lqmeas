#ifndef UNITSSTORAGE_H
#define UNITSSTORAGE_H

#include <QObject>

class QJsonObject;
class LQError;

namespace LQMeas {
    class Unit;


    class UnitsStorage  : public QObject {
        Q_OBJECT
    public:
        void save(QJsonObject &cfgObj, const Unit &unit) const;
        const LQMeas::Unit &load(const QJsonObject &cfgObj, const Unit &defaultUnit) const;

        const Unit &get(const QString &id, LQError &err) const;

        static UnitsStorage &instance();

        QList<const Unit *> analogUnits() const;
        QList<const Unit *> allUnits() const;

        UnitsStorage(QObject *parent = nullptr);
    private:

    };
}

#endif // UNITSSTORAGE_H
