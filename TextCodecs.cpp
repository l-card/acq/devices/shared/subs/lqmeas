#include "TextCodecs.h"
#include <QTextCodec>

namespace LQMeas {
    QTextCodec &TextCodecs::cp1251() {
        static QTextCodec &codec {*QTextCodec::codecForName("Windows-1251")};
        return codec;
    }
}
