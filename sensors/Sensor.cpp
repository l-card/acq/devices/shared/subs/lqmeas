#include "Sensor.h"
#include "LQError.h"

namespace LQMeas {
    Sensor::Sensor(const SensorTypeInfo *typeInfo) : m_typeInfo(typeInfo) {

    }

    void Sensor::setSensorTypeInfo(const SensorTypeInfo *typeInfo) {
        if (typeInfo != m_typeInfo) {
            m_typeInfo = typeInfo;
            Q_EMIT sensorTypeChanged();
        }
    }

    void Sensor::sensorAssignDevConfig(DeviceConfig *devCfg, int adcCh) {
        if ((m_devcfg != devCfg) || (m_adcCh != adcCh)) {
            DeviceConfig *prevCfg = m_devcfg;
            int prevCh = m_adcCh;
            m_devcfg = devCfg;
            m_adcCh = adcCh;
            protSensorDevConfigUpdate(devCfg, adcCh, prevCfg, prevCh);
        }
    }

    void Sensor::sensorProcStart(LQError &err) {
        protSensorProcStart(err);
        if (err.isSuccess()) {
            m_proc_run = true;
        }
    }

    void Sensor::sensorProcStop(LQError &err) {
        protSensorProcStop(err);
        if (err.isSuccess()) {
            m_proc_run = false;
        }
    }

    void Sensor::sensorProcData(QVector<double> &data, LQError &err) {
        protSensorProcData(data, err);
    }
}
