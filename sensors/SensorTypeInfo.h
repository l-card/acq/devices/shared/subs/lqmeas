#ifndef LQMEAS_SENSORTYPEINFO_H
#define LQMEAS_SENSORTYPEINFO_H


#include <QString>
#include <QList>

namespace LQMeas {
    class Unit;

    class SensorTypeInfo {
    public:
        virtual ~SensorTypeInfo();
        /* название типа датчика (без модификаций) */
        virtual QString sensorTypeName() const = 0;
        /* название датчика с учетом модификаций */
        virtual QString sensorModelName() const {return  sensorTypeName();}

        virtual int sensorRangesCnt() const {return  1;}
        virtual int sensorModesCnt() const {return  1;}
        virtual double sensorPhysMaxVal(int range_num = 0, int mode_num = 0) const = 0;
        virtual double sensorPhysMinVal(int range_num = 0, int mode_num = 0) const = 0;
        virtual const Unit &sensorPhysUnit(int mode_num = 0) const = 0;
        virtual double sensorElectricalMaxVal() const = 0;
        virtual double sensorElectricalMinVal() const = 0;
        virtual const Unit &sensorElectricalUnit() const = 0;

        /* список моделей, совместимых с данной (включая и саму данную модель) */
        virtual QList<const SensorTypeInfo *> compatibleModels() const = 0;
    };
}

#endif // LQMEAS_SENSORTYPEINFO_H
