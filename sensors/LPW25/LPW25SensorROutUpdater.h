#ifndef LQMEAS_LPW25SENSORROUTUPDATER_H
#define LQMEAS_LPW25SENSORROUTUPDATER_H

#include "lqmeas/ifaces/in/adc/icp/DevAdcICPSensorRoutUpdater.h"

namespace LQMeas {
    class LPW25;

    class LPW25SensorROutUpdater : public DevAdcICPSensorRoutUpdater {
        Q_OBJECT
    public:
        LPW25SensorROutUpdater(LPW25 *lpw);

        double rOut(double isrc_ma) override;
    protected:
        void protISrcUpdate(double isrc_ma) override;
    private:
        LPW25 *m_lpw;
    };
}

#endif // LQMEAS_LPW25SENSORROUTUPDATER_H
