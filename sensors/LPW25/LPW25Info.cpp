#include "LPW25Info.h"
#include "LPW25TypeInfo.h"

namespace LQMeas {
    LPW25Info::LPW25Info(const TLPW25_INFO &info) :
        m_info{info},
        m_type{LPW25TypeInfo::tedsModelInfo(static_cast<int>(info.ModelID))},
        m_serial{QString::number(info.SerialNumber)} {
    }

    LPW25Info::LPW25Info(const LPW25TypeInfo *type, const QString &serial) :
        m_type{type}, m_serial{serial} {

        LPW25_InfoStdFill(&m_info, static_cast<DWORD>(type->tedsModelNumber()));
    }

    void LPW25Info::setSerail(const QString &serial) {
        m_serial = serial;
    }
}
