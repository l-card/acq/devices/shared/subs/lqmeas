#ifndef LQMEAS_LPW25_H
#define LQMEAS_LPW25_H

#include "ltr/include/lpw25api.h"
#include "lqmeas/sensors/Sensor.h"
#include "lqmeas/ifaces/teds/TedsTransducer.h"

namespace LQMeas {
    class LPW25Info;
    class LPW25TypeInfo;
    class LPW25SensorROutUpdater;

    class LPW25 : public Sensor, public TedsTransducer {
        Q_OBJECT
    public:
        explicit LPW25(const LPW25TypeInfo *type);
        explicit LPW25(const LPW25 &lpw25);
        ~LPW25() override;


        TedsTransducer *teds() override {return  this;}

        Sensor *clone() const override;

        const SensorTypeInfo *tedsCheckType(const TedsBasicInfo &info) const override;
        void tedsParseData(TedsDataRdStream &rdData, LQError &err) override;

        double sensitivity() const {return  m_ctx.Sens;}

        const LPW25Info &info() const {return  *m_info;}

        void setInfo(const LPW25 &lpw25);
        void setInfo(const LPW25Info &info);
        void setType(const LPW25TypeInfo &type);
        void setSerial(QString serial);

    public Q_SLOTS:
        void setSensitivity(double sens);
    protected:
        void protSensorDevConfigUpdate(DeviceConfig *devCfg, int adcCh, DeviceConfig *prevDevCfg, int prevCh) override;
        void protSensorProcStart(LQError &err) override;
        void protSensorProcStop(LQError &err) override;
        void protSensorProcData(QVector<double> &data, LQError &err) override;
    private Q_SLOTS:
        void updateChConfig();
    private:

        void getError(int errCode, LQError &err);
        QString errorString(int errCode);

        TLPW25_PROC_CTX m_ctx;
        LPW25Info *m_info;
        LPW25SensorROutUpdater *m_rout;

    };
}

#endif // LQMEAS_LPW25_H
