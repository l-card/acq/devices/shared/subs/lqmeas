#include "LPW25.h"
#include "LPW25TypeInfo.h"
#include "LPW25Info.h"
#include "LPW25SensorROutUpdater.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/devs/LTR/LTRNativeError.h"
#include "lqmeas/devs/DeviceConfig.h"
#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "lqmeas/ifaces/teds/info/TedsBasicInfo.h"
#include "lqmeas/ifaces/teds/TedsDataRdStream.h"
#include "lqmeas/ifaces/in/adc/icp/DevAdcICPConfig.h"

namespace LQMeas {
    LPW25::LPW25(const LPW25TypeInfo *type) : Sensor{type}, m_info{new LPW25Info{type}} {
        LPW25_ProcessInit(&m_ctx);

        m_ctx.Sens = m_info->rawInfo().CalInfo.CalCoef.Sens;
        m_rout = new LPW25SensorROutUpdater(this);
    }

    LPW25::LPW25(const LPW25 &lpw25) : Sensor{&lpw25.sensorTypeInfo()},
        m_info{new LPW25Info{*lpw25.m_info}} {

        LPW25_ProcessInit(&m_ctx);
        LPW25_ProcessCopy(&m_ctx, &lpw25.m_ctx);

        m_rout = new LPW25SensorROutUpdater{this};
    }

    LPW25::~LPW25() {
        delete m_info;
    }

    Sensor *LPW25::clone() const {
        return new LPW25(*this);
    }

    const SensorTypeInfo *LPW25::tedsCheckType(const TedsBasicInfo &info) const     {
        const SensorTypeInfo *ret {nullptr};
        if (info.manufacturerId() == LTEDS_LCARD_MANUFACTURER_ID) {
            ret = LPW25TypeInfo::tedsModelInfo(info.modelNumber());
        }
        return  ret;
    }

    void LPW25::tedsParseData(TedsDataRdStream &rdData, LQError &err) {
        BOOLEAN allParsed;
        TLPW25_INFO info;
        getError(LPW25_TEDSDecode(reinterpret_cast<const BYTE*>(rdData.rawData().data()),
                                  static_cast<DWORD>(rdData.rawData().size()),
                                  0,
                                  &m_ctx,
                                  &info,
                                  &allParsed), err);
        if (err.isSuccess()) {
            setInfo(LPW25Info(info));
        }
    }

    void LPW25::setInfo(const LPW25 &lpw25) {
        setInfo(lpw25.info());
        LPW25_ProcessCopy(&m_ctx, &lpw25.m_ctx);
    }


    void LPW25::setInfo(const LPW25Info &info) {        
        const LPW25TypeInfo &prevType = m_info->type();
        delete m_info;
        m_info = new LPW25Info(info);
        if (&prevType != &m_info->type()) {
            setType(m_info->type());
        }
    }

    void LPW25::setType(const LPW25TypeInfo &type) {
        if (&m_info->type() != &type) {
            setInfo(LPW25Info(&type));
        } else {
            setSensorTypeInfo(&type);
        }
    }

    void LPW25::setSerial(QString serial) {
        m_info->setSerail(serial);
    }

    void LPW25::setSensitivity(double sens) {
        m_ctx.Sens = sens;
    }

    void LPW25::protSensorDevConfigUpdate(DeviceConfig *devCfg, int adcCh,
                                          DeviceConfig *prevDevCfg, int prevCh)  {

        if (prevDevCfg) {
            if (prevDevCfg->adcConfig()) {
                disconnect(prevDevCfg->adcConfig(), &DevAdcConfig::adcChConfiguredFreqChanged,
                        this, &LPW25::updateChConfig);
                disconnect(prevDevCfg->adcConfig(), &DevAdcConfig::updated,  this, &LPW25::updateChConfig);
            }
            if (prevDevCfg->adcICPConfig()) {
                prevDevCfg->adcICPConfig()->adcICPChSetSensorROutUpdater(prevCh, nullptr);
            }
        }

        if (devCfg) {
            if (devCfg->adcConfig()) {
                connect(devCfg->adcConfig(), &DevAdcConfig::adcChConfiguredFreqChanged,
                        this, &LPW25::updateChConfig);
                connect(devCfg->adcConfig(), &DevAdcConfig::updated,
                        this, &LPW25::updateChConfig);

                updateChConfig();
            }
            if (devCfg->adcICPConfig()) {
                devCfg->adcICPConfig()->adcICPChSetSensorROutUpdater(adcCh, m_rout);
            }
        }
    }

    void LPW25::protSensorProcStart(LQError &err) {
        getError(LPW25_ProcessStart(&m_ctx), err);
    }

    void LPW25::protSensorProcStop(LQError &err) {
        getError(LPW25_ProcessStop(&m_ctx), err);
    }

    void LPW25::protSensorProcData(QVector<double> &data, LQError &err) {
        getError(LPW25_ProcessData(&m_ctx, data.data(), data.data(),
                                   static_cast<DWORD>(data.size()),
                                   LPW25_PROC_FLAG_PHYS |
                                   LPW25_PROC_FLAG_PHASE_COR), err);
    }

    void LPW25::updateChConfig() {
        if (devConfig() && devConfig()->adcConfig()) {
            m_ctx.Fd = devConfig()->adcConfig()->adcChFreq();
        }
    }

    void LPW25::getError(int errCode, LQError &err)  {
        if (errCode != LTR_OK) {
            err = LTRNativeError::error(errCode, errorString(errCode));
        }
    }

    QString LPW25::errorString(int errCode) {
        return  QSTRING_FROM_CSTR(LPW25_GetErrorString(errCode));
    }
}
