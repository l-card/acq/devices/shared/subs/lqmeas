#include "LPW25TypeInfo.h"
#include "LPW25.h"
#include "ltr/include/lpw25api.h"
#include "ltr/include/ltedsapi.h"
#include "lqmeas/units/std/Voltage.h"
#include "lqmeas/units/std/Current.h"
#include <QStringBuilder>

namespace LQMeas {
    class LPW25TypeInfoU230 : public LPW25TypeInfo {
    public:
        double sensorPhysMaxVal(int range_num = 0, int mode_num = 0) const override {
            Q_UNUSED(range_num) Q_UNUSED(mode_num)
            return  LPW25_PHYSRANGE_U_2_230;
        }
        double sensorPhysMinVal(int range_num = 0, int mode_num = 0) const override {
            return  -sensorPhysMaxVal(range_num, mode_num);
        }
        const Unit &sensorPhysUnit(int mode_num = 0) const override {
            Q_UNUSED(mode_num)
            return LQMeas::Units::Voltage::V();
        }
        double sensorElectricalMaxVal() const override {
            return LPW25_PHYSRANGE_U_2_230 * nominalSensitivity();
        }
        double sensorElectricalMinVal() const override {
            return  -sensorElectricalMaxVal();
        }
        const Unit &sensorElectricalUnit() const override {
            return  LQMeas::Units::Voltage::V();
        }

        double nominalSensitivity() const override {
            return  LPW25_DEFAULT_SENS_U_2_230;
        }

        virtual QString channelStr() const = 0;

        QString sensorModelName() const override {
            return QStringLiteral("LPW25-U-2-230-1, ") % channelStr();
        }

        QList<const SensorTypeInfo *> compatibleModels() const override {
            static const QList<const SensorTypeInfo *> list {
                    &LPW25TypeInfo::typeInfoU230Ch1(),
                    &LPW25TypeInfo::typeInfoU230Ch2()
            };
            return list;
        }
    };

    class LPW25TypeInfoI5 : public LPW25TypeInfo {
    public:
        double sensorPhysMaxVal(int range_num = 0, int mode_num = 0) const override {
            Q_UNUSED(range_num) Q_UNUSED(mode_num)
            return  LPW25_PHYSRANGE_I_1_5;
        }
        double sensorPhysMinVal(int range_num = 0, int mode_num = 0) const override {
            return  -sensorPhysMaxVal(range_num, mode_num);
        }
        const Unit &sensorPhysUnit(int mode_num = 0) const override {
            Q_UNUSED(mode_num)
            return LQMeas::Units::Current::A();
        }
        double sensorElectricalMaxVal() const override {
            return LPW25_PHYSRANGE_I_1_5 * nominalSensitivity();
        }
        double sensorElectricalMinVal() const override {
            return  -sensorElectricalMaxVal();
        }
        const Unit &sensorElectricalUnit() const override {
            return  LQMeas::Units::Voltage::V();
        }

        double nominalSensitivity() const override {
            return  LPW25_DEFAULT_SENS_I_1_5;
        }

        virtual int modificationNum() const = 0;

        QString sensorModelName() const override {
            return QStringLiteral("LPW25-I-1-5-") % QString::number(modificationNum());
        }

        QList<const SensorTypeInfo *> compatibleModels() const override {
            static const QList<const SensorTypeInfo *> list {
                    &LPW25TypeInfo::typeInfoI5_1(),
                    &LPW25TypeInfo::typeInfoI5_2()
            };
            return list;
        }
    };


    const QString &LPW25TypeInfo::name() {
        static const QString str {QStringLiteral("LPW25")};
        return str;
    }

    const LPW25TypeInfo &LPW25TypeInfo::typeInfoU230Ch1() {
        static const class LPW25TypeInfoU230Ch1 : public LPW25TypeInfoU230 {
            int tedsModelNumber() const override {
                return  LTEDS_LCARD_MODEL_ID_LPW25_U_2_230_CH1;
            }
            QString channelStr() const override {
                return  LPW25::tr("Channel 1");
            }
        } type;
        return  type;
    }

    const LPW25TypeInfo &LPW25TypeInfo::typeInfoU230Ch2() {
        static const class LPW25TypeInfoU230Ch2 : public LPW25TypeInfoU230 {
            int tedsModelNumber() const override {
                return  LTEDS_LCARD_MODEL_ID_LPW25_U_2_230_CH2;
            }
            QString channelStr() const override {
                return  LPW25::tr("Channel 2");
            }
        } type;
        return  type;
    }

    const LPW25TypeInfo &LPW25TypeInfo::typeInfoI5_1() {
        static const class LPW25TypeInfoI5_1 : public LPW25TypeInfoI5 {
            int tedsModelNumber() const override {
                return  LTEDS_LCARD_MODEL_ID_LPW25_I_1_5_1;
            }
            int modificationNum() const override {
                return  1;
            }
        } type;
        return  type;
    }

    const LPW25TypeInfo &LPW25TypeInfo::typeInfoI5_2() {
        static const class LPW25TypeInfoI5_2 : public LPW25TypeInfoI5 {
            int tedsModelNumber() const override {
                return  LTEDS_LCARD_MODEL_ID_LPW25_I_1_5_2;
            }
            int modificationNum() const override {
                return  2;
            }
        } type;
        return  type;
    }

    const QList<const LPW25TypeInfo *> &LPW25TypeInfo::typeModifications() {
        static const QList<const LPW25TypeInfo *> list {
            &typeInfoU230Ch1(),
            &typeInfoU230Ch2(),
            &typeInfoI5_1(),
            &typeInfoI5_2()
        };
        return list;
    }

    const LPW25TypeInfo *LPW25TypeInfo::tedsModelInfo(int modelID) {
        const QList<const LPW25TypeInfo *> &modList {typeModifications()};
        auto it {std::find_if(modList.constBegin(), modList.constEnd(),
                              [modelID](auto type){return type->tedsModelNumber() == modelID;})};
        return it == modList.constEnd() ? nullptr : *it;
    }

    LPW25TypeInfo::LPW25TypeInfo() {

    }
}
