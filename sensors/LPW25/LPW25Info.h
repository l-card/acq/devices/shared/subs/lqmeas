#ifndef LQMEAS_LPW25INFO_H
#define LQMEAS_LPW25INFO_H

#include <QString>
#include "ltr/include/lpw25api.h"

namespace LQMeas {
    class LPW25TypeInfo;

    class LPW25Info {
    public:
        LPW25Info(const TLPW25_INFO &info);
        LPW25Info(const LPW25TypeInfo *type, const QString &serial = QString{});


        const LPW25TypeInfo &type() const {return *m_type;}
        const QString &serial() const {return  m_serial;}
        const TLPW25_INFO &rawInfo() const {return  m_info;}

        void setSerail(const QString &serial);
    private:
        TLPW25_INFO m_info;
        const LPW25TypeInfo *m_type;
        QString m_serial;
    };
}

#endif // LQMEAS_LPW25INFO_H
