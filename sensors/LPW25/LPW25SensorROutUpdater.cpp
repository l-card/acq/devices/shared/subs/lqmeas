#include "LPW25SensorROutUpdater.h"
#include "LPW25.h"
#include "LPW25Info.h"

namespace LQMeas {
    LPW25SensorROutUpdater::LPW25SensorROutUpdater(LPW25 *lpw) :
        DevAdcICPSensorRoutUpdater(lpw), m_lpw(lpw) {

    }

    double LPW25SensorROutUpdater::rOut(double isrc_ma) {
        double ri10 = m_lpw->info().rawInfo().CalInfo.ROutCoefs.R_I_10;
        double ri2_86 = m_lpw->info().rawInfo().CalInfo.ROutCoefs.R_I_2_86;
        double k = (ri10 - ri2_86) / (10 - 2.86);
        return  (isrc_ma - 2.86) * k +  ri2_86;
    }

    void LPW25SensorROutUpdater::protISrcUpdate(double isrc_ma) {
        Q_EMIT rOutChanged(rOut(isrc_ma));
    }

}
