#ifndef LQMEAS_LPW25TYPEINFO_H
#define LQMEAS_LPW25TYPEINFO_H

#include <QMetaType>
#include "lqmeas/sensors/SensorTypeInfo.h"

namespace LQMeas {
    class LPW25TypeInfo : public SensorTypeInfo {
    public:
        static const QString &name();
        static const LPW25TypeInfo &typeInfoU230Ch1();
        static const LPW25TypeInfo &typeInfoU230Ch2();
        static const LPW25TypeInfo &typeInfoI5_1();
        static const LPW25TypeInfo &typeInfoI5_2();



        QString sensorTypeName() const override {return  name();}
        virtual int tedsModelNumber() const = 0;
        virtual double nominalSensitivity() const  = 0;

        static const QList<const LPW25TypeInfo *> &typeModifications();
        static const LPW25TypeInfo *tedsModelInfo(int modelID);
    protected:
        LPW25TypeInfo();
    };
}

Q_DECLARE_METATYPE(const LQMeas::LPW25TypeInfo *);

#endif // LQMEAS_LPW25TYPEINFO_H
