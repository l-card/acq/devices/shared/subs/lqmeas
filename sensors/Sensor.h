#ifndef LQMEAS_SENSOR_H
#define LQMEAS_SENSOR_H

#include <QObject>
class LQError;

namespace LQMeas {
    class DeviceConfig;
    class TedsTransducer;
    class SensorTypeInfo;

    class Sensor : public QObject {
        Q_OBJECT
    public:
        virtual TedsTransducer *teds() {return  nullptr;}


        void sensorAssignDevConfig(DeviceConfig *devCfg, int adcCh);

        void sensorProcStart(LQError &err);
        void sensorProcStop(LQError &err);
        void sensorProcData(QVector<double> &data, LQError &err);
        bool sensorProcIsRunning() const {return  m_proc_run;}

        DeviceConfig *devConfig() const {return  m_devcfg;}
        int adcChannel() const {return  m_adcCh;}

        const SensorTypeInfo &sensorTypeInfo() const {return  *m_typeInfo;}

        virtual Sensor *clone() const = 0;
    Q_SIGNALS:
        void sensorTypeChanged();
    protected:
        Sensor(const SensorTypeInfo *sensorTypeInfo);
        void setSensorTypeInfo(const SensorTypeInfo *sensorTypeInfo);

        virtual void protSensorDevConfigUpdate(DeviceConfig *devCfg, int adcCh,
                                               DeviceConfig *prevDevCfg, int prevCh) = 0;
        virtual void protSensorProcStart(LQError &err) = 0;
        virtual void protSensorProcStop(LQError &err) = 0;
        virtual void protSensorProcData(QVector<double> &data, LQError &err) = 0;
    private:
        const SensorTypeInfo *m_typeInfo;
        DeviceConfig *m_devcfg {nullptr};
        int m_adcCh {-1};
        bool m_proc_run {false};
    };
}

#endif // SENSOR_H
