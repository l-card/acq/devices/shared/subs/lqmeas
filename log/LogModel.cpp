#include "LogModel.h"
#include <QBrush>
#include <QMutexLocker>
#include "Log.h"
#include <QSettings>

namespace LQMeas {
    static const QString ui_set_log_model_group { QStringLiteral("LogModel") };
    static const QString ui_set_log_model_lvl   { QStringLiteral("loglvl") };

    static const QString datetime_format        {QStringLiteral("h:mm:ss, d.MM.yyyy")};

    LogModel::LogModel(QObject *parent) : QAbstractTableModel(parent) {
        QSettings set;
        set.beginGroup(ui_set_log_model_group);
        setLogLevel(Log::instance()->level(
                        static_cast<LogLevel::Number>(
                            set.value(ui_set_log_model_lvl, static_cast<int>(LogLevel::Detail)).toInt())));
        set.endGroup();

        connect(Log::instance(), &Log::message, this, &LogModel::addNewMessage);
    }

    LogModel::~LogModel() {
        QSettings set;
        set.beginGroup(ui_set_log_model_group);
        set.setValue(ui_set_log_model_lvl, static_cast<int>(m_lvl.number()));
        set.endGroup();
    }

    int LogModel::rowCount(const QModelIndex &parent) const {
        Q_UNUSED(parent)
        return m_showMsgList.size();
    }

    int LogModel::columnCount(const QModelIndex &parent) const {
        Q_UNUSED(parent)
        return COLUMN_CNT;
    }

    QVariant LogModel::data(const QModelIndex &index, int role) const {
        QVariant ret;
        if (index.isValid()) {
            const QSharedPointer<LogMessage> &msg {m_showMsgList.at(index.row())};
            if (role == Qt::ItemDataRole::ForegroundRole) {
                ret = QBrush{msg->level().color()};
            } else if (role == Qt::ItemDataRole::DisplayRole) {
                switch (index.column()) {
                    case COLUMN_TIME:
                        ret = msg->time().toString(datetime_format);
                        break;
                    case COLUMN_LEVEL: {
                            QString levelStr {msg->level().name()};
                            if (showErrRequired(msg)) {
                                levelStr.append(QString(" (%1)").arg(QString::number(msg->error().errorCode())));
                            }
                            ret = levelStr;
                        }
                        break;
                    case COLUMN_MSG: {
                            QString txt;
                            txt.append(msg->text());
                            if (showErrRequired(msg)) {
                                txt.append(QString(": %1").arg(msg->error().msg()));
                            }
                            ret = txt;
                        }
                        break;

                }
            } else if (role == Qt::ItemDataRole::ToolTipRole) {
                ret = msg->text();
            }
        }

        return ret;
    }

    Qt::ItemFlags LogModel::flags(const QModelIndex &index) const {
        return !index.isValid() ? Qt::ItemFlag::NoItemFlags :
            Qt::ItemFlag::ItemIsEnabled | Qt::ItemFlag::ItemIsSelectable;
    }

    QVariant LogModel::headerData(int section, Qt::Orientation orientation, int role) const {
        QVariant ret;
        if ((role == Qt::ItemDataRole::DisplayRole) && (orientation == Qt::Orientation::Horizontal)) {
            switch (section) {
                case COLUMN_TIME:
                    ret = tr("Record time");
                    break;
                case COLUMN_LEVEL:
                    ret = tr("Level");
                    break;
                case COLUMN_MSG:
                    ret = tr("Text");
                    break;
                default:
                    break;
            }
        }
        return ret;
    }

    void LogModel::setLogLevel(const LogLevel &lvl) {
        m_lvl = lvl;
        redrawLogRecords();
    }


    void LogModel::addNewMessage(const QSharedPointer<LQMeas::LogMessage> &msg) {
        QMutexLocker lock{&m_lock};
        addMessageList(QList<QSharedPointer<LogMessage> >{msg});
        m_allMsgList.append(msg);
    }

    void LogModel::addMessageList(const QList<QSharedPointer<LQMeas::LogMessage> > &msgList) {
        QList<QSharedPointer<LogMessage> > newMsgList;
        for (const QSharedPointer<LogMessage> &msg : msgList) {
            if (!msg->level().isLessCritical(&m_lvl)) {
                newMsgList.append(msg);
            }
        }

        if (newMsgList.size()) {
            beginInsertRows(QModelIndex{}, m_showMsgList.size(), m_showMsgList.size()+newMsgList.size()-1);
            m_showMsgList += newMsgList;
            endInsertRows();
        }
    }

    void LogModel::redrawLogRecords() {
        QMutexLocker lock{&m_lock};

        if (m_showMsgList.size()!=0) {
            beginRemoveRows(QModelIndex{}, 0, m_showMsgList.size()-1);
            m_showMsgList.clear();
            endRemoveRows();
        }

        addMessageList(m_allMsgList);
    }

    bool LogModel::showErrRequired(QSharedPointer<LogMessage> msg) const {
        return ((msg->level().number() == LogLevel::FatalError)  ||
                    (msg->level().number() == LogLevel::Error) ||
                    (msg->level().number() == LogLevel::Warning)) &&
                    !msg->error().isSuccess();
    }

    void LogModel::clear() {
        m_allMsgList.clear();
        redrawLogRecords();
    }
}
