#include "LogPanel.h"
#include "Log.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QTableView>
#include <QMenu>
#include <QAction>
#include <QEvent>
#include <QHeaderView>
#include <QScrollBar>

#include "table/actions/TableCopyAction.h"

namespace LQMeas {
    LogPanel::LogPanel(QWidget *parent) :
        QDockWidget{parent}  {

        this->setObjectName(QStringLiteral("LQMeasLogPanel"));

        m_model = new LogModel{this};

        m_logbox = new QTableView{};
        m_logbox->horizontalHeader()->setVisible(false);
        m_logbox->verticalHeader()->setVisible(false);
        m_logbox->horizontalHeader()->setStretchLastSection(true);
        m_logbox->setEditTriggers(QAbstractItemView::EditTrigger::NoEditTriggers);
        m_logbox->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);
        m_logbox->setMinimumWidth(320);

        m_logbox->setStyleSheet(QStringLiteral("QTableView::item { padding: 3px;}"));

        m_logbox->setModel(m_model);


        m_logbox->setColumnWidth(0, 175);
        m_logbox->setColumnWidth(1, 135);

        connect(m_model, &LogModel::rowsAboutToBeInserted, this, &LogPanel::checkScroll);

        connect(m_model, &LogModel::rowsInserted, this, &LogPanel::onInserted);


        checkScroll();

        m_actClear = new QAction(m_logbox);
        m_actClear->setIcon(QIcon(":/icons/clear.png"));
        connect(m_actClear, &QAction::triggered, this, &LogPanel::clear);
        m_actClear->setShortcut(tr("Ctrl+D"));
        m_logbox->addAction(m_actClear);

        m_logbox->setContextMenuPolicy(Qt::ContextMenuPolicy::CustomContextMenu);
        connect(m_logbox, &QTableView::customContextMenuRequested,
                this, &LogPanel::showContextMenu);

        m_contextMenu = new QMenu(this);


        m_actCopy = new TableCopyAction(m_logbox);
        m_actCopy->setIcon(QIcon(QStringLiteral(":/icons/copy.png")));
        m_logbox->addAction(m_actCopy);
        m_contextMenu->addAction(m_actCopy);

        m_actSelectAll= new QAction(this);
        m_actSelectAll->setIcon(QIcon(QStringLiteral(":/icons/select_all.png")));
        m_actSelectAll->setShortcut(QKeySequence::StandardKey::SelectAll);
        m_logbox->addAction(m_actSelectAll);
        m_contextMenu->addAction(m_actSelectAll);
        connect(m_actSelectAll, &QAction::triggered, m_logbox, &QTableView::selectAll);


        m_contextMenu->addAction(m_actClear);


        m_levelMenu = new QMenu{};
        m_contextMenu->addMenu(m_levelMenu);

        QActionGroup *lvlActions {new QActionGroup{m_levelMenu}};
        const QList<LogLevel> &lvls {Log::instance()->logLevels()};

        for (const LogLevel &lvl : lvls) {
            QAction* act {new QAction{lvl.name(), lvlActions}};
            act->setCheckable(true);
            if (lvl.number() == m_model->curLogLevel().number())
                act->setChecked(true);
            m_logLvlActMap[act] = lvl.number();
            lvlActions->addAction(act);
        }
        connect(lvlActions, &QActionGroup::triggered, this, &LogPanel::lvlActTriggered);

        m_levelMenu->addActions(lvlActions->actions());



        m_actResize = new QAction{this};
        connect(m_actResize, &QAction::triggered, this, &LogPanel::resizeRows);
        m_contextMenu->addAction(m_actResize);

        retranslateUi();

        setWidget(m_logbox);

        connect(this, &LogPanel::visibilityChanged, this, &LogPanel::resizeRows);
    }

    LogPanel::~LogPanel() {

    }

    void LogPanel::clear() {
        m_model->clear();
    }

    void LogPanel::resizeRows() {
        m_logbox->resizeRowsToContents();
        m_logbox->repaint();
    }

    void LogPanel::changeEvent(QEvent *event) {
        switch (event->type()) {
            case QEvent::Type::LanguageChange:
                retranslateUi();
                break;
            default:
                break;
        }

        QDockWidget::changeEvent(event);
    }

    void LogPanel::showContextMenu(QPoint point) {
        m_contextMenu->popup(m_logbox->mapToGlobal(point));
    }

    void LogPanel::lvlActTriggered(QAction *act) {
        m_model->setLogLevel(Log::instance()->level(m_logLvlActMap[act]));
    }

    void LogPanel::retranslateUi() {
        setWindowTitle(tr("Log"));
        m_levelMenu->setTitle(tr("Show log level"));
        m_actClear->setText(tr("Clear"));
        m_actSelectAll->setText(tr("Select All"));
        m_actResize->setText(tr("Recalculate rows size"));
        m_actCopy->retranslate();
        for (QAction *act : m_logLvlActMap.keys()) {
            act->setText(Log::instance()->level(m_logLvlActMap[act]).name());
        }
    }

    void LogPanel::checkScroll() {
        QScrollBar* bar { m_logbox->verticalScrollBar() };
        bool at_end = bar->value() == bar->maximum();

        if (at_end) {
           connect(m_model, &LogModel::rowsInserted, m_logbox,  &QTableView::scrollToBottom);
        } else {
           disconnect(m_model, &LogModel::rowsInserted, m_logbox,  &QTableView::scrollToBottom);
        }
    }

    void LogPanel::onInserted(const QModelIndex &, int first, int last) {
        for (int i = first; i <= last; ++i) {
            m_logbox->resizeRowToContents(i);
        }
    }
}
