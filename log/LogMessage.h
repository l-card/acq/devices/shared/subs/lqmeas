#ifndef LQMEAS_LOGMESSAGE
#define LQMEAS_LOGMESSAGE


#include <QSharedPointer>
#include <QDateTime>

#include "LogLevel.h"
#include "LQError.h"


namespace LQMeas {
    class Log;

    class LogMessage {
    public:
        const LogLevel  &level() const {return m_lvl;}
        const LQError   &error() const {return m_err;}
        const QString   &text() const {return m_txt;}
        const QDateTime &time() const {return m_time;}

    private:
        LogMessage(const QDateTime &time, const LogLevel &lvl, const QString &txt, const LQError &err) :
            m_time{time}, m_lvl{lvl}, m_txt{txt}, m_err{err} {}

        QDateTime m_time;
        LogLevel  m_lvl;
        QString   m_txt;
        LQError   m_err;

        friend class Log;
    };
}


#endif // LQMEAS_LOGMESSAGE

