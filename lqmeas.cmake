# Файл для включения в проект CMAKE.
# Перед включением библиотека Qt5 должна быть уже обнаружена и включен AUTOMOC и AUTOUIC
#
# На выходе возвращаются следующие переменные
#   LQMEAS_SOURCES              - исходные коды для включения в проект
#   LQMEAS_GENFILES             - сгенерированные файлы для включения в проект
#   LQMEAS_FILES                - все файлы (LQMEAS_SOURCES и LQMEAS_GENFILES)
#   LQMEAS_INCLUDE_DIRS         - директории заголовков для включения
#   LQMEAS_LIBRARIES            - библиотеки для линковки проекта
#   LQMEAS_LIBRARY_DIRS         - директории линковки
#   LQMEAS_COMPILE_DEFINITIONS  - определения для компилятора
#
# Перед включением могут быть установленны следующие конфигурационные переменные:
#   LQMEAS_DEVS                - список устройств, поддержка которых должна быть включена
#   LQMEAS_NO_GUI              - сборка с зависимостью только от QtCore
#   LQMEAS_USE_TEDS            - поддержка чтения с TEDS-датчиков
#   LQMEAS_USE_OPCUA           - поддержка протокола OPC-UA (для VI)
#   LQMEAS_USE_LTRAVRUPDATER      - вклюение кода обновления прошивок AVR модулей LTR

set(LQMEAS_DIR ${CMAKE_CURRENT_LIST_DIR})

set(LQMEAS_INCLUDE_DIRS ${LQMEAS_DIR}/.. ${LQMEAS_DIR}/subs/lqerror ${LQMEAS_DIR}/subs/QStringVersion)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${LQMEAS_DIR}/cmake/Modules/")


macro(bool_evaluate VAR)
    if (${ARGN})
        set(${VAR} ON)
    else()
        set(${VAR} OFF)
    endif()
endmacro()

macro(add_optional_files FLAG OPT_LIST FULL_LIST)
    if (${FLAG})
        set(${OPT_LIST} ${${OPT_LIST}} ${ARGN})
    endif()

    set(${FULL_LIST} ${${FULL_LIST}} ${ARGN})
endmacro()

add_optional_files(ON LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
    ${LQMEAS_DIR}/subs/lqerror/LQError.h
    ${LQMEAS_DIR}/subs/lqerror/LQError.cpp
    ${LQMEAS_DIR}/subs/QStringVersion/QStringVersion.h
    ${LQMEAS_DIR}/subs/QStringVersion/QStringVersion.cpp
    ${LQMEAS_DIR}/StdErrors.h
    ${LQMEAS_DIR}/StdErrors.cpp

    ${LQMEAS_DIR}/lqtdefs.h
    ${LQMEAS_DIR}/TextCodecs.h
    ${LQMEAS_DIR}/TextCodecs.cpp
    ${LQMEAS_DIR}/EnumNamedValue.h
    ${LQMEAS_DIR}/EnumConfigKey.h

    ${LQMEAS_DIR}/StdConfigKeys.h
    ${LQMEAS_DIR}/StdConfigKeys.cpp


    ${LQMEAS_DIR}/misc/QListHelper.h
    ${LQMEAS_DIR}/misc/LQTypeReg.h

    ${LQMEAS_DIR}/units/Unit.h
    ${LQMEAS_DIR}/units/Unit.cpp
    ${LQMEAS_DIR}/units/UnitGroup.h
    ${LQMEAS_DIR}/units/UnitGroup.cpp
    ${LQMEAS_DIR}/units/UnitConverter.h
    ${LQMEAS_DIR}/units/UnitConverter.cpp
    ${LQMEAS_DIR}/units/UnitsStorage.h
    ${LQMEAS_DIR}/units/UnitsStorage.cpp
    ${LQMEAS_DIR}/units/std/UnitGroups.h
    ${LQMEAS_DIR}/units/std/UnitGroups.cpp
    ${LQMEAS_DIR}/units/std/UnitPrefixes.h
    ${LQMEAS_DIR}/units/std/Voltage.h
    ${LQMEAS_DIR}/units/std/Voltage.cpp
    ${LQMEAS_DIR}/units/std/Current.h
    ${LQMEAS_DIR}/units/std/Current.cpp
    ${LQMEAS_DIR}/units/std/Resistance.h
    ${LQMEAS_DIR}/units/std/Resistance.cpp
    ${LQMEAS_DIR}/units/std/Force.h
    ${LQMEAS_DIR}/units/std/Force.cpp
    ${LQMEAS_DIR}/units/std/Energy.h
    ${LQMEAS_DIR}/units/std/Energy.cpp
    ${LQMEAS_DIR}/units/std/Power.h
    ${LQMEAS_DIR}/units/std/Power.cpp
    ${LQMEAS_DIR}/units/std/ElectricCharge.h
    ${LQMEAS_DIR}/units/std/ElectricCharge.cpp
    ${LQMEAS_DIR}/units/std/Temperature.h
    ${LQMEAS_DIR}/units/std/Temperature.cpp
    ${LQMEAS_DIR}/units/std/Time.h
    ${LQMEAS_DIR}/units/std/Time.cpp
    ${LQMEAS_DIR}/units/std/Frequency.h
    ${LQMEAS_DIR}/units/std/Frequency.cpp
    ${LQMEAS_DIR}/units/std/RotationalSpeed.h
    ${LQMEAS_DIR}/units/std/RotationalSpeed.cpp
    ${LQMEAS_DIR}/units/std/Displacement.h
    ${LQMEAS_DIR}/units/std/Displacement.cpp
    ${LQMEAS_DIR}/units/std/Velocity.h
    ${LQMEAS_DIR}/units/std/Velocity.cpp
    ${LQMEAS_DIR}/units/std/Acceleration.h
    ${LQMEAS_DIR}/units/std/Acceleration.cpp
    ${LQMEAS_DIR}/units/std/Mass.h
    ${LQMEAS_DIR}/units/std/Mass.cpp
    ${LQMEAS_DIR}/units/std/Pressure.h
    ${LQMEAS_DIR}/units/std/Pressure.cpp
    ${LQMEAS_DIR}/units/std/PressureImpulse.h
    ${LQMEAS_DIR}/units/std/PressureImpulse.cpp
    ${LQMEAS_DIR}/units/std/MassFlowRate.h
    ${LQMEAS_DIR}/units/std/MassFlowRate.cpp
    ${LQMEAS_DIR}/units/std/VolumetricFlowRate.h
    ${LQMEAS_DIR}/units/std/VolumetricFlowRate.cpp
    ${LQMEAS_DIR}/units/std/Area.h
    ${LQMEAS_DIR}/units/std/Area.cpp
    ${LQMEAS_DIR}/units/std/Volume.h
    ${LQMEAS_DIR}/units/std/Volume.cpp
    ${LQMEAS_DIR}/units/std/Density.h
    ${LQMEAS_DIR}/units/std/Density.cpp
    ${LQMEAS_DIR}/units/std/Angle.h
    ${LQMEAS_DIR}/units/std/Angle.cpp
    ${LQMEAS_DIR}/units/std/Relative.h
    ${LQMEAS_DIR}/units/std/Relative.cpp
    ${LQMEAS_DIR}/units/std/RelativeDeformation.h
    ${LQMEAS_DIR}/units/std/RelativeDeformation.cpp
    ${LQMEAS_DIR}/units/std/Discrete.h
    ${LQMEAS_DIR}/units/std/Discrete.cpp
    ${LQMEAS_DIR}/units/std/Information.h
    ${LQMEAS_DIR}/units/std/Information.cpp
    ${LQMEAS_DIR}/units/std/InformationSpeed.h
    ${LQMEAS_DIR}/units/std/InformationSpeed.cpp
    ${LQMEAS_DIR}/units/std/Special.h
    ${LQMEAS_DIR}/units/std/Special.cpp
    ${LQMEAS_DIR}/units/std/Digital.h
    ${LQMEAS_DIR}/units/std/Digital.cpp

    ${LQMEAS_DIR}/log/Log.h
    ${LQMEAS_DIR}/log/Log.cpp
    ${LQMEAS_DIR}/log/LogLevel.h
    ${LQMEAS_DIR}/log/LogMessage.h



    ${LQMEAS_DIR}/devs/Device.h
    ${LQMEAS_DIR}/devs/Device.cpp
    ${LQMEAS_DIR}/devs/DeviceInterface.h
    ${LQMEAS_DIR}/devs/DeviceInterface.cpp
    ${LQMEAS_DIR}/devs/DeviceInfo.h
    ${LQMEAS_DIR}/devs/DeviceInfo.cpp
    ${LQMEAS_DIR}/devs/DeviceRef.h
    ${LQMEAS_DIR}/devs/DeviceRef.cpp
    ${LQMEAS_DIR}/devs/DeviceRefMethod.h
    ${LQMEAS_DIR}/devs/DeviceRefMethod.cpp
    ${LQMEAS_DIR}/devs/DeviceConfig.h
    ${LQMEAS_DIR}/devs/DeviceConfig.cpp
    ${LQMEAS_DIR}/devs/DeviceConfigItem.h
    ${LQMEAS_DIR}/devs/DeviceConfigItem.cpp
    ${LQMEAS_DIR}/devs/DeviceConfigIface.h
    ${LQMEAS_DIR}/devs/DeviceConfigIface.cpp
    ${LQMEAS_DIR}/devs/DeviceTypeInfo.h
    ${LQMEAS_DIR}/devs/DeviceTypeInfo.cpp
    ${LQMEAS_DIR}/devs/DeviceTypeSeries.h
    ${LQMEAS_DIR}/devs/DeviceTypeStdSeries.h
    ${LQMEAS_DIR}/devs/DeviceTypeStdSeries.cpp
    ${LQMEAS_DIR}/devs/DeviceValidator.h
    ${LQMEAS_DIR}/devs/DeviceValidator.cpp
    ${LQMEAS_DIR}/devs/DeviceNameValidator.h
    ${LQMEAS_DIR}/devs/DeviceNameValidator.cpp
    ${LQMEAS_DIR}/devs/DeviceFrameReceiver.h
    ${LQMEAS_DIR}/devs/DeviceFrameSender.h
    ${LQMEAS_DIR}/devs/resolver/DevicesResolver.h
    ${LQMEAS_DIR}/devs/resolver/DevicesResolver.cpp

    ${LQMEAS_DIR}/sensors/Sensor.h
    ${LQMEAS_DIR}/sensors/Sensor.cpp
    ${LQMEAS_DIR}/sensors/SensorTypeInfo.h
    ${LQMEAS_DIR}/sensors/SensorTypeInfo.cpp


    ${LQMEAS_DIR}/ifaces/out/Signals/SignalType.h
    ${LQMEAS_DIR}/ifaces/out/Signals/SignalConfig.h
    ${LQMEAS_DIR}/ifaces/out/Signals/SignalConfig.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/SignalGenerator.h
    ${LQMEAS_DIR}/ifaces/out/Signals/SignalGenerator.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/SignalFabric.h
    ${LQMEAS_DIR}/ifaces/out/Signals/SignalFabric.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/AnalogSignalGenerator.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/AnalogSignalType.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Const/AnalogSignalConst.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Const/AnalogSignalConst.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Const/AnalogSignalConstConfig.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Const/AnalogSignalConstConfig.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Const/AnalogSignalConstGenerator.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Const/AnalogSignalConstGenerator.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Sin/AnalogSignalSin.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Sin/AnalogSignalSin.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Sin/AnalogSignalSinConfig.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Sin/AnalogSignalSinConfig.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Sin/AnalogSignalSinGenerator.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Sin/AnalogSignalSinGenerator.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Pulse/AnalogSignalPulse.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Pulse/AnalogSignalPulse.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Pulse/AnalogSignalPulseConfig.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Pulse/AnalogSignalPulseConfig.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Pulse/AnalogSignalPulseGenerator.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Pulse/AnalogSignalPulseGenerator.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Ramp/AnalogSignalRamp.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Ramp/AnalogSignalRamp.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Ramp/AnalogSignalRampConfig.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Ramp/AnalogSignalRampConfig.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Ramp/AnalogSignalRampGenerator.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/Ramp/AnalogSignalRampGenerator.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/MultiPoint/AnalogSignalMultiPoint.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/MultiPoint/AnalogSignalMultiPoint.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/MultiPoint/AnalogSignalMultiPointConfig.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/MultiPoint/AnalogSignalMultiPointConfig.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/MultiPoint/AnalogSignalMultiPointGenerator.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/MultiPoint/AnalogSignalMultiPointGenerator.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/MultiPoint/AnalogSignalMultiPointConnector.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/MultiPoint/AnalogSignalMultiPointConnector.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/TimeSeq/AnalogSignalTimeSeq.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/TimeSeq/AnalogSignalTimeSeq.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/TimeSeq/AnalogSignalTimeSeqConfig.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/TimeSeq/AnalogSignalTimeSeqConfig.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/TimeSeq/AnalogSignalTimeSeqGenerator.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Analog/TimeSeq/AnalogSignalTimeSeqGenerator.cpp

    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/DiscreteSignalType.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/DiscreteSignalGenerator.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/Pulse/DiscreteSignalPulse.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/Pulse/DiscreteSignalPulse.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/Pulse/DiscreteSignalPulseConfig.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/Pulse/DiscreteSignalPulseConfig.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/Pulse/DiscreteSignalPulseGenerator.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/Pulse/DiscreteSignalPulseGenerator.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/Const/DiscreteSignalConst.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/Const/DiscreteSignalConst.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/Const/DiscreteSignalConstConfig.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/Const/DiscreteSignalConstConfig.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/Const/DiscreteSignalConstGenerator.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/Const/DiscreteSignalConstGenerator.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/SinglePulseBurst/DiscreteSignalSinglePulseBurst.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/SinglePulseBurst/DiscreteSignalSinglePulseBurst.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/SinglePulseBurst/DiscreteSignalSinglePulseBurstConfig.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/SinglePulseBurst/DiscreteSignalSinglePulseBurstConfig.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/SinglePulseBurst/DiscreteSignalSinglePulseBurstGenerator.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/SinglePulseBurst/DiscreteSignalSinglePulseBurstGenerator.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/MultiPoint/DiscreteSignalMultiPoint.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/MultiPoint/DiscreteSignalMultiPoint.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/MultiPoint/DiscreteSignalMultiPointConfig.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/MultiPoint/DiscreteSignalMultiPointConfig.cpp
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/MultiPoint/DiscreteSignalMultiPointGenerator.h
    ${LQMEAS_DIR}/ifaces/out/Signals/Discrete/MultiPoint/DiscreteSignalMultiPointGenerator.cpp




    ${LQMEAS_DIR}/ifaces/out/SignalConverter/DevOutDacConverter.h
    ${LQMEAS_DIR}/ifaces/out/SignalConverter/DevOutDacConverter.cpp
    ${LQMEAS_DIR}/ifaces/out/SignalConverter/SignalConverter.h
    ${LQMEAS_DIR}/ifaces/out/SignalConverter/SignalConverter.cpp
    ${LQMEAS_DIR}/ifaces/out/SignalConverter/Linear/SignalConverterLinear.h
    ${LQMEAS_DIR}/ifaces/out/SignalConverter/Linear/SignalConverterLinear.cpp

    ${LQMEAS_DIR}/ifaces/out/DevOutInfo.h
    ${LQMEAS_DIR}/ifaces/out/DevOutInfo.cpp
    ${LQMEAS_DIR}/ifaces/out/DevOutConfig.h
    ${LQMEAS_DIR}/ifaces/out/DevOutConfig.cpp
    ${LQMEAS_DIR}/ifaces/out/DevOutSync.h
    ${LQMEAS_DIR}/ifaces/out/DevOutSync.cpp
    ${LQMEAS_DIR}/ifaces/out/DevOutSyncConfig.h
    ${LQMEAS_DIR}/ifaces/out/DevOutSyncConfig.cpp
    ${LQMEAS_DIR}/ifaces/out/DevOutSyncStatusTracker.h
    ${LQMEAS_DIR}/ifaces/out/DevOutSyncStatusTracker.cpp
    ${LQMEAS_DIR}/ifaces/out/DevOutAsyncDac.h
    ${LQMEAS_DIR}/ifaces/out/DevOutAsyncDac.cpp
    ${LQMEAS_DIR}/ifaces/out/DevOutAsyncDig.h
    ${LQMEAS_DIR}/ifaces/out/DevOutAsyncDig.cpp
    ${LQMEAS_DIR}/ifaces/out/DevOutAsyncDigSwMask.h
    ${LQMEAS_DIR}/ifaces/out/DevOutAsyncDigSwMask.cpp
    ${LQMEAS_DIR}/ifaces/out/OutRamSignalGenerator.h
    ${LQMEAS_DIR}/ifaces/out/OutRamSignalGenerator.cpp
    ${LQMEAS_DIR}/ifaces/out/DevOutSyncEchoTracker.h
    ${LQMEAS_DIR}/ifaces/out/DevOutSyncEchoTracker.cpp
    ${LQMEAS_DIR}/ifaces/out/SyncModes/DevOutSyncIfaceSend.h
    ${LQMEAS_DIR}/ifaces/out/SyncModes/DevOutSyncIfaceSend.cpp
    ${LQMEAS_DIR}/ifaces/out/SyncModes/DevOutSyncModeImpl.h
    ${LQMEAS_DIR}/ifaces/out/SyncModes/DevOutSyncModeImpl.cpp
    ${LQMEAS_DIR}/ifaces/out/SyncModes/stream/DevOutSyncIfaceStream.h
    ${LQMEAS_DIR}/ifaces/out/SyncModes/stream/DevOutSyncIfaceStream.cpp
    ${LQMEAS_DIR}/ifaces/out/SyncModes/stream/DevOutSyncModeImplStream.h
    ${LQMEAS_DIR}/ifaces/out/SyncModes/stream/DevOutSyncModeImplStream.cpp
    ${LQMEAS_DIR}/ifaces/out/SyncModes/stream/DevOutSyncStreamThread.h
    ${LQMEAS_DIR}/ifaces/out/SyncModes/stream/DevOutSyncStreamThread.cpp
    ${LQMEAS_DIR}/ifaces/out/SyncModes/cycle/DevOutSyncIfaceCycle.h
    ${LQMEAS_DIR}/ifaces/out/SyncModes/cycle/DevOutSyncIfaceCycle.cpp
    ${LQMEAS_DIR}/ifaces/out/SyncModes/cycle/DevOutSyncModeImplCycle.h
    ${LQMEAS_DIR}/ifaces/out/SyncModes/cycle/DevOutSyncModeImplCycle.cpp



    ${LQMEAS_DIR}/ifaces/in/adc/DevAdc.h
    ${LQMEAS_DIR}/ifaces/in/adc/DevAdc.cpp
    ${LQMEAS_DIR}/ifaces/in/adc/DevAdcInfo.h
    ${LQMEAS_DIR}/ifaces/in/adc/DevAdcInfo.cpp
    ${LQMEAS_DIR}/ifaces/in/adc/DevAdcConfig.h
    ${LQMEAS_DIR}/ifaces/in/adc/DevAdcConfig.cpp
    ${LQMEAS_DIR}/ifaces/in/adc/DevAdcSeqBaseConfig.h
    ${LQMEAS_DIR}/ifaces/in/adc/DevAdcSeqBaseConfig.cpp
    ${LQMEAS_DIR}/ifaces/in/adc/DevAdcStdSeq32Config.h
    ${LQMEAS_DIR}/ifaces/in/adc/DevAdcStdSeq32Config.cpp
    ${LQMEAS_DIR}/ifaces/in/adc/icp/DevAdcICPConfig.h
    ${LQMEAS_DIR}/ifaces/in/adc/icp/DevAdcICPConfig.cpp
    ${LQMEAS_DIR}/ifaces/in/adc/icp/DevAdcICPSensorRoutUpdater.h
    ${LQMEAS_DIR}/ifaces/in/adc/icp/DevAdcICPSensorRoutUpdater.cpp
    ${LQMEAS_DIR}/ifaces/in/adc/tare/DevAdcTare.h
    ${LQMEAS_DIR}/ifaces/in/adc/tare/DevAdcTare.cpp
    ${LQMEAS_DIR}/ifaces/in/adc/tare/DevAdcTareOp.h
    ${LQMEAS_DIR}/ifaces/in/adc/tare/DevAdcTarePoint.h
    ${LQMEAS_DIR}/ifaces/in/adc/tare/DevAdcTarePoint.cpp
    ${LQMEAS_DIR}/ifaces/in/adc/tare/DevAdcTareChCoefs.h
    ${LQMEAS_DIR}/ifaces/in/adc/tare/DevAdcTareChCoefs.cpp
    ${LQMEAS_DIR}/ifaces/in/adc/tare/DevAdcTareCoefType.h
    ${LQMEAS_DIR}/ifaces/in/adc/tare/DevAdcTareInfo.h
    ${LQMEAS_DIR}/ifaces/in/adc/tare/DevAdcTareInfo.cpp
    ${LQMEAS_DIR}/ifaces/in/dig/DevInDigInfo.h
    ${LQMEAS_DIR}/ifaces/in/dig/DevInDigInfo.cpp
    ${LQMEAS_DIR}/ifaces/in/dig/DevInDigConfig.h
    ${LQMEAS_DIR}/ifaces/in/dig/DevInDigConfig.cpp
    ${LQMEAS_DIR}/ifaces/in/dig/DevInDigSyncBaseConfig.h
    ${LQMEAS_DIR}/ifaces/in/dig/DevInDigSyncBaseConfig.cpp
    ${LQMEAS_DIR}/ifaces/in/dig/DevInAsyncDig.h
    ${LQMEAS_DIR}/ifaces/in/dig/DevInAsyncDig.cpp
    ${LQMEAS_DIR}/ifaces/in/dig/DevInSyncDig.h
    ${LQMEAS_DIR}/ifaces/in/dig/DevInSyncDig.cpp
    ${LQMEAS_DIR}/ifaces/in/cntr/DevInCntrInfo.h
    ${LQMEAS_DIR}/ifaces/in/cntr/DevInCntrInfo.cpp
    ${LQMEAS_DIR}/ifaces/in/cntr/DevInCntrConfig.h
    ${LQMEAS_DIR}/ifaces/in/cntr/DevInCntrConfig.cpp
    ${LQMEAS_DIR}/ifaces/in/cntr/DevInCntr.h
    ${LQMEAS_DIR}/ifaces/in/cntr/DevInCntr.cpp
    ${LQMEAS_DIR}/ifaces/in/cntr/DevInCntrData.h
    ${LQMEAS_DIR}/ifaces/in/frame/DevInFrameStatus.h
    ${LQMEAS_DIR}/ifaces/in/frame/DevInFrameReq.h
    ${LQMEAS_DIR}/ifaces/in/frame/DevInFrameReq.cpp



    ${LQMEAS_DIR}/ifaces/sync/DevSyncMark.h
    ${LQMEAS_DIR}/ifaces/sync/DevSyncMark.cpp
    ${LQMEAS_DIR}/ifaces/sync/DevSyncMarksGen.h
    ${LQMEAS_DIR}/ifaces/sync/DevSyncMarksGen.cpp
    ${LQMEAS_DIR}/ifaces/sync/DevSyncMarksGenInfo.h
    ${LQMEAS_DIR}/ifaces/sync/DevSyncMarksGenInfo.cpp
    ${LQMEAS_DIR}/ifaces/sync/DevSyncMarksGenConfig.h
    ${LQMEAS_DIR}/ifaces/sync/DevSyncMarksGenConfig.cpp
    ${LQMEAS_DIR}/ifaces/sync/DevSyncMarksRecv.h
    ${LQMEAS_DIR}/ifaces/sync/DevSyncMarksRecv.cpp
    ${LQMEAS_DIR}/ifaces/sync/DevSyncMarksRecvInfo.h
    ${LQMEAS_DIR}/ifaces/sync/DevSyncMarksRecvInfo.cpp
    ${LQMEAS_DIR}/ifaces/sync/DevSyncMarksRecvConfig.h
    ${LQMEAS_DIR}/ifaces/sync/DevSyncMarksRecvConfig.cpp

    ${LQMEAS_DIR}/ifaces/fw/DevFwLoader.h
    ${LQMEAS_DIR}/ifaces/fw/DevFwLoader.cpp
    ${LQMEAS_DIR}/ifaces/fw/DevFwLoadProgressIndicator.h
    ${LQMEAS_DIR}/ifaces/fw/DevFwLoadProgressIndicator.cpp
    ${LQMEAS_DIR}/ifaces/fw/DevFwStdTypes.h
    ${LQMEAS_DIR}/ifaces/fw/DevFwStdTypes.cpp
   )


bool_evaluate(LQMEAS_GUI NOT LQMEAS_NO_GUI)
add_optional_files(LQMEAS_GUI LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
    ${LQMEAS_DIR}/log/LogModel.h
    ${LQMEAS_DIR}/log/LogModel.cpp
    ${LQMEAS_DIR}/log/LogPanel.h
    ${LQMEAS_DIR}/log/LogPanel.cpp
)




add_optional_files(ON LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
    ${LQMEAS_DIR}/devs/resolver/netBrowser/DeviceNetSvcRecord.h
    ${LQMEAS_DIR}/devs/resolver/netBrowser/DeviceNetSvcRecord.cpp
    ${LQMEAS_DIR}/devs/resolver/netBrowser/DeviceNetworkBrowser.h
    ${LQMEAS_DIR}/devs/resolver/netBrowser/DeviceNetworkBrowser.cpp
    )



set(LTR_MODULES LTR11 LTR12 LTR22 LTR24 LTR25 LTR27 LTR34 LTR35 LTR41 LTR42 LTR43 LTR51 LTR114 LTR210 LTR212 LTR216 LTRT13 LTRS412 LTRK511 LTRK415 LTRK416 LTRK71)


foreach(LTR_MODULE ${LTR_MODULES})    
    string(TOLOWER ${LTR_MODULE} LTR_MODULE_LOWER)
    set(LTR_MODULE_DIR ${LQMEAS_DIR}/devs/LTR/modules/${LTR_MODULE})

    list(FIND LQMEAS_DEVS ${LTR_MODULE} ${LTR_MODULE}_IDX)
    if(NOT ${LTR_MODULE}_IDX EQUAL -1)

        set(LQMEAS_DEV_${LTR_MODULE} ON)
        set(LQMEAS_DEV_LTR           ON)



        set(LQMEAS_LTRAPI_COMPONENTS ${LQMEAS_LTRAPI_COMPONENTS} ${LTR_MODULE_LOWER}api)
    endif()

    include(${LTR_MODULE_DIR}/${LTR_MODULE}.cmake)
    add_optional_files(LQMEAS_DEV_${LTR_MODULE} LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
                       ${LQMEAS_${LTR_MODULE}_SOURCES})
endforeach()

if(LQMEAS_USE_LTRAVRUPDATER)
    set(LQMEAS_DEV_LTR           ON)
    set(LQMEAS_USE_CRC           ON)
    set(LQMEAS_LTRAPI_COMPONENTS ${LQMEAS_LTRAPI_COMPONENTS} ltravrapi ltr010api)
endif(LQMEAS_USE_LTRAVRUPDATER)    

add_optional_files(LQMEAS_USE_LTRAVRUPDATER LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
    ${LQMEAS_DIR}/devs/LTR/updaters/avr/LTRAvrUpdater.h
    ${LQMEAS_DIR}/devs/LTR/updaters/avr/LTRAvrUpdater.cpp
    ${LQMEAS_DIR}/devs/LTR/updaters/avr/LTRAvrUpdaterErrors.h
    ${LQMEAS_DIR}/devs/LTR/updaters/avr/LTRAvrUpdaterErrors.cpp
    )

if(LQMEAS_DEV_LTR)
    set(LQMEAS_LTRAPI_COMPONENTS ${LQMEAS_LTRAPI_COMPONENTS} ltrapi)
endif(LQMEAS_DEV_LTR)

add_optional_files(LQMEAS_DEV_LTR LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
    ${LQMEAS_DIR}/devs/LTR/LTRNativeError.h
    ${LQMEAS_DIR}/devs/LTR/LTRNativeError.cpp
    ${LQMEAS_DIR}/devs/LTR/LTRResolver.h
    ${LQMEAS_DIR}/devs/LTR/LTRResolver.cpp
    ${LQMEAS_DIR}/devs/LTR/LTRModuleRef.h
    ${LQMEAS_DIR}/devs/LTR/LTRModuleRef.cpp
    ${LQMEAS_DIR}/devs/LTR/service/LTRService.h
    ${LQMEAS_DIR}/devs/LTR/service/LTRService.cpp
    ${LQMEAS_DIR}/devs/LTR/service/LTRServiceRef.h
    ${LQMEAS_DIR}/devs/LTR/service/LTRServiceRef.cpp
    ${LQMEAS_DIR}/devs/LTR/service/LTRServiceInfo.h
    ${LQMEAS_DIR}/devs/LTR/service/LTRServiceInfo.cpp
    ${LQMEAS_DIR}/devs/LTR/service/LTRServiceTypeInfo.h
    ${LQMEAS_DIR}/devs/LTR/service/LTRServiceTypeInfo.cpp
    ${LQMEAS_DIR}/devs/LTR/service/LTRServiceConfig.h
    ${LQMEAS_DIR}/devs/LTR/service/LTRServiceConfig.cpp
    ${LQMEAS_DIR}/devs/LTR/service/LTRServiceModuleBufConfig.h
    ${LQMEAS_DIR}/devs/LTR/service/LTRServiceModuleBufConfig.cpp
    ${LQMEAS_DIR}/devs/LTR/crates/LTRCrate.h
    ${LQMEAS_DIR}/devs/LTR/crates/LTRCrate.cpp
    ${LQMEAS_DIR}/devs/LTR/crates/LTRCrateRef.h
    ${LQMEAS_DIR}/devs/LTR/crates/LTRCrateRef.cpp
    ${LQMEAS_DIR}/devs/LTR/crates/LTRCrateTypeInfo.h
    ${LQMEAS_DIR}/devs/LTR/crates/LTRCrateTypeInfo.cpp
    ${LQMEAS_DIR}/devs/LTR/crates/LTRCrateInfo.h
    ${LQMEAS_DIR}/devs/LTR/crates/LTRCrateInfo.cpp
    ${LQMEAS_DIR}/devs/LTR/crates/LTRCrateConfig.h
    ${LQMEAS_DIR}/devs/LTR/crates/LTRCrateConfig.cpp
    ${LQMEAS_DIR}/devs/LTR/crates/LTRCrateSyncConfig.h
    ${LQMEAS_DIR}/devs/LTR/crates/LTRCrateSyncConfig.cpp
    ${LQMEAS_DIR}/devs/LTR/modules/LTRModule.h
    ${LQMEAS_DIR}/devs/LTR/modules/LTRModule.cpp
    ${LQMEAS_DIR}/devs/LTR/modules/LTRModuleTypeInfo.h
    ${LQMEAS_DIR}/devs/LTR/modules/LTRModuleTypeInfo.cpp
    ${LQMEAS_DIR}/devs/LTR/modules/LTRModuleFrameReceiver.h
    ${LQMEAS_DIR}/devs/LTR/modules/LTRModuleFrameReceiver.cpp
    ${LQMEAS_DIR}/devs/LTR/modules/LTRModuleRawWordsTracker.h
    ${LQMEAS_DIR}/devs/LTR/modules/LTRModuleRawWordsTracker.cpp
    ${LQMEAS_DIR}/devs/LTR/marks/LTRSyncMarks.h
    ${LQMEAS_DIR}/devs/LTR/marks/LTRSyncMarks.cpp
    ${LQMEAS_DIR}/devs/LTR/marks/LTRSyncMarksRecvConfig.h
    ${LQMEAS_DIR}/devs/LTR/marks/LTRSyncMarksRecvConfig.cpp
    ${LQMEAS_DIR}/devs/LTR/marks/LTRSyncMarksRecvInfo.h
    ${LQMEAS_DIR}/devs/LTR/marks/LTRSyncMarksRecvInfo.cpp
)


list(FIND LQMEAS_DEVS LPW25 LPW25_IDX)
if(NOT LPW25_IDX EQUAL -1)
    set(LQMEAS_DEV_LPW25 ON)
    set(LQMEAS_USE_TEDS ON)
    set(LQMEAS_LTRAPI_COMPONENTS ${LQMEAS_LTRAPI_COMPONENTS} lpw25api)
endif(NOT LPW25_IDX EQUAL -1)

add_optional_files(LQMEAS_DEV_LPW25 LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
    ${LQMEAS_DIR}/sensors/LPW25/LPW25.h
    ${LQMEAS_DIR}/sensors/LPW25/LPW25.cpp
    ${LQMEAS_DIR}/sensors/LPW25/LPW25TypeInfo.h
    ${LQMEAS_DIR}/sensors/LPW25/LPW25TypeInfo.cpp
    ${LQMEAS_DIR}/sensors/LPW25/LPW25Info.h
    ${LQMEAS_DIR}/sensors/LPW25/LPW25Info.cpp
    ${LQMEAS_DIR}/sensors/LPW25/LPW25SensorROutUpdater.h
    ${LQMEAS_DIR}/sensors/LPW25/LPW25SensorROutUpdater.cpp
)


add_optional_files(LQMEAS_USE_TEDS LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
    ${LQMEAS_DIR}/ifaces/teds/DevTedsAccess.h
    ${LQMEAS_DIR}/ifaces/teds/DevTedsAccess.cpp
    ${LQMEAS_DIR}/ifaces/teds/TedsDataRdStream.h
    ${LQMEAS_DIR}/ifaces/teds/TedsDataRdStream.cpp
    ${LQMEAS_DIR}/ifaces/teds/TedsTransducer.h
    ${LQMEAS_DIR}/ifaces/teds/TedsTransducer.cpp
    ${LQMEAS_DIR}/ifaces/teds/info/TedsInfo.h
    ${LQMEAS_DIR}/ifaces/teds/info/TedsInfo.cpp
    ${LQMEAS_DIR}/ifaces/teds/info/TedsBasicInfo.h
    ${LQMEAS_DIR}/ifaces/teds/info/TedsBasicInfo.cpp
)





if(LQMEAS_USE_TEDS)
    set(LQMEAS_LTRAPI_COMPONENTS ${LQMEAS_LTRAPI_COMPONENTS} ltedsapi)
endif(LQMEAS_USE_TEDS)


add_optional_files(LQMEAS_DEV_LTR01 LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
    ${LQMEAS_DIR}/devs/LTR/modules/LTR01.h
    ${LQMEAS_DIR}/devs/LTR/modules/LTR01.cpp
    )
if(LQMEAS_DEV_LTR01)
    set(LQMEAS_LTRAPI_COMPONENTS ${LQMEAS_LTRAPI_COMPONENTS} ltr01api)
endif(LQMEAS_DEV_LTR01)


#так как некоторые компоненты ltrapi могут избользоваться вне LTR,
#то проверку компонентов и подключение нужных библиотек делаем
#отдельно от проверки условиня LQMEAS_DEV_LTR
if(LQMEAS_LTRAPI_COMPONENTS)
    find_package(LTRAPI REQUIRED ${LQMEAS_LTRAPI_COMPONENTS})

    set(LQMEAS_INCLUDE_DIRS ${LQMEAS_INCLUDE_DIRS} ${LTRAPI_INCLUDE_DIRS})
    set(LQMEAS_COMPILE_DEFINITIONS ${LQMEAS_COMPILE_DEFINITIONS} ${LTRAPI_COMPILE_DEFINITIONS})
    set(LQMEAS_LIBRARIES ${LQMEAS_LIBRARIES} ${LTRAPI_LIBRARIES})
endif(LQMEAS_LTRAPI_COMPONENTS)



set(LCOMP_DEVICES  E14_140)
#E2010 E14_440

foreach(LCOMP_DEVICE ${LCOMP_DEVICES})
    set(LQMEAS_DEV_DIR ${LQMEAS_DIR}/devs/lcomp/modules/${LCOMP_DEVICE})

    list(FIND LQMEAS_DEVS ${LCOMP_DEVICE} ${LCOMP_DEVICE}_IDX)
    if(NOT ${LCOMP_DEVICE}_IDX EQUAL -1)
        string(TOLOWER ${LCOMP_DEVICE} LCOMP_MODULE_LOWER)

        set(LQMEAS_DEV_${LCOMP_DEVICE}  ON)
        set(LQMEAS_DEV_LCOMP            ON)        
    endif()

    add_optional_files(LQMEAS_DEV_${LCOMP_DEVICE} LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
        ${LQMEAS_DEV_DIR}/${LCOMP_DEVICE}.h
        ${LQMEAS_DEV_DIR}/${LCOMP_DEVICE}.cpp
        ${LQMEAS_DEV_DIR}/${LCOMP_DEVICE}Config.h
        ${LQMEAS_DEV_DIR}/${LCOMP_DEVICE}Config.cpp
        ${LQMEAS_DEV_DIR}/${LCOMP_DEVICE}Info.h
        ${LQMEAS_DEV_DIR}/${LCOMP_DEVICE}TypeInfo.h
        ${LQMEAS_DEV_DIR}/${LCOMP_DEVICE}TypeInfo.cpp
     )
endforeach()

if(LQMEAS_DEV_LCOMP)
    if(WIN32 AND NOT LCOMP_LIBRARY_ROOT_DIR)
        set(LCOMP_LIBRARY_ROOT_DIR "c:/Program Files (x86)/L-Card/LIBRARY")
    endif()
    set(LQMEAS_INCLUDE_DIRS ${LQMEAS_INCLUDE_DIRS} ${LCOMP_LIBRARY_ROOT_DIR})    
endif(LQMEAS_DEV_LCOMP)

add_optional_files(LQMEAS_DEV_LCOMP LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
    ${LQMEAS_DIR}/devs/lcomp/lcomp_headers.h
    ${LQMEAS_DIR}/devs/lcomp/lcompErrors.h
    ${LQMEAS_DIR}/devs/lcomp/lcompResolver.h
    ${LQMEAS_DIR}/devs/lcomp/lcompResolver.cpp
    ${LQMEAS_DIR}/devs/lcomp/lcompDevice.h
    ${LQMEAS_DIR}/devs/lcomp/lcompDevice.cpp
    ${LQMEAS_DIR}/devs/lcomp/lcompStreamDevice.h
    ${LQMEAS_DIR}/devs/lcomp/lcompStreamDevice.cpp
    ${LQMEAS_DIR}/devs/lcomp/lcompStreamReceiver.h
    ${LQMEAS_DIR}/devs/lcomp/lcompStreamReceiver.cpp
    ${LQMEAS_DIR}/devs/lcomp/lcompRecvProcessorIface.h
    ${LQMEAS_DIR}/devs/lcomp/lcompSeqAdcConfig.cpp
    ${LQMEAS_DIR}/devs/lcomp/lcompSeqAdcConfig.h
)



set(X502_DEVICES E502 L502 E16)
foreach(X502_DEVICE ${X502_DEVICES})    

    string(TOLOWER ${X502_DEVICE}  X502_DEVICE_LOWER)
    set(LQMEAS_DEV_DIR ${LQMEAS_DIR}/devs/x502/modules/${X502_DEVICE})

    list(FIND LQMEAS_DEVS ${X502_DEVICE} ${X502_DEVICE}_IDX)


    if(NOT ${X502_DEVICE}_IDX EQUAL -1)

        set(LQMEAS_DEV_${X502_DEVICE}  ON)
        set(LQMEAS_DEV_X502            ON)
        #E16 использует ту же бибилиотеку, что и E502
        if (${X502_DEVICE} STREQUAL E16)
            set(X502API_COMPONENTS         ${X502API_COMPONENTS} e502api)
        else()
            set(X502API_COMPONENTS         ${X502API_COMPONENTS} ${X502_DEVICE_LOWER}api)
        endif()
    endif()

    add_optional_files(LQMEAS_DEV_${X502_DEVICE} LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
        ${LQMEAS_DEV_DIR}/${X502_DEVICE}.h
        ${LQMEAS_DEV_DIR}/${X502_DEVICE}.cpp
        ${LQMEAS_DEV_DIR}/${X502_DEVICE}Info.h
        ${LQMEAS_DEV_DIR}/${X502_DEVICE}TypeInfo.h
        ${LQMEAS_DEV_DIR}/${X502_DEVICE}TypeInfo.cpp
    )
endforeach()


if(LQMEAS_DEV_X502)
    find_package(X502API REQUIRED ${X502API_COMPONENTS})

    set(LQMEAS_INCLUDE_DIRS ${LQMEAS_INCLUDE_DIRS} ${X502API_INCLUDE_DIRS})
    set(LQMEAS_LIBRARIES ${LQMEAS_LIBRARIES} ${X502API_LIBRARIES})
endif(LQMEAS_DEV_X502)

add_optional_files(LQMEAS_DEV_X502 LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
    ${LQMEAS_DIR}/devs/x502/x502NativeError.h
    ${LQMEAS_DIR}/devs/x502/x502NativeError.cpp
    ${LQMEAS_DIR}/devs/x502/x502.h
    ${LQMEAS_DIR}/devs/x502/x502.cpp
    ${LQMEAS_DIR}/devs/x502/x502TypeInfo.h
    ${LQMEAS_DIR}/devs/x502/x502TypeInfo.cpp
    ${LQMEAS_DIR}/devs/x502/x502Info.h
    ${LQMEAS_DIR}/devs/x502/x502Info.cpp
    ${LQMEAS_DIR}/devs/x502/x502Config.h
    ${LQMEAS_DIR}/devs/x502/x502Config.cpp
    ${LQMEAS_DIR}/devs/x502/x502AdcConfig.h
    ${LQMEAS_DIR}/devs/x502/x502AdcConfig.cpp
    ${LQMEAS_DIR}/devs/x502/x502OutConfig.h
    ${LQMEAS_DIR}/devs/x502/x502OutConfig.cpp
    ${LQMEAS_DIR}/devs/x502/x502InDigConfig.h
    ${LQMEAS_DIR}/devs/x502/x502InDigConfig.cpp
    ${LQMEAS_DIR}/devs/x502/x502Resolver.h
    ${LQMEAS_DIR}/devs/x502/x502Resolver.cpp
    ${LQMEAS_DIR}/devs/x502/x502Resolver.cpp

)


add_optional_files(LQMEAS_DEV_X502 LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
    ${LQMEAS_DIR}/devs/x502/net/x502NetDevice.h
    ${LQMEAS_DIR}/devs/x502/net/x502NetDevice.cpp
    ${LQMEAS_DIR}/devs/x502/net/x502DeviceRefIPAddr.h
    ${LQMEAS_DIR}/devs/x502/net/x502DeviceRefIPAddr.cpp
    ${LQMEAS_DIR}/devs/x502/net/x502DeviceRefNetSvc.h
    ${LQMEAS_DIR}/devs/x502/net/x502DeviceRefNetSvc.cpp
    ${LQMEAS_DIR}/devs/x502/net/x502NetSvcRecord.h
    ${LQMEAS_DIR}/devs/x502/net/x502NetSvcRecord.cpp
    ${LQMEAS_DIR}/devs/x502/net/x502NetworkBrowser.h
    ${LQMEAS_DIR}/devs/x502/net/x502NetworkBrowser.cpp
    ${LQMEAS_DIR}/devs/x502/net/x502IfaceSettings.h
)



set(LVIMS_DEVICES LVIMS VIB4)
foreach(LVIMS_DEVICE ${LVIMS_DEVICES})
    string(TOLOWER ${LVIMS_DEVICE}      LVIMS_DEVICE_LOWER)
    set(LQMEAS_DEV_DIR ${LQMEAS_DIR}/devs/vi/modules/${LVIMS_DEVICE_LOWER})

    list(FIND LQMEAS_DEVS ${LVIMS_DEVICE} ${LVIMS_DEVICE}_IDX)
    if(NOT ${LVIMS_DEVICE}_IDX EQUAL -1)        
        set(LQMEAS_DEV_${LVIMS_DEVICE}      ON)
        set(LQMEAS_DEV_VI_GROUP             ON)
        set(LQMEAS_USE_NET_AUTODISCOVERY    ON)
        set(LQMEAS_USE_FAST_CRC             ON)
    endif()

    if (${LVIMS_DEVICE} STREQUAL LVIMS)
        add_optional_files(LQMEAS_DEV_${LVIMS_DEVICE} LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
            ${LQMEAS_DEV_DIR}/LViMSSysModuleType.h
            ${LQMEAS_DEV_DIR}/LViMSSysModuleType.cpp
            ${LQMEAS_DEV_DIR}/LViMSNetAutodiscoveryServiceType.h
            ${LQMEAS_DEV_DIR}/LViMSNetAutodiscoveryServiceType.cpp
            ${LQMEAS_DEV_DIR}/LViMSNetAutodiscoveryRecordInfo.h
            ${LQMEAS_DEV_DIR}/LViMSNetAutodiscoveryRecordInfo.cpp
            ${LQMEAS_DEV_DIR}/icp/LViMS_ICP_TypeInfo.h
            ${LQMEAS_DEV_DIR}/icp/LViMS_ICP_TypeInfo.cpp
            ${LQMEAS_DEV_DIR}/nps/LViMS_NPS_TypeInfo.h
            ${LQMEAS_DEV_DIR}/nps/LViMS_NPS_TypeInfo.cpp
            ${LQMEAS_DEV_DIR}/rel/LViMS_REL_TypeInfo.h
            ${LQMEAS_DEV_DIR}/rel/LViMS_REL_TypeInfo.cpp
            ${LQMEAS_DEV_DIR}/switch/LViMS_SWITCH_TypeInfo.h
            ${LQMEAS_DEV_DIR}/switch/LViMS_SWITCH_TypeInfo.cpp
            )
    endif(${LVIMS_DEVICE} STREQUAL LVIMS)

    if (${LVIMS_DEVICE} STREQUAL VIB4)
        add_optional_files(LQMEAS_DEV_${LVIMS_DEVICE} LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
            ${LQMEAS_DEV_DIR}/Vib4SysModuleType.h
            ${LQMEAS_DEV_DIR}/Vib4SysModuleType.cpp
            ${LQMEAS_DEV_DIR}/Vib4NetAutodiscoveryServiceType.h
            ${LQMEAS_DEV_DIR}/Vib4NetAutodiscoveryServiceType.cpp
            ${LQMEAS_DEV_DIR}/Vib4NetAutodiscoveryRecordInfo.h
            ${LQMEAS_DEV_DIR}/Vib4NetAutodiscoveryRecordInfo.cpp
            ${LQMEAS_DEV_DIR}/icp/Vib4_ICP_TypeInfo.h
            ${LQMEAS_DEV_DIR}/icp/Vib4_ICP_TypeInfo.cpp
            ${LQMEAS_DEV_DIR}/nps/Vib4_NPS_TypeInfo.h
            ${LQMEAS_DEV_DIR}/nps/Vib4_NPS_TypeInfo.cpp
            ${LQMEAS_DEV_DIR}/rel/Vib4_REL_TypeInfo.h
            ${LQMEAS_DEV_DIR}/rel/Vib4_REL_TypeInfo.cpp
            ${LQMEAS_DEV_DIR}/switch/Vib4_SWITCH_TypeInfo.h
            ${LQMEAS_DEV_DIR}/switch/Vib4_SWITCH_TypeInfo.cpp
            )
    endif(${LVIMS_DEVICE} STREQUAL VIB4)
endforeach()


add_optional_files(LQMEAS_DEV_VI_GROUP LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
    ${LQMEAS_DIR}/devs/vi/ViErrors.h
    ${LQMEAS_DIR}/devs/vi/ViErrors.cpp

    ${LQMEAS_DIR}/devs/vi/devref/ViDeviceRefNetSvc.h
    ${LQMEAS_DIR}/devs/vi/devref/ViDeviceRefNetSvc.cpp
    ${LQMEAS_DIR}/devs/vi/devref/ViDeviceRefIpAddr.h
    ${LQMEAS_DIR}/devs/vi/devref/ViDeviceRefIpAddr.cpp
    ${LQMEAS_DIR}/devs/vi/devref/ViDeviceResolver.h
    ${LQMEAS_DIR}/devs/vi/devref/ViDeviceResolver.cpp

    ${LQMEAS_DIR}/devs/vi/modules/ViModuleTypeInfo.h
    ${LQMEAS_DIR}/devs/vi/modules/ViModuleTypeInfo.cpp
    ${LQMEAS_DIR}/devs/vi/modules/ViModuleInfo.h
    ${LQMEAS_DIR}/devs/vi/modules/ViModuleInfo.cpp
    ${LQMEAS_DIR}/devs/vi/modules/ViModule.h
    ${LQMEAS_DIR}/devs/vi/modules/ViModule.cpp
    ${LQMEAS_DIR}/devs/vi/modules/ViDiagnosticAdcChInfo.h
    ${LQMEAS_DIR}/devs/vi/modules/ViDiagnosticAdcChInfo.cpp

    ${LQMEAS_DIR}/devs/vi/ifaceset/ViIfaceSettings.h

    ${LQMEAS_DIR}/devs/vi/config/ViConfig.h
    ${LQMEAS_DIR}/devs/vi/config/ViConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/ViConfigItem.h
    ${LQMEAS_DIR}/devs/vi/config/ViConfigItem.cpp
    ${LQMEAS_DIR}/devs/vi/config/ViModuleConfigSerializer.h
    ${LQMEAS_DIR}/devs/vi/config/ViModuleConfigSerializer.cpp
    ${LQMEAS_DIR}/devs/vi/config/ViModuleConfig.h
    ${LQMEAS_DIR}/devs/vi/config/ViModuleConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/ViChannelConfig.h
    ${LQMEAS_DIR}/devs/vi/config/ViChannelConfig.cpp

    ${LQMEAS_DIR}/devs/vi/config/misc/ViConfigOptionalVal.h
    ${LQMEAS_DIR}/devs/vi/config/misc/ViConfigOptionalVal.cpp
    ${LQMEAS_DIR}/devs/vi/config/misc/ViConfigHystVal.h
    ${LQMEAS_DIR}/devs/vi/config/misc/ViConfigHystVal.cpp

    ${LQMEAS_DIR}/devs/vi/config/acq/ViAcqConfig.h
    ${LQMEAS_DIR}/devs/vi/config/acq/ViAcqConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/ViAcqChannelConfig.h
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/ViAcqChannelConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/ViAcqChannelType.h
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/ViAcqChannelType.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/phasetrig/ViAcqChannelPhaseTrigConfig.h
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/phasetrig/ViAcqChannelPhaseTrigConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/phasetrig/ViAcqChannelPhaseTrigPolarity.h
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/phasetrig/ViAcqChannelPhaseTrigPolarity.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/position/ViAcqChannelPositionConfig.h
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/position/ViAcqChannelPositionConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/position/ViAcqChannelPositionNormalDirection.h
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/position/ViAcqChannelPositionNormalDirection.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/types/ViAcqChannelTypeUnknown.h
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/types/ViAcqChannelTypeUnknown.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/types/ViAcqChannelTypeVibroAccel.h
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/types/ViAcqChannelTypeVibroAccel.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/types/ViAcqChannelTypeRadialVibration.h
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/types/ViAcqChannelTypeRadialVibration.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/types/ViAcqChannelTypePhaseTrigger.h
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/types/ViAcqChannelTypePhaseTrigger.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/types/ViAcqChannelTypeAxialPosition.h
    ${LQMEAS_DIR}/devs/vi/config/acq/channel/types/ViAcqChannelTypeAxialPosition.cpp

    ${LQMEAS_DIR}/devs/vi/config/acq/sensor/ViAcqSensorConfig.h
    ${LQMEAS_DIR}/devs/vi/config/acq/sensor/ViAcqSensorConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/sensor/ViAcqSensorType.h
    ${LQMEAS_DIR}/devs/vi/config/acq/sensor/ViAcqSensorType.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/sensor/ViAcqSensorConType.h
    ${LQMEAS_DIR}/devs/vi/config/acq/sensor/ViAcqSensorConType.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/sensor/types/unknown/ViAcqSensorTypeUnknown.h
    ${LQMEAS_DIR}/devs/vi/config/acq/sensor/types/unknown/ViAcqSensorTypeUnknown.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/sensor/types/accelerometer/ViAcqSensorTypeAccelerometer.h
    ${LQMEAS_DIR}/devs/vi/config/acq/sensor/types/accelerometer/ViAcqSensorTypeAccelerometer.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/sensor/types/proximeter/ViAcqSensorTypeProximeter.h
    ${LQMEAS_DIR}/devs/vi/config/acq/sensor/types/proximeter/ViAcqSensorTypeProximeter.cpp

    ${LQMEAS_DIR}/devs/vi/config/acq/meas/ViMeasConfig.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/ViMeasConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/ViMeasType.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/ViMeasType.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/ViMeasCondType.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/ViMeasCondType.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/ViMeasGroupConfig.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/ViMeasGroupConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/ViMeasGroupType.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/ViMeasGroupType.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/ViMeasParameterType.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/ViMeasParameterType.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/ViMeasFilterConfig.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/ViMeasFilterConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeUnknown.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeUnknown.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeVibroDisp.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeVibroDisp.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeVoltageGap.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeVoltageGap.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeRotSpeed.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeRotSpeed.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypePosition.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypePosition.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/groups/ViMeasGroupTypeSingle.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/groups/ViMeasGroupTypeSingle.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeVibroAccel.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeVibroAccel.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeVibroAccelHf.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeVibroAccelHf.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeVibroVelocity.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeVibroVelocity.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeVoltageDC.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeVoltageDC.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeVibroSmax.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/ViMeasTypeVibroSmax.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpBase.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpBase.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaBase.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaBase.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpAccel.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpAccel.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpVelocity.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpVelocity.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpDisp.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpDisp.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaAccel.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaAccel.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaVelocity.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaVelocity.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaDisp.h
    ${LQMEAS_DIR}/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaDisp.cpp




    ${LQMEAS_DIR}/devs/vi/config/acq/setpoint/ViSetpointConfig.h
    ${LQMEAS_DIR}/devs/vi/config/acq/setpoint/ViSetpointConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/acq/setpoint/ViSetpointConditionType.h
    ${LQMEAS_DIR}/devs/vi/config/acq/setpoint/ViSetpointConditionType.cpp

    ${LQMEAS_DIR}/devs/vi/config/events/ViEventsConfig.h
    ${LQMEAS_DIR}/devs/vi/config/events/ViEventsConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/events/ViEventConfig.h
    ${LQMEAS_DIR}/devs/vi/config/events/ViEventConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/events/logic/ViLogicElementConfig.h
    ${LQMEAS_DIR}/devs/vi/config/events/logic/ViLogicElementConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/events/logic/ViLogicElementType.h
    ${LQMEAS_DIR}/devs/vi/config/events/logic/ViLogicScheme.h
    ${LQMEAS_DIR}/devs/vi/config/events/logic/ViLogicScheme.cpp
    ${LQMEAS_DIR}/devs/vi/config/events/logic/types/ViLogicElementConfigAnd.h
    ${LQMEAS_DIR}/devs/vi/config/events/logic/types/ViLogicElementConfigAnd.cpp
    ${LQMEAS_DIR}/devs/vi/config/events/logic/types/ViLogicElementConfigOr.h
    ${LQMEAS_DIR}/devs/vi/config/events/logic/types/ViLogicElementConfigOr.cpp
    ${LQMEAS_DIR}/devs/vi/config/events/logic/types/ViLogicElementConfigNot.h
    ${LQMEAS_DIR}/devs/vi/config/events/logic/types/ViLogicElementConfigNot.cpp
    ${LQMEAS_DIR}/devs/vi/config/events/logic/types/ViLogicElementConfigMeasCond.h
    ${LQMEAS_DIR}/devs/vi/config/events/logic/types/ViLogicElementConfigMeasCond.cpp
    ${LQMEAS_DIR}/devs/vi/config/events/logic/types/ViLogicElementConfigDin.h
    ${LQMEAS_DIR}/devs/vi/config/events/logic/types/ViLogicElementConfigDin.cpp
    ${LQMEAS_DIR}/devs/vi/config/events/logic/types/ViLogicElementConfigEvent.h
    ${LQMEAS_DIR}/devs/vi/config/events/logic/types/ViLogicElementConfigEvent.cpp

    ${LQMEAS_DIR}/devs/vi/config/dout/ViDoutConfig.h
    ${LQMEAS_DIR}/devs/vi/config/dout/ViDoutConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/dout/ViDoutChannelConfig.h
    ${LQMEAS_DIR}/devs/vi/config/dout/ViDoutChannelConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/dout/ViDoutChannelType.h
    ${LQMEAS_DIR}/devs/vi/config/dout/ViDoutChannelType.cpp
    ${LQMEAS_DIR}/devs/vi/config/dout/ViDoutNormalOpState.h
    ${LQMEAS_DIR}/devs/vi/config/dout/ViDoutNormalOpState.cpp

    ${LQMEAS_DIR}/devs/vi/config/din/ViDinConfig.h
    ${LQMEAS_DIR}/devs/vi/config/din/ViDinConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/din/ViDinChannelConfig.h
    ${LQMEAS_DIR}/devs/vi/config/din/ViDinChannelConfig.cpp

    ${LQMEAS_DIR}/devs/vi/config/wf/ViWaveformsConfig.h
    ${LQMEAS_DIR}/devs/vi/config/wf/ViWaveformsConfig.cpp
    ${LQMEAS_DIR}/devs/vi/config/wf/ViWaveformSrcConfig.h
    ${LQMEAS_DIR}/devs/vi/config/wf/ViWaveformSrcConfig.cpp

    ${LQMEAS_DIR}/devs/vi/config/exch/ViExchConfig.h
    ${LQMEAS_DIR}/devs/vi/config/exch/ViExchConfig.cpp

    ${LQMEAS_DIR}/devs/vi/config/refs/ViConfigRefCtl.h
    ${LQMEAS_DIR}/devs/vi/config/refs/ViConfigRefCtl.cpp
    ${LQMEAS_DIR}/devs/vi/config/refs/ViConfigRef.h
    ${LQMEAS_DIR}/devs/vi/config/refs/ViConfigRef.cpp
    ${LQMEAS_DIR}/devs/vi/config/refs/ViConfigRefAcqCh.h
    ${LQMEAS_DIR}/devs/vi/config/refs/ViConfigRefAcqCh.cpp
    ${LQMEAS_DIR}/devs/vi/config/refs/ViConfigRefMeas.h
    ${LQMEAS_DIR}/devs/vi/config/refs/ViConfigRefMeas.cpp
    ${LQMEAS_DIR}/devs/vi/config/refs/ViConfigRefEvent.h
    ${LQMEAS_DIR}/devs/vi/config/refs/ViConfigRefEvent.cpp
    ${LQMEAS_DIR}/devs/vi/config/refs/ViConfigRefDin.h
    ${LQMEAS_DIR}/devs/vi/config/refs/ViConfigRefDin.cpp

    ${LQMEAS_DIR}/devs/vi/data/state/ViTimeClockId.h
    ${LQMEAS_DIR}/devs/vi/data/state/ViTimeStamp.h
    ${LQMEAS_DIR}/devs/vi/data/state/ViLocalTimeInfo.h
    ${LQMEAS_DIR}/devs/vi/data/state/ViManEventFlagState.h
    ${LQMEAS_DIR}/devs/vi/data/state/ViModuleState.h
    ${LQMEAS_DIR}/devs/vi/data/state/ViModuleState.cpp
    ${LQMEAS_DIR}/devs/vi/data/state/acq/ViMeasState.h
    ${LQMEAS_DIR}/devs/vi/data/state/acq/ViMeasState.cpp
    ${LQMEAS_DIR}/devs/vi/data/state/acq/ViAcqChannelState.h
    ${LQMEAS_DIR}/devs/vi/data/state/acq/ViAcqChannelState.cpp
    ${LQMEAS_DIR}/devs/vi/data/state/acq/ViAcqChannelStatus.h
    ${LQMEAS_DIR}/devs/vi/data/state/acq/ViAcqChannelStatus.cpp
    ${LQMEAS_DIR}/devs/vi/data/state/acq/ViAcqChannelConvInfo.h
    ${LQMEAS_DIR}/devs/vi/data/state/acq/ViAcqChannelStreamInfo.h
    ${LQMEAS_DIR}/devs/vi/data/state/dout/ViDoutChannelState.h
    ${LQMEAS_DIR}/devs/vi/data/state/dout/ViDoutChannelState.cpp
    ${LQMEAS_DIR}/devs/vi/data/state/dout/ViDoutTypeState.h
    ${LQMEAS_DIR}/devs/vi/data/state/dout/ViDoutTypeState.cpp
    ${LQMEAS_DIR}/devs/vi/data/state/wf/ViWfInfo.h
    ${LQMEAS_DIR}/devs/vi/data/state/wf/ViWfChInfo.h
    ${LQMEAS_DIR}/devs/vi/data/state/wf/ViWfChInfo.cpp
    ${LQMEAS_DIR}/devs/vi/data/state/wf/ViWfChDataBlock.h
    ${LQMEAS_DIR}/devs/vi/data/state/diagnostic/ViModuleDiagnostic.h
    ${LQMEAS_DIR}/devs/vi/data/state/diagnostic/ViDiagnosticAdcState.h
    ${LQMEAS_DIR}/devs/vi/data/state/diagnostic/ViModuleFaults.h
    ${LQMEAS_DIR}/devs/vi/data/state/diagnostic/ViModuleFaults.cpp
    ${LQMEAS_DIR}/devs/vi/data/req/ViAcqChSetFlagReqData.h    
    ${LQMEAS_DIR}/devs/vi/data/req/ViWfReqData.h
    ${LQMEAS_DIR}/devs/vi/data/req/ViDoutResetReqData.h
    ${LQMEAS_DIR}/devs/vi/data/req/ViDoutResetReqData.cpp
    ${LQMEAS_DIR}/devs/vi/data/req/ViDoutManualChangeReqData.h
    ${LQMEAS_DIR}/devs/vi/data/req/ViDoutManualChangeReqData.cpp




    ${LQMEAS_DIR}/devs/vi/protocol/vi_opcua_map.h
    ${LQMEAS_DIR}/devs/vi/protocol/vi_modbus_map.h
    ${LQMEAS_DIR}/devs/vi/protocol/vi_modbus_cmds.h
    ${LQMEAS_DIR}/devs/vi/protocol/vi_module_dev_faults.h
    ${LQMEAS_DIR}/devs/vi/protocol/vi_module_diagn_adc.h
    ${LQMEAS_DIR}/devs/vi/protocol/vi_module_errs.h
    ${LQMEAS_DIR}/devs/vi/protocol/vi_module_iface_settings.h
    ${LQMEAS_DIR}/devs/vi/protocol/vi_module_info.h
    ${LQMEAS_DIR}/devs/vi/protocol/vi_module_opcfg.h
    ${LQMEAS_DIR}/devs/vi/protocol/vi_module_state.h
    ${LQMEAS_DIR}/devs/vi/protocol/vi_module_types_descr.h
    ${LQMEAS_DIR}/devs/vi/protocol/vi_module_types_descr.c
    ${LQMEAS_DIR}/devs/vi/protocol/vi_module_types.h
    ${LQMEAS_DIR}/devs/vi/protocol/vi_net_autodisc.h
    ${LQMEAS_DIR}/devs/vi/protocol/vi_net_port.h
    ${LQMEAS_DIR}/devs/vi/protocol/vi_system_types_descr.h
    ${LQMEAS_DIR}/devs/vi/protocol/vi_system_types_descr.c
    ${LQMEAS_DIR}/devs/vi/protocol/vi_system_types.h



    ${LQMEAS_DIR}/devs/vi/netdiscovery/ViNetAutodiscoveryServiceType.h
    ${LQMEAS_DIR}/devs/vi/netdiscovery/ViNetAutodiscoveryServiceType.cpp
    ${LQMEAS_DIR}/devs/vi/netdiscovery/ViNetAutodiscoveryRecordInfo.h
    ${LQMEAS_DIR}/devs/vi/netdiscovery/ViNetAutodiscoveryRecordInfo.cpp

    ${LQMEAS_DIR}/devs/vi/connection/mb/ViModuleMbConnection.h
    ${LQMEAS_DIR}/devs/vi/connection/mb/ViModuleMbConnection.cpp
    ${LQMEAS_DIR}/devs/vi/connection/mb/ViModuleMbErrors.h
    ${LQMEAS_DIR}/devs/vi/connection/mb/ViModuleMbErrors.cpp

)




if(LQMEAS_DEV_VI_GROUP)


    #ищем libmodbus
    find_package(MODBUS REQUIRED)
    #включаем найденный путь к заголовкам limodbus и саму библиотеку
    set(LQMEAS_INCLUDE_DIRS ${LQMEAS_INCLUDE_DIRS} ${MODBUS_INCLUDE_DIRS})
    set(LQMEAS_LIBRARIES    ${LQMEAS_LIBRARIES} ${MODBUS_LIBRARIES})

    if(LQMEAS_USE_OPCUA)
        if (UNIX)
            find_package(open62541 REQUIRED COMPONENTS FullNamespace)
            set(LQMEAS_LIBRARIES    ${LQMEAS_LIBRARIES} open62541::open62541)
        endif(UNIX)
        if(WIN32)
            #включаем найденный путь к заголовкам limodbus и саму библиотеку
            set(LQMEAS_INCLUDE_DIRS ${LQMEAS_INCLUDE_DIRS} ${OPEN62541_INCLUDE_DIRS})
            set(LQMEAS_LIBRARIES    ${LQMEAS_LIBRARIES} ${OPEN62541_LIBRARIES})
        endif(WIN32)
        set(LQMEAS_USE_VI_OPCUA ON)
    endif(LQMEAS_USE_OPCUA)
endif(LQMEAS_DEV_VI_GROUP)

add_optional_files(LQMEAS_USE_VI_OPCUA LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
    ${LQMEAS_DIR}/devs/vi/connection/opcua/ViModuleOpcUaConnection.h
    ${LQMEAS_DIR}/devs/vi/connection/opcua/ViModuleOpcUaConnection.cpp
    ${LQMEAS_DIR}/devs/vi/connection/opcua/ViModuleOpcUaErrors.h
    ${LQMEAS_DIR}/devs/vi/connection/opcua/ViModuleOpcUaErrors.cpp
)

add_optional_files(LQMEAS_USE_NET_AUTODISCOVERY LQMEAS_SOURCES LQMEAS_TRANSLATION_SOURCES
    ${LQMEAS_DIR}/devs/resolver/netdiscovery/NetAutodiscoveryServer.h
    ${LQMEAS_DIR}/devs/resolver/netdiscovery/NetAutodiscoveryServer.cpp
    ${LQMEAS_DIR}/devs/resolver/netdiscovery/NetAutodiscoveryServerList.h
    ${LQMEAS_DIR}/devs/resolver/netdiscovery/NetAutodiscoveryServerList.cpp
    ${LQMEAS_DIR}/devs/resolver/netdiscovery/NetAutodiscoveryRecord.h
    ${LQMEAS_DIR}/devs/resolver/netdiscovery/NetAutodiscoveryRecord.cpp
    ${LQMEAS_DIR}/devs/resolver/netdiscovery/NetAutodiscoveryRecordInfo.h
    ${LQMEAS_DIR}/devs/resolver/netdiscovery/NetAutodiscoveryRecordInfo.cpp
    ${LQMEAS_DIR}/devs/resolver/netdiscovery/NetAutodiscoveryServiceType.h
    ${LQMEAS_DIR}/devs/resolver/netdiscovery/NetAutodiscoveryServiceType.cpp
)
if(LQMEAS_USE_NET_AUTODISCOVERY)
    include(${LQMEAS_DIR}/subs/QMdnsEngine/qmdnsengine.cmake)
    set(LQMEAS_SOURCES          ${LQMEAS_SOURCES} ${QMDNSENGINE_SOURCES})
    set(LQMEAS_GENFILES         ${LQMEAS_GENFILES} ${QMDNSENGINE_GENFILES})
    set(LQMEAS_LIBRARIES        ${LQMEAS_LIBRARIES}    ${QMDNSENGINE_LIBRARIES})
    set(LQMEAS_INCLUDE_DIRS     ${LQMEAS_INCLUDE_DIRS} ${QMDNSENGINE_INCLUDE_DIRS})
    set(LQMEAS_COMPILE_DEFINITIONS ${LQMEAS_COMPILE_DEFINITIONS} ${QMDNSENGINE_COMPILE_DEFINITIONS})
endif(LQMEAS_USE_NET_AUTODISCOVERY)

if(LQMEAS_USE_FAST_CRC)    
    set(LQMEAS_SOURCES  ${LQMEAS_SOURCES}
        ${LQMEAS_DIR}/subscfg/fast_crc_cfg.h
        ${LQMEAS_DIR}/subs/crc/fast_crc.h
        ${LQMEAS_DIR}/subs/crc/fast_crc.c
    )
endif(LQMEAS_USE_FAST_CRC)

if(LQMEAS_USE_CRC)
    set(LQMEAS_SOURCES  ${LQMEAS_SOURCES}
        ${LQMEAS_DIR}/subscfg/crclib_cfg.h
        ${LQMEAS_DIR}/subs/crc/crc.h
        ${LQMEAS_DIR}/subs/crc/crc.c
    )
endif(LQMEAS_USE_CRC)

if(LQMEAS_USE_FAST_CRC OR LQMEAS_USE_CRC)
    set(LQMEAS_INCLUDE_DIRS ${LQMEAS_INCLUDE_DIRS} ${LQMEAS_DIR}/subscfg  ${LQMEAS_DIR}/subs/crc)
endif(LQMEAS_USE_FAST_CRC OR LQMEAS_USE_CRC)


list(FIND LQMEAS_DEVS Agilent3458 Agilent3458_IDX)
if(NOT Agilent3458_IDX EQUAL -1)
    set(LQMEAS_DEV_AGILENT_3458  ON)

    set(LQMEAS_INCLUDE_DIRS ${LQMEAS_INCLUDE_DIRS} ${AG3458API_INCLUDE_DIR})
    set(LQMEAS_LIBRARY_DIRS ${LQMEAS_LIBRARY_DIRS} ${AG3458API_LIBRARIES_DIR})

    set(LQMEAS_LIBRARIES ${LQMEAS_LIBRARIES} Agilent3458api)

    set(LQMEAS_SOURCES ${LQMEAS_SOURCES}
        ${LQMEAS_DIR}/devs/gpib/gpibResolver.h
        ${LQMEAS_DIR}/devs/gpib/gpibResolver.cpp
        ${LQMEAS_DIR}/devs/gpib/Agilent3458.h
        ${LQMEAS_DIR}/devs/gpib/Agilent3458.cpp
    )
endif()

configure_file(${LQMEAS_DIR}/lqmeas_config.h.in ${CMAKE_CURRENT_BINARY_DIR}/lqmeas_config.h)
set(LQMEAS_INCLUDE_DIRS ${LQMEAS_INCLUDE_DIRS} ${CMAKE_CURRENT_BINARY_DIR})


include(${LQMEAS_DIR}/translations/cmake/qtranslation.cmake)

set(LQMEAS_TS_BASENAME lqmeas)
qtranslation_generate(LQMEAS_TRANSLATION_FILES ${LQMEAS_DIR}/translations ${LQMEAS_TS_BASENAME} ${LQMEAS_TRANSLATION_SOURCES})
set(LQMEAS_GENFILES ${LQMEAS_GENFILES}  ${LQMEAS_TRANSLATION_FILES})

set(LQMEAS_FILES  ${LQMEAS_SOURCES} ${LQMEAS_GENFILES})

