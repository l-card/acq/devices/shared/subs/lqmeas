#ifndef LQMEAS_STDERRORS_H
#define LQMEAS_STDERRORS_H

#include "LQError.h"
#include <QObject>

namespace LQMeas {
    class StdErrors : public QObject {
        Q_OBJECT
    public:
        static LQError DeviceUnsupConfig();
        static LQError SendBusy();
        static LQError StreamStopWait();
        static LQError RecvInsufficientData();
        static LQError InvalidAdcConfig();
        static LQError FeatureNotImplemented();
        static LQError OutSignalPointCount();
        static LQError OutGenRunning();
        static LQError InvalidConfigType();
        static LQError DataProcess();
        static LQError UnsupOutSyncRamMode();
        static LQError DeviceDisconnected();
        static LQError NoChannelEnabled();
        static LQError UnsupportedSignalType();
        static LQError UnsupportedTEDSNodeFamily(quint8 family);
        static LQError DevChNotInTEDSAccessMode();
        static LQError TareInvalidOp();
        static LQError TareInvalidPoint();
        static LQError RecvNoData();
        static LQError UnknownUnit(const QString &id);
        static LQError OutGenState();
        static LQError OutGenStartDoneTimeout();
        static LQError OutGenSigUpdateDoneTimeout();
        static LQError UnsupportedFeature();
        static LQError DeviceIsNotOpen();
    private:
        static LQError error(int code, const QString &msg);
    };
}

#endif // LQMEAS_STDERRORS_H

