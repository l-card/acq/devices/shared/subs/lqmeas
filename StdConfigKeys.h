#ifndef STDCONFIGKEYS_H
#define STDCONFIGKEYS_H

class QLatin1String;

namespace LQMeas {
    class StdConfigKeys {
    public:
        static const QLatin1String &ID();
        static const QLatin1String &enabled();
        static const QLatin1String &valid();
        static const QLatin1String &type();
        static const QLatin1String &value();
        static const QLatin1String &serialNumber();
        static const QLatin1String &version();
        static const QLatin1String &mcuVer();
        static const QLatin1String &mcuVerStr();
        static const QLatin1String &pldVer();
        static const QLatin1String &fpgaVer();
        static const QLatin1String &fpgaVerStr();
        static const QLatin1String &boardRev();        
        static const QLatin1String &ipcExtBwLF();
        static const QLatin1String &dev();
        static const QLatin1String &mezzanines();
        static const QLatin1String &customName();
        static const QLatin1String &clkId();
        static const QLatin1String &number();




        static const QLatin1String &channel();
        static const QLatin1String &channels();
        static const QLatin1String &mode();
        static const QLatin1String &rangeNum();
        static const QLatin1String &range();
        static const QLatin1String &freq();

        static const QLatin1String &acMode();
        static const QLatin1String &adcFreq();
        static const QLatin1String &adcFreqCode();
        static const QLatin1String &adcDataFmt();
        static const QLatin1String &iSrcValue();
        static const QLatin1String &frameSize();
        static const QLatin1String &frameFreq();

        static const QLatin1String &phaseCorEn();
        static const QLatin1String &pullups();
        static const QLatin1String &threshold();
        static const QLatin1String &high();
        static const QLatin1String &low();
        static const QLatin1String &edge();
        static const QLatin1String &upper();
        static const QLatin1String &under();
        static const QLatin1String &polarity();
        static const QLatin1String &position();



        static const QLatin1String &outputs();
        static const QLatin1String &dac();
        static const QLatin1String &dout();
        static const QLatin1String &din();
        static const QLatin1String &dacChannels();
        static const QLatin1String &doutChannels();
        static const QLatin1String &outFreq();
        static const QLatin1String &dacDataFmt();

        static const QLatin1String &points();
        static const QLatin1String &steps();
        static const QLatin1String &signal();
        static const QLatin1String &timeMs();
        static const QLatin1String &duration();
        static const QLatin1String &delay();

        static const QLatin1String &syncModeEn();
        static const QLatin1String &syncMode();
        static const QLatin1String &marks();
        static const QLatin1String &markStart();
        static const QLatin1String &markSecond();
        static const QLatin1String &syncStartMode();
        static const QLatin1String &syncConvMode();
        static const QLatin1String &event();

        static const QLatin1String &ip();
        static const QLatin1String &ipv4Addr();
        static const QLatin1String &conTout();        
        static const QLatin1String &netSvc();
        static const QLatin1String &instanceName();

        static const QLatin1String &initValue();
        static const QLatin1String &minValue();
        static const QLatin1String &maxValue();
    };
};

#endif // STDCONFIGKEYS_H
