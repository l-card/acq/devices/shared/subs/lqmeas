#ifndef LQMEAS_QLISTHELPER_H
#define LQMEAS_QLISTHELPER_H

#include <QList>

template<typename ItemType> class QListHelper {
public:
    static void move(QList<ItemType> list, int from, int to, int count) {
        Q_ASSERT(from + count <= list.size());
        Q_ASSERT(to + count <= list.size());
        Q_ASSERT(from >= 0);
        Q_ASSERT(to >= 0);
        Q_ASSERT(count >= 0);

        int move_offs = to - from;
        if (move_offs > 0) {
            for (int i = from + count - 1; i >= from; --i) {
                ItemType item = list.takeAt(i);
                list.insert(i+move_offs, item);
            }
        } else if (move_offs < 0) {
            for (int i = from; i < from + count; ++i) {
                ItemType item = list.takeAt(i);
                list.insert(i+move_offs, item);
            }
        }
    }
};

#endif // LQMEAS_QLISTHELPER_H
