#ifndef LQMEAS_LQTYPEREG_H
#define LQMEAS_LQTYPEREG_H

#include <QMetaType>
namespace LQMeas {
    template<typename type> class LQTypeReg {
    public:
        LQTypeReg(const char *name) {
            qRegisterMetaType<type>(name);
        }
    };
}


#define LQTYPE_REGISTER(classtype) do { \
    static const LQMeas::LQTypeReg< classtype > typereg( #classtype ); \
    (void)typereg; \
} while(0)

#endif // LQMEAS_LQTYPEREG_H
