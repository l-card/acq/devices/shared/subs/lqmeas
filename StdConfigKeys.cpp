#include "StdConfigKeys.h"
#include <QLatin1String>

namespace LQMeas {
    const QLatin1String &StdConfigKeys::ID() {
        static const QLatin1String key {"ID"};
        return key;
    }

    const QLatin1String &StdConfigKeys::enabled() {
        static const QLatin1String key {"Enabled"};
        return key;
    }

    const QLatin1String &StdConfigKeys::valid() {
        static const QLatin1String key {"Valid"};
        return key;
    }

    const QLatin1String &StdConfigKeys::type() {
        static const QLatin1String key {"Type"};
        return key;
    }

    const QLatin1String &StdConfigKeys::value() {
        static const QLatin1String key {"Value"};
        return key;
    }

    const QLatin1String &StdConfigKeys::serialNumber() {
        static const QLatin1String key {"Serial"};
        return key;
    }

    const QLatin1String &StdConfigKeys::version() {
        static const QLatin1String key {"Version"};
        return key;
    }

    const QLatin1String &StdConfigKeys::mcuVer() {
        static const QLatin1String key {"McuVer"};
        return key;
    }

    const QLatin1String &StdConfigKeys::mcuVerStr() {
        static const QLatin1String key {"McuVerStr"};
        return key;
    }

    const QLatin1String &StdConfigKeys::pldVer() {
        static const QLatin1String key {"PldVer"};
        return key;
    }

    const QLatin1String &StdConfigKeys::fpgaVer() {
        static const QLatin1String key {"FpgaVer"};
        return key;
    }

    const QLatin1String &StdConfigKeys::fpgaVerStr() {
        static const QLatin1String key {"FpgaVerStr"};
        return key;
    }

    const QLatin1String &StdConfigKeys::boardRev() {
        static const QLatin1String key {"BoardRev"};
        return key;
    }

    const QLatin1String &StdConfigKeys::ipcExtBwLF() {
        static const QLatin1String key {"ICPExtBwLF"};
        return key;
    }

    const QLatin1String &StdConfigKeys::dev() {
        static const QLatin1String key {"Dev"};
        return key;
    }

    const QLatin1String &StdConfigKeys::mezzanines() {
        static const QLatin1String key {"Mezzanines"};
        return key;
    }

    const QLatin1String &StdConfigKeys::customName() {
        static const QLatin1String key {"CustomName"};
        return key;
    }

    const QLatin1String &StdConfigKeys::clkId() {
        static const QLatin1String key {"ClkID"};
        return key;
    }

    const QLatin1String &StdConfigKeys::number() {
        static const QLatin1String key {"Number"};
        return key;
    }

    const QLatin1String &StdConfigKeys::netSvc() {
        static const QLatin1String key {"NetSvc"};
        return key;
    }

    const QLatin1String &StdConfigKeys::instanceName() {
        static const QLatin1String key {"InstanceName"};
        return key;
    }


    const QLatin1String &StdConfigKeys::initValue() {
        static const QLatin1String str { "InitValue" };
        return str;
    }

    const QLatin1String &StdConfigKeys::minValue() {
        static const QLatin1String str { "MinValue" };
        return str;
    }

    const QLatin1String &StdConfigKeys::maxValue() {
        static const QLatin1String str { "MaxValue" };
        return str;
    }

    const QLatin1String &StdConfigKeys::channel() {
        static const QLatin1String key {"Channel"};
        return key;
    }

    const QLatin1String &StdConfigKeys::mode() {
        static const QLatin1String key {"Mode"};
        return key;
    }

    const QLatin1String &StdConfigKeys::rangeNum() {
        static const QLatin1String key {"RangeNum"};
        return key;
    }

    const QLatin1String &StdConfigKeys::range() {
        static const QLatin1String key {"Range"};
        return key;
    }

    const QLatin1String &StdConfigKeys::freq() {
        static const QLatin1String key {"Freq"};
        return key;
    }

    const QLatin1String &StdConfigKeys::acMode() {
        static const QLatin1String key {"AcMode"};
        return key;
    }

    const QLatin1String &StdConfigKeys::adcFreq() {
        static const QLatin1String key {"AdcFreq"};
        return key;
    }

    const QLatin1String &StdConfigKeys::adcFreqCode() {
        static const QLatin1String key {"AdcFreqCode"};
        return key;
    }

    const QLatin1String &StdConfigKeys::adcDataFmt() {
        static const QLatin1String key {"AdcDataFmt"};
        return key;
    }

    const QLatin1String &StdConfigKeys::iSrcValue() {
        static const QLatin1String key {"ISrcValue"};
        return key;
    }

    const QLatin1String &StdConfigKeys::frameSize() {
        static const QLatin1String key {"FrameSize"};
        return key;
    }

    const QLatin1String &StdConfigKeys::frameFreq() {
        static const QLatin1String key {"FrameFreq"};
        return key;
    }

    const QLatin1String &StdConfigKeys::phaseCorEn() {
        static const QLatin1String key {"PhaseCor"};
        return key;
    }

    const QLatin1String &StdConfigKeys::pullups() {
        static const QLatin1String key {"Pullups"};
        return key;
    }

    const QLatin1String &StdConfigKeys::threshold() {
        static const QLatin1String str {"Threshold"};
        return str;
    }

    const QLatin1String &StdConfigKeys::high() {
        static const QLatin1String str {"High"};
        return str;
    }

    const QLatin1String &StdConfigKeys::low() {
        static const QLatin1String str {"Low"};
        return str;
    }

    const QLatin1String &StdConfigKeys::edge() {
        static const QLatin1String str {"Edge"};
        return str;
    }

    const QLatin1String &StdConfigKeys::upper() {
        static const QLatin1String str {"Upper"};
        return str;
    }

    const QLatin1String &StdConfigKeys::under() {
        static const QLatin1String str {"Under"};
        return str;
    }

    const QLatin1String &StdConfigKeys::polarity() {
        static const QLatin1String str {"Polarity"};
        return str;
    }

    const QLatin1String &StdConfigKeys::position() {
        static const QLatin1String str {"Position"};
        return str;
    }


    const QLatin1String &StdConfigKeys::outFreq() {
        static const QLatin1String key {"OutFreq"};
        return key;
    }

    const QLatin1String &StdConfigKeys::syncModeEn() {
        static const QLatin1String key {"SyncModeEn"};
        return key;
    }

    const QLatin1String &StdConfigKeys::syncMode() {
        static const QLatin1String key {"SyncMode"};
        return key;
    }

    const QLatin1String &StdConfigKeys::dacDataFmt() {
        static const QLatin1String key {"DacDataFmt"};
        return key;
    }

    const QLatin1String &StdConfigKeys::points() {
        static const QLatin1String key {"Points"};
        return key;
    }

    const QLatin1String &StdConfigKeys::steps() {
        static const QLatin1String key {"Steps"};
        return key;
    }

    const QLatin1String &StdConfigKeys::signal() {
        static const QLatin1String key {"Signal"};
        return key;
    }

    const QLatin1String &StdConfigKeys::timeMs() {
        static const QLatin1String key {"TimeMS"};
        return key;
    }

    const QLatin1String &StdConfigKeys::duration() {
        static const QLatin1String str { "Duration" };
        return str;
    }

    const QLatin1String &StdConfigKeys::delay() {
        static const QLatin1String str { "Delay" };
        return str;
    }

    const QLatin1String &StdConfigKeys::marks() {
        static const QLatin1String key {"Marks"};
        return key;
    }

    const QLatin1String &StdConfigKeys::markStart() {
        static const QLatin1String key {"Start"};
        return key;
    }

    const QLatin1String &StdConfigKeys::markSecond() {
        static const QLatin1String key {"Second"};
        return key;
    }

    const QLatin1String &StdConfigKeys::syncStartMode() {
        static const QLatin1String key {"SyncStartMode"};
        return key;
    }

    const QLatin1String &StdConfigKeys::syncConvMode() {
        static const QLatin1String key {"SyncConvMode"};
        return key;
    }

    const QLatin1String &StdConfigKeys::event() {
        static const QLatin1String key {"Event"};
        return key;
    }

    const QLatin1String &StdConfigKeys::ip() {
        static const QLatin1String key {"IP"};
        return key;
    }

    const QLatin1String &StdConfigKeys::channels() {
        static const QLatin1String key {"Channels"};
        return key;
    }

    const QLatin1String &StdConfigKeys::dacChannels() {
        static const QLatin1String key {"DacChannels"};
        return key;
    }

    const QLatin1String &StdConfigKeys::doutChannels() {
        static const QLatin1String key {"DoutChannels"};
        return key;
    }


    const QLatin1String &StdConfigKeys::outputs() {
        static const QLatin1String key {"Outputs"};
        return key;
    }

    const QLatin1String &StdConfigKeys::dac() {
        static const QLatin1String key {"DAC"};
        return key;
    }

    const QLatin1String &StdConfigKeys::dout() {
        static const QLatin1String key {"DOUT"};
        return key;
    }

    const QLatin1String &StdConfigKeys::din()  {
        static const QLatin1String key {"DIN"};
        return key;
    }

    const QLatin1String &StdConfigKeys::ipv4Addr() {
        static const QLatin1String key {"IPv4Addr"};
        return key;
    }

    const QLatin1String &LQMeas::StdConfigKeys::conTout() {
        static const QLatin1String key {"ConTout"};
        return key;
    }

}
