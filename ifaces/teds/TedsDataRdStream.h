#ifndef TEDSDATARDSTREAM_H
#define TEDSDATARDSTREAM_H

#include "info/TedsInfo.h"
#include <QByteArray>
class LQError;

namespace LQMeas {
    class TedsDataRdStream {
    public:
        TedsDataRdStream();
        TedsDataRdStream(const QByteArray &array);

        const QByteArray &rawData() const {return  m_raw_data;}
        const TedsBasicInfo &getBasicInfo(LQError &err);
        const TedsInfo &getFullData(LQError &err);
    private:
        void getError(int errCode, LQError &err) const;
        QString errorString(int err) const;


        enum class State {
            Init,
            BasicParsed,
            FullParased
        } m_state {State::Init};

        QByteArray m_raw_data;
        TedsInfo m_data;
        TLTEDS_DECODE_CONTEXT m_ctx;
    };
}

#endif // TEDSDATARDSTREAM_H
