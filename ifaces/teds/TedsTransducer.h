#ifndef LQMEAS_TEDSTRANSDUCER_H
#define LQMEAS_TEDSTRANSDUCER_H

class LQError;

namespace LQMeas {
    class TedsBasicInfo;
    class TedsDataRdStream;
    class SensorTypeInfo;

    /* Интерфейс для обработки данных TEDS преобразователем */
    class TedsTransducer {
    public:
        virtual ~TedsTransducer();

        /* проверка, какомой модели датчика соответствует basic TEDS.
         * возвращает нулевой указатель, если basic TEDS не соответствует
         * ни одной модели из поддерживаемых классом */
        virtual const SensorTypeInfo *tedsCheckType(const TedsBasicInfo &info) const = 0;
        /* разбор данных TEDS и изменение всей конфигурации и информации о
         * преобразователе в соответствие с разобранными данными */
        virtual void tedsParseData(TedsDataRdStream &rdData, LQError &err) = 0;
    };
}

#endif // LQMEAS_TEDSTRANSDUCER_H
