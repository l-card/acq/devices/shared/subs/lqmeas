#ifndef LQMEAS_DEVTEDSACCESS_H
#define LQMEAS_DEVTEDSACCESS_H

#include <QByteArray>
class LQError;

namespace LQMeas {
    class TedsDataRdStream;

    /* интерфейс устройства для доступа к данным TEDS подключенных преобразователей */
    class DevTedsAccess {
    public:
        DevTedsAccess();
        virtual ~DevTedsAccess();

        /* переход в режим доступа к данным TEDS подключенных преобразователей.
           ch_mask - маска каналов, которые будут опрашиваться */
        void tedsAccessStart(unsigned ch_mask, LQError &err);
        /* завершение доступа к TEDS и переход в штатный режим работы устройства */
        void tedsAccessFinish(LQError &err);
        /* чтение данных из памяти TEDS датчика, подключенного к указанному каналу.
         * до этого устройство должно быть переведено в режим доступа к датчикам
         * TEDS через tedsAccessStart() */
        void tedsMemRead(int ch, TedsDataRdStream &rdStream, LQError &err);
    protected:
        virtual void protTedsAccessStart(unsigned ch_mask, LQError &err) = 0;
        virtual void protTedsAccessFinish(LQError &err) = 0;
        virtual QByteArray protTedsMemRead(int ch, LQError &err) = 0;
    private:
        unsigned m_ch_acc_mask;
    };
}

#endif // LQMEAS_DEVTEDSACCESS_H
