#include "TedsDataRdStream.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/devs/LTR/LTRNativeError.h"

namespace LQMeas {
    TedsDataRdStream::TedsDataRdStream() {

    }

    TedsDataRdStream::TedsDataRdStream(const QByteArray &array) :
        m_raw_data{array} {

        LTEDS_DecodeInit(&m_ctx, reinterpret_cast<const BYTE *>(m_raw_data.data()),
                         static_cast<DWORD>(m_raw_data.size()), 0);
    }

    const TedsBasicInfo &TedsDataRdStream::getBasicInfo(LQError &err) {
        if (m_state == State::Init) {
            if (err.isSuccess()) {
                getError(LTEDS_GetBasicInfo(&m_ctx, &m_data.basicInfo().rawInfo()), err);
                if (err.isSuccess()) {
                    m_state = State::BasicParsed;
                }
            }
        }
        return  m_data.basicInfo();
    }

    const TedsInfo &TedsDataRdStream::getFullData(LQError &err) {
        if (m_state == State::Init) {
            (void)getBasicInfo(err);
        }
        if (m_state == State::BasicParsed) {
            /** @todo full parse */

        }

        return  m_data;
    }

    void TedsDataRdStream::getError(int errCode, LQError &err) const {
        if (errCode != LTEDS_OK) {
            err = LTRNativeError::error(errCode, errorString(errCode));
        }
    }

    QString TedsDataRdStream::errorString(int errCode) const {
        return QSTRING_FROM_CSTR(LTEDS_GetErrorString(errCode));
    }
}
