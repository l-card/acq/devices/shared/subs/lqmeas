#ifndef LQMEAS_TEDSINFO_H
#define LQMEAS_TEDSINFO_H

#include "TedsBasicInfo.h"

namespace LQMeas {
    class TedsInfo {
    public:
        TedsInfo();

        const TedsBasicInfo &basicInfo() const {return  m_basic_info;}
        TedsBasicInfo &basicInfo() {return  m_basic_info;}
    private:
        TedsBasicInfo m_basic_info;
        /** @todo дескрипторы */
    };
}

#endif // LQMEAS_TEDSINFO_H
