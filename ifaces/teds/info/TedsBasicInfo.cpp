#include "TedsBasicInfo.h"

namespace LQMeas {
    TedsBasicInfo::TedsBasicInfo() {
        memset(&m_info, 0, sizeof (m_info));
    }

    TedsBasicInfo::TedsBasicInfo(const TLTEDS_INFO_BASIC &info)  {
        m_info = info;
    }

    QString TedsBasicInfo::manufacturer() const {
        return  manufacturerId() == LTEDS_LCARD_MANUFACTURER_ID ? QStringLiteral("L Card") :
                                                                  QString::number(manufacturerId());
    }

    QString TedsBasicInfo::model() const {
        return  QString::number(modelNumber());
    }
}
