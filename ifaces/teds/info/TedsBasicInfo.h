#ifndef LQMEAS_TEDSBASICINFO_H
#define LQMEAS_TEDSBASICINFO_H

#include "ltr/include/ltedsapi.h"
#include <QChar>
#include <QString>

namespace LQMeas {
    class TedsBasicInfo  {
    public:
        TedsBasicInfo();
        TedsBasicInfo(const TLTEDS_INFO_BASIC &info);

        int manufacturerId() const {return  m_info.ManufacturerID;}        
        int modelNumber() const {return m_info.ModelNumber;}
        int versionNumber() const {return  m_info.VersionNumber;}
        QChar versionLetter() const {return  QChar::fromLatin1(m_info.VersionLetter);}
        QString serialNumber() const {return  QString::number(m_info.SerialNumber);}
        int serialNumberValue() const {return  static_cast<int>(m_info.SerialNumber);}

        QString manufacturer() const;
        QString model() const;

        const TLTEDS_INFO_BASIC &rawInfo() const {return  m_info;}
        TLTEDS_INFO_BASIC &rawInfo() {return  m_info;}
    private:
        TLTEDS_INFO_BASIC m_info;
    };
}

#endif // LQMEAS_TEDSBASICINFO_H
