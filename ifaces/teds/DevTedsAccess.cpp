#include "DevTedsAccess.h"
#include "TedsDataRdStream.h"
#include "lqmeas/StdErrors.h"

namespace LQMeas {
    DevTedsAccess::DevTedsAccess() : m_ch_acc_mask(0) {

    }

    DevTedsAccess::~DevTedsAccess() {

    }

    void DevTedsAccess::tedsAccessStart(unsigned ch_mask, LQError &err) {
        protTedsAccessStart(ch_mask, err);
        if (err.isSuccess()) {
            m_ch_acc_mask = ch_mask;
        }
    }

    void DevTedsAccess::tedsAccessFinish(LQError &err) {
        protTedsAccessFinish(err);
        if (err.isSuccess()) {
            m_ch_acc_mask = 0;
        }

    }

    void DevTedsAccess::tedsMemRead(int ch, TedsDataRdStream &rdStream, LQError &err) {
        if (m_ch_acc_mask & (1 << ch)) {
            QByteArray data = protTedsMemRead(ch, err);
            if (err.isSuccess()) {
                rdStream = TedsDataRdStream(data);
            }
        } else {
            err = StdErrors::DevChNotInTEDSAccessMode();
        }
    }
}
