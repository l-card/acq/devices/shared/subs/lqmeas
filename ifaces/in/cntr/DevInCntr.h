#ifndef DEVINCNTR_H
#define DEVINCNTR_H

#include "lqmeas/devs/Device.h"
#include "DevInCntrData.h"
class LQError;


namespace LQMeas {
    class DevInCntrConfig;

    class DevInCntr {
    public:
        const DevInCntrConfig &inCntrConfig() const;

        void inCntrEnable(LQError &err);
        void inCntrDisable(LQError &err);

        void inCntrStart(LQError &err);
        void inCntrStop(LQError &err);

        virtual bool inCntrIsRunning() const {return m_run;}

        void inCntrGetData(DevInCntrData *data, int size, unsigned flags,
                           unsigned tout, int &recvd_size, LQError &err);
        void inCntrGetData(DevInCntrData *data, int size, unsigned flags,
                           unsigned tout, LQError &err);
    protected:
        virtual void protInCntrEnable (LQError &err) {Q_UNUSED(err)}
        virtual void protInCntrDisable (LQError &err) {Q_UNUSED(err)}
        virtual void protInCntrStart(LQError &err) = 0;
        virtual void protInCntrStop(LQError &err) = 0;
        virtual void protInCntrGetData(DevInCntrData *data, int size, unsigned flags,
                                        unsigned tout, int &recvd_size, LQError &err) = 0;

        explicit DevInCntr(Device *dev);
    private:

        bool m_run {false};
        Device *m_dev;
    };
}


#endif // DEVINCNTR_H
