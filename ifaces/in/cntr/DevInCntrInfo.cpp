#include "DevInCntrInfo.h"
#include "DevInCntrConfig.h"


namespace LQMeas {
    DevInCntrInfo::~DevInCntrInfo() {

    }

    QString DevInCntrInfo::inCntrChName(int ch_num) const {
        return DevInCntrConfig::tr("Counter %1").arg(ch_num + 1);
    }
}
