#include "DevInCntr.h"
#include "lqmeas/StdErrors.h"
#include "lqmeas/devs/Device.h"
#include "lqmeas/devs/DeviceConfig.h"

namespace LQMeas {

    const DevInCntrConfig &DevInCntr::inCntrConfig() const {
        return *m_dev->config().inCntrConfig();
    }

    void DevInCntr::inCntrEnable(LQError &err)  {
        protInCntrEnable(err);
    }

    void DevInCntr::inCntrDisable(LQError &err) {
        protInCntrDisable(err);
    }

    void DevInCntr::inCntrStart(LQError &err) {
        inCntrEnable(err);
        if (err.isSuccess()) {
            protInCntrStart(err);
        }

        if (err.isSuccess()) {
            m_run = true;
        }
    }

    void DevInCntr::inCntrStop(LQError &err) {
        LQError stop_err;
        inCntrDisable(err);
        protInCntrStop(stop_err);
        err += stop_err;

        if (err.isSuccess()) {
            m_run = false;
        }
    }

    void DevInCntr::inCntrGetData(DevInCntrData *data, int size, unsigned flags, unsigned tout, int &recvd_size, LQError &err) {
        return protInCntrGetData(data, size, flags, tout, recvd_size, err);
    }

    void DevInCntr::inCntrGetData(DevInCntrData *data, int size, unsigned flags, unsigned tout, LQError &err)     {
        int recvd_size = 0;
        inCntrGetData(data, size, flags, tout, recvd_size, err);
        if (err.isSuccess()) {
            if (recvd_size == 0) {
                err = StdErrors::RecvNoData();
            } else if (recvd_size != size) {
                err = StdErrors::RecvInsufficientData();
            }
        }
    }

    DevInCntr::DevInCntr(Device *dev) : m_dev{dev} {

    }

}
