#ifndef DEVINCNTRINFO_H
#define DEVINCNTRINFO_H

#include <QtGlobal>

namespace LQMeas {
    class DevInCntrInfo {
    public:
        virtual ~DevInCntrInfo();
        virtual int inCntrChannelsCnt() const = 0;
        virtual bool inCntrChAviable(int ch_num) const { Q_UNUSED(ch_num) return true;}

        virtual QString inCntrChName(int ch_num) const;
        virtual double inCntrFreqMax(void) const = 0;
        virtual double inCntrFreqDefault(void) const {return inCntrFreqMax();}
    };
}

#endif // DEVINCNTRINFO_H
