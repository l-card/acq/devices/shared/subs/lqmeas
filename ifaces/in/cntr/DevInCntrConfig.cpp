#include "DevInCntrConfig.h"
#include "DevInCntrInfo.h"
#include "lqmeas/devs/DeviceTypeInfo.h"


namespace LQMeas {
    DevInCntrConfig::DevInCntrConfig(const DevInCntrInfo &devinfo) : m_in_cntr_info{&devinfo} {

    }

    DevInCntrConfig::DevInCntrConfig(const DevInCntrConfig &cfg) :
        m_in_cntr_info{&cfg.inCntrInfo()},
        m_ch_indexes{cfg.m_ch_indexes} {

    }

    bool DevInCntrConfig::inCntrChAvailable(int ch_num) const {
        return inCntrInfo().inCntrChAviable(ch_num);
    }

    int DevInCntrConfig::inCntrEnabledChCnt() const {
        int ch_cnt {0};
        for (int ch_num {0}; ch_num < m_in_cntr_info->inCntrChannelsCnt(); ++ch_num) {
            if (inCntrChEnabled(ch_num) && inCntrChAvailable(ch_num)) {
                ++ch_cnt;
            }
        }
        return ch_cnt;
    }

    int DevInCntrConfig::inCntrChNum(int idx) const {
        return idx < m_ch_indexes.size() ? m_ch_indexes[idx] : -1;
    }

    int DevInCntrConfig::inCntrChIdx(int ch_num) const {
        return m_ch_indexes.indexOf(ch_num);
    }

    void DevInCntrConfig::protUpdate() {
        m_ch_indexes.clear();
        for (int ch_num {0}; ch_num < m_in_cntr_info->inCntrChannelsCnt(); ++ch_num) {
            if (inCntrChEnabled(ch_num)) {
                m_ch_indexes.append(ch_num);
            }
        }
    }

    void DevInCntrConfig::protUpdateInfo(const DeviceTypeInfo *info) {
        DeviceConfigIface::protUpdateInfo(info);
        m_in_cntr_info = info->inCntr();
    }

    void DevInCntrConfig::notifyCntrChFreqChanged(int ch_num) {
        Q_EMIT inCntrChConfiguredFreqChanged(ch_num);
        Q_EMIT inCntrChConfigChanged(ch_num);
    }

    void DevInCntrConfig::notifyChConfigChanged(int ch_num) {
        Q_EMIT inCntrChConfigChanged(ch_num);
    }

}
