#ifndef DEVINCNTRDATA_H
#define DEVINCNTRDATA_H

#include <QtGlobal>

namespace LQMeas {
    struct DevInCntrData {
        quint16 N; /* Количество собтый за интервал */
        quint16 M; /* Расстояние от конца интервала до последнего события */
    };
}

#endif // DEVINCNTRDATA_H

