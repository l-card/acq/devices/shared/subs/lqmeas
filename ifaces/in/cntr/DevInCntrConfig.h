#ifndef DEVINCNTRCONFIG_H
#define DEVINCNTRCONFIG_H

#include "lqmeas/devs/DeviceConfigIface.h"
#include <QVector>

namespace LQMeas {
    class DevInCntrInfo;

    class DevInCntrConfig : public DeviceConfigIface {
        Q_OBJECT
    public:
        explicit DevInCntrConfig(const DevInCntrInfo &devinfo);
        explicit DevInCntrConfig(const DevInCntrConfig &cfg);
        const DevInCntrInfo &inCntrInfo() const {return *m_in_cntr_info;}

        virtual bool inCntrChAvailable(int ch_num) const;

        virtual bool inCntrChEnabled(int ch_num) const = 0;
        virtual int  inCntrEnabledChCnt() const;


        /*------- признаки, действительные только для синхронного ввода */
        /* признак, что cбор с цифровых линий запускается от внешнего источника */
        virtual bool inCntrExternalStart() const {return false;}
        /* установленная в данный момент частота ввода с цифровых линий в Гц (на каждый вход) */
        virtual double inCntrFreq() const = 0;


        /* получить номер физ. канала по индексу при приеме */
        virtual int inCntrChNum(int idx) const;
        virtual int inCntrChIdx(int ch_num) const;

    Q_SIGNALS:
        void inCntrChConfiguredFreqChanged(int ch_num);
        void inCntrChConfigChanged(int ch_num);
    protected:
        QString cfgIfaceName() const override {return  QStringLiteral("InCntr");}
        void protUpdate() override;
        void protUpdateInfo(const DeviceTypeInfo *info) override;

        void notifyCntrChFreqChanged(int ch_num = -1);
        void notifyChConfigChanged(int ch_num);
    private:
        const DevInCntrInfo *m_in_cntr_info;
        QVector<int> m_ch_indexes;

        friend class DeviceConfig;
    };
}



#endif // DEVINCNTRCONFIG_H
