#ifndef DEVINFRAMEREQ_H
#define DEVINFRAMEREQ_H

class LQError;

namespace LQMeas {
    class DevInFrameReq {
    public:
        virtual ~DevInFrameReq();
        void inFrameRequest(LQError &err);
    protected:
        virtual void protInFrameRequest(LQError &err) = 0;
    };
}

#endif // DEVINFRAMEREQ_H
