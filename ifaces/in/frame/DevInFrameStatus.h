#ifndef DEVINFRAMESTATUS_H
#define DEVINFRAMESTATUS_H

#include <QObject>
#include <QMetaType>

namespace LQMeas {
    class DevInFrameStatus {
    public:
        enum class Flag {
            None            = 0x0000,
            Overlapped      = 0x0001,
            SyncSkipped     = 0x0002,
            InvalidHistory  = 0x0010,
        };
        Q_DECLARE_FLAGS(Flags, Flag)

        DevInFrameStatus() {}
        explicit DevInFrameStatus(bool valid, Flags flags = Flag::None) :
            m_finished{true}, m_dataValid{valid}, m_flags{flags} {

        }

        bool isFinished() const {return m_finished;}
        bool isDataValid() const {return m_dataValid;}
        Flags flags() const {return m_flags;}
    private:
        bool m_finished {false};
        bool m_dataValid {true};
        Flags m_flags;
    };

    static DevInFrameStatus::Flags operator|(DevInFrameStatus::Flag lhs, DevInFrameStatus::Flag rhs) {
        return static_cast<DevInFrameStatus::Flags>(static_cast<char>(lhs) | static_cast<char>(rhs));
    }
    static DevInFrameStatus::Flags operator|(DevInFrameStatus::Flags lhs, DevInFrameStatus::Flag rhs) {
        return static_cast<DevInFrameStatus::Flags>(static_cast<char>(lhs) | static_cast<char>(rhs));
    }
    static DevInFrameStatus::Flags operator|=(DevInFrameStatus::Flags lhs, DevInFrameStatus::Flag rhs) {
        return static_cast<DevInFrameStatus::Flags>(static_cast<char>(lhs) | static_cast<char>(rhs));
    }
}

#endif // DEVINFRAMESTATUS_H
