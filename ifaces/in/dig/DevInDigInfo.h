#ifndef LQMEAS_DEVINDIGINFO_H
#define LQMEAS_DEVINDIGINFO_H

#include <QtGlobal>

namespace LQMeas {
    class DevInDigInfo {
    public:
        virtual ~DevInDigInfo();
        virtual int inDigChannelsCnt() const = 0;
        virtual bool inDigChAviable(int ch_num) const { Q_UNUSED(ch_num) return true;}

        virtual QString inDigChName(int ch_num) const;
        virtual bool inDigSyncSupport() const {return false;}
        virtual double inDigSyncFreqMax() const {return 0;}
        virtual double inDigSyncFreqDefault() const {return inDigSyncFreqMax();}
    };
}

#endif // LQMEAS_DEVINDIGINFO_H
