#ifndef LQMEAS_DEVINASYNCDIG_H
#define LQMEAS_DEVINASYNCDIG_H

class LQError;

namespace LQMeas {
    class DevInAsyncDig {
    public:
        virtual ~DevInAsyncDig();

        void inAsyncDig(unsigned &val, LQError &err);
    protected:
        virtual void protInAsyncDig(unsigned &val, LQError &err) = 0;
    };
}

#endif // LQMEAS_DEVINASYNCDIG_H
