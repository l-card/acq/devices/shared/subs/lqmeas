#ifndef LQMEAS_DEVINDIGCONFIG_H
#define LQMEAS_DEVINDIGCONFIG_H

#include "lqmeas/devs/DeviceConfigIface.h"


namespace LQMeas {
    class DevInDigInfo;

    class DevInDigConfig : public DeviceConfigIface {
        Q_OBJECT
    public:
        explicit DevInDigConfig(const DevInDigInfo &devinfo);
        explicit DevInDigConfig(const DevInDigConfig &cfg);
        const DevInDigInfo &inDigInfo() const {return *m_in_dig_info;}

        virtual bool inDigChAvailable(int ch_num) const;


        /*------- признаки, действительные только для синхронного ввода */

        /* разрешение синхронного ввода с цифровых линий устройства */
        virtual bool inDigSyncModeEnabled() const {return false;}
        /* признак, разрешен ли синхронный ввод с выбранного цифрового входа */
        virtual bool inDigChSyncModeEnabled(int ch_num) const {return inDigSyncModeEnabled();}
        /* доступен ли канал для разрешения для этой конфигурации и этого типа устройства */
        virtual bool inDigChSyncModeAvailable(int ch_num) const;

        virtual void inDigChSetSyncModeEnabled(int ch_num, bool en) { Q_UNUSED(ch_num) Q_UNUSED(en) }

        virtual int  inDigEnabledSyncChCnt() const;
        /* признак, что cбор с цифровых линий запускается от внешнего источника */
        virtual bool inDigSyncExternalStart() const {return false;}
        /* установленная в данный момент частота ввода с цифровых линий в Гц (на каждый вход) */
        virtual double inDigFreq() const {return 0;}

        virtual int inDigSampleSize() const = 0;

        virtual bool inDigPackedModeEnabled() const {return false;}
        virtual int inDigPackedModeWordBitCount() const {return 0;}
        virtual int inDigPackedModeWordBitOffset(int ch_num, int bitnum) const {return 0;}


    Q_SIGNALS:
        void inDigChConfiguredFreqChanged(int ch_num);
        void inDigChConfigChanged(int ch_num);
    protected:
        QString cfgIfaceName() const override {return  QStringLiteral("InDig");}
        void protUpdateInfo(const DeviceTypeInfo *info) override;

        void notifySyncChFreqChanged(int ch_num = -1);
        void notifyChConfigChanged(int ch_num);
    private:
        const DevInDigInfo *m_in_dig_info;

        friend class DeviceConfig;
    };
}

#endif // LQMEAS_DEVINDIGCONFIG_H
