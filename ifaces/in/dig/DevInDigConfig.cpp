#include "DevInDigConfig.h"
#include "DevInDigInfo.h"
#include "lqmeas/devs/DeviceTypeInfo.h"

namespace LQMeas {
    DevInDigConfig::DevInDigConfig(const DevInDigInfo &devinfo) : m_in_dig_info{&devinfo} {

    }

    DevInDigConfig::DevInDigConfig(const DevInDigConfig &cfg) : m_in_dig_info{&cfg.inDigInfo()} {
    }

    bool DevInDigConfig::inDigChAvailable(int ch_num) const {
        return inDigInfo().inDigChAviable(ch_num);
    }

    bool DevInDigConfig::inDigChSyncModeAvailable(int ch_num) const {
        return inDigInfo().inDigChAviable(ch_num);
    }

    int DevInDigConfig::inDigEnabledSyncChCnt() const {
        int ch_cnt {0};
        for (int ch_num {0}; ch_num < m_in_dig_info->inDigChannelsCnt(); ++ch_num) {
            if (inDigChSyncModeEnabled(ch_num) && inDigChSyncModeAvailable(ch_num)) {
                ++ch_cnt;
            }
        }
        return ch_cnt;
    }

    void DevInDigConfig::protUpdateInfo(const DeviceTypeInfo *info) {
        DeviceConfigIface::protUpdateInfo(info);
        m_in_dig_info = info->digin();
    }

    void DevInDigConfig::notifySyncChFreqChanged(int ch_num) {
        Q_EMIT inDigChConfiguredFreqChanged(ch_num);
        Q_EMIT inDigChConfigChanged(ch_num);
    }

    void DevInDigConfig::notifyChConfigChanged(int ch_num) {
        Q_EMIT inDigChConfigChanged(ch_num);
    }
}
