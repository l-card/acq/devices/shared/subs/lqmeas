#include "DevInAsyncDig.h"
#include "LQError.h"

namespace LQMeas {

    DevInAsyncDig::~DevInAsyncDig() {

    }

    void DevInAsyncDig::inAsyncDig(unsigned &val, LQError &err) {
        return protInAsyncDig(val, err);
    }
}
