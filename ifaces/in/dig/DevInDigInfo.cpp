#include "DevInDigInfo.h"
#include <QStringBuilder>

namespace LQMeas {
    DevInDigInfo::~DevInDigInfo() {

    }

    QString DevInDigInfo::inDigChName(int ch_num) const {
        return QStringLiteral("DI") % QString::number(ch_num + 1);
    }
}
