#ifndef LQMEAS_DEVINSYNCDIG_H
#define LQMEAS_DEVINSYNCDIG_H

#include "lqmeas/devs/Device.h"
class LQError;

namespace LQMeas {
    class DevInDigConfig;

    class DevInSyncDig {
    public:
        virtual ~DevInSyncDig();

        const DevInDigConfig &dinConfig() const;

        void inDigEnable(LQError &err);
        void inDigDisable(LQError &err);

        void inDigStart(LQError &err);
        void inDigStop(LQError &err);

        virtual bool inDigIsRunning() const {return m_run;}

        void inDigGetData(quint32 *data, int size, unsigned tout, LQError &err);
        void inDigGetData(quint32 *data, int size, unsigned tout, int &recvd_size, LQError &err);

    protected:
        explicit DevInSyncDig(Device *dev);

        virtual void protInDigEnable (LQError &err) {Q_UNUSED(err)}
        virtual void protInDigDisable (LQError &err) {Q_UNUSED(err)}
        virtual void protInDigStart(LQError &err) = 0;
        virtual void protInDigStop(LQError &err) = 0;
        virtual void protInDigGetData(quint32 *data, int size,
                                      unsigned tout, int &recvd_size, LQError &err) = 0;
    private:
        bool m_run {false};
        Device *m_dev;

        friend class Device;
    };
};

#endif // LQMEAS_DEVINSYNCDIG_H
