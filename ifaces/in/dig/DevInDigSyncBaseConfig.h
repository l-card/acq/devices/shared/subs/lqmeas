#ifndef LQMEAS_DEVINDIGBASECONFIG_H
#define LQMEAS_DEVINDIGBASECONFIG_H

#include "DevInDigConfig.h"

namespace LQMeas {
    class DevInDigSyncBaseConfig : public DevInDigConfig {
        Q_OBJECT
    public:
        explicit DevInDigSyncBaseConfig(const DevInDigInfo &devinfo);
        explicit DevInDigSyncBaseConfig(const DevInDigSyncBaseConfig &cfg);

        bool inDigSyncModeEnabled() const override {return m_din_params.ChEnMask != 0;}
        bool inDigChSyncModeEnabled(int ch_num) const override {return dinChEnIsSet(ch_num); }

        void inDigChSetSyncModeEnabled(int ch_num, bool en) override;

        double inDigFreq() const override {return m_din_results.DinFreq;}
    public Q_SLOTS:
        void inDigSetFreq(double val);
    protected:
        void protUpdate() override;
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;

        virtual  void dinChSave(int ch_num, QJsonObject &chObj) const {
            Q_UNUSED(ch_num) Q_UNUSED(chObj)
        }
        virtual void dinChLoad(int ch_num, const QJsonObject &chObj) {
            Q_UNUSED(ch_num) Q_UNUSED(chObj)
        }

        virtual void dinAdjustFreq(double &dinFreq) = 0;
    private:
        void dinChFillEn(int ch_num, bool en);
        bool dinChEnIsSet(int ch_num) const {return m_din_params.ChEnMask & (1UL << ch_num);}

        struct {
            double DinFreq;
            quint32 ChEnMask;
        } m_din_params;

        struct {
            double DinFreq;
        } m_din_results;
    };
}

#endif // LQMEAS_DEVINDIGBASECONFIG_H
