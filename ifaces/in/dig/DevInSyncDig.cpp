#include "DevInSyncDig.h"
#include "LQError.h"
#include "lqmeas/StdErrors.h"
#include "lqmeas/devs/DeviceConfig.h"

namespace  LQMeas {
    DevInSyncDig::DevInSyncDig(Device *dev) : m_dev{dev} {

    }

    DevInSyncDig::~DevInSyncDig() {

    }

    const DevInDigConfig &DevInSyncDig::dinConfig() const {
        return *m_dev->config().inDigConfig();
    }

    void DevInSyncDig::inDigEnable(LQError &err) {
        protInDigEnable(err);
    }

    void DevInSyncDig::inDigDisable(LQError &err)  {
        protInDigDisable(err);
    }

    void DevInSyncDig::inDigStart(LQError &err) {
        inDigEnable(err);
        if (err.isSuccess()) {
            protInDigStart(err);
        }

        if (err.isSuccess()) {
            m_run = true;
        }
    }

    void DevInSyncDig::inDigStop(LQError &err) {
        LQError stop_err;
        inDigDisable(err);
        protInDigStop(stop_err);
        err += stop_err;

        if (err.isSuccess()) {
            m_run = false;
        }
    }

    void DevInSyncDig::inDigGetData(quint32 *data, int size, unsigned tout, LQError &err) {
        int recvd_size {0};
        inDigGetData(data, size, tout, recvd_size, err);
        if (err.isSuccess()) {
            if (recvd_size == 0) {
                err = StdErrors::RecvNoData();
            } else if (recvd_size != size) {
                err = StdErrors::RecvInsufficientData();
            }
        }
    }


    void DevInSyncDig::inDigGetData(quint32 *data, int size, unsigned tout, int &recvd_size, LQError &err) {
        protInDigGetData(data, size, tout, recvd_size, err);
    }

}
