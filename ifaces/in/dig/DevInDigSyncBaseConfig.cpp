#include "DevInDigSyncBaseConfig.h"
#include "DevInDigInfo.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>
#include <QJsonArray>


namespace LQMeas {
    static const QLatin1String cfgkey_channels_array    {StdConfigKeys::channels()};
    static const QLatin1String cfgkey_ch_enabled        {StdConfigKeys::syncModeEn()};
    static const QLatin1String cfgkey_in_freq           {StdConfigKeys::freq()};

    DevInDigSyncBaseConfig::DevInDigSyncBaseConfig(const DevInDigInfo &devinfo) :
        DevInDigConfig{devinfo} {

        memset(&m_din_params, 0, sizeof(m_din_params));
        m_din_params.DinFreq = m_din_results.DinFreq = devinfo.inDigSyncFreqDefault();
    }

    DevInDigSyncBaseConfig::DevInDigSyncBaseConfig(const DevInDigSyncBaseConfig &inCfg) :
        DevInDigConfig{inCfg} {

        memcpy(&m_din_params, &inCfg.m_din_params, sizeof(m_din_params));
        memcpy(&m_din_results, &inCfg.m_din_results, sizeof(m_din_results));
    }

    void DevInDigSyncBaseConfig::inDigChSetSyncModeEnabled(int ch_num, bool en) {
        if ((ch_num < inDigInfo().inDigChannelsCnt()) && (inDigChSyncModeEnabled(ch_num) != en)) {
            dinChFillEn(ch_num, en);
            notifyChConfigChanged(ch_num);
            notifyConfigChanged();
        }
    }

    void DevInDigSyncBaseConfig::inDigSetFreq(double val) {
        if (!LQREAL_IS_EQUAL(m_din_params.DinFreq, val)) {
            m_din_params.DinFreq = val;
            notifySyncChFreqChanged(-1);
            notifyConfigChanged();
        }
    }

    void DevInDigSyncBaseConfig::protUpdate()  {
        DevInDigConfig::protUpdate();
        if (m_din_params.DinFreq <= 0) {
            m_din_results.DinFreq = inDigInfo().inDigSyncFreqDefault();
        } else if (m_din_params.DinFreq > inDigInfo().inDigSyncFreqMax()) {
            m_din_results.DinFreq = inDigInfo().inDigSyncFreqMax();
        } else {
            m_din_results.DinFreq = m_din_params.DinFreq;
        }

        const double prevDinFreq {m_din_results.DinFreq};
        dinAdjustFreq(m_din_results.DinFreq);
        if (!LQREAL_IS_EQUAL(prevDinFreq, m_din_results.DinFreq)) {
            notifySyncChFreqChanged();
            notifyConfigChanged();
        }
    }

    void DevInDigSyncBaseConfig::protSave(QJsonObject &cfgObj) const {
        const int dev_ch_cnt {inDigInfo().inDigChannelsCnt()};
        QJsonArray chArray;
        for (int ch_num {0}; ch_num < dev_ch_cnt; ++ch_num) {
            QJsonObject chObj;
            chObj[cfgkey_ch_enabled] = dinChEnIsSet(ch_num);
            dinChSave(ch_num, chObj);
            chArray.append(chObj);
        }
        cfgObj[cfgkey_channels_array] = chArray;
        cfgObj[cfgkey_in_freq] = m_din_params.DinFreq;
    }

    void DevInDigSyncBaseConfig::protLoad(const QJsonObject &cfgObj) {
        const int dev_ch_cnt {inDigInfo().inDigChannelsCnt()};

        m_din_params.ChEnMask = 0;
        const QJsonArray &chArray {cfgObj[cfgkey_channels_array].toArray()};
        int ch_num {0};
        for (const auto &it : chArray) {
            if (ch_num < dev_ch_cnt) {
                const QJsonObject &chObj {it.toObject()};
                dinChFillEn(ch_num, chObj[cfgkey_ch_enabled].toBool());
                dinChLoad(ch_num, chObj);
            }
            ++ch_num;
        }

        const double max_freq {inDigInfo().inDigSyncFreqMax()};
        const double def_freq {inDigInfo().inDigSyncFreqDefault()};
        const double din_freq {cfgObj[cfgkey_in_freq].toDouble(def_freq)};
        if ((din_freq > 0) && (din_freq <= max_freq)) {
            m_din_params.DinFreq = din_freq;
        } else {
            m_din_params.DinFreq = def_freq;
        }
    }


    void DevInDigSyncBaseConfig::dinChFillEn(int ch_num, bool en) {
        if (en) {
            m_din_params.ChEnMask |= (1 << ch_num);
        } else {
            m_din_params.ChEnMask &= ~(1 << ch_num);
        }
    }
}
