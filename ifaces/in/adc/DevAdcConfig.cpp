#include "DevAdcConfig.h"
#include "DevAdcInfo.h"
#include "lqmeas/devs/DeviceTypeInfo.h"
namespace LQMeas {

    DevAdcConfig::DevAdcConfig(const DevAdcInfo &info) : m_adc_info{&info} {

    }

    DevAdcConfig::DevAdcConfig(const DevAdcConfig &cfg) :
        m_adc_info{&cfg.adcInfo()},
        m_ch_indexes{cfg.m_ch_indexes} {
    }

    DevAdcConfig::~DevAdcConfig() {

    }

    double DevAdcConfig::adcChFreq() const {
        double ch_freq  {adcFreq()};
        if (m_adc_info->adcType() == DevAdcInfo::AdcType::Sequential) {
            const int enChCnt {adcEnabledChCnt()};
            if (enChCnt) {
                ch_freq/=enChCnt;
            }
        }
        return ch_freq;
    }

    double DevAdcConfig::adcChTimeShift(int ch_num) const {
        return m_adc_info->adcType() == DevAdcInfo::AdcType::Sequential ?
                    static_cast<double>(adcChIdx(ch_num))/adcFreq() : 0;
    }

    double DevAdcConfig::adcChRangeMaxVal(int ch_num) const {
        return adcInfo().adcChRangeMaxVal(ch_num, adcChRangeNum(ch_num), adcUnitMode(ch_num));
    }

    double DevAdcConfig::adcChRangeMinVal(int ch_num) const {
        return adcInfo().adcChRangeMinVal(ch_num, adcChRangeNum(ch_num), adcUnitMode(ch_num));
    }

    bool DevAdcConfig::adcChAvailable(int ch_num) const {
        return adcInfo().adcChAvailable(ch_num);
    }

    const Unit &DevAdcConfig::adcChRangeUnit(int ch_num) const {
        return adcInfo().adcChRangeUnit(ch_num, adcUnitMode(ch_num));
    }

    const Unit &DevAdcConfig::adcChValUnit(int ch_num) const {
        return adcInfo().adcChValUnit(ch_num, adcUnitMode(ch_num));
    }

    int DevAdcConfig::adcChNum(int idx) const {
        return idx < m_ch_indexes.size() ? m_ch_indexes[idx] : -1;
    }

    int DevAdcConfig::adcChIdx(int ch_num) const {
        return m_ch_indexes.indexOf(ch_num);
    }

    double DevAdcConfig::adcDefaultFreq() const {
        return m_adc_info->adcFreqDefault();
    }


    void DevAdcConfig::notifyChEnableStateChanged(int ch_num) {
        Q_EMIT adcChEnableStateChanged(ch_num, adcChEnabled(ch_num));
        Q_EMIT adcChConfigChanged(ch_num);
    }

    void DevAdcConfig::notifyChRangeChanged(int ch_num) {
        Q_EMIT adcChRangeChanged(ch_num, adcChRangeMinVal(ch_num), adcChRangeMaxVal(ch_num));
        Q_EMIT adcChConfigChanged(ch_num);
    }

    void DevAdcConfig::notifyChModeChanged(int ch_num) {
        Q_EMIT adcChModeChanged(ch_num);
        Q_EMIT adcChConfigChanged(ch_num);
    }

    void DevAdcConfig::notifyChFreqChanged(int ch_num) {
        Q_EMIT adcChConfiguredFreqChanged(ch_num);
        Q_EMIT adcChConfigChanged(ch_num);
    }

    void DevAdcConfig::notifyAdcFreqChanged() {
        Q_EMIT adcConfiguredFreqChanged();
    }

    void DevAdcConfig::notifyChConfigChanged(int ch) {
        Q_EMIT adcChConfigChanged(ch);
    }

    void DevAdcConfig::protUpdate() {
        m_ch_indexes.clear();
        for (int ch_num {0}; ch_num < m_adc_info->adcChannelsCnt(); ++ch_num) {
            if (adcChEnabled(ch_num)) {
                m_ch_indexes.append(ch_num);
            }
        }
    }

    void DevAdcConfig::protUpdateInfo(const DeviceTypeInfo *info)  {
        m_adc_info = info->adc();
        update();
    }

}
