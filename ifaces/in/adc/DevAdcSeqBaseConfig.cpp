#include "DevAdcSeqBaseConfig.h"
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/StdConfigKeys.h"
#include "lqmeas/ifaces/in/adc/DevAdcInfo.h"
#include "lqmeas/lqtdefs.h"
#include <QJsonObject>
#include <QJsonArray>

namespace LQMeas {
    static const QLatin1String cfgkey_channels_array    {StdConfigKeys::channels()};
    static const QLatin1String cfgkey_ch_enabled        {StdConfigKeys::enabled()};
    static const QLatin1String cfgkey_freq_adc          {"FreqAdc"};
    static const QLatin1String cfgkey_freq_ch           {"FreqCh"};
    static const QLatin1String cfgkey_use_max_ch_freq   {"UseMaxFreqCh"};

    DevAdcSeqBaseConfig::DevAdcSeqBaseConfig(const DevAdcInfo &info) :
        DevAdcConfig{info} {

        memset(&m_adc_params, 0, sizeof(m_adc_params));
        m_adc_params.adc_freq = adcDefaultFreq();
        m_adc_params.use_max_ch_freq = true;
        m_adc_results.adc_freq = m_adc_results.ch_freq = m_adc_params.adc_freq;
    }

    DevAdcSeqBaseConfig::DevAdcSeqBaseConfig(const DevAdcSeqBaseConfig &cfg) :
        DevAdcConfig{cfg} {

        memcpy(&m_adc_params, &cfg.m_adc_params, sizeof(m_adc_params));
        memcpy(&m_adc_results, &cfg.m_adc_results, sizeof(m_adc_results));
    }


    void DevAdcSeqBaseConfig::adcSetChEnabled(int ch_num, bool en) {
        if (adcChEnIsSet(ch_num) != en) {
            adcChFillEn(ch_num, en);
            notifyChEnableStateChanged(ch_num);
            notifyConfigChanged();
        }
    }

    void DevAdcSeqBaseConfig::adcSetFreq(double freq) {
        if (m_adc_params.adc_freq != freq) {
            m_adc_params.adc_freq = freq;
            notifyAdcFreqChanged();
            notifyConfigChanged();
        }
    }

    void DevAdcSeqBaseConfig::adcSetChFreq(double ch_freq) {
        if (m_adc_params.ch_freq != ch_freq) {
            m_adc_params.ch_freq = ch_freq;
            notifyChFreqChanged();
            notifyConfigChanged();
        }
    }

    void DevAdcSeqBaseConfig::adcSetUseMaxChFreq(bool en) {
        if (m_adc_params.use_max_ch_freq != en) {
            m_adc_params.use_max_ch_freq = en;
            notifyChFreqChanged();
            notifyConfigChanged();
        }
    }


    void DevAdcSeqBaseConfig::protUpdate() {
        DevAdcConfig::protUpdate();
        double adcFreq, adcChFreq;
        if (m_adc_params.adc_freq <= 0) {
            adcFreq = adcInfo().adcFreqDefault();
        } else if (m_adc_params.adc_freq > adcInfo().adcFreqMax()) {
            adcFreq = adcInfo().adcFreqMax();
        } else {
            adcFreq = m_adc_params.adc_freq;
        }


        if (m_adc_params.use_max_ch_freq || (m_adc_params.ch_freq <= 0) ||
                (m_adc_params.ch_freq > m_adc_results.adc_freq/adcEnabledChCnt())) {
            adcChFreq = adcFreq;
        } else {
            adcChFreq = m_adc_params.ch_freq;
        }

        adcAdjustFreq(adcFreq, adcChFreq);

        const bool adcFreqChanged {!LQREAL_IS_EQUAL(adcFreq, m_adc_results.adc_freq)};
        const bool adcChFreqChanged {!LQREAL_IS_EQUAL(adcChFreq, m_adc_results.ch_freq)};

        if (adcFreqChanged || adcChFreqChanged) {
            m_adc_results.adc_freq = adcFreq;
            m_adc_results.ch_freq = adcChFreq;

            if (adcFreqChanged) {
                notifyAdcFreqChanged();
            }
            if (adcChFreqChanged) {
                notifyChFreqChanged();
            }
            notifyConfigChanged();
        }
    }

    void DevAdcSeqBaseConfig::protSave(QJsonObject &adcObj) const {
        const int dev_ch_cnt {adcInfo().adcChannelsCnt()};
        QJsonArray chArray;
        for (int ch_num {0}; ch_num < dev_ch_cnt; ++ch_num) {
            QJsonObject chObj;
            chObj[cfgkey_ch_enabled] = adcChEnIsSet(ch_num);
            adcChSave(ch_num, chObj);
            chArray.append(chObj);
        }
        adcObj[cfgkey_channels_array] = chArray;
        adcObj[cfgkey_freq_adc] = m_adc_params.adc_freq;
        adcObj[cfgkey_freq_ch] = m_adc_params.ch_freq;
        adcObj[cfgkey_use_max_ch_freq] = m_adc_params.use_max_ch_freq;
    }

    void DevAdcSeqBaseConfig::protLoad(const QJsonObject &adcObj) {
        const QJsonArray &chArray {adcObj[cfgkey_channels_array].toArray()};
        const int dev_ch_cnt {adcInfo().adcChannelsCnt()};


        int ch_num {0};
        m_adc_params.ch_en_mask = 0;
        for (const auto &it : chArray) {
            const QJsonObject &chObj {it.toObject()};
            if (ch_num < dev_ch_cnt) {
                const QJsonObject &chObj {it.toObject()};
                adcChFillEn(ch_num, chObj[cfgkey_ch_enabled].toBool());
                adcChLoad(ch_num, chObj);
            }
            ++ch_num;
        }

        for ( ; ch_num < dev_ch_cnt; ++ch_num) {
            adcChSetDefault(ch_num);
        }

        const double max_freq {adcInfo().adcFreqMax()};
        const double def_freq {adcInfo().adcFreqDefault()};
        const double adc_freq {adcObj[cfgkey_freq_adc].toDouble(def_freq)};
        if ((adc_freq > 0) && (adc_freq <= max_freq)) {
            m_adc_params.adc_freq = adc_freq;
        } else {
            m_adc_params.adc_freq = def_freq;
        }

        const double chFreq {adcObj[cfgkey_freq_ch].toDouble()};
        if ((chFreq >= 0) && (chFreq <= m_adc_params.adc_freq)) {
            m_adc_params.ch_freq = chFreq;
        } else {
            m_adc_params.ch_freq = 0;
        }

        m_adc_params.use_max_ch_freq = adcObj[cfgkey_use_max_ch_freq].toBool(true);
    }

    void DevAdcSeqBaseConfig::adcChFillEn(int ch_num, bool en) {
        if (en) {
            m_adc_params.ch_en_mask |= (1 << ch_num);
        } else {
            m_adc_params.ch_en_mask &= ~(1 << ch_num);
        }
    }
}

