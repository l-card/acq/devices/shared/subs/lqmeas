#ifndef DEVADCICPSENSORROUTUPDATER_H
#define DEVADCICPSENSORROUTUPDATER_H

#include <QObject>

namespace LQMeas {
    class DevAdcICPSensorRoutUpdater : public QObject {
        Q_OBJECT
    public:
        DevAdcICPSensorRoutUpdater(QObject *parent = nullptr);

        virtual double rOut(double isrc_ma) = 0;
    Q_SIGNALS:
        void rOutChanged(double rout);
    public Q_SLOTS:
        void iSrcUpdate(double isrc_ma);
    protected:
        virtual void protISrcUpdate(double isrc_ma) = 0;
    };
}

#endif // DEVADCICPSENSORROUTUPDATER_H
