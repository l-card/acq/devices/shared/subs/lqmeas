#include "DevAdcICPConfig.h"
#include "DevAdcICPSensorRoutUpdater.h"
#include "../DevAdcInfo.h"
#include "../DevAdcConfig.h"
#include "lqmeas/devs/DeviceTypeInfo.h"
#include "lqmeas/devs/DeviceConfig.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonArray>
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_chs_array           {StdConfigKeys::channels()};
    static const QLatin1String cfgkey_ch_rout             {"ROut"};

    DevAdcICPConfig::ICPCh::ICPCh() {

    }

    DevAdcICPConfig::ICPCh::ICPCh(const DevAdcICPConfig::ICPCh &icpCh) :
        updater{icpCh.updater}, rout{icpCh.rout} {

    }

    DevAdcICPConfig::DevAdcICPConfig(const DevAdcInfo &info) : m_adc_info{&info} {
        updateChCount();
    }

    DevAdcICPConfig::DevAdcICPConfig(const DevAdcICPConfig &cfg) : m_adc_info{cfg.m_adc_info} {
        for (ICPCh *icpCh : cfg.m_chs) {
            m_chs.append(new ICPCh{*icpCh});
            enableUpdater(icpCh->updater, true);
        }
    }

    DevAdcICPConfig::~DevAdcICPConfig() {
        qDeleteAll(m_chs);
    }

    double DevAdcICPConfig::adcICPChSensorROut(int ch) const {
        return  m_chs.at(ch)->rout;
    }

    DevAdcICPSensorRoutUpdater *DevAdcICPConfig::adcICPChSensorROutUpdater(int ch) const {
        return  m_chs.at(ch)->updater;
    }

    void DevAdcICPConfig::adcICPChSetSensorROut(int ch, double rout) {
        if (!LQREAL_IS_EQUAL(rout, m_chs.at(ch)->rout)) {
            m_chs.at(ch)->rout = rout;
            Q_EMIT adcICPChSensorROutChanged(ch);
        }
    }

    void DevAdcICPConfig::adcICPChSetSensorROutUpdater(int ch, DevAdcICPSensorRoutUpdater *updater) {
        ICPCh *icpCh = m_chs.at(ch);
        if (icpCh->updater != updater) {
            if (icpCh->updater) {
                enableUpdater(icpCh->updater, false);
            }
            icpCh->updater = updater;
            if (icpCh->updater) {
                enableUpdater(icpCh->updater, true);
                chRoutUpdate(ch);
            }
        }
    }

    void DevAdcICPConfig::protInit(DeviceConfig *cfg) {
        if (cfg->adcConfig()) {
            connect(this, &DevAdcICPConfig::adcICPChISrcValChanged,
                    cfg->adcConfig(), &DevAdcConfig::adcChConfigChanged);
            connect(this, &DevAdcICPConfig::adcICPChSensorROutChanged,
                    cfg->adcConfig(), &DevAdcConfig::adcChConfigChanged);
        }
    }

    void DevAdcICPConfig::protUpdateInfo(const DeviceTypeInfo *info) {
        m_adc_info = info->adc();
        updateChCount();
    }

    void DevAdcICPConfig::protSave(QJsonObject &cfgObj) const {
        DeviceConfigIface::protSave(cfgObj);
        QJsonArray chsArray;
        for (ICPCh *icpCh : m_chs) {
            QJsonObject chObj;
            chObj[cfgkey_ch_rout] = icpCh->rout;
            chsArray.append(chObj);
        }
        cfgObj[cfgkey_chs_array] = chsArray;
    }

    void DevAdcICPConfig::protLoad(const QJsonObject &cfgObj) {
        DeviceConfigIface::protLoad(cfgObj);
        const QJsonArray &chsArray = cfgObj[cfgkey_chs_array].toArray();
        for (int ch_idx = 0; (ch_idx < m_chs.size()); ++ch_idx) {
            ICPCh *icpCh = m_chs.at(ch_idx);
            if (ch_idx < chsArray.size()) {
                QJsonObject chObj = chsArray.at(ch_idx).toObject();
                icpCh->rout = chObj[cfgkey_ch_rout].toDouble();
            } else {
                icpCh->rout = 0;
            }
        }
    }

    void DevAdcICPConfig::notifyISrcValChanged(int ch) {
        if (ch >= 0) {
            DevAdcICPSensorRoutUpdater *updater =  m_chs.at(ch)->updater;
            if (updater) {
                updater->iSrcUpdate(adcICPChISrcValue_mA(ch));
            }
            Q_EMIT adcICPChISrcValChanged(ch);
        } else {
            for (int ch_num = 0; ch_num < m_chs.size(); ++ch_num) {
                notifyISrcValChanged(ch_num);
            }
        }

    }

    void DevAdcICPConfig::notifyICPSupportChanged(int ch) {
        Q_EMIT adcICPChICPSupportChanged(ch);
    }

    void DevAdcICPConfig::onRSensorChanged() {
        DevAdcICPSensorRoutUpdater *updater = qobject_cast<DevAdcICPSensorRoutUpdater*>(sender());
        if (updater) {
            int ch_idx = 0;
            Q_FOREACH(ICPCh *icpCh, m_chs) {
                if (icpCh->updater == updater) {
                    chRoutUpdate(ch_idx);
                    break;
                }
                ++ch_idx;
            }
        }
    }

    void DevAdcICPConfig::enableUpdater(DevAdcICPSensorRoutUpdater *up, bool en) {
        if (up) {
            if (en) {
                connect(up, &DevAdcICPSensorRoutUpdater::rOutChanged, this, &DevAdcICPConfig::onRSensorChanged);
            } else {
                disconnect(up, &DevAdcICPSensorRoutUpdater::rOutChanged, this, &DevAdcICPConfig::onRSensorChanged);
            }
        }
    }

    void DevAdcICPConfig::chRoutUpdate(int ch_num)  {
        ICPCh *icpCh = m_chs.at(ch_num);
        if (icpCh->updater) {
            adcICPChSetSensorROut(ch_num, icpCh->updater->rOut(adcICPChISrcValue_mA(ch_num)));
        }
    }

    void DevAdcICPConfig::updateChCount() {
        int ch_cnt = m_adc_info->adcChannelsCnt();
        while (ch_cnt > m_chs.size()) {
            m_chs.append(new ICPCh());
        }
        while (ch_cnt < m_chs.size()) {
            ICPCh *icpCh = m_chs.takeLast();
            enableUpdater(icpCh->updater, false);
            delete  icpCh;
        }
    }

}
