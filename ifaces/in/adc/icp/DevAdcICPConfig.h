#ifndef LQMEAS_DEVADCICPCONFIG_H
#define LQMEAS_DEVADCICPCONFIG_H

#include "lqmeas/devs/DeviceConfigIface.h"
#include <QList>
#include <QPointer>
namespace LQMeas {
    class DevAdcICPSensorRoutUpdater;
    class DevAdcInfo;

    class DevAdcICPConfig : public DeviceConfigIface {
        Q_OBJECT
    public:
        DevAdcICPConfig(const DevAdcInfo &info);
        DevAdcICPConfig(const DevAdcICPConfig &cfg);

        const DevAdcInfo *adcInfo() const {return m_adc_info;}

        ~DevAdcICPConfig() override;

        /* поддерживает ли канал АЦП в текущем режиме чтение с ICP-датчиков */
        virtual bool adcChSupportICP(int ch) const  = 0;
        /* поддерживает ли канал АЦП в текущем режиме чтение данных TEDS */
        virtual bool adcChSupportTEDS(int ch) const = 0;
        /* значение тока питания датчика заданного канала ICP в мА */
        virtual double adcICPChISrcValue_mA(int ch) const = 0;


        double adcICPChSensorROut(int ch) const;
        DevAdcICPSensorRoutUpdater *adcICPChSensorROutUpdater(int ch) const;
    Q_SIGNALS:
        void adcICPChISrcValChanged(int ch);
        void adcICPChSensorROutChanged(int ch);
        void adcICPChICPSupportChanged(int ch);
    public Q_SLOTS:
        void adcICPChSetSensorROut(int ch, double rout);
        void adcICPChSetSensorROutUpdater(int ch, DevAdcICPSensorRoutUpdater *updater);
    protected:
        QString cfgIfaceName() const override {return  QStringLiteral("ICP");}
        void protInit(DeviceConfig *cfg) override;
        void protUpdateInfo(const DeviceTypeInfo *info) override;
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;


        void notifyISrcValChanged(int ch);
        void notifyICPSupportChanged(int ch);
    private Q_SLOTS:
        void onRSensorChanged();
    private:
        void enableUpdater(DevAdcICPSensorRoutUpdater *up, bool en);
        void chRoutUpdate(int ch_num);

        void updateChCount();
        struct ICPCh {
            ICPCh();
            ICPCh(const ICPCh &icpCh);

            QPointer<DevAdcICPSensorRoutUpdater> updater;
            double rout {0};
        };

        const DevAdcInfo *m_adc_info;
        QList<ICPCh *> m_chs;
    };
}

#endif // LQMEAS_DEVADCICPCONFIG_H
