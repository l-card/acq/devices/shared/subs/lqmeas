#ifndef DEVADCSEQBASECONFIG_H
#define DEVADCSEQBASECONFIG_H


#include "DevAdcConfig.h"
#include <QVector>

namespace LQMeas {
    /* Вспомогательный класс, реализующих установку стандартных параметров
     * для АЦП с коммутацией каналов на 32 канала с общей землей/16 дифференциальных
     * каналов с настраиваемым диапазоном для каждого канала */
    class DevAdcSeqBaseConfig : public DevAdcConfig {
        Q_OBJECT
    public:
        explicit DevAdcSeqBaseConfig(const DevAdcInfo &info);
        explicit DevAdcSeqBaseConfig(const DevAdcSeqBaseConfig &cfg);

        /* результирующие параметры, действительные только после adcUpdateConfig() */
        double adcFreq() const override {return m_adc_results.adc_freq;}
        double adcChFreq() const override {return m_adc_results.ch_freq;}
        /* изначально установленные параметры частоты */
        double adcConfiguredFreq() const {return m_adc_params.adc_freq;}
        double adcConfiguredChFreq() const {return m_adc_params.ch_freq;}
        bool adcUseMaxChFreq() const {return m_adc_params.use_max_ch_freq;}

        bool adcChEnabled(int ch_num) const override { return adcChEnIsSet(ch_num); }


    public Q_SLOTS:
        void adcSetChEnabled(int ch_num, bool en);
        void adcSetFreq(double freq);
        void adcSetChFreq(double ch_freq);
        void adcSetUseMaxChFreq(bool en);
    protected:
        void protUpdate() override;

        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;

        virtual void adcAdjustFreq(double &adcFreq, double &adcChFreq) = 0;
        virtual void adcChSave(int ch_num, QJsonObject &chObj) const = 0;
        virtual void adcChLoad(int ch_num, const QJsonObject &chObj) = 0;
        virtual void adcChSetDefault(int ch_num) = 0;
    private:
        void adcChFillEn(int ch_num, bool en);
        bool adcChEnIsSet(int ch_num) const {return m_adc_params.ch_en_mask & (1UL << ch_num);}

        struct {
            double adc_freq;
            double ch_freq;
            bool   use_max_ch_freq;
            quint32 ch_en_mask;
        } m_adc_params;

        struct {
            double adc_freq;
            double ch_freq;
        } m_adc_results;
    };
}



#endif // DEVADCSEQBASECONFIG_H
