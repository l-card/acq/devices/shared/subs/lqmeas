#ifndef LQMEAS_DEVADC_H
#define LQMEAS_DEVADC_H

#include <QVector>
#include "lqmeas/devs/Device.h"
#include "lqmeas/ifaces/in/frame/DevInFrameStatus.h"
#include "DevAdcInfo.h"
class LQError;

namespace LQMeas {
    class DevAdcConfig;

    class DevAdc {
    public:
        virtual ~DevAdc();

        enum class ChannelStatus {
            Ok,
            NoData,
            Disabled,
            Open,
            Short,
            Overload,
            Error,
            Unknown
        };

        const DevAdcConfig &adcConfig() const;

        virtual bool adcIsRunning() const {return m_run;}

        void adcEnable(LQError &err);
        void adcDisable(LQError &err);

        void adcPreStart(LQError &err);
        void adcStart(LQError &err);
        void adcStop(LQError &err);


        void adcGetData(double *data, int size, unsigned flags,
                        unsigned tout, int &recvd_size, LQError &err);
        void adcGetData(double *data, int size, unsigned flags,
                       unsigned tout, LQError &err);

        virtual ChannelStatus adcGetChStatus(int ch, bool clear = false);
        virtual DevInFrameStatus adcFrameStatus() const {
            return DevInFrameStatus{};
        }


        int adcChNum(int idx) const;
        int adcChIdx(int ch) const;
    protected:
        explicit DevAdc(Device *dev, int max_channels);


        virtual void protAdcPreStart(LQError &err) {Q_UNUSED(err)}
        virtual void protAdcEnable (LQError &err) {Q_UNUSED(err)}
        virtual void protAdcDisable (LQError &err) {Q_UNUSED(err)}
        virtual void protAdcStart(LQError &err) = 0;
        virtual void protAdcStop(LQError &err) = 0;
        virtual void protAdcGetData(double *data, int size, unsigned flags,
                                     unsigned tout, int &recvd_size, LQError &err) = 0;


        void adcSetChStatus(int ch, ChannelStatus status);
    private:
        void adcUpdateChanngelCfg();

        QVector<ChannelStatus> m_ch_status;
        bool m_run {false};
        Device *m_dev;

        friend class Device;
    };
}
#endif // LQMEAS_DEVADC_H
