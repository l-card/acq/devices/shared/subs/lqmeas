#ifndef LQMEAS_DEVADCCONFIG_H
#define LQMEAS_DEVADCCONFIG_H

#include <QVector>
#include "lqmeas/devs/DeviceConfigIface.h"

namespace LQMeas {
    class DeviceTypeInfo;
    class Device;
    class Unit;
    class DevAdcInfo;

    class DevAdcConfig : public DeviceConfigIface {
        Q_OBJECT
    public:
        explicit DevAdcConfig(const DevAdcInfo &info);
        DevAdcConfig(const DevAdcConfig &cfg);
        virtual ~DevAdcConfig() override;

        const DevAdcInfo &adcInfo() const {return *m_adc_info;}

        virtual double adcFreq() const = 0;
        virtual double adcChFreq() const;
        /* начальное смещение по времени заданного канала АЦП относительно первого канала */
        virtual double adcChTimeShift(int ch_num) const;

        virtual bool adcChEnabled(int ch_num) const = 0;
        virtual bool adcExternalStart() const {return false;}

        virtual int  adcEnabledChCnt() const {return m_ch_indexes.size();}

        virtual int adcChRangeNum(int ch_num) const = 0;
        virtual double adcChRangeMaxVal(int ch_num) const;
        virtual double adcChRangeMinVal(int ch_num) const;

        /* доступен ли канал для разрешения для этой конфигурации и этого типа устройства */
        virtual bool adcChAvailable(int ch_num) const;

        virtual int adcUnitMode(int ch_num) const { Q_UNUSED(ch_num) return 0;}
        virtual const Unit &adcChRangeUnit(int ch_num) const;
        virtual const Unit &adcChValUnit(int ch_num) const;
        /* скорость передачи отсчетов на канал. как правило равна скорости сбора, но
         * для устройств с внутренней памятью, где сбор быстрее выдачи, может отличаться */
        virtual double adcChTransfRate() const {return adcChFreq();}

        /* возможно ли измерение переменной составляющей сигнала по данному каналу */
        virtual bool adcChSupportMeasAC(int ch_num) const {
            Q_UNUSED(ch_num)
            return  true;
        }
        /* возможно ли измерение постоянной составляющей сигнала по данному каналу */
        virtual bool adcChSupportMeasDC(int ch_num) const {
            Q_UNUSED(ch_num)
            return  true;
        }

        /* признак, что АЦП настроено на сбор данных по кадрам, а не в непрерывном режиме */
        virtual bool adcFrameMode() const {return false;}
        /* Размер кадра в точках на канал, действительно только при adcFrameMode() */
        virtual int adcFrameSize() const {return 0;}
        /* если АЦП настроено на периодечский прием кадров, то возвращает частоту кадра, иначе - 0 */
        virtual double adcFrameFreq() const {return 0;}
        /* поддерживается ли настройка возможности программно запускать выдачу кадра */
        virtual bool adcFrameReqSupported() const {return false;}
        /* разрешен ли в текущих настройках программный запрос выдачи кадра */
        virtual bool adcFrameReqEnabled() const {return false;}


        /* получить номер физ. канала по индексу при приеме */
        virtual int adcChNum(int idx) const;
        virtual int adcChIdx(int ch_num) const;

        virtual int adcSampleSize() const = 0;

        double adcDefaultFreq() const;
    Q_SIGNALS:
        void adcChRangeChanged(int ch_num, double minVal, double maxVal);
        void adcChEnableStateChanged(int ch_num, bool en);
        void adcChModeChanged(int ch_num);
        void adcChConfiguredFreqChanged(int ch_num);
        void adcChConfigChanged(int ch_num);
        void adcConfiguredFreqChanged();
    protected:
        QString cfgIfaceName() const override {return  QStringLiteral("Adc");}

        void protUpdate() override;
        void protUpdateInfo(const DeviceTypeInfo *info) override;

        void notifyChEnableStateChanged(int ch_num);
        void notifyChRangeChanged(int ch_num);
        void notifyChModeChanged(int ch_num);
        void notifyChFreqChanged(int ch_num = -1);
        void notifyAdcFreqChanged();
        void notifyChConfigChanged(int ch_num);
    private:
        const DevAdcInfo *m_adc_info;
        QVector<int> m_ch_indexes;
        friend class DeviceConfig;
    };
}

#endif // LQMEAS_DEVADCCONFIG_H
