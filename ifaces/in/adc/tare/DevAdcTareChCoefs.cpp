#include "DevAdcTareChCoefs.h"

namespace LQMeas {
    DevAdcTareChCoefs::DevAdcTareChCoefs()  {

    }

    void DevAdcTareChCoefs::clear() {
        m_vals.clear();
    }

    void DevAdcTareChCoefs::setCoefValue(const DevAdcTareCoefType &type,
                                         const DevAdcTareChCoefs::Value &value) {
        m_vals[&type] = value;
    }

    const DevAdcTareChCoefs::Value DevAdcTareChCoefs::coefValue(
            const DevAdcTareCoefType &type) const {
        return m_vals[&type];
    }
}
