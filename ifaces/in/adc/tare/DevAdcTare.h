#ifndef LQMEAS_DEVADCTARE_H
#define LQMEAS_DEVADCTARE_H

#include <QtGlobal>
class LQError;

namespace LQMeas {
    class Device;
    class DevAdcTareOp;
    class DevAdcTarePoint;
    class DevAdcTareCoefType;
    class DevAdcTareChCoefs;

    class DevAdcTare {
    public:
        virtual ~DevAdcTare();
        virtual void adcTareExec(const DevAdcTareOp &op,
                                 const DevAdcTarePoint &point,
                                 quint32 ch_mask,
                                 quint32 &ch_ok_mask,
                                 LQError &err) = 0;
        virtual void adcTareGetChCoefs(int ch, DevAdcTareChCoefs &coefs, LQError &err) = 0;

        /* параметры, доступные при текущей конфигурации устройства.
         * по умолчанию аналогичны параметрам из информации о устройстве,
         * однако могут быть переопределены, если какие-то варианты не доступны для текущей конфигурации */
        virtual QList<const DevAdcTarePoint *> adcTareAvailablePoints() const;
        virtual QList<const DevAdcTareOp *> adcTareAvailableOps(const DevAdcTarePoint &point) const;
        virtual QList<const DevAdcTareCoefType *> adcTareAvailableCoefTypes() const;
    protected:
        DevAdcTare(Device *dev);
    private:
        Device *m_dev;
    };
}

#endif // LQMEAS_DEVADCTARE_H
