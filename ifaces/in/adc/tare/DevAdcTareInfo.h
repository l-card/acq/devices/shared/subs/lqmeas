#ifndef LQMEAS_DEVADCTAREINFO_H
#define LQMEAS_DEVADCTAREINFO_H

#include <QList>

namespace LQMeas {
    class DevAdcTareOp;
    class DevAdcTarePoint;
    class DevAdcTareCoefType;

    class DevAdcTareInfo {
    public:
        virtual ~DevAdcTareInfo();

        virtual QList<const DevAdcTarePoint *> adcTarePoints() const = 0;
        virtual QList<const DevAdcTareOp *> adcTareOps(const DevAdcTarePoint &point) const = 0;
        virtual QList<const DevAdcTareCoefType *> adcTareCoefTypes() const = 0;
    };
}

#endif // DEVADCTAREINFO_H
