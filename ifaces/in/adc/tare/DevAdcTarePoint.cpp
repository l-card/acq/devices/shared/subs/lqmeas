#include "DevAdcTarePoint.h"

namespace LQMeas {
    const DevAdcTarePoint &DevAdcTarePoints::zero() {
        static const class DevAdcTareParamOffset : public DevAdcTarePoint {
            QString id() const override {return QStringLiteral("zero");}
            QString displayName() const override {return tr("Zero");}
        } par;
        return par;
    }

    const DevAdcTarePoint &DevAdcTarePoints::scale() {
        static const class DevAdcTareParamScale : public DevAdcTarePoint {
            QString id() const override {return QStringLiteral("scale");}
            QString displayName() const override {return tr("Scale");}
        } par;
        return par;
    }
}
