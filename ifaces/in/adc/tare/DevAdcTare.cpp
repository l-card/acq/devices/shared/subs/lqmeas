#include "DevAdcTare.h"
#include "DevAdcTareInfo.h"
#include "lqmeas/devs/Device.h"
#include "lqmeas/devs/DeviceInfo.h"

namespace LQMeas {
    DevAdcTare::DevAdcTare(Device *dev) : m_dev{dev} {

    }

    DevAdcTare::~DevAdcTare() {
    }

    QList<const DevAdcTarePoint *> DevAdcTare::adcTareAvailablePoints() const {
        return m_dev->devInfo()->type().adcTare()->adcTarePoints();
    }

    QList<const DevAdcTareOp *> DevAdcTare::adcTareAvailableOps(const DevAdcTarePoint &point) const {
        return m_dev->devInfo()->type().adcTare()->adcTareOps(point);
    }

    QList<const DevAdcTareCoefType *> DevAdcTare::adcTareAvailableCoefTypes() const  {
        return m_dev->devInfo()->type().adcTare()->adcTareCoefTypes();
    }
}
