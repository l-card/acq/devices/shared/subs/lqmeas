#ifndef LQMEAS_DEVADCTAREOP_H
#define LQMEAS_DEVADCTAREOP_H


#include "lqmeas/EnumNamedValue.h"
#include <QMetaType>

namespace LQMeas {
    class DevAdcTareOp : public EnumNamedValue<QString> {
    public:
        virtual bool externalSignalRequired() const = 0;

        friend bool operator==(const DevAdcTareOp& op1, const DevAdcTareOp& op2) {
            return op1.id() == op2.id();
        }
    };
}

Q_DECLARE_METATYPE(const LQMeas::DevAdcTareOp *);


#endif // LQMEAS_DEVADCTAREOP_H
