#ifndef LQMEAS_DEVADCTAREPOINT_H
#define LQMEAS_DEVADCTAREPOINT_H

#include <QObject>
#include "lqmeas/EnumNamedValue.h"

namespace LQMeas {
    class DevAdcTarePoint  : public EnumNamedValue<QString> {
    public:
        bool operator==(const DevAdcTarePoint& type) const {
            return id() == type.id();
        }
    };

     class DevAdcTarePoints : public QObject {
         Q_OBJECT
     public:
         static const DevAdcTarePoint &zero() ;
         static const DevAdcTarePoint &scale() ;
     };
}

Q_DECLARE_METATYPE(const LQMeas::DevAdcTarePoint *);

#endif // DEVADCTAREPARAM_H
