#ifndef LQMEAS_DEVADCTARECOEFTYPE_H
#define LQMEAS_DEVADCTARECOEFTYPE_H


#include <QObject>
#include "lqmeas/EnumNamedValue.h"

namespace LQMeas {
    class Unit;

    class DevAdcTareCoefType  : public EnumNamedValue<QString> {
    public:
        virtual const Unit &unit() const = 0;
        virtual bool hasFractPart() const {return true;}
        virtual bool hasValidFlag() const = 0;

        bool operator==(const DevAdcTareCoefType& type) const {
            return id() == type.id();
        }
    };
}

Q_DECLARE_METATYPE(const LQMeas::DevAdcTareCoefType *);

#endif // DEVADCTARECOEFTYPE_H
