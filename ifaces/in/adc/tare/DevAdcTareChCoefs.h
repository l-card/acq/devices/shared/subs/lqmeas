#ifndef LQMEAS_DEVADCTARECHCOEFS_H
#define LQMEAS_DEVADCTARECHCOEFS_H

#include <QHash>
#include <QList>

namespace LQMeas {
    class DevAdcTareCoefType;

    class DevAdcTareChCoefs {
    public:
        struct Value {
            Value(double val, bool valid = true) :
                m_value{val}, m_valid{valid} {
            }
            Value() {}

            bool isValid() const {return m_valid;}
            double value() const {return m_value;}
        private:
            double m_value {0};
            bool m_valid {false};
        };

        DevAdcTareChCoefs();

        void clear();
        void setCoefValue(const DevAdcTareCoefType &type, const Value &value);
        void setCoefValue(const DevAdcTareCoefType &type, double value, bool valid = true) {
            setCoefValue(type, Value(value, valid));
        }
        const Value coefValue(const DevAdcTareCoefType &type) const;
    private:
        QHash<const DevAdcTareCoefType *, Value> m_vals;
    };
}

#endif // DEVADCTARECHCOEFS_H
