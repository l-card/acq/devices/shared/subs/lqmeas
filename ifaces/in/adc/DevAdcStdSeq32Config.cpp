#include "DevAdcStdSeq32Config.h"
#include "lqmeas/EnumConfigKey.h"
#include <QJsonObject>
#include <QJsonArray>
#include "lqmeas/StdConfigKeys.h"
#include "lqmeas/ifaces/in/adc/DevAdcInfo.h"

namespace LQMeas {
    static const QLatin1String cfgkey_ch_range_num        {StdConfigKeys::rangeNum()};
    static const QLatin1String cfgkey_ch_mode             {StdConfigKeys::mode()};

    typedef EnumConfigKey<DevAdcStdSeq32Config::AdcChMode> AdcChModeKey;
    static const AdcChModeKey adc_ch_modes[] {
        {QStringLiteral("comm"),    DevAdcStdSeq32Config::AdcChMode::Comm},
        {QStringLiteral("diff"),    DevAdcStdSeq32Config::AdcChMode::Diff},
        {QStringLiteral("zero"),    DevAdcStdSeq32Config::AdcChMode::Zero},
        {AdcChModeKey::defaultKey(),DevAdcStdSeq32Config::AdcChMode::Comm},
    };


    DevAdcStdSeq32Config::DevAdcStdSeq32Config(const DevAdcInfo &info) : DevAdcSeqBaseConfig{info} {
        memset(m_ch_params, 0, sizeof(m_ch_params));
    }

    DevAdcStdSeq32Config::DevAdcStdSeq32Config(const DevAdcStdSeq32Config &cfg) : DevAdcSeqBaseConfig{cfg} {
        memcpy(m_ch_params, cfg.m_ch_params, sizeof(m_ch_params));
    }

    void DevAdcStdSeq32Config::adcSetChMode(int ch_num, AdcChMode mode) {
        if (m_ch_params[ch_num].mode != mode) {
            m_ch_params[ch_num].mode = mode;
            notifyConfigChanged();
            Q_EMIT adcChModeChanged(ch_num);
        }
    }

    void DevAdcStdSeq32Config::adcSetChRangeNum(int ch_num, int range_num) {
        if (m_ch_params[ch_num].range != range_num) {
            m_ch_params[ch_num].range = range_num;
            notifyConfigChanged();
            notifyChRangeChanged(ch_num);
        }
    }

    void DevAdcStdSeq32Config::adcChSave(int ch_num, QJsonObject &chObj) const     {
        chObj[cfgkey_ch_range_num] = m_ch_params[ch_num].range;
        chObj[cfgkey_ch_mode] = AdcChModeKey::getKey(adc_ch_modes, m_ch_params[ch_num].mode);
    }

    void DevAdcStdSeq32Config::adcChLoad(int ch_num, const QJsonObject &chObj)     {
        const int range {chObj[cfgkey_ch_range_num].toInt()};
        if ((range >= 0) || (range < adcInfo().adcChRangesCnt(ch_num, adcUnitMode(ch_num)))) {
            m_ch_params[ch_num].range = range;
        } else {
            m_ch_params[ch_num].range = 0;
        }
        m_ch_params[ch_num].mode = AdcChModeKey::getValue(
                    adc_ch_modes, chObj[cfgkey_ch_mode].toString());
    }

    void DevAdcStdSeq32Config::adcChSetDefault(int ch_num) {
        m_ch_params[ch_num].mode = AdcChMode::Comm;
        m_ch_params[ch_num].range = 0;
    }

    bool DevAdcStdSeq32Config::adcHasZeroChannels() const {
        bool ret {false};
        for (int ch_num {0}; (ch_num < adc_channels_cnt) && !ret; ++ch_num) {
            if (adcChEnabled(ch_num) && (adcChMode(ch_num) == AdcChMode::Zero)) {
                ret = true;
            }
        }
        return ret;
    }
}
