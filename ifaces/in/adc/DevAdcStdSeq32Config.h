#ifndef DEVADCSTDSEQ32CONFIG_H
#define DEVADCSTDSEQ32CONFIG_H

#include "DevAdcSeqBaseConfig.h"

namespace LQMeas {
    /* Вспомогательный класс, реализующих установку стандартных параметров
     * для АЦП с коммутацией каналов на 32 канала с общей землей/16 дифференциальных
     * каналов с настраиваемым диапазоном для каждого канала */
    class DevAdcStdSeq32Config : public DevAdcSeqBaseConfig {
        Q_OBJECT
    public:
        static const int adc_channels_cnt {32};
        static const int adc_diff_channels_cnt {16};

        enum class AdcChMode {
            Comm  = 0,
            Diff = 1,
            Zero = 2
        };


        explicit DevAdcStdSeq32Config(const DevAdcInfo &info);
        DevAdcStdSeq32Config(const DevAdcStdSeq32Config &cfg);

        int adcChRangeNum(int ch_num) const {return m_ch_params[ch_num].range;}
        AdcChMode adcChMode(int ch_num) const {return m_ch_params[ch_num].mode;}

        bool adcHasZeroChannels() const;
    public Q_SLOTS:
        void adcSetChMode(int ch_num, DevAdcStdSeq32Config::AdcChMode mode);
        void adcSetChRangeNum(int ch_num, int range_num);
    protected:
        void adcChSave(int ch_num, QJsonObject &chObj) const;
        void adcChLoad(int ch_num, const QJsonObject &chObj);
        void adcChSetDefault(int ch_num);
    private:
        struct {
            AdcChMode mode;
            int range;
        } m_ch_params[adc_channels_cnt];
    };
}


#endif // DEVADCSTDSEQ32CONFIG_H
