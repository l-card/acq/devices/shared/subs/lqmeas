#include "DevAdcInfo.h"
#include "lqmeas/units/std/Voltage.h"

namespace LQMeas {

    const Unit &LQMeas::DevAdcInfo::adcChRangeUnit(int ch_num, int unit_mode_num) const {
        Q_UNUSED(ch_num)
        Q_UNUSED(unit_mode_num)
        return Units::Voltage::V();
    }

    DevAdcInfo::~DevAdcInfo() {

    }
}

