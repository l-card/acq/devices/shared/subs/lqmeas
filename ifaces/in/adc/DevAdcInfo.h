#ifndef LQMEAS_DEVADCINFO_H
#define LQMEAS_DEVADCINFO_H

#include <QtGlobal>

namespace LQMeas {
    class Unit;

    class DevAdcInfo {
    public:
        enum class AdcType {
            Parallel,   /** Параллельные каналы */
            Sequential  /** Последовательный опрос каналов (с коммутатором) */
        };

        virtual AdcType adcType() const = 0;
        virtual int adcChannelsCnt() const = 0;
        virtual double adcFreqMax() const = 0;
        virtual double adcFreqDefault() const {return adcFreqMax();}

        virtual bool adcIsChFreqConfigurable() const = 0;


        virtual int adcChRangesCnt(int ch_num, int unit_mode_num = 0) const = 0;
        virtual double adcChRangeMaxVal(int ch_num, int range, int unit_mode_num = 0) const = 0;
        virtual double adcChRangeMinVal(int ch_num, int range, int unit_mode_num = 0) const = 0;

        /* признак, присутствует ли физически канал в устройстве (в случае,
         * если каналы могут опционально устанавливаться и идти не подряд) */
        virtual bool adcChAvailable(int ch_num) const { Q_UNUSED(ch_num) return true;}
        /* Единицы измерения величин при задании диапазона */
        virtual const Unit &adcChRangeUnit(int ch_num, int unit_mode_num = 0) const;
        /* Единицы измерения величин полученных значений с АЦП */
        virtual const Unit &adcChValUnit(int ch_num, int unit_mode_num = 0) const {
            return adcChRangeUnit(ch_num, unit_mode_num);
        }
        /* Количество режимов АЦП, влияющих на измеряемые величины и возможные диапазоны */
        virtual int adcUnitModesCnt(int ch_num = 0) const {
            Q_UNUSED(ch_num)
            return 1;
        }

        /* Поддерживает ли устройство передачу данных по кадрам */
        virtual bool adcIsFrameModeSupported() const {return false;}
        /* Поддерживает ли устройство программный запрос кадров */
        virtual bool adcIsFrameReqSupported() const {return false;}

        virtual ~DevAdcInfo();
    };
}

#endif // LQMEAS_DEVADCINFO_H
