#include "DevAdc.h"
#include "DevAdcConfig.h"
#include "lqmeas/StdErrors.h"
#include "lqmeas/devs/Device.h"
#include "lqmeas/devs/DeviceConfig.h"
#include "lqmeas/log/Log.h"

namespace LQMeas {
    DevAdc::DevAdc(Device *dev, int max_channels) : m_dev{dev} {
        m_ch_status.resize(max_channels);
    }

    void DevAdc::adcStart(LQError &err) {
        for (int ch {0}; ch < m_ch_status.size(); ++ch) {
            m_ch_status[ch] = adcConfig().adcChEnabled(ch) ?
                        DevAdc::ChannelStatus::NoData : DevAdc::ChannelStatus::Disabled;
        }
        adcEnable(err);
        if (err.isSuccess()) {
            protAdcStart(err);
        }

        if (err.isSuccess()) {
            m_run = true;
            LQMeasLog->detail(Device::tr("Data acquisition started successfully"), m_dev);
        } else {
            LQMeasLog->error(Device::tr("Data acquisition start error"), err, m_dev);
        }
    }

    void DevAdc::adcStop(LQError &err) {
        LQError stop_err;
        adcDisable(err);
        protAdcStop(stop_err);
        err += stop_err;

        if (err.isSuccess()) {
            m_run = false;
            LQMeasLog->detail(Device::tr("Data acquisition stoped successfully"), m_dev);
        } else {
            LQMeasLog->error(Device::tr("Data acquisition stop error"), err, m_dev);
        }
    }

    void DevAdc::adcGetData(double *data, int size, unsigned flags, unsigned tout,
                            int &recvd_size, LQError &err) {
        return protAdcGetData(data, size, flags, tout, recvd_size, err);
    }

    void DevAdc::adcGetData(double *data, int size, unsigned flags,
                            unsigned tout, LQError &err) {
        int recvd_size = 0;
        adcGetData(data, size, flags, tout, recvd_size, err);
        if (err.isSuccess()) {
            if (recvd_size == 0) {
                err = StdErrors::RecvNoData();
            } else if (recvd_size != size) {
                err = StdErrors::RecvInsufficientData();
            }
        }
    }

    DevAdc::ChannelStatus DevAdc::adcGetChStatus(int ch, bool clear) {
        DevAdc::ChannelStatus ret;
        if (ch >= m_ch_status.size()) {
            ret = DevAdc::ChannelStatus::Error;
        } else {
            ret = m_ch_status[ch];
            if (clear && (ret!=DevAdc::ChannelStatus::Disabled)) {
                m_ch_status[ch] = DevAdc::ChannelStatus::NoData;
            }
        }
        return ret;
    }

    int DevAdc::adcChNum(int idx) const {
        return adcConfig().adcChNum(idx);
    }

    int DevAdc::adcChIdx(int ch) const {
        return adcConfig().adcChIdx(ch);
    }

    void DevAdc::adcSetChStatus(int ch, DevAdc::ChannelStatus status) {
        if (ch < m_ch_status.size()) {
            /* состояние ошибочной ситуации по каналу должно сохраняться до сброса статуса */
            if ((m_ch_status[ch]==ChannelStatus::NoData) || (status != ChannelStatus::Ok))
                m_ch_status[ch] = status;
        }
    }


    DevAdc::~DevAdc() {

    }

    const DevAdcConfig &DevAdc::adcConfig() const {
        return *m_dev->config().adcConfig();
    }

    void DevAdc::adcEnable(LQError &err) {
        protAdcEnable(err);
    }

    void DevAdc::adcDisable(LQError &err) {
        protAdcDisable(err);
    }

    void DevAdc::adcPreStart(LQError &err) {
        protAdcPreStart(err);
    }
}
