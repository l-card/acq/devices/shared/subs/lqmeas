#ifndef LQMEAS_DEVSYNCMARKSGENINFO_H
#define LQMEAS_DEVSYNCMARKSGENINFO_H

#include <QList>

namespace LQMeas {
    class DevSyncMarkType;
    class DevSyncMarksGenInfo {
    public:
        virtual ~DevSyncMarksGenInfo();
        virtual QList<const DevSyncMarkType *> syncMarkGenTypes() const = 0;
    };
}

#endif // DEVSYNCMARKSGENINFO_H
