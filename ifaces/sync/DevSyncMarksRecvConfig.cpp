#include "DevSyncMarksRecvConfig.h"

namespace LQMeas {
    void DevSyncMarksRecvConfig::syncMarksRecvFreqChangedNotify() {
        Q_EMIT syncMarksRecvFreqChanged();
    }
}
