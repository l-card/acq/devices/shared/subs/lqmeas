#ifndef LQMEAS_DEVSYNCMARKSRECV_H
#define LQMEAS_DEVSYNCMARKSRECV_H

#include "DevSyncMark.h"
class LQError;

namespace LQMeas {
    class DevSyncMarksRecv {
    public:
        virtual ~DevSyncMarksRecv();

        /* Ожидание когда устройство готово принимать метки. Как правило означает,
         * что принят хотя бы один кадр данных, что гарантирует, что метки не придут
         * до того, как старт реально завершиться */
        virtual bool syncMarkRecvWaitReady(unsigned tout, LQError &err) = 0;
        /* прием rcv_size числа слов в ожидании стартовой метки.
         * Отбрасывает все данные до кадра с найденной метокой.
         * Возвращает, найдена ли первая метка.
         * Если метка найдена, то в mark_offset возвращает смещение найденной
         * метки от начала кадра */
        virtual bool syncMarkRecvWaitStartMark(int rcv_size, unsigned tout, LQError &err) = 0;
        /* получение принятых меток, соответствующих отсчетам, принятым в последнем
         * кадре синхронного ввода */
        virtual const QVector<DevSyncMarkWord> syncMarkRecvGet(const DevSyncMarkType &type) = 0;

        virtual int syncMarkRecvFirstStartMarkCh() = 0;
    };
}

#endif // DEVSYNCMARKSRECV_H
