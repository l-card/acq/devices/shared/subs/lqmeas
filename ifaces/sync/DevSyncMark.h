#ifndef LQMEAS_DEVSYNCMARK_H
#define LQMEAS_DEVSYNCMARK_H

#include "lqmeas/EnumNamedValue.h"
#include <QObject>

namespace LQMeas {
    typedef quint16 DevSyncMarkWord;

    class DevSyncMarkType : public EnumNamedValue<QString> {
    public:
        bool operator==(const DevSyncMarkType& type) const {
            return id() == type.id();
        }
    };
}

Q_DECLARE_METATYPE(const LQMeas::DevSyncMarkType *);

#endif // DEVSYNCMARK_H
