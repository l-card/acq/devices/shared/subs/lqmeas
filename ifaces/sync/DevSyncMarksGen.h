#ifndef LQMEAS_DEVSYNCMARKSGEN_H
#define LQMEAS_DEVSYNCMARKSGEN_H

class LQError;

namespace LQMeas {


    class DevSyncMarksGen {
    public:
        virtual ~DevSyncMarksGen();
        virtual void syncMarksGenStartMaster(LQError &err) = 0;
        virtual void syncMarksGenStartSlave(LQError &err) = 0;
        virtual void syncMarksGenStop(LQError &err) = 0;
        virtual void syncMarksGenManual(LQError &err) = 0;
    };
}

#endif // DEVSYNCMARK_H
