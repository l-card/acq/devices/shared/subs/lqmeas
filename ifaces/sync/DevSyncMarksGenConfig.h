#ifndef LQMEAS_DEVSYNCMARKSGENCONFIG_H
#define LQMEAS_DEVSYNCMARKSGENCONFIG_H

#include "lqmeas/devs/DeviceConfigIface.h"

namespace LQMeas {
    class DevSyncMarkType;

    class DevSyncMarksGenConfig : public DeviceConfigIface {
        Q_OBJECT
    public:
        QString cfgIfaceName() const override {return  QStringLiteral("SyncMarksGen");}

        virtual bool syncMarksGenEnabled(const DevSyncMarkType &type) const = 0;
    };
}

#endif // DEVSYNCMARKSGENCONFIG_H
