#ifndef LQMEAS_DEVSYNCMARKSRECVINFO_H
#define LQMEAS_DEVSYNCMARKSRECVINFO_H

#include <QList>

namespace LQMeas {
    class DevSyncMarkType;

    class DevSyncMarksRecvInfo {
    public:
        virtual ~DevSyncMarksRecvInfo();
        virtual QList<const DevSyncMarkType *> syncMarkRecvTypes() const = 0;
    };
}

#endif // DEVSYNCMARKSRECVINFO_H
