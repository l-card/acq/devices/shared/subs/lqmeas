#ifndef LQMEAS_DEVSYNCMARKSRECVCONFIG_H
#define LQMEAS_DEVSYNCMARKSRECVCONFIG_H

#include "lqmeas/devs/DeviceConfigIface.h"

namespace LQMeas {
    class DevSyncMarkType;

    class DevSyncMarksRecvConfig : public DeviceConfigIface {
        Q_OBJECT
    public:
        QString cfgIfaceName() const override {return  QStringLiteral("SyncMarksRecv");}

        virtual bool syncMarksRecvStartOnMark() const = 0;
        virtual bool syncMarksRecvEnabled(const DevSyncMarkType &type) const = 0;
        virtual bool syncMarksRecvAvailable(const DevSyncMarkType &type) const = 0;
        virtual double syncMarksRecvFreq(const DevSyncMarkType &type) const = 0;

    protected:
        void syncMarksRecvFreqChangedNotify();
    Q_SIGNALS:
        void syncMarksRecvFreqChanged();
    };
}

#endif // DEVSYNCMARKSRECVCONFIG_H
