#include "DevOutAsyncDig.h"

namespace LQMeas {

    DevOutAsyncDig::~DevOutAsyncDig() {

    }

    void DevOutAsyncDig::outAsyncDigAll(unsigned val, unsigned mask, LQError &err) {
        protOutAsyncDig(val, mask, err);
    }

    void DevOutAsyncDig::outAsyncDigAll(unsigned val, LQError &err) {
        return outAsyncDigAll(val, 0, err);
    }

    void DevOutAsyncDig::outAsyncDigCh(int ch, bool val, LQError &err)  {
        return outAsyncDigAll((val ? 1 : 0) << ch, ~(1 << ch), err);
    }

}
