#ifndef DEVOUTASYNCDIGSWMASK_H
#define DEVOUTASYNCDIGSWMASK_H

#include "DevOutAsyncDig.h"
namespace LQMeas {
    class DevOutAsyncDigSwMask : public DevOutAsyncDig {
    public:
        DevOutAsyncDigSwMask(unsigned def_val = 0);

        virtual unsigned outAsyncCurValue() const {return m_out_val;}
    protected:
        void protOutAsyncDig(unsigned val, unsigned mask, LQError &err) override;

        virtual void protOutAsyncDigAll(unsigned val, LQError &err) = 0;
    private:
        unsigned m_out_val;
    };
}

#endif // DEVOUTASYNCDIGSWMASK_H
