#include "DevOutAsyncDigSwMask.h"

namespace LQMeas {
    DevOutAsyncDigSwMask::DevOutAsyncDigSwMask(unsigned def_val) : m_out_val{def_val} {

    }

    void DevOutAsyncDigSwMask::protOutAsyncDig(unsigned val, unsigned mask, LQError &err) {
        m_out_val = (val & ~mask) | (m_out_val & mask);
        protOutAsyncDigAll(m_out_val, err);
    }
}
