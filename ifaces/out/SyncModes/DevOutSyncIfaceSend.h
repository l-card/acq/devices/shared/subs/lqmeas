#ifndef LQMEAS_DEVOUTSYNCIFACESEND_H
#define LQMEAS_DEVOUTSYNCIFACESEND_H

class LQError;

namespace LQMeas {
    class DevOutSyncIfaceSend {
    public:
        virtual ~DevOutSyncIfaceSend();
        virtual void privOutSyncSendData(const double *dac_data, int dac_size,
                                         const unsigned *dig_data, int dig_size,
                                         unsigned flags, unsigned tout, LQError &err) = 0;
        virtual bool privOutSyncHasDefferedData() const = 0;
        virtual void privOutSyncTryFlushData(unsigned tout, LQError &err) = 0;
        virtual void privOutSyncSendFinish(LQError &err) = 0;

        virtual void privOutSyncFlushData(unsigned tout, LQError &err);

        virtual bool privOutSyncUnderflowOccured() const = 0;
    };
}

#endif // LQMEAS_DEVOUTSYNCIFACESEND_H
