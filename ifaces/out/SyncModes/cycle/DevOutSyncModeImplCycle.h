#ifndef LQMEAS_DEVOUTSYNCMODEIMPLCYCLE_H
#define LQMEAS_DEVOUTSYNCMODEIMPLCYCLE_H


#include "../DevOutSyncModeImpl.h"


namespace LQMeas {
    class Device;
    class DevOutSyncIfaceCycle;

    class DevOutSyncModeImplCycle : public DevOutSyncModeImpl {
        Q_OBJECT
    public:
        DevOutSyncModeImplCycle(Device *dev, DevOutSyncIfaceCycle *dev_cycle);


        DevOutSyncConfig::OutSyncRamMode outSyncMode() override {return DevOutSyncConfig::OutSyncRamMode::Cycle;}
        void outSyncModeImplSigSet(QSharedPointer<OutRamSignalGenerator> &ramSigGenerator, LQError &err) override;
        void outSyncModeImplStartPrepare(QSharedPointer<OutRamSignalGenerator> &ramSigGenerator, LQError &err) override;
        void outSyncModeImplStartRequest(LQError &err) override;
        bool outSyncModeImplStartWaitDone(unsigned tout, LQError &err) override;
        void outSyncModeImplStopRequest(unsigned tout, LQError &err) override;
        void outSyncModeImplStopWait(LQError &err) override;
        void outSyncModeImplSigUpdateRequest(QSharedPointer<OutRamSignalGenerator> &ramSigGenerator, LQError &err) override;
        bool outSyncModeImplSigUpdateWaitDone(unsigned tout, LQError &err) override;
    private:
        Device *m_dev;
        DevOutSyncIfaceCycle *m_iface;
        const double m_cycle_freq_rel_err;

        int m_sig_size;

        void outCalcCycleSigsParams(OutRamSignalGenerator *ramSigGenerator,
                                    double out_freq, int max_sig_size,
                                    int *res_size, LQError &err);
    };
}

#endif // LQMEAS_DEVOUTSYNCMODEIMPLCYCLE_H
