#ifndef LQMEAS_DEVOUTSYNCIFACECYCLE_H
#define LQMEAS_DEVOUTSYNCIFACECYCLE_H

#include "../DevOutSyncIfaceSend.h"
#include <QtGlobal>

namespace LQMeas {
    class OutRamSignalGenerator;

    class DevOutSyncIfaceCycle : public virtual DevOutSyncIfaceSend {
    public:
        virtual ~DevOutSyncIfaceCycle();
        /* Максимально возможное количество точек на сигнал для циклической генерации */
        virtual int outCycleGenMaxSize(const OutRamSignalGenerator &generator) const {
            Q_UNUSED(generator)
            return 0;
        }

        virtual void privOutCycleLoadStart(const OutRamSignalGenerator &generator, int size, LQError &err) = 0;
        virtual void privOutCycleLoadFinish(LQError &err) = 0;
        virtual void privOutCycleGenStartRequest(LQError &err) = 0;
        virtual bool privOutCycleGenStartWaitDone(unsigned tout, LQError &err) {
            Q_UNUSED(tout) Q_UNUSED(err)
            return true;
        }
        virtual void privOutCycleGenUpdateRequest(LQError &err) = 0;
        virtual bool privOutCycleGenUpdateWaitDone(unsigned tout, LQError &err) {
            Q_UNUSED(tout) Q_UNUSED(err)
            return true;
        }
        virtual void privOutCycleGenStopRequest(unsigned tout, LQError &err) = 0;
        virtual void privOutCycleGenStop(LQError &err) = 0;
    };
}

#endif // DEVOUTSYNCIFACECYCLE_H
