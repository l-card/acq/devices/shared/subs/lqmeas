#include "DevOutSyncModeImplCycle.h"
#include "DevOutSyncIfaceCycle.h"
#include "lqmeas/StdErrors.h"
#include "lqmeas/devs/Device.h"
#include "lqmeas/ifaces/out/DevOutSync.h"
#include "lqmeas/log/Log.h"
#include <qmath.h>

// функция вычисляет НОД
static int gcd( int a, int b ) {
    return b ? gcd( b, a % b ) : a;
}

// функция вычисляет НОК, используя НОД
static int lcm( int a, int b ){
    return a / gcd( a, b ) * b;
}


// source:
// http://www.isi.edu/~johnh/BLOG/1999/0728_RATIONAL_PI/fast_convergence.txt
// theory:
// https://en.wikipedia.org/wiki/Continued_fraction#Best_rational_approximations

static int rat_approx(double val, long *out_n, long *out_d, long max_d, double rel_err) {
    const int max_iter = 20;
    long coeff[max_iter];
    int sign = 1;
    double x = val;
    for (int i = 0; i < max_iter; ++i) {
        if (x < 0.0) {
            sign = -sign;
            x = -x;
        }
        long ipart = (long)x + (x - floor(x) > 0.5);
        coeff[i] = ipart * sign;
        x = 1.0 / (x - ipart);

        // compute numerator and denominator
        long num = coeff[i], den = 1;
        for (int j = i - 1; j >= 0; --j) {
            long t = num;
            num = coeff[j] * num + den;
            den = t;
        }
        if (den < 0) {
            num = -num;
            den = -den;
        }
        if (den > max_d)
            break;
        *out_n = num;
        *out_d = den;
        double approx_val = static_cast<double>(num) / den;
        double err = fabs(approx_val / val - 1.0);
        if (err <= rel_err)
            return 1;
    }
    return 0;
}


namespace LQMeas {
    DevOutSyncModeImplCycle::DevOutSyncModeImplCycle(LQMeas::Device *dev, LQMeas::DevOutSyncIfaceCycle *dev_cycle) :
        m_dev{dev}, m_iface{dev_cycle}, m_cycle_freq_rel_err{0.000001} {

    }

    void DevOutSyncModeImplCycle::outSyncModeImplSigSet(QSharedPointer<OutRamSignalGenerator> &ramSigGenerator, LQError &err) {

        outCalcCycleSigsParams(ramSigGenerator.data(), m_dev->devOutSync()->outSyncConfig()->outSyncGenFreq(),
                               m_iface->outCycleGenMaxSize(*ramSigGenerator),
                               &m_sig_size, err);
    }

    void DevOutSyncModeImplCycle::outSyncModeImplStartPrepare(QSharedPointer<OutRamSignalGenerator> &ramSigGenerator, LQError &err) {

        m_iface->privOutCycleLoadStart(*ramSigGenerator, m_sig_size, err);
        if (err.isSuccess()) {
            QVector<double> dac_data;
            QVector<unsigned> dig_data;

            ramSigGenerator->genNextData(m_sig_size, dac_data, dig_data, err);
            if (err.isSuccess()) {
                m_iface->privOutSyncSendData(dac_data.data(), dac_data.size(),
                                            dig_data.data(), dig_data.size(), 0, 0, err);
            }
            if (err.isSuccess())
                m_iface->privOutSyncFlushData(0, err);
            if (err.isSuccess())
                m_iface->privOutCycleLoadFinish(err);
        }

        if (err.isSuccess()) {
            LQMeasLog->detail(Device::tr("Output cycle generation setup done successfully"), m_dev);
        } else {
            LQMeasLog->error(Device::tr("Output cycle generation setup error"), err, m_dev);
        }
    }

    void DevOutSyncModeImplCycle::outSyncModeImplStartRequest(LQError &err) {
        m_iface->privOutCycleGenStartRequest(err);
        if (err.isSuccess()) {
            LQMeasLog->detail(Device::tr("Output cycle generation started successfully"), m_dev);
        } else {
            LQMeasLog->error(Device::tr("Output cycle generation start error"), err, m_dev);
        }
    }

    bool DevOutSyncModeImplCycle::outSyncModeImplStartWaitDone(unsigned tout, LQError &err)     {
        return m_iface->privOutCycleGenStartWaitDone(tout, err);
    }

    void DevOutSyncModeImplCycle::outSyncModeImplStopRequest(unsigned tout, LQError &err) {
        m_iface->privOutCycleGenStopRequest(tout, err);
    }

    void DevOutSyncModeImplCycle::outSyncModeImplStopWait(LQError &err) {
        m_iface->privOutCycleGenStop(err);
        if (err.isSuccess()) {
            LQMeasLog->detail(Device::tr("Output cycle generation stopped successfully"), m_dev);
        } else {
            LQMeasLog->error(Device::tr("Output cycle generation stop error"), err, m_dev);
        }
    }

    void DevOutSyncModeImplCycle::outSyncModeImplSigUpdateRequest(QSharedPointer<OutRamSignalGenerator> &ramSigGenerator, LQError &err) {
        outSyncModeImplStartPrepare(ramSigGenerator, err);
        if (err.isSuccess()) {
            m_iface->privOutCycleGenUpdateRequest(err);
            if (err.isSuccess()) {
                LQMeasLog->detail(Device::tr("Output cycle generation signal updated successfully"), m_dev);
            } else {
                LQMeasLog->error(Device::tr("Output cycle generation signal update error"), err, m_dev);
            }
        }
    }

    bool DevOutSyncModeImplCycle::outSyncModeImplSigUpdateWaitDone(unsigned tout, LQError &err) {
        return m_iface->privOutCycleGenUpdateWaitDone(tout, err);
    }

    void DevOutSyncModeImplCycle::outCalcCycleSigsParams(OutRamSignalGenerator *ramSigGenerator,
                                                          double out_freq, int max_sig_size,
                                                          int *res_size, LQError &err) {
        int sig_size {1};
        const QList<QSharedPointer<SignalConfig> > &ram_sigs {ramSigGenerator->allSignalsCfg()};

        for (const QSharedPointer<SignalConfig> &sigCfg : ram_sigs) {
            sigCfg->setPeriodicMode(true);
            double sig_freq {sigCfg->signalFreq()};
            long n = 0, d;

            if (sig_freq <= 0) {
                /* если сигнал имеет явно заданное кол-во точек, а не
                   определяемое по частоте, то нужно установить именно это
                   кол-во отсчетов - как НОК. Если не хватает места - то сигнал
                   не устанавливаем */
                int cur_size {lcm(sig_size, sigCfg->signalMinPoints())};
                if (max_sig_size && (cur_size > max_sig_size)) {
                    err = StdErrors::OutSignalPointCount();
                    break;
                }
            } else {
                /* если сигнал задан частотой, то пытаемся
                   подобрать ближайшую к отношению текущей частоты и частоты
                   сигнала дробь */


                /* если задана силшком большая частота - обрезаем до макс. возможной */
                if (out_freq < (sigCfg->signalMinPoints() * sig_freq)) {
                    sig_freq = out_freq / sigCfg->signalMinPoints();
                }

                if (sigCfg->signalStrictPoints()) {
                    const int points {static_cast<int>(out_freq/sig_freq + 0.5)};
                    sig_freq = out_freq / points;
                }


                rat_approx(sig_freq/(out_freq/sig_size), &n, &d, max_sig_size/sig_size,
                           m_cycle_freq_rel_err);
                /* qDebug() << QString("sig_f = %0, sig_size = %1, n = %2, d = %3")
                            .arg(QString::number(sig_freq))
                            .arg(QString::number(sig_size))
                            .arg(QString::number(n))
                            .arg(QString::number(d)); */
                /* нулевой числитель => доступного буфера не достаточно даже для одного
                   периода */
                if (n == 0) {
                    err = StdErrors::OutSignalPointCount();
                    break;
                }
                sig_size *= d;
            }
        }

        if (err.isSuccess()) {
            for (const QSharedPointer<SignalConfig> &sigCfg : ram_sigs) {
                if (sigCfg->signalFreq() > 0) {
                    if (!sigCfg->signalStrictPoints()) {
                        int periods = static_cast<int>(sigCfg->signalFreq()*sig_size/out_freq + 0.5);
                        if (periods==0)
                            periods = 1;
                        sigCfg->setRealParams(out_freq * periods / sig_size, out_freq);
                    } else {
                        int req_size = static_cast<int>(out_freq / sigCfg->signalFreq() + 0.5);
                        int periods = static_cast<int>(static_cast<double>(sig_size)/req_size + 0.5);
                        if (periods == 0)
                            periods = 1;
                        req_size = sig_size/periods;
                        sigCfg->setRealParams(out_freq/req_size, out_freq);
                    }
                } else {
                    sigCfg->setRealParams(SignalConfig::unspecifiedFreq(), out_freq);
                }
            }
        }

        if (err.isSuccess())
            *res_size = sig_size;
    }
}
