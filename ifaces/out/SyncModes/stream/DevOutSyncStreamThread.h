#ifndef LQMEAS_DEVOUTSYNCSTREAMTHREAD_H
#define LQMEAS_DEVOUTSYNCSTREAMTHREAD_H

#include "../../OutRamSignalGenerator.h"
#include <QThread>
#include "LQError.h"
class QElapsedTimer;

namespace LQMeas {

    class DevOutSyncIfaceStream;
    class Device;

    class DevOutSyncStreamThread  : public QThread {
        Q_OBJECT
    public:
        explicit DevOutSyncStreamThread(QSharedPointer<OutRamSignalGenerator> &generator,
                                        Device *dev, DevOutSyncIfaceStream *stream_iface);

        void setStopReq(unsigned tout) {m_stop_req = true; m_stop_tout = tout;}

        const LQError &error() const {return m_err;}
    Q_SIGNALS:
        void underflowOccured();
        void errorOccured(const LQError &err);
    protected:
        void run() override;
    private:
        unsigned remainTout(QElapsedTimer &tmr, unsigned tout);

        static constexpr unsigned out_send_addit_tout_ms {100};
        static constexpr unsigned out_default_stop_tout {30000};


        DevOutSyncIfaceStream *m_stream;
        Device *m_dev;
        QSharedPointer<OutRamSignalGenerator> m_generator;

        bool m_stop_req {false};
        unsigned m_stop_tout {out_default_stop_tout};
        LQError m_err;
    };
}

#endif // LQMEAS_DEVOUTSYNCSTREAMTHREAD_H
