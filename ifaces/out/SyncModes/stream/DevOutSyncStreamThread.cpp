#include "DevOutSyncStreamThread.h"
#include "lqmeas/devs/Device.h"
#include "lqmeas/ifaces/out/DevOutSync.h"
#include "lqmeas/ifaces/out/DevOutSyncEchoTracker.h"
#include "DevOutSyncIfaceStream.h"
#include "lqmeas/log/Log.h"
#include "lqmeas/devs/DeviceConfig.h"
#include "lqmeas/StdErrors.h"
#include <QElapsedTimer>


namespace LQMeas {
    DevOutSyncStreamThread::DevOutSyncStreamThread(
            QSharedPointer<OutRamSignalGenerator> &generator, Device *dev,
            LQMeas::DevOutSyncIfaceStream *stream_iface) :
        m_stream{stream_iface},
        m_dev{dev},
        m_generator{generator} {

    }

    void DevOutSyncStreamThread::run() {
        LQError err;
        quint64 gen_pos {0};
        DevOutSyncEchoTracker *echoTrack {m_dev->config().outSyncConfig()->outSyncEchoEnabled() ?
                    m_dev->devOutSyncEchoTracker() : nullptr};
        unsigned block_time_ms {static_cast<unsigned>(m_dev->devOutSync()->outSyncConfig()->outSyncSendSignalBockTimeMs())};
        int gen_points {static_cast<int>(m_dev->devOutSync()->outSyncConfig()->outSyncGenFreq()*block_time_ms/1000)};
        unsigned def_tout {block_time_ms + out_send_addit_tout_ms};
        bool stop_at_sigend {m_dev->config().outSyncConfig()->outSyncStreamLoadStopAtSignalEnd()};
        bool gen_last_pt {m_dev->config().outSyncConfig()->outSyncGenLastPointRequired()};
        bool has_data {true};


        while (err.isSuccess() && !m_stop_req && has_data) {
            QVector<double> dac_data;
            QVector<unsigned> dig_data;
            bool send_done {false};

            if (stop_at_sigend) {
                qint64 rem_pt {m_generator->remainingSigPoint()};
                if ((rem_pt >= 0) && (rem_pt < gen_points))
                    gen_points = rem_pt;
                if (gen_points == 0)
                    has_data = false;
            }

            if (has_data) {
                m_generator->genNextData(gen_points, dac_data, dig_data, err);

                while (err.isSuccess() && !m_stop_req && !send_done) {
                    m_stream->privOutSyncSendData(dac_data.data(), dac_data.size(),
                                                  dig_data.data(), dig_data.size(),
                                                  0, def_tout, err);
                    if (err == StdErrors::SendBusy()) {
                        err = LQError::Success();
                    } else if (err.isSuccess()) {
                        send_done = true;
                        gen_pos += static_cast<quint32>(gen_points);
                        /* проверяем опустошение буфера и сигнализируем сообщением
                           о данном событии, если оно произошло */
                        if (m_stream->privOutSyncUnderflowOccured()) {
                            Q_EMIT underflowOccured();
                        }
                    }
                }

                if (err.isSuccess() && !m_stop_req && echoTrack) {
                    bool echo_recvd_done {false};
                    m_stream->privOutSyncFlushData(def_tout, err);
                    while (err.isSuccess() && !m_stop_req && !echo_recvd_done) {
                        quint64 echo_pos;
                        echoTrack->outSyncEchoTrackWait(gen_pos, def_tout, echo_pos, err);
                        if (err.isSuccess() && (echo_pos == gen_pos)) {
                            echo_recvd_done = true;
                        }
                    }
                }
            }
        }

        QElapsedTimer stopTmr;
        stopTmr.start();

        /** @todo возможно следует сделать возможность изменить последней точки
         * с 0 на заданное число. Возможно также разделить настройки ожидания
         * завершения вывода сигнала и вывод дополнительной последней точки */
        if (err.isSuccess() && gen_last_pt) {
            QVector<double> dac_data;
            QVector<unsigned> dig_data;
            const int dac_ch_cnt {m_generator->dacSignalsCount()};
            const int dig_ch_cnt {m_generator->digSignalsCount()};

            for (int ch_idx {0}; ch_idx< dac_ch_cnt; ++ch_idx) {
                dac_data.append(0);
            }
            if (dig_ch_cnt > 0)
                dig_data.append(0);
            m_stream->privOutSyncSendData(dac_data.data(), dac_data.size(),
                                          dig_data.data(), dig_data.size(),
                                          0, def_tout, err);
            if (err.isSuccess())
                m_stream->privOutSyncFlushData(def_tout, err);

             if (err.isSuccess() && echoTrack) {
                 quint64 echo_pos;
                 /* на момент останова кол-во неподтвержденных слов соответствует
                  * кол-ву точек предзагруженных перед стартом (т.к. это количество
                  * всегда оставляем в буфере) + один последний отсчет */
                 quint64 preload_pts = static_cast<quint64>(
                             m_dev->devOutSync()->outSyncConfig()->outSyncGenFreq() *
                             m_dev->config().outSyncConfig()->outSyncPreloadSingalTimeMs()/1000);

                 echoTrack->outSyncEchoTrackWait(gen_pos + preload_pts + 1,
                                                 remainTout(stopTmr, m_stop_tout),
                                                 echo_pos, err);
             }
        }




        /* если завершили генерацию по концу сигнала, а не запросу на останов,
         * то дожидаемся явного запроса для полного выключения генерации */
        while (err.isSuccess() && !m_stop_req) {
            if (m_stream->privOutSyncHasDefferedData()) {
                m_stream->privOutSyncTryFlushData(100, err);
            } else {
                QThread::msleep(50);
            }
        }


        LQError stop_err;
        m_stream->privOutStreamStop(remainTout(stopTmr, m_stop_tout), stop_err);
        if (!stop_err.isSuccess())
            err.update(stop_err);
        m_err = err;

        if (err.isSuccess()) {
            LQMeasLog->detail(tr("Output data stream stopped successfully"), m_dev);
        } else {
            LQMeasLog->error(tr("Output data stream stop error"), err, m_dev);
            Q_EMIT errorOccured(err);
        }
    }

    unsigned DevOutSyncStreamThread::remainTout(QElapsedTimer &tmr, unsigned tout) {
        const qint64 elapsed {tmr.elapsed()};
        return elapsed > tout ? 0 : static_cast<unsigned>(tout - elapsed);
    }
}
