#ifndef LQMEAS_DEVOUTSYNCMODEIMPLSTREAM_H
#define LQMEAS_DEVOUTSYNCMODEIMPLSTREAM_H

#include "../DevOutSyncModeImpl.h"


namespace LQMeas {
    class Device;
    class DevOutSyncIfaceStream;
    class DevOutSyncStreamThread;

    class DevOutSyncModeImplStream : public DevOutSyncModeImpl {
        Q_OBJECT
    public:
        DevOutSyncModeImplStream(Device *dev, DevOutSyncIfaceStream *dev_stream);
        ~DevOutSyncModeImplStream() override;

        DevOutSyncConfig::OutSyncRamMode outSyncMode() override {return DevOutSyncConfig::OutSyncRamMode::Stream;}
        void outSyncModeImplSigSet(QSharedPointer<OutRamSignalGenerator> &ramSigGenerator, LQError &err) override;
        void outSyncModeImplStartPrepare(QSharedPointer<OutRamSignalGenerator> &ramSigGenerator, LQError &err) override;
        void outSyncModeImplStartRequest(LQError &err) override;
        bool outSyncModeImplStartWaitDone(unsigned tout, LQError &err) override;
        void outSyncModeImplStopRequest(unsigned tout, LQError &err) override;
        void outSyncModeImplStopWait(LQError &err) override;
        void outSyncModeImplSigUpdateRequest(QSharedPointer<OutRamSignalGenerator> &ramSigGenerator, LQError &err) override;
        bool outSyncModeImplSigUpdateWaitDone(unsigned tout, LQError &err) override;
    private:
        int preloadPoints();

        Device *m_dev;
        DevOutSyncIfaceStream *m_stream_iface;
        DevOutSyncStreamThread *m_thread {nullptr};

        QSharedPointer<OutRamSignalGenerator> m_generator;
    };
}

#endif // LQMEAS_DEVOUTSYNCMODEIMPLSTREAM_H
