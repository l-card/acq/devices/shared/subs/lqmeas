#ifndef LQMEAS_DEVOUTSYNCIFACESTREAM_H
#define LQMEAS_DEVOUTSYNCIFACESTREAM_H

#include "../DevOutSyncIfaceSend.h"
#include <QtGlobal>

namespace LQMeas {
    class DevOutSyncIfaceStream : public virtual DevOutSyncIfaceSend {
    public:
        virtual ~DevOutSyncIfaceStream();

        virtual void privOutStreamInit(LQError &err) = 0;
        virtual void privOutStreamStartRequest(LQError &err) = 0;
        virtual bool privOutStreamStartWaitDone(unsigned tout, LQError &err) {
            Q_UNUSED(tout) Q_UNUSED(err)
            return true;
        }
        virtual void privOutStreamStop(unsigned tout, LQError &err) = 0;
    };
}

#endif // LQMEAS_DEVOUTSYNCIFACESTREAM_H
