#include "DevOutSyncModeImplStream.h"
#include "lqmeas/ifaces/out/DevOutSync.h"
#include "DevOutSyncIfaceStream.h"
#include "lqmeas/StdErrors.h"
#include "lqmeas/devs/Device.h"
#include "lqmeas/devs/DeviceConfig.h"
#include "DevOutSyncStreamThread.h"

namespace LQMeas {
    DevOutSyncModeImplStream::DevOutSyncModeImplStream(Device *dev, DevOutSyncIfaceStream *dev_stream) :
        m_dev{dev}, m_stream_iface{dev_stream} {

    }

    DevOutSyncModeImplStream::~DevOutSyncModeImplStream() {
        delete m_thread;
    }

    void DevOutSyncModeImplStream::outSyncModeImplSigSet(QSharedPointer<OutRamSignalGenerator> &ramSigGenerator, LQError &err) {
        Q_UNUSED(err)

        const double out_freq {m_dev->devOutSync()->outSyncConfig()->outSyncGenFreq()};
        const QList<QSharedPointer<SignalConfig> > &sigs {ramSigGenerator->allSignalsCfg()};

        for (const QSharedPointer<SignalConfig> &sigCfg : sigs) {
            if (sigCfg->signalFreq() > 0) {
                if (sigCfg->signalStrictPoints()) {
                    int pts {static_cast<int>(out_freq/sigCfg->signalFreq() + 0.5)};
                    if (pts < sigCfg->signalMinPoints())
                        pts = sigCfg->signalMinPoints();
                    sigCfg->setRealParams(out_freq/pts, out_freq);
                } else {
                    sigCfg->setRealParams(sigCfg->signalFreq(), out_freq);
                }
            }
        }
    }

    void DevOutSyncModeImplStream::outSyncModeImplStartPrepare(
            QSharedPointer<OutRamSignalGenerator> &ramSigGenerator, LQError &err) {
        m_stream_iface->privOutStreamInit(err);
        if (err.isSuccess()) {
            m_generator = ramSigGenerator;
            QVector<double> dac_data;
            QVector<unsigned> dig_data;


            int gen_points = preloadPoints();
            /* если включен вывод только полезных точек и точек в сегнала меньше,
             * чем установленное время предзагрузки, то можем ограничить предзагрузку
             * только полезными точками для ускорения процесса */
            if (m_dev->config().outSyncConfig()->outSyncStreamLoadStopAtSignalEnd()) {
                int rem_points = m_generator->remainingSigPoint();
                if ((rem_points > 0) && (rem_points < gen_points))
                    gen_points = rem_points;
            }

            m_generator->genNextData(gen_points, dac_data, dig_data, err);

            if (err.isSuccess()) {
                m_stream_iface->privOutSyncSendData(
                            dac_data.data(), dac_data.size(),
                            dig_data.data(), dig_data.size(),
                            0, 0, err);
            }
        }
    }

    void DevOutSyncModeImplStream::outSyncModeImplStartRequest(LQError &err)  {
        m_stream_iface->privOutStreamStartRequest(err);
    }

    bool DevOutSyncModeImplStream::outSyncModeImplStartWaitDone(unsigned tout, LQError &err)     {
        bool ok {m_stream_iface->privOutStreamStartWaitDone(tout, err)};
        if (err.isSuccess() && ok) {
            delete m_thread;
            m_thread = new DevOutSyncStreamThread{m_generator, m_dev, m_stream_iface};
            connect(m_thread, &DevOutSyncStreamThread::underflowOccured,
                    this, &DevOutSyncModeImplStream::underflowOccured);
            connect(m_thread, &DevOutSyncStreamThread::errorOccured,
                    this, &DevOutSyncModeImplStream::errorOccured);
            m_thread->start();
        }
        return ok;
    }

    void DevOutSyncModeImplStream::outSyncModeImplStopRequest(unsigned tout, LQError &err) {
        Q_UNUSED(err)
        if (m_thread) {
            m_thread->setStopReq(tout);
        } else if (m_stream_iface) {
            m_stream_iface->privOutStreamStop(tout, err);
        }
    }

    void DevOutSyncModeImplStream::outSyncModeImplStopWait(LQError &err) {
        Q_UNUSED(err)
        if (m_thread) {
            m_thread->wait();
            err = m_thread->error();
            delete m_thread;
            m_thread = nullptr;
            m_generator.clear();
        }
    }

    void DevOutSyncModeImplStream::outSyncModeImplSigUpdateRequest(
            QSharedPointer<OutRamSignalGenerator> &ramSigGenerator, LQError &err) {
        Q_UNUSED(ramSigGenerator)
        /* Пока не поддерживаем замену на лету */
        err = StdErrors::OutGenRunning();
    }

    bool DevOutSyncModeImplStream::outSyncModeImplSigUpdateWaitDone(unsigned tout, LQError &err) {
        Q_UNUSED(tout)
        err = StdErrors::OutGenRunning();
        return false;
    }

    int DevOutSyncModeImplStream::preloadPoints() {
        return static_cast<int>(m_dev->devOutSync()->outSyncConfig()->outSyncGenFreq()*
                                m_dev->config().outSyncConfig()->outSyncPreloadSingalTimeMs()/1000);
    }

}

