#ifndef LQMEAS_DEVOUTSYNCMODEIMPL_H
#define LQMEAS_DEVOUTSYNCMODEIMPL_H

#include <QObject>
#include <QSharedPointer>
#include "lqmeas/ifaces/out/DevOutSyncConfig.h"
#include "lqmeas/ifaces/out/OutRamSignalGenerator.h"

namespace LQMeas {
    class DevOutSyncModeImpl : public QObject {
        Q_OBJECT
    public:
        virtual ~DevOutSyncModeImpl();

        virtual DevOutSyncConfig::OutSyncRamMode outSyncMode() = 0;
        /* Установка генератора для генерации новых точек сигнала.
           В данной функции класс реализации должен поправить сигналы, если это
           требуется в данном режиме и запомнить генератор для дальнейших функций. */
        virtual void outSyncModeImplSigSet(QSharedPointer<OutRamSignalGenerator> &ramSigGenerator, LQError &err) = 0;
        virtual void outSyncModeImplStartPrepare(QSharedPointer<OutRamSignalGenerator> &ramSigGenerator, LQError &err) = 0;
        virtual void outSyncModeImplStartRequest(LQError &err) = 0;
        virtual bool outSyncModeImplStartWaitDone(unsigned tout, LQError &err) = 0;
        virtual void outSyncModeImplStopRequest(unsigned tout, LQError &err) = 0;
        virtual void outSyncModeImplStopWait(LQError &err) = 0;
        virtual void outSyncModeImplSigUpdateRequest(QSharedPointer<OutRamSignalGenerator> &ramSigGenerator, LQError &err) = 0;
        virtual bool outSyncModeImplSigUpdateWaitDone(unsigned tout, LQError &err) = 0;
    Q_SIGNALS:
        void underflowOccured();
        void errorOccured(LQError err);
    };
}

#endif // LQMEAS_DEVOUTSYNCMODEIMPL_H
