#include "DevOutSyncIfaceSend.h"
#include "lqmeas/StdErrors.h"

namespace LQMeas {
    DevOutSyncIfaceSend::~DevOutSyncIfaceSend() {

    }

    void DevOutSyncIfaceSend::privOutSyncFlushData(unsigned tout, LQError &err) {
        if (privOutSyncHasDefferedData()) {
            privOutSyncTryFlushData(tout, err);
            if (err.isSuccess() && privOutSyncHasDefferedData()) {
                err = StdErrors::SendBusy();
            }
        }

        LQError ferr;
        privOutSyncSendFinish(ferr);
        err.update(ferr);
    }
}
