#ifndef LQMEAS_DEVOUTDACCONVERTER_H
#define LQMEAS_DEVOUTDACCONVERTER_H

#include <QVector>
#include <QSharedPointer>
class LQError;

namespace LQMeas {
    class Device;
    class DeviceInfo;
    class SignalConverter;

    class DevOutDacConverter {
    public:
        void outDacSetConverter(int ch_num, const SignalConverter &conv, LQError &err);
        void outDacClearConverter(int ch_num);
        void outDacClearConverters();
        bool outDacHasConverter(int ch_num) const;
        const SignalConverter &outDacConverter(int ch_num) const;

        double outDacConvertValue(int ch_num, double val) const;
    protected:
        DevOutDacConverter(Device *dev);
    private:
        void updateInfo(const DeviceInfo &info);

        Device *m_dev;
        QVector<QSharedPointer<SignalConverter> > m_ch_converters;



        friend class Device;
    };
}

#endif // LQMEAS_DEVOUTDACCONVERTER_H
