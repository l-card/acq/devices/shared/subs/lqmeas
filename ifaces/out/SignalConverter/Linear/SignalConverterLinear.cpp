#include "SignalConverterLinear.h"

namespace LQMeas {
    SignalConverterLinear::SignalConverterLinear(double k, double b) : m_k{k}, m_b{b} {

    }

    SignalConverterLinear::SignalConverterLinear(const SignalConverterLinear &conv) :
        m_k{conv.m_k}, m_b{conv.m_b} {

    }

    void SignalConverterLinear::setK(double k) {
        m_k = k;
    }

    void SignalConverterLinear::setB(double b) {
        m_b = b;
    }

    SignalConverter *SignalConverterLinear::clone() const {
        return new SignalConverterLinear{*this};
    }

    double SignalConverterLinear::covert(double val) {
        return m_k * val + m_b;
    }
}
