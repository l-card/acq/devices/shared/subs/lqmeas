#ifndef LQMEAS_SIGNALCONVERTERLINEAR_H
#define LQMEAS_SIGNALCONVERTERLINEAR_H

#include "../SignalConverter.h"

namespace LQMeas {
    class SignalConverterLinear : public SignalConverter {
    public:
        explicit SignalConverterLinear(double k = 1., double b = 0);
        SignalConverterLinear(const SignalConverterLinear &conv);

        double k() const {return m_k;}
        double b() const {return m_b;}

        void setK(double k);
        void setB(double b);

        SignalConverter *clone() const override;
        double covert(double val) override;
    private:
        double m_k;
        double m_b;
    };
}

#endif // LQMEAS_SIGNALCONVERTERLINEAR_H
