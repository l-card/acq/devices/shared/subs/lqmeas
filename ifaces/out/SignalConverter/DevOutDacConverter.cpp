#include "DevOutDacConverter.h"
#include "../DevOutInfo.h"
#include "lqmeas/devs/DeviceInfo.h"
#include "lqmeas/devs/DeviceTypeInfo.h"
#include "SignalConverter.h"

namespace LQMeas {

    void DevOutDacConverter::outDacSetConverter(int ch_num, const SignalConverter &conv, LQError &err) {
        Q_UNUSED(err)
        m_ch_converters[ch_num] = QSharedPointer<SignalConverter>{conv.clone()};
    }

    void DevOutDacConverter::outDacClearConverter(int ch_num) {
        m_ch_converters[ch_num].clear();
    }

    void DevOutDacConverter::outDacClearConverters() {
        for (QSharedPointer<SignalConverter> &conv : m_ch_converters) {
            conv.clear();
        }
    }

    bool DevOutDacConverter::outDacHasConverter(int ch_num) const {
        return m_ch_converters[ch_num] != nullptr;
    }

    const SignalConverter &DevOutDacConverter::outDacConverter(int ch_num) const {
        return *m_ch_converters[ch_num];
    }

    double DevOutDacConverter::outDacConvertValue(int ch_num, double val) const  {
        if (outDacHasConverter(ch_num)) {
            val = m_ch_converters[ch_num]->covert(val);
        }
        return val;
    }

    DevOutDacConverter::DevOutDacConverter(Device *dev) : m_dev{dev} {

    }

    void DevOutDacConverter::updateInfo(const DeviceInfo &info) {
        const LQMeas::DevOutInfo *outInfo = info.type().out();
        m_ch_converters.resize(outInfo->outDacChannelsCnt());
    }
}
