#ifndef LQMEAS_SIGNALCONVERTER_H
#define LQMEAS_SIGNALCONVERTER_H

namespace LQMeas {
    class SignalConverter {
    public:
        virtual ~SignalConverter();
        virtual SignalConverter *clone() const = 0;
        virtual double covert(double val) = 0;
    };
}

#endif // LQMEAS_SIGNALCONVERTER_H
