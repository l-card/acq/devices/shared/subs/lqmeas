#ifndef LQMEAS_DEVOUTASYNCDAC_H
#define LQMEAS_DEVOUTASYNCDAC_H

class LQError;

namespace LQMeas {
    class Device;

    /* Интерфейс для асинхронного вывода на ЦАП */
    class DevOutAsyncDac {
    public:
        virtual ~DevOutAsyncDac();
        void outAsyncDac(int ch, double val, LQError &err);
    protected:
        virtual void protOutAsyncDac(int ch, double val, LQError &err) = 0;

        DevOutAsyncDac(Device *dev);
    private:
        Device *m_dev;
    };
}

#endif // LQMEAS_DEVOUTASYNCDAC_H
