#include "DevOutSyncConfig.h"
#include "DevOutInfo.h"
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/StdConfigKeys.h"
#include "lqmeas/devs/DeviceTypeInfo.h"
#include "lqmeas/lqtdefs.h"
#include <QJsonObject>
#include <QJsonArray>


namespace LQMeas {
    static const QLatin1String cfgkey_sync_obj              {"Sync"};
    static const QLatin1String cfgkey_dac_chs_array         {StdConfigKeys::dacChannels()};
    static const QLatin1String cfgkey_dout_chs_array        {StdConfigKeys::doutChannels()};
    static const QLatin1String cfgkey_ch_enabled            {StdConfigKeys::enabled()};
    static const QLatin1String cfgkey_ch_gen_mode           {"GenMode"};
    static const QLatin1String cfgkey_ch_preset             {"Preset"};
    static const QLatin1String cfgkey_ch_preset_type        {StdConfigKeys::type()};
    static const QLatin1String cfgkey_ch_preset_value       {StdConfigKeys::value()};
    static const QLatin1String cfgkey_preload_sig_time      {"PreloadSigTime"};
    static const QLatin1String cfgkey_send_sig_block_time   {"SendSigBlockTime"};
    static const QLatin1String cfgkey_stream_last_pt_req    {"StreamLastPtReq"};
    static const QLatin1String cfgkey_stream_load_stop_at_sig_end  {"StreamLoadStopAtSigEnd"};
    static const QLatin1String cfgkey_out_sync_ram_mode     {"OutSyncRamMode"};


    typedef EnumConfigKey<DevOutSyncConfig::DacPreset::Type> PresetTypeKey;
    static const PresetTypeKey preset_types[] = {
        {QStringLiteral("first"),     DevOutSyncConfig::DacPreset::Type::FirstPoint},
        {QStringLiteral("specval"),   DevOutSyncConfig::DacPreset::Type::SpecValue},
        {QStringLiteral("notchange"), DevOutSyncConfig::DacPreset::Type::DontChange},
        {PresetTypeKey::defaultKey(), DevOutSyncConfig::DacPreset::Type::FirstPoint}
    };
    typedef EnumConfigKey<DevOutSyncConfig::OutSyncRamMode> SyncRamModeKey;
    static const SyncRamModeKey sync_ram_modes[] = {
        {QStringLiteral("cycle"),     DevOutSyncConfig::OutSyncRamMode::Cycle},
        {QStringLiteral("stream"),    DevOutSyncConfig::OutSyncRamMode::Stream},
        {SyncRamModeKey::defaultKey(),DevOutSyncConfig::OutSyncRamMode::Cycle}
    };

    typedef EnumConfigKey<DevOutSyncConfig::OutSyncChGenMode> SyncChGenModeKey;
    static const SyncChGenModeKey sync_ch_gen_modes[] = {
        {QStringLiteral("ram"),         DevOutSyncConfig::OutSyncChGenMode::Ram},
        {QStringLiteral("hard"),        DevOutSyncConfig::OutSyncChGenMode::Hard},
        {SyncChGenModeKey::defaultKey(),DevOutSyncConfig::OutSyncChGenMode::Ram}
    };


    DevOutSyncConfig::DevOutSyncConfig(const DevOutInfo &devinfo) :
        DevOutConfig{devinfo} {

        outSyncUpdateDevInfo(devinfo);
    }

    DevOutSyncConfig::DevOutSyncConfig(const DevOutSyncConfig &cfg) :
        DevOutConfig{cfg},
        m_outRamSyncMode{cfg.m_outRamSyncMode},
        m_ram_mode_configurable{cfg.m_ram_mode_configurable},
        m_dacParams{cfg.m_dacParams},
        m_digParams{cfg.m_digParams},
        m_preloadSigTimeMs{cfg.m_preloadSigTimeMs},
        m_sendSigBlockTimeMs{cfg.m_sendSigBlockTimeMs},
        m_streamOutLastPtReq{cfg.m_streamOutLastPtReq},
        m_streamLoadStopAtSigEnd{cfg.m_streamLoadStopAtSigEnd} {
    }

    void DevOutSyncConfig::outSyncSetPreloadSignalTimeMs(int ms) {
        if (m_preloadSigTimeMs != ms) {
            m_preloadSigTimeMs = ms;
            notifyConfigChanged();
        }
    }

    void DevOutSyncConfig::outSyncSetSendSignalBockTimeMs(int ms) {
        if (m_sendSigBlockTimeMs != ms) {
            m_sendSigBlockTimeMs = ms;
            notifyConfigChanged();
        }
    }

    void DevOutSyncConfig::outSyncDacSetChEnabled(int ch, bool en) {
        if (m_dacParams[ch].enabled != en) {
            m_dacParams[ch].enabled = en;
            notifyDacChSyncEnableStateChanged(ch);
            notifyConfigChanged();
        }
    }

    void DevOutSyncConfig::outSyncDigSetChEnabled(int ch, bool en) {
        if (en != m_digParams[ch].enabled) {
            m_digParams[ch].enabled = en;
            notifyDigChSyncEnableStateChanged(ch);
            notifyConfigChanged();
        }
    }

    void DevOutSyncConfig::outSyncDacSetPreset(int ch, const DacPreset &val) {
        m_dacParams[ch].preset = val;
        notifyConfigChanged();
    }

    void DevOutSyncConfig::outSyncDigSetPreset(int ch, const DigPreset &val) {
        m_digParams[ch].preset = val;
        notifyConfigChanged();
    }

    void DevOutSyncConfig::outSyncDacSetChGenMode(int ch, DevOutSyncConfig::OutSyncChGenMode mode) {
        if (mode != m_dacParams[ch].gen_mode) {
            m_dacParams[ch].gen_mode = mode;
            notifyDacChConfigChanged(ch);
            notifyConfigChanged();
        }
    }


    void DevOutSyncConfig::outSyncDigSetChGenMode(int ch, DevOutSyncConfig::OutSyncChGenMode mode) {
        if (mode != m_digParams[ch].gen_mode) {
            m_digParams[ch].gen_mode = mode;
            notifyDigChConfigChanged(ch);
            notifyConfigChanged();
        }
    }

    void DevOutSyncConfig::outSyncSetRamMode(DevOutSyncConfig::OutSyncRamMode mode) {
        if (m_outRamSyncMode != mode) {
            m_outRamSyncMode = mode;
            notifyConfigChanged();
        }
    }



    void DevOutSyncConfig::protLoad(const QJsonObject &outObj) {
        const QJsonObject &syncObj = outObj[cfgkey_sync_obj].toObject();
        if (outSyncDacChannelsCnt() != 0) {
            const QJsonArray &sigsArray = syncObj[cfgkey_dac_chs_array].toArray();
            int ch_cnt = sigsArray.size();
            for (int ch = 0; (ch < outSyncDacChannelsCnt()) && (ch < ch_cnt); ++ch) {
                const QJsonObject &chObj = sigsArray.at(ch).toObject();
                outSyncDacSetChEnabled(ch, chObj[cfgkey_ch_enabled].toBool());
                if (outInfo().outSyncDacChGenModeIsConfigurable()) {
                    outSyncDacSetChGenMode(ch, SyncChGenModeKey::getValue(
                                               sync_ch_gen_modes, chObj[cfgkey_ch_gen_mode].toString()));
                }


                const QJsonObject &presetObj = chObj[cfgkey_ch_preset].toObject();
                DacPreset preset;
                preset.type  = PresetTypeKey::getValue(preset_types, presetObj[cfgkey_ch_preset_type].toString());
                preset.value = presetObj[cfgkey_ch_preset_value].toDouble();
                outSyncDacSetPreset(ch, preset);
            }
        }

        if (outSyncDigChannelsCnt() != 0) {
            const QJsonArray &sigsArray = syncObj[cfgkey_dout_chs_array].toArray();
            int ch_cnt = sigsArray.size();
            for (int ch = 0; (ch < outSyncDigChannelsCnt()) && (ch < ch_cnt); ++ch) {
                const QJsonObject &chObj = sigsArray.at(ch).toObject();
                outSyncDigSetChEnabled(ch, chObj[cfgkey_ch_enabled].toBool());

                if (outInfo().outSyncDigChGenModeIsConfigurable()) {
                    outSyncDigSetChGenMode(ch, SyncChGenModeKey::getValue(
                                               sync_ch_gen_modes, chObj[cfgkey_ch_gen_mode].toString()));
                }

                const QJsonObject &presetObj = chObj[cfgkey_ch_preset].toObject();
                DigPreset preset;
                preset.type  = static_cast<DigPreset::Type>(
                            PresetTypeKey::getValue(preset_types, presetObj[cfgkey_ch_preset_type].toString()));
                preset.value = static_cast<unsigned>(presetObj[cfgkey_ch_preset_value].toInt());
                outSyncDigSetPreset(ch, preset);
            }
        }

        m_preloadSigTimeMs = syncObj[cfgkey_preload_sig_time].toInt(default_preload_time_ms);
        m_sendSigBlockTimeMs = syncObj[cfgkey_send_sig_block_time].toInt(default_block_time_ms);
        m_streamOutLastPtReq =  syncObj[cfgkey_stream_last_pt_req].toBool();
        m_streamLoadStopAtSigEnd = syncObj[cfgkey_stream_load_stop_at_sig_end].toBool();

        if (m_ram_mode_configurable) {
            outSyncSetRamMode(SyncRamModeKey::getValue(
                                  sync_ram_modes, syncObj[cfgkey_out_sync_ram_mode].toString()));
        }
    }

    void DevOutSyncConfig::protSave(QJsonObject &outObj) const {
        QJsonObject syncObj;

        if (outSyncDacChannelsCnt() != 0) {
            QJsonArray sigsArray;
            for (int ch = 0; ch < outSyncDacChannelsCnt(); ++ch) {
                QJsonObject chObj;
                chObj[cfgkey_ch_enabled] = outSyncDacChEnabled(ch);

                if (outInfo().outSyncDacChGenModeIsConfigurable()) {
                    chObj[cfgkey_ch_gen_mode] = SyncChGenModeKey::getKey(
                                sync_ch_gen_modes, outSyncDacChGenMode(ch));
                }

                QJsonObject presetObj;
                DacPreset preset = outSyncDacPreset(ch);
                presetObj[cfgkey_ch_preset_type] = PresetTypeKey::getKey(preset_types, preset.type);
                presetObj[cfgkey_ch_preset_value] = preset.value;
                chObj[cfgkey_ch_preset] = presetObj;
                sigsArray.append(chObj);
            }
            syncObj[cfgkey_dac_chs_array] = sigsArray;
        }

        if (outSyncDigChannelsCnt() != 0) {
            QJsonArray sigsArray;
            for (int ch = 0; ch < outSyncDigChannelsCnt(); ++ch) {
                QJsonObject chObj;
                chObj[cfgkey_ch_enabled] = outSyncDigChEnabled(ch);

                if (outInfo().outSyncDigChGenModeIsConfigurable()) {
                    chObj[cfgkey_ch_gen_mode] = SyncChGenModeKey::getKey(
                                sync_ch_gen_modes, outSyncDigChGenMode(ch));
                }

                QJsonObject presetObj;
                DigPreset preset = outSyncDigPreset(ch);
                presetObj[cfgkey_ch_preset_type] = PresetTypeKey::getKey(
                            preset_types, static_cast<DacPreset::Type>(preset.type));
                presetObj[cfgkey_ch_preset_value] = static_cast<int>(preset.value);
                chObj[cfgkey_ch_preset] = presetObj;
                sigsArray.append(chObj);
            }
            syncObj[cfgkey_dout_chs_array] = sigsArray;
        }

        syncObj[cfgkey_preload_sig_time] = m_preloadSigTimeMs;
        syncObj[cfgkey_send_sig_block_time] = m_sendSigBlockTimeMs;
        syncObj[cfgkey_stream_last_pt_req] = m_streamOutLastPtReq;
        syncObj[cfgkey_stream_load_stop_at_sig_end] = m_streamLoadStopAtSigEnd;

        if (m_ram_mode_configurable) {
            syncObj[cfgkey_out_sync_ram_mode] = SyncRamModeKey::getKey(
                        sync_ram_modes, outSyncRamMode());
        }

        outObj[cfgkey_sync_obj] = syncObj;
    }

    void DevOutSyncConfig::protUpdateInfo(const DeviceTypeInfo *info) {
        DevOutConfig::protUpdateInfo(info);
        outSyncUpdateDevInfo(*info->out());
    }

    void DevOutSyncConfig::notifyDacChFreqChanged(int ch) {
        Q_EMIT outDacChConfiguredFreqChanged(ch);
        Q_EMIT outDacChConfigChanged(ch);
    }

    void DevOutSyncConfig::notifyDigChFreqChanged(int ch) {
        Q_EMIT outDigChConfiguredFreqChanged(ch);
        Q_EMIT outDigChConfigChanged(ch);
    }

    void DevOutSyncConfig::outSyncUpdateDevInfo(const DevOutInfo &devinfo) {
        m_ram_mode_configurable = devinfo.outSyncRamModeIsConfigurable();
        m_dacParams.resize(devinfo.outDacChannelsCnt());
        m_digParams.resize(devinfo.outDigChannelsCnt());
    }

    DevOutSyncConfig::DacChParams::DacChParams(const DevOutSyncConfig::DacChParams *ch_par) {
        init(ch_par->enabled, ch_par->preset, ch_par->gen_mode);
    }

    DevOutSyncConfig::DacChParams::DacChParams(bool init_en,
                                               DevOutSyncConfig::DacPreset init_preset,
                                               OutSyncChGenMode gen_mode) {
        init(init_en, init_preset, gen_mode);
    }

    DevOutSyncConfig::DigChParams::DigChParams(const DevOutSyncConfig::DigChParams *ch_par) {
        init(ch_par->enabled, ch_par->preset, ch_par->gen_mode);
    }

    DevOutSyncConfig::DigChParams::DigChParams(bool init_en,
                                               DevOutSyncConfig::DigPreset init_preset,
                                               DevOutSyncConfig::OutSyncChGenMode gen_mode) {
        init(init_en, init_preset, gen_mode);
    }

    void DevOutSyncConfig::outSyncSetGenLastPointRequred(bool en) {
        if (m_streamOutLastPtReq != en) {
            m_streamOutLastPtReq = en;
            notifyConfigChanged();
        }
    }

    void DevOutSyncConfig::outSyncSetStreamLoadStopAtSignalEnd(bool en) {
        if (m_streamLoadStopAtSigEnd != en) {
            m_streamLoadStopAtSigEnd = en;
            notifyConfigChanged();
        }
    }


}
