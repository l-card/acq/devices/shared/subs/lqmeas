#ifndef DEVOUTSYNCECHOTRACKER_H
#define DEVOUTSYNCECHOTRACKER_H

#include <QtGlobal>
class LQError;

namespace LQMeas {
    /** Класс предназначен для отслеживания количества реально выведенных слов
     * на генерацию по эхо сигналу.
     * При начале генерации вызывается функци outSyncEchoTrackStart(),
     * а по завершению outSyncEchoTrackStop().
     *
     * Класс должен отслеживать кол-во выведенных точек на канал с момента вызова
     *  outSyncEchoTrackStart(). Функция outSyncEchoTrackWait() должна вернуть
     *  управление, когда выведено заданное кол-во точек или истек таймаут.
     *
     * Сейчас данный класс используется для контроля буфера в потоковом режиме
     * при разрешенном эхо. Но также он может использоваться пользователем
     * для отслеживания реально выведенного кол-ва отсчетов */
    class DevOutSyncEchoTracker {
    public:
        virtual ~DevOutSyncEchoTracker();
        virtual void outSyncEchoTrackStart(LQError &err) = 0;
        virtual void outSyncEchoTrackStop(LQError &err) = 0;
        virtual void outSyncEchoTrackWait(quint64 pos, unsigned tout, quint64 &fnd_pos, LQError &err) = 0;
    };
}

#endif // DEVOUTSYNCECHOTRACKER_H
