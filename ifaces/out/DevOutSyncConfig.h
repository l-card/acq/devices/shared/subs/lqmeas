#ifndef LQMEAS_DEVOUTSYNCCONFIG_H
#define LQMEAS_DEVOUTSYNCCONFIG_H

#include "DevOutConfig.h"
#include <QVector>
#include <QSharedPointer>

namespace LQMeas {
    class DevOutInfo;

    /* Конфигурация каналов для синхронного вывода. */
    /** @todo OutRamSyncMode сделать сохранение в зависимости от свойств устройства */
    class DevOutSyncConfig : public DevOutConfig {
        Q_OBJECT
    public:
        /** Режим работы для всех каналов с режимом #OutChModeRam */
        enum class OutSyncRamMode {
            Cycle = 0,   /** Циклический режим (автогенератора) */
            Stream = 1,  /** Потоковый режим */
        };

        /** Режим генерации сигнала для канала */
        enum class OutSyncChGenMode {
            Ram = 0, /** сигнал генерируется по точкам программным образом */
            Hard = 1 /** сигнал генерируется аппаратно по параметрам */
        };

        explicit DevOutSyncConfig(const DevOutInfo &devinfo);
        explicit DevOutSyncConfig(const DevOutSyncConfig &cfg);

        /* Настройки предустановки значения на вывод */
        template < class ValType> struct Preset {
            enum class Type {
                /* Перед запуском синхронного вывода устанавливается
                   первое значение сигнала на выходы */
                FirstPoint = 0,
                /* Перед запуском синхронного вывода асинхронно
                   устанавлиется указанное явно значение */
                SpecValue  = 1,
                /* Не происходит никакого асинхронного вывода на выходы
                   перед стартом. т.е. остается старое значение до
                   запуска синхронной выдачи */
                DontChange = 2
            };

            Type type;
            ValType value; /* значение, выводимое при type == TypeSpecValue */

            Preset() : type {Type::FirstPoint}, value{0} {
            }
        } ;

        struct DacPreset : public Preset<double> {
        };

        struct DigPreset : public Preset<unsigned> {
        };

        /* признак, что генерация запускается от внешнего источника */
        virtual bool outSyncExternalStart() const {return false;}

        /* Установленная в данный момент частота вывода в Гц (на каждый канал) */
        virtual double outSyncGenFreq() const = 0;

        virtual int outSyncDacSampleSize() const = 0;
        virtual int outSyncDigSampleSize() const = 0;

        /* Разрешение отслеживания эхо сигнала от синхронного вывода */
        virtual bool outSyncEchoEnabled() const {return false;}

        int outSyncPreloadSingalTimeMs() const {return m_preloadSigTimeMs;}
        void outSyncSetPreloadSignalTimeMs(int ms);

        int outSyncSendSignalBockTimeMs() const {return m_sendSigBlockTimeMs;}
        void outSyncSetSendSignalBockTimeMs(int ms);

        /* в потоковом режиме данный параметр говорит, что необходимо в конце вернуть
         * выходы в начальное состояние сгенерировав последнюю точку.
         * @note - сейчас генерируется всегда нулевое значение для всех каналов.
         *         также при включении эхо-режима при данной настройке идет
         *         ожидание эхо-ответа от этой точки и только после этого завершается
         *         сбор. Без эхо-ответа момент останова определить нельзя */
        bool outSyncGenLastPointRequired() const {return m_streamOutLastPtReq;}
        void outSyncSetGenLastPointRequred(bool en);

        /* Флаг определяет поведение вывода в потоке конечного во времени сигнала.
           Если false - то данные генерятся постоянно и после последней выведенной
                        точки загружаются одинаковые значения.
           Если true  - то после того, как будет выведен самый длинный сигнал,
                        загрузка данных будет прекращена, что в частности приведет
                        опустошению очереди. Однако это позволяет быстро остановить
                        вывод или в дальнейшем добавить мгновенно новый вывод в поток
                        (при реализации SignalsUpdate для потокового режима)
         */
        bool outSyncStreamLoadStopAtSignalEnd() const {return m_streamLoadStopAtSigEnd;}
        void outSyncSetStreamLoadStopAtSignalEnd(bool en);

        /* Разрешен ли указанный канал ЦАП для синхронного вывода */
        bool outSyncDacChEnabled(int ch) const {return outDacChSyncModeEnabled(ch) && m_dacParams.at(ch).enabled;}
        /* Разрешен ли указанный канал цифровых линий */
        bool outSyncDigChEnabled(int ch) const {return outDigChSyncModeEnabled(ch) && m_digParams.at(ch).enabled;}

        /* Разрешение заданного канала ЦАП для генерации синхронного сигнала */
        virtual void outSyncDacSetChEnabled(int ch, bool en);
        /* Разрешение заданного канала цифровых линий для генерации синхронного сигнала */
        virtual void outSyncDigSetChEnabled(int ch, bool en);

        /* текущие настройки предустановленного значения канала ЦАП */
        DacPreset outSyncDacPreset(int ch) const {return m_dacParams.at(ch).preset;}
        /* установить настройки предустановленного значения канала ЦАП */
        virtual void outSyncDacSetPreset(int ch, const DacPreset &val);

        /* текущие настройки предустановленного значения цифровой линии*/
        DigPreset outSyncDigPreset(int ch) const {return m_digParams.at(ch).preset;}
        /* установить настройки предустановленного значения цифровой линии*/
        virtual void outSyncDigSetPreset(int ch, const DigPreset &val);


        /* Режим генерации указанного канала ЦАП */
        virtual OutSyncChGenMode outSyncDacChGenMode(int ch) const {return m_dacParams.at(ch).gen_mode;}
        /* Режим генерации указанной цифровой линии */
        virtual OutSyncChGenMode outSyncDigChGenMode(int ch) const {return m_digParams.at(ch).gen_mode;}

        virtual void outSyncDacSetChGenMode(int ch, OutSyncChGenMode mode);
        virtual void outSyncDigSetChGenMode(int ch, OutSyncChGenMode mode);


        /* Текущий режим работы для каналов, генерируемых по точкам из RAM */
        OutSyncRamMode outSyncRamMode() const {return m_outRamSyncMode;}

        virtual void outSyncSetRamMode(OutSyncRamMode mode);



        /* Количество каналов ЦАП в данном экземпляре модуля */
        int outSyncDacChannelsCnt() const {return m_dacParams.size();}
        /* Количество цифровых линий в данном экземпляре модуля */
        int outSyncDigChannelsCnt() const {return m_digParams.size();}
    Q_SIGNALS:
        void outDacChConfiguredFreqChanged(int ch);
        void outDigChConfiguredFreqChanged(int ch);
    protected:
        void notifyDacChFreqChanged(int ch = -1);
        void notifyDigChFreqChanged(int ch = -1);

        /* Загрузка установленных сигналов и признака разрешения для
            всех каналов */
        void protLoad(const QJsonObject &outObj) override;
        /* Сохранение установленных сигналов и признака разрешения для
            всех каналов */
        void protSave(QJsonObject &outObj) const override;

        void protUpdateInfo(const DeviceTypeInfo *info) override;


    private:
        void outSyncUpdateDevInfo(const DevOutInfo &devinfo);


        template<class PresetType> struct ChParams {
        public:
            bool enabled;
            PresetType preset;
            OutSyncChGenMode gen_mode;
        protected:
            void init(bool init_en, PresetType init_preset, OutSyncChGenMode init_gen_mode) {
                enabled = init_en;
                preset = init_preset;
                gen_mode = init_gen_mode;
            }
        };


        struct DacChParams : public ChParams< DacPreset> {
        public:
            DacChParams(const DacChParams *ch_par);
            DacChParams(bool init_en = false,
                        DacPreset init_preset = DacPreset(),
                        OutSyncChGenMode gen_mode = OutSyncChGenMode::Ram);
        };

        struct DigChParams : public ChParams< DigPreset> {
        public:
            DigChParams(const DigChParams *ch_par);
            DigChParams(bool init_en = false,
                        DigPreset init_preset = DigPreset(),
                        OutSyncChGenMode gen_mode = OutSyncChGenMode::Ram);
        };


        static const int default_preload_time_ms = 2000;
        static const int default_block_time_ms = 200;

        OutSyncRamMode m_outRamSyncMode {OutSyncRamMode::Cycle};
        bool m_ram_mode_configurable {false};
        QVector<DacChParams> m_dacParams;
        QVector<DigChParams> m_digParams;
        const DevOutInfo *m_out_info {nullptr};
        int m_preloadSigTimeMs {default_preload_time_ms};
        int m_sendSigBlockTimeMs {default_block_time_ms};
        bool m_streamOutLastPtReq {false};
        bool m_streamLoadStopAtSigEnd {false};

        friend class DeviceConfig;
    };
}

#endif // LQMEAS_DEVOUTSYNCCONFIG_H
