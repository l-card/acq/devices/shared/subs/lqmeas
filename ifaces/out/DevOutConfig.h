#ifndef LQMEAS_DEVOUTCONFIG_H
#define LQMEAS_DEVOUTCONFIG_H


#include "lqmeas/devs/DeviceConfigIface.h"
#include <QVector>

namespace LQMeas {
    class Unit;
    class DevOutInfo;


    class DevOutConfig : public DeviceConfigIface {
        Q_OBJECT
    public:
        explicit DevOutConfig(const DevOutInfo &devinfo);
        explicit DevOutConfig(const DevOutConfig &cfg);
        const DevOutInfo &outInfo() const {return *m_out_info;}

        virtual ~DevOutConfig() override;

        virtual int outDacChRangeNum(int ch) const = 0;

        virtual double outDacChRangeMaxVal(int ch) const;
        virtual double outDacChRangeMinVal(int ch) const;

        virtual const Unit &outDacChValUnit(int ch) const;

        virtual bool outDacChAvailable(int ch) const;
        virtual bool outDigChAvailable(int ch) const;


        virtual bool outDacChSyncModeEnabled(int ch) const { Q_UNUSED(ch) return false;}
        virtual bool outDigChSyncModeEnabled(int ch) const { Q_UNUSED(ch) return false;}

    Q_SIGNALS:
        void outDacChConfigChanged(int ch);
        void outDigChConfigChanged(int ch);
        void outDacChRangeChanged(int ch, double minVal, double maxVal);
        void outDacChSyncEnableStateChanged(int ch, bool en);
        void outDigChSyncEnableStateChanged(int ch, bool en);
    protected:
        void protUpdateInfo(const DeviceTypeInfo *info) override;

        QString cfgIfaceName() const override {return  QStringLiteral("Out");}

        void notifyDacChRangeChanged(int ch);
        void notifyDacChConfigChanged(int ch);
        void notifyDigChConfigChanged(int ch);
        void notifyDacChSyncEnableStateChanged(int ch);
        void notifyDigChSyncEnableStateChanged(int ch);
    private:
        const DevOutInfo *m_out_info;

        friend class DeviceConfig;
    };
}

#endif // LQMEAS_DEVOUTCONFIG_H
