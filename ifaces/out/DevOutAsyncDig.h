#ifndef LQMEAS_DEVOUTASYNCDIG_H
#define LQMEAS_DEVOUTASYNCDIG_H

class LQError;

namespace LQMeas {

    /* Интерфейс для асинхронного вывода на цифровые линии */
    class DevOutAsyncDig {
    public:
        virtual ~DevOutAsyncDig();
        void outAsyncDigAll(unsigned val, unsigned mask, LQError &err);
        void outAsyncDigAll(unsigned val, LQError &err);
        void outAsyncDigCh(int ch, bool val, LQError &err);
    protected:
        virtual void protOutAsyncDig(unsigned val, unsigned mask, LQError &err) = 0;
    };
}

#endif // LQMEAS_DEVOUTASYNCDIG_H
