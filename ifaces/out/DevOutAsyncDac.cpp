#include "DevOutAsyncDac.h"
#include "lqmeas/devs/Device.h"
#include "SignalConverter/DevOutDacConverter.h"

namespace LQMeas {
    DevOutAsyncDac::~DevOutAsyncDac() {

    }

    void DevOutAsyncDac::outAsyncDac(int ch, double val, LQError &err) {
        if (m_dev->devOutDacConverter())
            val = m_dev->devOutDacConverter()->outDacConvertValue(ch, val);

        return protOutAsyncDac(ch, val, err);
    }

    DevOutAsyncDac::DevOutAsyncDac(Device *dev) : m_dev{dev} {

    }
}
