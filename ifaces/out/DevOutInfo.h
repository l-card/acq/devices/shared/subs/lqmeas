#ifndef LQMEAS_DEVOUTINFO_H
#define LQMEAS_DEVOUTINFO_H

#include <QtGlobal>

namespace LQMeas {
    class Unit;

    /* Данный класс содержит информацию о возможностях модуля вывода */
    class DevOutInfo {
    public:
        virtual ~DevOutInfo();
        /* поддерживается ли синхронный вывод на ЦАП */
        virtual bool outDacSyncSupport() const = 0;
        /* поддерживается ли синхронный вывод на цифровые линии */
        virtual bool outDigSyncSupport() const = 0;

        /* поддерживается ли асинхронный вывод на ЦАП */
        virtual bool outDacAsyncSupport() const = 0;
        /* поддерживается ли асинхронный вывод на цифровые линии */
        virtual bool outDigAsyncSupport() const = 0;
        /* признак, можно ли настраивать синхронный/асинхронный вывод на ЦАП
           индивидуально по каналам (true) или это общая настройка не весь
           вывод (false) */
        virtual bool outDacSyncModeCfgPerCh() const = 0;

        /* можно ли настраивать режим генерации сигнала из RAM (потоковый/циклический) */
        virtual bool outSyncRamModeIsConfigurable() const = 0;
        /* поддерживается ли настройка режима генерации ЦАП на канал (RAM/HARD) */
        virtual bool outSyncDacChGenModeIsConfigurable() const = 0;
        /* поддерживается ли настройка режима генерации цифр. линий на канал (RAM/HARD) */
        virtual bool outSyncDigChGenModeIsConfigurable() const = 0;

        /* Максимальное значение частоты генерации в Гц для данного модуля */
        virtual double outSyncGenFreqMax() const = 0;
        /* Значение по умолчанию частоты генерцаии в Гц для данного модуля */
        virtual double outSyncGenFreqDefault() const {return outSyncGenFreqMax();}
        /* поддерживается ли установка предварительных данных */
        virtual bool outSyncPresetSupport() const = 0;

        /* Количество каналов ЦАП в данном экземпляре модуля */
        virtual int outDacChannelsCnt() const = 0;
        /* Количество цифровых линий в данном экземпляре модуля */
        virtual int outDigChannelsCnt() const = 0;
        /* Присутствует ли указанный канал ЦАП для выбранного модуля */
        virtual bool outDacChAvailable(int ch) const {Q_UNUSED(ch); return true;}
        /* Присутствует ли указанный канал цифровых линий для выбранного модуля */
        virtual bool outDigChAvailable(int ch) const { Q_UNUSED(ch); return true;}

        /* Единицы измерения величин, выдаваемых на данном канале ЦАП */
        virtual const Unit &outDacChUnit(int ch) const;
        /* Количество диапазонов для данного канала */
        virtual int outDacChRangesCnt(int ch) const = 0;
        /* Максимальное пиковое значение для данного канала ЦАП для указанного диапазона */
        virtual double outDacChRangeMaxVal(int ch, int range) const = 0;
        /* Минимальное пиковое значение для данного канала ЦАП для указанного диапазона  */
        virtual double outDacChRangeMinVal(int ch, int range) const = 0;
    };
}

#endif // LQMEAS_DEVOUTINFO_H
