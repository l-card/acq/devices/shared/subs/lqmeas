#ifndef LQMEAS_DEVOUTSYNC_H
#define LQMEAS_DEVOUTSYNC_H

#include "DevOutSyncConfig.h"
#include "OutRamSignalGenerator.h"

class LQError;

namespace LQMeas {
    class Device;
    class DeviceInfo;
    class DevOutSyncModeImpl;
    class DevOutSyncStatusTracker;

    /***********************************************************************//**
        @brief Интерфейс для синхронного вывода аналоговых и/или цифровых сигналов.

        Интерфейс служит для реализации генерации синхронных сигналов.
        Информация о устройстве, поддерживающем данный интерфейс, должна также
        поддержать интерфейс DevOutInfo, который содержит информацию о поддерживаемых
        возможностях вывода устройства.

        Изначальная настройка режимов выполняется через конфигурацию
        устройства, которая должна наследоваться от DevOutSyncConfig.

        Вывыодимый сигнал настраивается через outSyncDacSetGenSignal()/outSyncDigSetGenSignal()
        до запуска генерации (может быть до или после конфигурации параметров
        самого устройства).

        Для вывода сигнала необходимо как установить действительный сигнал
        для канала, так и резрешить синхронный вывод в конфигурации.

        Помимо сигнала можно настроить предустановленное значение, которое будет
        устанавливаться асинхронно перед запуском синхронного вывода, т.е.
        это значение будет установленно до момента запуска синхронизации и
        с него начнется вывод.

        Этот интерфейс может служить как для генерации сигналов в циклическом, так
        и потоковом режиме, а также для генерации сигналов аппаратными средствами,
        без рассчета.

        Типичный вариант использования:
          - настройка модуля (в произвольном порядке) через DevOutSyncConfig:
               - установка предустановленных значений с помощью outSyncDacSetPreset()/outSyncDigSetPreset()
               - разрешение интересующих каналов с помощью outSyncDacSetChEnabled()/outSyncDigSetChEnabled()
          - установка сигналов для нужных каналов с помощью outSyncDacSetGenSignal()/outSyncDigSetGenSignal()
          - вызов outSyncGenPrepare() для предварительной предзагрузки сигналов (можно пропустить)
          - вызов outSyncGenStart() по которому начнется генерация

          для останова генерации
          - сперва вызвать outSyncGenStopRequest() по которому будет подан запрос на завершение
          - затем вызвать outSyncGenStopWait(), который вернет управление, когда генерация уже завершена

          для смены сигналов на лету (сейчас только в циклическом режиме):
          - установить новые сигналы с помощью outSyncDacSetGenSignal()/outSyncDigSetGenSignal()
          - вызвать outSyncGenUpdate(), по которому произойдет замена сигналов
            на установленные (только для тех каналов, для которых изменились сигналы)

         Для отслеживаения ошибок при запущенном выводе можно подключиться
         к сигналам объекта слежения, получаемого outSyncStatusTracker().
         При ошибках или некоторых событиях вывода этот объект генерирует
         соответствующие сигналы, если отслеживание этих событий поддерживается
         устройством в используемом режиме.


         При наследовании от данного класса необходимо после создания
         объекта добавить классы, реализующие поддержку определенного режима
         с помощью outSyncAddMode(). Есть стандартные реализации потокового
         и циклического вывода, которые требуют поддержки от устройства
         соответствующих стандартных приватных интерфейсов
      ************************************************************************/
    class DevOutSync {
    public:

        /* Признак, запущена ли сейчас генерация данных */
        bool outSyncGenRunning() const;

        /* Данная функция должна вызываться перед началом генерации. По ней
           могут быть выставлены начальные значения, предзагружены отсчеты
           сигналов и т.п. */
        void outSyncGenPrepare(LQError &err);
        /* Начало генерации сигнала в синхронном режиме */
        void outSyncGenStart(LQError &err);

        void outSyncGenStartRequest(LQError &err);
        bool outSyncGenStartWaitDone(unsigned tout, LQError &err);

        void outSyncGenStopRequest(unsigned tout, LQError &err);
        void outSyncGenStopRequest(LQError &err);
        /* Останов генерации сигнала в режиме автогенератора. */
        void outSyncGenStop(LQError &err);
        /* Обновление сигналов при включенной генерации */
        void outSyncGenUpdate(LQError &err);
        void outSyncGenUpdateRequest(LQError &err);
        bool outSyncGenUpdateWaitDone(unsigned tout, LQError &err);

        /* Задание сигнала для указанного выхода ЦАП */
        void outSyncDacSetGenSignal(int ch, const SignalConfig &signal, LQError &err);
        void outSyncDacClearGenSignal(int ch);
        /*Получить установленный сигнал для указанного выхода ЦАП */
        const SignalConfig &outSyncDacGenSignal(int ch) const {
            return *m_dac_signals_cfg.at(ch);
        }
        bool outSyncDacGenSignalIsValid(int ch) const {
            return !m_dac_signals_cfg.at(ch).isNull();
        }
        /* Задание сигнала для указанного цифрового выхода */
        void outSyncDigSetGenSignal(int ch, const SignalConfig &signal, LQError &err);
        void outSyncDigClearGenSignal(int ch);
        /* Получить установленный сигнал для указанного цифрового выхода */
        const SignalConfig &outSyncDigGenSignal(int ch) const {
            return *m_dig_signals_cfg.at(ch);
        }
        bool outSyncDigGenSignalIsValid(int ch) const {
            return !m_dig_signals_cfg.at(ch).isNull();
        }


        DevOutSyncStatusTracker *outSyncStatusTracker() const {return m_statusTracker;}

        const DevOutSyncConfig *outSyncConfig() const {return m_cfg;}

        virtual ~DevOutSync();

        bool outSyncHasEnabledChannels();
    protected:
        virtual void protOutSyncHardOnlyGenStartRequest(LQError &err);
        virtual bool protOutSyncHardOnlyGenStartWaitDone(unsigned tout, LQError &err);
        virtual void protOutSyncHardGenUpdate(LQError &err);

        virtual void protOutSyncHardOnlyGenStop(unsigned tout, LQError &err);

        QSharedPointer<SignalConfig> outSyncDacGenSignalMutable(int ch);

        DevOutSync(Device *dev);

        /* Добавить реализацию синхронного вывода, поддерживаемую модулем.
           Объект удаляется при удалении DevOutSync, поэтому можно не сохранять
           в классе реализации для удаления*/
        void outSyncAddMode(DevOutSyncModeImpl *impl);

        /* Проверка, что для указанного выхода ЦАП будет выполняться расчет
            точек для загрузки в модуль (канал разрешен, есть действительный
            сигнал и режим OutSyncChGenModeRam) */
        bool outSyncDacValidRamSignal(int ch) const;
        /* Проверка, что для указанного цифрового выхода будет выполняться расчет
            точек для загрузки в модуль (канал разрешен, есть действительный
            сигнал и режим OutSyncChGenModeRam) */
        bool outSyncDigValidRamSignal(int ch) const;
    private:
        static const unsigned default_start_tout = 5000;
        static const unsigned default_update_tout = 25000;

        void outSyncUpdateConfig(const DevOutSyncConfig *cfg);
        void outSyncUpdateInfo(const DeviceInfo &info);
        void internalSyncGenPrepare(LQError &err);
        void internalSyncSigUpdateFinish();

        /* асинхронная предустановка данных перед синхронным выводом */
        void presetVals(const QSharedPointer<OutRamSignalGenerator> &generator, LQError &err);

        const DevOutSyncConfig *m_cfg;
        QVector<QSharedPointer <SignalConfig> > m_dac_signals_cfg;
        QVector<QSharedPointer <SignalConfig> > m_dig_signals_cfg;

        QList<DevOutSyncModeImpl*> m_modesImpl;
        DevOutSyncModeImpl *m_curMode {nullptr};
        DevOutSyncStatusTracker *m_statusTracker;
        Device *m_dev;

        enum class GenState {
            Stop,
            Prepared,
            StartRequested,
            Running,
            UpdateRequested,
            StopReq
        } m_gen_state {GenState::Stop};

        friend class Device;
    };
}

#endif // LQMEAS_IDEVOUTSYNC_H
