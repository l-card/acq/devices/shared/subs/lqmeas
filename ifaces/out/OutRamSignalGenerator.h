#ifndef LQMEAS_OUTRAMSIGNALGENERATOR_H
#define LQMEAS_OUTRAMSIGNALGENERATOR_H

#include "Signals/SignalConfig.h"
#include <QVector>
#include <QList>
#include <QSharedPointer>

class LQError;

namespace LQMeas {
    class AnalogSignalGenerator;
    class DiscreteSignalGenerator;
    class SignalConverter;

    /* Класс предназначен для генерации точек заданного набора сигналов для последующей
       отправки в устройство.
       Перед генерацией необходимо вызвать genSignalsInit(), а затем вызывать
       genNextData() для получения новых точек для отправки в устройство */
    class OutRamSignalGenerator {
    public:
        /* сигналы создаются при создании. dac_chs и dig_chs задают номера каналов, которым
           соответствуют указанные сигналы */
        OutRamSignalGenerator(const QList<QSharedPointer<SignalConfig> > &dac_signals, const QList<int> &dac_chs,
                              const QList<const SignalConverter *> &converters,
                              const QList<QSharedPointer<SignalConfig> > &dig_signals, const QList<int> &dig_chs);
        ~OutRamSignalGenerator();


        int dacSignalsCount() const {return m_dac_signals_cfg.size();}
        int digSignalsCount() const {return m_dig_signals_cfg.size();}

        int dacSignalChannel(int sig_num) const {return m_dac_ch_nums.at(sig_num);}
        int digSignalChannel(int sig_num) const {return m_dig_ch_nums.at(sig_num);}

        const QList<QSharedPointer <SignalConfig> > &dacSignalsCfg() const {return m_dac_signals_cfg;}
        const QList<QSharedPointer <SignalConfig> > &digSignalsCfg() const {return m_dig_signals_cfg;}
        const QList<QSharedPointer <SignalConfig> > &allSignalsCfg() const {return m_all_signals_cfg;}


        /* Инициализация генерации сигналов. Переводит все сигналы в состояние
           начала генерации, т.е. чтобы genNextData() вернул первые точки сигналов */
        void genSignalsInit();


        /* генерация следующих points точек для всех сигналов выходных
           каналов. Отсчеты ЦАП чередуются для каждого канала.
           размер dac_data будет равен dacSignalsCount() * point.
           Цифровые линии - в виде битовой маски по номеру канала (не меняется
           от количества разрешенных каналов) - поддерживается только 32 выхода
        */
        void genNextData(int points, QVector<double> &dac_data,
                         QVector<unsigned> &dig_data, LQError &err);

        /* количество оставшихся "полезных" точек для генерации.
         * если < 0 => точек бесконечное множество */
        qint64 remainingSigPoint() const;
    private:
        const QList<AnalogSignalGenerator *> &dacGenerators() const {return m_dac_generators;}
        const QList<DiscreteSignalGenerator *> &digGenerators() const {return m_dig_generators;}

        QList<QSharedPointer <SignalConfig> > m_dac_signals_cfg;
        QList<QSharedPointer <SignalConfig> > m_dig_signals_cfg;
        QList<QSharedPointer <SignalConfig> > m_all_signals_cfg;
        QList<SignalConverter *> m_converters;
        QList<AnalogSignalGenerator *> m_dac_generators;
        QList<DiscreteSignalGenerator *> m_dig_generators;
        QList<int> m_dig_ch_nums;
        QList<int> m_dac_ch_nums;

        qint64 m_cur_point;
        qint64 m_total_points;
    };
}

#endif // LQMEAS_OUTRAMSIGNALGENERATOR_H
