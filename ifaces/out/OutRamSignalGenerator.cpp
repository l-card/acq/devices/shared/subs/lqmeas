#include "OutRamSignalGenerator.h"
#include "Signals/Analog/AnalogSignalGenerator.h"
#include "Signals/Discrete/DiscreteSignalGenerator.h"
#include "Signals/SignalType.h"
#include "SignalConverter/SignalConverter.h"

namespace LQMeas {
    OutRamSignalGenerator::OutRamSignalGenerator(
            const QList<QSharedPointer<SignalConfig> > &dac_signals,
            const QList<int> &dac_chs,
            const QList<const SignalConverter *> &converters,
            const QList<QSharedPointer<SignalConfig> > &dig_signals,
            const QList<int> &dig_chs) :
        m_cur_point{0}, m_total_points{0} {

        m_dac_signals_cfg = dac_signals;
        m_dig_signals_cfg = dig_signals;
        m_dig_ch_nums = dig_chs;
        m_dac_ch_nums = dac_chs;

        for (const auto &dac_sig_cfg : dac_signals) {
            m_all_signals_cfg.append(dac_sig_cfg);
        }

        for (const auto &dig_sig_cfg : dig_signals) {
            m_all_signals_cfg.append(dig_sig_cfg);
        }

        for (const SignalConverter *conv : converters) {
            m_converters.append(conv ? conv->clone() : nullptr);
        }
    }

    OutRamSignalGenerator::~OutRamSignalGenerator() {
        qDeleteAll(m_dac_generators);
        qDeleteAll(m_dig_generators);
        qDeleteAll(m_converters);
    }

    void OutRamSignalGenerator::genSignalsInit() {
        if (m_dac_generators.isEmpty() && m_dig_generators.isEmpty()) {
            for (const auto &dac_sig_cfg : dacSignalsCfg()) {
                m_dac_generators.append(static_cast<AnalogSignalGenerator*>(
                                        dac_sig_cfg->createGenerator()));
            }

            for (const auto &dig_sig_cfg : digSignalsCfg()) {
                m_dig_generators.append(static_cast<DiscreteSignalGenerator*>(
                                       dig_sig_cfg->createGenerator()));
            }
        }


        QList<SignalGenerator *> allGens;
        for (AnalogSignalGenerator *sigGen : dacGenerators()) {
            sigGen->signalGenInit();
            allGens.append(sigGen);
        }
        for (DiscreteSignalGenerator *sigGen : digGenerators()) {
            sigGen->signalGenInit();
            allGens.append(sigGen);
        }

        qint64 total_points = 0;
        for (SignalGenerator *sigGen : allGens) {
            if (total_points >= 0) {
                qint64 sig_points = sigGen->totalGenPoints();
                if ((sig_points < 0) || (sig_points > total_points))
                    total_points = sig_points;
            }
        }
        m_total_points = total_points;
        m_cur_point = 0;
    }

    void OutRamSignalGenerator::genNextData(int points, QVector<double> &dac_data,
                                            QVector<unsigned> &dig_data, LQError &err) {
        Q_UNUSED(err)

        int dac_ch_cnt {dacSignalsCount()};
        int dig_ch_cnt {digSignalsCount()};

        if (dac_ch_cnt > 0) {
            dac_data.resize(points*dac_ch_cnt);
            for (int ch_idx {0}; ch_idx < dac_ch_cnt; ++ch_idx) {
                AnalogSignalGenerator *sigGen = m_dac_generators.at(ch_idx);
                SignalConverter *sigConv = m_converters.at(ch_idx);
                for (int pt = 0; pt < points; ++pt) {
                    double val = sigGen->signalGenNextPoint();
                    if (sigConv)
                        val = sigConv->covert(val);
                    dac_data[pt * dac_ch_cnt + ch_idx] = val;
                }
            }
        } else {
            dac_data.clear();
        }

        if (dig_ch_cnt > 0) {
            dig_data.resize(points);
            memset(dig_data.data(), 0, static_cast<size_t>(dig_data.size())*sizeof(dig_data[0]));
            for (int ch_idx {0}; ch_idx < dig_ch_cnt; ++ch_idx) {
                DiscreteSignalGenerator *sigGen = m_dig_generators.at(ch_idx);

                for (int pt = 0; pt < points; ++pt) {
                    if (sigGen->signalGenNextPoint()) {
                        dig_data[pt] |= (1 << m_dig_ch_nums[ch_idx]);
                    }
                }
            }
        } else {
            dig_data.clear();
        }
        m_cur_point += points;
    }

    qint64 OutRamSignalGenerator::remainingSigPoint() const {
        return m_total_points < 0 ? -1 :
               m_total_points < m_cur_point ? 0 : m_total_points - m_cur_point;
    }

}
