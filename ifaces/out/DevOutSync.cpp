#include "DevOutSync.h"
#include "DevOutInfo.h"
#include "lqmeas/StdErrors.h"
#include "lqmeas/devs/Device.h"
#include "lqmeas/devs/DeviceTypeInfo.h"
#include "lqmeas/devs/DeviceConfig.h"
#include "DevOutSyncStatusTracker.h"
#include "DevOutSyncEchoTracker.h"
#include "SyncModes/DevOutSyncModeImpl.h"
#include "DevOutAsyncDac.h"
#include "DevOutAsyncDig.h"
#include "SignalConverter/DevOutDacConverter.h"
#include "Signals/Analog/Const/AnalogSignalConstConfig.h"
#include "Signals/Discrete/Const/DiscreteSignalConstConfig.h"
#include "lqmeas/devs/DeviceInfo.h"


namespace LQMeas {
    void DevOutSync::outSyncDacSetGenSignal(int ch, const SignalConfig &signal, LQError &err) {
        Q_UNUSED(err)
        m_dac_signals_cfg[ch] = QSharedPointer<SignalConfig>(signal.clone());
    }

    void DevOutSync::outSyncDigSetGenSignal(int ch, const SignalConfig &signal, LQError &err) {
        Q_UNUSED(err)
        m_dig_signals_cfg[ch] = QSharedPointer<SignalConfig>(signal.clone());
    }

    void DevOutSync::outSyncDacClearGenSignal(int ch) {
        m_dac_signals_cfg[ch].clear();
    }

    void DevOutSync::outSyncDigClearGenSignal(int ch) {
        m_dig_signals_cfg[ch].clear();
    }

    DevOutSync::~DevOutSync() {
        qDeleteAll(m_modesImpl);
        delete m_statusTracker;
    }

    bool DevOutSync::outSyncHasEnabledChannels() {
        bool ret = false;
        if (m_cfg) {
            for (int ch_idx = 0; !ret && (ch_idx < m_cfg->outSyncDacChannelsCnt()); ++ch_idx) {
                if (m_cfg->outSyncDacChEnabled(ch_idx) && outSyncDacGenSignalIsValid(ch_idx)) {
                    ret = true;
                }
            }
            for (int ch_idx = 0; !ret && (ch_idx < m_cfg->outSyncDigChannelsCnt()); ++ch_idx) {
                if (m_cfg->outSyncDigChEnabled(ch_idx) && outSyncDigGenSignalIsValid(ch_idx)) {
                    ret = true;
                    break;
                }
            }
        }
        return ret;
    }

    void DevOutSync::protOutSyncHardOnlyGenStop(unsigned tout, LQError &err) {
        Q_UNUSED(tout)
        err = StdErrors::NoChannelEnabled();
    }

    QSharedPointer<SignalConfig> DevOutSync::outSyncDacGenSignalMutable(int ch)     {
        return m_dac_signals_cfg.at(ch);
    }

    void DevOutSync::protOutSyncHardOnlyGenStartRequest(LQError &err) {
        err = StdErrors::NoChannelEnabled();
    }

    bool DevOutSync::protOutSyncHardOnlyGenStartWaitDone(unsigned tout, LQError &err) {
        err = StdErrors::NoChannelEnabled();
        return false;
    }

    void DevOutSync::protOutSyncHardGenUpdate(LQError &err) {
        Q_UNUSED(err)
    }

    bool DevOutSync::outSyncGenRunning() const {
        return (m_gen_state != GenState::Stop) && (m_gen_state != GenState::Prepared);
    }



    void DevOutSync::outSyncGenPrepare(LQError &err) {
        if (outSyncGenRunning()) {
            err = StdErrors::OutGenRunning();
        } else {
            m_curMode = nullptr;
            internalSyncGenPrepare(err);
        }

        if (err.isSuccess()) {
            protOutSyncHardGenUpdate(err);
        }

        if (err.isSuccess())
            m_gen_state = GenState::Prepared;
    }

    void DevOutSync::outSyncGenStart(LQError &err) {
        outSyncGenStartRequest(err);
        if (err.isSuccess()) {
            bool ok {outSyncGenStartWaitDone(default_start_tout, err)};
            if (!ok) {
                LQError stop_err;
                outSyncGenStop(stop_err);
                if (err.isSuccess())
                    err = StdErrors::OutGenStartDoneTimeout();
            }
        }
    }

    void DevOutSync::outSyncGenStartRequest(LQError &err)     {
        /* если не выполнен prepare, то выполняем тут */
        if (m_gen_state == GenState::Stop)
            outSyncGenPrepare(err);

        if (err.isSuccess() && (m_gen_state != GenState::Prepared)) {
            if (m_gen_state == GenState::Running) {
                err = StdErrors::OutGenRunning();
            } else {
                err = StdErrors::OutGenState();
            }
        }

        if (err.isSuccess() && m_cfg->outSyncEchoEnabled()) {
            m_dev->devOutSyncEchoTracker()->outSyncEchoTrackStart(err);
        }
        if (err.isSuccess()) {
            if (m_curMode) {
                m_curMode->outSyncModeImplStartRequest(err);
            } else {
                protOutSyncHardOnlyGenStartRequest(err);
            }
        }

        if (err.isSuccess())
            m_gen_state = GenState::StartRequested;
    }

    bool DevOutSync::outSyncGenStartWaitDone(unsigned tout, LQError &err) {
        bool ok {false};
        if (m_gen_state == GenState::Running) {
            ok = true;
        } else {
            if (m_gen_state != GenState::StartRequested) {
                outSyncGenStartRequest(err);
            }

            if (err.isSuccess()) {
                if (m_gen_state == GenState::StartRequested) {
                    if (m_curMode) {
                        ok = m_curMode->outSyncModeImplStartWaitDone(tout, err);
                    } else {
                        ok = protOutSyncHardOnlyGenStartWaitDone(tout, err);
                    }

                    if (ok) {
                        m_gen_state = GenState::Running;
                    }
                } else {
                    err = StdErrors::OutGenState();
                }
            }
        }
        return ok;
    }

    void DevOutSync::outSyncGenStopRequest(unsigned tout, LQError &err) {
        if ((m_gen_state == GenState::Running) ||
                (m_gen_state == GenState::StartRequested)  ||
                (m_gen_state == GenState::UpdateRequested)) {
            if (m_curMode) {
                m_curMode->outSyncModeImplStopRequest(tout, err);
            } else {
                protOutSyncHardOnlyGenStop(tout, err);
            }

            if (err.isSuccess())
                m_gen_state = GenState::StopReq;
        }
    }

    void DevOutSync::outSyncGenStopRequest(LQError &err) {
        outSyncGenStopRequest(30000, err);
    }

    void DevOutSync::outSyncGenStop(LQError &err) {
        if ((m_gen_state == GenState::Running) ||
                (m_gen_state == GenState::StartRequested)  ||
                (m_gen_state == GenState::UpdateRequested)) {
            outSyncGenStopRequest(err);
        }

        if (err.isSuccess() && (m_gen_state == GenState::StopReq)) {
            if (m_curMode) {
                m_curMode->outSyncModeImplStopWait(err);
                QObject::disconnect(m_curMode, &DevOutSyncModeImpl::underflowOccured,
                                    m_statusTracker, &DevOutSyncStatusTracker::underflowOccured);
                QObject::disconnect(m_curMode, &DevOutSyncModeImpl::errorOccured,
                                    m_statusTracker, &DevOutSyncStatusTracker::setError);
                m_curMode = nullptr;
            }

            if (m_cfg->outSyncEchoEnabled()) {
                m_dev->devOutSyncEchoTracker()->outSyncEchoTrackStop(err);
            }

            m_gen_state = GenState::Stop;
        }
    }

    void DevOutSync::outSyncGenUpdate(LQError &err) {
        outSyncGenUpdateRequest(err);
        if (err.isSuccess()) {
            bool ok = outSyncGenUpdateWaitDone(default_update_tout, err);
            if (!ok && !err.isSuccess()) {
                err = StdErrors::OutGenSigUpdateDoneTimeout();
            }
        }
    }

    void DevOutSync::outSyncGenUpdateRequest(LQError &err) {
        if (m_gen_state == GenState::Running) {
            internalSyncGenPrepare(err);
            if (err.isSuccess()) {
                m_gen_state = GenState::UpdateRequested;
            }
        } else {
            outSyncGenStartRequest(err);
        }
    }

    bool DevOutSync::outSyncGenUpdateWaitDone(unsigned tout, LQError &err) {
        bool ok = false;
        if (m_gen_state == GenState::UpdateRequested) {
            if (m_curMode) {
                ok = m_curMode->outSyncModeImplSigUpdateWaitDone(tout, err);
            }

            if (ok) {
                m_gen_state = GenState::Running;
                protOutSyncHardGenUpdate(err);
                ok = err.isSuccess();
            }
        } else if (m_gen_state == GenState::StartRequested) {
            ok = outSyncGenStartWaitDone(tout, err);
        }
        return ok;
    }

    DevOutSync::DevOutSync(Device *dev) :
        m_cfg{dev->config().outSyncConfig()},
        m_statusTracker{new DevOutSyncStatusTracker{nullptr, dev}},
        m_dev{dev} {

    }

    void DevOutSync::presetVals(const QSharedPointer<OutRamSignalGenerator> &generator, LQError &err) {
        if (m_dev->devTypeInfo().out()->outSyncPresetSupport()) {
            DevOutAsyncDac *async_dac = m_dev->devOutAsyncDac();
            DevOutAsyncDig *async_dig = m_dev->devOutAsyncDig();

            QVector<double> dac_data;
            QVector<unsigned> dig_data;

            /* генерируем по одной точке сигнала для асинхронного вывода
               начальных значений */
            generator->genSignalsInit();
            generator->genNextData(1, dac_data, dig_data, err);


            if (async_dac) {
                for (int sig_idx =0; (sig_idx < dac_data.size()) && err.isSuccess(); ++sig_idx) {
                    int ch_num = generator->dacSignalChannel(sig_idx);
                    DevOutSyncConfig::DacPreset preset = m_cfg->outSyncDacPreset(ch_num);
                    double point = 0;
                    bool preset_en = false;

                    switch (preset.type) {
                        case DevOutSyncConfig::DacPreset::Type::FirstPoint:
                            preset_en = true;
                            point = dac_data[sig_idx];
                            break;
                        case DevOutSyncConfig::DacPreset::Type::SpecValue:
                            preset_en = true;
                            point = preset.value;
                            break;
                        default:
                            preset_en = false;
                            break;
                    }
                    if (preset_en) {
                       async_dac->outAsyncDac(ch_num, point, err);
                    }
                }
            }

            if (async_dig) {
                unsigned set_mask = 0;
                unsigned set_val = 0;

                for (int sig_idx =0; (sig_idx < generator->digSignalsCount()) && err.isSuccess(); ++sig_idx) {
                    int ch_num = generator->digSignalChannel(sig_idx);
                    DevOutSyncConfig::DigPreset preset = m_cfg->outSyncDigPreset(ch_num);
                    unsigned point = 0;
                    bool preset_en = false;

                    switch (preset.type) {
                        case DevOutSyncConfig::DigPreset::Type::FirstPoint:
                            preset_en = true;
                            point = (dig_data[0] >> ch_num) & 1;
                            break;
                        case DevOutSyncConfig::DigPreset::Type::SpecValue:
                            preset_en = true;
                            point = preset.value;
                            break;
                        default:
                            preset_en = false;
                            break;
                    }

                    if (preset_en) {
                        set_mask |= (1 << ch_num);
                        if (point)
                            set_val  |= (1 << ch_num);
                    }
                }

                if (set_mask != 0) {
                    async_dig->outAsyncDigAll(set_val, ~set_mask, err);
                }
            }
        }
    }


    bool DevOutSync::outSyncDacValidRamSignal(int ch) const {
        return m_cfg->outSyncDacChEnabled(ch) &&
                (m_cfg->outSyncDacChGenMode(ch)== DevOutSyncConfig::OutSyncChGenMode::Ram);
    }

    bool DevOutSync::outSyncDigValidRamSignal(int ch) const {
        return m_cfg->outSyncDigChEnabled(ch) &&
                (m_cfg->outSyncDigChGenMode(ch)== DevOutSyncConfig::OutSyncChGenMode::Ram);
    }


    void DevOutSync::outSyncUpdateConfig(const DevOutSyncConfig *cfg) {
        m_cfg = cfg;
    }

    void DevOutSync::outSyncUpdateInfo(const DeviceInfo &info) {
        const LQMeas::DevOutInfo *outInfo = info.type().out();
        m_dac_signals_cfg.resize(outInfo->outDacChannelsCnt());
        m_dig_signals_cfg.resize(outInfo->outDigChannelsCnt());
    }

    void DevOutSync::internalSyncGenPrepare(LQError &err) {
        /* обработка генерируемых в RAM сигналов */

        /* получаем список каналов */
        QList<QSharedPointer <SignalConfig> > dac_signals;
        QList<QSharedPointer <SignalConfig> > dig_signals;
        QList<const SignalConverter *> dac_convs;
        QList<int> dac_chs;
        QList<int> dig_chs;

        for (int ch =0; ch < m_cfg->outSyncDacChannelsCnt(); ++ch) {
            if (outSyncDacValidRamSignal(ch)) {
                /* если сигнал не установлен, заменяем константой (т.к. последовательность
                 * должна совпадать с последовательностью разрешенных каналов) */
                if (!m_dac_signals_cfg.at(ch))
                    m_dac_signals_cfg[ch] = QSharedPointer <SignalConfig>(new AnalogSignalConstConfig());
                dac_signals.append(m_dac_signals_cfg[ch]);

                const SignalConverter *conv = nullptr;
                if (m_dev->devOutDacConverter() && m_dev->devOutDacConverter()->outDacHasConverter(ch))
                    conv = &m_dev->devOutDacConverter()->outDacConverter(ch);
                dac_convs.append(conv);
                dac_chs.append(ch);
            }
        }
        for (int ch =0; ch < m_cfg->outSyncDigChannelsCnt(); ++ch) {
            if (outSyncDigValidRamSignal(ch)) {
                if (!m_dig_signals_cfg.at(ch))
                    m_dig_signals_cfg[ch] = QSharedPointer <SignalConfig>(new DiscreteSignalConstConfig());
                dig_signals.append(m_dig_signals_cfg[ch]);
                dig_chs.append(ch);
            }
        }


        if (!dac_chs.isEmpty() || !dig_signals.isEmpty()) {
            bool first_set = !m_curMode;
            if (first_set) {
                /* ищем реализацию режима генерации из RAM */
                for (DevOutSyncModeImpl *impl : m_modesImpl) {
                    if (impl->outSyncMode() == m_cfg->outSyncRamMode()) {
                        m_curMode = impl;
                        break;
                    }
                }

                if (!m_curMode) {
                    err = StdErrors::UnsupOutSyncRamMode();
                }
            }

            if (err.isSuccess()) {
                /* создаем генератор для генерации точек резрешенных сигналов */
                QSharedPointer<OutRamSignalGenerator> gen{
                            new OutRamSignalGenerator{dac_signals, dac_chs,
                                                      dac_convs,
                                                      dig_signals, dig_chs}};


                /* назначение сигналов и их корректировака в реализации режимов */
                m_curMode->outSyncModeImplSigSet(gen, err);
                /* устанавливаем асинхронные начальные значения */
                if (err.isSuccess() && first_set)
                    presetVals(gen, err);
                /* предзагрузка сигналов */
                if (err.isSuccess()) {
                    gen->genSignalsInit();

                    if (first_set) {
                        m_curMode->outSyncModeImplStartPrepare(gen, err);

                        QObject::connect(m_curMode, &DevOutSyncModeImpl::underflowOccured,
                                         m_statusTracker, &DevOutSyncStatusTracker::underflowOccured);
                        QObject::connect(m_curMode, &DevOutSyncModeImpl::errorOccured,
                                         m_statusTracker, &DevOutSyncStatusTracker::setError);
                    } else {
                        m_curMode->outSyncModeImplSigUpdateRequest(gen, err);
                    }
                }
            }
        }
    }


    void DevOutSync::outSyncAddMode(DevOutSyncModeImpl *impl) {
        m_modesImpl.append(impl);
    }
}

