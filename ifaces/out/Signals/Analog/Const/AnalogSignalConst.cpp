#include "AnalogSignalConst.h"
#include "AnalogSignalConstConfig.h"
#include "AnalogSignalConstGenerator.h"

namespace LQMeas {
    const AnalogSignalConst &AnalogSignalConst::instance() {
        static const AnalogSignalConst type;
        return type;
    }

    SignalConfig *AnalogSignalConst::createConfig() const {
        return new AnalogSignalConstConfig{};
    }

    AnalogSignalGenerator *AnalogSignalConst::createAnalogGenerator(const SignalConfig &cfg) const {
        return new AnalogSignalConstGenerator{static_cast<const AnalogSignalConstConfig &>(cfg)};
    }
}
