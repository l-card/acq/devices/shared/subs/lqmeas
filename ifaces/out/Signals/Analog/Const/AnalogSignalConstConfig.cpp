#include "AnalogSignalConstConfig.h"
#include "AnalogSignalConst.h"
#include "lqmeas/lqtdefs.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_ampl           {"Amp"};

    AnalogSignalConstConfig::AnalogSignalConstConfig(double amp) :
        SignalConfig{AnalogSignalConst::instance()}, m_amp{amp} {

    }

    AnalogSignalConstConfig::AnalogSignalConstConfig(const AnalogSignalConstConfig &other) :
        SignalConfig{other}, m_amp{other.m_amp} {

    }

    SignalConfig *AnalogSignalConstConfig::clone() const {
        return new AnalogSignalConstConfig{*this};
    }

    void AnalogSignalConstConfig::setAmplitude(double amp) {
        m_amp = amp;
    }

    void AnalogSignalConstConfig::protLoad(const QJsonObject &sigObj) {
        m_amp = sigObj[cfgkey_ampl].toDouble();
    }

    void AnalogSignalConstConfig::protSave(QJsonObject &sigObj) const {
        sigObj[cfgkey_ampl] = m_amp;
    }

    bool AnalogSignalConstConfig::protIsEqual(const SignalConfig &other) const {
        const AnalogSignalConstConfig &otherSig
                = static_cast<const AnalogSignalConstConfig &>(other);
        return LQREAL_IS_EQUAL(m_amp, otherSig.m_amp);
    }
}
