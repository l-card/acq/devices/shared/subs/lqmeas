#ifndef LQMEAS_ANALOGSIGNALCONST_H
#define LQMEAS_ANALOGSIGNALCONST_H

#include "../AnalogSignalType.h"


namespace LQMeas {
    class AnalogSignalConst : public AnalogSignalType {
        Q_OBJECT
    public:
        static const AnalogSignalConst &instance();

        QString typeName() const override {return QStringLiteral("Const");}
        QString displayName() const override {return tr("Constant");}
        SignalConfig *createConfig() const override;
        AnalogSignalGenerator *createAnalogGenerator(const SignalConfig &cfg) const override;
    };
}


#endif // LAMEAS_ANALOGSIGNALCONST_H
