#ifndef LQMEAS_ANALOGSIGNALCONSTCONFIG_H
#define LQMEAS_ANALOGSIGNALCONSTCONFIG_H

#include "../../SignalConfig.h"

namespace LQMeas {
    class AnalogSignalConstConfig : public SignalConfig {
        Q_OBJECT
    public:
        AnalogSignalConstConfig(double amp = 0);
        AnalogSignalConstConfig(const AnalogSignalConstConfig &other);

        SignalConfig *clone() const override;

        double signalFreq() const override {return unspecifiedFreq();}
        int signalMinPoints() const override {return 1;}
        bool signalStrictPoints() const override {return false;}

        void setAmplitude(double amp);
        double amplitude() const {return m_amp;}
    protected:
        void protLoad(const QJsonObject &sigObj) override;
        void protSave(QJsonObject &sigObj) const override;
        bool protIsEqual(const SignalConfig &other) const override;
    private:
        double m_amp;
    };
}

#endif // LQMEAS_ANALOGSIGNALCONSTCONFIG_H
