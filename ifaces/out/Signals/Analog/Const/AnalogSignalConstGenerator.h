#ifndef LQMEAS_ANALOGSIGNALCONSTGENERATOR_H
#define LQMEAS_ANALOGSIGNALCONSTGENERATOR_H

#include "../AnalogSignalGenerator.h"
#include "AnalogSignalConstConfig.h"

namespace LQMeas {
    class AnalogSignalConstGenerator : public AnalogSignalGenerator{
    public:
        AnalogSignalConstGenerator(const AnalogSignalConstConfig &cfg);

        void signalGenInit() override {}
        double signalGenNextPoint() override {return m_amp;}
        qint64 totalGenPoints() const override {return 1;}
    private:
        double m_amp;
    };
}

#endif // LQMEAS_ANALOGSIGNALCONSTGENERATOR_H
