#include "AnalogSignalPulseConfig.h"
#include "AnalogSignalPulse.h"
#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String cfgkey_amp_pulse           {"AmpPulse"};
    static const QLatin1String cfgkey_amp_pause           {"AmpPause"};
    static const QLatin1String cfgkey_freq                {StdConfigKeys::freq()};
    static const QLatin1String cfgkey_duty_perc           {"DutyPerc"};

    AnalogSignalPulseConfig::AnalogSignalPulseConfig(
            double amp_pulse, double amp_pause, double freq, double dutyCycle) :
        SignalConfig{AnalogSignalPulse::instance()},
        m_amp_pulse{amp_pulse},
        m_amp_pause{amp_pause},
        m_freq{freq},
        m_duty{dutyCycle} {

    }

    AnalogSignalPulseConfig::AnalogSignalPulseConfig(const AnalogSignalPulseConfig &other) :
        SignalConfig(other),
        m_amp_pulse{other.m_amp_pulse},
        m_amp_pause{other.m_amp_pause},
        m_freq{other.m_freq},
        m_duty{other.m_duty} {

    }

    SignalConfig *AnalogSignalPulseConfig::clone() const {
        return new AnalogSignalPulseConfig(*this);
    }

    unsigned AnalogSignalPulseConfig::realPeriodPoints() const {
        return static_cast<unsigned>(signalGenFreq()/signalRealFreq() + 0.5);
    }

    unsigned AnalogSignalPulseConfig::realDutyPoints() const {
        return static_cast<unsigned>(m_duty * realPeriodPoints() + 0.5);
    }

    double AnalogSignalPulseConfig::realDutyCycle() const {
        return static_cast<double>(realDutyPoints())/realPeriodPoints();
    }

    void AnalogSignalPulseConfig::protLoad(const QJsonObject &sigObj) {
        m_amp_pulse = sigObj[cfgkey_amp_pulse].toDouble();
        m_amp_pause = sigObj[cfgkey_amp_pause].toDouble();
        m_freq = sigObj[cfgkey_freq].toDouble();
        m_duty = sigObj[cfgkey_duty_perc].toDouble();
    }

    void AnalogSignalPulseConfig::protSave(QJsonObject &sigObj) const {
        sigObj[cfgkey_amp_pulse] = m_amp_pulse;
        sigObj[cfgkey_amp_pause] = m_amp_pause;
        sigObj[cfgkey_freq] = m_freq;
        sigObj[cfgkey_duty_perc] = m_duty;
    }

    bool AnalogSignalPulseConfig::protIsEqual(const SignalConfig &other) const {
        const AnalogSignalPulseConfig &otherSig
                = static_cast<const AnalogSignalPulseConfig &>(other);
        return (m_amp_pulse == otherSig.m_amp_pulse) &&
                (m_amp_pause == otherSig.m_amp_pause) &&
                (m_freq == otherSig.m_freq) &&
                (m_duty == otherSig.m_duty);
    }
}
