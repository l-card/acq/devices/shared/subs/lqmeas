#ifndef LQMEAS_ANALOGSIGNALPULSE_H
#define LQMEAS_ANALOGSIGNALPULSE_H

#include "../AnalogSignalType.h"

namespace LQMeas {
    class AnalogSignalPulse : public AnalogSignalType {
        Q_OBJECT
    public:
        static const AnalogSignalPulse &instance();

        QString typeName() const override {return QStringLiteral("Pulse");}
        QString displayName() const override {return tr("Pulse");}
        SignalConfig *createConfig() const override;
        AnalogSignalGenerator *createAnalogGenerator(const SignalConfig &cfg) const override;
    };
}

#endif // LQMEAS_ANALOGSIGNALPULSE_H
