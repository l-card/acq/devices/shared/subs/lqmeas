#ifndef LQMEAS_ANALOGSIGNALPULSECONFIG_H
#define LQMEAS_ANALOGSIGNALPULSECONFIG_H

#include "../../SignalConfig.h"

namespace LQMeas {
    class AnalogSignalPulseConfig : public SignalConfig {
        Q_OBJECT
    public:
        AnalogSignalPulseConfig(double amp_pulse = 0, double amp_pause = 0, double freq = 0, double dutyCycle = 0);
        AnalogSignalPulseConfig(const AnalogSignalPulseConfig &other);

        SignalConfig *clone() const override;

        double signalFreq() const override {return m_freq;}
        int signalMinPoints() const override {return 2;}
        bool signalStrictPoints() const override {return true;}

        void setSignalFreq(double freq) {m_freq = freq;}

        void setPulseAmplitude(double amp) {m_amp_pulse =  amp;}
        double pulseAmplitude() const {return m_amp_pulse;}

        void setPauseAmplitude(double amp) {m_amp_pause = amp;}
        double pauseAmplitude() const {return m_amp_pause;}

        void setDutyCycle(double d) {m_duty = d;}
        double dutyCycle() const {return m_duty;}

        unsigned realPeriodPoints() const;
        unsigned realDutyPoints() const;
        double realDutyCycle() const;
    protected:
        void protLoad(const QJsonObject &sigObj) override;
        void protSave(QJsonObject &sigObj) const override;
        bool protIsEqual(const SignalConfig &other) const override;
    private:
        double m_amp_pulse, m_amp_pause;
        double m_freq;
        double m_duty;
    };
}

#endif // LQMEAS_ANALOGSIGNALPULSECONFIG_H
