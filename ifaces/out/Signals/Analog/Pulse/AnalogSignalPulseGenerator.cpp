#include "AnalogSignalPulseGenerator.h"
#include "AnalogSignalPulseConfig.h"

namespace LQMeas {
    AnalogSignalPulseGenerator::AnalogSignalPulseGenerator(const AnalogSignalPulseConfig &cfg) :
        m_pt_len{cfg.realPeriodPoints()},
        m_pt_duty{cfg.realDutyPoints()},
        m_amp_pulse{cfg.pulseAmplitude()},
        m_amp_pause{cfg.pauseAmplitude()} {
    }

    double AnalogSignalPulseGenerator::signalGenNextPoint() {
        double val = m_cur_pt < m_pt_duty ?  m_amp_pulse : m_amp_pause;
        if (++m_cur_pt == m_pt_len)
            m_cur_pt = 0;
        return val;
    }
}
