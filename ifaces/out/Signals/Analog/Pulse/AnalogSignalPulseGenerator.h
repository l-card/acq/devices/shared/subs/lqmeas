#ifndef LQMEAS_ANALOGSIGNALPULSEGENERATOR_H
#define LQMEAS_ANALOGSIGNALPULSEGENERATOR_H

#include "../AnalogSignalGenerator.h"
#include <QtGlobal>

namespace LQMeas {
    class AnalogSignalPulseConfig;

    class AnalogSignalPulseGenerator : public AnalogSignalGenerator{
    public:
        AnalogSignalPulseGenerator(const AnalogSignalPulseConfig &cfg);

        void signalGenInit() override {m_cur_pt = 0; }
        double signalGenNextPoint() override;
    private:
        unsigned m_cur_pt;
        unsigned m_pt_len;
        unsigned m_pt_duty;
        double m_amp_pulse;
        double m_amp_pause;
    };
}

#endif // LQMEAS_ANALOGSIGNALPULSEGENERATOR_H
