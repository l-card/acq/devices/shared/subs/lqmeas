#include "AnalogSignalPulse.h"
#include "AnalogSignalPulseConfig.h"
#include "AnalogSignalPulseGenerator.h"

namespace LQMeas {

    const AnalogSignalPulse &AnalogSignalPulse::instance() {
        static const AnalogSignalPulse type;
        return type;
    }

    SignalConfig *AnalogSignalPulse::createConfig() const {
        return new AnalogSignalPulseConfig{};
    }

    AnalogSignalGenerator *AnalogSignalPulse::createAnalogGenerator(const SignalConfig &cfg) const {
        return new AnalogSignalPulseGenerator{static_cast<const AnalogSignalPulseConfig &>(cfg)};
    }

}
