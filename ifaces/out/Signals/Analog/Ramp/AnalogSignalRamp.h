#ifndef LQMEAS_ANALOGSIGNALRAMP_H
#define LQMEAS_ANALOGSIGNALRAMP_H

#include "../AnalogSignalType.h"

namespace LQMeas {
    class AnalogSignalRamp : public AnalogSignalType {
        Q_OBJECT
    public:
        static const AnalogSignalRamp &instance();

        QString typeName() const override {return QStringLiteral("Ramp");}
        QString displayName() const override {return tr("Ramp");}
        SignalConfig *createConfig() const override;
        AnalogSignalGenerator *createAnalogGenerator(const SignalConfig &cfg) const override;
    };
}

#endif // LQMEAS_ANALOGSIGNALRAMP_H
