#include "AnalogSignalRamp.h"
#include "AnalogSignalRampConfig.h"
#include "AnalogSignalRampGenerator.h"

namespace LQMeas {

    const AnalogSignalRamp &AnalogSignalRamp::instance() {
        static const AnalogSignalRamp type;
        return type;
    }

    SignalConfig *AnalogSignalRamp::createConfig() const {
        return new AnalogSignalRampConfig{};
    }

    AnalogSignalGenerator *AnalogSignalRamp::createAnalogGenerator(const SignalConfig &cfg) const {
        return new AnalogSignalRampGenerator{static_cast<const AnalogSignalRampConfig &>(cfg)};
    }

}
