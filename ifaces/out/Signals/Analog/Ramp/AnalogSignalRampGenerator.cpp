#include "AnalogSignalRampGenerator.h"
#include "AnalogSignalRampConfig.h"

namespace LQMeas {
    AnalogSignalRampGenerator::AnalogSignalRampGenerator(const AnalogSignalRampConfig &cfg) :
        m_pt_len{cfg.realPeriodPoints()},
        m_init_amp{cfg.initAmplitude()} {

        m_damp = (m_pt_len > 1) ? (cfg.endAmplitude() - m_init_amp)/(m_pt_len - 1) : 0;
    }

    double AnalogSignalRampGenerator::signalGenNextPoint() {
        double val = m_cur_amp;
        if (++m_cur_pt == m_pt_len) {
            signalGenInit();
        } else {
            m_cur_amp += m_damp;
        }
        return val;
    }
}
