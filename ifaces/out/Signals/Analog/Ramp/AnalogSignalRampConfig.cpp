#include "AnalogSignalRampConfig.h"
#include "AnalogSignalRamp.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>


namespace LQMeas {
    static const QLatin1String cfgkey_amp_init          {"AmpInit"};
    static const QLatin1String cfgkey_amp_end           {"AmpEnd"};
    static const QLatin1String cfgkey_freq              {StdConfigKeys::freq()};

    AnalogSignalRampConfig::AnalogSignalRampConfig(double init_amp, double end_amp, double freq) :
        SignalConfig{AnalogSignalRamp::instance()},
        m_init_amp{init_amp},
        m_end_amp{end_amp},
        m_freq{freq} {

    }

    AnalogSignalRampConfig::AnalogSignalRampConfig(const AnalogSignalRampConfig &other) :
        SignalConfig{other},
        m_init_amp{other.m_init_amp},
        m_end_amp{other.m_end_amp},
        m_freq{other.m_freq} {

    }

    SignalConfig *AnalogSignalRampConfig::clone() const {
        return new AnalogSignalRampConfig(*this);
    }

    unsigned AnalogSignalRampConfig::realPeriodPoints() const {
        return static_cast<unsigned>(signalGenFreq()/signalRealFreq() + 0.5);
    }

    void AnalogSignalRampConfig::protLoad(const QJsonObject &sigObj) {
        m_init_amp = sigObj[cfgkey_amp_init].toDouble();
        m_end_amp  = sigObj[cfgkey_amp_end].toDouble();
        m_freq = sigObj[cfgkey_freq].toDouble();
    }

    void AnalogSignalRampConfig::protSave(QJsonObject &sigObj) const {
        sigObj[cfgkey_amp_init] = m_init_amp;
        sigObj[cfgkey_amp_end] = m_end_amp;
        sigObj[cfgkey_freq] = m_freq;
    }

    bool AnalogSignalRampConfig::protIsEqual(const SignalConfig &other) const  {
        const AnalogSignalRampConfig &otherSig
                = static_cast<const AnalogSignalRampConfig &>(other);
        return  (m_init_amp == otherSig.m_init_amp)  &&
                (m_end_amp == otherSig.m_end_amp) &&
                (m_freq == otherSig.m_freq);
    }
}
