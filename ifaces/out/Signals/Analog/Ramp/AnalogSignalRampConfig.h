#ifndef LQMEAS_ANALOGSIGNALRAMPCONFIG_H
#define LQMEAS_ANALOGSIGNALRAMPCONFIG_H

#include "../../SignalConfig.h"

namespace LQMeas {
    class AnalogSignalRampConfig : public SignalConfig {
        Q_OBJECT
    public:
        AnalogSignalRampConfig(double init_amp = 0, double end_amp = 0, double freq = 0);
        AnalogSignalRampConfig(const AnalogSignalRampConfig &other);

        SignalConfig *clone() const override;

        double signalFreq() const override {return m_freq;}
        int signalMinPoints() const override {return 2;}
        bool signalStrictPoints() const override {return true;}

        void setSignalFreq(double freq) {m_freq = freq;}

        double initAmplitude() const {return m_init_amp;}
        void setInitAmplitude(double amp) {m_init_amp = amp;}
        double endAmplitude()  const {return m_end_amp;}
        void setEndAmplitude(double amp) {m_end_amp = amp;}


        unsigned realPeriodPoints() const;
    protected:
        void protLoad(const QJsonObject &sigObj) override;
        void protSave(QJsonObject &sigObj) const override;
        bool protIsEqual(const SignalConfig &other) const override;
    private:        
        double m_init_amp, m_end_amp;
        double m_freq;
    };
}

#endif // LQMEAS_ANALOGSIGNALRAMPCONFIG_H
