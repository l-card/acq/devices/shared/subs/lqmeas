#ifndef LQMEAS_ANALOGSIGNALRAMPGENERATOR_H
#define LQMEAS_ANALOGSIGNALRAMPGENERATOR_H

#include "../AnalogSignalGenerator.h"
#include <QtGlobal>

namespace LQMeas {
    class AnalogSignalRampConfig;

    class AnalogSignalRampGenerator : public AnalogSignalGenerator{
    public:
        AnalogSignalRampGenerator(const AnalogSignalRampConfig &cfg);

        void signalGenInit() override {m_cur_pt = 0; m_cur_amp = m_init_amp;}
        double signalGenNextPoint() override;
    private:
        unsigned m_cur_pt;
        double   m_cur_amp;

        unsigned m_pt_len;
        double   m_init_amp;
        double   m_damp;
    };
}

#endif // LQMEAS_ANALOGSIGNALRAMPGENERATOR_H
