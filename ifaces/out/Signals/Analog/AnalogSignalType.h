#ifndef LQMEAS_ANALOGSIGNALTYPE_H
#define LQMEAS_ANALOGSIGNALTYPE_H

#include "../SignalType.h"
#include "AnalogSignalGenerator.h"

namespace LQMeas {
    class AnalogSignalType : public SignalType {
        Q_OBJECT
    public:
        static QString typeClassName() {return QStringLiteral("Analog");}

        virtual AnalogSignalGenerator *createAnalogGenerator(const SignalConfig &cfg) const = 0;

        QString className() const override {return typeClassName();}
        SignalGenerator *createGenerator(const SignalConfig &cfg) const override {
            return createAnalogGenerator(cfg);
        }
    };
}


#endif // LQMEAS_ANALOGSIGNALTYPE_H
