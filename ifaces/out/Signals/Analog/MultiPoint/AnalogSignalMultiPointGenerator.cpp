#include "AnalogSignalMultiPointGenerator.h"
#include "AnalogSignalMultiPointConnector.h"
#include <QtMath>

namespace LQMeas {
    AnalogSignalMultiPointGenerator::AnalogSignalMultiPointGenerator(const AnalogSignalMultiPointConfig &cfg) {
        for (int pt_idx = 0; pt_idx < cfg.pointsCount(); ++pt_idx) {
            m_pts.append(new AnalogSignalMultiPointConfig::Point{cfg.point(pt_idx)});
        }
        m_periodic = cfg.periodicMode();
        m_dt = 1000./cfg.signalGenFreq();
    }

    AnalogSignalMultiPointGenerator::~AnalogSignalMultiPointGenerator() {
        qDeleteAll(m_pts);
    }

    void AnalogSignalMultiPointGenerator::signalGenInit() {
        resetTime();
    }

    double AnalogSignalMultiPointGenerator::signalGenNextPoint() {
        while ((m_next_time_change >= 0) && (m_cur_time > m_next_time_change)) {
            ++m_cur_cfg_next_pt_num;
            m_next_time_change = m_cur_cfg_next_pt_num < m_pts.size() ?
                        m_pts.at(m_cur_cfg_next_pt_num)->timeMs() : -1;
            if (m_periodic && (m_next_time_change < 0)) {
                resetTime();
            }
        }
        double val = m_next_time_change < 0 ? (m_pts.isEmpty() ? 0 : m_pts.last()->value()) :
                                              m_pts.at(m_cur_cfg_next_pt_num)->connector().calcPoint(
                                                  m_cur_time, m_cur_cfg_next_pt_num, m_pts);
        m_cur_time += m_dt;
        return val;
    }

    qint64 AnalogSignalMultiPointGenerator::totalGenPoints() const {
        return m_periodic ? -1 : qCeil(m_pts.last()->timeMs()/m_dt);
    }

    void AnalogSignalMultiPointGenerator::resetTime()     {
        m_cur_time = 0;
        m_cur_cfg_next_pt_num = 0;


        m_next_time_change = m_pts.isEmpty() ? -1 : m_pts.at(0)->timeMs();
    }
}
