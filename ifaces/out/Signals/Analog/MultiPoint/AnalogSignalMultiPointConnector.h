#ifndef ANALOGSIGNALMULTIPOINTCONNECTOR_H
#define ANALOGSIGNALMULTIPOINTCONNECTOR_H

#include "lqmeas/EnumNamedValue.h"
#include "AnalogSignalMultiPointConfig.h"
#include <QList>

namespace LQMeas {
    class AnalogSignalMultiPointConnector : public EnumNamedValue<QString> {
    public:
        virtual double calcPoint(double time_ms, int next_pt_num,
                                 const QList<const AnalogSignalMultiPointConfig::Point *> &points)  const = 0;
    };

    class AnalogSignalMultiPointConnectors  : public QObject {
        Q_OBJECT
    public:
        static const AnalogSignalMultiPointConnector &step();
        static const AnalogSignalMultiPointConnector &linear();
    };
}

#endif // ANALOGSIGNALMULTIPOINTCONNECTOR_H
