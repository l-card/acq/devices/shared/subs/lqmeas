#include "AnalogSignalMultiPointConnector.h"


const LQMeas::AnalogSignalMultiPointConnector &LQMeas::AnalogSignalMultiPointConnectors::step() {
    static const class AnalogSignalMultiPointConnectorStep : public AnalogSignalMultiPointConnector {
        QString id() const override {return QStringLiteral("con_step");}
        QString displayName() const override {return tr("Step");}
        double calcPoint(double time_ms, int next_pt_num,
                         const QList<const AnalogSignalMultiPointConfig::Point *> &points)
                const override {
            Q_UNUSED(time_ms)
            return time_ms < points.at(next_pt_num)->timeMs() ?
                        (next_pt_num == 0 ? 0 : points.at(next_pt_num-1)->value()) :
                        points.at(next_pt_num)->value();
        }
    } con;
    return con;
}

const LQMeas::AnalogSignalMultiPointConnector &LQMeas::AnalogSignalMultiPointConnectors::linear() {
    static const class AnalogSignalMultiPointConnectorLinear : public AnalogSignalMultiPointConnector {
        QString id() const override {return QStringLiteral("con_linear");}
        QString displayName() const override {return tr("Linear");}
        double calcPoint(double time_ms, int next_pt_num,
                         const QList<const AnalogSignalMultiPointConfig::Point *> &points)
                const override {
            double prev_time = 0, prev_val = 0;

            if (next_pt_num != 0) {
                prev_time = points.at(next_pt_num-1)->timeMs();
                prev_val = points.at(next_pt_num-1)->value();
            }
            double next_time = points.at(next_pt_num)->timeMs();
            double next_val = points.at(next_pt_num)->value();

            double k = (next_val - prev_val)/(next_time - prev_time);
            return prev_val + k *(time_ms - prev_time);
        }
    } con;
    return con;
}
