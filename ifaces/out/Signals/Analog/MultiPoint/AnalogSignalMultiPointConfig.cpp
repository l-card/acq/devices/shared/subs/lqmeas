#include "AnalogSignalMultiPointConfig.h"
#include "AnalogSignalMultiPoint.h"
#include "AnalogSignalMultiPointConnector.h"
#include <QJsonObject>
#include <QJsonArray>
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String cfgkey_points_array          {StdConfigKeys::points()};
    static const QLatin1String cfgkey_point_value           {StdConfigKeys::value()};
    static const QLatin1String cfgkey_point_time            {StdConfigKeys::timeMs()};
    static const QLatin1String cfgkey_point_connector_obj   {"Connector"};
    static const QLatin1String cfgkey_point_connector_id    {StdConfigKeys::ID()};

    AnalogSignalMultiPointConfig::AnalogSignalMultiPointConfig() :
        SignalConfig{AnalogSignalMultiPoint::instance()} {

    }

    AnalogSignalMultiPointConfig::AnalogSignalMultiPointConfig(
            const AnalogSignalMultiPointConfig &other) :
        SignalConfig{other} {

        for (Point *pt : other.m_points) {
            m_points.append(new Point{*pt});
        }
    }

    AnalogSignalMultiPointConfig::~AnalogSignalMultiPointConfig() {
        clear();
    }

    SignalConfig *AnalogSignalMultiPointConfig::clone() const {
        return new AnalogSignalMultiPointConfig{*this};
    }

    double AnalogSignalMultiPointConfig::signalFreq() const {
        return  m_points.isEmpty() ? unspecifiedFreq() : 1000./m_points.last()->timeMs();
    }

    const QList<const AnalogSignalMultiPointConnector *> &AnalogSignalMultiPointConfig::supportedConnectors() {
        static const QList<const AnalogSignalMultiPointConnector *> list {
            &AnalogSignalMultiPointConnectors::step(),
            &AnalogSignalMultiPointConnectors::linear()
        };
        return list;
    }

    const AnalogSignalMultiPointConnector *AnalogSignalMultiPointConfig::getConnector(QString id) {
        const AnalogSignalMultiPointConnector *ret = nullptr;
        for (const AnalogSignalMultiPointConnector *con : supportedConnectors()) {
            if (con->id() == id) {
                ret = con;
                break;
            }
        }
        return ret;
    }

    void AnalogSignalMultiPointConfig::clear() {
        QList<Point *> points = m_points;
        m_points.clear();
        /* посылаем сигнал после очистка списка, но перед удалением элементов,
         * чтобы все получатели могли удалить ссылки */
        Q_EMIT pointListReconstructed();
        qDeleteAll(points);
    }

    int AnalogSignalMultiPointConfig::pointPos(const AnalogSignalMultiPointConfig::Point *pt) const {
        int ret = -1;
        for (int i = 0; i < m_points.size(); ++i) {
            const Point *checkPt = m_points.at(i);
            if (checkPt == pt) {
                ret = i;
                break;
            }
        }
        return ret;
    }

    int AnalogSignalMultiPointConfig::addPoint(const AnalogSignalMultiPointConfig::Point &pt) {
        int pos = getNewPointIdx(pt.timeMs());
        Point *newPt = new Point{pt};
        m_points.insert(pos, newPt);
        Q_EMIT pointAppend(newPt, pos);
        return pos;
    }

    int AnalogSignalMultiPointConfig::addPoint(double time_ms, double val, const AnalogSignalMultiPointConnector &con) {
        return addPoint(Point{time_ms, val, con});
    }

    void AnalogSignalMultiPointConfig::setPointValue(int pos, double val) {
        Point *pt = m_points.at(pos);
        pt->setVal(val);
        Q_EMIT pointConfigChanged(pt);
    }

    void AnalogSignalMultiPointConfig::setPointConnector(int pos, const AnalogSignalMultiPointConnector &con) {
        Point *pt = m_points.at(pos);
        pt->setConnector(con);
        Q_EMIT pointConfigChanged(pt);
    }

    void AnalogSignalMultiPointConfig::setPointTime(int pos, double time) {
        int new_pos = getNewPointIdx(time, pos);
        if (new_pos != pos) {
            Point *pt = m_points.takeAt(pos);
            pt->setTimeMs(time);
            m_points.insert(new_pos, pt);
            Q_EMIT pointMoved(pt);
            Q_EMIT pointConfigChanged(pt);
        } else {
            m_points.at(pos)->setTimeMs(time);
        }
    }

    void AnalogSignalMultiPointConfig::addPointsList(const QList<AnalogSignalMultiPointConfig::Point *> &pts) {
        m_points.append(pts);
        std::sort(m_points.begin(), m_points.end(), pointsSortCmp);
        Q_EMIT pointListReconstructed();
    }

    void AnalogSignalMultiPointConfig::removePoint(int pos) {
        Point *pt = m_points.takeAt(pos);
        Q_EMIT pointRemoved(pt);
        delete  pt;
    }



    void AnalogSignalMultiPointConfig::protLoad(const QJsonObject &sigObj) {
        clear();
        QList<Point *> newPoints;
        const QJsonArray &pointsArray = sigObj[cfgkey_points_array].toArray();
        for (QJsonArray::const_iterator it = pointsArray.constBegin(); it < pointsArray.constEnd(); it++) {
            const QJsonObject &ptObj = (*it).toObject();
            double time_ms = ptObj[cfgkey_point_time].toDouble();
            double val = ptObj[cfgkey_point_value].toDouble();
            const QJsonObject &conObj = ptObj[cfgkey_point_connector_obj].toObject();

            const AnalogSignalMultiPointConnector *con = getConnector(
                        conObj[cfgkey_point_connector_id].toString());
            if (!con)
                con = &AnalogSignalMultiPointConnectors::step();

            newPoints.append(new Point(time_ms, val, *con));
        }
        addPointsList(newPoints);
    }

    void AnalogSignalMultiPointConfig::protSave(QJsonObject &sigObj) const {
        QJsonArray pointsArray;
        for (Point *pt : m_points) {
            QJsonObject ptObj;
            ptObj[cfgkey_point_time] = pt->timeMs();
            ptObj[cfgkey_point_value] = pt->value();

            QJsonObject conObj;
            conObj[cfgkey_point_connector_id] = pt->connector().id();
            ptObj[cfgkey_point_connector_obj] = conObj;

            pointsArray.append(ptObj);
        }
        sigObj[cfgkey_points_array] = pointsArray;
    }

    bool AnalogSignalMultiPointConfig::protIsEqual(const SignalConfig &other) const {
        const AnalogSignalMultiPointConfig &otherSig
                = static_cast<const AnalogSignalMultiPointConfig &>(other);
        bool eq = (periodicMode() == otherSig.periodicMode()) && (m_points.size() == otherSig.m_points.size());
        for (int pt_idx = 0; eq && (pt_idx < m_points.size()); ++pt_idx) {
            Point *pt = m_points.at(pt_idx);
            Point *otherPt = otherSig.m_points.at(pt_idx);
            eq = LQREAL_IS_EQUAL(pt->value(), otherPt->value()) &&
                    LQREAL_IS_EQUAL(pt->timeMs(), otherPt->timeMs()) &&
                    (pt->connector() == otherPt->connector());
        }
        return eq;
    }

    int AnalogSignalMultiPointConfig::getNewPointIdx(double time_ms, int skip_pos) const {
        int pos = 0;
        for (int i = 0; i < m_points.size(); ++i) {
            if (i != skip_pos) {
                const Point *pt = m_points.at(i);
                if (pt->timeMs() < time_ms) {
                    pos++;
                } else {
                    break;
                }
            }
        }
        return pos;
    }

    bool AnalogSignalMultiPointConfig::pointsSortCmp(const AnalogSignalMultiPointConfig::Point *p1,
                                                             const AnalogSignalMultiPointConfig::Point *p2) {
        return  p1->timeMs() < p2->timeMs();
    }
}
