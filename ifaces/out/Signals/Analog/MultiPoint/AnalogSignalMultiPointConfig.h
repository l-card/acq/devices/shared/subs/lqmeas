#ifndef ANALOGSIGNALMULTIPOINTCONFIG_H
#define ANALOGSIGNALMULTIPOINTCONFIG_H


#include "../../SignalConfig.h"
#include <QList>


namespace LQMeas {
    class AnalogSignalMultiPointConnector;

    class AnalogSignalMultiPointConfig : public SignalConfig {
        Q_OBJECT
    public:
        struct Point {
        public:
            Point(double time_ms, double val, const AnalogSignalMultiPointConnector &con) :
                m_time_ms{time_ms}, m_val{val}, m_con{&con} {}

            Point(const Point &other) :
                m_time_ms{other.m_time_ms},
                m_val{other.m_val},
                m_con{other.m_con} {}

            double timeMs() const {return m_time_ms;}
            double value() const {return m_val;}
            const AnalogSignalMultiPointConnector &connector() const {return *m_con;}

            void setTimeMs(double ms) {m_time_ms = ms;}
            void setVal(double val) {m_val = val;}
            void setConnector(const AnalogSignalMultiPointConnector &con) {m_con = &con;}

        private:
            double m_time_ms;
            double m_val;
            const AnalogSignalMultiPointConnector *m_con;
        };


        AnalogSignalMultiPointConfig();
        AnalogSignalMultiPointConfig(const AnalogSignalMultiPointConfig &other);
        ~AnalogSignalMultiPointConfig() override;

        SignalConfig *clone() const override;

        double signalFreq() const override;
        int signalMinPoints() const override {return 1;}
        bool signalStrictPoints() const override {return true;}

        static const QList<const AnalogSignalMultiPointConnector *> &supportedConnectors();
        static const AnalogSignalMultiPointConnector *getConnector(QString id);


        void clear();

        const Point &point(int idx) const {return *m_points.at(idx);}
        int pointsCount() const {return m_points.size();}
        int pointPos(const Point *pt) const;

        int addPoint(const Point &pt);
        int addPoint(double time_ms, double val, const AnalogSignalMultiPointConnector &con);
        void setPointValue(int pos, double val);
        void setPointConnector(int pos, const AnalogSignalMultiPointConnector &con);
        void setPointTime(int pos, double time);
        void addPointsList(const QList<Point *> &pts);
        void removePoint(int pos);
    Q_SIGNALS:
        void pointAppend(const LQMeas::AnalogSignalMultiPointConfig::Point *pt, int idx);
        void pointRemoved(const LQMeas::AnalogSignalMultiPointConfig::Point *pt);
        void pointMoved(const LQMeas::AnalogSignalMultiPointConfig::Point *pt);
        void pointConfigChanged(const LQMeas::AnalogSignalMultiPointConfig::Point *pt);
        void pointListReconstructed();
    protected:
        void protLoad(const QJsonObject &sigObj) override;
        void protSave(QJsonObject &sigObj) const override;
        bool protIsEqual(const SignalConfig &other) const override;
    private:
        int getNewPointIdx(double time_ms, int skip_pos = -1) const;
        static bool pointsSortCmp(const Point *p1, const Point *p2);
        QList<Point *> m_points;
    };
}

#endif // ANALOGSIGNALMULTIPOINTCONFIG_H
