#ifndef ANALOGSIGNALMULTIPOINT_H
#define ANALOGSIGNALMULTIPOINT_H


#include "../AnalogSignalType.h"

namespace LQMeas {
    class AnalogSignalMultiPoint : public AnalogSignalType {
        Q_OBJECT
    public:
        static const AnalogSignalMultiPoint &instance();

        QString typeName() const override {return QStringLiteral("MultiPoint");}
        QString displayName() const override {return tr("Multiple Point Based");}
        SignalConfig *createConfig() const override;
        AnalogSignalGenerator *createAnalogGenerator(const SignalConfig &cfg) const override;
    };
}
#endif // ANALOGSIGNALMULTIPOINT_H
