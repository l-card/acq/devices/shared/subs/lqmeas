#ifndef ANALOGSIGNALMULTIPOINTGENERATOR_H
#define ANALOGSIGNALMULTIPOINTGENERATOR_H


#include "../AnalogSignalGenerator.h"
#include "AnalogSignalMultiPointConfig.h"

namespace LQMeas {
    class AnalogSignalMultiPointGenerator : public AnalogSignalGenerator{
    public:
        AnalogSignalMultiPointGenerator(const AnalogSignalMultiPointConfig &cfg);
        ~AnalogSignalMultiPointGenerator() override;

        void signalGenInit() override;
        double signalGenNextPoint() override;
        qint64 totalGenPoints() const override;
    private:
        void resetTime();


        double m_cur_time;
        double m_next_time_change;
        double m_dt;

        int m_cur_cfg_next_pt_num;
        QList<const AnalogSignalMultiPointConfig::Point *> m_pts;
        bool m_periodic;
    };
}

#endif // ANALOGSIGNALMULTIPOINTGENERATOR_H
