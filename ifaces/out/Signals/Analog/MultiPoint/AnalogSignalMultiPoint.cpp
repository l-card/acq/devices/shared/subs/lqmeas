#include "AnalogSignalMultiPoint.h"
#include "AnalogSignalMultiPointConfig.h"
#include "AnalogSignalMultiPointGenerator.h"
namespace LQMeas {
    const AnalogSignalMultiPoint &AnalogSignalMultiPoint::instance() {
        static const AnalogSignalMultiPoint type;
        return type;
    }

    SignalConfig *AnalogSignalMultiPoint::createConfig() const  {
        return new AnalogSignalMultiPointConfig{};
    }

    AnalogSignalGenerator *AnalogSignalMultiPoint::createAnalogGenerator(const SignalConfig &cfg) const {
        return new AnalogSignalMultiPointGenerator{static_cast<const AnalogSignalMultiPointConfig &>(cfg)};
    }
}
