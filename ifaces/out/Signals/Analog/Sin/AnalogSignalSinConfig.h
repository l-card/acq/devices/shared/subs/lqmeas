#ifndef LQMEAS_ANALOGSIGNALSINCONFIG_H
#define LQMEAS_ANALOGSIGNALSINCONFIG_H

#include "../../SignalConfig.h"

namespace LQMeas {
    class AnalogSignalSinConfig : public SignalConfig {
        Q_OBJECT
    public:
        AnalogSignalSinConfig(double amp = 0, double offs = 0, double freq = 0, double pha = 0);
        AnalogSignalSinConfig(const AnalogSignalSinConfig &other);

        SignalConfig *clone() const override;

        double signalFreq() const override {return m_freq;}
        int signalMinPoints() const override {return 2;}
        bool signalStrictPoints() const override {return false;}

        void setSignalFreq(double freq) {m_freq = freq;}

        void setAmplitude(double amp) {m_amp = amp;}
        double amplitude() const {return m_amp;}

        void setOffset(double offs) {m_offs = offs;}
        double offset() const {return m_offs;}

        void setPhase(double phase) {m_pha = phase;}
        double phase() const {return m_pha;}

    protected:
        void protLoad(const QJsonObject &sigObj) override;
        void protSave(QJsonObject &sigObj) const override;
        bool protIsEqual(const SignalConfig &other) const override;
    private:
        double m_amp;
        double m_offs;
        double m_freq;
        double m_pha;
    };
}

#endif // LQMEAS_ANALOGSIGNALSINCONFIG_H
