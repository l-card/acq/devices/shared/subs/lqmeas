#ifndef LQMEAS_ANALOGSIGNALSINGENERATOR_H
#define LQMEAS_ANALOGSIGNALSINGENERATOR_H

#include "../AnalogSignalGenerator.h"
#include "AnalogSignalSinConfig.h"

namespace LQMeas {
    class AnalogSignalSinGenerator : public AnalogSignalGenerator{
    public:
        AnalogSignalSinGenerator(const AnalogSignalSinConfig &cfg);

        void signalGenInit() override {m_cur_pha = m_init_pha; }
        double signalGenNextPoint() override;
    private:
        double m_amp;
        double m_offs;
        double m_init_pha;
        double m_d_pha;

        double m_cur_pha;
    };
}

#endif // LQMEAS_ANALOGSIGNALSINGENERATOR_H
