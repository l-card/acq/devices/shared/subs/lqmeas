#include "AnalogSignalSinConfig.h"
#include "AnalogSignalSin.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_amp      {"Amp"};
    static const QLatin1String cfgkey_offs     {"Offs"};
    static const QLatin1String cfgkey_freq     {StdConfigKeys::freq()};
    static const QLatin1String cfgkey_pha      {"Pha"};


    AnalogSignalSinConfig::AnalogSignalSinConfig(double amp, double offs, double freq, double pha) :
        SignalConfig{AnalogSignalSin::instance()},
        m_amp{amp},
        m_offs{offs},
        m_freq{freq},
        m_pha{pha} {

    }

    AnalogSignalSinConfig::AnalogSignalSinConfig(const AnalogSignalSinConfig &other) :
        SignalConfig{other},
        m_amp{other.m_amp},
        m_offs{other.m_offs},
        m_freq{other.m_freq},
        m_pha{other.m_pha} {

    }

    SignalConfig *AnalogSignalSinConfig::clone() const {
        return new AnalogSignalSinConfig(*this);
    }

    void AnalogSignalSinConfig::protLoad(const QJsonObject &sigObj) {
        m_amp = sigObj[cfgkey_amp].toDouble();
        m_offs = sigObj[cfgkey_offs].toDouble();
        m_freq = sigObj[cfgkey_freq].toDouble();
        m_pha = sigObj[cfgkey_pha].toDouble();
    }

    void AnalogSignalSinConfig::protSave(QJsonObject &sigObj) const {
        sigObj[cfgkey_amp] = m_amp;
        sigObj[cfgkey_offs] = m_offs;
        sigObj[cfgkey_freq] = m_freq;
        sigObj[cfgkey_pha] = m_pha;
    }

    bool AnalogSignalSinConfig::protIsEqual(const SignalConfig &other) const {
        const AnalogSignalSinConfig &otherSig
                = static_cast<const AnalogSignalSinConfig &>(other);
        return (m_amp == otherSig.m_amp) &&
                (m_offs == otherSig.m_offs) &&
                (m_freq == otherSig.m_freq) &&
                (m_pha == otherSig.m_pha);
    }
}
