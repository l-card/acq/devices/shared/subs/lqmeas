#include "AnalogSignalSinGenerator.h"
#include <qmath.h>

namespace LQMeas {
    AnalogSignalSinGenerator::AnalogSignalSinGenerator(const AnalogSignalSinConfig &cfg) :
        m_amp{cfg.amplitude()},
        m_offs{cfg.offset()},
        m_init_pha{2.*M_PI * cfg.phase()/360},
        m_d_pha{cfg.signalRealFreq()/cfg.signalGenFreq()},
        m_cur_pha{m_init_pha} {

    }

    double AnalogSignalSinGenerator::signalGenNextPoint() {
        double val = m_amp * sin(m_cur_pha) + m_offs;
        m_cur_pha += 2.*M_PI * m_d_pha;
        return val;
    }
}
