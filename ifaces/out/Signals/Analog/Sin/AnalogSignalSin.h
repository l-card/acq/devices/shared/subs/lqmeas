#ifndef LQMEAS_ANALOGSIGNALSIN_H
#define LQMEAS_ANALOGSIGNALSIN_H

#include "../AnalogSignalType.h"

namespace LQMeas {
    class AnalogSignalSin : public AnalogSignalType {
        Q_OBJECT
    public:
        static const AnalogSignalSin &instance();

        QString typeName() const override {return QStringLiteral("Sinus");}
        QString displayName() const override {return tr("Sinus");}
        SignalConfig *createConfig() const override;
        AnalogSignalGenerator *createAnalogGenerator(const SignalConfig &cfg) const override;
    };
}

#endif // LQMEAS_ANALOGSIGNALSIN_H
