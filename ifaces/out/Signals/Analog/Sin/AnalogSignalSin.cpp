#include "AnalogSignalSin.h"
#include "AnalogSignalSinConfig.h"
#include "AnalogSignalSinGenerator.h"

namespace LQMeas {

    const AnalogSignalSin &AnalogSignalSin::instance() {
        static const AnalogSignalSin type;
        return type;
    }

    SignalConfig *AnalogSignalSin::createConfig() const {
        return new AnalogSignalSinConfig{};
    }

    AnalogSignalGenerator *AnalogSignalSin::createAnalogGenerator(const SignalConfig &cfg) const {
        return new AnalogSignalSinGenerator{static_cast<const AnalogSignalSinConfig &>(cfg)};
    }

}
