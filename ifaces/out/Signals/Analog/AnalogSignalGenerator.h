#ifndef LQMEAS_ANALOGSIGNALGENERATOR_H
#define LQMEAS_ANALOGSIGNALGENERATOR_H

#include "../SignalGenerator.h"

namespace LQMeas {
    class AnalogSignalGenerator : public SignalGenerator {
    public:
        virtual double signalGenNextPoint() = 0;
    };
}


#endif // LQMEAS_ANALOGSIGNALGENERATOR_H
