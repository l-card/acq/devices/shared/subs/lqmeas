#include "AnalogSignalTimeSeq.h"
#include "AnalogSignalTimeSeqConfig.h"
#include "AnalogSignalTimeSeqGenerator.h"

namespace LQMeas {
    const AnalogSignalTimeSeq &AnalogSignalTimeSeq::instance() {
        static const AnalogSignalTimeSeq sigtype;
        return sigtype;
    }

    SignalConfig *AnalogSignalTimeSeq::createConfig() const {
        return new AnalogSignalTimeSeqConfig{};
    }

    AnalogSignalGenerator *AnalogSignalTimeSeq::createAnalogGenerator(const SignalConfig &cfg) const {
        return new AnalogSignalTimeSeqGenerator{
                    static_cast<const AnalogSignalTimeSeqConfig &>(cfg)};
    }
}
