#include "AnalogSignalTimeSeqGenerator.h"
#include <QtMath>

namespace LQMeas {
    AnalogSignalTimeSeqGenerator::GenStep::GenStep(const AnalogSignalTimeSeqConfig::Step &st) :
        gen{static_cast<AnalogSignalGenerator *>(st.signal().createGenerator())},
        time_ms{st.timeMs()} {
    }

    AnalogSignalTimeSeqGenerator::GenStep::~GenStep() {
        delete gen;
    }

    AnalogSignalTimeSeqGenerator::AnalogSignalTimeSeqGenerator(const AnalogSignalTimeSeqConfig &cfg) :
        m_periodic{cfg.periodicMode()},
        m_dt{1000./cfg.signalGenFreq()},
        m_total_ms{0} {

        for (int step_idx = 0; step_idx < cfg.stepsCount(); ++step_idx) {
            m_steps.append(new GenStep{cfg.step(step_idx)});
            m_total_ms += cfg.step(step_idx).timeMs();
        }

        resetTime();
    }

    AnalogSignalTimeSeqGenerator::~AnalogSignalTimeSeqGenerator() {
        qDeleteAll(m_steps);
    }

    void AnalogSignalTimeSeqGenerator::signalGenInit() {
        resetTime();
    }

    double AnalogSignalTimeSeqGenerator::signalGenNextPoint() {
        while ((m_next_time_change >= 0) && (m_cur_time > m_next_time_change)) {
            ++m_cur_cfg_next_step_num;
            m_next_time_change = m_cur_cfg_next_step_num < m_steps.size() ?
                        m_next_time_change + m_steps.at(m_cur_cfg_next_step_num)->time_ms : -1;
            if (m_periodic && (m_cur_cfg_next_step_num >= m_steps.size())) {
                resetTime();
            }
        }
        if (!(m_next_time_change < 0)) {
            m_cur_val = m_steps.at(m_cur_cfg_next_step_num)->gen->signalGenNextPoint();
        }

        m_cur_time += m_dt;
        return m_cur_val;
    }

    qint64 AnalogSignalTimeSeqGenerator::totalGenPoints() const {
        return m_periodic ? -1 : qCeil(m_total_ms / m_dt);
    }

    void AnalogSignalTimeSeqGenerator::resetTime() {
        m_cur_time = 0;
        m_cur_cfg_next_step_num = 0;
        m_cur_val = 0;


        m_next_time_change = m_steps.isEmpty() ? -1 : m_steps.at(0)->time_ms;

        for (GenStep *step : m_steps) {
            step->gen->signalGenInit();
        }
    }

}
