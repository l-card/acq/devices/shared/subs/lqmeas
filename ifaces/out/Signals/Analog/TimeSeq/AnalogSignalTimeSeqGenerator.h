#ifndef LQMEAS_ANALOGSIGNALTIMESEQGENERATOR_H
#define LQMEAS_ANALOGSIGNALTIMESEQGENERATOR_H

#include "../AnalogSignalGenerator.h"
#include "AnalogSignalTimeSeqConfig.h"

namespace LQMeas {
    class AnalogSignalTimeSeqGenerator : public AnalogSignalGenerator {
    public:
        AnalogSignalTimeSeqGenerator(const AnalogSignalTimeSeqConfig &cfg);
        ~AnalogSignalTimeSeqGenerator() override;

        void signalGenInit() override;
        double signalGenNextPoint() override;
        qint64 totalGenPoints() const override;
    private:
        void resetTime();
    private:
        struct GenStep {
            GenStep(const AnalogSignalTimeSeqConfig::Step &st);
            ~GenStep();

            AnalogSignalGenerator *gen;
            double time_ms;
        };


        bool m_periodic;
        double m_dt;

        int m_cur_cfg_next_step_num;        
        double m_cur_val;
        double m_cur_time;
        double m_next_time_change;
        double m_total_ms;
        QList<GenStep *> m_steps;
    };
}

#endif // LQMEAS_ANALOGSIGNALTIMESEQGENERATOR_H
