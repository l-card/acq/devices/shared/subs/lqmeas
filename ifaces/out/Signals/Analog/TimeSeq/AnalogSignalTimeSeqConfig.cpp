#include "AnalogSignalTimeSeqConfig.h"
#include "AnalogSignalTimeSeq.h"
#include <QJsonObject>
#include <QJsonArray>
#include "../../SignalFabric.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdConfigKeys.h"
#include "lqmeas/misc/QListHelper.h"
#include <memory>

namespace LQMeas {
    static const QLatin1String cfgkey_steps_array   {StdConfigKeys::steps()};
    static const QLatin1String cfgkey_step_signal   {StdConfigKeys::signal()};
    static const QLatin1String cfgkey_step_time     {StdConfigKeys::timeMs()};

    AnalogSignalTimeSeqConfig::AnalogSignalTimeSeqConfig() :
        SignalConfig{AnalogSignalTimeSeq::instance()} {

    }

    AnalogSignalTimeSeqConfig::AnalogSignalTimeSeqConfig(const AnalogSignalTimeSeqConfig &other) :
        SignalConfig{other} {

        for (Step *step : other.m_steps) {
            m_steps.append(new Step{*step});
        }
    }

    AnalogSignalTimeSeqConfig::~AnalogSignalTimeSeqConfig() {
        clear();
    }

    SignalConfig *LQMeas::AnalogSignalTimeSeqConfig::clone() const {
        return new AnalogSignalTimeSeqConfig{*this};
    }

    double AnalogSignalTimeSeqConfig::signalFreq() const {
        double time_ms = 0;
        for (Step *step : m_steps) {
            if (step->timeMs() > 0) {
                time_ms += step->timeMs();
            }
        }
        return time_ms > 0 ? 1000. / time_ms : unspecifiedFreq();
    }

    void AnalogSignalTimeSeqConfig::setRealParams(double sigFreq, double ptFreq) {
        SignalConfig::setRealParams(sigFreq, ptFreq);
        for (Step *st : qAsConst(m_steps)) {
            st->m_signal->setRealParams(st->signal().signalFreq(), ptFreq);
        }
    }

    void AnalogSignalTimeSeqConfig::clear() {
        QList<Step *> steps {m_steps};
        m_steps.clear();
        /* посылаем сигнал после очистка списка, но перед удалением элементов,
         * чтобы все получатели могли удалить ссылки */
        Q_EMIT stepListReconstructed();
        qDeleteAll(steps);
    }

    int AnalogSignalTimeSeqConfig::stepPos(const AnalogSignalTimeSeqConfig::Step *st) const {
        int ret = -1;

        int pos = 0;
        for (Step *checkStep : m_steps) {
            if (checkStep == st) {
                ret = pos;
                break;
            }
            ++pos;
        }
        return ret;
    }

    void AnalogSignalTimeSeqConfig::addStep(const SignalConfig &sig, double time_ms, int step_idx) {
        Step *step = new Step(sig, time_ms);
        if (step_idx < 0)
            step_idx = m_steps.size();
        m_steps.insert(step_idx, step);
        Q_EMIT stepAppend(step, step_idx);
    }

    void AnalogSignalTimeSeqConfig::addStepList(const QList<Step *> &steps) {
        m_steps.append(steps);
        Q_EMIT stepListReconstructed();
    }

    void AnalogSignalTimeSeqConfig::setStepSignal(int pos, const SignalConfig &sig) {
        Step *st = m_steps.at(pos);
        st->setSignal(sig);
        Q_EMIT stepConfigChanged(st);
    }

    void AnalogSignalTimeSeqConfig::setStepTime(int pos, double time_ms) {
        Step *st = m_steps.at(pos);
        st->setTimeMs(time_ms);
        Q_EMIT stepConfigChanged(st);
    }

    void AnalogSignalTimeSeqConfig::removeStep(int pos) {
        Step *st = m_steps.takeAt(pos);
        Q_EMIT stepRemoved(st);
        delete  st;
    }

    void AnalogSignalTimeSeqConfig::moveSteps(int from, int to, int count) {
        QList<const Step *> movedSteps;
        for (int i = from; i < from + count; ++i) {
            movedSteps.append(m_steps.at(i));
        }

        QListHelper<Step*>::move(m_steps, from, to, count);

        Q_EMIT stepsMoved(movedSteps);
    }

    void AnalogSignalTimeSeqConfig::protLoad(const QJsonObject &sigObj) {
        clear();
        QList<Step *> newSteps;
        const QJsonArray &stepsArray = sigObj[cfgkey_steps_array].toArray();
        for (QJsonArray::const_iterator it = stepsArray.constBegin(); it < stepsArray.constEnd(); ++it) {
            const QJsonObject &stepObj {(*it).toObject()};
            const QJsonObject &stepSigObj {stepObj[cfgkey_step_signal].toObject()};
            const double time_ms {stepObj[cfgkey_step_time].toDouble()};
            const std::unique_ptr<const LQMeas::SignalConfig>  sigcfg{SignalFabric::instance()->createConfig(stepSigObj)};
            if (sigcfg && (time_ms > 0)) {
                newSteps.append(new Step{*sigcfg, time_ms});
            }
        }
        addStepList(newSteps);
    }

    void AnalogSignalTimeSeqConfig::protSave(QJsonObject &sigObj) const {
        QJsonArray stepsArray;
        for (const Step *st : m_steps) {
            QJsonObject stepObj;
            QJsonObject stepSigObj;
            st->signal().save(stepSigObj);
            stepObj[cfgkey_step_signal] = stepSigObj;
            stepObj[cfgkey_step_time] = st->timeMs();
            stepsArray.append(stepObj);
        }
        sigObj[cfgkey_steps_array] = stepsArray;
    }

    bool AnalogSignalTimeSeqConfig::protIsEqual(const SignalConfig &other) const {
        const AnalogSignalTimeSeqConfig &otherSig {static_cast<const AnalogSignalTimeSeqConfig &>(other)};
        bool eq {(periodicMode() == otherSig.periodicMode()) && (m_steps.size() == otherSig.m_steps.size())};
        for (int pt_idx = 0; eq && (pt_idx < m_steps.size()); ++pt_idx) {
            const Step *step {m_steps.at(pt_idx)};
            const Step *otherStep {otherSig.m_steps.at(pt_idx)};
            eq = LQREAL_IS_EQUAL(step->timeMs(), otherStep->timeMs()) &&
                    step->signal().isEqual(otherStep->signal());
        }
        return eq;
    }
}
