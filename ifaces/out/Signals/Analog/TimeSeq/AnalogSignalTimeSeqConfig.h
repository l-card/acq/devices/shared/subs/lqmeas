#ifndef LQMEAS_ANALOGSIGNALTIMESEQCONFIG_H
#define LQMEAS_ANALOGSIGNALTIMESEQCONFIG_H

#include "../../SignalConfig.h"
#include <QList>

namespace LQMeas {
    class AnalogSignalTimeSeqConfig  : public SignalConfig {
        Q_OBJECT
    public:
        struct Step {
            explicit Step(const SignalConfig &sig, double time_ms) :
                m_signal(sig.clone()), m_time_ms(time_ms) {
            }
            Step(const Step &other) :
                m_signal(other.m_signal->clone()), m_time_ms(other.m_time_ms) {
            }
            ~Step() {
                delete m_signal;
            }

            double timeMs() const {return m_time_ms;}
            const SignalConfig &signal() const {return *m_signal;}

            void setTimeMs(double ms) {
                m_time_ms = ms;
            }
            void setSignal(const SignalConfig &sig) {
                delete  m_signal;
                m_signal = sig.clone();
            }
        private:
            SignalConfig *m_signal;
            double m_time_ms;

            friend class AnalogSignalTimeSeqConfig;
        };



        explicit AnalogSignalTimeSeqConfig();
        AnalogSignalTimeSeqConfig(const AnalogSignalTimeSeqConfig &other);
        ~AnalogSignalTimeSeqConfig() override;

        SignalConfig *clone() const override;

        double signalFreq() const override;
        int signalMinPoints() const override {return 1;}
        bool signalStrictPoints() const override {return true;}
        void setRealParams(double sigFreq, double ptFreq) override;

        void clear();

        const Step &step(int idx) const {return *m_steps.at(idx);}
        int stepsCount() const {return m_steps.size();}
        int stepPos(const Step *st) const;

        void addStep(const SignalConfig &sig, double time_ms, int step_idx = -1);
        void addStepList(const QList<Step *> &steps);
        void setStepSignal(int pos, const SignalConfig &sig);
        void setStepTime(int pos, double time_ms);
        void removeStep(int pos);
        void moveSteps(int from, int to, int count);
    Q_SIGNALS:
        void stepAppend(const LQMeas::AnalogSignalTimeSeqConfig::Step *step, int idx);
        void stepRemoved(const LQMeas::AnalogSignalTimeSeqConfig::Step *steppt);
        void stepsMoved(QList<const LQMeas::AnalogSignalTimeSeqConfig::Step *> steps);
        void stepConfigChanged(const LQMeas::AnalogSignalTimeSeqConfig::Step *step);
        void stepListReconstructed();
    protected:
        void protLoad(const QJsonObject &sigObj) override;
        void protSave(QJsonObject &sigObj) const override;
        bool protIsEqual(const SignalConfig &other) const override;
    private:
        QList<Step *> m_steps;
    };
}

#endif // LQMEAS_ANALOGSIGNALTIMESEQCONFIG_H
