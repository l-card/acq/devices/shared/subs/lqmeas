#ifndef LQMEAS_ANALOGSIGNALTIMESEQ_H
#define LQMEAS_ANALOGSIGNALTIMESEQ_H


#include "../AnalogSignalType.h"

namespace LQMeas {
    class AnalogSignalTimeSeq : public AnalogSignalType {
        Q_OBJECT
    public:
        static const AnalogSignalTimeSeq &instance();

        QString typeName() const override {return QStringLiteral("TimeSeq");}
        QString displayName() const override {return tr("Time sequence of signals");}
        SignalConfig *createConfig() const override;
        AnalogSignalGenerator *createAnalogGenerator(const SignalConfig &cfg) const override;
    };
}

#endif // LQMEAS_ANALOGSIGNALTIMESEQ_H
