#ifndef LQMEAS_SIGNALGENERATOR_H
#define LQMEAS_SIGNALGENERATOR_H

#include <QtGlobal>

namespace LQMeas {

    class SignalGenerator {
    public:
        virtual ~SignalGenerator();
        virtual void signalGenInit() = 0;
        virtual qint64 totalGenPoints() const {return -1;}
    };
}

#endif // LQMEAS_SIGNALGENERATOR_H
