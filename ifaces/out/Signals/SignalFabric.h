#ifndef LQMEAS_SIGNALFABRIC_H
#define LQMEAS_SIGNALFABRIC_H

#include <QList>
class QJsonObject;

namespace LQMeas {
    class SignalType;
    class SignalConfig;

    class SignalFabric {
    public:
        SignalFabric();

        static SignalFabric *instance();


        void addSignalType(const SignalType *type);
        void clearTypes();

        QList<const SignalType *> classTypeList(const QString &className) const;
        SignalConfig *createConfig(const QJsonObject &sigObj);
    private:
        QList<const SignalType *> m_types;
    };
}

#endif // LQMEAS_SIGNALFABRIC_H
