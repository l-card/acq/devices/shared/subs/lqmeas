#ifndef LQMEAS_SIGNALCONFIG_H
#define LQMEAS_SIGNALCONFIG_H

#include <QString>
#include <QObject>

class QJsonObject;

namespace LQMeas {
    class SignalType;
    class SignalGenerator;

    class SignalConfig : public QObject {
        Q_OBJECT
    public:
        virtual ~SignalConfig();

        static constexpr double unspecifiedFreq() {return -1.;}

        virtual SignalConfig *clone() const = 0;
        SignalGenerator *createGenerator() const;

        const SignalType &type() const {return *m_type;}
        bool isEqual(const SignalConfig &other) const;


        virtual double signalFreq() const = 0;
        virtual int signalMinPoints() const = 0;
        virtual bool signalStrictPoints() const = 0;

        double signalRealFreq() const {return m_real_sig_freq;}
        double signalGenFreq() const {return m_real_pt_freq;}

        virtual void setRealParams(double sigFreq, double ptFreq);

        void load(const QJsonObject &sigObj);
        void save(QJsonObject &sigObj) const;

        static QString getClassName(const QJsonObject &sigObj);
        static QString getTypeName(const QJsonObject &sigObj);

        /* признак, что сигнал должен воспроизводится в периодическом режиме.
         * Для части сигналов может влиять на генерацию (однократную или
         * переодическую). В циклическом выводе принудительно устанавливается
         * периодический режим. */
        bool periodicMode() const {return m_periodic_mode;}
        void setPeriodicMode(bool periodic) {m_periodic_mode = periodic;}
    protected:
        SignalConfig(const SignalType &type);
        SignalConfig(const SignalConfig &other);

        virtual void protLoad(const QJsonObject &sigObj) = 0;
        virtual void protSave(QJsonObject &sigObj) const = 0;
        virtual bool protIsEqual(const SignalConfig &other) const = 0;
    private:
        const SignalType *m_type;
        bool m_periodic_mode;

        double m_real_sig_freq, m_real_pt_freq;
    };
}

#endif // SIGNALCONFIG_H
