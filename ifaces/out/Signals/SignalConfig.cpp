#include "SignalConfig.h"
#include "SignalType.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_signal_class           {"Class"};
    static const QLatin1String cfgkey_signal_type            {"SignalType"};
    static const QLatin1String cfgkey_periodic_mode          {"PeriodicMode"};

    SignalConfig::~SignalConfig() {

    }

    SignalGenerator *SignalConfig::createGenerator() const {
        return m_type->createGenerator(*this);
    }

    bool SignalConfig::isEqual(const SignalConfig &other) const {
        return (m_type == other.m_type) && protIsEqual(other);
    }

    void SignalConfig::setRealParams(double sigFreq, double ptFreq) {
        m_real_sig_freq = sigFreq;
        m_real_pt_freq = ptFreq;
    }

    void SignalConfig::load(const QJsonObject &sigObj) {        
        if ((getClassName(sigObj) == type().className()) &&
            (getTypeName(sigObj) == type().typeName())) {
            m_periodic_mode = sigObj[cfgkey_periodic_mode].toBool(true);
            protLoad(sigObj);
        }
    }

    void SignalConfig::save(QJsonObject &sigObj) const {
        sigObj[cfgkey_signal_class] = type().className();
        sigObj[cfgkey_signal_type] = type().typeName();
        sigObj[cfgkey_periodic_mode] = m_periodic_mode;
        return protSave(sigObj);
    }

    QString SignalConfig::getClassName(const QJsonObject &sigObj) {
        return sigObj[cfgkey_signal_class].toString();
    }

    QString SignalConfig::getTypeName(const QJsonObject &sigObj) {
        return sigObj[cfgkey_signal_type].toString();
    }

    SignalConfig::SignalConfig(const SignalType &type) :
        m_type{&type},
        m_periodic_mode{true},
        m_real_sig_freq{unspecifiedFreq()},
        m_real_pt_freq{unspecifiedFreq()} {

    }

    SignalConfig::SignalConfig(const SignalConfig &other) :
        m_type{other.m_type},
        m_periodic_mode{other.m_periodic_mode},
        m_real_sig_freq{other.m_real_sig_freq},
        m_real_pt_freq{other.m_real_pt_freq} {

    }
}
