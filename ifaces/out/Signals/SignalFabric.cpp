#include "SignalFabric.h"
#include "SignalType.h"
#include "SignalConfig.h"

#include "Analog/Const/AnalogSignalConst.h"
#include "Analog/Sin/AnalogSignalSin.h"
#include "Analog/Pulse/AnalogSignalPulse.h"
#include "Analog/Ramp/AnalogSignalRamp.h"
#include "Analog/MultiPoint/AnalogSignalMultiPoint.h"
#include "Analog/TimeSeq/AnalogSignalTimeSeq.h"
#include "Discrete/Const/DiscreteSignalConst.h"
#include "Discrete/Pulse/DiscreteSignalPulse.h"
#include "Discrete/SinglePulseBurst/DiscreteSignalSinglePulseBurst.h"
#include "Discrete/MultiPoint/DiscreteSignalMultiPoint.h"


namespace LQMeas {
    SignalFabric::SignalFabric() {
        addSignalType(&AnalogSignalConst::instance());
        addSignalType(&AnalogSignalSin::instance());
        addSignalType(&AnalogSignalPulse::instance());
        addSignalType(&AnalogSignalRamp::instance());
        addSignalType(&AnalogSignalMultiPoint::instance());
        addSignalType(&AnalogSignalTimeSeq::instance());

        addSignalType(&DiscreteSignalConst::instance());
        addSignalType(&DiscreteSignalPulse::instance());
        addSignalType(&DiscreteSignalSinglePulseBurst::instance());
        addSignalType(&DiscreteSignalMultiPoint::instance());
    }

    SignalFabric *SignalFabric::instance() {
        static SignalFabric fabric;
        return &fabric;
    }

    void SignalFabric::addSignalType(const SignalType *type) {
        if (!m_types.contains(type))
            m_types.append(type);
    }

    void SignalFabric::clearTypes() {
        m_types.clear();
    }

    QList<const SignalType *> SignalFabric::classTypeList(const QString &className) const {
        QList<const SignalType *> ret;
        for (const SignalType *type : m_types) {
            if (type->className() == className) {
                ret.append(type);
            }
        }
        return ret;
    }

    SignalConfig *SignalFabric::createConfig(const QJsonObject &sigObj) {
        SignalConfig *ret = nullptr;
        QString className = SignalConfig::getClassName(sigObj);
        QString typeName = SignalConfig::getTypeName(sigObj);
        for (const SignalType *type : m_types) {
            if ((type->className() == className) &&
                (type->typeName() == typeName)) {
                ret = type->createConfig();
                if (ret)
                    ret->load(sigObj);
                break;
            }
        }
        return ret;
    }
}
