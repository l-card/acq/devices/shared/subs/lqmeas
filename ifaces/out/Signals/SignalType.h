#ifndef LQMEAS_SIGNALTYPE_H
#define LQMEAS_SIGNALTYPE_H

#include <QString>
#include <QObject>

namespace LQMeas {
    class SignalConfig;
    class SignalGenerator;
    class SignalType : public QObject {
        Q_OBJECT
    public:
        virtual QString className() const = 0;
        virtual QString typeName() const = 0;
        virtual QString displayName() const = 0;
        virtual SignalConfig *createConfig() const = 0;
        virtual SignalGenerator *createGenerator(const SignalConfig &cfg) const = 0;
    };
}

#endif // LQMEAS_SIGNAL_H
