#include "DiscreteSignalConst.h"
#include "DiscreteSignalConstConfig.h"
#include "DiscreteSignalConstGenerator.h"

namespace LQMeas {
    const DiscreteSignalConst &DiscreteSignalConst::instance() {
        static const DiscreteSignalConst type;
        return type;
    }

    SignalConfig *DiscreteSignalConst::createConfig() const {
        return new DiscreteSignalConstConfig{};
    }

    DiscreteSignalGenerator *DiscreteSignalConst::createDiscreteGenerator(const SignalConfig &cfg) const {
        return new DiscreteSignalConstGenerator{static_cast<const DiscreteSignalConstConfig &>(cfg)};
    }
}
