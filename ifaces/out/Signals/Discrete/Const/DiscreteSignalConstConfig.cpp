#include "DiscreteSignalConstConfig.h"
#include "DiscreteSignalConst.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>


namespace LQMeas {
    static const QLatin1String cfgkey_value {StdConfigKeys::value()};

    DiscreteSignalConstConfig::DiscreteSignalConstConfig(bool val) :
        SignalConfig{DiscreteSignalConst::instance()}, m_val{val} {

    }

    DiscreteSignalConstConfig::DiscreteSignalConstConfig(const DiscreteSignalConstConfig &other) :
        SignalConfig{other} {
        m_val = other.m_val;
    }

    SignalConfig *DiscreteSignalConstConfig::clone() const {
        return new DiscreteSignalConstConfig{*this};
    }

    void DiscreteSignalConstConfig::setValue(bool val) {
        m_val = val;
    }

    void DiscreteSignalConstConfig::protLoad(const QJsonObject &sigObj) {
        m_val = sigObj[cfgkey_value].toBool();
    }

    void DiscreteSignalConstConfig::protSave(QJsonObject &sigObj) const {
        sigObj[cfgkey_value] = m_val;
    }

    bool DiscreteSignalConstConfig::protIsEqual(const SignalConfig &other) const {
        const DiscreteSignalConstConfig &otherSig
                = static_cast<const DiscreteSignalConstConfig &>(other);
        return m_val == otherSig.m_val;
    }
}
