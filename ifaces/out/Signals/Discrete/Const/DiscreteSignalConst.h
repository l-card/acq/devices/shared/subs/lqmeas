#ifndef DISCRETESIGNALCONST_H
#define DISCRETESIGNALCONST_H


#include "../DiscreteSignalType.h"


namespace LQMeas {
    class DiscreteSignalConst : public DiscreteSignalType {
        Q_OBJECT
    public:
        static const DiscreteSignalConst &instance();

        QString typeName() const override {return QStringLiteral("Const");}
        QString displayName() const override {return tr("Constant");}
        SignalConfig *createConfig() const override;
        DiscreteSignalGenerator *createDiscreteGenerator(const SignalConfig &cfg) const override;
    };
}

#endif // DISCRETESIGNALCONST_H
