#ifndef DISCRETESIGNALCONSTCONFIG_H
#define DISCRETESIGNALCONSTCONFIG_H

#include "../../SignalConfig.h"

namespace LQMeas {
    class DiscreteSignalConstConfig : public SignalConfig {
    public:
        DiscreteSignalConstConfig(bool val = 0);
        DiscreteSignalConstConfig(const DiscreteSignalConstConfig &cfg);

        SignalConfig *clone() const override;

        double signalFreq() const override {return unspecifiedFreq();}
        int signalMinPoints() const override {return 1;}
        bool signalStrictPoints() const override {return false;}

        void setValue(bool val);
        bool value() const {return m_val;}
    protected:
        void protLoad(const QJsonObject &sigObj) override;
        void protSave(QJsonObject &sigObj) const override;
        bool protIsEqual(const SignalConfig &other) const override;
    private:
        bool m_val;
    };
}

#endif // DISCRETESIGNALCONSTCONFIG_H
