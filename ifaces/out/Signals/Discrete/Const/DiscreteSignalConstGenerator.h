#ifndef DISCRETESIGNALCONSTGENERATOR_H
#define DISCRETESIGNALCONSTGENERATOR_H

#include "../DiscreteSignalGenerator.h"
#include "DiscreteSignalConstConfig.h"

namespace LQMeas {
    class DiscreteSignalConstGenerator : public DiscreteSignalGenerator{
    public:
        DiscreteSignalConstGenerator(const DiscreteSignalConstConfig &cfg);

        void signalGenInit() override {}
        bool signalGenNextPoint() override {return m_val;}
        qint64 totalGenPoints() const override {return 1;}
    private:
        bool m_val;
    };
}

#endif // DISCRETESIGNALCONSTGENERATOR_H
