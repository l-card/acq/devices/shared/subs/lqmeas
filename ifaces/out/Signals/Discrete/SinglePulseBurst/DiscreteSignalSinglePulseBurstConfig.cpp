#include "DiscreteSignalSinglePulseBurstConfig.h"
#include "DiscreteSignalSinglePulseBurst.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_freq                {StdConfigKeys::freq()};
    static const QLatin1String cfgkey_duty_perc           {"DutyPerc"};
    static const QLatin1String cfgkey_pulse_cnt           {"PulsesCount"};

    DiscreteSignalSinglePulseBurstConfig::DiscreteSignalSinglePulseBurstConfig(
            double freq, double duty, int pulses_cnt) :
        SignalConfig{DiscreteSignalSinglePulseBurst::instance()},
        m_freq{freq},
        m_duty{duty},
        m_pulses_cnt{pulses_cnt} {

    }

    DiscreteSignalSinglePulseBurstConfig::DiscreteSignalSinglePulseBurstConfig(
            const DiscreteSignalSinglePulseBurstConfig &other) :
        SignalConfig{other},
        m_freq{other.m_freq},
        m_duty{other.m_duty},
        m_pulses_cnt{other.m_pulses_cnt} {

    }

    SignalConfig *DiscreteSignalSinglePulseBurstConfig::clone() const {
        return new DiscreteSignalSinglePulseBurstConfig{*this};
    }

    unsigned DiscreteSignalSinglePulseBurstConfig::realPeriodPoints() const {
        return static_cast<unsigned>(signalGenFreq()/signalRealFreq() + 0.5);
    }

    unsigned DiscreteSignalSinglePulseBurstConfig::realDutyPoints() const {
        return static_cast<unsigned>(m_duty * realPeriodPoints() + 0.5);
    }

    double DiscreteSignalSinglePulseBurstConfig::realDutyCycle() const {
        return static_cast<double>(realDutyPoints())/realPeriodPoints();
    }

    void DiscreteSignalSinglePulseBurstConfig::protLoad(const QJsonObject &sigObj) {
        m_freq = sigObj[cfgkey_freq].toDouble();
        m_pulses_cnt = sigObj[cfgkey_pulse_cnt].toInt();
        m_duty = sigObj[cfgkey_duty_perc].toDouble();
    }

    void DiscreteSignalSinglePulseBurstConfig::protSave(QJsonObject &sigObj) const {
        sigObj[cfgkey_freq] = m_freq;
        sigObj[cfgkey_pulse_cnt] = m_pulses_cnt;
        sigObj[cfgkey_duty_perc] = m_duty;
    }

    bool DiscreteSignalSinglePulseBurstConfig::protIsEqual(const SignalConfig &other) const {
        const DiscreteSignalSinglePulseBurstConfig &otherSig
                = static_cast<const DiscreteSignalSinglePulseBurstConfig &>(other);
        return (m_freq == otherSig.m_freq) &&
                (m_pulses_cnt == otherSig.m_pulses_cnt) &&
                (m_duty == otherSig.m_duty);
    }
}


