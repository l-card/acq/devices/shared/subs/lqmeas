#include "DiscreteSignalSinglePulseBurst.h"
#include "DiscreteSignalSinglePulseBurstConfig.h"
#include "DiscreteSignalSinglePulseBurstGenerator.h"

namespace LQMeas {
    const DiscreteSignalSinglePulseBurst &DiscreteSignalSinglePulseBurst::instance() {
        static const DiscreteSignalSinglePulseBurst type;
        return type;
    }

    SignalConfig *DiscreteSignalSinglePulseBurst::createConfig() const  {
        return new DiscreteSignalSinglePulseBurstConfig{};
    }

    DiscreteSignalGenerator *DiscreteSignalSinglePulseBurst::createDiscreteGenerator(const SignalConfig &cfg) const {
        return new DiscreteSignalSinglePulseBurstGenerator{static_cast<const DiscreteSignalSinglePulseBurstConfig &>(cfg)};
    }
}
