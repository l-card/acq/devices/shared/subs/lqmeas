#ifndef DISCRETESIGNALSINGLEPULSEBURSTGENERATOR_H
#define DISCRETESIGNALSINGLEPULSEBURSTGENERATOR_H

#include "lqmeas/ifaces/out/Signals/Discrete/DiscreteSignalGenerator.h"
#include "DiscreteSignalSinglePulseBurstConfig.h"

namespace LQMeas {
    class DiscreteSignalSinglePulseBurstGenerator : public DiscreteSignalGenerator{
    public:
        DiscreteSignalSinglePulseBurstGenerator(const DiscreteSignalSinglePulseBurstConfig &cfg);

        void signalGenInit() {m_cur_pt = 0; m_cur_pulse = 0; }
        bool signalGenNextPoint();
        qint64 totalGenPoints() const {return m_pt_len * m_pulses_cnt;}
    private:
        int m_cur_pulse;
        unsigned m_cur_pt;

        int m_pulses_cnt;
        unsigned m_pt_len;
        unsigned m_pt_duty;
    };
}

#endif // DISCRETESIGNALSINGLEPULSEBURSTGENERATOR_H
