#ifndef DISCRETESIGNALSINGLEPULSEBURST_H
#define DISCRETESIGNALSINGLEPULSEBURST_H

#include "lqmeas/ifaces/out/Signals/Discrete/DiscreteSignalType.h"

namespace LQMeas {
    class DiscreteSignalSinglePulseBurst : public DiscreteSignalType  {
        Q_OBJECT
    public:
        static const DiscreteSignalSinglePulseBurst &instance();

        QString typeName() const override {return QStringLiteral("SinglePulseBurst");}
        QString displayName() const override {return tr("Pulse Burst");}
        SignalConfig *createConfig() const override;
        DiscreteSignalGenerator *createDiscreteGenerator(const SignalConfig &cfg) const override;
    };
}




#endif // DISCRETESIGNALSINGLEPULSEBURST_H
