#ifndef DISCRETESIGNALSINGLEPULSEBURSTCONFIG_H
#define DISCRETESIGNALSINGLEPULSEBURSTCONFIG_H

#include "lqmeas/ifaces/out/Signals/SignalConfig.h"

namespace LQMeas {
    class DiscreteSignalSinglePulseBurstConfig : public SignalConfig {
    public:
        DiscreteSignalSinglePulseBurstConfig(double freq = 100, double duty = 0.5, int pulses_cnt = 1);
        DiscreteSignalSinglePulseBurstConfig(const DiscreteSignalSinglePulseBurstConfig &other);

        SignalConfig *clone() const override;

        double signalFreq() const override {return m_freq;}
        int signalMinPoints() const override {return 2;}
        bool signalStrictPoints() const override {return true;}

        void setSignalFreq(double freq) {m_freq = freq;}

        void setDutyCycle(double d) {m_duty = d;}
        double dutyCycle() const {return m_duty;}

        void setPulsesCount(int cnt) {m_pulses_cnt = cnt;}
        int  pulsesCount() const {return m_pulses_cnt;}

        unsigned realPeriodPoints() const;
        unsigned realDutyPoints() const;
        double realDutyCycle() const;
    protected:
        void protLoad(const QJsonObject &sigObj) override;
        void protSave(QJsonObject &sigObj) const override;
        bool protIsEqual(const SignalConfig &other) const override;
    private:
        double m_freq;
        double m_duty;
        int m_pulses_cnt;
    };
}

#endif // DISCRETESIGNALSINGLEPULSEBURSTCONFIG_H
