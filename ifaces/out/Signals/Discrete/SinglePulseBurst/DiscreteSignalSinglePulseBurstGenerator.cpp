#include "DiscreteSignalSinglePulseBurstGenerator.h"

namespace LQMeas {
    DiscreteSignalSinglePulseBurstGenerator::DiscreteSignalSinglePulseBurstGenerator(
            const DiscreteSignalSinglePulseBurstConfig &cfg) :
        m_pulses_cnt{cfg.pulsesCount()},
        m_pt_len{cfg.realPeriodPoints()},
        m_pt_duty{cfg.realDutyPoints()} {

    }

    bool DiscreteSignalSinglePulseBurstGenerator::signalGenNextPoint() {
        bool val = false;
        if (m_cur_pulse < m_pulses_cnt) {
            val = m_cur_pt < m_pt_duty;
            if (++m_cur_pt == m_pt_len) {
                m_cur_pt = 0;
                m_cur_pulse++;
            }
        }
        return val;
    }
}
