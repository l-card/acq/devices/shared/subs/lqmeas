#ifndef LQMEAS_DISCRETESIGNALGENERATOR_H
#define LQMEAS_DISCRETESIGNALGENERATOR_H

#include "../SignalGenerator.h"

namespace LQMeas {
    class DiscreteSignalGenerator : public SignalGenerator {
    public:
        virtual bool signalGenNextPoint() = 0;
    };
}


#endif // LQMEAS_DISCRETESIGNALGENERATOR_H
