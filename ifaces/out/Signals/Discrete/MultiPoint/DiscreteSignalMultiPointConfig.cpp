#include "DiscreteSignalMultiPointConfig.h"
#include "DiscreteSignalMultiPoint.h"
#include <QJsonObject>
#include <QJsonArray>
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String cfgkey_points_array  {StdConfigKeys::points()};
    static const QLatin1String cfgkey_point_value   {StdConfigKeys::value()};
    static const QLatin1String cfgkey_point_time    {StdConfigKeys::timeMs()};

    DiscreteSignalMultiPointConfig::DiscreteSignalMultiPointConfig() :
        SignalConfig{DiscreteSignalMultiPoint::instance()} {

    }

    DiscreteSignalMultiPointConfig::DiscreteSignalMultiPointConfig(
            const DiscreteSignalMultiPointConfig &other) :
        SignalConfig{other} {

        for (Point *pt : other.m_points) {
            m_points.append(new Point{*pt});
        }
    }

    DiscreteSignalMultiPointConfig::~DiscreteSignalMultiPointConfig() {
        clear();
    }

    SignalConfig *DiscreteSignalMultiPointConfig::clone() const {
        return new DiscreteSignalMultiPointConfig{*this};
    }

    double DiscreteSignalMultiPointConfig::signalFreq() const {
        return  m_points.isEmpty() ? unspecifiedFreq() : 1000./m_points.last()->timeMs();
    }

    void DiscreteSignalMultiPointConfig::clear() {
        QList<Point *> points {m_points};
        m_points.clear();
        /* посылаем сигнал после очистка списка, но перед удалением элементов,
         * чтобы все получатели могли удалить ссылки */
        Q_EMIT pointListReconstructed();
        qDeleteAll(points);
    }

    int DiscreteSignalMultiPointConfig::pointPos(const DiscreteSignalMultiPointConfig::Point *pt) const {
        int ret = -1;
        for (int i = 0; i < m_points.size(); ++i) {
            const Point *checkPt = m_points.at(i);
            if (checkPt == pt) {
                ret = i;
                break;
            }
        }
        return ret;
    }

    int DiscreteSignalMultiPointConfig::addPoint(const DiscreteSignalMultiPointConfig::Point &pt) {
        const int pos {getNewPointIdx(pt.timeMs())};
        Point *newPt {new Point{pt}};
        m_points.insert(pos, newPt);
        Q_EMIT pointAppend(newPt, pos);
        return pos;
    }

    int DiscreteSignalMultiPointConfig::addPoint(double time_ms, bool val) {
        return addPoint(Point(time_ms, val));
    }

    void DiscreteSignalMultiPointConfig::setPointValue(int pos, bool val) {
        Point *pt = m_points.at(pos);
        pt->setVal(val);
        Q_EMIT pointConfigChanged(pt);
    }

    void DiscreteSignalMultiPointConfig::setPointTime(int pos, double time) {
        int new_pos = getNewPointIdx(time, pos);
        if (new_pos != pos) {
            Point *pt = m_points.takeAt(pos);
            pt->setTimeMs(time);
            m_points.insert(new_pos, pt);
            Q_EMIT pointMoved(pt);
            Q_EMIT pointConfigChanged(pt);
        } else {
            m_points.at(pos)->setTimeMs(time);
        }
    }

    void DiscreteSignalMultiPointConfig::addPointsList(QList<DiscreteSignalMultiPointConfig::Point *> pts) {
        m_points.append(pts);
        std::sort(m_points.begin(), m_points.end(), pointsSortCmp);
        Q_EMIT pointListReconstructed();
    }

    void DiscreteSignalMultiPointConfig::removePoint(int pos) {
        Point *pt = m_points.takeAt(pos);
        Q_EMIT pointRemoved(pt);
        delete  pt;
    }

    void DiscreteSignalMultiPointConfig::protLoad(const QJsonObject &sigObj) {
        clear();
        QList<Point *> newPoints;
        const QJsonArray &pointsArray = sigObj[cfgkey_points_array].toArray();
        for (QJsonArray::const_iterator it {pointsArray.constBegin()}; it < pointsArray.constEnd(); ++it) {
            const QJsonObject &ptObj {(*it).toObject()};
            const double time_ms {ptObj[cfgkey_point_time].toDouble()};
            const bool val {ptObj[cfgkey_point_value].toBool()};
            newPoints.append(new Point{time_ms, val});
        }
        addPointsList(newPoints);
    }

    void DiscreteSignalMultiPointConfig::protSave(QJsonObject &sigObj) const {
        QJsonArray pointsArray;
        for (Point *pt : m_points) {
            QJsonObject ptObj;
            ptObj[cfgkey_point_time] = pt->timeMs();
            ptObj[cfgkey_point_value] = pt->value();
            pointsArray.append(ptObj);
        }
        sigObj[cfgkey_points_array] = pointsArray;
    }

    bool DiscreteSignalMultiPointConfig::protIsEqual(const SignalConfig &other) const {
        const DiscreteSignalMultiPointConfig &otherSig {
                static_cast<const DiscreteSignalMultiPointConfig &>(other)};
        bool eq {(periodicMode() == otherSig.periodicMode()) && (m_points.size() == otherSig.m_points.size())};
        for (int pt_idx {0}; eq && (pt_idx < m_points.size()); ++pt_idx) {
            const Point *pt {m_points.at(pt_idx)};
            const Point *otherPt {otherSig.m_points.at(pt_idx)};
            eq = (pt->value() == otherPt->value()) && LQREAL_IS_EQUAL(pt->timeMs(), otherPt->timeMs());
        }
        return eq;
    }

    int DiscreteSignalMultiPointConfig::getNewPointIdx(double time_ms, int skip_pos) const {
        int pos = 0;
        for (int i = 0; i < m_points.size(); ++i) {
            if (i != skip_pos) {
                const Point *pt {m_points.at(i)};
                if (pt->timeMs() < time_ms) {
                    ++pos;
                } else {
                    break;
                }
            }
        }
        return pos;
    }

    bool DiscreteSignalMultiPointConfig::pointsSortCmp(const DiscreteSignalMultiPointConfig::Point *p1,
                                                               const DiscreteSignalMultiPointConfig::Point *p2) {
        return  p1->timeMs() < p2->timeMs();
    }
}
