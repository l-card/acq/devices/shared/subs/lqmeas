#ifndef DISCRETESIGNALMULTIPOINTGENERATOR_H
#define DISCRETESIGNALMULTIPOINTGENERATOR_H


#include "../DiscreteSignalGenerator.h"
#include "DiscreteSignalMultiPointConfig.h"

namespace LQMeas {
    class DiscreteSignalMultiPointGenerator : public DiscreteSignalGenerator{
    public:
        DiscreteSignalMultiPointGenerator(const DiscreteSignalMultiPointConfig &cfg);

        void signalGenInit() override;
        bool signalGenNextPoint() override;
        qint64 totalGenPoints() const override;
    private:
        void resetTime();

        struct CfgPtParam {
            CfgPtParam(qint64 iidx, bool ival) : pt_idx{iidx}, val{ival} {}

            qint64 pt_idx;
            bool val;
        };


        qint64 m_cur_pt_idx;
        qint64 m_next_pt_change;
        bool m_cur_value;

        int m_cur_cfg_next_pt_num;
        QList<CfgPtParam> m_cfg_pts;
        bool m_periodic;
    };
}


#endif // DISCRETESIGNALMULTIPOINTGENERATOR_H
