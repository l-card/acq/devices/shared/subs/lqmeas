#include "DiscreteSignalMultiPointGenerator.h"

namespace LQMeas {
    DiscreteSignalMultiPointGenerator::DiscreteSignalMultiPointGenerator(const DiscreteSignalMultiPointConfig &cfg) {
        for (int pt_idx = 0; pt_idx < cfg.pointsCount(); ++pt_idx) {
            const DiscreteSignalMultiPointConfig::Point &pt = cfg.point(pt_idx);
            qint64 get_pt_idx = static_cast<qint64>(cfg.signalGenFreq() * pt.timeMs() / 1000 + 0.5);
            m_cfg_pts.append(CfgPtParam{get_pt_idx, pt.value()});
        }
        m_periodic = cfg.periodicMode();
    }

    void DiscreteSignalMultiPointGenerator::signalGenInit()     {
        resetTime();
    }

    bool DiscreteSignalMultiPointGenerator::signalGenNextPoint() {
        while ((m_next_pt_change >= 0) && (m_cur_pt_idx >= m_next_pt_change)) {
            m_cur_value = m_cfg_pts.at(m_cur_cfg_next_pt_num).val;
            ++m_cur_cfg_next_pt_num;
            m_next_pt_change = m_cur_cfg_next_pt_num < m_cfg_pts.size() ?
                        m_cfg_pts.at(m_cur_cfg_next_pt_num).pt_idx : -1;
            if (m_periodic && (m_next_pt_change < 0)) {
                resetTime();
            }
        }
        ++m_cur_pt_idx;
        return m_cur_value;
    }

    qint64 DiscreteSignalMultiPointGenerator::totalGenPoints() const {
        return m_periodic ? -1 : m_cfg_pts.last().pt_idx + 1;
    }

    void DiscreteSignalMultiPointGenerator::resetTime()     {
        m_cur_pt_idx = 0;
        m_cur_cfg_next_pt_num = 0;
        m_cur_value = false;

        m_next_pt_change = m_cfg_pts.isEmpty() ? -1 : m_cfg_pts.at(0).pt_idx;
    }
}
