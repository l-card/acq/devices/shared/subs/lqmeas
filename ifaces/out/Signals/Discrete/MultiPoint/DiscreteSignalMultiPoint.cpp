#include "DiscreteSignalMultiPoint.h"
#include "DiscreteSignalMultiPointConfig.h"
#include "DiscreteSignalMultiPointGenerator.h"
namespace LQMeas {
    const DiscreteSignalMultiPoint &DiscreteSignalMultiPoint::instance() {
        static const DiscreteSignalMultiPoint type;
        return type;
    }

    SignalConfig *DiscreteSignalMultiPoint::createConfig() const  {
        return new DiscreteSignalMultiPointConfig{};
    }

    DiscreteSignalGenerator *DiscreteSignalMultiPoint::createDiscreteGenerator(const SignalConfig &cfg) const {
        return new DiscreteSignalMultiPointGenerator{static_cast<const DiscreteSignalMultiPointConfig &>(cfg)};
    }
}
