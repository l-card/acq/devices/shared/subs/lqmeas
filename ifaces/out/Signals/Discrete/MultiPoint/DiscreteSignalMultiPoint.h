#ifndef DISCRETESIGNALMULTIPOINT_H
#define DISCRETESIGNALMULTIPOINT_H


#include "../DiscreteSignalType.h"

namespace LQMeas {
    class DiscreteSignalMultiPoint : public DiscreteSignalType {
        Q_OBJECT
    public:
        static const DiscreteSignalMultiPoint &instance();

        QString typeName() const override {return QStringLiteral("MultiPoint");}
        QString displayName() const override {return tr("Multiple Point Based");}
        SignalConfig *createConfig() const override;
        DiscreteSignalGenerator *createDiscreteGenerator(const SignalConfig &cfg) const override;
    };
}



#endif // DISCRETESIGNALMULTIPOINT_H
