#ifndef DISCRETESIGNALMULTIPOINTCONFIG_H
#define DISCRETESIGNALMULTIPOINTCONFIG_H


#include "../../SignalConfig.h"
#include <QList>

namespace LQMeas {
    class DiscreteSignalMultiPointConfig : public SignalConfig {
        Q_OBJECT
    public:
        struct Point {
        public:
            Point(double time_ms, bool val) : m_time_ms(time_ms), m_val(val) {}
            Point(const Point &other) : m_time_ms(other.m_time_ms), m_val(other.m_val) {}

            double timeMs() const {return m_time_ms;}
            bool value() const {return m_val;}

            void setTimeMs(double ms) {m_time_ms = ms;}
            void setVal(bool val) {m_val = val;}

        private:
            double m_time_ms;
            bool m_val;
        };


        DiscreteSignalMultiPointConfig();
        DiscreteSignalMultiPointConfig(const DiscreteSignalMultiPointConfig &other);
        ~DiscreteSignalMultiPointConfig() override;

        SignalConfig *clone() const override;

        double signalFreq() const override;
        int signalMinPoints() const override {return 1;}
        bool signalStrictPoints() const override {return true;}


        void clear();

        const Point &point(int idx) const {return *m_points.at(idx);}
        int pointsCount() const {return m_points.size();}
        int pointPos(const Point *pt) const;

        int addPoint(const Point &pt);
        int addPoint(double time_ms, bool val);
        void setPointValue(int pos, bool val);
        void setPointTime(int pos, double time);
        void addPointsList(QList<Point *> pts);
        void removePoint(int pos);
    Q_SIGNALS:
        void pointAppend(const LQMeas::DiscreteSignalMultiPointConfig::Point *pt, int idx);
        void pointRemoved(const LQMeas::DiscreteSignalMultiPointConfig::Point *pt);
        void pointMoved(const LQMeas::DiscreteSignalMultiPointConfig::Point *pt);
        void pointConfigChanged(const LQMeas::DiscreteSignalMultiPointConfig::Point *pt);
        void pointListReconstructed();
    protected:
        void protLoad(const QJsonObject &sigObj) override;
        void protSave(QJsonObject &sigObj) const override;
        bool protIsEqual(const SignalConfig &other) const override;
    private:
        int getNewPointIdx(double time_ms, int skip_pos = -1) const;
        static bool pointsSortCmp(const Point *p1, const Point *p2);
        QList<Point *> m_points;
    };
}

#endif // DISCRETESIGNALMULTIPOINTCONFIG_H
