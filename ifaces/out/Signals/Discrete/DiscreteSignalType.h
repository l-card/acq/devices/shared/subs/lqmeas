#ifndef LQMEAS_DISCRETESIGNALTYPE_H
#define LQMEAS_DISCRETESIGNALTYPE_H

#include "../SignalType.h"
#include "DiscreteSignalGenerator.h"

namespace LQMeas {
    class DiscreteSignalType : public SignalType {
        Q_OBJECT
    public:
        static QString typeClassName() {return QStringLiteral("Discrete");}

        virtual DiscreteSignalGenerator *createDiscreteGenerator(const SignalConfig &cfg) const = 0;

        QString className() const override {return typeClassName();}
        SignalGenerator *createGenerator(const SignalConfig &cfg) const override {
            return createDiscreteGenerator(cfg);
        }
    };
}

#endif // LQMEAS_DISCRETESIGNALTYPE_H
