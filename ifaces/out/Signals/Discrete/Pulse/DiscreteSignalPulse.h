#ifndef LQMEAS_DISCRETESIGNALPULSE_H
#define LQMEAS_DISCRETESIGNALPULSE_H

#include "../DiscreteSignalType.h"

namespace LQMeas {
    class DiscreteSignalPulse : public DiscreteSignalType {
        Q_OBJECT
    public:
        static const DiscreteSignalPulse &instance();

        QString typeName() const override {return QStringLiteral("Pulse");}
        QString displayName() const override {return tr("Pulse");}
        SignalConfig *createConfig() const override;
        DiscreteSignalGenerator *createDiscreteGenerator(const SignalConfig &cfg) const override;
    };
}

#endif // LQMEAS_DISCRETESIGNALPULSE_H
