#include "DiscreteSignalPulseGenerator.h"

namespace LQMeas {

    DiscreteSignalPulseGenerator::DiscreteSignalPulseGenerator(const DiscreteSignalPulseConfig &cfg) :
        m_pt_len{cfg.realPeriodPoints()},
        m_pt_duty{cfg.realDutyPoints()} {

    }

    bool DiscreteSignalPulseGenerator::signalGenNextPoint() {
        bool val = m_cur_pt < m_pt_duty;
        if (++m_cur_pt == m_pt_len)
            m_cur_pt = 0;
        return val;
    }
}
