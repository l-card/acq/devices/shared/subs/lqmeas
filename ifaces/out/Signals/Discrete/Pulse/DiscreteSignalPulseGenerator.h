#ifndef LQMEAS_DISCRETESIGNALPULSEGENERATOR_H
#define LQMEAS_DISCRETESIGNALPULSEGENERATOR_H

#include "../DiscreteSignalGenerator.h"
#include "DiscreteSignalPulseConfig.h"

namespace LQMeas {
    class DiscreteSignalPulseGenerator : public DiscreteSignalGenerator{
    public:
        DiscreteSignalPulseGenerator(const DiscreteSignalPulseConfig &cfg);

        void signalGenInit() override {m_cur_pt = 0; }
        bool signalGenNextPoint() override;
    private:
        unsigned m_cur_pt;
        unsigned m_pt_len;
        unsigned m_pt_duty;
    };
}

#endif // LQMEAS_DISCRETESIGNALPULSEGENERATOR_H
