#ifndef LQMEAS_DISCRETESIGNALPULSECONFIG_H
#define LQMEAS_DISCRETESIGNALPULSECONFIG_H


#include "../../SignalConfig.h"

namespace LQMeas {
    class DiscreteSignalPulseConfig : public SignalConfig {
    public:
        DiscreteSignalPulseConfig(double freq = 0, double dutyCycle = 0);
        DiscreteSignalPulseConfig(const DiscreteSignalPulseConfig &other);

        SignalConfig *clone() const override;

        double signalFreq() const override {return m_freq;}
        int signalMinPoints() const override {return 2;}
        bool signalStrictPoints() const override {return true;}

        void setSignalFreq(double freq) {m_freq = freq;}

        void setDutyCycle(double d) {m_duty = d;}
        double dutyCycle() const {return m_duty;}

        unsigned realPeriodPoints() const;
        unsigned realDutyPoints() const;
        double realDutyCycle() const;
    protected:
        void protLoad(const QJsonObject &sigObj) override;
        void protSave(QJsonObject &sigObj) const override;
        bool protIsEqual(const SignalConfig &other) const override;
    private:
        double m_freq;
        double m_duty;
    };
}

#endif // LQMEAS_DISCRETESIGNALPULSECONFIG_H
