#include "DiscreteSignalPulse.h"
#include "DiscreteSignalPulseConfig.h"
#include "DiscreteSignalPulseGenerator.h"
namespace LQMeas {
    const DiscreteSignalPulse &DiscreteSignalPulse::instance() {
        static const DiscreteSignalPulse type;
        return type;
    }

    SignalConfig *DiscreteSignalPulse::createConfig() const  {
        return new DiscreteSignalPulseConfig{};
    }

    DiscreteSignalGenerator *DiscreteSignalPulse::createDiscreteGenerator(const SignalConfig &cfg) const {
        return new DiscreteSignalPulseGenerator{static_cast<const DiscreteSignalPulseConfig &>(cfg)};
    }
}
