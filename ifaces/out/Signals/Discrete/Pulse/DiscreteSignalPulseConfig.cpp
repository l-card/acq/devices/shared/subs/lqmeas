#include "DiscreteSignalPulseConfig.h"
#include "DiscreteSignalPulse.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_freq                {StdConfigKeys::freq()};
    static const QLatin1String cfgkey_duty_perc           {"DutyPerc"};

    DiscreteSignalPulseConfig::DiscreteSignalPulseConfig(double freq, double duty) :
        SignalConfig{DiscreteSignalPulse::instance()},
        m_freq{freq},
        m_duty{duty} {

    }

    DiscreteSignalPulseConfig::DiscreteSignalPulseConfig(const DiscreteSignalPulseConfig &other) :
        SignalConfig{other},
        m_freq{other.m_freq},
        m_duty{other.m_duty} {

    }

    SignalConfig *DiscreteSignalPulseConfig::clone() const {
        return new DiscreteSignalPulseConfig{*this};
    }

    unsigned DiscreteSignalPulseConfig::realPeriodPoints() const {
        return static_cast<unsigned>(signalGenFreq()/signalRealFreq() + 0.5);
    }

    unsigned DiscreteSignalPulseConfig::realDutyPoints() const {
        return static_cast<unsigned>(m_duty * realPeriodPoints() + 0.5);
    }

    double DiscreteSignalPulseConfig::realDutyCycle() const {
        return static_cast<double>(realDutyPoints())/realPeriodPoints();
    }

    void DiscreteSignalPulseConfig::protLoad(const QJsonObject &sigObj) {
        m_freq = sigObj[cfgkey_freq].toDouble();
        m_duty = sigObj[cfgkey_duty_perc].toDouble();
    }

    void DiscreteSignalPulseConfig::protSave(QJsonObject &sigObj) const {
        sigObj[cfgkey_freq] = m_freq;
        sigObj[cfgkey_duty_perc] = m_duty;
    }

    bool DiscreteSignalPulseConfig::protIsEqual(const SignalConfig &other) const  {
        const DiscreteSignalPulseConfig &otherSig
                = static_cast<const DiscreteSignalPulseConfig &>(other);
        return (m_freq == otherSig.m_freq) &&
                (m_duty == otherSig.m_duty);
    }
}
