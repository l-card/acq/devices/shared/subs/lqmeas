#include "DevOutConfig.h"
#include <QJsonObject>
#include <QJsonArray>

#include "DevOutInfo.h"
#include "DevOutSyncConfig.h"
#include "lqmeas/devs/DeviceTypeInfo.h"

namespace LQMeas {
    DevOutConfig::DevOutConfig(const DevOutInfo &devinfo) : m_out_info{&devinfo} {

    }

    DevOutConfig::DevOutConfig(const DevOutConfig &cfg) : m_out_info{cfg.m_out_info} {
    }

    DevOutConfig::~DevOutConfig() {

    }

    double DevOutConfig::outDacChRangeMaxVal(int ch) const {
        return outInfo().outDacChRangeMaxVal(ch, outDacChRangeNum(ch));
    }

    double DevOutConfig::outDacChRangeMinVal(int ch) const {
        return outInfo().outDacChRangeMinVal(ch, outDacChRangeNum(ch));
    }

    const Unit &DevOutConfig::outDacChValUnit(int ch) const {
        return outInfo().outDacChUnit(ch);
    }

    bool DevOutConfig::outDacChAvailable(int ch) const {
        return outInfo().outDacChAvailable(ch);
    }

    bool DevOutConfig::outDigChAvailable(int ch) const {
        return outInfo().outDigChAvailable(ch);
    }

    void DevOutConfig::protUpdateInfo(const DeviceTypeInfo *info) {
        DeviceConfigIface::protUpdateInfo(info);
        m_out_info = info->out();
    }

    void DevOutConfig::notifyDacChRangeChanged(int ch) {
        Q_EMIT outDacChRangeChanged(ch, outDacChRangeMinVal(ch), outDacChRangeMaxVal(ch));
        Q_EMIT outDacChConfigChanged(ch);
    }

    void DevOutConfig::notifyDacChConfigChanged(int ch) {
        Q_EMIT outDacChConfigChanged(ch);
    }

    void DevOutConfig::notifyDigChConfigChanged(int ch) {
        Q_EMIT outDigChConfigChanged(ch);
    }

    void DevOutConfig::notifyDacChSyncEnableStateChanged(int ch) {
        Q_EMIT outDacChSyncEnableStateChanged(ch, outDacChSyncModeEnabled(ch));
        Q_EMIT outDacChConfigChanged(ch);
    }

    void DevOutConfig::notifyDigChSyncEnableStateChanged(int ch) {
        Q_EMIT outDigChSyncEnableStateChanged(ch, outDacChSyncModeEnabled(ch));
        Q_EMIT outDigChConfigChanged(ch);
    }
}
