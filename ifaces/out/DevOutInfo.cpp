#include "DevOutInfo.h"
#include "lqmeas/units/std/Voltage.h"

namespace LQMeas {
    DevOutInfo::~DevOutInfo() {

    }

    const LQMeas::Unit &LQMeas::DevOutInfo::outDacChUnit(int ch) const {
         Q_UNUSED(ch);
        return Units::Voltage::V();
    }
}
