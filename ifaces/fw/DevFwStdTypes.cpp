#include "DevFwStdTypes.h"

namespace LQMeas {
    const QString &DevFwStdTypes::fpga() {
        static const QString t {QStringLiteral("FPGA")};
        return t;
    }

    const QString &DevFwStdTypes::mcu() {
        static const QString t {QStringLiteral("MCU")};
        return t;
    }
}
