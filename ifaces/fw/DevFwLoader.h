#ifndef LQMEAS_DEVFWLOADER_H
#define LQMEAS_DEVFWLOADER_H

#include <QStringList>
class LQError;

namespace LQMeas {
    class DevFwLoadProgressIndicator;

    class DevFwLoader {
    public:
        virtual ~DevFwLoader();
        virtual QStringList devFwTypes() const = 0;
        virtual bool devFwIsLoaded(const QString &type, LQError &err) const = 0;
        virtual void devFwLoad(const QString &type, const QString &filename, DevFwLoadProgressIndicator *loadIndicator, LQError &err) = 0;
    };
}

#endif // DEVFWLOADER_H
