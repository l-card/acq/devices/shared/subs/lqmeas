#ifndef DEVFWSTDTYPES_H
#define DEVFWSTDTYPES_H

#include <QString>

namespace LQMeas {
    class DevFwStdTypes {
    public:
        static const QString &fpga();
        static const QString &mcu();
    };
}

#endif // DEVFWSTDTYPES_H
