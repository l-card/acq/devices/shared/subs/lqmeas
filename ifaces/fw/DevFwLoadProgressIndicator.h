#ifndef DEVFWLOADPROGRESSINDICATOR_H
#define DEVFWLOADPROGRESSINDICATOR_H

#include <QtGlobal>


namespace LQMeas {
    class Device;
    class DevFwLoadProgressIndicator {
    public:
        virtual ~DevFwLoadProgressIndicator();
        virtual void devFwLoadProgressIndication(Device &dev, qint64 done, qint64 total, const QString &stage) = 0;
    };
}

#endif // DEVFWLOADPROGRESSINDICATOR_H
