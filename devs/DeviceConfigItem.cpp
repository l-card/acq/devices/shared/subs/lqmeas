#include "DeviceConfigItem.h"
#include "lqmeas/lqtdefs.h"
#include <QJsonObject>

namespace LQMeas {


    DeviceConfigItem::DeviceConfigItem(QObject *parent) : QObject{parent} {

    }

    void DeviceConfigItem::save(QJsonObject &cfgObj) const {
        protSave(cfgObj);
    }

    void DeviceConfigItem::load(const QJsonObject &cfgObj) {
        protLoad(cfgObj);
        notifyConfigChanged();
    }

    void DeviceConfigItem::update() {
        protUpdate();
        Q_EMIT updated();
    }

    void DeviceConfigItem::addSubItem(DeviceConfigItem *item) {
        connect(item, &DeviceConfigItem::configChanged, this, &DeviceConfigItem::notifyConfigChanged);
        m_subItems.append(item);
    }

    void DeviceConfigItem::remSubItem(DeviceConfigItem *item) {
        disconnect(item, &DeviceConfigItem::configChanged, this, &DeviceConfigItem::notifyConfigChanged);
        m_subItems.removeAll(item);
    }

    void DeviceConfigItem::notifyConfigChanged() {
        Q_EMIT configChanged();
    }

    void DeviceConfigItem::protUpdate() {
        for (DeviceConfigItem *item : qAsConst(m_subItems)) {
            item->update();
        }
    }

    void DeviceConfigItem::protUpdateInfo(const DeviceTypeInfo *info) {
        for (DeviceConfigItem *item : qAsConst(m_subItems)) {
            item->protUpdateInfo(info);
        }
    }

    void DeviceConfigItem::protUpdateDevice(const Device *device) {
        for (DeviceConfigItem *item : qAsConst(m_subItems)) {
            item->protUpdateDevice(device);
        }
    }

    void DeviceConfigItem::saveSubItem(QJsonObject &curObj, const DeviceConfigItem &subItem, const QLatin1String &subkey) {
        QJsonObject subObj {curObj[subkey].toObject()};
        subItem.save(subObj);
        curObj[subkey] = subObj;
    }

    void DeviceConfigItem::loadSubItem(const QJsonObject &curObj, DeviceConfigItem &subItem, const QLatin1String &subkey) {
        const QJsonObject &subObj {curObj[subkey].toObject()};
        subItem.load(subObj);
    }
}
