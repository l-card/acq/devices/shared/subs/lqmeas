#ifndef LQMEAS_DEVICEVALIDATOR_H
#define LQMEAS_DEVICEVALIDATOR_H

#include <QtGlobal>

namespace LQMeas {
    class Device;

    class DeviceValidator  {
    public:
        virtual ~DeviceValidator();
        virtual bool deviceValid(const Device &device) const = 0;
    };

    class DeviceAllPassValidator : public DeviceValidator {
    public:
        bool deviceValid(const Device &device) const override {
            Q_UNUSED(device)
            return true;
        }
    };
}

#endif // LQMEAS_DEVICEVALIDATOR_H
