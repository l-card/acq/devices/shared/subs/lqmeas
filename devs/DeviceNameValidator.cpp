#include "DeviceNameValidator.h"
#include "Device.h"

namespace LQMeas {
    DeviceNameValidator::DeviceNameValidator(const QStringList &nameList) : m_devnames{nameList} {

    }

    bool DeviceNameValidator::deviceValid(const Device &device) const {
        return (m_devnames.contains(device.name()) || m_devnames.empty());
    }
}
