#ifndef LQMEAS_DEVICECONFIGOBJECT_H
#define LQMEAS_DEVICECONFIGOBJECT_H

#include <QObject>

class QJsonObject;

namespace LQMeas {
    class DeviceTypeInfo;
    class Device;

    class DeviceConfigItem : public QObject {
        Q_OBJECT
    public:
        explicit DeviceConfigItem(QObject *parent = nullptr);

        /* сохранение параметров конфигурации в объект JSON */
        void save(QJsonObject &cfgObj) const;
        /* загрузка параметров конфигурации из объекта JSON */
        void load(const QJsonObject &cfgObj);
    public Q_SLOTS:
        void update();
    Q_SIGNALS:
        void configChanged();
        void updated();
    protected Q_SLOTS:
        void notifyConfigChanged();
    protected:
        void addSubItem(DeviceConfigItem *item);
        void remSubItem(DeviceConfigItem *item);



        virtual void protSave(QJsonObject &cfgObj) const { Q_UNUSED(cfgObj) }
        virtual void protLoad(const QJsonObject &cfgObj) { Q_UNUSED(cfgObj) }
        virtual void protUpdate();
        virtual void protUpdateInfo(const DeviceTypeInfo *info);
        virtual void protUpdateDevice(const Device *device);

        static void saveSubItem(QJsonObject &curObj, const DeviceConfigItem &subItem, const QLatin1String &subkey);
        static void loadSubItem(const QJsonObject &curObj, DeviceConfigItem &subItem, const QLatin1String &subkey);
    private:
        QList<DeviceConfigItem *> m_subItems;
    };
}

#endif // LQMEAS_CONFIGOBJECT_H
