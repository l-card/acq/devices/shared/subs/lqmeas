#ifndef LQMEAS_DEVICETYPESTDSERIES_H
#define LQMEAS_DEVICETYPESTDSERIES_H

#include "DeviceTypeSeries.h"
#include <QObject>

namespace LQMeas {
    class DeviceTypeStdSeries : public QObject{
        Q_OBJECT
    public:
        static const DeviceTypeSeries &unknown();
        static const DeviceTypeSeries &emodules();
        static const DeviceTypeSeries &lboards();
        static const DeviceTypeSeries &ltr();
        static const DeviceTypeSeries &lvims();
        static const DeviceTypeSeries &vib4();
    };
}

#endif // LQMEAS_DEVICETYPESTDSERIES_H
