#include "DeviceConfig.h"
#include "Device.h"
#include "DeviceInfo.h"
#include "lqmeas/log/Log.h"
#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "lqmeas/ifaces/in/adc/icp/DevAdcICPConfig.h"
#include "lqmeas/ifaces/in/dig/DevInDigConfig.h"
#include "lqmeas/ifaces/in/cntr/DevInCntrConfig.h"
#include "lqmeas/ifaces/out/DevOutSyncConfig.h"
#include "lqmeas/ifaces/sync/DevSyncMarksGenConfig.h"
#include "lqmeas/ifaces/sync/DevSyncMarksRecvConfig.h"
#ifdef LQMEAS_DEV_VI_GROUP
#include "lqmeas/devs/vi/config/ViConfig.h"
#endif
#include "DeviceTypeInfo.h"
#include <QJsonObject>
#include <QStringBuilder>

namespace LQMeas {

    void DeviceConfig::update() {

        for (DeviceConfigIface *cfgIface : cfgIfaces()) {
            cfgIface->update();
        }
        protUpdate();
    }

    void DeviceConfig::onModuleIfaceChanged() {
        if (!m_load_progr) {
            notifyConfigChanged();
        }
    }

    void DeviceConfig::notifyConfigChanged() {
        Q_EMIT configChanged();
    }

    void DeviceConfig::addManualConfigIface(DeviceConfigIface *iface) {
        m_cfgIfaces += iface;
    }

    void DeviceConfig::init() {
        if (adcConfig())
            m_cfgIfaces += adcConfig();
        if (adcICPConfig())
            m_cfgIfaces += adcICPConfig();
        if (outConfig())
            m_cfgIfaces += outConfig();
        if (inDigConfig())
            m_cfgIfaces += inDigConfig();
        if (inCntrConfig())
            m_cfgIfaces += inCntrConfig();
        if (syncMarksGenConfig())
            m_cfgIfaces += syncMarksGenConfig();
        if (syncMarksRecvConfig())
            m_cfgIfaces += syncMarksRecvConfig();
#ifdef LQMEAS_DEV_VI_GROUP
        if (viConfig())
            m_cfgIfaces += viConfig();
#endif

        for (DeviceConfigIface *cfgIface : cfgIfaces()) {
            cfgIface->init(this);
            connect(cfgIface, &DeviceConfigIface::configChanged,
                    this, &DeviceConfig::onModuleIfaceChanged);
        }
    }

    DeviceConfig::~DeviceConfig() {

    }

    DeviceConfig::DeviceConfig() {

    }

    void DeviceConfig::save(QJsonObject &cfgObj) const {
        cfgObj.insert(configDevTypeKeyName(), configDevTypeName());
        protSave(cfgObj);
        for (DeviceConfigIface *cfgIface : cfgIfaces()) {
            QJsonObject ifaceObj;
            cfgIface->save(ifaceObj);
            cfgObj[cfgIface->cfgIfaceName()] = ifaceObj;
        }
    }

    void DeviceConfig::load(const QJsonObject &cfgObj) {
        m_load_progr = true;
        const QString &devName {cfgObj[configDevTypeKeyName()].toString()};

        if (devName == configDevTypeName()) {
            protLoad(cfgObj);
            for (DeviceConfigIface *cfgIface : cfgIfaces()) {
                cfgIface->load(cfgObj[cfgIface->cfgIfaceName()].toObject());
            }
            update();
        } else {
            LQMeasLog->report(LogLevel::Warning, Device::tr("Found configuration for invalid device type: expected %1, found %2")
                              .arg(configDevTypeName(), devName));
        }
        m_load_progr = false;
        notifyConfigChanged();
    }

    const QString &DeviceConfig::configDevTypeKeyName() {
        static const QString str {QStringLiteral("DevTypeName") };
        return str;
    }



    void DeviceConfig::reportLoadWarning(const QString &msg) const {
        LQMeasLog->report(LogLevel::Warning, Device::tr("Load config (device %1)").
                          arg(configDevTypeName()) % QStringLiteral(": ") % msg);
    }

    void DeviceConfig::updateDevTypeInfo(const DeviceTypeInfo *info) {
        protUpdateInfo(info);
        if (info) {
            for (DeviceConfigIface *cfgIface : cfgIfaces()) {
                cfgIface->protUpdateInfo(info);
            }
        }

    }

    void DeviceConfig::updateDevice(const Device *device) {
        updateDevTypeInfo(device ? &device->devInfo()->type() : nullptr);
        protUpdateDevice(device);
        for (DeviceConfigIface *cfgIface : cfgIfaces()) {
            cfgIface->protUpdateDevice(device);
        }
    }

    void DeviceConfig::setParentConfig(const DeviceConfig *parentCfg) {
        protUpdateParentConfig(parentCfg);
        m_parentConfig = parentCfg;
        for (DeviceConfigIface *cfgIface : cfgIfaces()) {
            cfgIface->protUpdateParentConfig(parentCfg);
        }
    }

    double DeviceConfig::inPeakSpeed() const {
        const DevAdcConfig *padcCfg {adcConfig()};
        const DevInDigConfig *inDigCfg {inDigConfig()};
        return (padcCfg ? padcCfg->adcChTransfRate() * padcCfg->adcSampleSize() * padcCfg->adcEnabledChCnt() : 0)
                + ((inDigCfg && inDigCfg->inDigSyncModeEnabled()) ? inDigCfg->inDigSampleSize() * inDigCfg->inDigFreq() : 0) ;
    }

    double DeviceConfig::outPeakSpeed() const {
        double ret {0};
        const DevOutSyncConfig *outSynCfg {outSyncConfig()};
        if (outSynCfg) {
            for (int ch_num {0}; ch_num < outSynCfg->outSyncDacChannelsCnt(); ++ch_num) {
                if ((outSynCfg->outSyncDacChEnabled(ch_num) &&
                     outSynCfg->outSyncDacChGenMode(ch_num) == DevOutSyncConfig::OutSyncChGenMode::Ram)) {
                    ret += outSynCfg->outSyncGenFreq() * outSynCfg->outSyncDacSampleSize();
                }
            }

            bool has_dig_sig {false};
            for (int ch_num {0}; (ch_num < outSynCfg->outSyncDigChannelsCnt()) && !has_dig_sig; ++ch_num) {
                if ((outSynCfg->outSyncDigChEnabled(ch_num) &&
                     outSynCfg->outSyncDigChGenMode(ch_num) == DevOutSyncConfig::OutSyncChGenMode::Ram)) {
                    has_dig_sig = true;
                }
            }
            if (has_dig_sig) {
                ret += outSynCfg->outSyncGenFreq() * outSynCfg->outSyncDigSampleSize();
            }
        }
        return ret;
    }

}
