#ifndef LQMEAS_DEVICENAMEVALIDATOR_H
#define LQMEAS_DEVICENAMEVALIDATOR_H

#include "DeviceValidator.h"
#include <QStringList>


namespace LQMeas {
    class DeviceNameValidator : public DeviceValidator {
    public:
        DeviceNameValidator(const QStringList &nameList);

        bool deviceValid(const Device &device) const override;
    private:
        QStringList m_devnames;
    };
}

#endif // LQMEAS_DEVICENAMEVALIDATOR_H
