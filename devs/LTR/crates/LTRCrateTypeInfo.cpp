#include "LTRCrateTypeInfo.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarks.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/devs/DeviceTypeStdSeries.h"

namespace LQMeas {
    const LTRCrateTypeInfo &LTRCrateTypeInfo::typeInfoU8_16() {
        static const LTRCrateTypeInfo info{LTR_CRATE_TYPE_LTR010, QStringLiteral("LTR-U-8/16"), 16, false};
        return info;
    }

    const LTRCrateTypeInfo &LTRCrateTypeInfo::typeInfoU1() {
        static const LTRCrateTypeInfo info{LTR_CRATE_TYPE_LTR021, QStringLiteral("LTR-U-1"), 1, false};
        return info;
    }

    const LTRCrateTypeInfo &LTRCrateTypeInfo::typeInfoEU8_16() {
        static const LTRCrateTypeInfo info{LTR_CRATE_TYPE_LTR030, QStringLiteral("LTR-EU-8/16"), 16};
        return info;
    }

    const LTRCrateTypeInfo &LTRCrateTypeInfo::typeInfoEU2() {
        static const LTRCrateTypeInfo info{LTR_CRATE_TYPE_LTR031, QStringLiteral("LTR-EU-2"), 2};
        return info;
    }

    const LTRCrateTypeInfo &LTRCrateTypeInfo::typeInfoE7_15() {
        static const LTRCrateTypeInfo info{LTR_CRATE_TYPE_LTR032, QStringLiteral("LTR-E-7/15"), 16};
        return info;
    }

    const LTRCrateTypeInfo &LTRCrateTypeInfo::typeInfoCU1() {
        static const LTRCrateTypeInfo info{LTR_CRATE_TYPE_LTR_CU_1, QStringLiteral("LTR-CU-1"), 1};
        return info;
    }

    const LTRCrateTypeInfo &LTRCrateTypeInfo::typeInfoCEU1() {
        static const LTRCrateTypeInfo info{LTR_CRATE_TYPE_LTR_CEU_1, QStringLiteral("LTR-CEU-1"), 1};
        return info;
    }
    
    const LTRCrateTypeInfo &LTRCrateTypeInfo::defaultTypeInfo() {
        static const LTRCrateTypeInfo info{LTR_CRATE_TYPE_UNKNOWN, QStringLiteral("Unknown"), 16};
        return info;
    }

    const LTRCrateTypeInfo *LTRCrateTypeInfo::typeInfo(en_LTR_CrateTypes crate_type) {
        const LTRCrateTypeInfo *ret {nullptr};
        const QList<const LTRCrateTypeInfo *> &crateTypes {LTRCrateTypeInfo::types()};
        for (const LTRCrateTypeInfo *type : crateTypes) {
            if (type->crateType() == crate_type) {
                ret = type;
                break;
            }
        }

        if (!ret) {
            ret = &LTRCrateTypeInfo::defaultTypeInfo();
        }
        return ret;
    }

    const QList<const LTRCrateTypeInfo *> &LTRCrateTypeInfo::types() {
        static const QList<const LTRCrateTypeInfo*> list {
            &typeInfoEU8_16(),
            &typeInfoEU2(),
            &typeInfoCEU1(),
            &typeInfoCU1(),
            &typeInfoU8_16(),
            &typeInfoU1(),
            &typeInfoE7_15()
        };
        return list;
    }

    const DeviceTypeSeries &LTRCrateTypeInfo::deviceTypeSeries() const {
        return DeviceTypeStdSeries::ltr();
    }

    QList<const DevSyncMarkType *> LTRCrateTypeInfo::syncMarkGenTypes() const {
        static const QList<const DevSyncMarkType *> list {
            &LTRSyncMarkTypes::start(),
            &LTRSyncMarkTypes::second()
        };
        return list;
    }

    QList<const DeviceTypeInfo *> LTRCrateTypeInfo::modificationList() const  {
        QList<const DeviceTypeInfo *> ret;
        const QList<const LTRCrateTypeInfo*> &crate_types = types();
        for (const LTRCrateTypeInfo* type : crate_types) {
            ret.append(type);
        }
        return ret;
    }

    QList<const DeviceInterface *> LTRCrateTypeInfo::supportedConInterfaces() const {
        static const QList<const DeviceInterface *> usb_eth {
            &DeviceInterfaces::usb(),
            &DeviceInterfaces::ethernet()
        };
        static const QList<const DeviceInterface *> usb {
            &DeviceInterfaces::usb()
        };
        static const QList<const DeviceInterface *> eth {
            &DeviceInterfaces::ethernet()
        };
        return ((m_type == LTR_CRATE_TYPE_LTR010)
                 || (m_type == LTR_CRATE_TYPE_LTR021)
                 || (m_type == LTR_CRATE_TYPE_LTR_CU_1)
                 || (m_type == LTR_CRATE_TYPE_BOOTLOADER)) ? usb :
                   (m_type == LTR_CRATE_TYPE_LTR032) ? eth : usb_eth;

    }

    LTRCrateTypeInfo::LTRCrateTypeInfo(en_LTR_CrateTypes type, const QString &crateName, int slot_cnt, bool sync_support) :
        m_type{type}, m_crateTypeName{crateName}, m_slots_cnt{slot_cnt}, m_sync_support{sync_support} {

    }

}
