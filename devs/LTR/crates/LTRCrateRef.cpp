#include "LTRCrateRef.h"
#include "lqmeas/devs/LTR/service/LTRServiceRef.h"
#include "lqmeas/devs/DeviceRefMethod.h"

namespace LQMeas {
    LTRCrateRef::LTRCrateRef(const LQMeas::DeviceInfo &info,
                             const DeviceInterface &iface,
                             const LTRServiceRef *svcRef) :
        DeviceRef{info, iface, DeviceRefMethods::serialNumber(), Flag::None, svcRef} {

    }

    LTRCrateRef::LTRCrateRef(const LTRCrateRef &ref) : DeviceRef{ref} {

    }

    const LTRServiceRef *LTRCrateRef::serviceRef() const {
        return static_cast<const LTRServiceRef *>(parentRef());
    }

    void LTRCrateRef::setServiceRef(const LTRServiceRef &ref) {
        setParentRef(ref);
    }

    void LTRCrateRef::clearServiceRef() {
        clearParentRef();
    }

    bool LTRCrateRef::checkDevIface() const {
        return false;
    }

    QString LTRCrateRef::protKey() const {
        QString baseKey {DeviceRef::protKey()};
        if (serviceRef()) {
            baseKey = QString("%1_%2").arg(serviceRef()->key(), baseKey);
        }
        return baseKey;
    }
}
