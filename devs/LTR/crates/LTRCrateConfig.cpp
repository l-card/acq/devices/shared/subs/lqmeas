#include "LTRCrateConfig.h"
#include "LTRCrateTypeInfo.h"

namespace LQMeas {
    QString LTRCrateConfig::typeConfigName() {
        return "LTRCrate";
    }

    const DevSyncMarksGenConfig *LTRCrateConfig::syncMarksGenConfig() const {
        return &m_sync;
    }


    LTRCrateConfig::LTRCrateConfig() {
        addManualConfigIface(&m_sync);
        init();
    }

    LTRCrateConfig::LTRCrateConfig(const LTRCrateConfig &cfg) : m_sync{cfg.sync()} {
        addManualConfigIface(&m_sync);
        init();
    }
}
