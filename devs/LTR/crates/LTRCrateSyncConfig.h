#ifndef LTRCRATESYNCCONFIG_H
#define LTRCRATESYNCCONFIG_H


#include "lqmeas/ifaces/sync/DevSyncMarksGenConfig.h"

namespace LQMeas {
    class LTRCrateSyncConfig : public DevSyncMarksGenConfig {
        Q_OBJECT
    public:
        static const int doutCount = 2;

        enum class DigOutMode {
            Const0,
            Const1,
            DigIn1,
            DigIn2,
            StartMarks,
            SecondMarks
        };


        enum class StartMarkMode {
            Off,
            Manual,
            OnStart,
            DigIn1Rise,
            DigIn1Fall,
            DigIn2Rise,
            DigIn2Fall
        };

        enum class SecondMarkMode {
            Off,
            PeriodicCrateTimer,
            DigIn1Rise,
            DigIn1Fall,
            DigIn2Rise,
            DigIn2Fall
        };

        QString cfgIfaceName() const override {return QStringLiteral("LTRCrateSync");}

        LTRCrateSyncConfig();
        LTRCrateSyncConfig(const LTRCrateSyncConfig &cfg);

        bool isEnabled() const {return m_params.enabled;}
        bool doutsIsEnabled() const {return m_params.dout.enabled;}
        DigOutMode doutMode(int outnum) const {return m_params.dout.modes[outnum];}
        StartMarkMode startMarkMode() const {return m_params.marks.start;}
        SecondMarkMode secondMarkMode() const {return m_params.marks.second;}

        bool startMarkIsExternal() const;
        bool secondMarkIsExternal() const;
        bool startMarkGenEnabled() const;
        bool secondMarkGenEnabled() const;

        bool childrenSyncStartEnabled() const;
        bool childrenSyncStartAvailable() const;

        bool syncMarksGenEnabled(const DevSyncMarkType &type) const override;
    public Q_SLOTS:
        void setEnabled(bool en);
        void setDoutsEnabled(bool en);
        void setDoutMode(int outnum, DigOutMode mode);
        void setStartMarkMode(StartMarkMode mode);
        void setSecondMarkMode(SecondMarkMode mode);
        void setChildrenSyncStartEnabled(bool en);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        struct {
            bool enabled;
            struct {
                bool enabled;
                DigOutMode modes[doutCount];
            } dout;

            struct {
                StartMarkMode start;
                SecondMarkMode second;
            } marks;

            struct {
                bool en;
            } childrenSyncStart;
        } m_params;
    };
}

Q_DECLARE_METATYPE(LQMeas::LTRCrateSyncConfig::DigOutMode);
Q_DECLARE_METATYPE(LQMeas::LTRCrateSyncConfig::StartMarkMode);
Q_DECLARE_METATYPE(LQMeas::LTRCrateSyncConfig::SecondMarkMode);

#endif // LTRCRATESYNCCONFIG_H
