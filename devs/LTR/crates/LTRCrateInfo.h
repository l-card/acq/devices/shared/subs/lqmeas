#ifndef LQMEAS_LTRCRATEINFO_H
#define LQMEAS_LTRCRATEINFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTRCrateTypeInfo.h"

namespace LQMeas {
    class LTRCrateInfo : public DeviceInfo {
    public:
        explicit LTRCrateInfo(const LTRCrateTypeInfo &type = LTRCrateTypeInfo::defaultTypeInfo(),
                     const QString &serial = QString{});

        explicit LTRCrateInfo(const TLTR_CRATE_DESCR &crateDescr, const TLTR_CRATE_STATISTIC &crateStat);

        const QString &firmwareVerStr() const {return m_fw_version;}
        const QString &fpgaVerStr() const {return m_fpga_version;}
        const QString &bootldrVerStr() const {return m_bootldr_version;}
        const QString &boardRevStr() const {return m_board_revision;}
        bool supportSync() const;


        DeviceInfo *clone() const override {
            return new LTRCrateInfo{*this};
        }

        int slotCount() const {return devspecTypeInfo().slotCount();}
        const LTRCrateTypeInfo &devspecTypeInfo() const {
            return static_cast<const LTRCrateTypeInfo &>(type());
        }
    protected:
        LTRCrateInfo(const LTRCrateInfo &info);

        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;

    private:
        QString m_fw_version;
        QString m_fpga_version;
        QString m_bootldr_version;
        QString m_board_revision;
    };
}

#endif // LTRCRATEINFO_H
