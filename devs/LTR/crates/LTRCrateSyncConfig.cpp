#include "LTRCrateSyncConfig.h"
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/StdConfigKeys.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarks.h"
#include <QJsonObject>
#include <QJsonArray>

namespace LQMeas {

    static const QLatin1String cfgkey_enabled                   {StdConfigKeys::enabled()};
    static const QLatin1String cfgkey_mode                      {StdConfigKeys::mode()};
    static const QLatin1String cfgkey_digout_obj                {"DigOut"};
    static const QLatin1String cfgkey_outputs_arr               {StdConfigKeys::outputs()};
    static const QLatin1String cfgkey_children_sync_start_obj   {"ChildrenSyncStart"};
    static const QLatin1String cfgkey_marks_obj                 {StdConfigKeys::marks()};
    static const QLatin1String cfgkey_mark_start_obj            {StdConfigKeys::markStart()};
    static const QLatin1String cfgkey_mark_second_obj           {StdConfigKeys::markSecond()};




    typedef EnumConfigKey<LTRCrateSyncConfig::DigOutMode> DigOutModeKey;
    static const DigOutModeKey digout_modes[] = {
        {QStringLiteral("const0"),          LTRCrateSyncConfig::DigOutMode::Const0},
        {QStringLiteral("const1"),          LTRCrateSyncConfig::DigOutMode::Const1},
        {QStringLiteral("digin1"),          LTRCrateSyncConfig::DigOutMode::DigIn1},
        {QStringLiteral("digin2"),          LTRCrateSyncConfig::DigOutMode::DigIn2},
        {QStringLiteral("startmarks"),      LTRCrateSyncConfig::DigOutMode::StartMarks},
        {QStringLiteral("secmarks"),        LTRCrateSyncConfig::DigOutMode::SecondMarks},
        {DigOutModeKey::defaultKey(),       LTRCrateSyncConfig::DigOutMode::Const0},
    };

    typedef EnumConfigKey<LTRCrateSyncConfig::StartMarkMode> StartMarkModeKey;
    static const StartMarkModeKey startmark_modes[] = {
        {QStringLiteral("off"),             LTRCrateSyncConfig::StartMarkMode::Off},
        {QStringLiteral("manual"),          LTRCrateSyncConfig::StartMarkMode::Manual},
        {QStringLiteral("onstart"),         LTRCrateSyncConfig::StartMarkMode::OnStart},
        {QStringLiteral("digin1rise"),      LTRCrateSyncConfig::StartMarkMode::DigIn1Rise},
        {QStringLiteral("digin1fall"),      LTRCrateSyncConfig::StartMarkMode::DigIn1Fall},
        {QStringLiteral("digin2rise"),      LTRCrateSyncConfig::StartMarkMode::DigIn2Rise},
        {QStringLiteral("digin2fall"),      LTRCrateSyncConfig::StartMarkMode::DigIn2Fall},
        {StartMarkModeKey::defaultKey(),    LTRCrateSyncConfig::StartMarkMode::Off},
    };

    typedef EnumConfigKey<LTRCrateSyncConfig::SecondMarkMode> SecondMarkModeKey;
    static const SecondMarkModeKey secmark_modes[] = {
        {QStringLiteral("off"),             LTRCrateSyncConfig::SecondMarkMode::Off},
        {QStringLiteral("periodictmr"),     LTRCrateSyncConfig::SecondMarkMode::PeriodicCrateTimer},
        {QStringLiteral("digin1rise"),      LTRCrateSyncConfig::SecondMarkMode::DigIn1Rise},
        {QStringLiteral("digin1fall"),      LTRCrateSyncConfig::SecondMarkMode::DigIn1Fall},
        {QStringLiteral("digin2rise"),      LTRCrateSyncConfig::SecondMarkMode::DigIn2Rise},
        {QStringLiteral("digin2fall"),      LTRCrateSyncConfig::SecondMarkMode::DigIn2Fall},
        {SecondMarkModeKey::defaultKey(),   LTRCrateSyncConfig::SecondMarkMode::Off},
    };


    LTRCrateSyncConfig::LTRCrateSyncConfig() {
        memset(&m_params, 0, sizeof(m_params));
    }

    LTRCrateSyncConfig::LTRCrateSyncConfig(const LTRCrateSyncConfig &cfg) {
        memcpy(&m_params, &cfg.m_params, sizeof(m_params));
    }

    bool LTRCrateSyncConfig::startMarkIsExternal() const {
        return (m_params.marks.start == StartMarkMode::DigIn1Rise)
                || (m_params.marks.start == StartMarkMode::DigIn1Fall)
                || (m_params.marks.start == StartMarkMode::DigIn2Rise)
                || (m_params.marks.start == StartMarkMode::DigIn2Fall);
    }

    bool LTRCrateSyncConfig::secondMarkIsExternal() const  {
        return (m_params.marks.second == SecondMarkMode::DigIn1Rise)
                || (m_params.marks.second == SecondMarkMode::DigIn1Fall)
                || (m_params.marks.second == SecondMarkMode::DigIn2Rise)
                || (m_params.marks.second == SecondMarkMode::DigIn2Fall);
    }

    bool LTRCrateSyncConfig::startMarkGenEnabled() const {
        return startMarkMode() != StartMarkMode::Off;
    }

    bool LTRCrateSyncConfig::secondMarkGenEnabled() const {
        return secondMarkMode() != SecondMarkMode::Off;
    }

    bool LTRCrateSyncConfig::childrenSyncStartEnabled() const {
        return childrenSyncStartAvailable() && m_params.childrenSyncStart.en;
    }

    bool LTRCrateSyncConfig::childrenSyncStartAvailable() const {
        return startMarkGenEnabled();
    }


    bool LTRCrateSyncConfig::syncMarksGenEnabled(const DevSyncMarkType &type) const {
        bool ret = false;
        if (type == LTRSyncMarkTypes::start()) {
            ret = startMarkGenEnabled();
        } else if (type == LTRSyncMarkTypes::second()) {
            ret = secondMarkGenEnabled();
        }
        return ret;
    }

    void LTRCrateSyncConfig::setEnabled(bool en) {
        if (m_params.enabled != en) {
            m_params.enabled = en;
            notifyConfigChanged();
        }
    }

    void LTRCrateSyncConfig::setDoutsEnabled(bool en) {
        if (m_params.dout.enabled != en) {
            m_params.dout.enabled = en;
            notifyConfigChanged();
        }
    }

    void LTRCrateSyncConfig::setDoutMode(int outnum, LTRCrateSyncConfig::DigOutMode mode) {
        if (m_params.dout.modes[outnum] != mode) {
            m_params.dout.modes[outnum] = mode;
            notifyConfigChanged();
        }
    }

    void LTRCrateSyncConfig::setStartMarkMode(LTRCrateSyncConfig::StartMarkMode mode) {
        if (m_params.marks.start != mode) {
            m_params.marks.start = mode;
            notifyConfigChanged();
        }
    }

    void LTRCrateSyncConfig::setSecondMarkMode(LTRCrateSyncConfig::SecondMarkMode mode) {
        if (m_params.marks.second != mode) {
            m_params.marks.second = mode;
            notifyConfigChanged();
        }
    }

    void LTRCrateSyncConfig::setChildrenSyncStartEnabled(bool en) {
        if (m_params.childrenSyncStart.en != en) {
            m_params.childrenSyncStart.en = en;
            notifyConfigChanged();
        }
    }

    void LTRCrateSyncConfig::protSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_enabled] = m_params.enabled;

        {
            QJsonObject doutCfgObj;
            doutCfgObj[cfgkey_enabled] = m_params.dout.enabled;
            QJsonArray doutsArray;
            for (int ch = 0; ch < doutCount; ++ch) {
                QJsonObject doutObj;
                doutObj[cfgkey_mode] = DigOutModeKey::getKey(digout_modes, m_params.dout.modes[ch]);
                doutsArray.append(doutObj);
            }
            doutCfgObj[cfgkey_outputs_arr] = doutsArray;
            cfgObj[cfgkey_digout_obj] = doutCfgObj;
        }


        {
            QJsonObject marksObj;

            QJsonObject startMarkObj;
            startMarkObj[cfgkey_mode] = StartMarkModeKey::getKey(startmark_modes, m_params.marks.start);
            marksObj[cfgkey_mark_start_obj] = startMarkObj;

            QJsonObject secMarkObj;
            secMarkObj[cfgkey_mode] = SecondMarkModeKey::getKey(secmark_modes, m_params.marks.second);
            marksObj[cfgkey_mark_second_obj] = secMarkObj;

            cfgObj[cfgkey_marks_obj] = marksObj;
        }

        {
            QJsonObject childrenStartObj;
            childrenStartObj[cfgkey_enabled] = m_params.childrenSyncStart.en;
            cfgObj[cfgkey_children_sync_start_obj] = childrenStartObj;
        }
    }

    void LTRCrateSyncConfig::protLoad(const QJsonObject &cfgObj) {
        m_params.enabled = cfgObj[cfgkey_enabled].toBool();

        {
            const QJsonObject &doutCfgObj = cfgObj[cfgkey_digout_obj].toObject();
            m_params.dout.enabled = doutCfgObj[cfgkey_enabled].toBool();

            const QJsonArray &doutsArray = doutCfgObj[cfgkey_outputs_arr].toArray();
            for (int ch = 0; ch < doutCount; ++ch) {
                if (ch < doutsArray.size()) {
                    const QJsonObject &doutObj = doutsArray.at(ch).toObject();
                    m_params.dout.modes[ch] = DigOutModeKey::getValue(
                                digout_modes, doutObj[cfgkey_mode].toString());
                } else {
                    m_params.dout.modes[ch] = LTRCrateSyncConfig::DigOutMode::Const0;
                }
            }
        }

        {
            const QJsonObject &marksObj = cfgObj[cfgkey_marks_obj].toObject();

            const QJsonObject &startMarkObj = marksObj[cfgkey_mark_start_obj].toObject();
            m_params.marks.start = StartMarkModeKey::getValue(
                        startmark_modes, startMarkObj[cfgkey_mode].toString());

            const QJsonObject &secMarkObj = marksObj[cfgkey_mark_second_obj].toObject();
            m_params.marks.second = SecondMarkModeKey::getValue(
                        secmark_modes, secMarkObj[cfgkey_mode].toString());
        }

        {
            const QJsonObject &childrenStartObj =cfgObj[cfgkey_children_sync_start_obj].toObject();
            m_params.childrenSyncStart.en = childrenStartObj[cfgkey_enabled].toBool();
        }
    }
}
