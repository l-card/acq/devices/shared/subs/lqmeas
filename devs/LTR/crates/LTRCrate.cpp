#include "LTRCrate.h"
#include "LTRCrateRef.h"
#include "LTRCrateInfo.h"
#include "LTRCrateConfig.h"
#include "lqmeas/devs/LTR/service/LTRServiceRef.h"
#include "lqmeas/devs/LTR/service/LTRServiceInfo.h"
#include "lqmeas/devs/LTR/LTRNativeError.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/devs/LTR/modules/LTRModule.h"
#include "lqmeas/devs/LTR/LTRResolver.h"
#include "lqmeas/devs/DeviceValidator.h"
#include "lqmeas/devs/DeviceNameValidator.h"
#include "lqmeas/devs/DeviceRef.h"
#include "lqmeas/log/Log.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdErrors.h"



namespace LQMeas {
    static en_LTR_DigOutCfg f_get_out_cfg_code(LTRCrateSyncConfig::DigOutMode mode) {
        return mode == LTRCrateSyncConfig::DigOutMode::Const1 ? LTR_DIGOUT_CONST1 :
               mode == LTRCrateSyncConfig::DigOutMode::DigIn1 ? LTR_DIGOUT_DIGIN1 :
               mode == LTRCrateSyncConfig::DigOutMode::DigIn2 ? LTR_DIGOUT_DIGIN2 :
               mode == LTRCrateSyncConfig::DigOutMode::StartMarks ? LTR_DIGOUT_START :
               mode == LTRCrateSyncConfig::DigOutMode::SecondMarks ? LTR_DIGOUT_SECOND :
                                                                     LTR_DIGOUT_CONST0;
    }

    static en_LTR_MarkMode f_get_start_mark_mode_code(LTRCrateSyncConfig::StartMarkMode mode) {
        return mode == LTRCrateSyncConfig::StartMarkMode::DigIn1Rise ? LTR_MARK_EXT_DIGIN1_RISE :
               mode == LTRCrateSyncConfig::StartMarkMode::DigIn1Fall ? LTR_MARK_EXT_DIGIN1_FALL :
               mode == LTRCrateSyncConfig::StartMarkMode::DigIn2Rise ? LTR_MARK_EXT_DIGIN2_RISE :
               mode == LTRCrateSyncConfig::StartMarkMode::DigIn2Fall ? LTR_MARK_EXT_DIGIN2_FALL :
                                                                       LTR_MARK_OFF;
    }

    static en_LTR_MarkMode f_get_second_mark_mode_code(LTRCrateSyncConfig::SecondMarkMode mode) {
        return mode == LTRCrateSyncConfig::SecondMarkMode::PeriodicCrateTimer  ? LTR_MARK_INTERNAL :
               mode == LTRCrateSyncConfig::SecondMarkMode::DigIn1Rise ? LTR_MARK_EXT_DIGIN1_RISE :
               mode == LTRCrateSyncConfig::SecondMarkMode::DigIn1Fall ? LTR_MARK_EXT_DIGIN1_FALL :
               mode == LTRCrateSyncConfig::SecondMarkMode::DigIn2Rise ? LTR_MARK_EXT_DIGIN2_RISE :
               mode == LTRCrateSyncConfig::SecondMarkMode::DigIn2Fall ? LTR_MARK_EXT_DIGIN2_FALL :
                                                                        LTR_MARK_OFF;
    }

    const LTRCrateTypeInfo *LTRCrate::getCrateType(WORD crateType) {
        return LTRCrateTypeInfo::typeInfo(static_cast<en_LTR_CrateTypes>(crateType));
    }

    const DeviceInterface &LTRCrate::getCrateIface(BYTE ifaceCode)     {
        return ifaceCode == LTR_CRATE_IFACE_USB ? DeviceInterfaces::usb() :
               ifaceCode == LTR_CRATE_IFACE_TCPIP ? DeviceInterfaces::ethernet() :
                                                    DeviceInterfaces::unknown();
    }

    LTRCrate::LTRCrate(const LTRServiceRef &srvref, const QString &serial, const TLTR_CRATE_INFO &info) :
        Device{new LTRCrateConfig{},
               LTRCrateInfo{*getCrateType(info.CrateType), serial},
               getCrateIface(info.CrateInterface)
               },
        m_svcRef{new LTRServiceRef{srvref}},
        m_iface_code{LTR_CRATE_IFACE_UNKNOWN} {

        LTR_Init(&m_hnd);
        m_modules.resize(slotCount());
    }

    void LTRCrate::updateModules(LQError &err) {
        QList<QSharedPointer<LTRModule> > ret;
        bool close_req {false};
        if (!isOpened()) {
            open(err);
            if (err.isSuccess()) {
                close_req = true;
            }
        }

        if (isOpened()) {
            WORD mids[LTR_MODULES_PER_CRATE_MAX];
            INT err_code = LTR_GetCrateModules(&m_hnd, mids);
            if (err_code != LTR_OK) {
                /* попытка обновить соединение с крейтом, если не удалось получить список модулей */
                close();
                open(err);
                err_code = LTR_GetCrateModules(&m_hnd, mids);
            }
            if (err_code==LTR_OK) {
                TLTR_MODULE_STATISTIC mstat[LTR_MODULES_PER_CRATE_MAX];
                memset(mstat, 0, sizeof(mstat));
                for (int slot = 0; slot < LTR_MODULES_PER_CRATE_MAX; ++slot) {
                    mstat[slot].size = offsetof(TLTR_MODULE_STATISTIC, mid) + sizeof(mstat[slot].mid);
                    mstat[slot].mid = mids[slot];
                }
                updateModulesInfo(mstat);
            }

            getError(err_code, err);

            if (close_req)
                close();
        }
    }

    void LTRCrate::updateModulesInfo(const TLTR_MODULE_STATISTIC modstats[]) {
        for (int slot=0; (slot < LTR_MODULES_PER_CRATE_MAX) && (slot < m_modules.size()); ++slot) {
            quint16 mid = modstats[slot].mid;
            if ((mid != LTR_MID_EMPTY) && (mid != LTR_MID_IDENTIFYING)) {
                QSharedPointer<LTRModule> module = m_modules[slot];
                if (!module || (module->mid()!= mid)) {
                    m_modules[slot] = LTRResolver::createModule(
                                this,
                                slot + LTR_CC_CHNUM_MODULE1,
                                mid);
                }
            } else {
                /* если пуст - то удаляем старую информацию о модуле */
                m_modules[slot] = QSharedPointer<LTRModule>();
            }
        }
    }

    void LTRCrate::updateInfo(const TLTR_CRATE_DESCR &crateDescr,
                              const TLTR_CRATE_STATISTIC &crate_stat,
                              const LQMeas::LTRServiceInfo &svcInfo) {
        m_svcRef->setInfo(svcInfo);
        setDeviceInfo(LTRCrateInfo(crateDescr, crate_stat));
        setIface(getCrateIface(crate_stat.crate_intf & 0xFF));
        for (const QSharedPointer<LTRModule> &module : m_modules) {
            if (module) {
                module->updateCrateInfo(*devspecInfo(), m_svcRef->serviceInfo());
            }
        }
    }

    void LTRCrate::clear() {
        m_modules.clear();
    }

    LTRCrate::~LTRCrate() {
        if (isOpened())
            close();
        delete m_svcRef;
    }


    int LTRCrate::slotCount() const {
        return devspecInfo()->slotCount();
    }

    bool LTRCrate::isOpened() const {
        return LTR_IsOpened(&m_hnd)==LTR_OK;
    }


    QList<QSharedPointer<LTRModule> > LTRCrate::moduleList() const {
        return moduleList(DeviceAllPassValidator());
    }

    QList<QSharedPointer<LTRModule> > LTRCrate::moduleList(QStringList nameFilter) const {
        return moduleList(DeviceNameValidator(nameFilter));
    }



    QList<QSharedPointer<LTRModule> > LTRCrate::moduleList(const DeviceValidator &validator) const {
        QList<QSharedPointer<LTRModule> > ret;
        for (const QSharedPointer<LTRModule> &module : m_modules) {
            if (module && validator.deviceValid(*module)) {
                ret.append(module);
            }
        }
        return ret;
    }

    QList<QSharedPointer<Device> > LTRCrate::protGetDevList(const QList<const DeviceRef *> &devRefs, ResolveFlags flags) {
        Q_UNUSED(devRefs)
        Q_UNUSED(flags)

        QList<QSharedPointer<LTRModule> > modules = moduleList();
        QList<QSharedPointer<Device> > retlist;
        for (const QSharedPointer<LTRModule> &module : modules) {
            retlist.append(module);
        }
        return retlist;
    }

    QSharedPointer<const LTRCrateInfo> LTRCrate::devspecInfo() const {
        return devInfo().staticCast<const LTRCrateInfo>();
    }

    const LTRCrateConfig &LTRCrate::devspecConfig() const {
        return static_cast<const LTRCrateConfig &>(config());
    }

    DeviceRef *LTRCrate::createRef() const {
        /* не различаем ссылку на крейт по интерфейсу, т.к. рабочий всегда только один,
         * и мы можем не перекофигрировать крейт при смене интефрейса */
        return new LTRCrateRef{*devInfo(), iface(), m_svcRef};

    }

    void LTRCrate::syncMarksGenStartMaster(LQError &err) {
        INT err_code {LTR_OK};
        const LTRCrateSyncConfig &syncCfg {devspecConfig().sync()};


        if ((err_code == LTR_OK) && (syncCfg.secondMarkMode() ==
                                     LTRCrateSyncConfig::SecondMarkMode::PeriodicCrateTimer)) {
            err_code = LTR_StartSecondMark(&m_hnd, LTR_MARK_INTERNAL);
        }

        if ((err_code == LTR_OK) && (syncCfg.startMarkMode() ==
                                     LTRCrateSyncConfig::StartMarkMode::OnStart)) {
            err_code = LTR_MakeStartMark(&m_hnd, LTR_MARK_INTERNAL);
        }

        getError(err_code, err);
    }

    void LTRCrate::syncMarksGenStartSlave(LQError &err) {
        INT err_code {LTR_OK};
        const LTRCrateSyncConfig &syncCfg = devspecConfig().sync();

        if ((err_code == LTR_OK) && syncCfg.secondMarkIsExternal()) {
            err_code = LTR_StartSecondMark(&m_hnd, f_get_second_mark_mode_code(syncCfg.secondMarkMode()));
        }

        if ((err_code == LTR_OK) && syncCfg.startMarkIsExternal()) {
            err_code = LTR_MakeStartMark(&m_hnd, f_get_start_mark_mode_code(syncCfg.startMarkMode()));
        }


        getError(err_code, err);
    }

    void LTRCrate::syncMarksGenStop(LQError &err) {
        INT err_code = LTR_MakeStartMark(&m_hnd, LTR_MARK_OFF);
        if (err_code == LTR_OK) {
            err_code = LTR_StopSecondMark(&m_hnd);
        }
        getError(err_code, err);
    }

    void LTRCrate::syncMarksGenManual(LQError &err) {
        if (devspecConfig().sync().startMarkMode() == LTRCrateSyncConfig::StartMarkMode::Manual) {
            getError(LTR_MakeStartMark(&m_hnd, LTR_MARK_INTERNAL), err);
        }

    }

    void LTRCrate::getError(INT err_code, LQError &err)  {
        if (err_code != LTR_OK) {
            err = error(err_code);
        }
    }

    LQError LTRCrate::error(INT err_code) {
        return err_code == LTR_OK ? LQError::Success() : LTRNativeError::error(
                                        err_code, QSTRING_FROM_CSTR(LTR_GetErrorString(err_code)));
    }


    void LTRCrate::protOpen(OpenFlags flags, LQError &err) {
        Q_UNUSED(flags)

        getError(LTR_OpenCrate(&m_hnd,
                               m_svcRef->ltrdIPv4Addr(),
                               m_svcRef->ltrdTcpPort(),
                               m_iface_code,
                               QSTRING_TO_CSTR(devInfo()->serial())),
                 err);
    }

    void LTRCrate::protClose(LQError &err) {
        getError(LTR_Close(&m_hnd), err);
    }

    void LTRCrate::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTRCrateConfig *setCfg = qobject_cast<const LTRCrateConfig *>(&cfg);
        if (setCfg) {
            if (devspecInfo()->supportSync() && setCfg->sync().isEnabled()) {
                TLTR_CONFIG outcfg;

                outcfg.digout_en = setCfg->sync().doutsIsEnabled();
                for (unsigned userio_num = 0; userio_num < sizeof(outcfg.userio)/sizeof(outcfg.userio[0]); ++userio_num) {
                    outcfg.userio[userio_num] = LTR_USERIO_DEFAULT;
                }
                for (unsigned digout_num = 0; digout_num < sizeof(outcfg.digout)/sizeof(outcfg.digout[0]); ++digout_num) {
                    outcfg.digout[digout_num] = f_get_out_cfg_code(setCfg->sync().doutMode(static_cast<int>(digout_num)));
                }

                syncMarksGenStop(err);
                if (err.isSuccess()) {
                    getError(LTR_Config(&m_hnd, &outcfg), err);
                }
            }
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }
}
