#ifndef LQMEAS_LTRCRATE_H
#define LQMEAS_LTRCRATE_H


#include <QStringList>
#include <QSharedPointer>
#include <QVector>
#include "ltr/include/ltrapi.h"
#include "lqmeas/devs/Device.h"
#include "lqmeas/devs/resolver/DevicesResolver.h"
#include "lqmeas/ifaces/sync/DevSyncMarksGen.h"

namespace LQMeas {
    class LTRModule;
    class LTRResolver;
    class DeviceValidator;
    class LTRCrateInfo;
    class LTRCrateTypeInfo;
    class LTRCrateConfig;
    class LTRServiceRef;
    class LTRServiceInfo;

    class LTRCrate : public Device, public DevicesResolver, public DevSyncMarksGen {
        Q_OBJECT
    public:
        ~LTRCrate() override;

        int slotCount() const;
        virtual bool isOpened() const override;
        const LTRServiceRef &serviceRef() const {return *m_svcRef;}

        QList<QSharedPointer<LTRModule> > moduleList() const;
        QList<QSharedPointer<LTRModule> > moduleList(QStringList nameFilter) const;
        QList<QSharedPointer<LTRModule> > moduleList(const DeviceValidator &validator) const;

        DevicesResolver *childResolver() override {return static_cast<DevicesResolver *>(this); }
        DevSyncMarksGen *devSyncMarksGen() override {return static_cast<DevSyncMarksGen *>(this); }

        QSharedPointer<const LTRCrateInfo> devspecInfo() const;
        const LTRCrateConfig &devspecConfig() const;

        DeviceRef *createRef() const override;

        void syncMarksGenStartMaster(LQError &err) override;
        void syncMarksGenStartSlave(LQError &err) override;
        void syncMarksGenStop(LQError &err) override;
        void syncMarksGenManual(LQError &err) override;

        static void getError(INT err_code, LQError &err);
        static LQError error(INT err_code);

    protected:
        QList<QSharedPointer<Device> > protGetDevList(const QList<const DeviceRef *> &devRefs, ResolveFlags flags) override;
        void protOpen(OpenFlags flags, LQError &err) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;
    private:
        static const LTRCrateTypeInfo *getCrateType(WORD crateType);
        static const DeviceInterface &getCrateIface(BYTE ifaceCode);

        explicit LTRCrate(const LTRServiceRef &srvref, const QString &serial, const TLTR_CRATE_INFO &info);

        void updateModules(LQError &err);
        void updateInfo(const TLTR_CRATE_DESCR &crateDescr, const TLTR_CRATE_STATISTIC &crate_stat,
                        const LTRServiceInfo &svcInfo);
        void updateModulesInfo(const TLTR_MODULE_STATISTIC modstats[]);

        /* из-за взаимных ссылок крейта и модуля нужно вручную делать очистку из
         * LTRResolver, чтобы объекты могли быть удалены, если не используются извне */
        void clear();

        LTRServiceRef *m_svcRef;
        BYTE m_iface_code;
        QVector<QSharedPointer<LTRModule> > m_modules;

        mutable TLTR m_hnd;

        friend class LTRService;
        friend class LTRCrateInfo;
    };
}

#endif // LQMEAS_LTRCRATE_H
