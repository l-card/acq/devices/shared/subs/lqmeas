#ifndef LTRCRATECONFIG_H
#define LTRCRATECONFIG_H


#include "lqmeas/devs/DeviceConfig.h"
#include "LTRCrateSyncConfig.h"

namespace LQMeas {
    class LTRCrateConfig : public DeviceConfig {
        Q_OBJECT
    public:
        static QString typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        const DevSyncMarksGenConfig *syncMarksGenConfig() const override;

        LTRCrateSyncConfig &sync() {return  m_sync;}
        const LTRCrateSyncConfig &sync() const {return  m_sync;}

        DeviceConfig *clone() const override {
            return new LTRCrateConfig{*this};
        }

        LTRCrateConfig();
        LTRCrateConfig(const LTRCrateConfig &cfg);
    private:
        LTRCrateSyncConfig m_sync;
    };
}

#endif // LTRCRATECONFIG_H
