#include "LTRCrateInfo.h"
#include "LTRCrate.h"
#include "lqmeas/lqtdefs.h"
#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String cfgkey_firmware_version {"FirmwareVer"};
    static const QLatin1String cfgkey_bootldr_ver {"BootldrVer"};

    LTRCrateInfo::LTRCrateInfo(const LTRCrateTypeInfo &type, const QString &serial) :
        DeviceInfo{type, serial} {

    }

    LTRCrateInfo::LTRCrateInfo(const TLTR_CRATE_DESCR &crateDescr, const TLTR_CRATE_STATISTIC &crateStat) :
        DeviceInfo{*LTRCrate::getCrateType(crateStat.crate_type),
                   QSTRING_FROM_CSTR(crateDescr.serial)} {

        m_fw_version = QSTRING_FROM_CSTR(crateDescr.soft_ver);
        m_fpga_version = QSTRING_FROM_CSTR(crateDescr.fpga_version);
        m_bootldr_version = QSTRING_FROM_CSTR(crateDescr.bootloader_ver);
        m_board_revision = QSTRING_FROM_CSTR(crateDescr.brd_revision);
    }

    bool LTRCrateInfo::supportSync() const {
        return devspecTypeInfo().supportSync();
    }



    LTRCrateInfo::LTRCrateInfo(const LTRCrateInfo &info) : DeviceInfo{info},
        m_fw_version{info.m_fw_version},
        m_fpga_version{info.m_fpga_version},
        m_bootldr_version{info.m_bootldr_version},
        m_board_revision{info.m_board_revision} {

    }

    void LTRCrateInfo::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_firmware_version] = m_fw_version;
        infoObj[StdConfigKeys::fpgaVer()] = m_fpga_version;
        infoObj[cfgkey_bootldr_ver] = m_bootldr_version;
        infoObj[StdConfigKeys::boardRev()] = m_board_revision;
    }

    void LTRCrateInfo::protLoad(const QJsonObject &infoObj) {
        m_fw_version = infoObj[cfgkey_firmware_version].toString();
        m_fpga_version = infoObj[StdConfigKeys::fpgaVer()].toString();
        m_bootldr_version = infoObj[cfgkey_bootldr_ver].toString();
        m_board_revision = infoObj[StdConfigKeys::boardRev()].toString();
    }
}
