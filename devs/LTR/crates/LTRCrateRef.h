#ifndef LQMEAS_LTRCRATEREF_H
#define LQMEAS_LTRCRATEREF_H

#include "lqmeas/devs/DeviceRef.h"


namespace LQMeas {
    class LTRServiceRef;

    class LTRCrateRef : public DeviceRef {
    public:
        explicit LTRCrateRef(const DeviceInfo &info, const DeviceInterface &iface,
                             const LTRServiceRef *svcRef = nullptr);

        DeviceRef *clone() const override {return new LTRCrateRef{*this};}

        const LTRServiceRef *serviceRef() const;
        void setServiceRef(const LTRServiceRef &ref);
        void clearServiceRef();
    protected:
        bool checkDevIface() const override;
        QString protKey() const override;
    private:
        LTRCrateRef(const LTRCrateRef &ref);
    };
}

#endif // LTRCRATEREF_H
