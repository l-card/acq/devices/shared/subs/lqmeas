#ifndef LQMEAS_LTRCRATETYPEINFO_H
#define LQMEAS_LTRCRATETYPEINFO_H


#include "lqmeas/devs/DeviceTypeInfo.h"
#include "lqmeas/ifaces/sync/DevSyncMarksGenInfo.h"
#include "ltr/include/ltrapi.h"
#include <QList>

namespace LQMeas {
    class LTRCrateTypeInfo : public DeviceTypeInfo, public DevSyncMarksGenInfo {
    public:
        static const LTRCrateTypeInfo &defaultTypeInfo();

        static const LTRCrateTypeInfo &typeInfoU8_16();
        static const LTRCrateTypeInfo &typeInfoU1();
        static const LTRCrateTypeInfo &typeInfoEU8_16();
        static const LTRCrateTypeInfo &typeInfoEU2();
        static const LTRCrateTypeInfo &typeInfoE7_15();
        static const LTRCrateTypeInfo &typeInfoCU1();
        static const LTRCrateTypeInfo &typeInfoCEU1();

        static const LTRCrateTypeInfo *typeInfo(en_LTR_CrateTypes crate_type);

        static const QList<const LTRCrateTypeInfo *> &types();


        QString deviceTypeName() const override {return m_crateTypeName;}
        const DeviceTypeSeries &deviceTypeSeries() const override;
        int slotCount() const {return m_slots_cnt;}
        int maxChildDevicesCount() const override {return slotCount();}

        const DevSyncMarksGenInfo *syncMarkGen() const  override {
            return m_sync_support ? static_cast<const DevSyncMarksGenInfo *>(this) : nullptr;
        }
        QList<const DevSyncMarkType *> syncMarkGenTypes() const override;

        en_LTR_CrateTypes crateType() const {return m_type;}
        bool supportSync() const {return m_sync_support;}

        QList<const DeviceTypeInfo *> modificationList() const override;
        QList<const DeviceInterface *> supportedConInterfaces() const override;
    private:
        LTRCrateTypeInfo(en_LTR_CrateTypes type, const QString &crateName, int slot_cnt, bool sync_support = true);

        en_LTR_CrateTypes m_type;
        QString m_crateTypeName;
        int m_slots_cnt;
        bool m_sync_support;
    };
}

#endif // LQMEAS_LTRCRATETYPEINFO_H
