#ifndef LTRMODULEREF_H
#define LTRMODULEREF_H


#include "lqmeas/devs/DeviceRef.h"

namespace LQMeas {
    class LTRModuleRef : public DeviceRef {
    public:
        LTRModuleRef(const DeviceInfo &info, const DeviceRef &crate, int slot_num);

        static const QString &typeRefTypeID();
        QString refTypeID() const override {return typeRefTypeID();}

        DeviceRef *clone() const override {return new LTRModuleRef(*this);}
        QString displayName() const override;

        bool sameDevice(const DeviceRef &ref) const override;
        bool sameDeviceLocation(const DeviceRef &ref) const override;
        DevCheckResult checkDevice(const Device &device) const override;

        bool serialUsed() const override {return false;}
        int parentPos() const override {return moduleSlot() - 1;}
        int moduleSlot() const  {return m_slot;}

        void save(QJsonObject &refObj) const override;
        void load(const QJsonObject &refObj) override;

        static int loadSlot(const QJsonObject &refObj);
    protected:
        QString protKey() const override;

        LTRModuleRef(const LTRModuleRef& ref);
    private:
        int m_slot;
    };
}

#endif // LTRMODULEREF_H
