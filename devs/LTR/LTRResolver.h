#ifndef LQMEAS_LTRRESOLVER_H
#define LQMEAS_LTRRESOLVER_H

#include <QList>
#include <QStringList>
#include <QSharedPointer>

#include "lqmeas/devs/resolver/DevicesResolver.h"

namespace LQMeas {
    class LTRService;
    class LTRServiceRef;
    class LTRModule;
    class LTRCrate;
    class Device;
    class DeviceValidator;


    class LTRResolver : public DevicesResolver {
    public:


        QList<QSharedPointer<LTRCrate> >  getCratesList(LQError &err);
        QList<QSharedPointer<LTRCrate> >  getCratesList(const QList< QSharedPointer<LTRService> > &svcList, LQError &err);
        QList<QSharedPointer<LTRModule> > getModuleList(const QStringList &nameFilter, LQError &err);
        QList<QSharedPointer<LTRModule> > getModuleList(const DeviceValidator &validator, LQError &err);

        QList<QSharedPointer<LTRService> > services() const {return m_svcList;}
        QSharedPointer<LTRService> service(const LTRServiceRef &ref) const;
        QSharedPointer<LTRService> addService(const LTRServiceRef &ref);
        void clearServices();

        QSharedPointer<LTRCrate> crate(const LQMeas::DeviceRef &crateRef) const;
        static LTRResolver &resolver();
    protected:
        QList<QSharedPointer<Device> > protGetDevList(const QList<const DeviceRef *> &devRefs, ResolveFlags flags) override;
    private:
        LTRResolver();
        ~LTRResolver() override;

        static const LTRServiceRef *getSvcRef(const DeviceRef &devref);

        QSharedPointer<LTRService> m_defaultSvc;
        QList< QSharedPointer<LTRService> > m_svcList;


        static QSharedPointer<LTRModule> createModule(LTRCrate *getCrate, int slot, quint16 mid);

        friend class LTRCrate;
        friend class LTRService;
    };
}

#endif // LTRRESOLVER_H
