#ifndef LQMEAS_LTRK416CONFIG_H
#define LQMEAS_LTRK416CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"

namespace LQMeas {
    class LTRK416Config: public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        DeviceConfig *clone() const override {
            return new LTRK416Config{*this};
        }


        explicit LTRK416Config();
        LTRK416Config(const LTRK416Config &cfg);
    private:
    };
}

#endif // LQMEAS_LTRK416CONFIG_H
