#ifndef LQMEAS_LTRK416TYPEINFO_H
#define LQMEAS_LTRK416TYPEINFO_H


#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"
#include "lqmeas/ifaces/out/DevOutInfo.h"

namespace LQMeas {
    class LTRK416TypeInfo : public LTRModuleTypeInfo, public DevOutInfo {
    public:
        static const QString &name();
        QString deviceTypeName() const override {return name();}

        static const int tdm_channels_cnt {2};

        static const LTRK416TypeInfo &defaultInfo();

        QList<const DeviceTypeInfo *> modificationList() const override;

        int outDigChannelsCnt() const override { return 0; }
        int outDacChannelsCnt() const override { return 1; }
        bool outDacSyncSupport() const override {return false;}
        bool outDigSyncSupport() const override {return false;}
        bool outDacAsyncSupport() const override {return true;}
        bool outDigAsyncSupport() const override {return true;}
        bool outDacSyncModeCfgPerCh() const override {return false;}
        bool outSyncRamModeIsConfigurable() const override {return false;}
        bool outSyncDacChGenModeIsConfigurable() const override {return false;}
        bool outSyncDigChGenModeIsConfigurable() const override {return false;}
        double outSyncGenFreqMax() const override {return 0;}
        bool outSyncPresetSupport() const override {return false;}
        int outDacChRangesCnt(int ch) const override {Q_UNUSED(ch) return 1;}
        double outDacChRangeMaxVal(int ch, int range) const override;
        double outDacChRangeMinVal(int ch, int range) const override;

    private:
        LTRK416TypeInfo();
    };
}
#endif // LQMEAS_LTRK416TYPEINFO_H
