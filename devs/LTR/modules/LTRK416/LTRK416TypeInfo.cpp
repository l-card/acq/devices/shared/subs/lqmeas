#include "LTRK416TypeInfo.h"
#include "ltr/include/ltrk416api.h"
#include <QStringBuilder>

namespace LQMeas {
    const QString &LTRK416TypeInfo::name() {
        static const QString str {QStringLiteral("LTRK416") };
        return str;
    }

    const LTRK416TypeInfo &LTRK416TypeInfo::defaultInfo() {
        static const LTRK416TypeInfo type;
        return type;
    }



    QList<const DeviceTypeInfo *> LTRK416TypeInfo::modificationList() const  {
        static const QList<const DeviceTypeInfo *> list {
            &defaultInfo(),
        };
        return list;
    }


    double LTRK416TypeInfo::outDacChRangeMaxVal(int ch, int range) const {
        Q_UNUSED(ch) Q_UNUSED(range)
        return LTRK416_DAC_VALUE_MAX;
    }

    double LTRK416TypeInfo::outDacChRangeMinVal(int ch, int range) const {
        Q_UNUSED(ch) Q_UNUSED(range)
        return 0;
    }

    LTRK416TypeInfo::LTRK416TypeInfo() {

    }
}
