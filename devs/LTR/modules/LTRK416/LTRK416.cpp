#include "LTRK416.h"
#include "LTRK416Config.h"
#include "LTRK416Info.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdErrors.h"

namespace LQMeas {
    LTRK416::~LTRK416() {
        LTRK416_Close(&m_hnd);
    }

    bool LTRK416::isOpened() const  {
        return LTRK416_IsOpened(&m_hnd) == LTR_OK;
    }

    QString LTRK416::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTRK416_GetErrorString(err));;
    }

    const LTRK416Config &LTRK416::devspecConfig() const {
        return static_cast<const LTRK416Config &>(config());
    }


    QSharedPointer<const LTRK416Info> LTRK416::devspecInfo() const {
        return devInfo().staticCast<const LTRK416Info>();
    }



    TLTR *LTRK416::channel() const {
        return &m_hnd.Channel;
    }

    int LTRK416::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        INT ltr_err = LTRK416_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, static_cast<WORD>(slot));
        if (LTRK416_IsOpened(&m_hnd) == LTR_OK) {
            setDeviceInfo(LTRK416Info{LTRK416TypeInfo::defaultInfo(),
                                      QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                      m_hnd.ModuleInfo.VerPLD});
        }
        return ltr_err;
    }

    void LTRK416::protClose(LQError &err) {
        getError(LTRK416_Close(&m_hnd), err);
    }

    void LTRK416::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTRK416Config *setCfg {qobject_cast<const LTRK416Config *>(&cfg)};
        if (!setCfg) {
            err += StdErrors::InvalidConfigType();
        }
    }

    void LTRK416::protOutAsyncDac(int ch, double val, LQError &err) {
        Q_UNUSED(ch)
        getError(LTRK416_SetDAC(&m_hnd, val, LTRK416_DAC_FLAG_CALIBR | LTRK416_DAC_FLAG_VALUE), err);
    }

    LTRK416::LTRK416(LTRCrate *crate, int slot, QObject *parent) :
        LTR01{crate, slot, typeModuleSubID, new LTRK416Config{}, LTRK416Info{}, parent},
        DevOutAsyncDac{this} {

        LTRK416_Init(&m_hnd);
    }

}
