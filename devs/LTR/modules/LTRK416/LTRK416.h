#ifndef LQMEAS_LTRK416_H
#define LQMEAS_LTRK416_H


#include "ltr/include/ltrk416api.h"
#include "lqmeas/devs/LTR/modules/LTR01.h"
#include "lqmeas/ifaces/out/DevOutAsyncDac.h"

namespace LQMeas {
    class LTRK416Config;
    class LTRK416Info;
    class LTRK416VregConfig;
    class LTRK416TDMConfig;

    class LTRK416  : public LTR01, public DevOutAsyncDac {
        Q_OBJECT
    public:
        static const e_LTR01_SUBID typeModuleSubID {LTR01_SUBID_LTRK416};

        ~LTRK416() override;
        bool isOpened() const override;

        DevOutAsyncDac *devOutAsyncDac() override {
            return static_cast<DevOutAsyncDac*>(this);
        }


        QString errorString(int err) const override;

        const LTRK416Config &devspecConfig() const;
        QSharedPointer<const LTRK416Info> devspecInfo() const;

        TLTRK416 *rawHandle() {return &m_hnd;}
        const TLTRK416 *rawHandle() const {return &m_hnd;}
    protected:
        TLTR *channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;

        void protOutAsyncDac(int ch, double val, LQError &err) override;
    private:
        explicit LTRK416(LTRCrate *crate, int slot, QObject *parent = nullptr);

        mutable TLTRK416 m_hnd;

        friend class LTRResolver;
    };
}

#endif // LQMEAS_LTRK416_H
