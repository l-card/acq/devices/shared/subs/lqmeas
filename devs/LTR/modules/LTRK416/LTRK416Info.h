#ifndef LQMEAS_LTRK416INFO_H
#define LQMEAS_LTRK416INFO_H


#include "lqmeas/devs/DeviceInfo.h"
#include "LTRK416TypeInfo.h"

namespace LQMeas {
    class LTRK416Info: public DeviceInfo {
    public:
        explicit LTRK416Info(const LTRK416TypeInfo &type = LTRK416TypeInfo::defaultInfo(),
                            const QString &serial = QString{}, int pld_ver = 0);
        LTRK416Info(const LTRK416Info &info);

        int pldVer() const {return m_pld_ver;}
        QString pldVerStr() const;

        const LTRK416TypeInfo &devspecType() const {return static_cast<const LTRK416TypeInfo &>(type());}

        DeviceInfo *clone() const override {return new LTRK416Info{*this};}
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        int m_pld_ver;
    };
}

#endif // LQMEAS_LTRK416INFO_H
