#include "LTRK416Config.h"
#include "LTRK416TypeInfo.h"
namespace LQMeas {

    const QString &LTRK416Config::typeConfigName() {
        return LTRK416TypeInfo::name();
    }

    LTRK416Config::LTRK416Config() {

    }

    LTRK416Config::LTRK416Config(const LTRK416Config &cfg) {

    }
}
