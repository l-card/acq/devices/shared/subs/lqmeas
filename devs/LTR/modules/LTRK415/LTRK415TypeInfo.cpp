#include "LTRK415TypeInfo.h"
#include "ltr/include/ltrk415api.h"
#include <QStringBuilder>

namespace LQMeas {
    const QString &LTRK415TypeInfo::name() {
        static const QString str {QStringLiteral("LTRK415") };
        return str;
    }

    const LTRK415TypeInfo &LTRK415TypeInfo::defaultInfo() {
        static const LTRK415TypeInfo type;
        return type;
    }



    QList<const DeviceTypeInfo *> LTRK415TypeInfo::modificationList() const  {
        static const QList<const DeviceTypeInfo *> list {
            &defaultInfo(),
        };
        return list;
    }

    LTRK415TypeInfo::LTRK415TypeInfo() {

    }
}
