#ifndef LQMEAS_LTRK415_H
#define LQMEAS_LTRK415_H


#include "ltr/include/ltrk415api.h"
#include "lqmeas/devs/LTR/modules/LTR01.h"

namespace LQMeas {
    class LTRK415Config;
    class LTRK415Info;

    class LTRK415  : public LTR01 {
        Q_OBJECT
    public:
        static const e_LTR01_SUBID typeModuleSubID {LTR01_SUBID_LTRK415};

        enum class OutSwState {
            Off     = LTRK415_OUT_SW_STATE_OFF,
            On      = LTRK415_OUT_SW_STATE_ON,
            On_Inv  = LTRK415_OUT_SW_STATE_ON_INV,
        };


        ~LTRK415() override;
        bool isOpened() const override;

        QString errorString(int err) const override;

        const LTRK415Config &devspecConfig() const;
        QSharedPointer<const LTRK415Info> devspecInfo() const;

        TLTRK415 *rawHandle() {return &m_hnd;}
        const TLTRK415 *rawHandle() const {return &m_hnd;}

        void outSwState(int port_num, int sw_num, OutSwState sw, LQError &err);
    protected:
        TLTR *channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;
    private:
        explicit LTRK415(LTRCrate *crate, int slot, QObject *parent = nullptr);

        mutable TLTRK415 m_hnd;

        friend class LTRResolver;
    };
}

Q_DECLARE_METATYPE(LQMeas::LTRK415::OutSwState);

#endif // LQMEAS_LTRK415_H
