#ifndef LQMEAS_LTRK415CONFIG_H
#define LQMEAS_LTRK415CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"

namespace LQMeas {
    class LTRK415Config: public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        DeviceConfig *clone() const override {
            return new LTRK415Config{*this};
        }


        explicit LTRK415Config();
        LTRK415Config(const LTRK415Config &cfg);
    private:
    };
}

#endif // LQMEAS_LTRK415CONFIG_H
