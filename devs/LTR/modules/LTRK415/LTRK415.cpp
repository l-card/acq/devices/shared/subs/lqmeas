#include "LTRK415.h"
#include "LTRK415Config.h"
#include "LTRK415Info.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdErrors.h"

namespace LQMeas {
    LTRK415::~LTRK415() {
        LTRK415_Close(&m_hnd);
    }

    bool LTRK415::isOpened() const  {
        return LTRK415_IsOpened(&m_hnd) == LTR_OK;
    }

    QString LTRK415::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTRK415_GetErrorString(err));;
    }

    const LTRK415Config &LTRK415::devspecConfig() const {
        return static_cast<const LTRK415Config &>(config());
    }


    QSharedPointer<const LTRK415Info> LTRK415::devspecInfo() const {
        return devInfo().staticCast<const LTRK415Info>();
    }

    void LTRK415::outSwState(int port_num, int sw_num, LTRK415::OutSwState sw, LQError &err) {
        return getError(LTRK415_SetOutSwState(&m_hnd, port_num, sw_num, static_cast<BYTE>(sw)), err);
    }

    TLTR *LTRK415::channel() const {
        return &m_hnd.Channel;
    }

    int LTRK415::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        INT ltr_err = LTRK415_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, static_cast<WORD>(slot));
        if (LTRK415_IsOpened(&m_hnd) == LTR_OK) {
            setDeviceInfo(LTRK415Info{LTRK415TypeInfo::defaultInfo(),
                                      QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                      m_hnd.ModuleInfo.VerPLD});
        }
        return ltr_err;
    }



    void LTRK415::protClose(LQError &err) {
        getError(LTRK415_Close(&m_hnd), err);
    }

    void LTRK415::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTRK415Config *setCfg {qobject_cast<const LTRK415Config *>(&cfg)};
        if (!setCfg) {
            err += StdErrors::InvalidConfigType();
        }
    }


    LTRK415::LTRK415(LTRCrate *crate, int slot, QObject *parent) :
        LTR01{crate, slot, typeModuleSubID, new LTRK415Config{}, LTRK415Info{}, parent} {

        LTRK415_Init(&m_hnd);
    }

}
