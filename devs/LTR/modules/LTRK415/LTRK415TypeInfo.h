#ifndef LQMEAS_LTRK415TYPEINFO_H
#define LQMEAS_LTRK415TYPEINFO_H


#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"
#include "ltr/include/ltrk415api.h"

namespace LQMeas {
    class LTRK415TypeInfo : public LTRModuleTypeInfo {
    public:
        static const QString &name();
        QString deviceTypeName() const override {return name();}


        static const LTRK415TypeInfo &defaultInfo();

        QList<const DeviceTypeInfo *> modificationList() const override;

        static const int out_sw_channel_cnt {LTRK415_OUT_CH_CNT};
        static const int out_sw_cnt_per_ch  {LTRK415_OUT_CH_SW_CNT};
    private:
        LTRK415TypeInfo();
    };
}
#endif // LQMEAS_LTRK415TYPEINFO_H
