#ifndef LQMEAS_LTRK415INFO_H
#define LQMEAS_LTRK415INFO_H


#include "lqmeas/devs/DeviceInfo.h"
#include "LTRK415TypeInfo.h"

namespace LQMeas {
    class LTRK415Info: public DeviceInfo {
    public:
        explicit LTRK415Info(const LTRK415TypeInfo &type = LTRK415TypeInfo::defaultInfo(),
                            const QString &serial = QString{}, int pld_ver = 0);
        LTRK415Info(const LTRK415Info &info);

        int pldVer() const {return m_pld_ver;}
        QString pldVerStr() const;

        const LTRK415TypeInfo &devspecType() const {return static_cast<const LTRK415TypeInfo &>(type());}

        DeviceInfo *clone() const override {return new LTRK415Info{*this};}
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        int m_pld_ver;
    };
}

#endif // LQMEAS_LTRK415INFO_H
