#include "LTRK415Info.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_pld_ver           {StdConfigKeys::pldVer()};

    LTRK415Info::LTRK415Info(const LTRK415TypeInfo &type, const QString &serial, int pld_ver) :
        DeviceInfo{type, serial}, m_pld_ver{pld_ver} {

    }

    LTRK415Info::LTRK415Info(const LTRK415Info &info) :
        DeviceInfo{info},
        m_pld_ver{info.m_pld_ver} {

    }

    QString LTRK415Info::pldVerStr() const {
        return QString::number(m_pld_ver);
    }

    void LTRK415Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_pld_ver] = m_pld_ver;
    }

    void LTRK415Info::protLoad(const QJsonObject &infoObj) {
        m_pld_ver = infoObj[cfgkey_pld_ver].toInt();
    }

}
