#include "LTR51.h"
#include "LTR51Info.h"
#include "LTR51Config.h"
#include "LTR51AdcConfig.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "lqmeas/devs/LTR/modules/LTRModuleFrameReceiver.h"
#include "lqmeas/log/Log.h"
#include "lqmeas/StdErrors.h"
#include <QElapsedTimer>

namespace LQMeas {
    LTR51::LTR51(LTRCrate *crate, int slot, QObject *parent) :
        LTRModule{crate, slot, typeModuleID, new LTR51Config{}, LTR51Info{}, parent},
        DevAdc{this, LTR51TypeInfo::adc_channels_cnt},
        DevInCntr{this},
        m_receiver{new LTRModuleFrameReceiver{this}} {

        LTR51_Init(&m_hnd);
    }



    LTR51::~LTR51() {
        LTR51_Close(&m_hnd);
    }

    bool LTR51::isOpened() const {
        return LTR51_IsOpened(&m_hnd) == LTR_OK;
    }

    DevSyncMarksRecv *LTR51::devSyncMarkRecv() {
        return m_receiver.get();
    }

    QString LTR51::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTR51_GetErrorString(err));
    }

    const LTR51Config &LTR51::devspecConfig() const {
        return static_cast<const LTR51Config &>(config());
    }

    QSharedPointer<const LTR51Info> LTR51::devspecInfo() const {
        return devInfo().staticCast<const LTR51Info>();
    }

    TLTR *LTR51::channel() const {
        return &m_hnd.Channel;
    }

    int LTR51::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        Q_UNUSED(flags)
        INT ltr_err {LTR51_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, static_cast<WORD>(slot), nullptr)};

        if (ltr_err == LTR_OK) {
            const QList<const LTR51MezzanineTypeInfo *> &mezTypes {LTR51MezzanineTypeInfo::allTypes()};

            QList<LTR51MezzanineInfo *> mez_infos;
            for (int mez_idx {0}; mez_idx < LTR51TypeInfo::mezzanines_cnt; ++mez_idx) {
                LTR51MezzanineInfo *mezInfo = nullptr;


                if (((m_hnd.ChannelsEna >> (2 * mez_idx)) & 0x3) != 0) {
                    mezInfo = new LTR51MezzanineInfo{LTR51MezzanineTypeInfo::FLFH()};
                }
                mez_infos.append(mezInfo);
            }


            setDeviceInfo(LTR51Info{mez_infos,
                                    QSTRING_FROM_CSTR(reinterpret_cast<char*>(m_hnd.ModuleInfo.Serial)),
                                    QSTRING_FROM_CSTR(reinterpret_cast<char*>(m_hnd.ModuleInfo.FirmwareVersion)),
                                    QSTRING_FROM_CSTR(reinterpret_cast<char*>(m_hnd.ModuleInfo.FPGA_Version))});
        }
        return ltr_err;
    }

    void LTR51::protClose(LQError &err) {
        return getError(LTR51_Close(&m_hnd), err);
    }

    void LTR51::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTR51Config *setCfg {qobject_cast<const LTR51Config *>(&cfg)};
        if (setCfg) {
            int ch_cnt {0};
            for (int ch_num {0}; ch_num < LTR51TypeInfo::adc_channels_cnt; ++ch_num) {
                if (setCfg->adc().adcChEnabled(ch_num)) {
                    LTR51AdcConfig::EdgeMode edge_mode {setCfg->adc().adcChEdgeMode(ch_num)};
                    LTR51AdcConfig::ThresholdRange thresh_range {setCfg->adc().adcChThresholdRange(ch_num)};
                    double thresh_h {setCfg->adc().adcChThresholdHigh(ch_num)};
                    double thresh_l {setCfg->adc().adcChThresholdLow(ch_num)};
                    m_hnd.LChTbl[ch_cnt++] =
                            LTR51_CreateLChannel(ch_num + 1, &thresh_h, &thresh_l,
                                                 thresh_range == LTR51AdcConfig::ThresholdRange::U_10 ?
                                                     LTR51_THRESHOLD_RANGE_10V : LTR51_THRESHOLD_RANGE_1_2V,
                                                 edge_mode == LTR51AdcConfig::EdgeMode::Rise ?
                                                     LTR51_EDGE_MODE_RISE : LTR51_EDGE_MODE_FALL);
                }
            }
            m_hnd.LChQnt = ch_cnt;
            m_hnd.AcqTime = setCfg->adc().adcAcqTime();
            m_hnd.SetUserPars = setCfg->adc().adcCustomFsParEnabled();
            if (m_hnd.SetUserPars) {
                m_hnd.Fs = setCfg->adc().adcFs();
                m_hnd.Base = static_cast<WORD>(setCfg->adc().adcFs() * setCfg->adc().adcBaseTime() / 1000 + 0.5);
            }

            getError(LTR51_Config(&m_hnd), err);
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }

    INT LQMeas::LTR51::ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) {
        return LTR51_Recv(&m_hnd, wrds, marks, size, tout);
    }

    void LTR51::protAdcEnable(LQError &err) {
        m_freq_en = true;
    }

    void LTR51::protAdcDisable(LQError &err) {
        m_freq_en = false;
    }

    void LTR51::acqStart(LQError &err) {
        getError(LTR51_Start(&m_hnd), err);
        if (err.isSuccess()) {
            m_run = true;
            int frameSize {2*LTR51_CHANNEL_CNT*m_hnd.TbaseQnt};
            m_receiver->start(frameSize, frameSize);
        }
    }

    void LTR51::acqStop(LQError &err) {
        getError(LTR51_Stop(&m_hnd), err);
        m_run = false;
        m_receiver->clear();
    }

    void LTR51::rxNewData(int frames_cnt, unsigned tout, LQError &err) {
        DWORD *wrds;
        int src_proc_size {0};
        const int en_ch_cnt {adcConfig().adcEnabledChCnt()};
        int recvd_words {0};

        m_receiver->getFrames(frames_cnt * m_receiver->frameSize(), tout,
                              wrds, recvd_words, err);
        if (err.isSuccess() && (recvd_words > 0)) {
            int recvd_frames = recvd_words/m_receiver->frameSize();


            if (m_freq_en) {
                const int upd_freq_size {m_lastFreqWrdsCnt + recvd_frames * en_ch_cnt};
                if (upd_freq_size > m_lastFreqData.size())
                    m_lastFreqData.resize(upd_freq_size);
            }
            if (m_cntr_en) {
                const int upd_cntr_size {m_lastCntrWrdsCnt + recvd_frames * en_ch_cnt * m_hnd.TbaseQnt};
                if (upd_cntr_size > m_lastCntrData.size())
                    m_lastCntrData.resize(upd_cntr_size);
            }


            while ((src_proc_size < recvd_words) && err.isSuccess()) {
                DWORD psize {static_cast<DWORD>(m_receiver->frameSize())};
                getError(LTR51_ProcessData(&m_hnd, &wrds[src_proc_size],
                                           m_cntr_en ? &m_lastCntrData[m_lastCntrWrdsCnt] : nullptr,
                                           m_freq_en ? &m_lastFreqData[m_lastFreqWrdsCnt] : nullptr, &psize), err);
                if (err.isSuccess()) {
                    src_proc_size += m_receiver->frameSize();
                    if (m_freq_en)
                        m_lastFreqWrdsCnt += en_ch_cnt;
                    if (m_cntr_en) {
                        m_lastCntrWrdsCnt += en_ch_cnt * m_hnd.TbaseQnt;
                    }
                } else {
                    LQMeasLog->error(tr("Process data error"), err, this);
                }
            }
        }
    }

    void LTR51::protAdcStart(LQError &err) {
        QMutexLocker lock{&m_lock};
        m_freq_run = true;
        if (!m_run) {
            acqStart(err);
        }
    }

    void LTR51::protAdcStop(LQError &err) {
        QMutexLocker lock{&m_lock};
        m_freq_run = false;
        m_lastFreqData.clear();
        m_lastFreqWrdsCnt = 0;
        if (m_run && !m_cntr_run) {
            acqStop(err);
        }
    }

    void LTR51::protAdcGetData(double *data, int size, unsigned flags,
                               unsigned tout, int &recvd_size, LQError &err) {
        const int frame_size {adcConfig().adcEnabledChCnt()};
        QMutexLocker lock{&m_lock};
        /* при нулевом таймауте если данные уже есть в буфере, то просто их
         * сразу возвращаем, не пробуя еще принять */
        if ((m_lastFreqWrdsCnt < size) && !((tout == 0) && (m_lastFreqWrdsCnt > 0))) {
            QElapsedTimer tmr; tmr.start();
            do {
                int rx_freames = (size - m_lastFreqWrdsCnt)/frame_size;
                qint64 elapsed {tmr.elapsed()};
                rxNewData(rx_freames, static_cast<unsigned>(elapsed > tout ? 0 : tout - elapsed), err);
            } while (err.isSuccess() && (m_lastFreqWrdsCnt < size) && (tmr.elapsed() < tout));
        }

        if (m_lastFreqWrdsCnt > 0) {
            const int cpy_wrds {m_lastFreqWrdsCnt < size ? (m_lastFreqWrdsCnt/frame_size) * frame_size : size};
            memcpy(data, m_lastFreqData.data(), sizeof(*data) * cpy_wrds);

            m_lastFreqWrdsCnt -= cpy_wrds;
            if (m_lastFreqWrdsCnt > 0)  {
                memmove(m_lastFreqData.data(), &m_lastFreqData.data()[cpy_wrds], sizeof(*data) * m_lastFreqWrdsCnt);
            }

            recvd_size = cpy_wrds;
        } else {
            recvd_size = 0;
        }
    }

    void LTR51::protInCntrEnable(LQError &err) {
        m_cntr_en = true;
    }

    void LTR51::protInCntrDisable(LQError &err) {
        m_cntr_en = false;
    }

    void LTR51::protInCntrStart(LQError &err) {
        QMutexLocker lock{&m_lock};
        m_cntr_run = true;
        if (!m_run) {
            acqStart(err);
        }
    }

    void LTR51::protInCntrStop(LQError &err) {
        QMutexLocker lock{&m_lock};
        m_cntr_run = false;
        m_lastCntrData.clear();
        m_lastCntrWrdsCnt = 0;
        if (m_run && !m_freq_run) {
            acqStop(err);
        }
    }

    void LTR51::protInCntrGetData(DevInCntrData *data, int size, unsigned flags, unsigned tout, int &recvd_size, LQError &err) {
        const int frame_size {adcConfig().adcEnabledChCnt() * m_hnd.TbaseQnt};
        QMutexLocker lock{&m_lock};
        /* при нулевом таймауте если данные уже есть в буфере, то просто их
         * сразу возвращаем, не пробуя еще принять */
        if ((m_lastCntrWrdsCnt < size) && !((tout == 0) && (m_lastCntrWrdsCnt > 0))) {
            QElapsedTimer tmr; tmr.start();
            do {
                int rx_freames = (size - m_lastCntrWrdsCnt)/frame_size;
                qint64 elapsed {tmr.elapsed()};
                rxNewData(rx_freames, static_cast<unsigned>(elapsed > tout ? 0 : tout - elapsed), err);
            } while (err.isSuccess() && (m_lastCntrWrdsCnt < size) && (tmr.elapsed() < tout));
        }

        if (m_lastCntrWrdsCnt > 0) {
            const int cpy_wrds {m_lastCntrWrdsCnt < size ? (m_lastCntrWrdsCnt/frame_size) * frame_size : size};
            for (int i {0}; i < cpy_wrds; ++i) {
                const quint32 wrd {m_lastCntrData[i]};
                data[i].M = wrd & 0xFFFF;
                data[i].N = (wrd >> 16) & 0xFFFF;
            }

            m_lastCntrWrdsCnt -= cpy_wrds;
            if (m_lastCntrWrdsCnt > 0)  {
                memmove(m_lastCntrData.data(), &m_lastCntrData.data()[cpy_wrds], sizeof(m_lastCntrData[0]) * m_lastCntrWrdsCnt);
            }

            recvd_size = cpy_wrds;
        } else {
            recvd_size = 0;
        }
    }
}
