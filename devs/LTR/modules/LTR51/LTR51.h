#ifndef LQMEAS_LTR51_H
#define LQMEAS_LTR51_H



#include "ltr/include/ltr51api.h"
#include "lqmeas/devs/LTR/modules/LTRModule.h"
#include "lqmeas/ifaces/in/adc/DevAdc.h"
#include "lqmeas/ifaces/in/cntr/DevInCntr.h"
#include <memory>
#include <QMutex>

namespace LQMeas {
    class LTR51Config;
    class LTR51Info;
    class LTRModuleFrameReceiver;

    class LTR51 : public LTRModule, public DevAdc, public DevInCntr {
        Q_OBJECT
    public:
        static const quint16 typeModuleID {LTR_MID_LTR51};

        ~LTR51() override;
        bool isOpened() const override;
        /* ----------------------- интерфейс ввода  --------------------------*/
        DevAdc *devAdc() override {return static_cast<DevAdc*>(this);}
        DevSyncMarksRecv *devSyncMarkRecv() override;
        DevInCntr *devInCntr() override {return static_cast<DevInCntr*>(this);}

        QString errorString(int err) const override;
        bool adcIsRunning() const override {return m_run;}

        const LTR51Config &devspecConfig() const;
        QSharedPointer<const LTR51Info> devspecInfo() const;
    protected:
        TLTR* channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;

        INT ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) override;

        void protAdcEnable(LQError &err) override;
        void protAdcDisable(LQError &err) override;
        void protAdcStart(LQError &err) override;
        void protAdcStop(LQError &err) override;
        void protAdcGetData(double *data, int size, unsigned flags,
                            unsigned tout, int &recvd_size, LQError &err) override;

        void protInCntrEnable(LQError &err) override;
        void protInCntrDisable(LQError &err) override;
        void protInCntrStart(LQError &err) override;
        void protInCntrStop(LQError &err) override;
        void protInCntrGetData(DevInCntrData *data, int size, unsigned flags,
                               unsigned tout, int &recvd_size, LQError &err) override;
    private:
        explicit LTR51(LTRCrate *crate, int slot, QObject *parent = nullptr);
        void acqStart(LQError &err);
        void acqStop(LQError &err);

        void rxNewData(int frames_cnt, unsigned tout, LQError &err);

        mutable TLTR51 m_hnd;
        const std::unique_ptr<LTRModuleFrameReceiver> m_receiver;

        bool m_freq_en {false};
        bool m_cntr_en {false};
        bool m_freq_run {false};
        bool m_cntr_run {false};
        bool m_run {false};
        mutable QMutex m_lock;
        QVector<double> m_lastFreqData;        
        QVector<DWORD> m_lastCntrData;
        int m_lastFreqWrdsCnt {0};
        int m_lastCntrWrdsCnt {0};

        friend class LTRResolver;
    };
}
#endif // LQMEAS_LTR51_H
