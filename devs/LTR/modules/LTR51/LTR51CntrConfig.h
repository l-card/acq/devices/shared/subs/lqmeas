#ifndef LQMEAS_LTR51CNTRCONFIG_H
#define LQMEAS_LTR51CNTRCONFIG_H

#include "lqmeas/ifaces/in/cntr/DevInCntrConfig.h"
#include "LTR51TypeInfo.h"

namespace LQMeas {
    class LTR51Config;

    class LTR51CntrConfig : public DevInCntrConfig {
        Q_OBJECT
    public:
        bool inCntrChEnabled(int ch_num) const override;
        double inCntrFreq() const override;
        bool inCntrExternalStart() const override;

        bool cntrRecvEnabled() const {return m_cntrs_en;}
    public Q_SLOTS:
        void setCntrRecvEnabled(bool en);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        friend class LTR51Config;

        LTR51CntrConfig(LTR51Config &devcfg, const DevInCntrInfo &info);
        LTR51CntrConfig(LTR51Config &devcfg, const LTR51CntrConfig &cfg);

        LTR51Config *m_devcfg;
        bool m_cntrs_en {false};
    };
}

#endif // LQMEAS_LTR51CNTRCONFIG_H
