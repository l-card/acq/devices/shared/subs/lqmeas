#include "LTR51MezzanineTypeInfo.h"


const LQMeas::LTR51MezzanineTypeInfo &LQMeas::LTR51MezzanineTypeInfo::FLFH() {
    static const class LTR51MezzanineTypeInfoFLFH : public LTR51MezzanineTypeInfo {
        QString idName() const override {return QStringLiteral("FLFH");}
        QString displayName() const override {return QStringLiteral("H-51FL/FH");}
        int channelsCnt() const override {return 2;}
    } info;
    return info;
}

const QList<const LQMeas::LTR51MezzanineTypeInfo *> &LQMeas::LTR51MezzanineTypeInfo::allTypes() {
    static const QList<const LTR51MezzanineTypeInfo *> list {
        &FLFH()
    };
    return list;
}
