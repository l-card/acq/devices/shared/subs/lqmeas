#ifndef LQMEAS_LTR51MEZZANINETYPEINFO_H
#define LQMEAS_LTR51MEZZANINETYPEINFO_H

#include <QString>
#include <QList>

namespace LQMeas {
    class LTR51MezzanineTypeInfo {
    public:
        virtual QString idName() const = 0;
        virtual QString displayName() const = 0;
        virtual int channelsCnt() const = 0;

        static const LTR51MezzanineTypeInfo &FLFH();

        static const QList<const LTR51MezzanineTypeInfo *> &allTypes();
    };
}

#endif // LQMEAS_LTR51MEZZANINETYPEINFO_H
