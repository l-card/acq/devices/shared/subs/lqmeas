#include "LTR51CntrConfig.h"
#include "LTR51Config.h"
#include "LTR51AdcConfig.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String &cfgkey_enabled         {StdConfigKeys::enabled()};


    bool LTR51CntrConfig::inCntrChEnabled(int ch_num) const {
        return m_devcfg->adc().adcChEnabled(ch_num) && m_cntrs_en;
    }

    double LTR51CntrConfig::inCntrFreq() const {
        return 1000./m_devcfg->adc().adcBaseTime();
    }

    bool LTR51CntrConfig::inCntrExternalStart() const  {
        return m_devcfg->adc().adcExternalStart();
    }

    void LTR51CntrConfig::setCntrRecvEnabled(bool en) {
        if (en != m_cntrs_en) {
            m_cntrs_en = en;
            notifyChConfigChanged(-1);
            notifyConfigChanged();
        }
    }

    void LTR51CntrConfig::protSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_enabled] = m_cntrs_en;
    }

    void LTR51CntrConfig::protLoad(const QJsonObject &cfgObj) {
        m_cntrs_en = cfgObj[cfgkey_enabled].toBool();
    }

    LTR51CntrConfig::LTR51CntrConfig(LTR51Config &devcfg, const DevInCntrInfo &info) :
        DevInCntrConfig{info}, m_devcfg{&devcfg}  {

    }

    LTR51CntrConfig::LTR51CntrConfig(LTR51Config &devcfg, const LTR51CntrConfig &cfg) :
        DevInCntrConfig{cfg}, m_devcfg{&devcfg},
        m_cntrs_en{cfg.m_cntrs_en} {

    }
}
