#include "LTR51TypeInfo.h"
#include "lqmeas/units/std/Special.h"
#include "lqmeas/units/std/Frequency.h"
#include "ltr/include/ltr51api.h"
namespace LQMeas {
    const QString &LTR51TypeInfo::name() {
        static const QString str {QStringLiteral("LTR51")};
        return str;
    }
    int LTR51TypeInfo::adcChRangesCnt(int ch_num, int unit_mode_num) const {
        Q_UNUSED(unit_mode_num)
        return adcChAvailable(ch_num) ? 1 : 0;
    }

    double LTR51TypeInfo::adcChRangeMaxVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(range) Q_UNUSED(unit_mode_num)
        return adcChAvailable(ch_num) ? LTR51_FS_MAX/2 : 0;
    }

    double LTR51TypeInfo::adcChRangeMinVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(range) Q_UNUSED(unit_mode_num)
        return 0;
    }

    bool LTR51TypeInfo::adcChAvailable(int ch_num) const {
        int mezzanine_num = ch_num / adc_channesls_per_mezzanine;
        int mezzanine_ch  = ch_num % adc_channesls_per_mezzanine;
        return m_mezzanines.at(mezzanine_num) && (mezzanine_ch < m_mezzanines.at(mezzanine_num)->channelsCnt());
    }

    const Unit &LTR51TypeInfo::adcChRangeUnit(int ch_num, int unit_mode_num) const {
        Q_UNUSED(unit_mode_num)
        return adcChAvailable(ch_num) ? Units::Frequency::Hz() : Units::Special::unknown();
    }

    double LTR51TypeInfo::inCntrFreqMax() const  {
        return LTR51_FS_MAX/(LTR51_BASE_VAL_MIN);
    }

    LTR51TypeInfo::LTR51TypeInfo(const QList<const LTR51MezzanineTypeInfo *> &mezzanines) :
        m_dyn_allocated{true} {

        for (int mez_idx {0}; mez_idx < mezzanines_cnt; ++mez_idx) {
            m_mezzanines.append(mez_idx < mezzanines.size() ? mezzanines.at(mez_idx) : nullptr);
        }
    }

    const DeviceTypeInfo *LTR51TypeInfo::clone() const {
        return isDynamicAllocated() ? new LTR51TypeInfo{m_mezzanines} : this;
    }

    QList<const DeviceTypeInfo *> LTR51TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list {
                                                        &defaultTypeInfo()
        };
        return list;
    }

    LTR51TypeInfo::LTR51TypeInfo() : m_dyn_allocated{false} {
        for (int mez_idx {0}; mez_idx < mezzanines_cnt; ++mez_idx) {
            m_mezzanines.append(nullptr);
        }
    }
    
    const LTR51TypeInfo &LTR51TypeInfo::defaultTypeInfo() {
        static const LTR51TypeInfo info;
        return info;
    }

    double LTR51TypeInfo::adcFreqMax() const {
        return LTR51_FS_MAX/(2*LTR51_BASE_VAL_MIN);
    }

    double LTR51TypeInfo::adcFreqDefault() const   {
        return 50;
    }


}
