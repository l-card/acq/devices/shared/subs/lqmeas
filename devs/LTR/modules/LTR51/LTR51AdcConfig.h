#ifndef LQMEAS_LTR51ADCCONFIG_H
#define LQMEAS_LTR51ADCCONFIG_H

#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "LTR51TypeInfo.h"


namespace LQMeas {
    class LTR51AdcConfig : public DevAdcConfig {
        Q_OBJECT
    public:
        enum class ThresholdRange {
            U_10 = 0,
            U_1_2,
        };

        enum class EdgeMode {
            Rise = 0,
            Fall
        };

        bool adcChEnabled(int ch_num) const override  {return m_params.ch[ch_num].enabled;}
        EdgeMode adcChEdgeMode(int ch_num) const {return m_params.ch[ch_num].edgeMode;}
        ThresholdRange  adcChThresholdRange(int ch_num) const {return m_params.ch[ch_num].threshRange;}
        double   adcChThresholdHigh(int ch_num) const {return m_results.ch[ch_num].threshHigh;}
        double   adcChThresholdLow(int ch_num) const {return m_results.ch[ch_num].threshLow;}

        int adcChRangeNum(int ch_num) const  override {return 0;}
        double adcChRangeMaxVal(int ch_num) const override;
        double adcChRangeMinVal(int ch_num) const override;
        double adcFreq() const override;
        double adcCntrsValsFreq() const;

        int adcSampleSize() const override {return 8;}

        bool adcCustomFsParEnabled() const {return m_params.customFsEn;}
        double adcFs() const {return m_results.fs;}
        double adcBaseTime() const {return m_results.tbase_ms;}
        double adcAcqTime() const {return m_results.tacq_ms;}
    public Q_SLOTS:
        void adcSetCustomFsParEnabled(bool en);
        void adcSetFs(double fs);
        void adcSetBaseTime(double ms);
        void adcSetAcqTime(double ms);

        void adcSetChEnabled(int ch_num, bool en);
        void adcSetChEdgeMode(int ch_num, LTR51AdcConfig::EdgeMode mode);
        void adcSetChThresholdRange(int ch_num, LTR51AdcConfig::ThresholdRange range);
        void adcSetChThresholdHigh(int ch_num, double val);
        void adcSetChThresholdLow(int ch_num, double val);
    protected:
        void protUpdate() override;
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        LTR51AdcConfig(const DevAdcInfo &info);
        LTR51AdcConfig(const LTR51AdcConfig &cfg);

        void updateThreshold(int ch_num);

        static constexpr double defaultThreshHigh()   {return 0.1;}
        static constexpr double defaultThreshLow()    {return -0.1;}
        static constexpr double defaultFs()           {return 500000;}
        static constexpr double defaultTBaseMs()      {return 10;}
        static constexpr int    defaultTAcqPeriods()  {return 2;}

        struct {
            struct {
                bool            enabled {false};
                EdgeMode        edgeMode {EdgeMode::Rise};
                ThresholdRange  threshRange {ThresholdRange::U_10};
                double          threshHigh {defaultThreshHigh()};
                double          threshLow  {defaultThreshLow()};
            } ch[LTR51TypeInfo::adc_channels_cnt];

            bool    customFsEn;
            double  fs;
            double  tbase_ms;
            double  tacq_ms;
        } m_params;
        struct {
            struct {
                double    threshHigh;
                double    threshLow;
            } ch[LTR51TypeInfo::adc_channels_cnt];
            double  fs;
            double  tbase_ms;
            double  tacq_ms;
        } m_results;

        friend class LTR51Config;
    };
}

#endif // LQMEAS_LTR51ADCCONFIG_H
