#ifndef LQMEAS_LTR51TYPEINFO_H
#define LQMEAS_LTR51TYPEINFO_H

#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"
#include "lqmeas/ifaces/in/adc/DevAdcInfo.h"
#include "lqmeas/ifaces/in/cntr/DevInCntrInfo.h"
#include "LTR51MezzanineTypeInfo.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvInfo.h"

namespace LQMeas {
    class LTR51TypeInfo : public LTRModuleTypeInfo, public DevAdcInfo, public DevInCntrInfo, public LTRSyncMarksRecvInfo {

    public:
        static const QString &name();
        static const LTR51TypeInfo &defaultTypeInfo();

        const DevAdcInfo *adc() const override {
            return static_cast<const DevAdcInfo *>(this);
        }
        const DevInCntrInfo *inCntr() const override {
            return static_cast<const DevInCntrInfo *>(this);
        }
        const DevSyncMarksRecvInfo *syncMarkRecv() const override {
            return static_cast<const DevSyncMarksRecvInfo *>(this);
        }

        static const int adc_channels_cnt {16};
        static const int mezzanines_cnt {8};
        static const int adc_channesls_per_mezzanine {2};

        QString deviceTypeName() const override {return name();}

        AdcType adcType() const override {return AdcType::Parallel;}
        int adcChannelsCnt() const override {return 16;}
        double adcFreqMax() const override;
        double adcFreqDefault() const override;
        bool adcIsChFreqConfigurable() const override {return false;}

        int adcChRangesCnt(int ch_num, int unit_mode_num = 0) const override;
        double adcChRangeMaxVal(int ch_num, int range, int unit_mode_num = 0) const override;
        double adcChRangeMinVal(int ch_num, int range, int unit_mode_num = 0) const override;
        bool adcChAvailable(int ch_num) const override;
        const Unit &adcChRangeUnit(int ch_num, int unit_mode_num = 0) const override;

        int inCntrChannelsCnt() const override {return adcChannelsCnt();}
        bool inCntrChAviable(int ch_num) const override { return adcChAvailable(ch_num); }
        double inCntrFreqMax(void) const override;



        const LTR51MezzanineTypeInfo *mezzanine(int mez_idx) const {return m_mezzanines.at(mez_idx);}

        explicit LTR51TypeInfo(const QList<const LTR51MezzanineTypeInfo *> &mezzanines);
        const DeviceTypeInfo *clone() const override;

        QList<const DeviceTypeInfo *> modificationList() const override;
    private:
        LTR51TypeInfo();

        bool isDynamicAllocated() const override {return m_dyn_allocated;}

        QList<const LTR51MezzanineTypeInfo *> m_mezzanines;
        const bool m_dyn_allocated;
    };
}

#endif // LQMEAS_LTR51TYPEINFO_H
