#include "LTR51Info.h"
#include <QJsonObject>
#include <QJsonArray>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas{
    static const QLatin1String &cfgkey_mezzanines_group     {StdConfigKeys::mezzanines()};
    static const QLatin1String &cfgkey_mezzanine_key_type   {StdConfigKeys::type()};

    static const QLatin1String &cfgkey_mcu_ver              {StdConfigKeys::mcuVer()};
    static const QLatin1String &cfgkey_fpga_ver             {StdConfigKeys::fpgaVer()};

    LTR51Info::LTR51Info(const LTR51TypeInfo &type, const QString &serial, const QString &mcu_ver, const QString & fpga_ver) :
        DeviceInfo{type, serial},
        m_mcu_ver{mcu_ver},
        m_fpga_ver{fpga_ver} {

        for (int mez_idx {0}; mez_idx < LTR51TypeInfo::mezzanines_cnt; ++mez_idx) {
            m_mezzanines.append(nullptr);
        }
    }

    LTR51Info::LTR51Info(QList<LTR51MezzanineInfo *> mezzanines, const QString &serial, const QString &mcu_ver, const QString & fpga_ver) :
        DeviceInfo{LTR51TypeInfo{mezzaninesTypesList(mezzanines)}, serial},
        m_mcu_ver{mcu_ver},
        m_fpga_ver{fpga_ver} {

        for (int mez_idx {0}; mez_idx < LTR51TypeInfo::mezzanines_cnt; ++mez_idx) {
            m_mezzanines.append(mez_idx < mezzanines.size() ? mezzanines.at(mez_idx) : nullptr);
        }
    }

    LTR51Info::~LTR51Info() {
        qDeleteAll(m_mezzanines);
    }



    void LTR51Info::saveMezzanineInfo(QJsonObject &infoObj, const QList<LTR51MezzanineInfo *> &mezInfoList)  {
        QJsonArray mezArray;
        for (int mez_idx {0}; mez_idx < LTR51TypeInfo::mezzanines_cnt; ++mez_idx) {
            QJsonObject mezObj;
            LTR51MezzanineInfo *mezInfo {mez_idx < mezInfoList.size() ? mezInfoList.at(mez_idx) : nullptr};
            if (mezInfo) {
                mezObj[cfgkey_mezzanine_key_type] = mezInfo->type().idName();
            }
            mezArray.append(mezObj);
        }
        infoObj[cfgkey_mezzanines_group] = mezArray;
    }

    void LTR51Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_mcu_ver] = m_mcu_ver;
        infoObj[cfgkey_fpga_ver] = m_fpga_ver;
        saveMezzanineInfo(infoObj, m_mezzanines);
    }

    void LTR51Info::protLoad(const QJsonObject &infoObj) {
        m_mcu_ver = infoObj[cfgkey_mcu_ver].toString();
        m_fpga_ver = infoObj[cfgkey_fpga_ver].toString();

        const QJsonArray &mezArray {infoObj[cfgkey_mezzanines_group].toArray()};
        qDeleteAll(m_mezzanines);
        for (int mez_idx {0}; mez_idx < LTR51TypeInfo::mezzanines_cnt; ++mez_idx) {
            LTR51MezzanineInfo *info {nullptr};
            if (mez_idx < mezArray.size()) {
                const QJsonObject &mezObj {mezArray.at(mez_idx).toObject()};
                const QString &mezTypeId {mezObj[cfgkey_mezzanine_key_type].toString()};
                const QList<const LTR51MezzanineTypeInfo *> &mezTypes {LTR51MezzanineTypeInfo::allTypes()};
                const auto &it {std::find_if(mezTypes.cbegin(), mezTypes.cend(),
                                            [&mezTypeId](const auto mezType){return mezType->idName() == mezTypeId;})};
                if (it != mezTypes.cend()) {
                    info = new LTR51MezzanineInfo{*(*it)};
                }
            }
            m_mezzanines[mez_idx] = info;
        }
        setType(new LTR51TypeInfo{mezzaninesTypesList(m_mezzanines)});
    }

    QList<const LTR51MezzanineTypeInfo *> LTR51Info::mezzaninesTypesList(
            const QList<LTR51MezzanineInfo *> &infoList) {
        QList<const LTR51MezzanineTypeInfo *> ret;
        for (LTR51MezzanineInfo *info : infoList) {
            ret.append(info ? &info->type() : nullptr);
        }
        return ret;
    }

    LTR51Info::LTR51Info(const LTR51Info &info):
        DeviceInfo{info},
        m_mcu_ver{info.m_mcu_ver},
        m_fpga_ver{info.m_fpga_ver} {
        for (int mez_idx {0}; mez_idx < LTR51TypeInfo::mezzanines_cnt; ++mez_idx) {
            m_mezzanines.append(info.m_mezzanines.at(mez_idx) ?
                                new LTR51MezzanineInfo{*info.m_mezzanines.at(mez_idx)} : nullptr);
        }
    }
}
