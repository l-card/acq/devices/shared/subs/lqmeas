#ifndef LQMEAS_LTR51INFO_H
#define LQMEAS_LTR51INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTR51TypeInfo.h"
#include "LTR51MezzanineInfo.h"

namespace LQMeas {
    class LTR51Info : public DeviceInfo {
    public:
        explicit LTR51Info(const LTR51TypeInfo &type = LTR51TypeInfo::defaultTypeInfo(),
                  const QString &serial = QString{}, const QString &mcu_ver = QString{}, const QString & fpga_ver = QString{});
        explicit LTR51Info(QList<LTR51MezzanineInfo *> mezzanines,
                  const QString &serial = QString{}, const QString &mcu_ver = QString{}, const QString & fpga_ver = QString{});
        ~LTR51Info() override;


        DeviceInfo *clone() const override {return new LTR51Info{*this};}

        const QString mcuVerStr() const {return m_mcu_ver;}
        const QString fpgaVerStr() const {return m_fpga_ver;}


        const LTR51MezzanineInfo *mezzanine(int mez_idx) const {
            return m_mezzanines.at(mez_idx);
        }

        const LTR51TypeInfo &devspecTypeInfo() const {
            return static_cast<const LTR51TypeInfo &>(type());
        }

        static void saveMezzanineInfo(QJsonObject &infoObj, const QList<LTR51MezzanineInfo *> &mezInfoList);
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        static QList<const LTR51MezzanineTypeInfo *> mezzaninesTypesList(
                const QList<LTR51MezzanineInfo *> &infoList);

        LTR51Info(const LTR51Info &info);

        QString m_mcu_ver;
        QString m_fpga_ver;
        QList<LTR51MezzanineInfo *> m_mezzanines;
    };
}

#endif // LQMEAS_LTR51INFO_H
