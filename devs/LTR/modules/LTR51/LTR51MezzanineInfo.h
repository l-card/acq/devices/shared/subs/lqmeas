#ifndef LQMEAS_LTR51MEZZANINEINFO_H
#define LQMEAS_LTR51MEZZANINEINFO_H

#include "LTR51MezzanineTypeInfo.h"

namespace LQMeas {
    class LTR51MezzanineInfo  {
    public:
        explicit LTR51MezzanineInfo(const LTR51MezzanineTypeInfo &type) : m_type{&type} {
        }

        LTR51MezzanineInfo(const LTR51MezzanineInfo &info) :
            m_type{&info.type()} {
        }

        const LTR51MezzanineTypeInfo &type() const {return *m_type;}
    private:
        const LTR51MezzanineTypeInfo *const m_type;
    };
}

#endif // LQMEAS_LTR51MEZZANINEINFO_H
