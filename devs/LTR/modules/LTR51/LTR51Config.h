#ifndef LQMEAS_LTR51CONFIG_H
#define LQMEAS_LTR51CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"
#include <memory>

namespace LQMeas {
    class LTR51AdcConfig;
    class LTR51CntrConfig;
    class LTRSyncMarksRecvConfig;

    class LTR51Config : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        const DevAdcConfig *adcConfig() const override;
        const DevInCntrConfig *inCntrConfig() const override;
        const DevSyncMarksRecvConfig *syncMarksRecvConfig() const override;

        LTR51AdcConfig &adc() {return *m_adc;}
        const LTR51AdcConfig &adc() const {return *m_adc;}
        LTR51CntrConfig &cntr() {return *m_cntr;}
        const LTR51CntrConfig &cntr() const {return *m_cntr;}
        LTRSyncMarksRecvConfig &marksRecv() {return *m_syncRecv;}
        const LTRSyncMarksRecvConfig &marksRecv() const {return *m_syncRecv;}

        DeviceConfig *clone() const override {
            return new LTR51Config{*this};
        }

        double inPeakSpeed() const override;

        LTR51Config();
        LTR51Config(const LTR51Config &cfg);
        ~LTR51Config() override;
    protected:
        void protUpdate() override;
    private:
        const std::unique_ptr<LTR51AdcConfig> m_adc;
        const std::unique_ptr<LTR51CntrConfig> m_cntr;
        const std::unique_ptr<LTRSyncMarksRecvConfig> m_syncRecv;
    };
}


#endif // LQMEAS_LTR51CONFIG_H
