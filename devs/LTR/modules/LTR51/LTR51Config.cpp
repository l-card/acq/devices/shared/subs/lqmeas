#include "LTR51Config.h"
#include "LTR51AdcConfig.h"
#include "LTR51CntrConfig.h"
#include "LTR51TypeInfo.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvConfig.h"

namespace LQMeas {

    const QString &LTR51Config::typeConfigName() {
        return LTR51TypeInfo::name();
    }

    const DevAdcConfig *LTR51Config::adcConfig() const  {
        return m_adc.get();
    }

    const DevInCntrConfig *LTR51Config::inCntrConfig() const {
        return m_cntr.get();
    }

    const DevSyncMarksRecvConfig *LTR51Config::syncMarksRecvConfig() const {
        return m_syncRecv.get();
    }

    double LTR51Config::inPeakSpeed() const {
        /* скорость вычисляется исходя из скорости передачи счетчиков по всем каналам */
        return m_adc->adcEnabledChCnt() > 0 ?  m_adc->adcCntrsValsFreq() * m_adc->adcSampleSize() * LTR51TypeInfo::adc_channels_cnt : 0;
    }

    LTR51Config::LTR51Config() :
        m_adc{new LTR51AdcConfig{LTR51TypeInfo::defaultTypeInfo()}},
        m_cntr{new LTR51CntrConfig{*this, LTR51TypeInfo::defaultTypeInfo()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this}} {

        init();
    }

    LTR51Config::LTR51Config(const LTR51Config &cfg) :
        m_adc{new LTR51AdcConfig{cfg.adc()}},
        m_cntr{new LTR51CntrConfig{*this, cfg.cntr()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this, cfg.marksRecv()}} {

        init();
    }

    void LTR51Config::protUpdate() {
        m_syncRecv->updateSyncMarksRecvFreq(m_adc->adcCntrsValsFreq());
    }

    LQMeas::LTR51Config::~LTR51Config() {

    }
}
