#include "LTR51AdcConfig.h"
#include "LTR51TypeInfo.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/StdConfigKeys.h"
#include "ltr/include/ltr51api.h"
#include <QJsonArray>
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String &cfgkey_ch_enabled         {StdConfigKeys::enabled()};
    static const QLatin1String &cfgkey_ch_edge            {StdConfigKeys::edge()};
    static const QLatin1String &cfgkey_ch_thresh_group    {StdConfigKeys::threshold()};
    static const QLatin1String &cfgkey_ch_thresh_range    {StdConfigKeys::range()};
    static const QLatin1String &cfgkey_ch_thresh_high     {StdConfigKeys::high()};
    static const QLatin1String &cfgkey_ch_thresh_low      {StdConfigKeys::low()};
    static const QLatin1String &cfgkey_channels_arr       {StdConfigKeys::channels()};
    static const QLatin1String cfgkey_custom_fs_en        {"CustomFsEn"};
    static const QLatin1String cfgkey_fs                  {"Fs"};
    static const QLatin1String cfgkey_tbase               {"Tbase"};
    static const QLatin1String cfgkey_tacq                {"Tacq"};

    static const double f_fref {LTR51_REF_FREQ};


    typedef EnumConfigKey<LTR51AdcConfig::EdgeMode> LTR51ChEdgeModeKey;
    static const LTR51ChEdgeModeKey ch_edge_modes[] = {
        {QStringLiteral("rise"),                     LTR51AdcConfig::EdgeMode::Rise},
        {QStringLiteral("fall"),                     LTR51AdcConfig::EdgeMode::Fall},
        {LTR51ChEdgeModeKey::defaultKey(),           LTR51AdcConfig::EdgeMode::Rise},
    };

    typedef EnumConfigKey<LTR51AdcConfig::ThresholdRange> LTR51ChThresholdRangeKey;
    static const LTR51ChThresholdRangeKey ch_thresh_ranges[] = {
        {QStringLiteral("U10"),                     LTR51AdcConfig::ThresholdRange::U_10},
        {QStringLiteral("U1.2"),                    LTR51AdcConfig::ThresholdRange::U_1_2},
        {LTR51ChThresholdRangeKey::defaultKey(),    LTR51AdcConfig::ThresholdRange::U_10},
    };


    double LTR51AdcConfig::adcChRangeMaxVal(int ch_num) const {
        return m_results.fs/2;
    }

    double LTR51AdcConfig::adcChRangeMinVal(int ch_num) const {
        return 0;
    }

    double LTR51AdcConfig::adcFreq() const {
        return 1000./m_results.tacq_ms;
    }

    double LTR51AdcConfig::adcCntrsValsFreq() const {
        return 1000./m_results.tbase_ms;
    }

    void LTR51AdcConfig::adcSetCustomFsParEnabled(bool en) {
        if (m_params.customFsEn != en) {
            m_params.customFsEn = en;
            notifyConfigChanged();
        }
    }

    void LTR51AdcConfig::adcSetFs(double fs) {
        if (!LQREAL_IS_EQUAL(fs, m_params.fs)) {
            m_params.fs = fs;
            notifyConfigChanged();
        }
    }

    void LTR51AdcConfig::adcSetBaseTime(double ms) {
        if (!LQREAL_IS_EQUAL(ms, m_params.tbase_ms)) {
            m_params.tbase_ms = ms;
            notifyConfigChanged();
        }
    }

    void LTR51AdcConfig::adcSetAcqTime(double ms) {
        if (!LQREAL_IS_EQUAL(ms, m_params.tacq_ms)) {
            m_params.tacq_ms = ms;
            notifyConfigChanged();
        }
    }

    void LTR51AdcConfig::adcSetChEnabled(int ch_num, bool en) {
        if (m_params.ch[ch_num].enabled != en) {
            m_params.ch[ch_num].enabled = en;
            notifyChEnableStateChanged(ch_num);
            notifyChConfigChanged(ch_num);
            notifyConfigChanged();
        }
    }

    void LTR51AdcConfig::adcSetChEdgeMode(int ch_num, EdgeMode mode)  {
        if (m_params.ch[ch_num].edgeMode != mode) {
            m_params.ch[ch_num].edgeMode = mode;
            notifyChConfigChanged(ch_num);
            notifyConfigChanged();
        }
    }

    void LTR51AdcConfig::adcSetChThresholdRange(int ch_num, ThresholdRange range) {
        if (m_params.ch[ch_num].threshRange != range) {
            m_params.ch[ch_num].threshRange = range;
            notifyChConfigChanged(ch_num);
            notifyConfigChanged();
        }
    }

    void LTR51AdcConfig::adcSetChThresholdHigh(int ch_num, double val) {
        if (!LQREAL_IS_EQUAL(m_params.ch[ch_num].threshHigh, val)) {
            m_params.ch[ch_num].threshHigh = val;
            updateThreshold(ch_num);
            notifyChConfigChanged(ch_num);
            notifyConfigChanged();
        }
    }

    void LTR51AdcConfig::adcSetChThresholdLow(int ch_num, double val) {
        if (!LQREAL_IS_EQUAL(m_params.ch[ch_num].threshLow, val)) {
            m_params.ch[ch_num].threshLow = val;
            updateThreshold(ch_num);
            notifyChConfigChanged(ch_num);
            notifyConfigChanged();
        }
    }

    void LTR51AdcConfig::protUpdate() {
        DevAdcConfig::protUpdate();
        for (int ch_num {0}; ch_num < LTR51TypeInfo::adc_channels_cnt; ++ch_num) {
            updateThreshold(ch_num);
        }

        double fs;
        double tbase_ms;


        if (!m_params.customFsEn) {
             fs = defaultFs();
             tbase_ms = defaultTBaseMs();
        } else {
            fs = m_params.fs;
            if (fs > 0) {
                if (fs > LTR51_FS_MAX) {
                    fs = LTR51_FS_MAX;
                } else if (fs < f_fref/LTR51_FSDIV_MAX) {
                    fs = f_fref/LTR51_FSDIV_MAX;
                }
            } else {
                fs = LTR51_FS_MAX;
            }
            const unsigned fsdiv {static_cast<unsigned>(f_fref/fs + 0.5)};
            fs = f_fref/fsdiv;

            const unsigned base {qMin<unsigned>(qMax<unsigned>(static_cast<unsigned>(fs * m_params.tbase_ms / 1000 + 0.5),
                                           LTR51_BASE_VAL_MIN), LTR51_BASE_VAL_MAX)};
            tbase_ms = 1000. * base / fs;
        }


        const unsigned acq_cnt {qMax<unsigned>(static_cast<unsigned>(m_params.tacq_ms/tbase_ms + 0.5), 2)};
        const double tacq_ms =  tbase_ms * acq_cnt;

        if (!LQREAL_IS_EQUAL(m_results.fs, fs)
                || !LQREAL_IS_EQUAL(m_results.tbase_ms, tbase_ms)
                || !LQREAL_IS_EQUAL(m_results.tacq_ms, tacq_ms)
                ) {
            m_results.fs = fs;
            m_results.tbase_ms = tbase_ms;
            m_results.tacq_ms = tacq_ms;
            notifyConfigChanged();
        }
    }

    void LTR51AdcConfig::protSave(QJsonObject &cfgObj) const {
        QJsonArray chArray;
        for (int ch {0}; ch < LTR51TypeInfo::adc_channels_cnt; ++ch) {
            QJsonObject chObj;
            chObj[cfgkey_ch_enabled] = m_params.ch[ch].enabled;
            chObj[cfgkey_ch_edge] = LTR51ChEdgeModeKey::getKey(ch_edge_modes, m_params.ch[ch].edgeMode);
            QJsonObject threshObj;
            threshObj[cfgkey_ch_thresh_range] = LTR51ChThresholdRangeKey::getKey(ch_thresh_ranges, m_params.ch[ch].threshRange);
            threshObj[cfgkey_ch_thresh_high]  = m_params.ch[ch].threshHigh;
            threshObj[cfgkey_ch_thresh_low]   = m_params.ch[ch].threshLow;
            chObj[cfgkey_ch_thresh_group] = threshObj;
            chArray.append(chObj);
        }
        cfgObj[cfgkey_channels_arr] = chArray;

        cfgObj[cfgkey_custom_fs_en] = m_params.customFsEn;
        cfgObj[cfgkey_fs]  = m_params.fs;
        cfgObj[cfgkey_tbase]  = m_params.tbase_ms;
        cfgObj[cfgkey_tacq]  = m_params.tacq_ms;
    }

    void LTR51AdcConfig::protLoad(const QJsonObject &cfgObj)  {
        const QJsonArray &chArray {cfgObj[cfgkey_channels_arr].toArray()};
        const int ch_cnt {chArray.size()};
        for (int ch {0}; (ch < ch_cnt) && (ch < LTR51TypeInfo::adc_channels_cnt); ++ch) {
            const QJsonObject &chObj {chArray.at(ch).toObject()};

            m_params.ch[ch].enabled = chObj[cfgkey_ch_enabled].toBool();
            m_params.ch[ch].edgeMode = LTR51ChEdgeModeKey::getValue(ch_edge_modes, chObj[cfgkey_ch_edge].toString());
            const QJsonObject &threshObj {chObj[cfgkey_ch_thresh_group].toObject()};
            m_params.ch[ch].threshRange = LTR51ChThresholdRangeKey::getValue(ch_thresh_ranges, threshObj[cfgkey_ch_thresh_range].toString());
            m_params.ch[ch].threshHigh  = threshObj[cfgkey_ch_thresh_high].toDouble(defaultThreshHigh());
            m_params.ch[ch].threshLow   = threshObj[cfgkey_ch_thresh_low].toDouble(defaultThreshLow());
        }


        m_params.customFsEn = cfgObj[cfgkey_custom_fs_en].toBool();
        m_params.fs = cfgObj[cfgkey_fs].toDouble(defaultFs());
        m_params.tbase_ms = cfgObj[cfgkey_tbase].toDouble(defaultTBaseMs());
        m_params.tacq_ms  = cfgObj[cfgkey_tacq].toDouble(defaultTAcqPeriods()*m_params.tbase_ms);
    }

    LTR51AdcConfig::LTR51AdcConfig(const DevAdcInfo &info) : DevAdcConfig{info} {
        memset(&m_params, 0, sizeof(m_params));
        m_params.fs = m_results.fs = defaultFs();
        m_params.tbase_ms = m_results.tbase_ms = defaultTBaseMs();
        m_params.tacq_ms = m_results.tacq_ms = defaultTAcqPeriods()*m_results.tbase_ms;
        for (int ch_num {0}; ch_num < LTR51TypeInfo::adc_channels_cnt; ++ch_num) {
            m_params.ch[ch_num].threshHigh = defaultThreshHigh();
            m_params.ch[ch_num].threshLow = defaultThreshLow();
            updateThreshold(ch_num);
        }

    }

    LTR51AdcConfig::LTR51AdcConfig(const LTR51AdcConfig &cfg)  : DevAdcConfig{cfg} {
        memcpy(&m_params, &cfg.m_params, sizeof(m_params));
        memcpy(&m_results, &cfg.m_results, sizeof(m_results));
    }

    void LTR51AdcConfig::updateThreshold(int ch_num) {
        m_results.ch[ch_num].threshHigh = m_params.ch[ch_num].threshHigh;
        m_results.ch[ch_num].threshLow = m_params.ch[ch_num].threshLow;
        LTR51_CreateLChannel(0, &m_results.ch[ch_num].threshHigh, &m_results.ch[ch_num].threshLow, static_cast<INT>(m_params.ch[ch_num].threshRange), 0);
    }

}
