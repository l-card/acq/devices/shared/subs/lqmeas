#ifndef LQMEAS_LTR11INFO_H
#define LQMEAS_LTR11INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTR11TypeInfo.h"

namespace LQMeas {
    class LTR11Info : public DeviceInfo {
    public:
        LTR11Info(const LTR11TypeInfo &type = LTR11TypeInfo::defaultTypeInfo(),
                  const QString &serial = QString{}, int mcu_ver = 0);

        DeviceInfo *clone() const override;
        int mcuVer() const {return m_mcu_ver;}
        QString mcuVerStr() const;


        const LTR11TypeInfo &devspecTypeInfo() const {
            return static_cast<const LTR11TypeInfo &>(type());
        }
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        LTR11Info(const LTR11Info &info);

        int m_mcu_ver;
    };
}

#endif // LTR11INFO_H
