#include "LTR11Info.h"
#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String &cfgkey_mcu_ver              {StdConfigKeys::mcuVer()};

    LTR11Info::LTR11Info(const LTR11TypeInfo &type, const QString &serial, int mcu_ver) :
        DeviceInfo{type, serial}, m_mcu_ver{mcu_ver} {

    }

    LTR11Info::LTR11Info(const LTR11Info &info) : DeviceInfo{info}, m_mcu_ver{info.m_mcu_ver} {

    }

    DeviceInfo *LTR11Info::clone() const {
        return new LTR11Info{*this};
    }

    QString LTR11Info::mcuVerStr() const {
        return QString("%1.%2").arg((m_mcu_ver >> 8) & 0xFF).arg(m_mcu_ver & 0xFF);
    }

    void LTR11Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_mcu_ver] = m_mcu_ver;
    }

    void LTR11Info::protLoad(const QJsonObject &infoObj) {
        m_mcu_ver = infoObj[cfgkey_mcu_ver].toInt();
    }


}
