#include "LTR11TypeInfo.h"


namespace LQMeas {

    static const double f_ranges[] = {10., 2.5, 0.625, 0.156};

    const QString &LTR11TypeInfo::name() {
        static const QString str {QStringLiteral("LTR11")};
        return str;
    }

    int LTR11TypeInfo::adcChRangesCnt(int ch_num, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return sizeof(f_ranges)/sizeof(f_ranges[0]);
    }

    double LTR11TypeInfo::adcChRangeMaxVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
         return f_ranges[range];
    }

    double LTR11TypeInfo::adcChRangeMinVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return -f_ranges[range];
    }

    QList<const DeviceTypeInfo *> LTR11TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list { &defaultTypeInfo() };
        return list;
    }
    
    const LTR11TypeInfo &LTR11TypeInfo::defaultTypeInfo() {
        static const LTR11TypeInfo info;
        return info;
    }
}
