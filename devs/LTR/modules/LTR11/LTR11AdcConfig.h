#ifndef LQMEAS_LTR11ADCCONFIG_H
#define LQMEAS_LTR11ADCCONFIG_H

#include "lqmeas/ifaces/in/adc/DevAdcStdSeq32Config.h"

namespace LQMeas {
    class LTR11Config;

    class LTR11AdcConfig : public DevAdcStdSeq32Config {
        Q_OBJECT
    public:
        enum class AdcChRange {
            U_10 = 0,
            U_2_5,
            U_0_625,
            U_0_156
        };

        enum class StartMode {
            Internal,
            ExtRise,
            ExtFall
        };

        enum class ConvMode {
            Internal,
            ExtRise,
            ExtFall
        };


        bool adcExternalStart() const override;
        int adcSampleSize() const override {return 4;}

        StartMode startMode() const {return m_startMode;}
        ConvMode  convMode() const {return m_convMode;}
        AdcChRange adcChRange(int ch_num) const {return static_cast<AdcChRange>(adcChRangeNum(ch_num));}

        int typeAdcRangeNum(AdcChRange range) {return static_cast<int>(range);}
    public Q_SLOTS:
        void setStartMode(LTR11AdcConfig::StartMode mode);
        void setConvMode(LTR11AdcConfig::ConvMode mode);
        void adcSetChRange(int ch_num, LTR11AdcConfig::AdcChRange range) {
            adcSetChRangeNum(ch_num, typeAdcRangeNum(range));
        }
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
        void adcAdjustFreq(double &adcFreq, double &adcChFreq) override;
    private:
        explicit LTR11AdcConfig(const DevAdcInfo &info);
        LTR11AdcConfig(const LTR11AdcConfig &adcCfg);


        StartMode m_startMode;
        ConvMode m_convMode;

        friend class LTR11Config;
    };
}


Q_DECLARE_METATYPE(LQMeas::LTR11AdcConfig::StartMode);
Q_DECLARE_METATYPE(LQMeas::LTR11AdcConfig::ConvMode);

#endif // LQMEAS_LTR11ADCCONFIG_H
