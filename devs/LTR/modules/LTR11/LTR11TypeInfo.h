#ifndef LQMEAS_LTR11TYPEINFO_H
#define LQMEAS_LTR11TYPEINFO_H

#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"
#include "lqmeas/ifaces/in/adc/DevAdcInfo.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvInfo.h"

namespace LQMeas {
    class LTR11TypeInfo : public LTRModuleTypeInfo, public DevAdcInfo, public LTRSyncMarksRecvInfo {
    public:
        static const QString &name();
        static const LTR11TypeInfo &defaultTypeInfo();

        const DevAdcInfo *adc() const override {
            return static_cast<const DevAdcInfo *>(this);
        }
        const DevSyncMarksRecvInfo *syncMarkRecv() const override {
            return static_cast<const DevSyncMarksRecvInfo *>(this);
        }

        QString deviceTypeName() const override {return name();}

        AdcType adcType() const override {return AdcType::Sequential;}
        int adcChannelsCnt() const override {return 32;}
        double adcFreqMax() const override {return 400000;}
        double adcFreqDefault() const override {return 10000;}

        bool adcIsChFreqConfigurable() const override {return false;}

        int adcChRangesCnt(int ch_num, int unit_mode_num = 0) const override;
        double adcChRangeMaxVal(int ch_num, int range, int unit_mode_num = 0) const override;
        double adcChRangeMinVal(int ch_num, int range, int unit_mode_num = 0) const override;

        QList<const DeviceTypeInfo *> modificationList() const override;
    };
}

#endif // LQMEAS_LTR11TYPEINFO_H
