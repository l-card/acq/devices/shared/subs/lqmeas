#include "LTR11.h"
#include "LTR11Info.h"
#include "LTR11Config.h"
#include "LTR11AdcConfig.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdErrors.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "lqmeas/devs/LTR/modules/LTRModuleFrameReceiver.h"
#include "lqmeas/log/Log.h"


namespace LQMeas {
    LTR11::LTR11(LTRCrate *crate, int slot, QObject *parent) :
        LTRModule{crate, slot, typeModuleID, new LTR11Config{}, LTR11Info{}, parent},
        DevAdc{this, LTR11AdcConfig::adc_channels_cnt},
        m_receiver{new LTRModuleFrameReceiver{this}} {

        LTR11_Init(&m_hnd);
    }

    LTR11::~LTR11() {
        LTR11_Close(&m_hnd);
    }

    bool LTR11::isOpened() const {
        return LTR11_IsOpened(&m_hnd) == LTR_OK;
    }

    DevSyncMarksRecv *LTR11::devSyncMarkRecv() {
        return m_receiver.get();
    }

    QString LTR11::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTR11_GetErrorString(err));
    }

    const LTR11Config &LTR11::devspecConfig() const {
        return static_cast<const LTR11Config &>(config());
    }

    QSharedPointer<const LTR11Info> LTR11::devspecInfo() const {
        return devInfo().staticCast<const LTR11Info>();
    }

    TLTR *LTR11::channel() const {
        return &m_hnd.Channel;
    }

    int LTR11::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        Q_UNUSED(flags)
        INT ltr_err = LTR11_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, slot);
        if (ltr_err == LTR_OK) {
            ltr_err = LTR11_GetConfig(&m_hnd);
        }

        if (ltr_err == LTR_OK) {
            setDeviceInfo(LTR11Info(LTR11TypeInfo::defaultTypeInfo(),
                                    QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial), m_hnd.ModuleInfo.Ver));
        }
        return ltr_err;
    }

    void LTR11::protClose(LQError &err) {
        return getError(LTR11_Close(&m_hnd), err);
    }

    void LTR11::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTR11Config *setCfg {qobject_cast<const LTR11Config *>(&cfg)};
        if (setCfg) {
            LTR11_FindAdcFreqParams(setCfg->adc().adcFreq(), &m_hnd.ADCRate.prescaler,
                                    &m_hnd.ADCRate.divider, nullptr);
            m_hnd.ADCMode = LTR11_ADCMODE_ACQ;
            m_hnd.InpMode = setCfg->adc().convMode() == LTR11AdcConfig::ConvMode::ExtFall ? LTR11_INPMODE_EXTFALL :
                            setCfg->adc().convMode() == LTR11AdcConfig::ConvMode::ExtRise ? LTR11_INPMODE_EXTRISE : LTR11_INPMODE_INT ;
            m_hnd.StartADCMode = setCfg->adc().startMode() == LTR11AdcConfig::StartMode::ExtFall ? LTR11_STARTADCMODE_EXTFALL :
                                 setCfg->adc().startMode() == LTR11AdcConfig::StartMode::ExtRise ? LTR11_STARTADCMODE_EXTRISE :
                                                                                        LTR11_STARTADCMODE_INT;
            int ch_cnt {0};
            for (int ch {0}; ch < LTR11AdcConfig::adc_channels_cnt; ++ch) {
                if (setCfg->adc().adcChEnabled(ch)) {
                    const LTR11AdcConfig::AdcChMode mode {setCfg->adc().adcChMode(ch)};
                    BYTE mode_code = mode == LTR11AdcConfig::AdcChMode::Comm ? LTR11_CHMODE_COMM :
                                     (mode == LTR11AdcConfig::AdcChMode::Zero ? LTR11_CHMODE_ZERO : LTR11_CHMODE_DIFF);

                    m_hnd.LChTbl[ch_cnt] = LTR11_CreateLChannel(
                                static_cast<BYTE>(ch), mode_code,
                                static_cast<BYTE>(setCfg->adc().adcChRangeNum(ch)));
                    ++ch_cnt;
                }
            }
            m_hnd.LChQnt = ch_cnt;

            getError(LTR11_SetADC(&m_hnd), err);
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }

    INT LTR11::ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) {
        return LTR11_Recv(&m_hnd, wrds, marks, size, tout);
    }

    void LTR11::protAdcStart(LQError &err) {
        getError(LTR11_Start(&m_hnd), err);
        if (err.isSuccess()) {
            m_run = true;
            m_receiver->start(adcConfig().adcEnabledChCnt(), 1);
        }
    }

    void LTR11::protAdcStop(LQError &err) {
        getError(LTR11_Stop(&m_hnd), err);
        m_run = false;
        m_receiver->clear();
    }

    void LTR11::protAdcGetData(double *data, int size, unsigned flags,
                               unsigned tout, int &recvd_size, LQError &err) {
        int recvd_words {0};
        DWORD *wrds;
        INT proc_size {0};

        m_receiver->getFrames(size, tout, wrds, recvd_words, err);

        if (err.isSuccess() && (recvd_words > 0)) {
            proc_size = static_cast<INT>(recvd_words);

            getError(LTR11_ProcessData(&m_hnd, wrds, data, &proc_size, TRUE, TRUE), err);
            if (!err.isSuccess()) {
                LQMeasLog->error(tr("Process data error"), err, this);
            }
        }

        if (err.isSuccess())
            recvd_size = proc_size;
    }
}
