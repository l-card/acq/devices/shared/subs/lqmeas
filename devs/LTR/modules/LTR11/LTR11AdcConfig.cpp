#include "LTR11AdcConfig.h"
#include "ltr/include/ltr11api.h"
#include <QJsonObject>
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String cfgkey_sync_start_mode  {StdConfigKeys::syncStartMode()};
    static const QLatin1String cfgkey_sync_conv_mode   {StdConfigKeys::syncConvMode()};


    typedef EnumConfigKey<LTR11AdcConfig::StartMode> SyncModeKey;
    static const SyncModeKey start_modes[] {
        {QStringLiteral("int"),     LTR11AdcConfig::StartMode::Internal},
        {QStringLiteral("extrise"), LTR11AdcConfig::StartMode::ExtRise},
        {QStringLiteral("extfall"), LTR11AdcConfig::StartMode::ExtFall},
        {SyncModeKey::defaultKey(), LTR11AdcConfig::StartMode::Internal},
    };

    typedef EnumConfigKey<LTR11AdcConfig::ConvMode> ConvModeKey;
    static const ConvModeKey conv_modes[] {
        {QStringLiteral("int"),     LTR11AdcConfig::ConvMode::Internal},
        {QStringLiteral("extrise"), LTR11AdcConfig::ConvMode::ExtRise},
        {QStringLiteral("extfall"), LTR11AdcConfig::ConvMode::ExtFall},
        {ConvModeKey::defaultKey(), LTR11AdcConfig::ConvMode::Internal},
    };

    LTR11AdcConfig::LTR11AdcConfig(const DevAdcInfo &info) :  DevAdcStdSeq32Config(info) {
        m_convMode = ConvMode::Internal;
        m_startMode = StartMode::Internal;
    }

    LTR11AdcConfig::LTR11AdcConfig(const LTR11AdcConfig &adcCfg) :
        DevAdcStdSeq32Config(adcCfg) {

        m_convMode = adcCfg.m_convMode;
        m_startMode = adcCfg.m_startMode;
    }

    bool LQMeas::LTR11AdcConfig::adcExternalStart() const {
        return (m_startMode != LTR11AdcConfig::StartMode::Internal) ||
                (m_convMode != LTR11AdcConfig::ConvMode::Internal);
    }

    void LTR11AdcConfig::setStartMode(LTR11AdcConfig::StartMode mode) {
        if ((mode == StartMode::Internal) ||  (mode == StartMode::ExtRise) || (mode == StartMode::ExtFall)) {
            if (m_startMode != mode) {
                m_startMode = mode;
                notifyConfigChanged();
            }
        }
    }

    void LTR11AdcConfig::setConvMode(ConvMode mode)  {
        if ((mode == ConvMode::Internal) || (mode == ConvMode::ExtRise) || (mode == ConvMode::ExtFall)) {
            if (m_convMode != mode) {
                m_convMode = mode;
                notifyConfigChanged();
            }
        }
    }

    void LTR11AdcConfig::protSave(QJsonObject &cfgObj) const {
        DevAdcStdSeq32Config::protSave(cfgObj);

        cfgObj[cfgkey_sync_start_mode] = SyncModeKey::getKey(start_modes, m_startMode);
        cfgObj[cfgkey_sync_conv_mode]  = ConvModeKey::getKey(conv_modes, m_convMode);
    }

    void LTR11AdcConfig::protLoad(const QJsonObject &cfgObj) {
        DevAdcStdSeq32Config::protLoad(cfgObj);

        m_startMode = SyncModeKey::getValue(start_modes, cfgObj[cfgkey_sync_start_mode].toString());
        m_convMode = ConvModeKey::getValue(conv_modes, cfgObj[cfgkey_sync_conv_mode].toString());
    }

    void LTR11AdcConfig::adcAdjustFreq(double &adcFreq, double &adcChFreq) {
        LTR11_FindAdcFreqParams(adcFreq, nullptr, nullptr, &adcFreq);
        if (adcEnabledChCnt() > 0) {
            adcChFreq = adcFreq/adcEnabledChCnt();
        }
    }
}
