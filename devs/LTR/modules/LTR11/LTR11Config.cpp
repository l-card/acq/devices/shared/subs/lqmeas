#include "LTR11Config.h"
#include "LTR11TypeInfo.h"
#include "LTR11AdcConfig.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvConfig.h"

namespace LQMeas {

    const QString &LTR11Config::typeConfigName() {
        return LTR11TypeInfo::name();
    }

    const DevAdcConfig *LTR11Config::adcConfig() const {
        return m_adc.get();
    }

    const DevSyncMarksRecvConfig *LTR11Config::syncMarksRecvConfig() const {
        return m_syncRecv.get();
    }

    LTR11Config::LTR11Config() :
        m_adc{new LTR11AdcConfig{LTR11TypeInfo::defaultTypeInfo()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this}} {

        init();
    }

    LTR11Config::LTR11Config(const LTR11Config &cfg) :
        m_adc{new LTR11AdcConfig{cfg.adc()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this, cfg.marksRecv()}} {

        init();
    }

    void LTR11Config::protUpdate() {
        m_syncRecv->updateSyncMarksRecvFreq(m_adc->adcFreq());
    }

    LQMeas::LTR11Config::~LTR11Config() {

    }
}
