#ifndef LQMEAS_LTR11CONFIG_H
#define LQMEAS_LTR11CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"
#include <memory>

namespace LQMeas {
    class LTRSyncMarksRecvConfig;
    class LTR11AdcConfig;

    class LTR11Config : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        const DevAdcConfig *adcConfig() const override;
        const DevSyncMarksRecvConfig *syncMarksRecvConfig() const override;

        LTR11AdcConfig &adc() {return  *m_adc;}
        const LTR11AdcConfig &adc() const {return  *m_adc;}

        LTRSyncMarksRecvConfig &marksRecv() {return  *m_syncRecv;}
        const LTRSyncMarksRecvConfig &marksRecv() const {return  *m_syncRecv;}

        DeviceConfig *clone() const override {
            return new LTR11Config{*this};
        }

        LTR11Config();
        LTR11Config(const LTR11Config &cfg);
        ~LTR11Config() override;
    protected:
        void protUpdate() override;
    private:
        const std::unique_ptr<LTR11AdcConfig> m_adc;
        const std::unique_ptr<LTRSyncMarksRecvConfig> m_syncRecv;
    };
}

#endif // LQMEAS_LTR11CONFIG_H
