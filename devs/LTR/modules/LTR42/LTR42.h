#ifndef LQMEAS_LTR42_H
#define LQMEAS_LTR42_H

#include "ltr/include/ltr42api.h"
#include "lqmeas/devs/LTR/modules/LTRModule.h"
#include "lqmeas/ifaces/out/DevOutAsyncDigSwMask.h"

namespace LQMeas {
    class LTR42Config;
    class LTR42Info;

    class LTR42 : public LTRModule, public DevOutAsyncDigSwMask {
        Q_OBJECT
    public:
        static const quint16 typeModuleID {LTR_MID_LTR42};

        ~LTR42() override;
        bool isOpened() const override;

        DevOutAsyncDig *devOutAsyncDig() override {
            return static_cast<DevOutAsyncDig*>(this);
        }

        QString errorString(int err) const override;

        const LTR42Config &devspecConfig() const;
        QSharedPointer<const LTR42Info> devspecInfo() const;
    protected:
        TLTR* channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;

        void protOutAsyncDigAll(unsigned val, LQError &err) override;
    private:
        explicit LTR42(LTRCrate *crate, int slot, QObject *parent = nullptr);
        mutable TLTR42 m_hnd;

        friend class LTRResolver;
    };
}

#endif // LQMEAS_LTR42_H
