#include "LTR42.h"
#include "LTR42Info.h"
#include "LTR42Config.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdErrors.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"


namespace LQMeas {
    LTR42::LTR42(LTRCrate *crate, int slot, QObject *parent) :
        LTRModule{crate, slot, typeModuleID, new LTR42Config{}, LTR42Info{}, parent} {

        LTR42_Init(&m_hnd);
    }

    LTR42::~LTR42() {
        LTR42_Close(&m_hnd);
    }

    bool LTR42::isOpened() const {
        return LTR42_IsOpened(&m_hnd) == LTR_OK;
    }

    QString LTR42::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTR42_GetErrorString(err));
    }

    const LTR42Config &LTR42::devspecConfig() const {
        return static_cast<const LTR42Config &>(config());
    }

    QSharedPointer<const LTR42Info> LTR42::devspecInfo() const {
        return devInfo().staticCast<const LTR42Info>();
    }

    TLTR *LTR42::channel() const {
        return &m_hnd.Channel;
    }

    int LTR42::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags)     {
        Q_UNUSED(flags)
        INT ltr_err = LTR42_Open(&m_hnd, static_cast<INT>(ltrd_ip), ltrd_port, crateSn, slot);

        if (ltr_err == LTR_OK) {
            setDeviceInfo(LTR42Info{LTR42TypeInfo::defaultTypeInfo(),
                                    QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                    QSTRING_FROM_CSTR(m_hnd.ModuleInfo.FirmwareVersion)});
        }
        return ltr_err;
    }

    void LTR42::protClose(LQError &err) {
        return getError(LTR42_Close(&m_hnd), err);
    }

    void LTR42::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTR42Config *setCfg {qobject_cast<const LTR42Config *>(&cfg)};
        if (setCfg) {
            getError(LTR42_Config(&m_hnd), err);
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }


    void LTR42::protOutAsyncDigAll(unsigned val, LQError &err) {
        getError(LTR42_WritePort(&m_hnd, val & 0xFFFF), err);
    }
}
