#ifndef LQMEAS_LTR42CONFIG_H
#define LQMEAS_LTR42CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"
#include "LTR42OutConfig.h"

namespace LQMeas {
    class LTR42Config : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        const DevOutConfig *outConfig() const override {
            return &m_out;
        }
        LTR42OutConfig &out() {return  m_out;}
        const LTR42OutConfig &out() const {return  m_out;}

        DeviceConfig *clone() const override {
            return new LTR42Config{*this};
        }

        LTR42Config();
        LTR42Config(const LTR42Config &cfg);
    private:
        LTR42OutConfig m_out;
    };
}

#endif // LQMEAS_LTR42CONFIG_H
