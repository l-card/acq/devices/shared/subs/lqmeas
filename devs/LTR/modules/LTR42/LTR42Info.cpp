#include "LTR42Info.h"
#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String &cfgkey_mcu_ver              {StdConfigKeys::mcuVer()};

    LTR42Info::LTR42Info(const LTR42TypeInfo &type, const QString &serial, const QString &mcu_ver_str) :
        DeviceInfo{type, serial}, m_mcu_ver_str{mcu_ver_str} {

    }

    LTR42Info::LTR42Info(const LTR42Info &info) : DeviceInfo{info}, m_mcu_ver_str{info.m_mcu_ver_str} {
    }

    DeviceInfo *LTR42Info::clone() const {
        return new LTR42Info{*this};
    }


    void LTR42Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_mcu_ver] = m_mcu_ver_str;
    }

    void LTR42Info::protLoad(const QJsonObject &infoObj) {
        m_mcu_ver_str = infoObj[cfgkey_mcu_ver].toString();
    }


}
