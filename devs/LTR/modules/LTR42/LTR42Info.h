#ifndef LQMEAS_LTR42INFO_H
#define LQMEAS_LTR42INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTR42TypeInfo.h"

namespace LQMeas {
    class LTR42Info : public DeviceInfo {
    public:
        LTR42Info(const LTR42TypeInfo &type = LTR42TypeInfo::defaultTypeInfo(),
                  const QString &serial = QString{},
                  const QString &mcu_ver_str = QString{});

        DeviceInfo *clone() const override;
        const QString &mcuVerStr() const {return m_mcu_ver_str;}


        const LTR42TypeInfo &devspecTypeInfo() const {
            return static_cast<const LTR42TypeInfo &>(type());
        }
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        LTR42Info(const LTR42Info &info);

        QString m_mcu_ver_str;
    };
}


#endif // LQMEAS_LTR42INFO_H
