#ifndef LQMEAS_LTR42OUTCONFIG_H
#define LQMEAS_LTR42OUTCONFIG_H

#include "lqmeas/ifaces/out/DevOutConfig.h"

namespace LQMeas {
    class LTR42OutConfig  : public DevOutConfig   {
        Q_OBJECT
    public:
        int outDacChRangeNum(int ch) const override { Q_UNUSED(ch) return 0;}
    private:
        LTR42OutConfig(const DevOutInfo &info);
        LTR42OutConfig(const LTR42OutConfig &outCfg);

        friend class LTR42Config;
    };
}

#endif // LQMEAS_LTR42OUTCONFIG_H
