#include "LTR42TypeInfo.h"

namespace LQMeas {
    const QString &LTR42TypeInfo::name() {
        static const QString str {QStringLiteral("LTR42")};
        return str;
    }
    
    const LTR42TypeInfo &LTR42TypeInfo::defaultTypeInfo() {
    static const LTR42TypeInfo info;
        return info;
    }

    QList<const DeviceTypeInfo *> LTR42TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list { &defaultTypeInfo() };
        return list;
    }
}
