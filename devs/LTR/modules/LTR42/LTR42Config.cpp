#include "LTR42Config.h"
#include "LTR42.h"
#include "LTR42TypeInfo.h"
#include <QJsonObject>


namespace LQMeas {
    const QString &LTR42Config::typeConfigName() {
        return LTR42TypeInfo::name();
    }
    
    LTR42Config::LTR42Config() : m_out{*LTR42TypeInfo::defaultTypeInfo().out()} {
        init();
    }

    LTR42Config::LTR42Config(const LTR42Config &cfg) : m_out{cfg.out()} {
        init();
    }




}
