#include "LTR42OutConfig.h"
#include "LTR42Config.h"

namespace LQMeas {
    LTR42OutConfig::LTR42OutConfig(const DevOutInfo &info) : DevOutConfig{info} {

    }

    LTR42OutConfig::LTR42OutConfig(const LTR42OutConfig &outCfg) : DevOutConfig{outCfg} {

    }
}
