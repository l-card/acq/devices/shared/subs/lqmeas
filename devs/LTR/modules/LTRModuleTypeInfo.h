#ifndef LQMEAS_LTRMODULETYPEINFO_H
#define LQMEAS_LTRMODULETYPEINFO_H

#include "lqmeas/devs/DeviceTypeInfo.h"

namespace LQMeas {
    class LTRModuleTypeInfo : public DeviceTypeInfo {
    public:
        const DeviceTypeSeries &deviceTypeSeries() const override;
        QList<const DeviceInterface *> supportedConInterfaces() const override;
    };
}

#endif // LTRMODULETYPEINFO_H
