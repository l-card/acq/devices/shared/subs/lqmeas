#ifndef LQMEAS_LTRMODULEFRAMERECEIVER_H
#define LQMEAS_LTRMODULEFRAMERECEIVER_H

#include "ltr/include/ltrapitypes.h"
#include "lqmeas/ifaces/sync/DevSyncMarksRecv.h"

#include <QVector>

namespace LQMeas {
    class LTRModule;

    class LTRModuleFrameReceiver : public DevSyncMarksRecv {
    public:
        explicit LTRModuleFrameReceiver(LTRModule *module);

        void getFrames(int size, unsigned tout, DWORD *&frame, int &recvd_size, LQError &err);

        /** @todo добавить параметр cnt_in_frame для отброса последний слов
         * (например для термометра в LTR114) */

        /* запуск с указанием:
         *   frame_size - размер кадра в словах (возврат идет кратным этому размеру)
         *   marks_div  - делитель меток - оставляется одна метка из marks_div.
         *                Служит если marks_div слов соответствуют одному времени
         *                (несколько слов на один отсчет и/или несколько параллельных отсчетов) */
        void start(int frame_size, int marks_div);
        void clear();

        bool syncMarkRecvWaitReady(unsigned tout, LQError &err) override;
        bool syncMarkRecvWaitStartMark(int rcv_size, unsigned tout, LQError &err) override;
        const QVector<DevSyncMarkWord> syncMarkRecvGet(const DevSyncMarkType &type) override;
        int syncMarkRecvFirstStartMarkCh() override {return m_first_mark_offset;}

        int frameSize() const {return m_frame_size;}
    private:
        void rawGetFrames(int size, unsigned tout, DWORD *&frame, DWORD **pmarks, int &recvd_size, LQError &err);

        QVector<DevSyncMarkWord> m_start_marks;
        QVector<DevSyncMarkWord> m_sec_marks;

        LTRModule *m_module;
        int m_frame_size {1};
        int m_marks_div {1};


        QVector<DWORD>  m_last_wrds;
        QVector<DWORD>  m_last_marks;

        int m_last_wrds_cnt {0};
        int m_last_wrds_offs {0};
        bool m_marks_valid {false};
        DWORD m_last_tmark;

        int m_first_mark_offset {-1};

        bool m_wait_mark_req {false};
    };
}

#endif // LTRMODULEFRAMERECEIVER_H
