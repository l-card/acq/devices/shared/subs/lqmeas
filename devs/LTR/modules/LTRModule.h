#ifndef LQMEAS_LTRMODULE_H
#define LQMEAS_LTRMODULE_H


#include <QPointer>

#include "lqmeas/devs/Device.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "ltr/include/ltrapi.h"

namespace LQMeas {
    class LTRResolver;
    class LTRModuleRawWordsTracker;

    class LTRModule : public Device {
        Q_OBJECT
    public:
        enum class MarkType {
            Start = 0,
            Second = 1
        };


        ~LTRModule() override;

        unsigned mid() const {return m_mid;}
        int slot() const {return m_slot; }


        QString location() const override {return tr("Slot %1").arg(QString::number(slot()));}

        DeviceRef *createRef() const override;

        const LTRServiceRef &serviceRef() const {return *m_svcRef;}
        const DeviceRef &ltrCrateRef() const {return *m_crateRef;}

        void getError(int errCode, LQError &err) const;
        LQError error(int errCode) const;

        void setRawWordsTracker(LTRModuleRawWordsTracker *tracker);
    protected:
        explicit LTRModule(LTRCrate *crate, int slot,
                           unsigned mid, DeviceConfig *defaultCfg,
                           const DeviceInfo &info, QObject *parent = nullptr);

        void protOpen(OpenFlags flags, LQError &err) override final;


        virtual int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn,  int slot, OpenFlags flags) = 0;

        virtual INT ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout);
        virtual INT ltrRawWordsSend(const DWORD *wrds, DWORD size, DWORD tout);

        virtual QString errorString(int err) const;
        virtual TLTR *channel() const = 0;

        void rawWordsWithMraksReceive(DWORD *wrds, DWORD *marks, int size,
                                      unsigned tout, int &recvd_size, LQError &err);
        void rawWordsSend(const DWORD *wrds, int size, unsigned tout,
                          int &sent_size, LQError &err);
    private:
        void updateCrateInfo(const DeviceInfo &crateInfo, const LTRServiceInfo &svcInfo);


        LTRServiceRef *m_svcRef;
        DeviceRef *m_crateRef;

        int m_slot;
        unsigned m_mid;

        LTRModuleRawWordsTracker *m_rawWordsTracker {nullptr};

        friend class LTRCrate;
        friend class LTRResolver;
        friend class LTRModuleFrameReceiver;
    };
}

#endif // LTRMODULE_H
