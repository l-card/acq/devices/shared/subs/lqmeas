#include "LTRModuleFrameReceiver.h"
#include "LTRModule.h"
#include "../marks/LTRSyncMarks.h"
#include "../marks/LTRSyncMarksRecvConfig.h"
#include "lqmeas/devs/DeviceConfig.h"
#include "lqmeas/ifaces/sync/DevSyncMarksRecvConfig.h"
#include "lqmeas/log/Log.h"

namespace LQMeas {
    static inline quint16 ltr_tmark_wrd_get_start(DWORD word) {
        return (word >> 16) & 0xFFFF;
    }
    static inline quint16 ltr_tmark_wrd_get_second(DWORD word) {
        return word & 0xFFFF;
    }

    LTRModuleFrameReceiver::LTRModuleFrameReceiver(LQMeas::LTRModule *module) :
        m_module{module} {

    }

    void LTRModuleFrameReceiver::getFrames(int size, unsigned tout, DWORD *&frame, int &recvd_size, LQError &err) {
        DWORD *pmarks = nullptr;

        if (m_wait_mark_req) {
            bool done = syncMarkRecvWaitStartMark(size, tout, err);
            if (err.isSuccess() && done)
                m_wait_mark_req = false;
        }

        if (err.isSuccess() && !m_wait_mark_req) {
            bool start_marks_en = false, sec_marks_en = false;
            const LTRSyncMarksRecvConfig *syncCfg = static_cast<const LTRSyncMarksRecvConfig *>(
                        m_module->config().syncMarksRecvConfig());
            if (syncCfg) {
                start_marks_en = syncCfg->startMarksRecvEnabled();
                sec_marks_en = syncCfg->secondMarksRecvEnabled();
            }


            rawGetFrames(size, tout, frame, (start_marks_en || sec_marks_en)  ? &pmarks : nullptr,
                         recvd_size, err);
            if (err.isSuccess() && pmarks) {
                int marks_count = recvd_size / m_marks_div;

                if (start_marks_en && (m_start_marks.size() < marks_count))
                    m_start_marks.resize(marks_count);

                if (sec_marks_en && (m_sec_marks.size() < marks_count))
                    m_sec_marks.resize(marks_count);

                for (int pos = 0; pos < marks_count; ++pos) {
                    DWORD tmark = pmarks[(pos + 1) * m_marks_div - 1];
                    if (start_marks_en)
                        m_start_marks[pos] = ltr_tmark_wrd_get_start(tmark);
                    if (sec_marks_en)
                        m_sec_marks[pos] = ltr_tmark_wrd_get_second(tmark);
                }
            }
        }
    }

    void LTRModuleFrameReceiver::rawGetFrames(int size, unsigned tout,
                                           DWORD *&frame, DWORD **pmarks,
                                           int &recvd_size, LQError &err)     {
        int recvd = 0;
        if (size > m_last_wrds.size()) {
            m_last_wrds.resize(size);
        }

        if (pmarks && (size > m_last_marks.size())) {
            m_last_marks.resize(size);
        }

        if ((m_last_wrds_cnt != 0) && (m_last_wrds_offs != 0))  {
            memmove(m_last_wrds.data(), &m_last_wrds.data()[m_last_wrds_offs],
                    static_cast<size_t>(m_last_wrds_cnt)*sizeof(m_last_wrds[0]));
            if (m_marks_valid && pmarks) {
                memmove(m_last_marks.data(), &m_last_marks.data()[m_last_wrds_offs],
                    static_cast<size_t>(m_last_wrds_cnt)*sizeof(m_last_marks[0]));
            }
        }

        if (m_last_wrds_cnt < size) {
            m_module->rawWordsWithMraksReceive(&m_last_wrds.data()[m_last_wrds_cnt],
                                        pmarks ? &m_last_marks.data()[m_last_wrds_cnt] : nullptr,
                                        size - m_last_wrds_cnt, tout,
                                        recvd, err);
        }
        if (err.isSuccess()) {
            if (recvd > 0) {
                /* если до этого был прием без меток и принят неполный кадр, то
                 * заполняем это пространство меткой первого нового слова */
                if (pmarks && (m_last_wrds_cnt > 0) && !m_marks_valid) {
                    for (int i = 0; i < m_last_wrds_cnt; ++i) {
                        m_last_marks[i] = m_last_marks[m_last_wrds_cnt];
                    }
                }
            }

            if ((recvd + m_last_wrds_cnt) > 0) {
                int tail_size;
                recvd += m_last_wrds_cnt;

                tail_size = recvd % m_frame_size;
                recvd -= tail_size;

                recvd_size = recvd;

                frame = m_last_wrds.data();
                if (pmarks) {
                    *pmarks = m_last_marks.data();
                    m_marks_valid = true;
                    m_last_tmark = m_last_marks[recvd-1];
                } else {
                    m_marks_valid = false;
                }

                m_last_wrds_cnt = tail_size;
                m_last_wrds_offs = recvd;
            }
        } else {
            LQMeasLog->error(Device::tr("Data receive error"), err, m_module);
        }
    }

    void LTRModuleFrameReceiver::start(int frame_size, int marks_div) {
        clear();
        m_frame_size = frame_size;
        m_marks_div = marks_div;

        m_wait_mark_req = m_module->config().syncMarksRecvConfig()
                         && m_module->config().syncMarksRecvConfig()->syncMarksRecvStartOnMark();
    }

    void LTRModuleFrameReceiver::clear() {
        m_start_marks.clear();
        m_sec_marks.clear();
        m_last_wrds.clear();
        m_last_marks.clear();

        m_last_wrds_cnt = 0;
        m_last_wrds_offs = 0;
        m_marks_valid = false;
        m_first_mark_offset = 0;


    }

    bool LTRModuleFrameReceiver::syncMarkRecvWaitReady(unsigned tout, LQError &err) {
        DWORD *frame, *marks;
        int recvd;
        bool rdy {false};

        rawGetFrames(frameSize(), tout, frame, &marks, recvd, err);
        if (err.isSuccess()) {
            rdy = recvd > 0;
        }
        return rdy;
    }

    bool LTRModuleFrameReceiver::syncMarkRecvWaitStartMark(int rcv_size, unsigned tout, LQError &err)  {
        DWORD *frame, *marks;
        int recvd;
        bool fnd {false};
        bool prev_mark_valid {m_marks_valid};
        DWORD prev_tmark {m_last_tmark};


        rawGetFrames(rcv_size, tout, frame, &marks, recvd, err);
        if (err.isSuccess() && (recvd > 0)) {
            if (!prev_mark_valid)
                prev_tmark = marks[0];

            quint16 first_start_mark = ltr_tmark_wrd_get_start(prev_tmark);
            if (first_start_mark != ltr_tmark_wrd_get_start(m_last_tmark)) {
                for (int pos = 0; (pos < recvd) && !fnd; ++pos) {
                    quint16 start_mark = ltr_tmark_wrd_get_start(marks[pos]);
                    if (start_mark != first_start_mark) {
                        fnd = true;
                        m_first_mark_offset = pos % m_frame_size / m_marks_div;

                        int frame_pos = pos - pos % m_frame_size;

                        /* помечаем данные от начала кадра, в котором обнаружена
                         * метка старт, как непринятые */
                        m_last_wrds_offs = frame_pos;
                        m_last_wrds_cnt  = recvd - frame_pos;
                    }
                }
            }
        }

        return fnd;
    }

    const QVector<DevSyncMarkWord> LTRModuleFrameReceiver::syncMarkRecvGet(const DevSyncMarkType &type) {
        return &type == &LTRSyncMarkTypes::start() ? m_start_marks :
               &type == &LTRSyncMarkTypes::second() ? m_sec_marks :
                                                      QVector<DevSyncMarkWord>();
    }
}
