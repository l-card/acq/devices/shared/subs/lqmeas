#ifndef LQMEAS_LTR41INCONFIG_H
#define LQMEAS_LTR41INCONFIG_H

#include "lqmeas/ifaces/in/dig/DevInDigSyncBaseConfig.h"

namespace LQMeas {
    class LTR41InConfig : public DevInDigSyncBaseConfig {
        Q_OBJECT
    public:
        int inDigSampleSize() const override {return 4;}
    protected:
        void dinAdjustFreq(double &dinFreq) override;
    private:
        explicit LTR41InConfig(const DevInDigInfo &info);
        explicit LTR41InConfig(const LTR41InConfig &inCfg);

        friend class LTR41Config;
    };
}

#endif // LQMEAS_LTR41INCONFIG_H
