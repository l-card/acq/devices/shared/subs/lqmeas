#ifndef LQMEAS_LTR41_H
#define LQMEAS_LTR41_H

#include "ltr/include/ltr41api.h"
#include "lqmeas/devs/LTR/modules/LTRModule.h"
#include "lqmeas/ifaces/in/dig/DevInAsyncDig.h"
#include "lqmeas/ifaces/in/dig/DevInSyncDig.h"
#include <memory>

namespace LQMeas {
    class LTR41Config;
    class LTR41Info;
    class LTRModuleFrameReceiver;

    class LTR41 : public LTRModule, public DevInAsyncDig, public DevInSyncDig {
        Q_OBJECT
    public:
        static const quint16 typeModuleID {LTR_MID_LTR41};

        ~LTR41() override;
        bool isOpened() const override;

        DevInAsyncDig *devInAsyncDig() override { return static_cast<DevInAsyncDig*>(this); }
        DevInSyncDig *devInSyncDig() override {return static_cast<DevInSyncDig*>(this);}
        DevSyncMarksRecv *devSyncMarkRecv() override;

        QString errorString(int err) const override;

        const LTR41Config &devspecConfig() const;
        QSharedPointer<const LTR41Info> devspecInfo() const;
    protected:
        TLTR* channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;

        void protInAsyncDig(unsigned &val, LQError &err) override;
        void protInDigStart(LQError &err) override;
        void protInDigStop(LQError &err) override;
        void protInDigGetData(quint32 *data, int size,  unsigned tout, int &recvd_size, LQError &err) override;

        INT ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) override;
    private:
        explicit LTR41(LTRCrate *crate, int slot, QObject *parent = nullptr);
        mutable TLTR41 m_hnd;
        const std::unique_ptr<LTRModuleFrameReceiver> m_receiver;

        friend class LTRResolver;
        bool m_stream_run {false};
        QVector<WORD> m_proc_buf;
    };
}

#endif // LQMEAS_LTR41_H
