#ifndef LQMEAS_LTR41TYPEINFO_H
#define LQMEAS_LTR41TYPEINFO_H


#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"
#include "lqmeas/ifaces/in/dig/DevInDigInfo.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvInfo.h"

namespace LQMeas {
    class LTR41TypeInfo : public LTRModuleTypeInfo, public DevInDigInfo, public LTRSyncMarksRecvInfo {
    public:
        static const QString &name();
        static const LTR41TypeInfo &defaultTypeInfo();

        const DevInDigInfo *digin() const override {return static_cast<const DevInDigInfo *>(this);}
        const DevSyncMarksRecvInfo *syncMarkRecv() const override {
            return static_cast<const DevSyncMarksRecvInfo *>(this);
        }

        QString deviceTypeName() const override {return name();}
        QList<const DeviceTypeInfo *> modificationList() const override;

        /* --------------- DevInDigInfo --------------------------------------*/
        int inDigChannelsCnt() const override {return 16;}
        bool inDigSyncSupport(void) const override {return true;}
        double inDigSyncFreqMax(void) const override {return 100 * 1000;}
    };
}

#endif // LQMEAS_LTR41TYPEINFO_H
