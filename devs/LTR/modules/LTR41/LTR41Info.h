#ifndef LQMEAS_LTR41INFO_H
#define LQMEAS_LTR41INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTR41TypeInfo.h"

namespace LQMeas {
    class LTR41Info : public DeviceInfo {
    public:
        LTR41Info(const LTR41TypeInfo &type = LTR41TypeInfo::defaultTypeInfo(),
                  const QString &serial = QString{},
                  const QString &mcu_ver_str = QString{});

        DeviceInfo *clone() const override;
        const QString &mcuVerStr() const {return m_mcu_ver_str;}


        const LTR41TypeInfo &devspecTypeInfo() const {
            return static_cast<const LTR41TypeInfo &>(type());
        }
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        LTR41Info(const LTR41Info &info);

        QString m_mcu_ver_str;
    };
}

#endif // LQMEAS_LTR41INFO_H
