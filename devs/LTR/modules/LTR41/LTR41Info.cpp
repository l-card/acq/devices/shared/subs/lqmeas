#include "LTR41Info.h"
#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String &cfgkey_mcu_ver              {StdConfigKeys::mcuVer()};

    LTR41Info::LTR41Info(const LTR41TypeInfo &type, const QString &serial, const QString &mcu_ver_str) :
        DeviceInfo{type, serial}, m_mcu_ver_str{mcu_ver_str} {

    }

    LTR41Info::LTR41Info(const LTR41Info &info) :
        DeviceInfo{info}, m_mcu_ver_str{info.m_mcu_ver_str} {
    }

    DeviceInfo *LTR41Info::clone() const {
        return new LTR41Info{*this};
    }


    void LTR41Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_mcu_ver] = m_mcu_ver_str;
    }

    void LTR41Info::protLoad(const QJsonObject &infoObj) {
        m_mcu_ver_str = infoObj[cfgkey_mcu_ver].toString();
    }


}
