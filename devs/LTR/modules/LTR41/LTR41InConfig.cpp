#include "LTR41InConfig.h"
#include "ltr/include/ltr41api.h"

namespace LQMeas {
    LTR41InConfig::LTR41InConfig(const DevInDigInfo &info) : DevInDigSyncBaseConfig{info} {

    }

    LTR41InConfig::LTR41InConfig(const LTR41InConfig &inCfg) : DevInDigSyncBaseConfig{inCfg} {

    }

    void LTR41InConfig::dinAdjustFreq(double &dinFreq) {
        LTR41_CalcStreamReadFreq(dinFreq, &dinFreq);
    }
}
