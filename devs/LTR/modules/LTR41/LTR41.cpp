#include "LTR41.h"
#include "LTR41Info.h"
#include "LTR41Config.h"
#include "LTR41InConfig.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdErrors.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/devs/LTR/modules/LTRModuleFrameReceiver.h"

namespace LQMeas {
    LTR41::LTR41(LTRCrate *crate, int slot, QObject *parent) :
        LTRModule{crate, slot, typeModuleID, new LTR41Config{}, LTR41Info{}, parent},
        DevInSyncDig{this},
        m_receiver{new LTRModuleFrameReceiver{this}}{

        LTR41_Init(&m_hnd);
    }

    LTR41::~LTR41() {
        LTR41_Close(&m_hnd);
    }

    bool LTR41::isOpened() const {
        return LTR41_IsOpened(&m_hnd) == LTR_OK;
    }

    DevSyncMarksRecv *LTR41::devSyncMarkRecv() {
        return m_receiver.get();
    }

    QString LTR41::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTR41_GetErrorString(err));
    }

    const LTR41Config &LTR41::devspecConfig() const {
        return static_cast<const LTR41Config &>(config());
    }

    QSharedPointer<const LTR41Info> LTR41::devspecInfo() const {
        return devInfo().staticCast<const LTR41Info>();
    }

    TLTR *LTR41::channel() const {
        return &m_hnd.Channel;
    }

    int LTR41::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        Q_UNUSED(flags)
        INT ltr_err = LTR41_Open(&m_hnd, static_cast<INT>(ltrd_ip), ltrd_port, crateSn, slot);

        if (ltr_err == LTR_OK) {
            m_stream_run = false;
            setDeviceInfo(LTR41Info{LTR41TypeInfo::defaultTypeInfo(),
                                    QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                    QSTRING_FROM_CSTR(m_hnd.ModuleInfo.FirmwareVersion)});
        }
        return ltr_err;
    }

    void LTR41::protClose(LQError &err) {
        getError(LTR41_Close(&m_hnd), err);
    }

    void LTR41::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTR41Config *setCfg {qobject_cast<const LTR41Config *>(&cfg)};
        if (setCfg) {
            m_hnd.StreamReadRate = setCfg->din().inDigFreq();
            getError(LTR41_Config(&m_hnd), err);
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }

    void LTR41::protInAsyncDig(unsigned &val, LQError &err) {
        WORD rd_data;
        INT err_code = LTR41_ReadPort(&m_hnd, &rd_data);
        if (err_code == LTR_OK) {
            val = rd_data;
        } else {
            getError(err_code, err);
        }
    }

    void LTR41::protInDigStart(LQError &err) {
        if (!m_stream_run) {
            getError(LTR41_StartStreamRead(&m_hnd), err);
            if (err.isSuccess()) {
                m_stream_run = true;
                m_receiver->start(1, 1);
            }
        }
    }

    void LTR41::protInDigStop(LQError &err) {
        if (m_stream_run) {
            getError(LTR41_StopStreamRead(&m_hnd), err);
            if (err.isSuccess()) {
                m_stream_run = false;
            }
        }
    }

    void LTR41::protInDigGetData(quint32 *data, int size, unsigned tout, int &recvd_size, LQError &err) {
        int wrds_cnt {size};
        int recvd_words {0};
        DWORD *wrds;
        DWORD proc_size {0};

        m_receiver->getFrames(wrds_cnt, tout, wrds, recvd_words, err);
        if (err.isSuccess() && (recvd_words > 0)) {
            proc_size = static_cast<DWORD>(recvd_words);
            if (m_proc_buf.size() < recvd_words)
                m_proc_buf.resize(recvd_words);

            getError(LTR41_ProcessData(&m_hnd, wrds, m_proc_buf.data(), &proc_size), err);
        }

        if (err.isSuccess()) {
            for (DWORD i {0}; i < proc_size; ++i) {
                data[i] = m_proc_buf.at(i);
            }
            recvd_size = static_cast<int>(proc_size);
        }
    }

    INT LTR41::ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) {
        INT ret {LTR_OK};
        if (m_stream_run) {
            ret = LTR41_Recv(&m_hnd, wrds, marks, size, tout);
        }
        return ret;
    }
}
