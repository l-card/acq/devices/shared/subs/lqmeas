#include "LTR41TypeInfo.h"

namespace LQMeas {
    const QString &LTR41TypeInfo::name() {
        static const QString str {QStringLiteral("LTR41")};
        return str;
    }
    
    const LTR41TypeInfo &LTR41TypeInfo::defaultTypeInfo() {
        static const LTR41TypeInfo info;
        return info;
    }

    QList<const DeviceTypeInfo *> LTR41TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list { &defaultTypeInfo() };
        return list;
    }
}
