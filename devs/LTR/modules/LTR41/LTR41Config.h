#ifndef LQMEAS_LTR41CONFIG_H
#define LQMEAS_LTR41CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"
#include <memory>

namespace LQMeas {
    class LTR41InConfig;
    class LTRSyncMarksRecvConfig;

    class LTR41Config : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        const DevInDigConfig *inDigConfig() const override;
        const DevSyncMarksRecvConfig *syncMarksRecvConfig() const override;

        LTR41InConfig &din() {return  *m_in;}
        const LTR41InConfig &din() const {return  *m_in;}
        LTRSyncMarksRecvConfig &marksRecv() {return  *m_syncRecv;}
        const LTRSyncMarksRecvConfig &marksRecv() const {return  *m_syncRecv;}

        DeviceConfig *clone() const override {
            return new LTR41Config{*this};
        }

        LTR41Config();
        LTR41Config(const LTR41Config &cfg);
    protected:
        void protUpdate() override;
    private:
        const std::unique_ptr<LTR41InConfig> m_in;
        const std::unique_ptr<LTRSyncMarksRecvConfig> m_syncRecv;
    };
}

#endif // LQMEAS_LTR41CONFIG_H
