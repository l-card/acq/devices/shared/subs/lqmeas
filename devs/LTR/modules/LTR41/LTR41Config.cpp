#include "LTR41Config.h"
#include "LTR41.h"
#include "LTR41TypeInfo.h"
#include "LTR41InConfig.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvConfig.h"
#include <QJsonObject>

namespace LQMeas {
    const QString &LTR41Config::typeConfigName() {
        return LTR41TypeInfo::name();
    }

    const DevInDigConfig *LTR41Config::inDigConfig() const {
        return m_in.get();
    }

    const DevSyncMarksRecvConfig *LTR41Config::syncMarksRecvConfig() const {
        return m_syncRecv.get();
    }

    LTR41Config::LTR41Config() :
        m_in{new LTR41InConfig{*LTR41TypeInfo::defaultTypeInfo().digin()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this}} {

        init();
    }


    LTR41Config::LTR41Config(const LTR41Config &cfg) :
        m_in{new LTR41InConfig{cfg.din()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this, cfg.marksRecv()}}{

        init();
    }

    void LTR41Config::protUpdate() {
        m_syncRecv->updateSyncMarksRecvFreq(din().inDigFreq());
    }
}
