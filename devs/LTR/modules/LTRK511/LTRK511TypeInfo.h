#ifndef LQMEAS_LTRK511TYPEINFO_H
#define LQMEAS_LTRK511TYPEINFO_H


#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"
#include "lqmeas/ifaces/out/DevOutInfo.h"

namespace LQMeas {
    class LTRK511TypeInfo : public LTRModuleTypeInfo, public DevOutInfo {
    public:
        static const QString &name();
        QString deviceTypeName() const override {return name();}

        virtual int modificationNum() const = 0;
        virtual QString outSwTypeName() const = 0;

        static const LTRK511TypeInfo &defaultTypeInfo();

        static const LTRK511TypeInfo &typeInfoBase();
        static const LTRK511TypeInfo &typeInfoMod1();
        static const LTRK511TypeInfo &typeInfoMod2();

        QList<const DeviceTypeInfo *> modificationList() const override;

        bool outDacSyncSupport() const override {return false;}
        bool outDigSyncSupport() const override {return false;}
        bool outDacAsyncSupport() const override {return true;}
        bool outDigAsyncSupport() const override {return true;}
        bool outDacSyncModeCfgPerCh() const override {return false;}
        bool outSyncRamModeIsConfigurable() const override {return false;}
        bool outSyncDacChGenModeIsConfigurable() const override {return false;}
        bool outSyncDigChGenModeIsConfigurable() const override {return false;}
        double outSyncGenFreqMax() const override {return 0;}
        bool outSyncPresetSupport() const override {return false;}
        int outDacChRangesCnt(int ch) const override {Q_UNUSED(ch) return 1;}
        double outDacChRangeMaxVal(int ch, int range) const override;
        double outDacChRangeMinVal(int ch, int range) const override;

    private:
        LTRK511TypeInfo();
    };
}
#endif // LQMEAS_LTRK511TYPEINFO_H
