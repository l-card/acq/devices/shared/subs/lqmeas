#include "LTRK511.h"
#include "LTRK511Config.h"
#include "LTRK511Info.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdErrors.h"

namespace LQMeas {
    LTRK511::~LTRK511() {
        LTRK511_Close(&m_hnd);
    }

    bool LTRK511::isOpened() const  {
        return LTRK511_IsOpened(&m_hnd) == LTR_OK;
    }

    QString LTRK511::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTRK511_GetErrorString(err));;
    }

    const LTRK511Config &LTRK511::devspecConfig() const {
        return static_cast<const LTRK511Config &>(config());
    }


    QSharedPointer<const LTRK511Info> LTRK511::devspecInfo() const {
        return devInfo().staticCast<const LTRK511Info>();
    }

    void LTRK511::outSwState(int sw_num, LTRK511::OutSwState sw, LQError &err) {
        return getError(LTRK511_SetOutSwState(&m_hnd, sw_num, static_cast<BYTE>(sw)), err);
    }

    TLTR *LTRK511::channel() const {
        return &m_hnd.Channel;
    }

    int LTRK511::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        INT ltr_err = LTRK511_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, static_cast<WORD>(slot));
        if (LTRK511_IsOpened(&m_hnd) == LTR_OK) {
            const LTRK511TypeInfo *fnd_type {&LTRK511TypeInfo::defaultTypeInfo()};
            const QList<const DeviceTypeInfo *> type_list {fnd_type->modificationList()};
            for (const DeviceTypeInfo *type : type_list) {
                const LTRK511TypeInfo *ltrk511_type {static_cast<const LTRK511TypeInfo *>(type)};
                if (ltrk511_type->modificationNum() == m_hnd.ModuleInfo.Modification) {
                    fnd_type = ltrk511_type;
                }
            }

            setDeviceInfo(LTRK511Info{*fnd_type, QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                      m_hnd.ModuleInfo.VerPLD});
        }
        return ltr_err;
    }

    void LTRK511::protClose(LQError &err) {
        getError(LTRK511_Close(&m_hnd), err);
    }

    void LTRK511::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTRK511Config *setCfg {qobject_cast<const LTRK511Config *>(&cfg)};
        if (!setCfg) {
            err += StdErrors::InvalidConfigType();
        }
    }

    void LTRK511::protOutAsyncDigAll(unsigned val, LQError &err) {
        getError(LTRK511_SetRelayStates(&m_hnd, val & 0xFFFF), err);
    }

    void LTRK511::protOutAsyncDac(int ch, double val, LQError &err) {
        Q_UNUSED(ch)
        getError(LTRK511_SetDAC(&m_hnd, val, LTRK511_DAC_FLAG_CALIBR | LTRK511_DAC_FLAG_VALUE), err);
    }

    LTRK511::LTRK511(LTRCrate *crate, int slot, QObject *parent) :
        LTR01{crate, slot, typeModuleSubID, new LTRK511Config{}, LTRK511Info{}, parent},
        DevOutAsyncDac{this} {

        LTRK511_Init(&m_hnd);
    }

}
