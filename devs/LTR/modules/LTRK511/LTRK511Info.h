#ifndef LQMEAS_LTRK511INFO_H
#define LQMEAS_LTRK511INFO_H


#include "lqmeas/devs/DeviceInfo.h"
#include "LTRK511TypeInfo.h"

namespace LQMeas {
    class LTRK511Info: public DeviceInfo {
    public:
        explicit LTRK511Info(const LTRK511TypeInfo &type = LTRK511TypeInfo::defaultTypeInfo(),
                            const QString &serial = QString{}, int pld_ver = 0);
        LTRK511Info(const LTRK511Info &info);

        int pldVer() const {return m_pld_ver;}
        QString pldVerStr() const;

        const LTRK511TypeInfo &devspecType() const {return static_cast<const LTRK511TypeInfo &>(type());}

        DeviceInfo *clone() const override {return new LTRK511Info{*this};}
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        int m_pld_ver;
    };
}

#endif // LQMEAS_LTRK511INFO_H
