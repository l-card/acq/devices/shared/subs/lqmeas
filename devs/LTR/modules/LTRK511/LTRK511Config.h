#ifndef LQMEAS_LTRK511CONFIG_H
#define LQMEAS_LTRK511CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"

namespace LQMeas {
    class LTRK511Config: public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        DeviceConfig *clone() const override {
            return new LTRK511Config{*this};
        }


        explicit LTRK511Config();
        LTRK511Config(const LTRK511Config &cfg);
    private:
    };
}

#endif // LQMEAS_LTRK511CONFIG_H
