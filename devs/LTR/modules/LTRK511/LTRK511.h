#ifndef LQMEAS_LTRK511_H
#define LQMEAS_LTRK511_H


#include "ltr/include/ltrk511api.h"
#include "lqmeas/devs/LTR/modules/LTR01.h"
#include "lqmeas/ifaces/out/DevOutAsyncDigSwMask.h"
#include "lqmeas/ifaces/out/DevOutAsyncDac.h"

namespace LQMeas {
    class LTRK511Config;
    class LTRK511Info;

    class LTRK511  : public LTR01, public DevOutAsyncDigSwMask, public DevOutAsyncDac {
        Q_OBJECT
    public:
        static const e_LTR01_SUBID typeModuleSubID {LTR01_SUBID_LTRK511};

        enum class OutSwState {
            Open   = LTRK511_OUT_SW_STATE_OPEN,
            ExtSig = LTRK511_OUT_SW_STATE_EXT_SIG,
            IntDiv = LTRK511_OUT_SW_STATE_INT_DIV,
        };


        ~LTRK511() override;
        bool isOpened() const override;

        DevOutAsyncDig *devOutAsyncDig() override {
            return static_cast<DevOutAsyncDig*>(this);
        }
        DevOutAsyncDac *devOutAsyncDac() override {
            return static_cast<DevOutAsyncDac*>(this);
        }


        QString errorString(int err) const override;

        const LTRK511Config &devspecConfig() const;
        QSharedPointer<const LTRK511Info> devspecInfo() const;

        TLTRK511 *rawHandle() {return &m_hnd;}
        const TLTRK511 *rawHandle() const {return &m_hnd;}

        void outSwState(int sw_num, OutSwState sw, LQError &err);
    protected:
        TLTR *channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;

        void protOutAsyncDigAll(unsigned val, LQError &err) override;
        void protOutAsyncDac(int ch, double val, LQError &err) override;
    private:
        explicit LTRK511(LTRCrate *crate, int slot, QObject *parent = nullptr);

        mutable TLTRK511 m_hnd;

        friend class LTRResolver;
    };
}

Q_DECLARE_METATYPE(LQMeas::LTRK511::OutSwState);

#endif // LQMEAS_LTRK511_H
