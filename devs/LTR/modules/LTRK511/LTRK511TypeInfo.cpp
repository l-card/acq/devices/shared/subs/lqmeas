#include "LTRK511TypeInfo.h"
#include "ltr/include/ltrk511api.h"
#include <QStringBuilder>

namespace LQMeas {
    static const QString outsw_type_name_adg453 {QStringLiteral("ADG453")};
    static const QString outsw_type_name_adg444 {QStringLiteral("ADG444")};

    const QString &LTRK511TypeInfo::name() {
        static const QString str {QStringLiteral("LTRK511") };
        return str;
    }
    
    const LTRK511TypeInfo &LTRK511TypeInfo::defaultTypeInfo() {
        return typeInfoBase();
    }

    const LTRK511TypeInfo &LTRK511TypeInfo::typeInfoBase() {
        static const class LTRK511TypeInfoBase : public LTRK511TypeInfo {
        public:
            QString outSwTypeName() const override { return outsw_type_name_adg444; }
            int modificationNum() const override   { return 0; }
            int outDigChannelsCnt() const override { return 2; }
            int outDacChannelsCnt() const override { return 1; }
        } type;
        return type;
    }

    const LTRK511TypeInfo &LTRK511TypeInfo::typeInfoMod1() {
        static const class LTRK511TypeInfoM1 : public LTRK511TypeInfo {
        public:
            QString deviceModificationName() const override { return name() % QStringLiteral("-01"); }
            QString outSwTypeName() const override { return outsw_type_name_adg444; }
            int modificationNum() const override   { return 1; }
            int outDigChannelsCnt() const override { return 1; }
            int outDacChannelsCnt() const override { return 0; }
        } type;
        return type;
    }

    const LTRK511TypeInfo &LTRK511TypeInfo::typeInfoMod2() {
        static const class LTRK511TypeInfoM2 : public LTRK511TypeInfo {
        public:
            QString deviceModificationName() const override { return name() % QStringLiteral("-02"); }
            QString outSwTypeName() const override { return outsw_type_name_adg453; }
            int modificationNum() const override   { return 2; }
            int outDigChannelsCnt() const override { return 1; }
            int outDacChannelsCnt() const override { return 0; }
        } type;
        return type;
    }

    QList<const DeviceTypeInfo *> LTRK511TypeInfo::modificationList() const  {
        static const QList<const DeviceTypeInfo *> list {
            &typeInfoBase(),
            &typeInfoMod1(),
            &typeInfoMod2()
        };
        return list;
    }


    double LTRK511TypeInfo::outDacChRangeMaxVal(int ch, int range) const {
        Q_UNUSED(ch) Q_UNUSED(range)
        return LTRK511_DAC_VALUE_MAX;
    }

    double LTRK511TypeInfo::outDacChRangeMinVal(int ch, int range) const {
        Q_UNUSED(ch) Q_UNUSED(range)
        return 0;
    }

    LTRK511TypeInfo::LTRK511TypeInfo() {

    }
}
