#include "LTRK511Info.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_pld_ver           {StdConfigKeys::pldVer()};

    LTRK511Info::LTRK511Info(const LTRK511TypeInfo &type, const QString &serial, int pld_ver) :
        DeviceInfo{type, serial}, m_pld_ver{pld_ver} {

    }

    LTRK511Info::LTRK511Info(const LTRK511Info &info) :
        DeviceInfo{info},
        m_pld_ver{info.m_pld_ver} {

    }

    QString LTRK511Info::pldVerStr() const {
        return QString::number(m_pld_ver);
    }

    void LTRK511Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_pld_ver] = m_pld_ver;
    }

    void LTRK511Info::protLoad(const QJsonObject &infoObj) {
        m_pld_ver = infoObj[cfgkey_pld_ver].toInt();
    }

}
