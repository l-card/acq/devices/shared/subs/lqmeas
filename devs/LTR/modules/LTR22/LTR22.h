#ifndef LQMEAS_LTR22_H
#define LQMEAS_LTR22_H


#include "ltr/include/ltr22api.h"
#include "lqmeas/devs/LTR/modules/LTRModule.h"
#include "lqmeas/ifaces/in/adc/DevAdc.h"
#include <memory>

namespace LQMeas {
    class LTR22Config;
    class LTR22Info;
    class LTRModuleFrameReceiver;

    class LTR22 : public LTRModule, public DevAdc {
        Q_OBJECT
    public:
        static const quint16 typeModuleID {LTR_MID_LTR22};

        ~LTR22() override;
        bool isOpened() const override;

        /* ----------------------- интерфейс ввода  --------------------------*/
        DevAdc *devAdc() override {return static_cast<DevAdc*>(this);}
        DevSyncMarksRecv *devSyncMarkRecv() override;

        QString errorString(int err) const override;

        bool adcIsRunning() const override;

        const LTR22Config &devspecConfig() const;
        QSharedPointer<const LTR22Info> devspecInfo() const;
    protected:
        TLTR* channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn,  int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;

        INT ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) override;

        void protAdcStart(LQError &err) override;
        void protAdcStop(LQError &err) override;
        void protAdcGetData(double *data, int size, unsigned flags,
                            unsigned tout, int &recvd_size, LQError &err) override;
    private:
        explicit LTR22(LTRCrate *crate, int slot, QObject *parent = nullptr);
        mutable TLTR22 m_hnd;
        const std::unique_ptr<LTRModuleFrameReceiver> m_receiver;

        friend class LTRResolver;
    };
}
#endif // LQMEAS_LTR22_H
