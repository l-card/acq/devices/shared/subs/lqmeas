#include "LTR22TypeInfo.h"
#include "LTR22AdcConfig.h"

static const double f_ranges[] = {10., 3., 1, 0.3, 0.1, 0.03};

static const double f_adc_freqs[] = {
    78125.,         52083.3333333, 39062.5,       26041.6666667,  19531.25,
    17361.1111111, 15625.,        13020.8333333, 11160.7142857,  10416.6666667,
     9765.625,      8680.5555556,  7812.5,        7440.47619048,  7102.27272727,
     6510.41666667, 6009.61538462, 5787.03703704, 5580.35714286,  5208.33333333,
     4734.84848485, 4340.27777778, 4006.41025641, 3720.23809524,  3472.22222222
};

namespace LQMeas {
    const QString &LTR22TypeInfo::name() {
        static const QString str {QStringLiteral("LTR22")};
        return str;
    }

    double LTR22TypeInfo::adcFreqMax() const {
        return f_adc_freqs[0];
    }

    int LTR22TypeInfo::adcChRangesCnt(int ch_num, int unit_mode_num) const {
        Q_UNUSED(ch_num)
        Q_UNUSED(unit_mode_num)
        return sizeof(f_ranges)/sizeof(f_ranges[0]);
    }

    double LTR22TypeInfo::adcChRangeMaxVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num)
        Q_UNUSED(unit_mode_num)
         return f_ranges[range];
    }

    double LTR22TypeInfo::adcChRangeMinVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num)
        Q_UNUSED(unit_mode_num)
        return -f_ranges[range];
    }
    
    
    const LTR22TypeInfo &LTR22TypeInfo::defaultTypeInfo() {
        static const LTR22TypeInfo info;
        return info;
    }

    int LTR22TypeInfo::adcChannelsCnt() const {
         return LTR22AdcConfig::adc_channels_cnt;
    }

    double LTR22TypeInfo::typeAdcFreqVal(int freq_idx) {
        return f_adc_freqs[freq_idx];
    }

    int LTR22TypeInfo::typeAdcFreqsCnt() {
        return sizeof(f_adc_freqs)/sizeof(f_adc_freqs[0]);
    }

    QList<const DeviceTypeInfo *> LTR22TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list {&defaultTypeInfo()};
        return list;
    }
}
