#include "LTR22Config.h"
#include "LTR22TypeInfo.h"
#include "LTR22AdcConfig.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvConfig.h"

namespace LQMeas {
    const QString &LTR22Config::typeConfigName() {
        return LTR22TypeInfo::name();
    }

    const DevAdcConfig *LTR22Config::adcConfig() const  {
        return m_adc.get();
    }

    const DevSyncMarksRecvConfig *LTR22Config::syncMarksRecvConfig() const {
        return m_syncRecv.get();
    }

    LTR22Config::LTR22Config() :
        m_adc{new LTR22AdcConfig{LTR22TypeInfo::defaultTypeInfo()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this}} {

        init();
    }

    LTR22Config::LTR22Config(const LTR22Config &cfg) :
        m_adc{new LTR22AdcConfig{cfg.adc()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this, cfg.marksRecv()}} {

       init();
    }

    void LTR22Config::protUpdate() {
        m_syncRecv->updateSyncMarksRecvFreq(m_adc->adcChFreq());
    }

    LQMeas::LTR22Config::~LTR22Config() {

    }

}
