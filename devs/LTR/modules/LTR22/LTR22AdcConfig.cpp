#include "LTR22AdcConfig.h"
#include "LTR22.h"
#include "LTR22TypeInfo.h"
#include "ltr/include/ltr22api.h"
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>
#include <QJsonArray>

namespace LQMeas {
    static const QLatin1String cfgkey_ch_enabled         {StdConfigKeys::enabled()};
    static const QLatin1String cfgkey_ch_range_num       {StdConfigKeys::rangeNum()};
    static const QLatin1String cfgkey_channels_arr       {StdConfigKeys::channels()};
    static const QLatin1String cfgkey_ac_mode            {StdConfigKeys::acMode()};
    static const QLatin1String cfgkey_adc_freq_code      {StdConfigKeys::adcFreqCode()};
    static const QLatin1String cfgkey_adc_zero_mode      {"ZeroMode"};

    typedef EnumConfigKey<LTR22AdcConfig::AcMode> LTR22AcModeKey;
    static const LTR22AcModeKey adc_acmodes[] = {
        {QStringLiteral("acdc"),        LTR22AdcConfig::AcMode::AcDc},
        {QStringLiteral("ac"),          LTR22AdcConfig::AcMode::AcOnly},
        {LTR22AcModeKey::defaultKey(),  LTR22AdcConfig::AcMode::AcDc},
    };


    LTR22AdcConfig::LTR22AdcConfig(const DevAdcInfo &info) : DevAdcConfig{info} {
        memset(&m_params, 0, sizeof(m_params));
        for (int ch {0}; ch < adc_channels_cnt; ++ch) {
            m_params.ch[ch].enabled = false;
            m_params.ch[ch].range_num = defaultRangeNum();
        }
    }

    LTR22AdcConfig::LTR22AdcConfig(const LTR22AdcConfig &cfg) : DevAdcConfig{cfg} {
         memcpy(&m_params, &cfg.m_params, sizeof (m_params));
    }

    double LTR22AdcConfig::adcFreq() const {
        return LTR22TypeInfo::typeAdcFreqVal(m_params.freq_num);
    }

    double LTR22AdcConfig::typeAdcFreqVal(LTR22AdcConfig::AdcFreq freq) {
        return LTR22TypeInfo::typeAdcFreqVal(typeAdcFreqNum(freq));
    }

    void LTR22AdcConfig::adcSetChEnabled(int ch, bool en) {
        if (m_params.ch[ch].enabled != en) {
            m_params.ch[ch].enabled = en;
            notifyChEnableStateChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR22AdcConfig::adcSetChRangeNum(int ch, int range_num) {
        if (m_params.ch[ch].range_num != range_num) {
            m_params.ch[ch].range_num = range_num;
            notifyChRangeChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR22AdcConfig::adcSetAcMode(LTR22AdcConfig::AcMode mode) {
        if (m_params.ac_mode != mode) {
            m_params.ac_mode = mode;
            notifyChModeChanged(-1);
            notifyConfigChanged();
        }
    }

    void LTR22AdcConfig::adcSetFreqNum(int freq_num) {
        if (m_params.freq_num != freq_num) {
            m_params.freq_num = freq_num;
            notifyChFreqChanged();
            notifyAdcFreqChanged();
            notifyConfigChanged();
        }
    }

    void LTR22AdcConfig::adcSetZeroModeEnabled(bool en) {
        if (m_params.zero_mode_en != en) {
            m_params.zero_mode_en = en;
            notifyConfigChanged();
        }
    }


    void LTR22AdcConfig::protSave(QJsonObject &cfgObj) const {
        QJsonArray chArray;
        for (int ch {0}; ch < adc_channels_cnt; ++ch) {
            QJsonObject chObj;
            chObj[cfgkey_ch_enabled] = m_params.ch[ch].enabled;
            chObj[cfgkey_ch_range_num] = m_params.ch[ch].range_num;
            chArray.append(chObj);
        }
        cfgObj[cfgkey_channels_arr] = chArray;
        cfgObj[cfgkey_ac_mode] = LTR22AcModeKey::getKey(adc_acmodes, m_params.ac_mode);
        cfgObj[cfgkey_adc_freq_code] = m_params.freq_num;
        cfgObj[cfgkey_adc_zero_mode] = m_params.zero_mode_en;
    }

    void LTR22AdcConfig::protLoad(const QJsonObject &cfgObj) {
        const QJsonArray &chArray {cfgObj[cfgkey_channels_arr].toArray()};
        const int ch_cnt {chArray.size()};

        for (int ch {0}; (ch < ch_cnt) && (ch < adc_channels_cnt); ++ch) {
            const QJsonObject &chObj {chArray.at(ch).toObject()};

            m_params.ch[ch].enabled = chObj[cfgkey_ch_enabled].toBool();

            const int range {chObj[cfgkey_ch_range_num].toInt(defaultRangeNum())};
            if ((range >= 0) || (range < adcInfo().adcChRangesCnt(ch, adcUnitMode(ch)))) {
                m_params.ch[ch].range_num = range;
            } else {
                m_params.ch[ch].range_num = defaultRangeNum();
            }

        }

        for (int ch {ch_cnt}; ch < adc_channels_cnt; ++ch) {
            m_params.ch[ch].enabled = false;
            m_params.ch[ch].range_num = defaultRangeNum();
        }

        m_params.ac_mode = LTR22AcModeKey::getValue(adc_acmodes, cfgObj[cfgkey_ac_mode].toString());
        m_params.freq_num = cfgObj[cfgkey_adc_freq_code].toInt(defaultFreqNum());
        m_params.zero_mode_en = cfgObj[cfgkey_adc_zero_mode].toBool(false);
    }
}
