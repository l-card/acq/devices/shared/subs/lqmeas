#ifndef LQMEAS_LTR22INFO_H
#define LQMEAS_LTR22INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTR22TypeInfo.h"

namespace LQMeas {
    class LTR22Info : public DeviceInfo {
    public:
        LTR22Info(const LTR22TypeInfo &type = LTR22TypeInfo::defaultTypeInfo(),
                  const QString &serial = QString{}, quint32 mcu_ver = 0);


        DeviceInfo *clone() const override {return new LTR22Info{*this};}
        quint32 mcuVer() const {return m_mcu_ver;}
        QString mcuVerStr() const;

        const LTR22TypeInfo &devspecTypeInfo() const {
            return static_cast<const LTR22TypeInfo &>(type());
        }
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        LTR22Info(const LTR22Info &info);

        quint32 m_mcu_ver;
    };
}

#endif // LQMEAS_LTR22INFO_H
