#include "LTR22.h"
#include "LTR22Config.h"
#include "LTR22AdcConfig.h"
#include "LTR22Info.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "lqmeas/devs/LTR/modules/LTRModuleFrameReceiver.h"
#include "lqmeas/log/Log.h"
#include "lqmeas/StdErrors.h"

namespace LQMeas {
    LTR22::LTR22(LTRCrate *crate, int slot, QObject *parent) :
        LTRModule{crate, slot, typeModuleID, new LTR22Config{}, LTR22Info{}, parent},
        DevAdc{this, LTR22AdcConfig::adc_channels_cnt},
        m_receiver{new LTRModuleFrameReceiver{this}} {

        LTR22_Init(&m_hnd);
    }


    LTR22::~LTR22() {
        LTR22_Close(&m_hnd);
    }

    bool LTR22:: isOpened() const {
        return LTR22_IsOpened(&m_hnd) == LTR_OK;
    }

    DevSyncMarksRecv *LTR22::devSyncMarkRecv() {
        return m_receiver.get();
    }

    bool LTR22::adcIsRunning() const {
        return m_hnd.DataReadingProcessed ? true : false;
    }

    const LTR22Config &LTR22::devspecConfig() const {
        return static_cast<const LTR22Config &>(config());
    }

    QSharedPointer<const LTR22Info> LTR22::devspecInfo() const {
        return devInfo().staticCast<const LTR22Info>();
    }

    TLTR *LTR22::channel() const {
        return &m_hnd.Channel;
    }

    int LTR22::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        Q_UNUSED(flags)
        INT ltr_err = LTR22_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, static_cast<WORD>(slot));
        if (ltr_err == LTR_OK) {
            ltr_err = LTR22_GetConfig(&m_hnd);
        }

        if (ltr_err == LTR_OK) {
            setDeviceInfo(LTR22Info{LTR22TypeInfo::defaultTypeInfo(),
                                    QSTRING_FROM_CSTR(reinterpret_cast<char*>(
                                        m_hnd.ModuleInfo.Description.SerialNumber)),
                                    m_hnd.ModuleInfo.CPU.FirmwareVersion});
        }
        return ltr_err;
    }

    void LTR22::protClose(LQError &err) {
        getError(LTR22_Close(&m_hnd), err);
    }


    void LTR22::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTR22Config *setCfg = qobject_cast<const LTR22Config *>(&cfg);
        if (setCfg) {
            LTR22_FindAdcFreqParams(LTR22TypeInfo::typeAdcFreqVal(setCfg->adc().adcFreqNum()),
                                    &m_hnd.Fdiv_rg, &m_hnd.Adc384, nullptr, nullptr);
            m_hnd.AC_DC_State = setCfg->adc().adcAcMode() == LTR22AdcConfig::AcMode::AcDc;
            m_hnd.MeasureADCZero = setCfg->adc().adcZeroModeEnabled();
            m_hnd.SyncMaster = false;

            for (int ch=0; ch < LTR22AdcConfig::adc_channels_cnt; ++ch) {
                LTR22AdcConfig::AdcChRange range  =  setCfg->adc().adcChRange(ch);
                BYTE range_code = range == LTR22AdcConfig::AdcChRange::U_10 ? LTR22_ADC_RANGE_10 :
                                 range == LTR22AdcConfig::AdcChRange::U_3  ? LTR22_ADC_RANGE_3 :
                                 range == LTR22AdcConfig::AdcChRange::U_1  ? LTR22_ADC_RANGE_1 :
                                 range == LTR22AdcConfig::AdcChRange::U_0_3 ? LTR22_ADC_RANGE_0_3 :
                                 range == LTR22AdcConfig::AdcChRange::U_0_1 ? LTR22_ADC_RANGE_0_1 :
                                 range == LTR22AdcConfig::AdcChRange::U_0_03 ? LTR22_ADC_RANGE_0_03 :
                                                                               LTR22_ADC_RANGE_10;


                m_hnd.ChannelEnabled[ch] = setCfg->adc().adcChEnabled(ch);
                m_hnd.ADCChannelRange[ch] = range_code;
            }

            getError(LTR22_SetConfig(&m_hnd), err);
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }

    INT LTR22::ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) {
        return LTR22_Recv(&m_hnd, wrds, marks, size, tout);
    }

    void LTR22::protAdcStart(LQError &err) {
        getError(LTR22_StartADC(&m_hnd, false), err);
        if (err.isSuccess()) {
            int frame_size = adcConfig().adcEnabledChCnt();
            m_receiver->start(frame_size, frame_size);
        }
    }

    void LTR22::protAdcStop(LQError &err) {
        INT err_code = LTR22_StopADC(&m_hnd);
        if (err_code == LTR_OK)
            err_code = LTR22_ClearBuffer(&m_hnd, TRUE);        
        m_receiver->clear();
        getError(err_code, err);

    }

    void LTR22::protAdcGetData(double *data, int size, unsigned flags,
                               unsigned tout, int &recvd_size, LQError &err) {
        int wrds_cnt {size};
        int recvd_words {0};
        BYTE OvFlags[LTR22AdcConfig::adc_channels_cnt];
        DWORD *wrds;

        INT proc_size {0};

        m_receiver->getFrames(wrds_cnt, tout, wrds, recvd_words, err);

        if (err.isSuccess() && (recvd_words != 0)) {
            DWORD proc_flags {LTR22_PROC_FLAG_VOLT | LTR22_PROC_FLAG_CALIBR};
            proc_size = static_cast<INT>(recvd_words);

            getError(LTR22_ProcessDataEx(&m_hnd, wrds, data, &proc_size,
                                         proc_flags, OvFlags, nullptr), err);
            if (!err.isSuccess()) {
                LQMeasLog->error(tr("Process data error"), err, this);
            } else {
                int en_ch_cnt = adcConfig().adcEnabledChCnt();
                for (int ch_idx = 0; ch_idx < en_ch_cnt; ++ch_idx) {
                    int ch_num = adcChNum(ch_idx);
                    ChannelStatus status = ChannelStatus::Ok;
                    if (OvFlags[ch_num])
                        status = ChannelStatus::Overload;
                    adcSetChStatus(ch_num, status);
                }
            }
        }

        if (err.isSuccess())
            recvd_size = proc_size;
    }

    QString LTR22::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTR22_GetErrorString(err));
    }
}
