#include "LTR22Info.h"

#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String &cfgkey_mcu_ver              {StdConfigKeys::mcuVer()};

    LTR22Info::LTR22Info(const LTR22TypeInfo &type, const QString &serial, quint32 mcu_ver) :
        DeviceInfo{type, serial}, m_mcu_ver{mcu_ver} {

    }

    LTR22Info::LTR22Info(const LTR22Info &info) : DeviceInfo{info}, m_mcu_ver{info.m_mcu_ver} {

    }

    QString LQMeas::LTR22Info::mcuVerStr() const {
        return QString{"%1.%2.%3.%4"}
                .arg((m_mcu_ver >> 24) & 0xFF)
                .arg((m_mcu_ver >> 16) & 0xFF)
                .arg((m_mcu_ver >>  8) & 0xFF)
                .arg((m_mcu_ver >>  0) & 0xFF);
    }

    void LTR22Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_mcu_ver] = static_cast<int>(m_mcu_ver);
    }

    void LTR22Info::protLoad(const QJsonObject &infoObj) {
        m_mcu_ver = static_cast<quint32>(infoObj[cfgkey_mcu_ver].toInt());
    }


}
