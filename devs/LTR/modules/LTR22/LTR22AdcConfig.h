#ifndef LQMEAS_LTR22ADCCONFIG_H
#define LQMEAS_LTR22ADCCONFIG_H

#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"


namespace LQMeas {
    class LTR22AdcConfig : public  DevAdcConfig {
        Q_OBJECT
    public:
        static const int adc_channels_cnt {4};

        enum class AcMode {
            AcDc,
            AcOnly
        };

        enum class AdcChRange {
            U_10,
            U_3,
            U_1,
            U_0_3,
            U_0_1,
            U_0_03
        };


        enum class AdcFreq {
            Freq_78K  = 0,
            Freq_52K  = 1,
            Freq_39K  = 2,
            Freq_26K  = 3,
            Freq_19K  = 4,
            Freq_17K  = 5,
            Freq_15K  = 6,
            Freq_13K  = 7,
            Freq_11K  = 8,
            Freq_10K  = 9,
            Freq_9K7  = 10,
            Freq_8K6  = 11,
            Freq_7K8  = 12,
            Freq_7K4  = 13,
            Freq_7K1  = 14,
            Freq_6K5  = 15,
            Freq_6K0  = 16,
            Freq_5K7  = 17,
            Freq_5K5  = 18,
            Freq_5K2  = 19,
            Freq_4K7  = 20,
            Freq_4K3  = 21,
            Freq_4K0  = 22,
            Freq_3K7  = 23,
            Freq_3K4  = 24
        };

        double adcFreq() const override;
        bool adcChEnabled(int ch) const override {return m_params.ch[ch].enabled;}
        int adcChRangeNum(int ch) const override { return m_params.ch[ch].range_num; }
        AdcChRange adcChRange(int ch) const {return static_cast<AdcChRange>(adcChRangeNum(ch));}
        AcMode adcAcMode() const {return m_params.ac_mode;}
        int adcFreqNum() const {return m_params.freq_num;}
        AdcFreq adcFreqCode() const {return static_cast<AdcFreq>(m_params.freq_num);}
        bool adcZeroModeEnabled() const {return m_params.zero_mode_en;}
        bool adcChSupportMeasDC(int ch) const override {
            Q_UNUSED(ch)
            return  adcAcMode() == AcMode::AcDc;
        }

        int adcSampleSize() const override {return 4;}



        static int typeAdcFreqNum(LTR22AdcConfig::AdcFreq freq) { return static_cast<int>(freq);}
        static int typeAdcRangeNum(LTR22AdcConfig::AdcChRange range) { return static_cast<int>(range);}
        static double typeAdcFreqVal(LTR22AdcConfig::AdcFreq freq);
    public Q_SLOTS:
        void adcSetChEnabled(int ch, bool en);
        void adcSetChRangeNum(int ch, int range_num);
        void adcSetChRange(int ch, LTR22AdcConfig::AdcChRange range) {
            adcSetChRangeNum(ch, typeAdcRangeNum(range));
        }
        void adcSetAcMode(LTR22AdcConfig::AcMode mode);
        void adcSetFreqNum(int freq_num);
        void adcSetFreqCode(LTR22AdcConfig::AdcFreq freq_code) {
            adcSetFreqNum(typeAdcFreqNum(freq_code));
        }
        void adcSetZeroModeEnabled(bool en);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        LTR22AdcConfig(const DevAdcInfo &info);
        LTR22AdcConfig(const LTR22AdcConfig &cfg);

        static int defaultRangeNum() {return typeAdcRangeNum(AdcChRange::U_10);}
        static int defaultFreqNum()  {return typeAdcFreqNum(AdcFreq::Freq_10K);}

        struct {
            int           freq_num;
            AcMode        ac_mode;
            bool          zero_mode_en;

            struct {
                bool      enabled;
                int       range_num;
            } ch[adc_channels_cnt];
        } m_params;

        friend class LTR22Config;
    };
}

Q_DECLARE_METATYPE(LQMeas::LTR22AdcConfig::AcMode);
Q_DECLARE_METATYPE(LQMeas::LTR22AdcConfig::AdcChRange);
Q_DECLARE_METATYPE(LQMeas::LTR22AdcConfig::AdcFreq);

#endif // LQMEAS_LTR22ADCCONFIG_H
