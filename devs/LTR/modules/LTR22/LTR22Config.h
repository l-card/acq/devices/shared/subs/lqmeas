#ifndef LQMEAS_LTR22CONFIG_H
#define LQMEAS_LTR22CONFIG_H


#include "lqmeas/devs/DeviceConfig.h"
#include <memory>

namespace LQMeas {
    class LTRSyncMarksRecvConfig;
    class LTR22AdcConfig;

    class LTR22Config : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        const DevAdcConfig *adcConfig() const  override;
        const DevSyncMarksRecvConfig *syncMarksRecvConfig() const override;

        LTR22AdcConfig &adc() {return  *m_adc;}
        const LTR22AdcConfig &adc() const {return  *m_adc;}

        LTRSyncMarksRecvConfig &marksRecv() {return  *m_syncRecv;}
        const LTRSyncMarksRecvConfig &marksRecv() const {return  *m_syncRecv;}

        DeviceConfig *clone() const override {
            return new LTR22Config{*this};
        }

        LTR22Config();
        LTR22Config(const LTR22Config &cfg);
        ~LTR22Config() override;
    protected:
        void protUpdate() override;
    private:
        const std::unique_ptr<LTR22AdcConfig> m_adc;
        const std::unique_ptr<LTRSyncMarksRecvConfig> m_syncRecv;

    };
}
#endif // LQMEAS_LTR22CONFIG_H
