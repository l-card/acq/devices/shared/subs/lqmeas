#ifndef LQMEAS_LTR22TYPEINFO_H
#define LQMEAS_LTR22TYPEINFO_H


#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"
#include "lqmeas/ifaces/in/adc/DevAdcInfo.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvInfo.h"

namespace LQMeas {
    class LTR22TypeInfo : public LTRModuleTypeInfo, public DevAdcInfo, public LTRSyncMarksRecvInfo {
    public:
        static const QString &name();

        static const LTR22TypeInfo &defaultTypeInfo();

        const DevAdcInfo *adc() const override {
            return static_cast<const DevAdcInfo *>(this);
        }
        const DevSyncMarksRecvInfo *syncMarkRecv() const override {
            return static_cast<const DevSyncMarksRecvInfo *>(this);
        }

        QString deviceTypeName() const override {return name();}

        AdcType adcType() const override {return AdcType::Parallel;}
        int adcChannelsCnt() const override;
        double adcFreqMax() const override;

        bool adcIsChFreqConfigurable() const override {return false;}

        int adcChRangesCnt(int ch_num, int unit_mode_num = 0) const override;
        double adcChRangeMaxVal(int ch_num, int range, int unit_mode_num = 0) const override;
        double adcChRangeMinVal(int ch_num, int range, int unit_mode_num = 0) const override;

        static double typeAdcFreqVal(int freq_idx);
        static int typeAdcFreqsCnt();

        QList<const DeviceTypeInfo *> modificationList() const override;
    private:
        LTR22TypeInfo() {
        }
    };
}

#endif // LQMEAS_LTR22TYPEINFO_H
