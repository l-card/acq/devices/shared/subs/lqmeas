#include "LTRModuleTypeInfo.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/devs/DeviceTypeStdSeries.h"

const LQMeas::DeviceTypeSeries &LQMeas::LTRModuleTypeInfo::deviceTypeSeries() const {
    return DeviceTypeStdSeries::ltr();
}

QList<const LQMeas::DeviceInterface *> LQMeas::LTRModuleTypeInfo::supportedConInterfaces() const {
    static const QList<const DeviceInterface *> ifaces {
        &DeviceInterfaces::ltrModule()
    };
    return ifaces;
}
