#ifndef LQMEAS_LTRT13INFO_H
#define LQMEAS_LTRT13INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTRT13TypeInfo.h"

namespace LQMeas {
    class LTRT13Info: public DeviceInfo {
    public:
        explicit LTRT13Info(const LTRT13TypeInfo &type = LTRT13TypeInfo::defaultTypeInfo(),
                            const QString &serial = QString{}, int pld_ver = 0);
        LTRT13Info(const LTRT13Info &info);

        int pldVer() const {return m_pld_ver;}
        QString pldVerStr() const;

        DeviceInfo *clone() const override {return new LTRT13Info{*this};}
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        int m_pld_ver;
    };
}

#endif // LQMEAS_LTRT13INFO_H
