#include "LTRT13Info.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_pld_ver           {StdConfigKeys::pldVer()};

    LTRT13Info::LTRT13Info(const LTRT13TypeInfo &type, const QString &serial, int pld_ver) :
        DeviceInfo{type, serial}, m_pld_ver{pld_ver} {

    }

    LTRT13Info::LTRT13Info(const LTRT13Info &info) :
        DeviceInfo{info},
        m_pld_ver{info.m_pld_ver} {

    }

    QString LTRT13Info::pldVerStr() const {
        return QString::number(m_pld_ver);
    }

    void LTRT13Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_pld_ver] = m_pld_ver;
    }

    void LTRT13Info::protLoad(const QJsonObject &infoObj) {
        m_pld_ver = infoObj[cfgkey_pld_ver].toInt();
    }

}
