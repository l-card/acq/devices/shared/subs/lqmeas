#include "LTRT13.h"
#include "LTRT13Config.h"
#include "LTRT13Info.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdErrors.h"
#include <QDateTime>

extern "C" LTRT13API_DllExport(INT) LTRT13_WriteInfo(TLTRT13 *hnd);

namespace LQMeas {
    LTRT13::~LTRT13() {
        LTRT13_Close(&m_hnd);
    }

    bool LTRT13::isOpened() const {
        return LTRT13_IsOpened(&m_hnd) == LTR_OK;
    }

    QString LTRT13::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTRT13_GetErrorString(err));;
    }

    const LTRT13Config &LTRT13::devspecConfig() const {
        return static_cast<const LTRT13Config &>(config());
    }


    QSharedPointer<const LTRT13Info> LTRT13::devspecInfo() const {
        return devInfo().staticCast<const LTRT13Info>();
    }

    void LTRT13::setExtStartLevel(bool lvl, LQError &err) {
        getError(LTRT13_SetExtStartLevel(&m_hnd, lvl), err);
    }

    void LTRT13::setExtSyncGenEnabled(bool en, LQError &err) {
        getError(LTRT13_SetExtSyncGenEnabled(&m_hnd, en), err);
    }

    void LTRT13::setMarksGenEnabled(bool en, LQError &err) {
        getError(LTRT13_SetMarksGenEnabled(&m_hnd, en), err);
    }

    bool LTRT13::extStartLevel() const {
        return isOpened() && m_hnd.State.ExtStartLevel;
    }

    bool LTRT13::extSyncGenIsEnabled() const {
        return isOpened() && m_hnd.State.ExtSyncGenEn;
    }

    bool LTRT13::markGenIsEnabled() const {
        return isOpened() && m_hnd.State.MarkGenEn;
    }

    double LTRT13::rShuntCbrValue() const {
        return m_hnd.ModuleInfo.RValue;
    }

    QDateTime LTRT13::cbrTime() const {
        return m_hnd.ModuleInfo.CbrTime == 0 ? QDateTime() : QDateTime::fromMSecsSinceEpoch(m_hnd.ModuleInfo.CbrTime * 1000);
    }

    double LTRT13::rShuntNomValue() {
        return LTRT13_RVAL_NOM;
    }

    void LTRT13::writeCbrValue(double rval, LQError &err) {
        m_hnd.ModuleInfo.RValue = rval;
        m_hnd.ModuleInfo.CbrTime = QDateTime::currentMSecsSinceEpoch();
        getError(LTRT13_WriteInfo(&m_hnd), err);
    }

    void LTRT13::eraseCbrValue(LQError &err) {
        m_hnd.ModuleInfo.RValue = LTRT13_RVAL_NOM;
        m_hnd.ModuleInfo.CbrTime = 0;
        getError(LTRT13_WriteInfo(&m_hnd), err);
    }

    TLTR *LTRT13::channel() const {
        return &m_hnd.Channel;
    }

    int LTRT13::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        INT ltr_err = LTRT13_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, static_cast<WORD>(slot));
        if (LTRT13_IsOpened(&m_hnd) == LTR_OK) {
            setDeviceInfo(LTRT13Info{LTRT13TypeInfo::defaultTypeInfo(),
                                     QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                     m_hnd.ModuleInfo.VerPLD});
        }
        return ltr_err;
    }

    void LTRT13::protClose(LQError &err) {
        getError(LTRT13_Close(&m_hnd), err);
    }

    void LTRT13::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTRT13Config *setCfg {qobject_cast<const LTRT13Config *>(&cfg)};
        if (setCfg) {
            INT ltr_err {LTRT13_FillExtSyncFreqParams(&m_hnd, setCfg->extSyncFreq(), NULL)};
            if ((ltr_err == LTR_OK) && extSyncGenIsEnabled()) {
                /* повторное разрешение генерации для обновления частоты */
                ltr_err = LTRT13_SetExtSyncGenEnabled(&m_hnd, TRUE);
            }
            getError(ltr_err, err);
        }  else {
            err += StdErrors::InvalidConfigType();
        }
    }

    LTRT13::LTRT13(LQMeas::LTRCrate *crate, int slot, QObject *parent) :
        LTR01{crate, slot, typeModuleSubID, new LTRT13Config{}, LTRT13Info{}, parent} {

        LTRT13_Init(&m_hnd);
    }
}
