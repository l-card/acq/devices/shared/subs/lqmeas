#ifndef LTRT13_H
#define LTRT13_H

#include "ltr/include/ltrt13api.h"
#include "lqmeas/devs/LTR/modules/LTR01.h"

namespace LQMeas {
    class LTRT13Config;
    class LTRT13Info;

    class LTRT13  : public LTR01 {
        Q_OBJECT
    public:
        static const e_LTR01_SUBID typeModuleSubID {LTR01_SUBID_LTRT13};


        ~LTRT13() override;
        bool isOpened() const override;
        QString errorString(int err) const override;

        const LTRT13Config &devspecConfig() const;
        QSharedPointer<const LTRT13Info> devspecInfo() const;


        TLTRT13 *rawHandle() {return &m_hnd;}
        const TLTRT13 *rawHandle() const {return &m_hnd;}

        void setExtStartLevel(bool lvl, LQError &err);
        void setExtSyncGenEnabled(bool en, LQError &err);
        void setMarksGenEnabled(bool en, LQError &err);

        bool extStartLevel() const;
        bool extSyncGenIsEnabled() const;
        bool markGenIsEnabled() const;

        double rShuntCbrValue() const;
        QDateTime cbrTime() const;
        static double rShuntNomValue();

        void writeCbrValue(double rval, LQError &err);
        void eraseCbrValue(LQError &err);
    protected:
        TLTR *channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;

    private:
        explicit LTRT13(LTRCrate *crate, int slot, QObject *parent = nullptr);

        mutable TLTRT13 m_hnd;

        friend class LTRResolver;
    };
}

#endif // LTRT13_H
