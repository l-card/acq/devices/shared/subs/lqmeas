#ifndef LQMEAS_LTRT13TYPEINFO_H
#define LQMEAS_LTRT13TYPEINFO_H

#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"

namespace LQMeas {
    class LTRT13TypeInfo : public LTRModuleTypeInfo  {
    public:
        static const QString &name();
        QString deviceTypeName() const override {return name();}

        static const LTRT13TypeInfo &defaultTypeInfo();

        QList<const DeviceTypeInfo *> modificationList() const override;
    private:
        LTRT13TypeInfo();
    };
}
#endif // LQMEAS_LTRT13TYPEINFO_H
