#ifndef LQMEAS_LTRT13CONFIG_H
#define LQMEAS_LTRT13CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"

namespace LQMeas {
    class LTRT13Config: public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        DeviceConfig *clone() const override {
            return new LTRT13Config{*this};
        }


        explicit LTRT13Config();
        LTRT13Config(const LTRT13Config &cfg);

        double extSyncFreq() const {return m_results.extSyncFreq;}
        double configuredExtSyncFreq() const {return m_params.extSyncFreq;}

    public Q_SLOTS:
        void setExtSyncFreq(double freq);
    protected:
        void protUpdate() override;
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        static constexpr double defaultExtSyncFreq() {return 400000;}

        struct {
            double extSyncFreq;
        } m_params;

        struct {
            double extSyncFreq;
        } m_results;
    };
}

#endif // LQMEAS_LTRT13CONFIG_H
