#include "LTRT13TypeInfo.h"

namespace LQMeas {
    const QString &LTRT13TypeInfo::name() {
        static const QString str {QStringLiteral("LTRT13") };
        return str;
    }
    
    const LTRT13TypeInfo &LTRT13TypeInfo::defaultTypeInfo() {
        static const LTRT13TypeInfo type;
        return type;
    }

    QList<const DeviceTypeInfo *> LTRT13TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list {
                                                        &defaultTypeInfo()
        };
        return list;
    }

    LTRT13TypeInfo::LTRT13TypeInfo() {

    }
}
