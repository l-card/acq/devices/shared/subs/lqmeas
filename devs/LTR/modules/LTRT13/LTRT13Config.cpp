#include "LTRT13Config.h"
#include "LTRT13TypeInfo.h"
#include <QJsonObject>
#include "ltr/include/ltrt13api.h"
#include "lqmeas/lqtdefs.h"

namespace LQMeas {
    static const QLatin1String cfgkey_ltrt13_obj                {"LTRT13"};
    static const QLatin1String cfgkey_ltrt13_ext_sync_freq      {"ExtSyncFreq"};

    const QString &LTRT13Config::typeConfigName() {
        return LTRT13TypeInfo::name();
    }

    LTRT13Config::LTRT13Config() {
        m_params.extSyncFreq = m_results.extSyncFreq = defaultExtSyncFreq();
    }

    LTRT13Config::LTRT13Config(const LTRT13Config &cfg) {
        memcpy(&m_params, &cfg.m_params, sizeof(m_params));
        memcpy(&m_results, &cfg.m_results, sizeof(m_results));
    }

    void LTRT13Config::setExtSyncFreq(double freq) {
        if (!LQREAL_IS_EQUAL(freq, m_params.extSyncFreq)) {
            m_params.extSyncFreq = freq;
            notifyConfigChanged();
        }
    }

    void LTRT13Config::protUpdate() {
        DeviceConfig::protUpdate();
        LTRT13_FindExtSyncFreqParams(m_params.extSyncFreq, nullptr, &m_results.extSyncFreq);
    }

    void LTRT13Config::protSave(QJsonObject &cfgObj) const {
        DeviceConfig::protSave(cfgObj);
        QJsonObject ltrt13obj;
        ltrt13obj[cfgkey_ltrt13_ext_sync_freq] = m_params.extSyncFreq;
        cfgObj[cfgkey_ltrt13_obj] = ltrt13obj;
    }

    void LTRT13Config::protLoad(const QJsonObject &cfgObj) {
        DeviceConfig::protLoad(cfgObj);
        const QJsonObject &ltrt13obj {cfgObj[cfgkey_ltrt13_obj].toObject()};
        m_params.extSyncFreq = ltrt13obj[cfgkey_ltrt13_ext_sync_freq].toDouble(defaultExtSyncFreq());
    }
}
