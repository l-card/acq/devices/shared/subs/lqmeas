#ifndef LTR27MEZZANINEINFO_H
#define LTR27MEZZANINEINFO_H

#include "LTR27MezzanineTypeInfo.h"

namespace LQMeas {
    class LTR27MezzanineInfo  {
    public:
        explicit LTR27MezzanineInfo(const LTR27MezzanineTypeInfo &type,
                           const QString &serial = QString{},
                           const QString &revision = QString{}) :
            m_type{&type}, m_serial{serial}, m_revision{revision} {
        }

        LTR27MezzanineInfo(const LTR27MezzanineInfo &info) :
            m_type{&info.type()}, m_serial{info.serial()}, m_revision{info.revision()} {
        }


        const LTR27MezzanineTypeInfo &type() const {return *m_type;}
        const QString &serial() const {return m_serial;}
        const QString &revision() const {return m_revision;}
    private:
        const LTR27MezzanineTypeInfo *const m_type;
        QString m_serial;
        QString m_revision;
    };
}

#endif // LTR27MEZZANINEINFO_H
