#include "LTR27AdcConfig.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>
#include <QJsonArray>
#include "ltr/include/ltr27api.h"
#include "lqmeas/lqtdefs.h"

namespace LQMeas {
    static const QLatin1String &cfgkey_ch_enabled         {StdConfigKeys::enabled()};
    static const QLatin1String &cfgkey_channels_arr       {StdConfigKeys::channels()};
    static const QLatin1String &cfgkey_adc_freq           {StdConfigKeys::adcFreq()};

    LTR27AdcConfig::LTR27AdcConfig(const DevAdcInfo &info) : DevAdcConfig{info} {
        memset(&m_params, 0, sizeof(m_params));
        m_params.adc_freq = m_results.adc_freq = adcDefaultFreq();
    }

    LTR27AdcConfig::LTR27AdcConfig(const LTR27AdcConfig &cfg) : DevAdcConfig{cfg} {
        memcpy(&m_params, &cfg.m_params, sizeof(m_params));
        memcpy(&m_results, &cfg.m_results, sizeof(m_results));
    }

    void LTR27AdcConfig::adcSetChEnabled(int ch, bool en) {
        if (m_params.ch[ch].enabled != en) {
            m_params.ch[ch].enabled = en;
            notifyChEnableStateChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR27AdcConfig::adcSetFreq(double freq) {
         if (!LQREAL_IS_EQUAL(m_params.adc_freq, freq)) {
             m_params.adc_freq = freq;
             notifyAdcFreqChanged();
             notifyChFreqChanged();
             notifyConfigChanged();
         }
    }

    void LTR27AdcConfig::protSave(QJsonObject &cfgObj) const {
        QJsonArray chArray;
        for (int ch_num {0}; ch_num < LTR27TypeInfo::adc_channels_cnt; ++ch_num) {
            QJsonObject chObj;
            chObj[cfgkey_ch_enabled] = m_params.ch[ch_num].enabled;
            chArray.append(chObj);
        }
        cfgObj[cfgkey_channels_arr] = chArray;
        cfgObj[cfgkey_adc_freq] = m_params.adc_freq;
    }

    void LTR27AdcConfig::protLoad(const QJsonObject &cfgObj) {
        const QJsonArray &chArray {cfgObj[cfgkey_channels_arr].toArray()};
        const int ch_cnt {chArray.size()};
        for (int ch_num {0}; (ch_num < ch_cnt) && (ch_num < LTR27TypeInfo::adc_channels_cnt); ++ch_num) {
            const QJsonObject &chObj {chArray.at(ch_num).toObject()};
            m_params.ch[ch_num].enabled = chObj[cfgkey_ch_enabled].toBool();
        }
        m_params.adc_freq = cfgObj[cfgkey_adc_freq].toDouble(adcDefaultFreq());
    }

    void LTR27AdcConfig::protUpdate() {
        DevAdcConfig::protUpdate();
        LTR27_FindAdcFreqParams(m_params.adc_freq, nullptr, &m_results.adc_freq);
    }
}
