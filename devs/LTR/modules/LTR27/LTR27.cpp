#include "LTR27.h"
#include "LTR27Info.h"
#include "LTR27Config.h"
#include "LTR27AdcConfig.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "lqmeas/devs/LTR/modules/LTRModuleFrameReceiver.h"
#include "lqmeas/log/Log.h"
#include "lqmeas/StdErrors.h"

namespace LQMeas {
    LTR27::LTR27(LTRCrate *crate, int slot, QObject *parent) :
        LTRModule{crate, slot, typeModuleID, new LTR27Config{}, LTR27Info{}, parent},
        DevAdc{this, LTR27TypeInfo::adc_channels_cnt},
        m_receiver{new LTRModuleFrameReceiver{this}} {

        LTR27_Init(&m_hnd);
    }

    LTR27::~LTR27() {
        LTR27_Close(&m_hnd);
    }

    bool LTR27::isOpened() const {
        return LTR27_IsOpened(&m_hnd) == LTR_OK;
    }

    DevSyncMarksRecv *LTR27::devSyncMarkRecv() {
        return m_receiver.get();
    }

    QString LTR27::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTR27_GetErrorString(err));
    }

    const LTR27Config &LTR27::devspecConfig() const {
        return static_cast<const LTR27Config &>(config());
    }

    QSharedPointer<const LTR27Info> LTR27::devspecInfo() const {
        return devInfo().staticCast<const LTR27Info>();
    }

    TLTR *LTR27::channel() const {
        return &m_hnd.Channel;
    }

    int LTR27::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        INT ltr_err {LTR27_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, static_cast<WORD>(slot))};
        if (ltr_err == LTR_OK) {
            ltr_err = LTR27_GetConfig(&m_hnd);
        }
        if (ltr_err == LTR_OK) {
            ltr_err = LTR27_GetDescription(&m_hnd, LTR27_MODULE_DESCRIPTION);
        }
        if (ltr_err == LTR_OK) {
            for (int mez_idx = 0; (mez_idx < LTR27TypeInfo::mezzanines_cnt) &&
                                  (ltr_err == LTR_OK); ++mez_idx) {
                if (strcmp(m_hnd.Mezzanine[mez_idx].Name, "EMPTY") != 0) {
                    ltr_err = LTR27_GetDescription(&m_hnd, static_cast<WORD>(LTR27_MEZZANINE1_DESCRIPTION << mez_idx));
                }
            }
        }
        if (ltr_err == LTR_OK) {
            const QList<const LTR27MezzanineTypeInfo *> &mezTypes {LTR27MezzanineTypeInfo::allTypes()};

            QList<LTR27MezzanineInfo *> mez_infos;
            for (int mez_idx {0}; mez_idx < LTR27TypeInfo::mezzanines_cnt; ++mez_idx) {
                LTR27MezzanineInfo *mezInfo = nullptr;

                /* копируем калибровочные коэффициенты (в противном случае не будут применяться) */
                for(int cbr_idx {0}; cbr_idx < 4; ++cbr_idx) {
                    m_hnd.Mezzanine[mez_idx].CalibrCoeff[cbr_idx] = m_hnd.ModuleInfo.Mezzanine[mez_idx].Calibration[cbr_idx];
                }

                if(m_hnd.ModuleInfo.Mezzanine[mez_idx].Active) {
                    const QString mezIdName {QSTRING_FROM_CSTR(reinterpret_cast<char*>(m_hnd.ModuleInfo.Mezzanine[mez_idx].Name))};
                    const auto &it {std::find_if(mezTypes.cbegin(), mezTypes.cend(),
                                                [&mezIdName](const auto mezType){return mezType->idName() == mezIdName;})};
                    if (it != mezTypes.cend()) {
                        mezInfo = new LTR27MezzanineInfo{*(*it),
                                                         QSTRING_FROM_CSTR(reinterpret_cast<char*>(m_hnd.ModuleInfo.Mezzanine[mez_idx].SerialNumber)),
                                                         QString(QChar(static_cast<char>(m_hnd.ModuleInfo.Mezzanine[mez_idx].Revision)))};

                    }
                }
                mez_infos.append(mezInfo);
            }


            setDeviceInfo(LTR27Info{mez_infos,
                                    QSTRING_FROM_CSTR(reinterpret_cast<char*>(m_hnd.ModuleInfo.Module.SerialNumber)),
                                    static_cast<unsigned>(m_hnd.ModuleInfo.Cpu.FirmwareVersion),
                                    static_cast<int>(m_hnd.ModuleInfo.Module.VerPLD)});
        }
        return ltr_err;
    }

    void LTR27::protClose(LQError &err) {
        return getError(LTR27_Close(&m_hnd), err);
    }

    void LTR27::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTR27Config *setCfg {qobject_cast<const LTR27Config *>(&cfg)};
        if (setCfg) {
            LTR27_FindAdcFreqParams(setCfg->adc().adcFreq(), &m_hnd.FrequencyDivisor, nullptr);
            m_en_ch_mask = 0;
            for (int ch_num {0}; ch_num < LTR27TypeInfo::adc_channels_cnt; ++ch_num) {
                if (setCfg->adc().adcChEnabled(ch_num)) {
                    m_en_ch_mask |= (1 << ch_num);
                }
            }
            getError(LTR27_SetConfig(&m_hnd), err);
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }

    INT LQMeas::LTR27::ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) {
        return LTR27_Recv(&m_hnd, wrds, marks, size, tout);
    }

    void LTR27::protAdcStart(LQError &err) {
        getError(LTR27_ADCStart(&m_hnd), err);
        if (err.isSuccess()) {
            m_run = true;
            int frameSize {LTR27TypeInfo::adc_channels_cnt};
            m_receiver->start(frameSize, frameSize);
        }
    }

    void LTR27::protAdcStop(LQError &err) {
        getError(LTR27_ADCStop(&m_hnd), err);
        m_run = false;
        m_receiver->clear();
    }

    void LTR27::protAdcGetData(double *data, int size, unsigned flags,
                               unsigned tout, int &recvd_size, LQError &err) {
        DWORD *wrds;
        DWORD proc_size {0};
        const int en_ch_cnt {adcConfig().adcEnabledChCnt()};
        const int frames_cnt {size/en_ch_cnt};
        int recvd_words {0};


        m_receiver->getFrames(frames_cnt * LTR27TypeInfo::adc_channels_cnt, tout,
                              wrds, recvd_words, err);
        if (err.isSuccess() && (recvd_words != 0)) {
            proc_size = static_cast<DWORD>(recvd_words);

            getError(LTR27_ProcessDataEx(&m_hnd, wrds, data, &proc_size,
                                         LTR27_PROC_FLAG_CALIBR | LTR27_PROC_FLAG_CONV_VALUE,
                                         m_en_ch_mask, nullptr), err);
            if (!err.isSuccess()) {
                LQMeasLog->error(tr("Process data error"), err, this);
            }
        }

        if (err.isSuccess())
            recvd_size = static_cast<int>(proc_size);
    }
}
