#include "LTR27Config.h"
#include "LTR27AdcConfig.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvConfig.h"

namespace LQMeas {

    const QString &LTR27Config::typeConfigName() {
        return LTR27TypeInfo::name();
    }

    const DevAdcConfig *LTR27Config::adcConfig() const  {
        return m_adc.get();
    }

    const DevSyncMarksRecvConfig *LTR27Config::syncMarksRecvConfig() const {
        return m_syncRecv.get();
    }

    double LTR27Config::inPeakSpeed() const {
        /** передаются всегда данные по всем каналам, независимо от разрешения и наличия мезанинов */
        return  m_adc->adcEnabledChCnt() > 0 ? m_adc->adcChFreq() * m_adc->adcSampleSize() * LTR27TypeInfo::adc_channels_cnt : 0;
    }

    LTR27Config::LTR27Config() :
        m_adc{new LTR27AdcConfig{LTR27TypeInfo::defaultTypeInfo()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this}} {

        init();
    }

    LTR27Config::LTR27Config(const LTR27Config &cfg) :
        m_adc{new LTR27AdcConfig{cfg.adc()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this, cfg.marksRecv()}} {

        init();
    }

    void LTR27Config::protUpdate() {
        m_syncRecv->updateSyncMarksRecvFreq(m_adc->adcChFreq());
    }

    LQMeas::LTR27Config::~LTR27Config() {

    }
}
