#ifndef LQMEAS_LTR27MEZZANINETYPEINFO_H
#define LQMEAS_LTR27MEZZANINETYPEINFO_H

#include <QString>
#include <QList>
#include "lqmeas/units/Unit.h"



namespace LQMeas {
    class LTR27MezzanineTypeInfo {
    public:
        virtual ~LTR27MezzanineTypeInfo();

        virtual QString idName() const = 0;
        virtual QString displayName() const = 0;
        virtual int channelsCnt() const = 0;
        virtual const Unit &units() const = 0;
        virtual double rangeMaxVal() const = 0;
        virtual double rangeMinVal() const = 0;


        static const LTR27MezzanineTypeInfo &U01();
        static const LTR27MezzanineTypeInfo &U10();
        static const LTR27MezzanineTypeInfo &U20();
        static const LTR27MezzanineTypeInfo &U100();
        static const LTR27MezzanineTypeInfo &I5();
        static const LTR27MezzanineTypeInfo &I10();
        static const LTR27MezzanineTypeInfo &I20();
        static const LTR27MezzanineTypeInfo &T();
        static const LTR27MezzanineTypeInfo &R100();
        static const LTR27MezzanineTypeInfo &R250();

        static const QList<const LTR27MezzanineTypeInfo *> &allTypes();
    };
}

#endif // LTR27MEZZANINETYPEINFO_H
