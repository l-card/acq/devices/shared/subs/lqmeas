#include "LTR27Info.h"
#include <QJsonObject>
#include <QJsonArray>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas{
    static const QLatin1String &cfgkey_mezzanines_group     {StdConfigKeys::mezzanines()};
    static const QLatin1String &cfgkey_mezzanine_key_type   {StdConfigKeys::type()};
    static const QLatin1String &cfgkey_mezzanine_key_serial {StdConfigKeys::serialNumber()};

    static const QLatin1String &cfgkey_mcu_ver              {StdConfigKeys::mcuVer()};
    static const QLatin1String &cfgkey_pld_ver              {StdConfigKeys::pldVer()};

    LTR27Info::LTR27Info(const LTR27TypeInfo &type, const QString &serial, unsigned mcu_ver, int pld_ver) :
        DeviceInfo{type, serial},
        m_mcu_ver{mcu_ver},
        m_pld_ver{pld_ver} {

        for (int mez_idx {0}; mez_idx < LTR27TypeInfo::mezzanines_cnt; ++mez_idx) {
            m_mezzanines.append(nullptr);
        }
    }

    LTR27Info::LTR27Info(QList<LTR27MezzanineInfo *> mezzanines, const QString &serial, unsigned mcu_ver, int pld_ver) :
        DeviceInfo{LTR27TypeInfo{mezzaninesTypesList(mezzanines)}, serial},
        m_mcu_ver{mcu_ver},
        m_pld_ver{pld_ver} {

        for (int mez_idx {0}; mez_idx < LTR27TypeInfo::mezzanines_cnt; ++mez_idx) {
            m_mezzanines.append(mez_idx < mezzanines.size() ? mezzanines.at(mez_idx) : nullptr);
        }
    }

    LTR27Info::~LTR27Info() {
        qDeleteAll(m_mezzanines);
    }

    QString LTR27Info::mcuVerStr() const {
        return QString{"%0.%1.%2.%3"}
                .arg(QString::number((m_mcu_ver >> 24) & 0xFF),
                     QString::number((m_mcu_ver >> 16) & 0xFF),
                     QString::number((m_mcu_ver >>  8) & 0xFF),
                     QString::number((m_mcu_ver >>  0) & 0xFF)
        );
    }

    void LTR27Info::saveMezzanineInfo(QJsonObject &infoObj, const QList<LTR27MezzanineInfo *> &mezInfoList)  {
        QJsonArray mezArray;
        for (int mez_idx {0}; mez_idx < LTR27TypeInfo::mezzanines_cnt; ++mez_idx) {
            QJsonObject mezObj;
            LTR27MezzanineInfo *mezInfo {mez_idx < mezInfoList.size() ? mezInfoList.at(mez_idx) : nullptr};
            if (mezInfo) {
                mezObj[cfgkey_mezzanine_key_type] = mezInfo->type().idName();
                mezObj[cfgkey_mezzanine_key_serial] = mezInfo->serial();
            }
            mezArray.append(mezObj);
        }
        infoObj[cfgkey_mezzanines_group] = mezArray;
    }

    void LTR27Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_mcu_ver] = static_cast<int>(m_mcu_ver);
        infoObj[cfgkey_pld_ver] = m_pld_ver;
        saveMezzanineInfo(infoObj, m_mezzanines);
    }

    void LTR27Info::protLoad(const QJsonObject &infoObj) {
        m_mcu_ver = infoObj[cfgkey_mcu_ver].toInt();
        m_pld_ver = infoObj[cfgkey_pld_ver].toInt();

        const QJsonArray &mezArray {infoObj[cfgkey_mezzanines_group].toArray()};
        qDeleteAll(m_mezzanines);
        for (int mez_idx {0}; mez_idx < LTR27TypeInfo::mezzanines_cnt; ++mez_idx) {
            LTR27MezzanineInfo *info {nullptr};
            if (mez_idx < mezArray.size()) {
                const QJsonObject &mezObj {mezArray.at(mez_idx).toObject()};
                const QString &mezTypeId {mezObj[cfgkey_mezzanine_key_type].toString()};
                const QList<const LTR27MezzanineTypeInfo *> &mezTypes {LTR27MezzanineTypeInfo::allTypes()};
                const auto &it {std::find_if(mezTypes.cbegin(), mezTypes.cend(),
                                            [&mezTypeId](const auto mezType){return mezType->idName() == mezTypeId;})};
                if (it != mezTypes.cend()) {
                    info = new LTR27MezzanineInfo{*(*it), mezObj[cfgkey_mezzanine_key_serial].toString()};
                }
            }
            m_mezzanines[mez_idx] = info;
        }
        setType(new LTR27TypeInfo{mezzaninesTypesList(m_mezzanines)});
    }

    QList<const LTR27MezzanineTypeInfo *> LTR27Info::mezzaninesTypesList(
            const QList<LTR27MezzanineInfo *> &infoList) {
        QList<const LTR27MezzanineTypeInfo *> ret;
        for (LTR27MezzanineInfo *info : infoList) {
            ret.append(info ? &info->type() : nullptr);
        }
        return ret;
    }

    LTR27Info::LTR27Info(const LTR27Info &info):
        DeviceInfo{info},
        m_mcu_ver{info.m_mcu_ver},
        m_pld_ver{info.m_pld_ver} {
        for (int mez_idx {0}; mez_idx < LTR27TypeInfo::mezzanines_cnt; ++mez_idx) {
            m_mezzanines.append(info.m_mezzanines.at(mez_idx) ?
                                new LTR27MezzanineInfo{*info.m_mezzanines.at(mez_idx)} : nullptr);
        }
    }
}
