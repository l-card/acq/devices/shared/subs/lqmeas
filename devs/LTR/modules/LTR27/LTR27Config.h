#ifndef LQMEAS_LTR27CONFIG_H
#define LQMEAS_LTR27CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"
#include <memory>

namespace LQMeas {
    class LTR27AdcConfig;
    class LTRSyncMarksRecvConfig;

    class LTR27Config : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        const DevAdcConfig *adcConfig() const override;
        const DevSyncMarksRecvConfig *syncMarksRecvConfig() const override;

        LTR27AdcConfig &adc() {return *m_adc;}
        const LTR27AdcConfig &adc() const {return *m_adc;}
        LTRSyncMarksRecvConfig &marksRecv() {return *m_syncRecv;}
        const LTRSyncMarksRecvConfig &marksRecv() const {return *m_syncRecv;}

        DeviceConfig *clone() const override {
            return new LTR27Config{*this};
        }

        double inPeakSpeed() const override;

        LTR27Config();
        LTR27Config(const LTR27Config &cfg);
        ~LTR27Config() override;
    protected:
        void protUpdate() override;
    private:
        const std::unique_ptr<LTR27AdcConfig> m_adc;
        const std::unique_ptr<LTRSyncMarksRecvConfig> m_syncRecv;
    };
}

#endif // LQMEAS_LTR27CONFIG_H
