#ifndef LQMEAS_LTR27TYPEINFO_H
#define LQMEAS_LTR27TYPEINFO_H


#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"
#include "lqmeas/ifaces/in/adc/DevAdcInfo.h"
#include "LTR27MezzanineTypeInfo.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvInfo.h"

namespace LQMeas {
    class LTR27TypeInfo : public LTRModuleTypeInfo, public DevAdcInfo, public LTRSyncMarksRecvInfo {
    public:
        static const QString &name();
        static const LTR27TypeInfo &defaultTypeInfo();

        const DevAdcInfo *adc() const override {
            return static_cast<const DevAdcInfo *>(this);
        }
        const DevSyncMarksRecvInfo *syncMarkRecv() const override {
            return static_cast<const DevSyncMarksRecvInfo *>(this);
        }

        static const int adc_channels_cnt {16};
        static const int mezzanines_cnt {8};
        static const int adc_channesls_per_mezzanine {2};


        QString deviceTypeName() const override {return name();}

        AdcType adcType() const override {return AdcType::Parallel;}
        int adcChannelsCnt() const override {return 16;}
        double adcFreqMax() const override {return 1000;}
        double adcFreqDefault() const override {return 10;}
        bool adcIsChFreqConfigurable() const override {return false;}

        int adcChRangesCnt(int ch_num, int unit_mode_num = 0) const override;
        double adcChRangeMaxVal(int ch_num, int range, int unit_mode_num = 0) const override;
        double adcChRangeMinVal(int ch_num, int range, int unit_mode_num = 0) const override;
        bool adcChAvailable(int ch_num) const override;
        const Unit &adcChRangeUnit(int ch_num, int unit_mode_num = 0) const override;

        const LTR27MezzanineTypeInfo *mezzanine(int mez_idx) const {return m_mezzanines.at(mez_idx);}

        explicit LTR27TypeInfo(const QList<const LTR27MezzanineTypeInfo *> &mezzanines);
        const DeviceTypeInfo *clone() const override;

        QList<const DeviceTypeInfo *> modificationList() const override;
    private:
        LTR27TypeInfo();

        bool isDynamicAllocated() const override {return m_dyn_allocated;}

        QList<const LTR27MezzanineTypeInfo *> m_mezzanines;
        const bool m_dyn_allocated;
    };
}
#endif // LQMEAS_LTR27TYPEINFO_H
