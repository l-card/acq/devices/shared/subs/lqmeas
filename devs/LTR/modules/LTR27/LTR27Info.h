#ifndef LQMEAS_LTR27INFO_H
#define LQMEAS_LTR27INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTR27TypeInfo.h"
#include "LTR27MezzanineInfo.h"

namespace LQMeas {
    class LTR27Info : public DeviceInfo {
    public:
        explicit LTR27Info(const LTR27TypeInfo &type = LTR27TypeInfo::defaultTypeInfo(),
                  const QString &serial = QString{}, unsigned mcu_ver = 0, int pld_ver = 0);
        explicit LTR27Info(QList<LTR27MezzanineInfo *> mezzanines,
                  const QString &serial = QString{}, unsigned mcu_ver = 0, int pld_ver = 0);
        ~LTR27Info() override;


        DeviceInfo *clone() const override {return new LTR27Info{*this};}
        unsigned mcuVer() const {return m_mcu_ver;}
        QString mcuVerStr() const;
        int pldVer() const {return m_pld_ver;}
        QString pldVerStr() const {return QString::number(m_pld_ver);}


        const LTR27MezzanineInfo *mezzanine(int mez_idx) const {
            return m_mezzanines.at(mez_idx);
        }

        const LTR27TypeInfo &devspecTypeInfo() const {
            return static_cast<const LTR27TypeInfo &>(type());
        }

        static void saveMezzanineInfo(QJsonObject &infoObj, const QList<LTR27MezzanineInfo *> &mezInfoList);
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        static QList<const LTR27MezzanineTypeInfo *> mezzaninesTypesList(
                const QList<LTR27MezzanineInfo *> &infoList);

        LTR27Info(const LTR27Info &info);

        unsigned m_mcu_ver;
        int m_pld_ver;
        QList<LTR27MezzanineInfo *> m_mezzanines;
    };
}
#endif // LQMEAS_LTR27INFO_H
