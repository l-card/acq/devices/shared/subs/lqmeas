#ifndef LQMEAS_LTR27ADCCONFIG_H
#define LQMEAS_LTR27ADCCONFIG_H

#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "LTR27TypeInfo.h"

namespace LQMeas {
    class LTR27AdcConfig : public DevAdcConfig {
        Q_OBJECT
    public:
        double adcFreq() const override {return m_results.adc_freq;}
        double adcConfiguredFreq() const {return m_params.adc_freq; }
        bool adcChEnabled(int ch) const override {return m_params.ch[ch].enabled;}
        int adcChRangeNum(int ch) const override { Q_UNUSED(ch) return 0;}

        int adcSampleSize() const override {return 4;}
    public Q_SLOTS:
        void adcSetChEnabled(int ch, bool en);
        void adcSetFreq(double freq);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
        void protUpdate() override;
    private:
        explicit LTR27AdcConfig(const DevAdcInfo &info);
        LTR27AdcConfig(const LTR27AdcConfig &cfg);

        struct {
            struct {
                bool enabled;
            } ch[LTR27TypeInfo::adc_channels_cnt];
            double   adc_freq;
        } m_params;
        struct {
            double adc_freq;
        } m_results;

        friend class LTR27Config;
    };
}

#endif // LQMEAS_LTR27ADCCONFIG_H
