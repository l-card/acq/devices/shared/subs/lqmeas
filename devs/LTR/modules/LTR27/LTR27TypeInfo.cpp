#include "LTR27TypeInfo.h"
#include "lqmeas/units/std/Special.h"
namespace LQMeas {
    const QString &LTR27TypeInfo::name() {
        static const QString str {QStringLiteral("LTR27")};
        return str;
    }
    int LTR27TypeInfo::adcChRangesCnt(int ch_num, int unit_mode_num) const {
        Q_UNUSED(unit_mode_num)
        return adcChAvailable(ch_num) ? 1 : 0;
    }

    double LTR27TypeInfo::adcChRangeMaxVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(range) Q_UNUSED(unit_mode_num)
        return adcChAvailable(ch_num) ? m_mezzanines.at(ch_num / adc_channesls_per_mezzanine)->rangeMaxVal() : 0;
    }

    double LTR27TypeInfo::adcChRangeMinVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(range) Q_UNUSED(unit_mode_num)
        return adcChAvailable(ch_num) ? m_mezzanines.at(ch_num / adc_channesls_per_mezzanine)->rangeMinVal() : 0;
    }

    bool LTR27TypeInfo::adcChAvailable(int ch_num) const {
        int mezzanine_num = ch_num / adc_channesls_per_mezzanine;
        int mezzanine_ch  = ch_num % adc_channesls_per_mezzanine;
        return m_mezzanines.at(mezzanine_num) && (mezzanine_ch < m_mezzanines.at(mezzanine_num)->channelsCnt());
    }

    const Unit &LTR27TypeInfo::adcChRangeUnit(int ch_num, int unit_mode_num) const {
        Q_UNUSED(unit_mode_num)
        return adcChAvailable(ch_num) ? m_mezzanines.at(ch_num / adc_channesls_per_mezzanine)->units() : Units::Special::unknown();
    }

    LTR27TypeInfo::LTR27TypeInfo(const QList<const LTR27MezzanineTypeInfo *> &mezzanines) :
        m_dyn_allocated{true} {

        for (int mez_idx {0}; mez_idx < mezzanines_cnt; ++mez_idx) {
            m_mezzanines.append(mez_idx < mezzanines.size() ? mezzanines.at(mez_idx) : nullptr);
        }
    }

    const DeviceTypeInfo *LTR27TypeInfo::clone() const {
        return isDynamicAllocated() ? new LTR27TypeInfo{m_mezzanines} : this;
    }

    QList<const DeviceTypeInfo *> LTR27TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list {
                                                        &defaultTypeInfo()
        };
        return list;
    }

    LTR27TypeInfo::LTR27TypeInfo() : m_dyn_allocated{false} {
        for (int mez_idx {0}; mez_idx < mezzanines_cnt; ++mez_idx) {
            m_mezzanines.append(nullptr);
        }
    }
    
    const LTR27TypeInfo &LTR27TypeInfo::defaultTypeInfo() {
        static const LTR27TypeInfo info;
        return info;
    }


}
