#include "LTR27MezzanineTypeInfo.h"
#include "lqmeas/units/std/Voltage.h"
#include "lqmeas/units/std/Current.h"
#include "lqmeas/units/std/Resistance.h"

namespace LQMeas {
    LTR27MezzanineTypeInfo::~LTR27MezzanineTypeInfo() {

    }

    const LTR27MezzanineTypeInfo &LTR27MezzanineTypeInfo::U01() {
        static const class LTR27MezzanineTypeInfoU01 : public LTR27MezzanineTypeInfo {
            QString idName() const override {return QStringLiteral("U01");}
            QString displayName() const override {return QStringLiteral("H-27U-01");}
            int channelsCnt() const override {return 2;}
            const Unit &units() const override {return Units::Voltage::V();}
            double rangeMaxVal() const override {return 1.;}
            double rangeMinVal() const override {return -1.;}
        } info;
        return info;
    }

    const LTR27MezzanineTypeInfo &LTR27MezzanineTypeInfo::U10() {
        static const class LTR27MezzanineTypeInfoU10 : public LTR27MezzanineTypeInfo {
            QString idName() const override {return QStringLiteral("U10");}
            QString displayName() const override {return QStringLiteral("H-27U-10");}
            int channelsCnt() const override {return 2;}
            const Unit &units() const override {return Units::Voltage::V();}
            double rangeMaxVal() const override {return 10.;}
            double rangeMinVal() const override {return -10.;}
        } info;
        return info;
    }

    const LTR27MezzanineTypeInfo &LTR27MezzanineTypeInfo::U20() {
        static const class LTR27MezzanineTypeInfoU20 : public LTR27MezzanineTypeInfo {
            QString idName() const override {return QStringLiteral("U20");}
            QString displayName() const override {return QStringLiteral("H-27U-20");}
            int channelsCnt() const override {return 2;}
            const Unit &units() const override {return Units::Voltage::V();}
            double rangeMaxVal() const override {return 20.;}
            double rangeMinVal() const override {return 0.;}
        } info;
        return info;
    }

    const LTR27MezzanineTypeInfo &LTR27MezzanineTypeInfo::U100() {
        static const class LTR27MezzanineTypeInfoU100 : public LTR27MezzanineTypeInfo {
            QString idName() const override {return QStringLiteral("U100");}
            QString displayName() const override {return QStringLiteral("H-27U-100");}
            int channelsCnt() const override {return 2;}
            const Unit &units() const override {return Units::Voltage::V();}
            double rangeMaxVal() const override {return 100.;}
            double rangeMinVal() const override {return 0.;}
        } info;
        return info;
    }

    const LTR27MezzanineTypeInfo &LTR27MezzanineTypeInfo::I5() {
        static const class LTR27MezzanineTypeInfoI5 : public LTR27MezzanineTypeInfo {
            QString idName() const override {return QStringLiteral("I5");}
            QString displayName() const override {return QStringLiteral("H-27I-5");}
            int channelsCnt() const override {return 2;}
            const Unit &units() const override {return Units::Current::mA();}
            double rangeMaxVal() const override {return 5.;}
            double rangeMinVal() const override {return 0.;}
        } info;
        return info;
    }

    const LTR27MezzanineTypeInfo &LTR27MezzanineTypeInfo::I10() {
        static const class LTR27MezzanineTypeInfoI10 : public LTR27MezzanineTypeInfo {
            QString idName() const override {return QStringLiteral("I10");}
            QString displayName() const override {return QStringLiteral("H-27I-10");}
            int channelsCnt() const override {return 2;}
            const Unit &units() const override {return Units::Current::mA();}
            double rangeMaxVal() const override {return 10.;}
            double rangeMinVal() const override {return -10.;}
        } info;
        return info;
    }

    const LTR27MezzanineTypeInfo &LTR27MezzanineTypeInfo::I20() {
        static const class LTR27MezzanineTypeInfoI20 : public LTR27MezzanineTypeInfo {
            QString idName() const override {return QStringLiteral("I20");}
            QString displayName() const override {return QStringLiteral("H-27I-20");}
            int channelsCnt() const override {return 2;}
            const Unit &units() const override {return Units::Current::mA();}
            double rangeMaxVal() const override {return 20.;}
            double rangeMinVal() const override {return 0.;}
        } info;
        return info;
    }

    const LTR27MezzanineTypeInfo &LTR27MezzanineTypeInfo::T() {
        static const class LTR27MezzanineTypeInfoT : public LTR27MezzanineTypeInfo {
            QString idName() const override {return QStringLiteral("T");}
            QString displayName() const override {return QStringLiteral("H-27T");}
            int channelsCnt() const override {return 2;}
            const Unit &units() const override {return Units::Voltage::mV();}
            double rangeMaxVal() const override {return 75.;}
            double rangeMinVal() const override {return -25.;}
        } info;
        return info;
    }

    const LTR27MezzanineTypeInfo &LTR27MezzanineTypeInfo::R100() {
        static const class LTR27MezzanineTypeInfoR100 : public LTR27MezzanineTypeInfo {
            QString idName() const override {return QStringLiteral("R100");}
            QString displayName() const override {return QStringLiteral("H-27R-100");}
            int channelsCnt() const override {return 1;}
            const Unit &units() const override {return Units::Resistance::ohm();}
            double rangeMaxVal() const override {return 100.;}
            double rangeMinVal() const override {return 0.;}
        } info;
        return info;
    }

    const LTR27MezzanineTypeInfo &LTR27MezzanineTypeInfo::R250() {
        static const class LTR27MezzanineTypeInfoR250 : public LTR27MezzanineTypeInfo {
            QString idName() const override {return QStringLiteral("R250");}
            QString displayName() const override {return QStringLiteral("H-27R-250");}
            int channelsCnt() const override {return 1;}
            const Unit &units() const override {return Units::Resistance::ohm();}
            double rangeMaxVal() const override {return 250.;}
            double rangeMinVal() const override {return 0.;}
        } info;
        return info;
    }

    const QList<const LTR27MezzanineTypeInfo *> &LTR27MezzanineTypeInfo::allTypes() {
        static const QList<const LTR27MezzanineTypeInfo *> list {
            &U01(),
            &U10(),
            &U20(),
            &U100(),
            &I5(),
            &I10(),
            &I20(),
            &T(),
            &R100(),
            &R250(),
        };
        return list;
    }

}
