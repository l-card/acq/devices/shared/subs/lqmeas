#ifndef LQMEAS_LTR212CONFIG_H
#define LQMEAS_LTR212CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"
#include <memory>


namespace LQMeas {
    class LTRSyncMarksRecvConfig;
    class LTR212AdcConfig;

    class LTR212Config : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override { return typeConfigName();}

        const DevAdcConfig *adcConfig() const override;
        const DevSyncMarksRecvConfig *syncMarksRecvConfig() const override;

        LTR212AdcConfig &adc() {return  *m_adc;}
        const LTR212AdcConfig &adc() const {return  *m_adc;}

        LTRSyncMarksRecvConfig &marksRecv() {return  *m_syncRecv;}
        const LTRSyncMarksRecvConfig &marksRecv() const {return  *m_syncRecv;}

        DeviceConfig *clone() const override {
            return new LTR212Config{*this};
        }

        LTR212Config();
        LTR212Config(const LTR212Config &cfg);
        ~LTR212Config() override;
    protected:
        void protUpdate() override;
    private:
        const std::unique_ptr<LTR212AdcConfig> m_adc;
        const std::unique_ptr<LTRSyncMarksRecvConfig> m_syncRecv;
    };
}

#endif // LQMEAS_LTR212CONFIG_H
