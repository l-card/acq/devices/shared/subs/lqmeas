#include "LTR212TypeInfo.h"
#include "LTR212AdcTareCoefs.h"
#include "LTR212AdcTareOps.h"
#include "lqmeas/ifaces/in/adc/tare/DevAdcTarePoint.h"
#include "lqmeas/units/std/Voltage.h"

static const struct {
    double min;
    double max;
} f_ranges[] = {
    {-80, 80},
    {-40, 40},
    {-20, 20},
    {-10, 10},
    {0, 80},
    {0, 40},
    {0, 20},
    {0, 10}
};

namespace LQMeas {
    const QString &LTR212TypeInfo::name() {
        static const QString str {QStringLiteral("LTR212")};
        return str;
    }

    double LTR212TypeInfo::adcFreqMax() const {
        return 7680.;
    }

    int LTR212TypeInfo::adcChRangesCnt(int ch_num, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return sizeof(f_ranges)/sizeof(f_ranges[0]);
    }

    double LTR212TypeInfo::adcChRangeMaxVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return f_ranges[range].max;
    }

    double LTR212TypeInfo::adcChRangeMinVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
                return f_ranges[range].min;
    }

    const Unit &LTR212TypeInfo::adcChRangeUnit(int ch_num, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num);
        return Units::Voltage::mV();
    }

    const Unit &LTR212TypeInfo::adcChValUnit(int ch_num, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num);
        return Units::Voltage::V();
    }

    QList<const DevAdcTarePoint *> LTR212TypeInfo::adcTarePoints() const {
        static const QList<const DevAdcTarePoint *> list {
            &DevAdcTarePoints::zero(),
            &DevAdcTarePoints::scale()
        };
        return list;
    }

    QList<const DevAdcTareOp *> LTR212TypeInfo::adcTareOps(const DevAdcTarePoint &point) const {
        Q_UNUSED(point)
        static const QList<const DevAdcTareOp *> list {
            &LTR212AdcTareOps::externalTare(),
            &LTR212AdcTareOps::fabricCoefs(),
            &LTR212AdcTareOps::reset()
        };
        return list;
    }

    QList<const DevAdcTareCoefType *> LTR212TypeInfo::adcTareCoefTypes() const {
        static const QList<const DevAdcTareCoefType *> list {
            &LTR212AdcTareCoefs::dacValue(),
            &LTR212AdcTareCoefs::offsetCoef(),
            &LTR212AdcTareCoefs::scaleCoef()
        };
        return list;
    }

    const LTR212TypeInfo &LTR212TypeInfo::typeInfoOld() {
        static const class LTR212TypeInfoOld : public LTR212TypeInfo {
        public:
            QString deviceModificationName() const override {return QStringLiteral("LTR212");}
            bool supportQuarterBridge() const override {return false;}
            virtual bool supportCbrUref2_5() const override {return false;}
        } info;
        return info;
    }

    const LTR212TypeInfo &LTR212TypeInfo::typeInfoMod1()     {
        static const class LTR212TypeInfoMod1 : public LTR212TypeInfo {
        public:
            QString deviceModificationName() const override {return QStringLiteral("LTR212M-1");}
            bool supportQuarterBridge() const override {return true;}
            virtual bool supportCbrUref2_5() const override {return true;}
        } info;
        return info;
    }

    const LTR212TypeInfo &LTR212TypeInfo::typeInfoMod2() {
        static const class LTR212TypeInfoMod2 : public LTR212TypeInfo {
        public:
            QString deviceModificationName() const override {return QStringLiteral("LTR212M-2");}
            bool supportQuarterBridge() const override {return false;}
            virtual bool supportCbrUref2_5() const override {return true;}
        } info;
        return info;
    }
    
    const LTR212TypeInfo &LTR212TypeInfo::defaultTypeInfo() {
        return typeInfoMod1();
    }

    const QList<const LTR212TypeInfo *> &LTR212TypeInfo::types() {
        static const QList<const LTR212TypeInfo *> list {
            &typeInfoOld(),
            &typeInfoMod1(),
            &typeInfoMod2()
        };
        return list;
    }

    QList<const DeviceTypeInfo *> LTR212TypeInfo::modificationList() const {
        QList<const DeviceTypeInfo *> ret;
        const QList<const LTR212TypeInfo*> &ltr212_types {types()};
        for (const LTR212TypeInfo* type : ltr212_types) {
            ret.append(type);
        }
        return ret;
    }
}
