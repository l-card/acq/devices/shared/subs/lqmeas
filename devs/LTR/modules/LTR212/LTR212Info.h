#ifndef LQMEAS_LTR212INFO_H
#define LQMEAS_LTR212INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTR212TypeInfo.h"

namespace LQMeas {
    class LTR212Info : public DeviceInfo {
    public:
        LTR212Info(const LTR212TypeInfo &type = LTR212TypeInfo::defaultTypeInfo(),
                   const QString &serial = QString{},
                   const QString &bios_ver = QString{});


        DeviceInfo *clone() const override {return new LTR212Info{*this};}
        const QString &biosVerStr() const {return m_bios_ver;}


        bool supportQuarterBridge() const {return devspecTypeInfo().supportQuarterBridge();}
        bool supportCbrUref2_5() const {return devspecTypeInfo().supportCbrUref2_5();}

        const LTR212TypeInfo &devspecTypeInfo() const {
            return static_cast<const LTR212TypeInfo &>(type());
        }
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        LTR212Info(const LTR212Info &info);

        QString m_bios_ver;
    };
}


#endif // LTR212INFO_H
