#ifndef LQMEAS_LTR212ADCTARECOEFS_H
#define LQMEAS_LTR212ADCTARECOEFS_H


#include <QObject>
#include "lqmeas/ifaces/in/adc/tare/DevAdcTareCoefType.h"

namespace LQMeas {
    class LTR212AdcTareCoefs : public QObject {
        Q_OBJECT
    public:
        static const DevAdcTareCoefType &dacValue();
        static const DevAdcTareCoefType &offsetCoef();
        static const DevAdcTareCoefType &scaleCoef();
    };
}

#endif // LQMEAS_LTR212ADCTARECOEFS_H
