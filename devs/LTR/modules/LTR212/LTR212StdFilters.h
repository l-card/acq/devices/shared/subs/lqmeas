#ifndef LQMEAS_LTR212STDFILTERS_H
#define LQMEAS_LTR212STDFILTERS_H

#include "LTR212Filter.h"
#include <QList>

namespace LQMeas {
    class LTR212StdFilters {
    public:
        static const LTR212Filter &fir_d40_f25();
        static const LTR212Filter &fir_d24_f70();
        static const LTR212Filter &fir_d11_f258();
        static const LTR212Filter &fir_d7_f456();
        static const LTR212Filter &fir_d5_f675();

        static const LTR212Filter &iirDefault();

        static const QList<const LTR212Filter *> &firFilterList();
    };
}

#endif // LTR212STDFILTERS_H
