#ifndef LQMEAS_LTR212TYPEINFO_H
#define LQMEAS_LTR212TYPEINFO_H

#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"
#include "lqmeas/ifaces/in/adc/DevAdcInfo.h"
#include "lqmeas/ifaces/in/adc/tare/DevAdcTareInfo.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvInfo.h"


namespace LQMeas {
    class LTR212TypeInfo : public LTRModuleTypeInfo, public DevAdcInfo,
            public DevAdcTareInfo, public LTRSyncMarksRecvInfo {
    public:
        static const QString &name();
        static const LTR212TypeInfo &typeInfoOld();
        static const LTR212TypeInfo &typeInfoMod1();
        static const LTR212TypeInfo &typeInfoMod2();
        static const LTR212TypeInfo &defaultTypeInfo();

        static const QList<const LTR212TypeInfo *> &types();

        static const int adc_channels_cnt {8};

        const DevAdcInfo *adc() const override {
            return static_cast<const DevAdcInfo *>(this);
        }
        const DevAdcTareInfo *adcTare() const override {
            return static_cast<const DevAdcTareInfo *>(this);
        }
        const DevSyncMarksRecvInfo *syncMarkRecv() const override {
            return static_cast<const DevSyncMarksRecvInfo *>(this);
        }

        QString deviceTypeName() const override {return name();}


        AdcType adcType() const override {return AdcType::Parallel;}
        int adcChannelsCnt() const override {return adc_channels_cnt;}
        double adcFreqMax() const override;

        bool adcIsChFreqConfigurable() const override {return false;}

        int adcChRangesCnt(int ch_num, int unit_mode_num = 0) const override;
        double adcChRangeMaxVal(int ch_num, int range, int unit_mode_num = 0) const override;
        double adcChRangeMinVal(int ch_num, int range, int unit_mode_num = 0) const override;
        const Unit &adcChRangeUnit(int ch_num, int unit_mode_num = 0) const override;
        const Unit &adcChValUnit(int ch_num, int unit_mode_num = 0) const override;

        QList<const DevAdcTarePoint *> adcTarePoints() const override;
        QList<const DevAdcTareOp *> adcTareOps(const DevAdcTarePoint &point) const override;
        QList<const DevAdcTareCoefType *> adcTareCoefTypes() const override;

        virtual bool supportQuarterBridge() const  = 0;
        virtual bool supportCbrUref2_5() const = 0;

        QList<const DeviceTypeInfo *> modificationList() const override;
    private:
    };
}


#endif // LQMEAS_LTR212TYPEINFO_H
