#include "LTR212AdcTareCoefs.h"
#include "lqmeas/units/std/Voltage.h"
#include "lqmeas/units/std/Digital.h"
#include "lqmeas/units/std/Relative.h"

namespace LQMeas {
    const DevAdcTareCoefType &LTR212AdcTareCoefs::dacValue() {
        static class LTR212AdcTareCoefTypeDacValue : public DevAdcTareCoefType {
            QString id() const override {return QStringLiteral("dacValue");}
            QString displayName() const override {return tr("DAC Value");}
            const Unit &unit() const override {return Units::Voltage::mV();}
            bool hasValidFlag() const override {return  false;}
        } coef;
        return coef;
    }

    const DevAdcTareCoefType &LTR212AdcTareCoefs::offsetCoef() {
        static class LTR212AdcTareCoefTypeOffset : public DevAdcTareCoefType {
            QString id() const override {return QStringLiteral("offsetCoef");}
            QString displayName() const override {return tr("Offset Code");}
            const Unit &unit() const override {return Units::Digital::codes();}
            virtual bool hasFractPart() const override {return false;}
            bool hasValidFlag() const override {return  false;}
        } coef;
        return coef;
    }

    const DevAdcTareCoefType &LTR212AdcTareCoefs::scaleCoef() {
        static class LTR212AdcTareCoefTypeScale : public DevAdcTareCoefType {
            QString id() const override {return QStringLiteral("scaleCoef");}
            QString displayName() const override {return tr("Scale Ceof");}
            const Unit &unit() const override {return Units::Relative::ratio();}
            bool hasValidFlag() const override {return  false;}
        } coef;
        return coef;
    }
}
