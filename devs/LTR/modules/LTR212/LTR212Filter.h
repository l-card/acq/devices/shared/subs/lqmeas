#ifndef LQMEAS_LTR212FILTER_H
#define LQMEAS_LTR212FILTER_H

#include <QString>

namespace LQMeas {
    class LTR212Filter {
    public:
        const QString &id() const {return m_id;}

        int     decimation() const {return m_dec;}
        const unsigned short *coefs() const {return m_coefs;}
        int     coefsCnt() const {return m_coefsCnt;}

        double  fNotch() const {return m_f_notch;}
        double  fs() const {return m_fs;}
        bool    isIIR() const {return m_isIIR;}

        LTR212Filter(const QString &id, int decimation, const unsigned short *coefs, int coefs_cnt,
                     double fNotch, bool iir, double fs = 7680) :
            m_id{id}, m_dec{decimation}, m_coefs{coefs}, m_coefsCnt{coefs_cnt},
            m_f_notch{fNotch}, m_isIIR{iir}, m_fs{fs} {

        }
    private:
        QString m_id;
        int    m_dec;
        const unsigned short *m_coefs;
        int  m_coefsCnt;
        double m_f_notch;
        bool m_isIIR;
        double m_fs;

    };
}

#endif // LTR212FILTER_H
