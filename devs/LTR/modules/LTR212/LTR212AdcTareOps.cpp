#include "LTR212AdcTareOps.h"

namespace LQMeas {
    const DevAdcTareOp &LTR212AdcTareOps::externalTare() {
        static const class LTR212AdcTareOpExtermal : public DevAdcTareOp {
            QString id() const override {return QStringLiteral("external");}
            QString displayName() const override {return tr("External Tare");}
            bool externalSignalRequired() const override {return true;}
        } op;
        return op;
    }

    const DevAdcTareOp &LTR212AdcTareOps::fabricCoefs() {
        static const class LTR212AdcTareOpFabric : public DevAdcTareOp {
            QString id() const override {return QStringLiteral("fabric");}
            QString displayName() const override {return tr("Fabric Coefficients");}
            bool externalSignalRequired() const override {return false;}
        } op;
        return op;
    }

    const DevAdcTareOp &LTR212AdcTareOps::reset() {
        static const class LTR212AdcTareOpReset : public DevAdcTareOp {
            QString id() const override {return QStringLiteral("reset");}
            QString displayName() const override {return tr("Reset");}
            bool externalSignalRequired() const override {return false;}
        } op;
        return op;
    }
}
