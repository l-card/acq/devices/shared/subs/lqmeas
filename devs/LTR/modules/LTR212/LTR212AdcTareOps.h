#ifndef LQMEAS_LTR212ADCTAREOPS_H
#define LQMEAS_LTR212ADCTAREOPS_H

#include <QObject>
#include "lqmeas/ifaces/in/adc/tare/DevAdcTareOp.h"

namespace LQMeas {
    class LTR212AdcTareOps : public QObject {
        Q_OBJECT
    public:
        static const DevAdcTareOp &externalTare();
        static const DevAdcTareOp &fabricCoefs();
        static const DevAdcTareOp &reset();
    };
}

#endif // LQMEAS_LTR212ADCTAREOPS_H
