#include "LTR212Config.h"
#include "LTR212Info.h"
#include "LTR212AdcConfig.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvConfig.h"

namespace LQMeas {
    const QString &LTR212Config::typeConfigName() {
        return LTR212TypeInfo::name();
    }

    const DevAdcConfig *LTR212Config::adcConfig() const {
        return m_adc.get();
    }

    const DevSyncMarksRecvConfig *LTR212Config::syncMarksRecvConfig() const {
        return m_syncRecv.get();
    }

    LTR212Config::LTR212Config() :
        m_adc{new LTR212AdcConfig{LTR212TypeInfo::defaultTypeInfo()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this}} {

        init();
    }

    LTR212Config::LTR212Config(const LTR212Config &cfg) :
        m_adc{new LTR212AdcConfig{cfg.adc()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this, cfg.marksRecv()}} {

        init();
    }

    void LTR212Config::protUpdate() {
        m_syncRecv->updateSyncMarksRecvFreq(m_adc->adcChFreq());
    }

    LQMeas::LTR212Config::~LTR212Config() {

    }

}
