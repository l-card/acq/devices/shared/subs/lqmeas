#include "LTR212.h"
#include "LTR212Config.h"
#include "LTR212AdcConfig.h"
#include "LTR212Info.h"
#include "LTR212AdcTareOps.h"
#include "LTR212AdcTareCoefs.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "lqmeas/ifaces/in/adc/tare/DevAdcTarePoint.h"
#include "lqmeas/ifaces/in/adc/tare/DevAdcTareChCoefs.h"
#include "LTR212Filter.h"
#include "lqmeas/devs/LTR/modules/LTRModuleFrameReceiver.h"
#include "lqmeas/log/Log.h"
#include "lqmeas/StdErrors.h"

namespace LQMeas {
    static const DWORD default_scale_code  {0x593CEA};
    static const DWORD default_offset_code {0x800000};
    static const BYTE  default_dac_code    {0x20};

    LTR212::LTR212(LTRCrate *crate, int slot, QObject *parent) :
        LTRModule{crate, slot, typeModuleID, new LTR212Config{}, LTR212Info{}, parent},
        DevAdc{this, LTR212TypeInfo::adc_channels_cnt},
        DevAdcTare{this},
        m_receiver{new LTRModuleFrameReceiver{this}} {

        LTR212_Init(&m_hnd);
    }

    void LTR212::fillFilterParams(const LTR212Filter *flt, TLTR212_FILTER *flt_params) {
        flt_params->decimation = static_cast<BYTE>(flt->decimation() & 0xFF);
        flt_params->fs = flt->fs();
        flt_params->taps = static_cast<BYTE>(flt->coefsCnt() & 0xFF);
        for (int i = 0; (i < flt->coefsCnt()) && (i < LTR212_FIR_ORDER_MAX); ++i) {
            flt_params->koeff[i] = static_cast<SHORT>(flt->coefs()[i]);
        }
    }


    LTR212::~LTR212() {
        LTR212_Close(&m_hnd);
    }

    bool LTR212:: isOpened() const {
        return LTR212_IsOpened(&m_hnd) == LTR_OK;
    }

    DevSyncMarksRecv *LTR212::devSyncMarkRecv() {
        return m_receiver.get();
    }

    bool LTR212::adcIsRunning() const {
        return m_run;
    }

    void LTR212::adcTareExec(const DevAdcTareOp &op,
                             const DevAdcTarePoint &point,
                             quint32 ch_mask, quint32 &ch_ok_mask,
                             LQError &err)  {
        INT ltr_err_code {LTR_OK};
        ch_ok_mask = 0;
        if (op == LTR212AdcTareOps::reset()) {
            /* для сброса коэф. просто вычитываем текущие коээфициенты и
             * заменяем нужные на коэф. по умолчанию */
            for (int ch_num {0}; ch_num < LTR212_CHANNEL_CNT; ++ch_num) {
                if (ch_mask & (1 << ch_num)) {
                    if (point == DevAdcTarePoints::zero()) {
                        m_tareCoefs.DAC_Value[ch_num] = default_dac_code;
                        m_tareCoefs.Offset[ch_num]    = default_offset_code;
                    } else if (point == DevAdcTarePoints::scale()) {
                        m_tareCoefs.Scale[ch_num]     = default_scale_code;
                    }
                }
            }

            /* после замены записываем коэффициенты во flash и кодек */
            ltr_err_code = LTR212_WriteUSR_Clb(&m_hnd, &m_tareCoefs);
            if (ltr_err_code == LTR_OK) {
                ltr_err_code = LTR212_Load_Clb(&m_hnd);
            }
            if (ltr_err_code == LTR_OK)
                ch_ok_mask = ch_mask;
        } else {
            BYTE lch_mask = 0;
            BYTE lch_ok_mask = 0;
            /* перевод из маски физ. каналов в маску лог. каналов, используемую
             * в LTR212_Calibrate() */
            for (int ch_num {0}; ch_num < LTR212_CHANNEL_CNT; ++ch_num) {
                if (ch_mask & (1 << ch_num)) {
                    const int lch_num {adcChIdx(ch_num)};
                    if (lch_num >= 0) {
                        lch_mask |= (1 << lch_num);
                    }
                }
            }


            if (op == LTR212AdcTareOps::fabricCoefs()) {
                /* при внутренней калибровке сперва загружаем текущие коэф.
                 * в кодек для сохранения действительности некалебруемых
                 * сейчас парметрах, после чего выполняется LTR212_Calibrate
                 * для перезаписи коэффициентов */
                ltr_err_code = LTR212_Load_Clb(&m_hnd);
                if (ltr_err_code == LTR_OK) {
                    if (point == DevAdcTarePoints::zero()) {
                        ltr_err_code = LTR212_Calibrate(&m_hnd, &lch_mask, LTR212_CALIBR_MODE_INT_ZERO, FALSE);
                    } else if (point == DevAdcTarePoints::scale()) {
                        ltr_err_code = LTR212_Calibrate(&m_hnd, &lch_mask, LTR212_CALIBR_MODE_INT_SCALE, FALSE);
                    } else {
                        ltr_err_code = LTR_ERROR_PARAMETERS;
                    }
                }
            } else if (op == LTR212AdcTareOps::externalTare()) {
                if (point == DevAdcTarePoints::zero()) {
                    ltr_err_code = LTR212_Calibrate(&m_hnd, &lch_mask, LTR212_CALIBR_MODE_EXT_ZERO_SAVE_SCALE, TRUE);
                } else if (point == DevAdcTarePoints::scale()) {
                    /* для использования тарировки нуля загружаем коэф. с помощью
                     * LTR212_Load_Clb() */
                    ltr_err_code = LTR212_Load_Clb(&m_hnd);
                    if (ltr_err_code == LTR_OK) {
                        ltr_err_code = LTR212_Calibrate(&m_hnd, &lch_mask, LTR212_CALIBR_MODE_EXT_FULL_2ND_STAGE, FALSE);
                    }
                } else {
                    ltr_err_code = LTR_ERROR_PARAMETERS;
                }
            }

            if (ltr_err_code == LTR_OK) {
                lch_ok_mask = lch_mask;
            } else if (ltr_err_code == LTR212_ERR_CANT_CALIBRATE) {
                /* данный код говорит, что по части каналов не найден коэффициент.
                 * в этом случае возвращаем ОК, но меняем маску каналов */
                ltr_err_code = LTR_OK;
                lch_ok_mask = lch_mask;
            }

            /* обновляем коэф. в структуре на полученные после тарировки
             * во flash-памяти */
            if (ltr_err_code == LTR_OK) {
                ltr_err_code = LTR212_ReadUSR_Clb(&m_hnd, &m_tareCoefs);
            }

            /* обратный перевод успешно завершенных каналов из маски логич.
             * каналов в маску физических */
            if (ltr_err_code == LTR_OK) {
                for (int ch_idx {0}; ch_idx < devspecConfig().adc().adcEnabledChCnt(); ++ch_idx) {
                    if (lch_ok_mask & (1 << ch_idx)) {
                        int ch_num = adcChNum(ch_idx);
                        if (ch_num >= 0)
                            ch_ok_mask |= 1 << ch_num;
                    }
                }
            }
        }

        getError(ltr_err_code, err);
    }

    void LTR212::adcTareGetChCoefs(int ch, DevAdcTareChCoefs &coefs, LQError &err) {
        Q_UNUSED(err)

        coefs.clear();
        coefs.setCoefValue(LTR212AdcTareCoefs::dacValue(),
                           (static_cast<double>(m_tareCoefs.DAC_Value[ch]) - default_dac_code) *
                           (devspecConfig().adc().adcRefValue() == LTR212AdcConfig::RefValue::U_5V ? 2.5 : 1.25));
        coefs.setCoefValue(LTR212AdcTareCoefs::offsetCoef(),
                           static_cast<double>(m_tareCoefs.Offset[ch]) - default_offset_code);
        coefs.setCoefValue(LTR212AdcTareCoefs::scaleCoef(),
                              static_cast<double>(m_tareCoefs.Scale[ch])/default_scale_code);
    }

    QList<const DevAdcTareOp *> LTR212::adcTareAvailableOps(const DevAdcTarePoint &point) const {
        QList<const DevAdcTareOp *> ret;
        ret.append(&LTR212AdcTareOps::externalTare());
        if (point == DevAdcTarePoints::scale()) {
            if (devspecConfig().adc().adcFabricScaleCoefsAvailable()) {
                ret.append(&LTR212AdcTareOps::fabricCoefs());
            }
        } else {
            if (devspecConfig().adc().adcFabricCoefsAvailable()) {
                ret.append(&LTR212AdcTareOps::fabricCoefs());
            }
        }
        ret.append(&LTR212AdcTareOps::reset());
        return ret;
    }

    const LTR212Config &LTR212::devspecConfig() const {
        return static_cast<const LTR212Config&>(config());
    }

    QSharedPointer<const LTR212Info> LTR212::devspecInfo() const {
        return devInfo().staticCast<const LTR212Info>();
    }

    TLTR *LTR212::channel() const {
        return &m_hnd.Channel;
    }

    int LTR212::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags)     {
        Q_UNUSED(flags)
        int ltr_err = LTR212_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, slot, nullptr);
        if (ltr_err == LTR_OK) {
            ltr_err = LTR212_TestEEPROM(&m_hnd);
        }

        if (ltr_err == LTR_OK) {
            ltr_err = LTR212_ReadUSR_Clb(&m_hnd, &m_tareCoefs);
        }

        if (ltr_err == LTR_OK) {
            const LTR212TypeInfo *typeInfo = &LTR212TypeInfo::defaultTypeInfo();
            switch (m_hnd.ModuleInfo.Type) {
            case LTR212_OLD: typeInfo = &LTR212TypeInfo::typeInfoOld(); break;
            case LTR212_M_1: typeInfo = &LTR212TypeInfo::typeInfoMod1(); break;
            case LTR212_M_2: typeInfo = &LTR212TypeInfo::typeInfoMod2(); break;
            }

            setDeviceInfo(LTR212Info(*typeInfo,  QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                     QSTRING_FROM_CSTR(m_hnd.ModuleInfo.BiosVersion)));
        }
        return ltr_err;
    }

    void LTR212::protClose(LQError &err) {
        getError(LTR212_Close(&m_hnd), err);
    }


    void LTR212::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTR212Config *setCfg {qobject_cast<const LTR212Config *>(&cfg)};
        if (setCfg) {
            TLTR212_FILTER iirFlt;
            TLTR212_FILTER firFlt;

            switch (setCfg->adc().adcAcqMode()) {
                case LTR212AdcConfig::AcqMode::MediumRes4Ch: m_hnd.AcqMode = LTR212_FOUR_CHANNELS_WITH_MEDIUM_RESOLUTION; break;
                case LTR212AdcConfig::AcqMode::HighRes4Ch: m_hnd.AcqMode = LTR212_FOUR_CHANNELS_WITH_HIGH_RESOLUTION; break;
                case LTR212AdcConfig::AcqMode::HighRes8Ch: m_hnd.AcqMode = LTR212_EIGHT_CHANNELS_WITH_HIGH_RESOLUTION; break;
            }

            switch (setCfg->adc().adcRefValue()) {
                case LTR212AdcConfig::RefValue::U_5V: m_hnd.REF = LTR212_REF_5V; break;
                case LTR212AdcConfig::RefValue::U_2_5V: m_hnd.REF = LTR212_REF_2_5V; break;
            }

            switch (setCfg->adc().adcCbrMode()) {
                case LTR212AdcConfig::CbrMode::None: m_hnd.UseClb = FALSE; m_hnd.UseFabricClb = FALSE; break;
                case LTR212AdcConfig::CbrMode::Fabric: m_hnd.UseClb = FALSE; m_hnd.UseFabricClb = TRUE; break;
                case LTR212AdcConfig::CbrMode::UserTare: m_hnd.UseClb = TRUE; m_hnd.UseFabricClb = FALSE; break;
            }

            m_hnd.AC = setCfg->adc().adcAlternatingRef() ? 1 : 0;
            m_hnd.LChQnt = 0;
            for (int ch_num = 0; ch_num < LTR212TypeInfo::adc_channels_cnt; ++ch_num) {
                if (setCfg->adc().adcChEnabled(ch_num)) {
                    LTR212_SCALE scale_code;
                    LTR212_BRIDGE_TYPES bridge_code;
                    switch (setCfg->adc().adcChRange(ch_num)) {
                        case LTR212AdcConfig::AdcChRange::Bipolar_80: scale_code = LTR212_SCALE_B_80; break;
                        case LTR212AdcConfig::AdcChRange::Bipolar_40: scale_code = LTR212_SCALE_B_40; break;
                        case LTR212AdcConfig::AdcChRange::Bipolar_20: scale_code = LTR212_SCALE_B_20; break;
                        case LTR212AdcConfig::AdcChRange::Bipolar_10: scale_code = LTR212_SCALE_B_10; break;
                        case LTR212AdcConfig::AdcChRange::Unipolar_80: scale_code = LTR212_SCALE_U_80; break;
                        case LTR212AdcConfig::AdcChRange::Unipolar_40: scale_code = LTR212_SCALE_U_40; break;
                        case LTR212AdcConfig::AdcChRange::Unipolar_20: scale_code = LTR212_SCALE_U_20; break;
                        case LTR212AdcConfig::AdcChRange::Unipolar_10: scale_code = LTR212_SCALE_U_10; break;
                    }

                    switch (setCfg->adc().adcChBridgeType(ch_num)) {
                        case LTR212AdcConfig::BridgeType::FullOrHalf: bridge_code = LTR212_FULL_OR_HALF_BRIDGE; break;
                        case LTR212AdcConfig::BridgeType::Quarter:
                            switch (setCfg->adc().adcQuarterBridgeResistor()) {
                                case LTR212AdcConfig::QuarterBridgeResistor::R_200:
                                    bridge_code = LTR212_QUARTER_BRIDGE_WITH_200_Ohm;
                                    break;
                                case LTR212AdcConfig::QuarterBridgeResistor::R_350:
                                    bridge_code = LTR212_QUARTER_BRIDGE_WITH_350_Ohm;
                                    break;
                                case LTR212AdcConfig::QuarterBridgeResistor::R_User:
                                    bridge_code = LTR212_QUARTER_BRIDGE_WITH_CUSTOM_Ohm;
                                    break;
                            }
                            break;
                        case LTR212AdcConfig::BridgeType::QuarterUnbalanced:
                            switch (setCfg->adc().adcQuarterBridgeResistor()) {
                                case LTR212AdcConfig::QuarterBridgeResistor::R_200:
                                    bridge_code = LTR212_UNBALANCED_QUARTER_BRIDGE_WITH_200_Ohm;
                                    break;
                                case LTR212AdcConfig::QuarterBridgeResistor::R_350:
                                    bridge_code = LTR212_UNBALANCED_QUARTER_BRIDGE_WITH_350_Ohm;
                                    break;
                                case LTR212AdcConfig::QuarterBridgeResistor::R_User:
                                    bridge_code = LTR212_UNBALANCED_QUARTER_BRIDGE_WITH_CUSTOM_Ohm;
                                    break;
                            }
                            break;
                    }


                    m_hnd.LChTbl[m_hnd.LChQnt] = LTR212_CreateLChannel2(
                                static_cast<DWORD>(ch_num + 1), scale_code, bridge_code);
                    ++m_hnd.LChQnt;
                }

                m_hnd.filter.FIR = 0;
                m_hnd.filter.IIR = 0;
                if (setCfg->adc().adcDspFiltersAvailable()) {
                    if (setCfg->adc().adcDspFIR()) {
                        m_hnd.filter.FIR = 1;
                        fillFilterParams(setCfg->adc().adcDspFIR(), &firFlt);
                    }
                    if (setCfg->adc().adcDspIIR()) {
                        m_hnd.filter.IIR = 1;
                        fillFilterParams(setCfg->adc().adcDspIIR(), &iirFlt);
                    }
                }
            }
            getError(LTR212_SetADCExpFlt(&m_hnd, &firFlt, &iirFlt), err);
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }

    INT LTR212::ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) {
        return LTR212_Recv(&m_hnd, wrds, marks, size, tout);
    }

    void LTR212::protAdcStart(LQError &err) {
        getError(LTR212_Start(&m_hnd), err);
        if (err.isSuccess()) {
            const int frame_size {adcConfig().adcEnabledChCnt() * 2};
            m_run = true;            
            m_receiver->start(frame_size, frame_size);
        }
    }

    void LTR212::protAdcStop(LQError &err) {
        getError(LTR212_Stop(&m_hnd), err);
        m_run = false;
        m_receiver->clear();
    }

    void LTR212::protAdcGetData(double *data, int size, unsigned flags, unsigned tout,
                                int &recvd_size, LQError &err) {
        int wrds_cnt {2*size};
        int recvd_words {0};
        DWORD *wrds;
        DWORD proc_size {0};

        m_receiver->getFrames(wrds_cnt, tout, wrds, recvd_words, err);

        if (err.isSuccess() && (recvd_words!=0)) {
            proc_size = static_cast<DWORD>(recvd_words);

            getError(LTR212_ProcessData(&m_hnd, wrds, data, &proc_size, TRUE), err);
            if (!err.isSuccess()) {
                LQMeasLog->error(tr("Process data error"), err, this);
            }
        }

        if (err.isSuccess())
            recvd_size = static_cast<int>(proc_size);
    }

    QString LTR212::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTR212_GetErrorString(err));
    }
}
