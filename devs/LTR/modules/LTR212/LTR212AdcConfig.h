#ifndef LQMEAS_LTR212ADCCONFIG_H
#define LQMEAS_LTR212ADCCONFIG_H

#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "LTR212TypeInfo.h"

namespace LQMeas {
    class LTR212Filter;

    class LTR212AdcConfig : public DevAdcConfig {
        Q_OBJECT
    public:
        enum class BridgeType {
            FullOrHalf,
            Quarter,
            QuarterUnbalanced
        };

        enum class QuarterBridgeResistor {
            R_200,
            R_350,
            R_User
        };

        enum class AdcChRange {
            Bipolar_80,
            Bipolar_40,
            Bipolar_20,
            Bipolar_10,
            Unipolar_80,
            Unipolar_40,
            Unipolar_20,
            Unipolar_10,
        };

        enum class AcqMode {
            MediumRes4Ch,
            HighRes4Ch,
            HighRes8Ch
        };

        enum class RefValue {
            U_5V,
            U_2_5V
        };

        enum class CbrMode {
            None,
            Fabric,
            UserTare
        };





        double adcFreq() const override {return m_results.f_adc;}
        bool adcChEnabled(int ch) const override {return m_params.Ch[ch].enabled;}
        int adcChRangeNum(int ch) const override { return m_params.Ch[ch].range_num; }
        AdcChRange adcChRange(int ch) const {return static_cast<AdcChRange>(adcChRangeNum(ch));}

        BridgeType adcChBridgeType(int ch) const {return m_params.Ch[ch].bridge_type;}
        bool adcChAvailable(int ch_num) const override;

        AcqMode adcAcqMode() const {return m_params.acq_mode;}
        RefValue  adcRefValue() const {return m_params.ref_val;}
        CbrMode adcCbrMode() const {return m_params.cbr_mode;}
        QuarterBridgeResistor adcQuarterBridgeResistor() const {return m_params.qb_resistor;}
        bool adcAlternatingRef() const {return m_params.alt_ref;}
        const LTR212Filter *adcDspFIR() const {return m_params.fir;}
        const LTR212Filter *adcDspIIR() const {return m_params.iir;}

        int adcMaxChCnt() const;
        bool adcAlternatingRefAvailable() const;
        bool adcFabricCoefsAvailable() const;
        bool adcFabricScaleCoefsAvailable() const;
        bool adcDspFiltersAvailable() const;
        /* имеет ли смысл настройка adcQuarterBridgeResistor() в текущей конфигурации (должен хотя бы один канал быть четвертьмостом) */
        bool adcQuarterBridgeResistorConfigApplicable() const;

        int adcSampleSize() const override {return 8;}

        static int typeAdcRangeNum(AdcChRange range) {return static_cast<int>(range);}
    public Q_SLOTS:
        void adcSetChEnabled(int ch, bool en);
        void adcSetChRangeNum(int ch, int range_num);
        void adcSetChRange(int ch, LQMeas::LTR212AdcConfig::AdcChRange range) {
            adcSetChRangeNum(ch, typeAdcRangeNum(range));
        }
        void adcSetChBridgeType(int ch, LQMeas::LTR212AdcConfig::BridgeType type);

        void adcSetAcqMode(LQMeas::LTR212AdcConfig::AcqMode mode);
        void adcSetRefValue(LQMeas::LTR212AdcConfig::RefValue ref_val);
        void adcSetCbrMode(LQMeas::LTR212AdcConfig::CbrMode cbr_mode);
        void adcSetQuarterBridgeResistor(LQMeas::LTR212AdcConfig::QuarterBridgeResistor res_val);
        void adcSetAlternatingRef(bool en);

        void adcSetDspFIR(const QString &id);
        void adcSetDspIIR(const QString &id);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
        void protUpdate() override;
    private:
        LTR212AdcConfig(const DevAdcInfo &info);
        LTR212AdcConfig(const LTR212AdcConfig &cfg);

        void calcResultFreq();

        struct {
            AcqMode acq_mode;
            RefValue  ref_val;
            CbrMode cbr_mode;
            QuarterBridgeResistor qb_resistor;
            bool alt_ref;
            const LTR212Filter *fir;
            const LTR212Filter *iir;

            struct {
                bool        enabled;
                int         range_num;
                BridgeType  bridge_type;
            } Ch[LTR212TypeInfo::adc_channels_cnt];
        } m_params;

        struct {
            double f_adc;
        } m_results;

        friend class LTR212Config;
    };
}

Q_DECLARE_METATYPE(LQMeas::LTR212AdcConfig::AcqMode)
Q_DECLARE_METATYPE(LQMeas::LTR212AdcConfig::RefValue)
Q_DECLARE_METATYPE(LQMeas::LTR212AdcConfig::CbrMode)
Q_DECLARE_METATYPE(LQMeas::LTR212AdcConfig::QuarterBridgeResistor)


#endif // LQMEAS_LTR212ADCCONFIG_H
