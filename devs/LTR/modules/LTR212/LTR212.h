#ifndef LQMEAS_LTR212_H
#define LQMEAS_LTR212_H

#include "ltr/include/ltr212api.h"
#include "lqmeas/devs/LTR/modules/LTRModule.h"
#include "lqmeas/ifaces/in/adc/DevAdc.h"
#include "lqmeas/ifaces/in/adc/tare/DevAdcTare.h"
#include <memory>

namespace LQMeas {
    class LTR212Config;
    class LTR212Info;
    class LTR212Filter;
    class LTRModuleFrameReceiver;

    class LTR212 : public LTRModule, public DevAdc, public DevAdcTare {
        Q_OBJECT
    public:
        static const quint16 typeModuleID {LTR_MID_LTR212};

        ~LTR212() override;
        bool isOpened() const override;

        /* ----------------------- интерфейс ввода  --------------------------*/
        DevAdc *devAdc() override {return static_cast<DevAdc*>(this);}
        DevAdcTare *devAdcTare() override {return static_cast<DevAdcTare *>(this);}
        DevSyncMarksRecv *devSyncMarkRecv() override;

        QString errorString(int err) const override;

        bool adcIsRunning() const override;

        void adcTareExec(const DevAdcTareOp &op,
                         const DevAdcTarePoint &point,
                         quint32 ch_mask,
                         quint32 &ch_ok_mask,
                         LQError &err) override;
        void adcTareGetChCoefs(int ch, DevAdcTareChCoefs &coefs, LQError &err) override;
        QList<const DevAdcTareOp *> adcTareAvailableOps(const DevAdcTarePoint &point) const override;

        const LTR212Config &devspecConfig() const;
        QSharedPointer<const LTR212Info> devspecInfo() const;
    protected:
        TLTR* channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn,  int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;

        INT ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) override;

        void protAdcStart(LQError &err) override;
        void protAdcStop(LQError &err) override;
        void protAdcGetData(double *data, int size, unsigned flags,
                            unsigned tout, int &recvd_size, LQError &err) override;

    private:
        explicit LTR212(LTRCrate *crate, int slot, QObject *parent = nullptr);
        void fillFilterParams(const LTR212Filter *flt, TLTR212_FILTER *flt_params);

        mutable TLTR212 m_hnd;
        TLTR212_USER_TARE_COEFS m_tareCoefs;
        const std::unique_ptr<LTRModuleFrameReceiver> m_receiver;
        bool m_run {false};

        friend class LTRResolver;
    };
}

#endif // LQMEAS_LTR212_H
