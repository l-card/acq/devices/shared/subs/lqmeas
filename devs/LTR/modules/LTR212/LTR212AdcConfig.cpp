#include "LTR212AdcConfig.h"
#include "LTR212Config.h"
#include "LTR212Info.h"
#include "LTR212.h"
#include "LTR212StdFilters.h"
#include "ltr/include/ltr212api.h"
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>
#include <QJsonArray>


namespace LQMeas {
    static const QLatin1String &cfgkey_ch_enabled       {StdConfigKeys::enabled()};
    static const QLatin1String &cfgkey_ch_range         {StdConfigKeys::range()};
    static const QLatin1String cfgkey_ch_bridge_type    {"BridgeType"};
    static const QLatin1String &cfgkey_channels_arr     {StdConfigKeys::channels()};
    static const QLatin1String cfgkey_acq_mode          {"AcqMode"};
    static const QLatin1String cfgkey_uref_val          {"RefVal"};
    static const QLatin1String cfgkey_cbr_mode          {"CbrVal"};
    static const QLatin1String cfgkey_qb_resistor       {"QbResistor"};
    static const QLatin1String cfgkey_alternate_ref     {"AltRef"};
    static const QLatin1String &cfgkey_iir_enabled      {StdConfigKeys::enabled()};
    static const QLatin1String cfgkey_iir_id            {"ID"};
    static const QLatin1String cfgkey_iir_obj           {"IIR"};
    static const QLatin1String &cfgkey_fir_enabled      {StdConfigKeys::enabled()};
    static const QLatin1String cfgkey_fir_id            {cfgkey_iir_id};
    static const QLatin1String cfgkey_fir_obj           {"FIR"};


    typedef EnumConfigKey<LTR212AdcConfig::AcqMode> LTR212AcqModeKey;
    static const LTR212AcqModeKey adc_acq_modes[] = {
        {QStringLiteral("med4ch"),              LTR212AdcConfig::AcqMode::MediumRes4Ch},
        {QStringLiteral("high4ch"),             LTR212AdcConfig::AcqMode::HighRes4Ch},
        {QStringLiteral("high8ch"),             LTR212AdcConfig::AcqMode::HighRes8Ch},
        {LTR212AcqModeKey::defaultKey(),        LTR212AdcConfig::AcqMode::HighRes4Ch},
    };

    typedef EnumConfigKey<LTR212AdcConfig::BridgeType> LTR212ChBridgeTypeKey;
    static const LTR212ChBridgeTypeKey adc_ch_birdgetypes[] = {
        {QStringLiteral("fh"),                  LTR212AdcConfig::BridgeType::FullOrHalf},
        {QStringLiteral("q"),                   LTR212AdcConfig::BridgeType::Quarter},
        {QStringLiteral("qu"),                  LTR212AdcConfig::BridgeType::QuarterUnbalanced},
        {LTR212ChBridgeTypeKey::defaultKey(),   LTR212AdcConfig::BridgeType::FullOrHalf},
    };

    typedef EnumConfigKey<LTR212AdcConfig::QuarterBridgeResistor> LTR212QuarterBridgeResistorKey;
    static const LTR212QuarterBridgeResistorKey adc_qbridgeresistors[] = {
        {QStringLiteral("200"),                 LTR212AdcConfig::QuarterBridgeResistor::R_200},
        {QStringLiteral("350"),                 LTR212AdcConfig::QuarterBridgeResistor::R_350},
        {QStringLiteral("custom"),              LTR212AdcConfig::QuarterBridgeResistor::R_User},
        {LTR212QuarterBridgeResistorKey::defaultKey(),  LTR212AdcConfig::QuarterBridgeResistor::R_200},
    };

    typedef EnumConfigKey<LTR212AdcConfig::AdcChRange> LTR212ChRangeKey;
    static const LTR212ChRangeKey adc_ch_ranges[] = {
        {QStringLiteral("b80"),                 LTR212AdcConfig::AdcChRange::Bipolar_80},
        {QStringLiteral("b40"),                 LTR212AdcConfig::AdcChRange::Bipolar_40},
        {QStringLiteral("b20"),                 LTR212AdcConfig::AdcChRange::Bipolar_20},
        {QStringLiteral("b10"),                 LTR212AdcConfig::AdcChRange::Bipolar_10},
        {QStringLiteral("u80"),                 LTR212AdcConfig::AdcChRange::Unipolar_80},
        {QStringLiteral("u40"),                 LTR212AdcConfig::AdcChRange::Unipolar_40},
        {QStringLiteral("u20"),                 LTR212AdcConfig::AdcChRange::Unipolar_20},
        {QStringLiteral("u10"),                 LTR212AdcConfig::AdcChRange::Unipolar_10},
        {LTR212ChRangeKey::defaultKey(),        LTR212AdcConfig::AdcChRange::Bipolar_80},
    };

    typedef EnumConfigKey<LTR212AdcConfig::RefValue> LTR212RefValKey;
    static const LTR212RefValKey adc_ref_vals[] = {
        {QStringLiteral("5"),                   LTR212AdcConfig::RefValue::U_5V},
        {QStringLiteral("2.5"),                 LTR212AdcConfig::RefValue::U_2_5V},
        {LTR212RefValKey::defaultKey(),         LTR212AdcConfig::RefValue::U_5V},
    };

    typedef EnumConfigKey<LTR212AdcConfig::CbrMode> LTR212CbrModeKey;
    static const LTR212CbrModeKey adc_cbr_modes[] = {
        {QStringLiteral("none"),                LTR212AdcConfig::CbrMode::None},
        {QStringLiteral("fabric"),              LTR212AdcConfig::CbrMode::Fabric},
        {QStringLiteral("tare"),                LTR212AdcConfig::CbrMode::UserTare},
        {LTR212CbrModeKey::defaultKey(),        LTR212AdcConfig::CbrMode::UserTare}
    };

    bool LTR212AdcConfig::adcChAvailable(int ch_num) const {
        return ch_num < adcMaxChCnt();
    }

    void LTR212AdcConfig::adcSetDspFIR(const QString &id) {
        const QList<const LTR212Filter *> &fltList {LTR212StdFilters::firFilterList()};
        const auto &it {std::find_if(fltList.constBegin(), fltList.constEnd(),
                                     [&id](auto flt){return flt->id() == id;})};
        const LTR212Filter *fndFlt {it == fltList.constEnd() ? nullptr : *it};

        if (m_params.fir != fndFlt) {
            m_params.fir = fndFlt;
            notifyConfigChanged();
        }
    }

    void LTR212AdcConfig::adcSetDspIIR(const QString &id) {
        const LTR212Filter *fndFlt {id == LTR212StdFilters::iirDefault().id() ?
                        &LTR212StdFilters::iirDefault() : nullptr};

        if (fndFlt != m_params.iir) {
            m_params.iir = fndFlt;
            notifyConfigChanged();
        }
    }

    int LTR212AdcConfig::adcMaxChCnt() const {
        return adcAcqMode() == AcqMode::HighRes8Ch ? 8 : 4;
    }

    bool LTR212AdcConfig::adcAlternatingRefAvailable() const {
        return adcAcqMode() != AcqMode::MediumRes4Ch;
    }

    bool LTR212AdcConfig::adcFabricCoefsAvailable() const {
        return (adcAcqMode() != AcqMode::MediumRes4Ch) &&
                adcFabricScaleCoefsAvailable();
    }

    bool LTR212AdcConfig::adcFabricScaleCoefsAvailable() const {
        return (static_cast<const LTR212TypeInfo&>(adcInfo()).supportCbrUref2_5() ||
                (adcRefValue() != RefValue::U_2_5V));
    }

    bool LTR212AdcConfig::adcDspFiltersAvailable() const {
        return adcAcqMode() == AcqMode::MediumRes4Ch;
    }

    bool LTR212AdcConfig::adcQuarterBridgeResistorConfigApplicable() const {
        bool has_qbridge_cfg {false};

        for (int ch_num {0}; ch_num < LTR212TypeInfo::adc_channels_cnt; ++ch_num) {
            if (adcChEnabled(ch_num) && (adcChBridgeType(ch_num) != BridgeType::FullOrHalf)) {
                has_qbridge_cfg = true;
            }
        }

        return has_qbridge_cfg;
    }

    void LTR212AdcConfig::adcSetChEnabled(int ch, bool en) {
        if (m_params.Ch[ch].enabled != en) {
            m_params.Ch[ch].enabled = en;
            notifyChEnableStateChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR212AdcConfig::adcSetChRangeNum(int ch, int range_num) {
        if (m_params.Ch[ch].range_num != range_num) {
            m_params.Ch[ch].range_num = range_num;
            notifyChRangeChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR212AdcConfig::adcSetChBridgeType(int ch, LTR212AdcConfig::BridgeType type) {
        if (m_params.Ch[ch].bridge_type != type) {
            m_params.Ch[ch].bridge_type = type;
            notifyConfigChanged();
        }
    }

    void LTR212AdcConfig::adcSetAcqMode(LTR212AdcConfig::AcqMode mode) {
        if (m_params.acq_mode != mode) {
            m_params.acq_mode = mode;
            notifyConfigChanged();
        }
    }

    void LTR212AdcConfig::adcSetRefValue(RefValue ref_val) {
        if (m_params.ref_val != ref_val) {
            m_params.ref_val = ref_val;
            notifyConfigChanged();
        }
    }

    void LTR212AdcConfig::adcSetCbrMode(LTR212AdcConfig::CbrMode cbr_mode) {
        if (m_params.cbr_mode != cbr_mode) {
            m_params.cbr_mode = cbr_mode;
            notifyConfigChanged();
        }
    }

    void LTR212AdcConfig::adcSetQuarterBridgeResistor(LTR212AdcConfig::QuarterBridgeResistor res_val) {
        if (m_params.qb_resistor != res_val) {
            m_params.qb_resistor = res_val;
            notifyConfigChanged();
        }
    }

    void LTR212AdcConfig::adcSetAlternatingRef(bool en) {
        if (m_params.alt_ref != en) {
            m_params.alt_ref = en;
            notifyConfigChanged();
        }
    }


    void LTR212AdcConfig::protSave(QJsonObject &cfgObj) const     {
        QJsonArray chArray;
        for (int ch {0}; ch < LTR212TypeInfo::adc_channels_cnt; ++ch) {
            QJsonObject chObj;
            chObj[cfgkey_ch_enabled]     = m_params.Ch[ch].enabled;
            chObj[cfgkey_ch_range]       = LTR212ChRangeKey::getKey(adc_ch_ranges, adcChRange(ch));
            chObj[cfgkey_ch_bridge_type] = LTR212ChBridgeTypeKey::getKey(adc_ch_birdgetypes, m_params.Ch[ch].bridge_type);
            chArray.append(chObj);
        }
        cfgObj[cfgkey_channels_arr] = chArray;

        cfgObj[cfgkey_acq_mode] = LTR212AcqModeKey::getKey(adc_acq_modes, m_params.acq_mode);
        cfgObj[cfgkey_uref_val] = LTR212RefValKey::getKey(adc_ref_vals, m_params.ref_val);
        cfgObj[cfgkey_cbr_mode] = LTR212CbrModeKey::getKey(adc_cbr_modes, m_params.cbr_mode);
        cfgObj[cfgkey_qb_resistor]  = LTR212QuarterBridgeResistorKey::getKey(adc_qbridgeresistors, m_params.qb_resistor);
        cfgObj[cfgkey_alternate_ref] = m_params.alt_ref;

        QJsonObject iirObj;
        iirObj[cfgkey_iir_enabled] = m_params.iir != nullptr;
        if (m_params.iir)
            iirObj[cfgkey_iir_id] = m_params.iir->id();
        cfgObj[cfgkey_iir_obj] = iirObj;


        QJsonObject firObj;
        firObj[cfgkey_fir_enabled] = m_params.fir != nullptr;
        if (m_params.fir)
            firObj[cfgkey_fir_id] = m_params.fir->id();
        cfgObj[cfgkey_fir_obj] = firObj;
    }

    void LTR212AdcConfig::protLoad(const QJsonObject &cfgObj)     {
        const QJsonArray &chArray {cfgObj[cfgkey_channels_arr].toArray()};
        const int ch_cnt {chArray.size()};
        for (int ch {0}; (ch < ch_cnt) && (ch < LTR212TypeInfo::adc_channels_cnt); ++ch) {
            const QJsonObject &chObj {chArray.at(ch).toObject()};

            m_params.Ch[ch].enabled = chObj[cfgkey_ch_enabled].toBool(ch == 0);
            m_params.Ch[ch].range_num = typeAdcRangeNum(
                        LTR212ChRangeKey::getValue(adc_ch_ranges, chObj[cfgkey_ch_range].toString()));
            m_params.Ch[ch].bridge_type = LTR212ChBridgeTypeKey::getValue(
                        adc_ch_birdgetypes, chObj[cfgkey_ch_bridge_type].toString());
        }

        for (int ch {ch_cnt}; ch < LTR212TypeInfo::adc_channels_cnt; ++ch) {
            m_params.Ch[ch].enabled = false;
            m_params.Ch[ch].range_num = typeAdcRangeNum(AdcChRange::Bipolar_80);
            m_params.Ch[ch].bridge_type = BridgeType::FullOrHalf;
        }

        m_params.acq_mode = LTR212AcqModeKey::getValue(adc_acq_modes, cfgObj[cfgkey_acq_mode].toString());
        m_params.ref_val  = LTR212RefValKey::getValue(adc_ref_vals, cfgObj[cfgkey_uref_val].toString());
        m_params.cbr_mode = LTR212CbrModeKey::getValue(adc_cbr_modes, cfgObj[cfgkey_cbr_mode].toString());
        m_params.qb_resistor = LTR212QuarterBridgeResistorKey::getValue(adc_qbridgeresistors, cfgObj[cfgkey_qb_resistor].toString());
        m_params.alt_ref = cfgObj[cfgkey_alternate_ref].toBool();

        const QJsonObject &iirObj {cfgObj[cfgkey_iir_obj].toObject()};
        if (iirObj[cfgkey_iir_enabled].toBool()) {
            adcSetDspIIR(iirObj[cfgkey_iir_id].toString());
        } else {
            m_params.iir = nullptr;
        }


        const QJsonObject &firObj {cfgObj[cfgkey_fir_obj].toObject()};
        if (firObj[cfgkey_fir_enabled].toBool()) {
            adcSetDspFIR(firObj[cfgkey_fir_enabled].toString());
        } else {
            m_params.fir = nullptr;
        }
    }

    void LTR212AdcConfig::protUpdate() {        
        DevAdcConfig::protUpdate();
        calcResultFreq();

    }

    void LTR212AdcConfig::calcResultFreq()  {
        const double fs_for_modes[3] {7680.0, 150.15, 3.4 };
        const int decimation {(adcDspFiltersAvailable() && m_params.fir) ? m_params.fir->decimation() : 1};
        m_results.f_adc = fs_for_modes[static_cast<int>(m_params.acq_mode)]/decimation;
    }

    LTR212AdcConfig::LTR212AdcConfig(const DevAdcInfo &info) : DevAdcConfig(info) {
        memset(&m_params, 0, sizeof(m_params));
        calcResultFreq();
    }

    LTR212AdcConfig::LTR212AdcConfig(const LTR212AdcConfig &cfg) : DevAdcConfig(cfg) {
        memcpy(&m_params, &cfg.m_params, sizeof (m_params));
        memcpy(&m_results, &cfg.m_results, sizeof (m_results));
    }

}
