#include "LTR212Info.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_bios_ver {"BiosVerStr"};

    LTR212Info::LTR212Info(const LTR212TypeInfo &type, const QString &serial, const QString &bios_ver) :
        DeviceInfo{type, serial}, m_bios_ver{bios_ver} {

    }

    LTR212Info::LTR212Info(const LTR212Info &info) : DeviceInfo{info}, m_bios_ver{info.m_bios_ver} {

    }

    void LTR212Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_bios_ver] = m_bios_ver;
    }

    void LTR212Info::protLoad(const QJsonObject &infoObj) {
        m_bios_ver = infoObj[cfgkey_bios_ver].toString();
    }


}
