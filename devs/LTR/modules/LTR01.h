#ifndef LQMEAS_LTR01_H
#define LQMEAS_LTR01_H

#include "ltr/include/ltr01api.h"
#include "lqmeas/devs/LTR/modules/LTRModule.h"

namespace LQMeas {
    class LTR01 : public LTRModule {
    public:
        static const quint16 typeModuleID {LTR_MID_LTR01};

        e_LTR01_SUBID moduleSubID() const {return m_subid;}
    protected:
        explicit LTR01(LTRCrate *crate, int slot,
                       e_LTR01_SUBID subid, DeviceConfig *defaultCfg,
                       const DeviceInfo &info, QObject *parent = nullptr);
    private:
        e_LTR01_SUBID m_subid;
    };
}

#endif // LQMEAS_LTR01_H
