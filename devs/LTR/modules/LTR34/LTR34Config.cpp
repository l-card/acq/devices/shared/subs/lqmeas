#include "LTR34Config.h"
#include "LTR34TypeInfo.h"
#include "LTR34OutConfig.h"


namespace LQMeas {
    const QString &LQMeas::LTR34Config::typeConfigName() {
        return LTR34TypeInfo::name();
    }

    const DevOutConfig *LTR34Config::outConfig() const {
        return  m_out.get();
    }
    const DevOutSyncConfig *LTR34Config::outSyncConfig() const {
        return  m_out.get();
    }
    
    LTR34Config::LTR34Config() : m_out{new LTR34OutConfig{LTR34TypeInfo::defaultTypeInfo()}} {
        init();
    }

    LTR34Config::LTR34Config(const LTR34Config &cfg) :m_out{new LTR34OutConfig{cfg.out()}} {
        init();
    }

    LTR34Config::~LTR34Config() {
    }
}
