#include "LTR34Info.h"
#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String &cfgkey_fpga_ver             {StdConfigKeys::fpgaVer()};

    LTR34Info::LTR34Info(const LTR34TypeInfo &type, const QString &serial, const QString &fpga_ver_str) :
        DeviceInfo{type, serial}, m_fpga_ver{fpga_ver_str} {

    }

    LTR34Info::LTR34Info(const LTR34Info &info) : DeviceInfo{info},
        m_fpga_ver{info.m_fpga_ver} {

    }


    void LTR34Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_fpga_ver] = m_fpga_ver;
    }

    void LTR34Info::protLoad(const QJsonObject &infoObj) {
        m_fpga_ver = infoObj[cfgkey_fpga_ver].toString();
    }
}
