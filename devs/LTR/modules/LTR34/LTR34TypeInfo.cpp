#include "LTR34TypeInfo.h"

namespace LQMeas {
    static const double f_ranges[] = {10., 1.};

    const QString &LTR34TypeInfo::name() {
        static const QString str {QStringLiteral("LTR34")};
        return str;
    }

    QString LTR34TypeInfo::deviceModificationName() const {
        return QString("%1-%2").arg(deviceTypeName()).arg(outDacChannelsCnt());
    }
    
    const LTR34TypeInfo &LTR34TypeInfo::defaultTypeInfo() {
        return typeInfoMod8();
    }

    const LTR34TypeInfo &LTR34TypeInfo::typeInfoMod8() {
        static const LTR34TypeInfo info{8};
        return info;
    }

    const LTR34TypeInfo &LTR34TypeInfo::typeInfoMod4() {
        static const LTR34TypeInfo info{4};
        return info;
    }

    double LTR34TypeInfo::outSyncGenFreqMax() const {
        return 500000;
    }

    int LTR34TypeInfo::outDacChRangesCnt(int ch) const {
        Q_UNUSED(ch)
        return sizeof(f_ranges)/sizeof(f_ranges[0]);
    }

    double LTR34TypeInfo::outDacChRangeMaxVal(int ch, int range) const {
        Q_UNUSED(ch)
        return f_ranges[range];
    }

    double LTR34TypeInfo::outDacChRangeMinVal(int ch, int range) const {
        Q_UNUSED(ch)
        return -f_ranges[range];
    }

    QList<const DeviceTypeInfo *> LTR34TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list {
            &typeInfoMod8(),
            &typeInfoMod4()
        };
        return list;
    }

    LTR34TypeInfo::LTR34TypeInfo(int dac_ch_cnt) : m_dac_ch_cnt{dac_ch_cnt} {

    }
}



