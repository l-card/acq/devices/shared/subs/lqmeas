#include "LTR34OutConfig.h"
#include <QJsonObject>
#include <QJsonArray>
#include "LTR34TypeInfo.h"
#include "lqmeas/StdConfigKeys.h"
#include "ltr/include/ltr34api.h"

namespace LQMeas {
    static const QLatin1String cfgkey_out_freq          {StdConfigKeys::outFreq()};
    static const QLatin1String cfgkey_sync_mode_en      {StdConfigKeys::syncModeEn()};
    static const QLatin1String cfgkey_ch_range_num      {StdConfigKeys::rangeNum()};
    static const QLatin1String cfgkey_dac_channels_arr  {StdConfigKeys::dacChannels()};
    static const QLatin1String cfgkey_ext_start         {"ExtStart"};
    static const QLatin1String cfgkey_echo_en           {"EchoEn"};
    static const QLatin1String cfgkey_dev_obj           {StdConfigKeys::dev()};

    void LTR34OutConfig::outSyncSetEchoEnabled(bool en)     {
        if (m_params.echoEnabled != en) {
            m_params.echoEnabled = en;
            notifyConfigChanged();
        }
    }

    void LTR34OutConfig::outSetSyncModeEnabled(bool en) {
        if (m_params.syncMode != en) {
            m_params.syncMode = en;
            notifyDacChSyncEnableStateChanged(-1);
            notifyConfigChanged();
        }
    }

    void LTR34OutConfig::outSetExternalStart(bool en) {
        if (m_params.externalStart != en) {
            m_params.externalStart = en;
            notifyConfigChanged();
        }
    }

    void LTR34OutConfig::outSyncSetGenFreq(double val) {
        if (m_params.OutFreq != val) {
            m_params.OutFreq = val;
            notifyDacChFreqChanged(-1);
            notifyConfigChanged();
        }
    }

    void LTR34OutConfig::outDacSetChRangeNum(int ch, int range)  {
        if (m_params.ch[ch].range != range) {
            m_params.ch[ch].range = range;
            notifyDacChRangeChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR34OutConfig::protSave(QJsonObject &cfgObj) const {
        DevOutSyncConfig::protSave(cfgObj);

        QJsonObject devObj;
        devObj[cfgkey_out_freq] = m_params.OutFreq;
        devObj[cfgkey_sync_mode_en] = m_params.syncMode;
        devObj[cfgkey_ext_start] = m_params.externalStart;
        devObj[cfgkey_echo_en] = m_params.echoEnabled;
        QJsonArray dacChArray;
        for (int ch_idx {0}; ch_idx < max_dac_ch_cnt; ++ch_idx) {
            QJsonObject dacChObj;
            dacChObj[cfgkey_ch_range_num] = m_params.ch[ch_idx].range;
            dacChArray.append(dacChObj);
        }
        devObj[cfgkey_dac_channels_arr] = dacChArray;
        cfgObj[cfgkey_dev_obj] = devObj;
    }

    void LTR34OutConfig::protLoad(const QJsonObject &cfgObj) {
        DevOutSyncConfig::protLoad(cfgObj);

        const QJsonObject &devObj {cfgObj[cfgkey_dev_obj].toObject()};
        m_params.OutFreq = devObj[cfgkey_out_freq].toDouble(outInfo().outSyncGenFreqMax());
        m_params.syncMode = devObj[cfgkey_sync_mode_en].toBool();
        m_params.externalStart = devObj[cfgkey_ext_start].toBool();
        m_params.echoEnabled = devObj[cfgkey_echo_en].toBool();

        const QJsonArray &dacChArray {devObj[cfgkey_dac_channels_arr].toArray()};
        for (int ch_idx {0}; (ch_idx < dacChArray.size()) && (ch_idx < max_dac_ch_cnt); ++ch_idx) {
            const QJsonObject &dacChObj {dacChArray.at(ch_idx).toObject()};
            m_params.ch[ch_idx].range = dacChObj[cfgkey_ch_range_num].toInt();
        }
    }

    void LTR34OutConfig::protUpdate()  {
        DevOutSyncConfig::protUpdate();

        int en_ch_cnt {0};
        for (int ch {0}; ch < outSyncDacChannelsCnt(); ++ch) {
            if (outSyncDacChEnabled(ch))
                ++en_ch_cnt;
        }
        if ((en_ch_cnt < 8) && (en_ch_cnt > 4)) {
            en_ch_cnt = 8;
        } else if (en_ch_cnt == 3) {
            en_ch_cnt = 4;
        }

        BYTE div;
        LTR34_FindDacFreqDivisor(m_params.OutFreq, static_cast<BYTE>(en_ch_cnt), &div, &m_results.out_freq);
        ++en_ch_cnt;
    }

    LTR34OutConfig::LTR34OutConfig(const DevOutInfo &info) : DevOutSyncConfig{info} {
        memset(&m_params, 0, sizeof(m_params));
        m_params.OutFreq = m_results.out_freq = info.outSyncGenFreqMax();
    }

    LTR34OutConfig::LTR34OutConfig(const LTR34OutConfig &outCfg) : DevOutSyncConfig{outCfg} {
        memcpy(&m_params, &outCfg.m_params, sizeof(outCfg.m_params));
        memcpy(&m_results, &outCfg.m_results, sizeof(outCfg.m_results));
    }
}
