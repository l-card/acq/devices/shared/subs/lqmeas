#include "LTR34.h"
#include "LTR34Config.h"
#include "LTR34OutConfig.h"
#include "LTR34Info.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdErrors.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/devs/LTR/modules/LTRModuleFrameReceiver.h"
#include "lqmeas/ifaces/out/SyncModes/stream/DevOutSyncModeImplStream.h"
#include "lqmeas/ifaces/out/SyncModes/cycle/DevOutSyncModeImplCycle.h"


namespace LQMeas {

    LTR34::LTR34(LTRCrate *crate, int slot, QObject *parent) :
        LTRModule{crate, slot, typeModuleID, new LTR34Config{}, LTR34Info{}, parent},
        DevOutSync{this},
        DevOutAsyncDac{this},
        DevOutDacConverter{this},
        m_sender{this},
        m_echoReceiver{new LTRModuleFrameReceiver{this}} {

        LTR34_Init(&m_hnd);

        outSyncAddMode(new DevOutSyncModeImplCycle{this, this});
        outSyncAddMode(new DevOutSyncModeImplStream{this, this});
    }



    LTR34::~LTR34() {
        LTR34_Close(&m_hnd);
    }

    bool LTR34::isOpened() const {
        return LTR34_IsOpened(&m_hnd) == LTR_OK;
    }

    QString LTR34::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTR34_GetErrorString(err));
    }

    const LTR34Config &LTR34::devspecConfig() const {
        return static_cast<const LTR34Config &>(config());
    }

    QSharedPointer<const LTR34Info> LTR34::devspecInfo() const {
        return devInfo().staticCast<const LTR34Info>();
    }

    INT LQMeas::LTR34::ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) {
        return LTR34_Recv(&m_hnd, wrds, marks, size, tout);
    }

    INT LTR34::ltrRawWordsSend(const DWORD *wrds, DWORD size, DWORD tout) {
        return LTR34_Send(&m_hnd, wrds, size, tout);
    }

    void LTR34::outSyncEchoTrackStart(LQError &err) {
        Q_UNUSED(err)
        m_echo_pos = 0;
        int ch_cnt {0};
        for (int ch_idx {0}; ch_idx < devTypeInfo().out()->outDacChannelsCnt(); ++ch_idx) {
            if (devspecConfig().out().outSyncDacChEnabled(ch_idx))
                ++ch_cnt;
        }
        m_echoReceiver->start(ch_cnt, ch_cnt);
    }

    void LTR34::outSyncEchoTrackStop(LQError &err) {
        Q_UNUSED(err)
    }

    void LTR34::outSyncEchoTrackWait(quint64 wt_pos, unsigned tout, quint64 &fnd_pos, LQError &err) {
        if (m_echo_pos < wt_pos) {
            DWORD *frame;
            int recv {static_cast<int>(wt_pos - m_echo_pos)};
            int recvd;
            m_echoReceiver->getFrames(recv*m_echoReceiver->frameSize(),
                                      tout, frame, recvd, err);
            if (err.isSuccess()) {
                m_echo_pos += static_cast<quint64>(recvd/m_echoReceiver->frameSize());
            }
        }

        fnd_pos = m_echo_pos;
    }

    TLTR *LTR34::channel() const {
        return &m_hnd.Channel;
    }

    int LTR34::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        Q_UNUSED(flags)
        INT ltr_err = LTR34_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, static_cast<WORD>(slot));
        if (ltr_err == LTR_OK) {
            memset(m_last_async_vals, 0, sizeof(m_last_async_vals));
            m_rst_done = false;

            const LTR34TypeInfo *type_info {m_hnd.ModuleInfo.MaxChannelQnt == 4 ?
                                               &LTR34TypeInfo::typeInfoMod4() : &LTR34TypeInfo::typeInfoMod8()};
            setDeviceInfo(LTR34Info{*type_info, QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                    QSTRING_FROM_CSTR(m_hnd.ModuleInfo.FPGA_Version)});
        }
        return ltr_err;
    }

    void LTR34::protClose(LQError &err) {
        getError(LTR34_Close(&m_hnd), err);
    }

    void LTR34::protConfigure(const DeviceConfig &cfg, LQError &err)  {
        const LTR34Config *setCfg {qobject_cast<const LTR34Config *>(&cfg)};
        if (setCfg) {
            INT ltr_err {LTR_OK};

            const int dac_ch_cnt {devTypeInfo().out()->outDacChannelsCnt()};
            if (m_hnd.DACRunning)
                LTR34_DACStop(&m_hnd);

            m_hnd.RingMode = setCfg->out().outSyncGenModeEnabled() &&
                    (setCfg->out().outSyncRamMode() == DevOutSyncConfig::OutSyncRamMode::Cycle);
            m_hnd.ExternalStart = setCfg->out().outExternalStart() && setCfg->out().outSyncGenModeEnabled();
            m_hnd.UseClb = TRUE;
            m_hnd.AcknowledgeType = setCfg->out().outSyncEchoEnabled() ? LTR34_ACKTYPE_ECHO : LTR34_ACKTYPE_STATUS;

            if (ltr_err == LTR_OK) {
                m_hnd.ChannelQnt = 0;
                for (int ch = 0; ch < dac_ch_cnt; ++ch) {
                    if (!setCfg->out().outSyncGenModeEnabled() || setCfg->out().outSyncDacChEnabled(ch)) {
                        m_hnd.LChTbl[m_hnd.ChannelQnt] = LTR34_CreateLChannel(
                                    static_cast<BYTE>(ch + 1), setCfg->out().outDacChRangeNum(ch) != 0);
                        ++m_hnd.ChannelQnt;
                    }
                }

                ltr_err = LTR34_FindDacFreqDivisor(setCfg->out().outSyncGenFreq(), m_hnd.ChannelQnt,
                                                     &m_hnd.FrequencyDivisor, nullptr);
            }

            if (ltr_err != LTR_OK) {
                getError(ltr_err, err);
            }

            if (err.isSuccess())
                genReset(err);
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }

    void LTR34::privOutSyncSendData(const double *dac_data, int dac_size,
                                    const unsigned *dig_data, int dig_size,
                                    unsigned flags, unsigned tout, LQError &err) {
        Q_UNUSED(dig_data) Q_UNUSED(dig_size) Q_UNUSED(flags)

        if (m_sender.unsentWordsCnt()) {
             m_sender.flushData(tout, err);
        }

        if (err.isSuccess()) {
            if (m_sender.unsentWordsCnt()!=0) {
                err = LQMeas::StdErrors::SendBusy();
            } else {
                DWORD snd_size = static_cast<DWORD>(dac_size);
                QScopedArrayPointer<DWORD> wrds (new DWORD[snd_size]);

                getError(LTR34_ProcessData(&m_hnd, dac_data, wrds.data(), snd_size, TRUE), err);
                if (!err.isSuccess()) {
                    LQMeasLog->error(tr("Prepare data error"), err, this);
                } else {
                    m_sender.setFrameSize(static_cast<int>(snd_size));
                    m_sender.putFrames(wrds.data(), static_cast<int>(snd_size), tout, nullptr, err);
                }
            }
        }
    }

    bool LTR34::privOutSyncHasDefferedData() const {
        return m_sender.unsentWordsCnt() > 0;
    }

    void LTR34::privOutSyncTryFlushData(unsigned tout, LQError &err) {
        m_sender.flushData(tout, err);
    }

    void LTR34::privOutSyncSendFinish(LQError &err) {
        Q_UNUSED(err)
        m_sender.setFrameSize(0);
    }

    bool LTR34::privOutSyncUnderflowOccured() const {
        return false;
    }

    int LTR34::outCycleGenMaxSize(const OutRamSignalGenerator &generator) const {
        int chCnt = generator.dacSignalsCount();
        return chCnt == 0 ? 0 : LTR34_MAX_BUFFER_SIZE/chCnt;
    }

    void LTR34::privOutCycleLoadStart(const OutRamSignalGenerator &generator, int size, LQError &err) {
        Q_UNUSED(generator) Q_UNUSED(size)
        /* Смена страниц на лету не поддерживается, поэтому при запущенной генерации
           необходимо остановить ЦАП и сбросить модуль перед загрузкой нового сигнала */
        if (m_hnd.DACRunning) {
            LTR34_DACStop(&m_hnd);
        }

        if (!m_rst_done) {
            genReset(err);
        }
    }

    void LTR34::privOutCycleLoadFinish(LQError &err) {
        Q_UNUSED(err)
    }

    void LTR34::privOutCycleGenStartRequest(LQError &err) {
        genStart(err);
    }

    void LTR34::privOutCycleGenStopRequest(unsigned tout, LQError &err) {
        genStop(tout, err);
    }

    void LTR34::privOutCycleGenUpdateRequest(LQError &err) {
        err = StdErrors::UnsupportedFeature();
    }

    void LTR34::privOutCycleGenStop(LQError &err) {
        Q_UNUSED(err)
    }

    void LTR34::privOutStreamInit(LQError &err) {
        Q_UNUSED(err)
    }

    void LTR34::privOutStreamStartRequest(LQError &err) {
        genStart(err);
    }

    void LTR34::privOutStreamStop(unsigned tout, LQError &err) {
        genStop(tout, err);
    }

    void LTR34::protOutAsyncDac(int ch, double val, LQError &err) {
        m_last_async_vals[ch] = val;
        privOutSyncSendData(m_last_async_vals, m_hnd.ChannelQnt, nullptr, 0, 0, 0, err);
        if (err.isSuccess())
            privOutSyncFlushData(100, err);
        if (err.isSuccess() && !m_hnd.DACRunning) {
            privOutStreamStartRequest(err);
        }
    }

    void LTR34::genReset(LQError &err) {
        INT ltr_err = LTR34_Reset(&m_hnd);
        if (ltr_err == LTR_OK) {
            ltr_err = LTR34_Config(&m_hnd);
        }
        if (ltr_err == LTR_OK) {
            m_rst_done = true;
        } else {
            getError(ltr_err, err);
        }
    }

    void LTR34::genStart(LQError &err) {
        if (!m_rst_done) {
            genReset(err);
        }
        if (err.isSuccess())
            getError(LTR34_DACStart(&m_hnd), err);
        if (err.isSuccess()) {
            m_rst_done = false;
        }
    }

    void LTR34::genStop(unsigned tout, LQError &err) {
        if (m_sender.unsentWordsCnt()) {
            m_sender.flushData(tout, err);
            m_sender.clearData();
        }
        m_echoReceiver->clear();
        INT ltr_err = LTR34_DACStopRequest(&m_hnd);
        if (ltr_err == LTR_OK)
            ltr_err = LTR34_DACStopResponseWait(&m_hnd, tout);        
        getError(ltr_err, err);
    }
}
