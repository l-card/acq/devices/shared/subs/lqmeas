#ifndef LQMEAS_LTR34TYPEINFO_H
#define LQMEAS_LTR34TYPEINFO_H

#include "lqmeas/devs/DeviceTypeInfo.h"
#include "lqmeas/ifaces/out/DevOutInfo.h"
#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"

namespace LQMeas {
    class LTR34TypeInfo : public LTRModuleTypeInfo, public DevOutInfo {
    public:
        static const QString &name();
        QString deviceTypeName() const override {return name();}
        QString deviceModificationName() const override;

        static const LTR34TypeInfo &defaultTypeInfo();
        static const LTR34TypeInfo &typeInfoMod8();
        static const LTR34TypeInfo &typeInfoMod4();

        static const int rangeNum10V {0};
        static const int rangeNum1V  {1};


        /* ----------------- DevOutInfo -------------------------------------*/
        const DevOutInfo *out() const override {return static_cast<const DevOutInfo *>(this);}

        bool outDacSyncSupport() const override {return true;}
        bool outDigSyncSupport() const override {return false;}

        bool outDacAsyncSupport() const override {return true;}
        bool outDigAsyncSupport() const override {return false;}

        bool outDacSyncModeCfgPerCh() const override {return false;}

        bool outSyncRamModeIsConfigurable() const override {return true;}
        bool outSyncDacChGenModeIsConfigurable() const override {return false;}
        bool outSyncDigChGenModeIsConfigurable() const override {return false;}


        double outSyncGenFreqMax() const override;
        bool outSyncPresetSupport() const override {return false;}

        int outDacChannelsCnt() const override {return m_dac_ch_cnt;}
        int outDigChannelsCnt() const override {return 0;}
        int outDacChRangesCnt(int ch) const override;
        double outDacChRangeMaxVal(int ch, int range) const override;
        double outDacChRangeMinVal(int ch, int range) const override;

        QList<const DeviceTypeInfo *> modificationList() const override;
    protected:
        explicit LTR34TypeInfo(int dac_ch_cnt);
    private:
        int m_dac_ch_cnt;
    };
}

#endif // LQMEAS_LTR34TYPEINFO_H
