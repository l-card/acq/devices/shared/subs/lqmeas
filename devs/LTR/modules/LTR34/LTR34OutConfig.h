#ifndef LQMEAS_LTR34OUTCONFIG_H
#define LQMEAS_LTR34OUTCONFIG_H

#include "lqmeas/ifaces/out/DevOutSyncConfig.h"

namespace LQMeas {
    class LTR34OutConfig : public DevOutSyncConfig {
        Q_OBJECT
    public:
        bool outSyncGenModeEnabled() const {return m_params.syncMode;}
        void outSetSyncModeEnabled(bool en);

        bool outExternalStart() const {return m_params.externalStart;}
        void outSetExternalStart(bool en);

        void outSyncSetGenFreq(double val);
        double outSyncGenFreq() const override {return m_results.out_freq;}

        void outDacSetChRangeNum(int ch, int range);
        int outDacChRangeNum(int ch) const override {return m_params.ch[ch].range;}

        bool outDacChSyncModeEnabled(int ch) const override { Q_UNUSED(ch) return m_params.syncMode;}
        bool outDigChSyncModeEnabled(int ch) const override { Q_UNUSED(ch) return false;}

        bool outSyncEchoEnabled() const override {return m_params.echoEnabled;}
        void outSyncSetEchoEnabled(bool en);

        int outSyncDacSampleSize() const override {return 4;}
        int outSyncDigSampleSize() const override {return 0;}
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
        void protUpdate() override;
    private:
        static const int max_dac_ch_cnt {8};

        friend class LTR34Config;
        LTR34OutConfig(const DevOutInfo &info);
        LTR34OutConfig(const LTR34OutConfig &outCfg);

        struct {
            struct {
                int range;
            } ch[max_dac_ch_cnt];
            bool syncMode;
            bool externalStart;
            bool echoEnabled;
            double OutFreq;
        } m_params;


        struct {
            double out_freq;
        } m_results;
    };
}

#endif // LQMEAS_LTR34OUTCONFIG_H
