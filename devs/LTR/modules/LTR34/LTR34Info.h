#ifndef LQMEAS_LTR34INFO_H
#define LQMEAS_LTR34INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTR34TypeInfo.h"

namespace LQMeas {
    class LTR34Info : public DeviceInfo {
    public:
        explicit LTR34Info(const LTR34TypeInfo &type = LTR34TypeInfo::defaultTypeInfo(),
                           const QString &serial = QString{},
                           const QString &fpga_ver_str = QString{});
        LTR34Info(const LTR34Info &info);

        const QString &fpgaVerStr() const {return m_fpga_ver;}

        DeviceInfo *clone() const override {return new LTR34Info{*this};}
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        QString m_fpga_ver;
    };
}

#endif // LQMEAS_LTR34INFO_H
