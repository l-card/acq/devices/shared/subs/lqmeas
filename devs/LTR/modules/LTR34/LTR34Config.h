#ifndef LQMEAS_LTR34CONFIG_H
#define LQMEAS_LTR34CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"
#include <memory>

namespace LQMeas {
    class LTR34OutConfig;

    class LTR34Config : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        const DevOutConfig *outConfig() const override;
        const DevOutSyncConfig *outSyncConfig() const override;

        LTR34OutConfig &out() {return  *m_out;}
        const LTR34OutConfig &out() const {return  *m_out;}

        DeviceConfig *clone() const override {
            return new LTR34Config{*this};
        }

        LTR34Config();
        LTR34Config(const LTR34Config &cfg);
        ~LTR34Config() override;
    private:        
        const std::unique_ptr<LTR34OutConfig> m_out;
    };
}

#endif // LQMEAS_LTR34CONFIG_H
