#ifndef LQMEAS_LTR34_H
#define LQMEAS_LTR34_H

#include "ltr/include/ltr34api.h"
#include "lqmeas/devs/LTR/modules/LTRModule.h"

#include "lqmeas/devs/DeviceFrameSender.h"
#include "lqmeas/ifaces/out/DevOutSync.h"
#include "lqmeas/ifaces/out/DevOutAsyncDac.h"
#include "lqmeas/ifaces/out/DevOutSyncEchoTracker.h"
#include "lqmeas/ifaces/out/SignalConverter/DevOutDacConverter.h"
#include "lqmeas/ifaces/out/SyncModes/stream/DevOutSyncIfaceStream.h"
#include "lqmeas/ifaces/out/SyncModes/cycle/DevOutSyncIfaceCycle.h"
#include <memory>

namespace LQMeas {
    class LTR34Config;
    class LTR34Info;
    class LTRModuleFrameReceiver;

    class LTR34 : public LTRModule, public DevOutSync, public DevOutAsyncDac,
            public DevOutSyncEchoTracker, public DevOutDacConverter,
            DevOutSyncIfaceCycle, DevOutSyncIfaceStream {
        Q_OBJECT
    public:
        static const quint16 typeModuleID {LTR_MID_LTR34};

        ~LTR34() override;
        bool isOpened() const override;

        DevOutSync *devOutSync() override {return static_cast<DevOutSync*>(this);}
        DevOutAsyncDac *devOutAsyncDac() override {return static_cast<DevOutAsyncDac *>(this);}
        DevOutSyncEchoTracker *devOutSyncEchoTracker() override {return static_cast<DevOutSyncEchoTracker *>(this);}
        DevOutDacConverter *devOutDacConverter() override {return static_cast<DevOutDacConverter *>(this);}

        QString errorString(int err) const override;

        const LTR34Config &devspecConfig() const;
        QSharedPointer<const LTR34Info> devspecInfo() const;

        void outSyncEchoTrackStart(LQError &err) override;
        void outSyncEchoTrackStop(LQError &err) override;
        void outSyncEchoTrackWait(quint64 wt_pos, unsigned tout, quint64 &fnd_pos, LQError &err) override;
    protected:
        TLTR* channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn,  int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;
    private:
        explicit LTR34(LTRCrate *crate, int slot, QObject *parent = nullptr);

        INT ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) override;
        INT ltrRawWordsSend(const DWORD *wrds, DWORD size, DWORD tout) override;

        void privOutSyncSendData(const double *dac_data, int dac_size,
                                 const unsigned *dig_data, int dig_size,
                                 unsigned flags, unsigned tout, LQError &err) override;
        bool privOutSyncHasDefferedData() const override;
        void privOutSyncTryFlushData(unsigned tout, LQError &err) override;
        void privOutSyncSendFinish(LQError &err) override;
        bool privOutSyncUnderflowOccured() const override;


        int outCycleGenMaxSize(const OutRamSignalGenerator &generator) const override;
        void privOutCycleLoadStart(const OutRamSignalGenerator &generator, int size, LQError &err) override;
        void privOutCycleLoadFinish(LQError &err) override;
        void privOutCycleGenStartRequest(LQError &err) override;
        void privOutCycleGenStopRequest(unsigned tout, LQError &err) override;
        void privOutCycleGenUpdateRequest(LQError &err) override;
        void privOutCycleGenStop(LQError &err) override;

        void privOutStreamInit(LQError &err) override;
        void privOutStreamStartRequest(LQError &err) override;
        void privOutStreamStop(unsigned tout, LQError &err) override;


        void protOutAsyncDac(int ch, double val, LQError &err) override;

        void genReset(LQError &err);
        void genStart(LQError &err);
        void genStop(unsigned tout, LQError &err);

        mutable TLTR34 m_hnd;
        DeviceFrameSender<LTR34, DWORD> m_sender;
        const std::unique_ptr<LTRModuleFrameReceiver> m_echoReceiver;
        double m_last_async_vals[LTR34_DAC_NUMBER_MAX];

        friend class LTRResolver;
        friend class DeviceFrameSender<LTR34, DWORD>;

        quint64 m_echo_pos {0};
        /* перед запуском генерации модуля обязательно нужно сделать
         * Reset и Config. Это может быть сделано уже при конфигурации,
         * а может быть режим запуска после останова без изменения настроек.
         * В этом случае сброс нужен вручную. Это поле указывает, был ли
         * сброс с момента последнего запуска сбора */
        bool m_rst_done {false};
    };

}

#endif // LTR34_H
