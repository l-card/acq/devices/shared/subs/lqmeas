#ifndef LQMEAS_LTR216INFO_H
#define LQMEAS_LTR216INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTR216TypeInfo.h"

namespace LQMeas {
    class LTR216Info : public DeviceInfo {
    public:
        LTR216Info(const LTR216TypeInfo &type = LTR216TypeInfo::defaultTypeInfo(),
                   const QString &serial = QString{}, int fpga_ver = 0);


        DeviceInfo *clone() const override {return new LTR216Info{*this};}
        int fpgaVer() const {return m_fpga_ver;}
        QString fpgaVerStr() const {return QString::number(m_fpga_ver);}


        const LTR216TypeInfo &devspecTypeInfo() const {
            return static_cast<const LTR216TypeInfo &>(type());
        }
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        LTR216Info(const LTR216Info &info);

        int m_fpga_ver;
    };
}

#endif // LQMEAS_LTR216INFO_H
