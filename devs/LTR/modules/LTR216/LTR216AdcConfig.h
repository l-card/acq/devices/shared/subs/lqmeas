#ifndef LQMEAS_LTR216ADCCONFIG_H
#define LQMEAS_LTR216ADCCONFIG_H

#include "lqmeas/ifaces/in/adc/DevAdcSeqBaseConfig.h"
#include "ltr/include/ltr216api.h"

namespace LQMeas {
    class LTR216AdcConfig : public DevAdcSeqBaseConfig {
        Q_OBJECT
    public:
        static const int adc_channels_cnt {16};

        enum class AdcChRange {
            U_0_35 = 0,
            U_0_70
        };

        enum class FilterType {
            SINC5_1 = LTR216_FILTER_SINC5_1,
            SINC3 = LTR216_FILTER_SINC3,
            ENH_50HZ = LTR216_FILTER_ENH_50HZ
        };

        enum class BgMeas {
            NoMeas    = 0,
            Offs      = LTR216_BG_MEAS_OFFS,
            Uref      = LTR216_BG_MEAS_UREF,
            UrefOffs  = LTR216_BG_MEAS_UREF_OFFS,
            Vadj      = LTR216_BG_MEAS_VADJ,
            Uneg      = LTR216_BG_MEAS_UNEG,
            UrefShort = LTR216_BG_MEAS_UREF_SHORT,
            UrefOpen  = LTR216_BG_MEAS_UREF_OPEN,
            UnegOpen  = LTR216_BG_MEAS_UNEG_OPEN,
            ChShort   = LTR216_BG_MEAS_CH_SHORT,
            ChOpen    = LTR216_BG_MEAS_CH_OPEN,
            ChUx      = LTR216_BG_MEAS_CH_UX,
            ChCm      = LTR216_BG_MEAS_CH_CM
        };
        Q_DECLARE_FLAGS(BgMeasMask, BgMeas);

        class BgMeasGroup {
        public:
            virtual ~BgMeasGroup();
            virtual QString name() const = 0;
            virtual QString id() const = 0;
            virtual BgMeasMask measMask() const = 0;
        };

        static const BgMeasGroup &measGroupAll();
        static const BgMeasGroup &measGroupNoOpen();
        static const BgMeasGroup &measGroupNoBgTable();
        static const BgMeasGroup &measGroupUnbalanceOnly();
        static const QList<const BgMeasGroup *> &supportedMeasGroup();


        /* возвращает максимально количество каналов, которое можно разрешить
         * для текущих настроек модуля */
        int adcMaxEnabledChCnt();

        int adcChRangeNum(int ch) const override {return m_ch_params[ch].Range;}
        AdcChRange adcChRange(int ch) const {return static_cast<AdcChRange>(adcChRangeNum(ch));}

        double iSrc() const {return m_adc_results.isrc;}
        double configuredISrc() const {return m_params.isrc;}

        bool singleChannelMode() const {return m_params.singleChMode;}
        bool useCh16ForUref() const {return m_params.ch16forUref;}
        bool tareEnabled() const {return m_params.tareCoefEn;}
        double requredSwitchTimeUs() const {return m_params.swTimeUs;}
        double switchTimeUs() const {return m_adc_results.swTimeUs;}
        const BgMeasGroup *bgMeasGroup() const {return m_params.bgMeasGroup;}
        BgMeasMask bgMeasMask() const;
        bool bgMeasIsEnabled(BgMeas meas) const;

        double shortThresholdR() const {return m_params.shortThresholdR;}
        double cableLength() const {return m_params.cableLength;}
        double cableCapacityPerUnit() const {return m_params.cableCapPerUnit;}

        bool manualFilterConfigEnabled() const {return m_params.manualFilter.En;}
        FilterType manualFilterType() const {return m_params.manualFilter.Type;}
        int manualFilterOdr() const {return m_params.manualFilter.OdrCode;}



        FilterType filterType() const {return static_cast<FilterType>(m_adc_results.filterParams.FilterType);}
        int filterOdrCode() const {return static_cast<int>(m_adc_results.filterParams.AdcOdrCode);}
        double filterOdr() const {return m_adc_results.filterParams.Odr;}
        double filterNotchFreq() const {return m_adc_results.filterParams.NotchFreq;}
        double filterNotchDb() const {return m_adc_results.filterParams.NotchDB;}
        bool filterNotchDbIsValid() const {return filterType() == FilterType::ENH_50HZ;}

        int adcSwModeCode() const;

        int adcSampleSize() const override {return 8;}
        static constexpr int typeAdcRangeNum(LQMeas::LTR216AdcConfig::AdcChRange range) {return static_cast<int>(range);}
    public Q_SLOTS:
        void adcSetChRangeNum(int ch, int range_num);
        void adcSetChRange(int ch, LQMeas::LTR216AdcConfig::AdcChRange range);

        void setISrc(double val);
        void setSingleChannelMode(bool en);
        void setUseCh16ForUref(bool en);
        void setTareEnabled(bool en);
        void setRequredSwitchTimeUs(double us);
        void setBgMeasGroup(const LQMeas::LTR216AdcConfig::BgMeasGroup *group);
        void setBgMeasEnabled(LQMeas::LTR216AdcConfig::BgMeas meas, bool en);
        void setShortThresholdR(double r);
        void setCableLength(double len);
        void setCableCapacityPerUnit(double val);
        void setManualFilterConfigEnabled(bool en);
        void setManualFilterType(LQMeas::LTR216AdcConfig::FilterType type);
        void setManualFilterOdr(int odr);

    protected:
        void protUpdateDevice(const Device *device) override;
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;

        void adcChSave(int ch_num, QJsonObject &chObj) const override;
        void adcChLoad(int ch_num, const QJsonObject &chObj) override;
        void adcChSetDefault(int ch_num) override;
        void adcAdjustFreq(double &adcFreq, double &adcChFreq) override;

    private:
        void fillConfig(TLTR216_CONFIG *cfg, const TLTR216_MODULE_INFO *info) const;

        static constexpr double defaultAdcFreq() {return 10000;}
        static constexpr double defaultSwTimeUs() {return 10;}
        static constexpr double defaultISrc() {return 2.5;}

        LTR216AdcConfig(const DevAdcInfo &info);
        LTR216AdcConfig(const LTR216AdcConfig &adcCfg);

        TLTR216_MODULE_INFO m_minfo;

        struct {
            int Range;
        } m_ch_params[adc_channels_cnt];

        struct  {
            double isrc;
            bool singleChMode;
            bool ch16forUref;
            bool tareCoefEn;
            double swTimeUs;


            const BgMeasGroup *bgMeasGroup;
            BgMeasMask bgMeasMask;
            double shortThresholdR;
            double cableLength;
            double cableCapPerUnit;

            struct {
                bool En;
                FilterType Type;
                int OdrCode;
            } manualFilter;
        } m_params;

        struct {
            double isrc;
            double swTimeUs;
            TLTR216_FILTER_OUT_PARAMS filterParams;
        } m_adc_results;

        friend class LTR216;
        friend class LTR216Config;
    };
}

Q_DECLARE_OPERATORS_FOR_FLAGS(LQMeas::LTR216AdcConfig::BgMeasMask)
Q_DECLARE_METATYPE(LQMeas::LTR216AdcConfig::AdcChRange);
Q_DECLARE_METATYPE(LQMeas::LTR216AdcConfig::FilterType);
Q_DECLARE_METATYPE(LQMeas::LTR216AdcConfig::BgMeas);


#endif // LQMEAS_LTR216ADCCONFIG_H
