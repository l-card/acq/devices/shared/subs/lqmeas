#include "LTR216.h"
#include "LTR216Info.h"
#include "LTR216Config.h"
#include "LTR216AdcConfig.h"
#include "LTR216AdcTareOps.h"
#include "LTR216AdcTareCoefs.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/devs/DeviceRef.h"
#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "lqmeas/devs/LTR/modules/LTRModuleFrameReceiver.h"
#include "lqmeas/log/Log.h"
#include "lqmeas/ifaces/in/adc/tare/DevAdcTareChCoefs.h"
#include "lqmeas/ifaces/in/adc/tare/DevAdcTarePoint.h"
#include "lqmeas/StdErrors.h"

namespace LQMeas {
    LTR216::LTR216(LTRCrate  *crate, int slot, QObject *parent) :
        LTRModule{crate, slot, typeModuleID, new LTR216Config{}, LTR216Info{}, parent},
        DevAdc{this, LTR216AdcConfig::adc_channels_cnt},
        DevAdcTare{this},
        m_receiver{new LTRModuleFrameReceiver{this}} {

        LTR216_Init(&m_hnd);
    }

    LTR216::~LTR216() {
        LTR216_Close(&m_hnd);
    }

    bool LTR216::isOpened() const {
        return LTR216_IsOpened(&m_hnd) == LTR_OK;
    }

    DevSyncMarksRecv *LTR216::devSyncMarkRecv() {
        return m_receiver.get();
    }

    QString LTR216::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTR216_GetErrorString(err));
    }

    bool LTR216::adcIsRunning() const     {
        return m_hnd.State.Run != FALSE;
    }

    void LTR216::adcGetRawFrames(int frames_cnt, unsigned tout,
                                 DWORD *&frames_buf, int &recvd_words, LQError &err)     {
        m_receiver->getFrames(frames_cnt * static_cast<int>(m_hnd.State.FrameWordsCount),
                             tout, frames_buf, recvd_words, err);
    }

    const LTR216Config &LTR216::devspecConfig() const {
        return static_cast<const LTR216Config &>(config());
    }

    QSharedPointer<const LTR216Info> LTR216::devspecInfo() const {
        return devInfo().staticCast<const LTR216Info>();
    }

    TLTR *LTR216::channel() const {
        return &m_hnd.Channel;
    }

    int LTR216::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags)  {
        Q_UNUSED(flags)
        int ltr_err = LTR216_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, slot);

        if (ltr_err == LTR_OK) {
            setDeviceInfo(LTR216Info{LTR216TypeInfo::defaultTypeInfo(),
                                     QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                     m_hnd.ModuleInfo.VerFPGA});
        }
        return ltr_err;
    }

    void LTR216::protClose(LQError &err) {
        getError(LTR216_Close(&m_hnd), err);
    }

    void LTR216::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTR216Config *setCfg = qobject_cast<const LTR216Config *>(&cfg);
        if (setCfg) {
            setCfg->adc().fillConfig(&m_hnd.Cfg, &m_hnd.ModuleInfo);
            getError(LTR216_SetADC(&m_hnd), err);
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }

    INT LTR216::ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) {
        return LTR216_Recv(&m_hnd, wrds, marks, size, tout);
    }

    void LTR216::protAdcPreStart(LQError &err)     {
        getError(LTR216_InitMeasParams(&m_hnd, LTR216_INIT_MEAS_ALL, 0, nullptr), err);
    }

    void LTR216::protAdcStart(LQError &err) {
        getError(LTR216_Start(&m_hnd), err);
        if (err.isSuccess()) {
            int frameSize = static_cast<int>(m_hnd.State.FrameWordsCount);
            m_receiver->start(frameSize, 2);
        }
    }

    void LTR216::protAdcStop(LQError &err) {
        getError(LTR216_Stop(&m_hnd), err);
        m_receiver->clear();
    }

    void LTR216::protAdcGetData(double *data, int size, unsigned flags,
                                unsigned tout, int &recvd_size, LQError &err) {
        int recvd_words {0};
        DWORD *wrds;
        INT proc_size {0};
        const int frames_cnt {size/adcConfig().adcEnabledChCnt()};


        m_receiver->getFrames(frames_cnt * static_cast<int>(m_hnd.State.FrameWordsCount),
                              tout, wrds, recvd_words, err);

        if (err.isSuccess() && (recvd_words != 0)) {
            proc_size = static_cast<INT>(recvd_words);

            getError(LTR216_ProcessData(&m_hnd, wrds, data, &proc_size,
                                        flags, nullptr, nullptr), err);
            if (!err.isSuccess()) {
                LQMeasLog->error(tr("Process data error"), err, this);
            }
        }

        if (err.isSuccess()) {
            recvd_size = proc_size;
        }
    }

    void LTR216::adcTareExec(const DevAdcTareOp &op, const DevAdcTarePoint &point,
                             quint32 ch_mask, quint32 &ch_ok_mask, LQError &err) {
        ch_ok_mask = 0;
        if (op == LTR216AdcTareOps::externalTare()) {
            if (point == DevAdcTarePoints::zero()) {
                INT err_code = LTR216_InitMeasParams(&m_hnd, LTR216_INIT_MEAS_OFFS, 0, nullptr);
                if (err_code == LTR_OK) {
                    DWORD ret_ok_msk;
                    err_code = LTR216_TareOffset(&m_hnd, ch_mask, &ret_ok_msk);
                    if (err_code == LTR_OK)
                        ch_ok_mask = ret_ok_msk;
                }
                if ((err_code != LTR_OK) && (err_code != LTR216_ERR_TARE_CHANNELS)) {
                    getError(err_code, err);
                }
            } else if (point == DevAdcTarePoints::scale()) {
                INT err_code = LTR216_InitMeasParams(&m_hnd, LTR216_INIT_MEAS_UREF, 0, nullptr);
                if (err_code == LTR_OK) {
                    DWORD ret_ok_msk;
                    err_code = LTR216_TareScale(&m_hnd, ch_mask, &ret_ok_msk);
                    if (err_code == LTR_OK)
                        ch_ok_mask = ret_ok_msk;
                }
                if ((err_code != LTR_OK) && (err_code != LTR216_ERR_TARE_CHANNELS)) {
                    getError(err_code, err);
                }
            } else {
                err = StdErrors::TareInvalidPoint();
            }
        } else if (op == LTR216AdcTareOps::reset()) {
            if (point == DevAdcTarePoints::zero()) {
                for (int ch_idx = 0; ch_idx < devInfo()->type().adc()->adcChannelsCnt(); ++ch_idx) {
                    if (ch_mask & (1 << ch_idx)) {
                        m_hnd.ModuleInfo.Tare[ch_idx].OffsetValid = FALSE;
                        m_hnd.ModuleInfo.Tare[ch_idx].Offset = 0;
                        m_hnd.ModuleInfo.Tare[ch_idx].DacValid = FALSE;
                        m_hnd.ModuleInfo.Tare[ch_idx].DacValue = 0;
                        ch_ok_mask |= (1 << ch_idx);
                    }
                }
            } else if (point == DevAdcTarePoints::scale()) {
                for (int ch_idx = 0; ch_idx < devInfo()->type().adc()->adcChannelsCnt(); ++ch_idx) {
                    if (ch_mask & (1 << ch_idx)) {
                        m_hnd.ModuleInfo.Tare[ch_idx].ScaleValid = FALSE;
                        m_hnd.ModuleInfo.Tare[ch_idx].Scale = 1;
                        ch_ok_mask |= (1 << ch_idx);
                    }
                }
            } else {
                err = StdErrors::TareInvalidPoint();
            }
        } else {
            err = StdErrors::TareInvalidOp();
        }

        if (ch_ok_mask != 0) {
            int err_code = LTR216_WriteTareInfo(&m_hnd, ch_ok_mask);
            if (err_code != LTR_OK) {
                LQError wrError;
                getError(err_code, wrError);
                err.update(wrError);
                ch_ok_mask = 0;
            }
        }
    }

    void LTR216::adcTareGetChCoefs(int ch, DevAdcTareChCoefs &coefs, LQError &err) {
        Q_UNUSED(err)

        const TLTR216_TARE_CH_COEFS &chCoefs = m_hnd.ModuleInfo.Tare[ch];
        coefs.clear();
        coefs.setCoefValue(LTR216AdcTareCoefs::dacValue(),   chCoefs.DacValue*1000, chCoefs.DacValid);
        coefs.setCoefValue(LTR216AdcTareCoefs::offsetCoef(), chCoefs.Offset*1000,   chCoefs.OffsetValid);
        coefs.setCoefValue(LTR216AdcTareCoefs::scaleCoef(),  chCoefs.Scale,         chCoefs.ScaleValid);
   }
}
