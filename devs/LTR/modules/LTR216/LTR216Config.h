#ifndef LQMEAS_LTR216CONFIG_H
#define LQMEAS_LTR216CONFIG_H


#include "lqmeas/devs/DeviceConfig.h"
#include <memory>

namespace LQMeas {
    class LTRSyncMarksRecvConfig;
    class LTR216AdcConfig;

    class LTR216Config : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        const DevAdcConfig *adcConfig() const override;
        const DevSyncMarksRecvConfig *syncMarksRecvConfig() const override;

        LTR216AdcConfig &adc() {return  *m_adc;}
        const LTR216AdcConfig &adc() const {return  *m_adc;}

        LTRSyncMarksRecvConfig &marksRecv() {return  *m_syncRecv;}
        const LTRSyncMarksRecvConfig &marksRecv() const {return  *m_syncRecv;}

        double inPeakSpeed() const override;

        DeviceConfig *clone() const override {
            return new LTR216Config{*this};
        }

        LTR216Config();
        LTR216Config(const LTR216Config &cfg);
        ~LTR216Config() override;
    protected:
        void protUpdate() override;
    private:
        const std::unique_ptr<LTR216AdcConfig> m_adc;
        const std::unique_ptr<LTRSyncMarksRecvConfig> m_syncRecv;
    };
}
#endif // LQMEAS_LTR216CONFIG_H
