#ifndef LQMEAS_LTR216ADCTAREOPS_H
#define LQMEAS_LTR216ADCTAREOPS_H

#include <QObject>
#include "lqmeas/ifaces/in/adc/tare/DevAdcTareOp.h"

namespace LQMeas {
    class LTR216AdcTareOps : public QObject {
        Q_OBJECT
    public:
        static const DevAdcTareOp &externalTare();
        static const DevAdcTareOp &reset();
    };
}

#endif // LQMEAS_LTR216ADCTAREOPS_H
