#include "LTR216AdcConfig.h"
#include <QJsonObject>
#include <QJsonArray>
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/StdConfigKeys.h"
#include "LTR216.h"
#include "LTR216TypeInfo.h"

extern "C" LTR216API_DllExport(INT) LTR216_InitDefaultModuleInfo(TLTR216_MODULE_INFO *minfo);

namespace LQMeas {

    static const QLatin1String cfgkey_range_num         {StdConfigKeys::rangeNum()};
    static const QLatin1String cfgkey_isrc              {"ISrc"};
    static const QLatin1String cfgkey_single_ch_mode    {"SingleChMode"};
    static const QLatin1String cfgkey_ch16_uref         {"Ch16ForUref"};
    static const QLatin1String cfgkey_tare_en           {"TareEn"};
    static const QLatin1String cfgkey_swtime_us         {"SwTimeUS"};
    static const QLatin1String cfgkey_bgmeas_obj        {"BgMeas"};
    static const QLatin1String cfgkey_bgmeas_group      {"Group"};
    static const QLatin1String cfgkey_bgmeas_mask       {"Mask"};
    static const QLatin1String cfgkey_short_thresh_r    {"ShortThreshR"};
    static const QLatin1String cfgkey_cable_obj         {"Cable"};
    static const QLatin1String cfgkey_cable_length      {"Length"};
    static const QLatin1String cfgkey_cable_c_per_unit  {"CapacityPerUnit"};
    static const QLatin1String cfgkey_manual_flt_obj    {"ManualFilter"};
    static const QLatin1String cfgkey_manual_flt_en     {"En"};
    static const QLatin1String cfgkey_manual_flt_type   {StdConfigKeys::type()};
    static const QLatin1String cfgkey_manual_flt_odr    {"Odr"};


    typedef EnumConfigKey<LTR216AdcConfig::FilterType> FilterTypeKey;
    static const FilterTypeKey filter_types[] = {
        {QStringLiteral("sinc5_1"),     LTR216AdcConfig::FilterType::SINC5_1},
        {QStringLiteral("sinc3"),       LTR216AdcConfig::FilterType::SINC3},
        {QStringLiteral("eh50hz"),      LTR216AdcConfig::FilterType::ENH_50HZ},
        {FilterTypeKey::defaultKey(),   LTR216AdcConfig::FilterType::SINC5_1},
    };



    LTR216AdcConfig::LTR216AdcConfig(const DevAdcInfo &info) : DevAdcSeqBaseConfig{info} {

        memset(m_ch_params, 0, sizeof(m_ch_params));
        memset(&m_params, 0, sizeof(m_params));
        memset(&m_adc_results, 0, sizeof(m_adc_results));

        m_params.ch16forUref = true;
        m_params.singleChMode = false;
        m_params.isrc = defaultISrc();
        adcSetFreq(defaultAdcFreq());
        m_params.swTimeUs = defaultSwTimeUs();
        m_params.bgMeasGroup = nullptr;

        LTR216_InitDefaultModuleInfo(&m_minfo);
    }

    LTR216AdcConfig::LTR216AdcConfig(const LTR216AdcConfig &adcCfg) : DevAdcSeqBaseConfig{adcCfg} {
        memcpy(m_ch_params, adcCfg.m_ch_params, sizeof(m_ch_params));
        memcpy(&m_params, &adcCfg.m_params, sizeof(m_params));
        memcpy(&m_adc_results, &adcCfg.m_adc_results, sizeof(m_adc_results));
        memcpy(&m_minfo, &adcCfg.m_minfo, sizeof(m_minfo));
    }


    const LTR216AdcConfig::BgMeasGroup &LTR216AdcConfig::measGroupAll() {
        static const class BgMeasGroupAll : public BgMeasGroup {
            QString name() const override {return LTR216::tr("All measurements");}
            QString id() const override {return QStringLiteral("all");}
            BgMeasMask measMask() const override {return static_cast<BgMeasMask>(LTR216_BG_MEASGROUP_ALL);}
        } measgroup;
        return measgroup;
    }

    const LTR216AdcConfig::BgMeasGroup &LTR216AdcConfig::measGroupNoOpen() {
        static const class BgMeasGroupNoOpen : public BgMeasGroup {
            QString name() const override {return LTR216::tr("Without open circuit check");}
            QString id() const override {return QStringLiteral("noopen");}
            BgMeasMask measMask() const override {return static_cast<BgMeasMask>(
                            LTR216_BG_MEASGROUP_ALL & ~LTR216_BG_MEASGROUP_OPEN);}
        } measgroup;
        return measgroup;
    }

    const LTR216AdcConfig::BgMeasGroup &LTR216AdcConfig::measGroupNoBgTable() {
        static const class BgMeasGroupNoBgTable : public BgMeasGroup {
            QString name() const override {return LTR216::tr("Without background table");}
            QString id() const override {return QStringLiteral("nobgtable");}
            BgMeasMask measMask() const override {return static_cast<BgMeasMask>(
                            LTR216_BG_MEASGROUP_ALL & ~LTR216_BG_MEASGROUP_SECTBL);}
        } measgroup;
        return measgroup;
    }

    const LTR216AdcConfig::BgMeasGroup &LTR216AdcConfig::measGroupUnbalanceOnly() {
        static const class BgMeasGroupUnbalance : public BgMeasGroup {
            QString name() const override {return LTR216::tr("Unbalance only");}
            QString id() const override {return QStringLiteral("unbalance");}
            BgMeasMask measMask() const override {return BgMeas::NoMeas;}
        } measgroup;
        return measgroup;
    }

    const QList<const LTR216AdcConfig::BgMeasGroup *> &LTR216AdcConfig::supportedMeasGroup() {
        static const QList<const LTR216AdcConfig::BgMeasGroup *> list {
            &measGroupAll(),
            &measGroupNoOpen(),
            &measGroupNoBgTable(),
            &measGroupUnbalanceOnly()
        };
        return list;

    }

    LTR216AdcConfig::BgMeasGroup::~BgMeasGroup() {

    }

    int LTR216AdcConfig::adcMaxEnabledChCnt() {
        return singleChannelMode() ? 1 : (m_params.ch16forUref ? 15 : 16);
    }

    LTR216AdcConfig::BgMeasMask LTR216AdcConfig::bgMeasMask() const {
        return m_params.bgMeasGroup ? m_params.bgMeasGroup->measMask() : m_params.bgMeasMask;
    }

    bool LTR216AdcConfig::bgMeasIsEnabled(LTR216AdcConfig::BgMeas meas) const {
        return bgMeasMask() & meas;
    }

    int LTR216AdcConfig::adcSwModeCode() const {
        return singleChannelMode() ? LTR216_ADC_SWMODE_SIGNLECH_CONT :
                                     LTR216_ADC_SWMODE_MULTICH_SYNC;
    }

    void LTR216AdcConfig::adcSetChRangeNum(int ch, int range_num) {
        if (m_ch_params[ch].Range != range_num) {
            m_ch_params[ch].Range = range_num;
            notifyChRangeChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR216AdcConfig::adcSetChRange(int ch, LTR216AdcConfig::AdcChRange range) {
        return adcSetChRangeNum(ch, typeAdcRangeNum(range));
    }

    void LTR216AdcConfig::setISrc(double val) {
        if (m_params.isrc != val) {
            m_params.isrc = val;
            notifyConfigChanged();
        }
    }

    void LTR216AdcConfig::setSingleChannelMode(bool en) {
        if (m_params.singleChMode != en) {
            m_params.singleChMode = en;
            notifyChFreqChanged();
            notifyAdcFreqChanged();
            notifyConfigChanged();
        }
    }

    void LTR216AdcConfig::setUseCh16ForUref(bool en) {
        if (m_params.ch16forUref != en) {
            m_params.ch16forUref = en;
            notifyChFreqChanged();
            notifyConfigChanged();
        }
    }

    void LTR216AdcConfig::setTareEnabled(bool en) {
        if (m_params.tareCoefEn != en) {
            m_params.tareCoefEn = en;
            notifyConfigChanged();
        }
    }

    void LTR216AdcConfig::setRequredSwitchTimeUs(double us) {
        if (m_params.swTimeUs != us) {
            m_params.swTimeUs = us;
            notifyAdcFreqChanged();
            notifyChFreqChanged();
            notifyConfigChanged();
        }
    }

    void LTR216AdcConfig::setBgMeasGroup(const LTR216AdcConfig::BgMeasGroup *group) {
        if (m_params.bgMeasGroup != group) {
            m_params.bgMeasGroup = group;
            notifyAdcFreqChanged();
            notifyChFreqChanged();
            notifyConfigChanged();
        }
    }

    void LTR216AdcConfig::setBgMeasEnabled(BgMeas meas, bool en) {
        if (bgMeasIsEnabled(meas) != en) {
            if (en) {
                m_params.bgMeasMask |= meas;
            } else {
                m_params.bgMeasMask &= ~static_cast<LTR216AdcConfig::BgMeasMask>(meas);
            }
            m_params.bgMeasGroup = nullptr;
            notifyAdcFreqChanged();
            notifyChFreqChanged();
            notifyConfigChanged();
        }
    }

    void LTR216AdcConfig::setShortThresholdR(double r) {
        if (m_params.shortThresholdR != r) {
            m_params.shortThresholdR = r;
            notifyConfigChanged();
        }
    }

    void LTR216AdcConfig::setCableLength(double len) {
        if (m_params.cableLength != len) {
            m_params.cableLength = len;
            notifyConfigChanged();
        }
    }

    void LTR216AdcConfig::setCableCapacityPerUnit(double val) {
        if (m_params.cableCapPerUnit != val) {
            m_params.cableCapPerUnit = val;
            notifyConfigChanged();
        }
    }

    void LTR216AdcConfig::setManualFilterConfigEnabled(bool en) {
        if (m_params.manualFilter.En != en) {
            m_params.manualFilter.En = en;
            notifyChFreqChanged();
            notifyAdcFreqChanged();
            notifyConfigChanged();
        }
    }

    void LTR216AdcConfig::setManualFilterType(FilterType type) {
        if (m_params.manualFilter.Type != type) {
            m_params.manualFilter.Type = type;
            notifyChFreqChanged();
            notifyAdcFreqChanged();
            notifyConfigChanged();
        }
    }

    void LTR216AdcConfig::setManualFilterOdr(int odr)  {
        if (m_params.manualFilter.OdrCode != odr) {
            m_params.manualFilter.OdrCode = odr;
            notifyChFreqChanged();
            notifyAdcFreqChanged();
            notifyConfigChanged();
        }
    }


    void LTR216AdcConfig::protUpdateDevice(const Device *device) {
        const LTR216 * ltr216 {qobject_cast<const LTR216 *>(device)};
        if (ltr216) {
            m_minfo = ltr216->rawHandle()->ModuleInfo;
        }
    }

    void LTR216AdcConfig::protSave(QJsonObject &cfgObj) const {
        DevAdcSeqBaseConfig::protSave(cfgObj);

        cfgObj[cfgkey_isrc] = m_params.isrc;
        cfgObj[cfgkey_single_ch_mode] = m_params.singleChMode;
        cfgObj[cfgkey_ch16_uref] = m_params.ch16forUref;
        cfgObj[cfgkey_tare_en] = m_params.tareCoefEn;
        cfgObj[cfgkey_swtime_us] = m_params.swTimeUs;


        QJsonObject bgMeas;
        bgMeas[cfgkey_bgmeas_group] = m_params.bgMeasGroup ? m_params.bgMeasGroup->id() : QString();
        bgMeas[cfgkey_bgmeas_mask] = static_cast<int>(m_params.bgMeasMask);
        cfgObj[cfgkey_bgmeas_obj] = bgMeas;

        cfgObj[cfgkey_short_thresh_r] = m_params.shortThresholdR;
        QJsonObject cableObj;
        cableObj[cfgkey_cable_length] = m_params.cableLength;
        cableObj[cfgkey_cable_c_per_unit] = m_params.cableCapPerUnit;
        cfgObj[cfgkey_cable_obj] = cableObj;



        QJsonObject filterObj;
        filterObj[cfgkey_manual_flt_en] = m_params.manualFilter.En;
        filterObj[cfgkey_manual_flt_type] = FilterTypeKey::getKey(
                    filter_types, m_params.manualFilter.Type);
        filterObj[cfgkey_manual_flt_odr] = m_params.manualFilter.OdrCode;
        cfgObj[cfgkey_manual_flt_obj] = filterObj;
    }

    void LTR216AdcConfig::protLoad(const QJsonObject &cfgObj) {
        DevAdcSeqBaseConfig::protLoad(cfgObj);

        m_params.isrc = cfgObj[cfgkey_isrc].toDouble(defaultISrc());
        m_params.singleChMode = cfgObj[cfgkey_single_ch_mode].toBool();
        m_params.ch16forUref = cfgObj[cfgkey_ch16_uref].toBool(true);
        m_params.tareCoefEn = cfgObj[cfgkey_tare_en].toBool();
        m_params.swTimeUs = cfgObj[cfgkey_swtime_us].toDouble(defaultSwTimeUs());

        const QJsonObject &bgMeas {cfgObj[cfgkey_bgmeas_obj].toObject()};
        const QString &groupNameId {bgMeas[cfgkey_bgmeas_group].toString()};
        m_params.bgMeasGroup = nullptr;
        if (!groupNameId.isEmpty()) {
            const auto &groups {supportedMeasGroup()};
            const auto &it {std::find_if(groups.constBegin(), groups.constEnd(),
                                   [&groupNameId](const auto *group) {return group->id() == groupNameId;})};
            if (it != groups.constEnd()) {
                m_params.bgMeasGroup = *it;
            }
        }
        m_params.bgMeasMask = static_cast<BgMeasMask>(bgMeas[cfgkey_bgmeas_mask].toInt());

        m_params.shortThresholdR = cfgObj[cfgkey_short_thresh_r].toDouble();

        const QJsonObject &cableObj {cfgObj[cfgkey_cable_obj].toObject()};
        m_params.cableLength = cableObj[cfgkey_cable_length].toDouble();
        m_params.cableCapPerUnit = cableObj[cfgkey_cable_c_per_unit].toDouble();


        const QJsonObject &filterObj {cfgObj[cfgkey_manual_flt_obj].toObject()};
        m_params.manualFilter.En = filterObj[cfgkey_manual_flt_en].toBool();
        m_params.manualFilter.Type = FilterTypeKey::getValue(
                    filter_types, filterObj[cfgkey_manual_flt_type].toString());
        m_params.manualFilter.OdrCode = filterObj[cfgkey_manual_flt_odr].toInt();
    }

    void LTR216AdcConfig::adcChSave(int ch_num, QJsonObject &chObj) const {
        chObj[cfgkey_range_num] = m_ch_params[ch_num].Range;
    }

    void LTR216AdcConfig::adcChLoad(int ch_num, const QJsonObject &chObj) {
        const int range {chObj[cfgkey_range_num].toInt()};
        if ((range >= 0) || (range < adcInfo().adcChRangesCnt(ch_num, adcUnitMode(ch_num)))) {
            m_ch_params[ch_num].Range = range;
        } else {
            m_ch_params[ch_num].Range = 0;
        }
    }

    void LTR216AdcConfig::adcChSetDefault(int ch_num) {
        m_ch_params[ch_num].Range = 0;
    }

    void LTR216AdcConfig::adcAdjustFreq(double &adcFreq, double &adcChFreq) {
        TLTR216_CONFIG tmpCfg;
        fillConfig(&tmpCfg, &m_minfo);



        LTR216_CalcISrcValue(&m_minfo.FabricCbr.ISrc, tmpCfg.ISrcCode, &m_adc_results.isrc);

        LTR216_GetFilterOutParams(static_cast<DWORD>(adcSwModeCode()),
                                  tmpCfg.FilterType, tmpCfg.AdcOdrCode,
                                  &m_adc_results.filterParams);
        if (singleChannelMode()) {
            adcFreq = adcChFreq = m_adc_results.filterParams.Odr;
            m_adc_results.swTimeUs = 0;
        } else {
            TLTR216_RAWCONFIG tmpRawCfg;
            LTR216_FillRawTables(&m_minfo, &tmpCfg, &tmpRawCfg);
            LTR216_FindSyncFreqDiv(adcFreq, nullptr, &adcFreq);
            m_adc_results.swTimeUs = (1./(adcFreq) - 1./m_adc_results.filterParams.Odr) * 1000000;
            adcChFreq = tmpRawCfg.FrameSize > 0 ? adcFreq/tmpRawCfg.FrameSize : adcFreq;
        }
    }


    void LTR216AdcConfig::fillConfig(TLTR216_CONFIG *cfg, const TLTR216_MODULE_INFO *info) const {
        double adcFreq {adcConfiguredFreq()};

        memset(cfg, 0, sizeof(TLTR216_CONFIG));
        cfg->AdcSwMode = static_cast<BYTE>(adcSwModeCode());
        LTR216_FindISrcCode(info ? &info->FabricCbr.ISrc : nullptr, m_params.isrc,
                            &cfg->ISrcCode, nullptr);
        if (!singleChannelMode()) {
            LTR216_FindSyncFreqDiv(adcFreq, &cfg->SyncFreqDiv, &adcFreq);
        }

        cfg->AdcMinSwTimeUs = m_params.swTimeUs;
        cfg->AdcReqFrameFreq = adcUseMaxChFreq() ? 0 : adcConfiguredChFreq();




        if (!m_params.manualFilter.En) {
            TLTR216_FILTER_OUT_PARAMS flt_params;
            LTR216_FindFilterParams(static_cast<DWORD>(adcSwModeCode()),
                                    adcFreq, m_params.swTimeUs,
                                   &flt_params, nullptr);
            cfg->FilterType = flt_params.FilterType;
            cfg->AdcOdrCode = flt_params.AdcOdrCode;
        } else {
            cfg->FilterType = static_cast<DWORD>(m_params.manualFilter.Type);
            cfg->AdcOdrCode = static_cast<DWORD>(m_params.manualFilter.OdrCode);
        }

        cfg->TareEnabled = tareEnabled();
        cfg->Ch16ForUref = useCh16ForUref();

        cfg->BgMeas = bgMeasMask();

        cfg->ShortThresholdR = shortThresholdR();
        cfg->CableLength = cableLength();
        cfg->CableCapacityPerUnit = cableCapacityPerUnit();

        cfg->LChCnt = 0;
        for (int ch_num {0}; ch_num < LTR216_CHANNELS_CNT; ++ch_num) {
            if (adcChEnabled(ch_num)) {
                cfg->LChTbl[cfg->LChCnt].PhyChannel = static_cast<BYTE>(ch_num);
                cfg->LChTbl[cfg->LChCnt].Range = static_cast<BYTE>(adcChRangeNum(ch_num));
                ++cfg->LChCnt;
            }
        }
    }
}

