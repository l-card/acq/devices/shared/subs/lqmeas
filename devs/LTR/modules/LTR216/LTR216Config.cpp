#include "LTR216Config.h"
#include "LTR216AdcConfig.h"
#include "LTR216TypeInfo.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvConfig.h"

namespace LQMeas {
    const QString &LTR216Config::typeConfigName() {
        return LTR216TypeInfo::name();
    }

    const DevAdcConfig *LTR216Config::adcConfig() const {
        return m_adc.get();
    }

    const DevSyncMarksRecvConfig *LTR216Config::syncMarksRecvConfig() const {
        return m_syncRecv.get();
    }

    double LTR216Config::inPeakSpeed() const {
        /** @note в LTR114 в межкадровый интервал приходят фоновые измерения, поэтому
         *  поток считается от исходной частоты АЦП, а не частоты кадров */
        return m_adc->adcEnabledChCnt() > 0 ? m_adc->adcSampleSize() * m_adc->adcFreq() : 0;
    }

    LTR216Config::LTR216Config() :
        m_adc{new LTR216AdcConfig{LTR216TypeInfo::defaultTypeInfo()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this}} {

        init();
    }

    LTR216Config::LTR216Config(const LTR216Config &cfg) :
        m_adc{new LTR216AdcConfig{cfg.adc()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this, cfg.marksRecv()}} {

        init();
    }

    LTR216Config::~LTR216Config() {
    }

    void LTR216Config::protUpdate() {
        m_syncRecv->updateSyncMarksRecvFreq(m_adc->adcFreq());
    }
}
