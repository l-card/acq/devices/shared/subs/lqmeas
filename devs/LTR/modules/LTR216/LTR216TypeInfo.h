#ifndef LQMEAS_LTR216TYPEINFO_H
#define LQMEAS_LTR216TYPEINFO_H


#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"
#include "lqmeas/ifaces/in/adc/DevAdcInfo.h"
#include "lqmeas/ifaces/in/adc/tare/DevAdcTareInfo.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvInfo.h"

namespace LQMeas {
    class LTR216TypeInfo : public LTRModuleTypeInfo, public DevAdcInfo,
            public DevAdcTareInfo, public LTRSyncMarksRecvInfo {
    public:
        static const QString &name();
        static const LTR216TypeInfo &defaultTypeInfo();

        const DevAdcInfo *adc() const override {
            return static_cast<const DevAdcInfo *>(this);
        }
        const DevAdcTareInfo *adcTare() const override {
            return static_cast<const DevAdcTareInfo *>(this);
        }
        const DevSyncMarksRecvInfo *syncMarkRecv() const override {
            return static_cast<const DevSyncMarksRecvInfo *>(this);
        }

        QString deviceTypeName() const override {return name();}

        AdcType adcType() const override {return AdcType::Sequential;}
        int adcChannelsCnt() const override;
        double adcFreqMax() const override {return 250000;}

        bool adcIsChFreqConfigurable() const override {return true;}

        int adcChRangesCnt(int ch_num, int unit_mode_num = 0) const override;
        double adcChRangeMaxVal(int ch_num, int range, int unit_mode_num = 0) const override;
        double adcChRangeMinVal(int ch_num, int range, int unit_mode_num = 0) const override;
        const Unit &adcChRangeUnit(int ch_num, int unit_mode_num = 0) const override;
        const Unit &adcChValUnit(int ch_num, int unit_mode_num = 0) const  override;

        QList<const DevAdcTarePoint *> adcTarePoints() const override;
        QList<const DevAdcTareOp *> adcTareOps(const DevAdcTarePoint &point) const override;
        QList<const DevAdcTareCoefType *> adcTareCoefTypes() const override;

        QList<const DeviceTypeInfo *> modificationList() const override;
    };
}

#endif // LQMEAS_LTR216TYPEINFO_H
