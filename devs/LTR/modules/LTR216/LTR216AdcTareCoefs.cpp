#include "LTR216AdcTareCoefs.h"
#include "lqmeas/units/std/Voltage.h"
#include "lqmeas/units/std/Relative.h"

namespace LQMeas {
    const DevAdcTareCoefType &LTR216AdcTareCoefs::dacValue() {
        static class LTR216AdcTareCoefTypeDacValue : public DevAdcTareCoefType {
            QString id() const override {return QStringLiteral("dacValue");}
            QString displayName() const override {return tr("DAC Value");}
            const Unit &unit() const override {return Units::Voltage::mV();}
            bool hasValidFlag() const override {return  true;}
        } coef;
        return coef;
    }

    const DevAdcTareCoefType &LTR216AdcTareCoefs::offsetCoef() {
        static class LTR216AdcTareCoefTypeOffset : public DevAdcTareCoefType {
            QString id() const override {return QStringLiteral("offsetCoef");}
            QString displayName() const override {return tr("Offset Ceof");}
            const Unit &unit() const override {return Units::Voltage::mV();}
            bool hasValidFlag() const override {return  true;}
        } coef;
        return coef;
    }

    const DevAdcTareCoefType &LTR216AdcTareCoefs::scaleCoef() {
        static class LTR216AdcTareCoefTypeScale : public DevAdcTareCoefType {
            QString id() const override {return QStringLiteral("scaleCoef");}
            QString displayName() const override {return tr("Scale Ceof");}
            const Unit &unit() const override {return Units::Relative::ratio();}
            bool hasValidFlag() const override {return  true;}
        } coef;
        return coef;
    }
}
