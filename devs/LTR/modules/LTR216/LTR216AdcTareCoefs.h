#ifndef LQMEAS_LTR216ADCTARECOEFS_H
#define LQMEAS_LTR216ADCTARECOEFS_H

#include <QObject>
#include "lqmeas/ifaces/in/adc/tare/DevAdcTareCoefType.h"

namespace LQMeas {
    class LTR216AdcTareCoefs : public QObject {
        Q_OBJECT
    public:
        static const DevAdcTareCoefType &dacValue();
        static const DevAdcTareCoefType &offsetCoef();
        static const DevAdcTareCoefType &scaleCoef();
    };
}

#endif // LQMEAS_LTR216ADCTARECOEFS_H
