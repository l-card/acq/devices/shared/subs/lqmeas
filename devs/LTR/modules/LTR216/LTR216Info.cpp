#include "LTR216Info.h"
#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String &cfgkey_fpga_ver             {StdConfigKeys::fpgaVer()};

    LTR216Info::LTR216Info(const LTR216TypeInfo &type, const QString &serial, int fpga_ver) :
        DeviceInfo{type, serial}, m_fpga_ver{fpga_ver} {

    }

    LTR216Info::LTR216Info(const LTR216Info &info) : DeviceInfo{info}, m_fpga_ver{info.m_fpga_ver} {

    }
    void LTR216Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_fpga_ver] = m_fpga_ver;
    }

    void LTR216Info::protLoad(const QJsonObject &infoObj) {
        m_fpga_ver = infoObj[cfgkey_fpga_ver].toInt();
    }


}
