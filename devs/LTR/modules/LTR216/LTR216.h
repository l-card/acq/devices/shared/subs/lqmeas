#ifndef LQMEAS_LTR216_H
#define LQMEAS_LTR216_H



#include "ltr/include/ltr216api.h"
#include "lqmeas/devs/LTR/modules/LTRModule.h"
#include "lqmeas/ifaces/in/adc/DevAdc.h"
#include "lqmeas/ifaces/in/adc/tare/DevAdcTare.h"
#include <memory>

namespace LQMeas {
    class LTR216Config;
    class LTR216Info;
    class LTRModuleFrameReceiver;
    class LTR216 : public LTRModule, public DevAdc, public DevAdcTare {
        Q_OBJECT
    public:
        static const quint16 typeModuleID {LTR_MID_LTR216};

        ~LTR216() override;

        bool isOpened() const override;
        /* ----------------------- интерфейс ввода  --------------------------*/
        DevAdc *devAdc() override {return static_cast<DevAdc*>(this);}
        DevAdcTare *devAdcTare() override {return static_cast<DevAdcTare *>(this);}
        DevSyncMarksRecv *devSyncMarkRecv() override;

        QString errorString(int err) const override;

        bool adcIsRunning() const override;


        void adcTareExec(const DevAdcTareOp &op,
                         const DevAdcTarePoint &point,
                         quint32 ch_mask,
                         quint32 &ch_ok_mask,
                         LQError &err) override;
        void adcTareGetChCoefs(int ch, DevAdcTareChCoefs &coefs, LQError &err) override;

        void adcGetRawFrames(int frames_cnt, unsigned tout, DWORD *&frames_buf,
                             int &recvd_words, LQError &err);

        const LTR216Config &devspecConfig() const;
        QSharedPointer<const LTR216Info> devspecInfo() const;

        TLTR216 *rawHandle() {return &m_hnd;}
        const TLTR216 *rawHandle() const {return &m_hnd;}
    protected:
        TLTR* channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn,  int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;

        INT ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) override;

        void protAdcPreStart(LQError &err) override;
        void protAdcStart(LQError &err) override;
        void protAdcStop(LQError &err) override;
        void protAdcGetData(double *data, int size, unsigned flags,
                            unsigned tout, int &recvd_size, LQError &err) override;
    private:
        explicit LTR216(LTRCrate *crate, int slot, QObject *parent = nullptr);


        mutable TLTR216 m_hnd;
        const std::unique_ptr<LTRModuleFrameReceiver> m_receiver;

        friend class LTRResolver;
    };
}

#endif // LQMEAS_LTR216_H

