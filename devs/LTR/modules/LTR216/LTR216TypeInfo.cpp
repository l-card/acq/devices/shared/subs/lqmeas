#include "LTR216TypeInfo.h"
#include "LTR216AdcTareOps.h"
#include "LTR216AdcTareCoefs.h"
#include "LTR216AdcConfig.h"
#include "lqmeas/ifaces/in/adc/tare/DevAdcTarePoint.h"
#include "lqmeas/units/std/Voltage.h"

namespace LQMeas {

    static const double f_ranges[] = {35, 70};

    const QString &LTR216TypeInfo::name() {
        static const QString str {QStringLiteral("LTR216")};
        return str;
    }

    int LTR216TypeInfo::adcChannelsCnt() const {
        return LTR216AdcConfig::adc_channels_cnt;
    }

    int LTR216TypeInfo::adcChRangesCnt(int ch_num, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return sizeof(f_ranges)/sizeof(f_ranges[0]);
    }

    double LTR216TypeInfo::adcChRangeMaxVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return f_ranges[range];
    }

    double LTR216TypeInfo::adcChRangeMinVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return -f_ranges[range];
    }

    const Unit &LTR216TypeInfo::adcChRangeUnit(int ch_num, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return Units::Voltage::mV();
    }

    const Unit &LTR216TypeInfo::adcChValUnit(int ch_num, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return Units::Voltage::V();
    }

    QList<const DevAdcTarePoint *> LTR216TypeInfo::adcTarePoints() const {
        static const QList<const DevAdcTarePoint *> list {
            &DevAdcTarePoints::zero(),
            &DevAdcTarePoints::scale()
        };
        return list;
    }

    QList<const DevAdcTareOp *> LTR216TypeInfo::adcTareOps(const DevAdcTarePoint &point) const {
        Q_UNUSED(point)
        static const QList<const DevAdcTareOp *> list {
            &LTR216AdcTareOps::externalTare(),
            &LTR216AdcTareOps::reset()
        };
        return list;
    }

    QList<const DevAdcTareCoefType *> LTR216TypeInfo::adcTareCoefTypes() const {
        static const QList<const DevAdcTareCoefType *> list {
            &LTR216AdcTareCoefs::dacValue(),
            &LTR216AdcTareCoefs::offsetCoef(),
            &LTR216AdcTareCoefs::scaleCoef()
        };
        return list;
    }

    QList<const DeviceTypeInfo *> LTR216TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list { &defaultTypeInfo() };
        return list;
    }
    
    const LTR216TypeInfo &LTR216TypeInfo::defaultTypeInfo() {
        static const LTR216TypeInfo info;
        return info;
    }


}
