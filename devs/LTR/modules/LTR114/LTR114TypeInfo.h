#ifndef LQMEAS_LTR114TYPEINFO_H
#define LQMEAS_LTR114TYPEINFO_H

#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"
#include "lqmeas/ifaces/in/adc/DevAdcInfo.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvInfo.h"

namespace LQMeas {
    class LTR114TypeInfo : public LTRModuleTypeInfo, public DevAdcInfo, public LTRSyncMarksRecvInfo {
    public:
        static const QString &name();
        static const LTR114TypeInfo &defaultTypeInfo();

        const DevAdcInfo *adc() const override {
            return static_cast<const DevAdcInfo *>(this);
        }
        const DevSyncMarksRecvInfo *syncMarkRecv() const override {
            return static_cast<const DevSyncMarksRecvInfo *>(this);
        }

        QString deviceTypeName() const override {return name();}

        AdcType adcType() const override {return AdcType::Sequential;}
        int adcChannelsCnt() const override {return 16;}
        double adcFreqMax() const override {return 4000;}

        bool adcIsChFreqConfigurable() const override {return true;}

        int adcChRangesCnt(int ch_num, int unit_mode_num = 0) const override;
        double adcChRangeMaxVal(int ch_num, int range, int unit_mode_num = 0) const override;
        double adcChRangeMinVal(int ch_num, int range, int unit_mode_num = 0) const override;
        const Unit &adcChRangeUnit(int ch_num, int unit_mode_num = 0) const override;
        int adcUnitModesCnt(int ch_num = 0) const override;

        QList<const DeviceTypeInfo *> modificationList() const override;

        static const int adcUnitModeU {0};
        static const int adcUnitModeR {1};
        static const int adcChNumT    {16};
    };
}



#endif // LTR1144TYPEINFO_H
