#include "LTR114TypeInfo.h"
#include "lqmeas/units/std/Voltage.h"
#include "lqmeas/units/std/Resistance.h"
#include "lqmeas/units/std/Temperature.h"

namespace LQMeas {

    static const double f_u_ranges[] = {10., 2., 0.4};
    static const double f_r_ranges[] = {4000, 1200, 400};
    static const double f_t_range_min = -25;
    static const double f_t_range_max = +75;

    const QString &LTR114TypeInfo::name() {
        static const QString str {QStringLiteral("LTR114")};
        return str;
    }

    int LTR114TypeInfo::adcChRangesCnt(int ch_num, int unit_mode_num) const {
        return ch_num == adcChNumT ? 1 : (unit_mode_num == adcUnitModeU ?
                                             sizeof(f_u_ranges)/sizeof(f_u_ranges[0]) :
                                             sizeof(f_r_ranges)/sizeof(f_r_ranges[0]));
    }

    double LTR114TypeInfo::adcChRangeMaxVal(int ch_num, int range, int unit_mode_num) const {
         return ch_num == adcChNumT ? f_t_range_max : (unit_mode_num == adcUnitModeU ?
                                                           f_u_ranges[range] : f_r_ranges[range]);
    }

    double LTR114TypeInfo::adcChRangeMinVal(int ch_num, int range, int unit_mode_num) const {
        return ch_num == adcChNumT ? f_t_range_min : (unit_mode_num == adcUnitModeU ?
                                                          -f_u_ranges[range] : 0);
    }

    const Unit &LTR114TypeInfo::adcChRangeUnit(int ch_num, int unit_mode_num) const     {
        return (ch_num == adcChNumT)           ? Units::Temperature::degreeC() :
               (unit_mode_num == adcUnitModeU) ? Units::Voltage::V() :
                                                 Units::Resistance::ohm();
    }

    int LTR114TypeInfo::adcUnitModesCnt(int ch_num) const {
        return ch_num == adcChNumT ? 1 : 2;
    }

    QList<const DeviceTypeInfo *> LTR114TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list { &defaultTypeInfo() };
        return list;
    }
    
    
    
    const LTR114TypeInfo &LTR114TypeInfo::defaultTypeInfo() {
        static const LTR114TypeInfo info;
        return info;
    }
}
