#include "LTR114Config.h"
#include "LTR114TypeInfo.h"
#include "LTR114AdcConfig.h"
#include "ltr/include/ltr114api.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvConfig.h"


namespace LQMeas {
    const QString &LTR114Config::typeConfigName() {
        return LTR114TypeInfo::name();
    }

    const DevAdcConfig *LTR114Config::adcConfig() const {
        return m_adc.get();
    }

    const DevSyncMarksRecvConfig *LTR114Config::syncMarksRecvConfig() const {
        return m_syncRecv.get();
    }

    double LTR114Config::inPeakSpeed() const {
        /** @note в LTR114 в межкадровый интервал приходят фоновые измерения, поэтому
         *  поток считается от исходной частоты АЦП, а не частоты кадров */
        return m_adc->adcEnabledChCnt() > 0 ? m_adc->adcSampleSize() * m_adc->adcFreq() : 0;
    }

    LTR114Config::LTR114Config() :
        m_adc{new LTR114AdcConfig{LTR114TypeInfo::defaultTypeInfo()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this}} {

        init();
    }

    LTR114Config::LTR114Config(const LTR114Config &cfg) :
        m_adc{new LTR114AdcConfig{cfg.adc()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this, cfg.marksRecv()}} {

        init();
    }

    LTR114Config::~LTR114Config() {

    }

    void LTR114Config::protUpdate() {
        m_syncRecv->updateSyncMarksRecvFreq(m_adc->adcFreq());
    }
}
