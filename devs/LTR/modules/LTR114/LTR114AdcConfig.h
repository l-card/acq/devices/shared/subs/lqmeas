#ifndef LQMEAS_LTR114ADCCONFIG_H
#define LQMEAS_LTR114ADCCONFIG_H

#include "lqmeas/ifaces/in/adc/DevAdcSeqBaseConfig.h"
#include "ltr/include/ltr114api.h"

namespace LQMeas {
    class LTR114AdcConfig : public DevAdcSeqBaseConfig {
        Q_OBJECT
    public:
        static const int adc_channels_cnt {16};
        static const int adc_diff_channels_cnt {16};
        static const int adc_r_channels_cnt {8};

        enum class AdcChRange {
            U_10 = 0,
            U_2 = 1,
            U_0_4 = 2,

            R_4000 = 0,
            R_1200 = 1,
            R_400 = 2,
        };


        enum class AdcChMode {
            Diff,
            R,
            AlternatingR
        };


        enum class SyncMode {
            Internal,
            Master,
            ExternalSlave
        };

        enum class AutoCbrMode {
            OnStart,
            OnOperation
        };




        int adcUnitMode(int ch_num) const override;

        int adcChRangeNum(int ch_num) const override {return m_ch_params[ch_num].Range;}
        AdcChRange adcChRange(int ch_num) const {return static_cast<AdcChRange>(adcChRangeNum(ch_num));}
        AdcChMode adcChMode(int ch_num) const {return m_ch_params[ch_num].Mode;}

        bool adcExternalStart() const override {
            return m_syncMode != SyncMode::ExternalSlave;
        }

        int adcSampleSize() const override {return 8;}

        SyncMode  syncMode() const {return m_syncMode;}
        AutoCbrMode autoCbrMode() const {return m_autoCbrMode;}

        static int typeAdcRangeNum(AdcChRange range) {return static_cast<int>(range);}
    Q_SIGNALS:
        void syncModeChanged();
        void adcChModeChanged(int ch_num);
        void adcAutoCbrModeChanged();
    public Q_SLOTS:
        void setSyncMode(LTR114AdcConfig::SyncMode mode);
        void setAutoCbrMode(LTR114AdcConfig::AutoCbrMode cbrMode);
        void adcSetChMode(int ch_num, LTR114AdcConfig::AdcChMode mode);
        void adcSetChRangeNum(int ch_num, int range_num);
        void adcSetChRange(int ch_num, LTR114AdcConfig::AdcChRange range) {
            adcSetChRangeNum(ch_num, typeAdcRangeNum(range));
        }
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;

        void adcChSave(int ch_num, QJsonObject &chObj) const override;
        void adcChLoad(int ch_num, const QJsonObject &chObj) override;
        void adcChSetDefault(int ch_num) override;
        void adcAdjustFreq(double &adcFreq, double &adcChFreq) override;
    private:
        static INT adcChRangeCodeU(int range_num);
        static INT adcChRangeCodeR(int range_num);

        LTR114AdcConfig(const DevAdcInfo &info);
        LTR114AdcConfig(const LTR114AdcConfig &adcCfg);

        INT correctionModeCode() const;
        INT fillLChTable(TLTR114_LCHANNEL *ltable) const;

        SyncMode m_syncMode;
        AutoCbrMode m_autoCbrMode;
        struct {
            AdcChMode Mode;
            int Range;
        } m_ch_params[adc_channels_cnt];

        friend class LTR114Config;
        friend class LTR114;
    };
}

Q_DECLARE_METATYPE(LQMeas::LTR114AdcConfig::AdcChRange);
Q_DECLARE_METATYPE(LQMeas::LTR114AdcConfig::AdcChMode);
Q_DECLARE_METATYPE(LQMeas::LTR114AdcConfig::SyncMode);
Q_DECLARE_METATYPE(LQMeas::LTR114AdcConfig::AutoCbrMode);



#endif // LQMEAS_LTR114ADCCONFIG_H
