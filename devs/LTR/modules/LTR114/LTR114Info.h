#ifndef LQMEAS_LTR114INFO_H
#define LQMEAS_LTR114INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTR114TypeInfo.h"

namespace LQMeas {
    class LTR114Info : public DeviceInfo {
    public:
        LTR114Info(const LTR114TypeInfo &type = LTR114TypeInfo::defaultTypeInfo(),
                   const QString &serial = QString{}, int mcu_ver = 0, int ver_pld = 0);

        DeviceInfo *clone() const override;
        int mcuVer() const {return m_mcu_ver;}
        QString mcuVerStr() const;
        int pldVer() const {return m_pld_ver;}
        QString pldVerStr() const;


        const LTR114TypeInfo &devspecTypeInfo() const {
            return static_cast<const LTR114TypeInfo &>(type());
        }
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        LTR114Info(const LTR114Info &info);

        int m_mcu_ver;
        int m_pld_ver;
    };
}

#endif // LTR114INFO_H
