#ifndef LQMEAS_LTR114CONFIG_H
#define LQMEAS_LTR114CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"
#include <memory>

namespace LQMeas {
    class LTRSyncMarksRecvConfig;
    class LTR114AdcConfig;

    class LTR114Config : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const  override {return typeConfigName();}

        const DevAdcConfig *adcConfig() const override;
        const DevSyncMarksRecvConfig *syncMarksRecvConfig() const override;

        LTR114AdcConfig &adc() {return  *m_adc;}
        const LTR114AdcConfig &adc() const {return  *m_adc;}

        LTRSyncMarksRecvConfig &marksRecv() {return  *m_syncRecv;}
        const LTRSyncMarksRecvConfig &marksRecv() const {return  *m_syncRecv;}

        double inPeakSpeed() const override;

        DeviceConfig *clone() const override {
            return new LTR114Config{*this};
        }

        LTR114Config();
        LTR114Config(const LTR114Config &cfg);
        ~LTR114Config() override;
    protected:
        void protUpdate() override;
    private:
        const std::unique_ptr<LTR114AdcConfig> m_adc;
        const std::unique_ptr<LTRSyncMarksRecvConfig> m_syncRecv;
    };
}

#endif // LQMEAS_LTR114CONFIG_H
