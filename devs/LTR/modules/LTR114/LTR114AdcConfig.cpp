#include "LTR114AdcConfig.h"
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/StdConfigKeys.h"
#include "ltr/include/ltr114api.h"
#include "LTR114TypeInfo.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_sync_mode           {StdConfigKeys::syncMode()};
    static const QLatin1String cfgkey_sync_autocbr_mode   {"AutoCbrMode"};
    static const QLatin1String cfgkey_ch_range_num        {StdConfigKeys::rangeNum()};
    static const QLatin1String cfgkey_ch_mode             {StdConfigKeys::mode()};

    typedef EnumConfigKey<LTR114AdcConfig::SyncMode> SyncModeKey;
    static const SyncModeKey sync_modes[] = {
        {QStringLiteral("int"),         LTR114AdcConfig::SyncMode::Internal},
        {QStringLiteral("master"),      LTR114AdcConfig::SyncMode::Master},
        {QStringLiteral("extslave"),    LTR114AdcConfig::SyncMode::ExternalSlave},
        {SyncModeKey::defaultKey(),     LTR114AdcConfig::SyncMode::Internal},
    };

    typedef EnumConfigKey<LTR114AdcConfig::AdcChMode> AdcChModeKey;
    static const AdcChModeKey adc_ch_modes[] = {
        {QStringLiteral("diff"),        LTR114AdcConfig::AdcChMode::Diff},
        {QStringLiteral("r"),           LTR114AdcConfig::AdcChMode::R},
        {QStringLiteral("ravg"),        LTR114AdcConfig::AdcChMode::AlternatingR},
        {AdcChModeKey::defaultKey(),    LTR114AdcConfig::AdcChMode::Diff},
    };

    typedef EnumConfigKey<LTR114AdcConfig::AutoCbrMode> AutoCbrModeKey;
    static const AutoCbrModeKey auto_cbr_modes[] = {
        {QStringLiteral("start"),       LTR114AdcConfig::AutoCbrMode::OnStart},
        {QStringLiteral("op"),          LTR114AdcConfig::AutoCbrMode::OnOperation},
        {AutoCbrModeKey::defaultKey(),  LTR114AdcConfig::AutoCbrMode::OnStart}
    };

    LTR114AdcConfig::LTR114AdcConfig(const DevAdcInfo &info) : DevAdcSeqBaseConfig{info} {
        memset(m_ch_params, 0, sizeof(m_ch_params));
        m_syncMode = SyncMode::Internal;
        m_autoCbrMode = AutoCbrMode::OnStart;
    }

    LTR114AdcConfig::LTR114AdcConfig(const LTR114AdcConfig &adcCfg) : DevAdcSeqBaseConfig{adcCfg} {
        memcpy(m_ch_params, adcCfg.m_ch_params, sizeof(m_ch_params));
        m_syncMode = adcCfg.m_syncMode;
        m_autoCbrMode = adcCfg.m_autoCbrMode;
    }

    INT LTR114AdcConfig::correctionModeCode() const {
        return (m_autoCbrMode == LTR114AdcConfig::AutoCbrMode::OnOperation) ?
                    LTR114_CORRECTION_MODE_AUTO : LTR114_CORRECTION_MODE_INIT;
    }

    INT LTR114AdcConfig::fillLChTable(TLTR114_LCHANNEL *ltable) const {
        int lch_cnt {0};
        for (int ch {0}; ch < LTR114AdcConfig::adc_diff_channels_cnt; ++ch) {
            if (adcChEnabled(ch)) {
                if (ltable) {
                    INT measMode { adcChMode(ch) == LTR114AdcConfig::AdcChMode::Diff ?
                                   LTR114_MEASMODE_U : LTR114_MEASMODE_R};
                    int rangeNum { adcChRangeNum(ch) };

                    ltable[lch_cnt] = LTR114_CreateLChannel(measMode, ch, measMode == LTR114_MEASMODE_U ?
                                                            adcChRangeCodeU(rangeNum) : adcChRangeCodeR(rangeNum));
                }
                ++lch_cnt;

                if (adcChMode(ch) == LTR114AdcConfig::AdcChMode::AlternatingR) {
                    if (ltable) {
                        ltable[lch_cnt] = LTR114_CreateLChannel(LTR114_MEASMODE_NR, ch, adcChRangeCodeR(adcChRangeNum(ch)));
                    }
                    ++lch_cnt;
                }
            }
        }
        return lch_cnt;
    }


    int LTR114AdcConfig::adcUnitMode(int ch) const     {
        return ch == LTR114TypeInfo::adcChNumT ? 0 : (m_ch_params[ch].Mode == AdcChMode::Diff ?
                                                          LTR114TypeInfo::adcUnitModeU :
                                                          LTR114TypeInfo::adcUnitModeR);
    }

    void LTR114AdcConfig::adcSetChMode(int ch_num, LTR114AdcConfig::AdcChMode mode) {
        if (m_ch_params[ch_num].Mode != mode) {
            m_ch_params[ch_num].Mode = mode;
            notifyChRangeChanged(ch_num);
            notifyChModeChanged(ch_num);
            notifyConfigChanged();
        }
    }

    void LTR114AdcConfig::adcSetChRangeNum(int ch_num, int range_num) {
        if (m_ch_params[ch_num].Range != range_num){
            m_ch_params[ch_num].Range = range_num;
            notifyChRangeChanged(ch_num);
            notifyConfigChanged();
        }
    }

    void LTR114AdcConfig::setSyncMode(LTR114AdcConfig::SyncMode mode) {
        if (m_syncMode != mode) {
            m_syncMode = mode;
            Q_EMIT syncModeChanged();
            notifyConfigChanged();
        }
    }

    void LTR114AdcConfig::setAutoCbrMode(LTR114AdcConfig::AutoCbrMode cbrMode) {
        if (m_autoCbrMode != cbrMode) {
            m_autoCbrMode = cbrMode;
            Q_EMIT adcAutoCbrModeChanged();
            notifyConfigChanged();
        }
    }

    void LTR114AdcConfig::protSave(QJsonObject &cfgObj) const     {
        cfgObj[cfgkey_sync_mode] = SyncModeKey::getKey(sync_modes, m_syncMode);
        cfgObj[cfgkey_sync_autocbr_mode] = AutoCbrModeKey::getKey(auto_cbr_modes, m_autoCbrMode);
        DevAdcSeqBaseConfig::protSave(cfgObj);
    }

    void LTR114AdcConfig::protLoad(const QJsonObject &cfgObj) {
        m_syncMode = SyncModeKey::getValue(sync_modes, cfgObj[cfgkey_sync_mode].toString());
        m_autoCbrMode = AutoCbrModeKey::getValue(auto_cbr_modes, cfgObj[cfgkey_sync_autocbr_mode].toString());
        DevAdcSeqBaseConfig::protLoad(cfgObj);
    }

    void LTR114AdcConfig::adcChSave(int ch_num, QJsonObject &chObj) const {
        chObj[cfgkey_ch_range_num] = m_ch_params[ch_num].Range;
        chObj[cfgkey_ch_mode] = AdcChModeKey::getKey(adc_ch_modes, m_ch_params[ch_num].Mode);
    }

    void LTR114AdcConfig::adcChLoad(int ch_num, const QJsonObject &chObj) {
        const int range {chObj[cfgkey_ch_range_num].toInt()};
        if ((range >= 0) || (range < adcInfo().adcChRangesCnt(ch_num, adcUnitMode(ch_num)))) {
            m_ch_params[ch_num].Range = range;
        } else {
            m_ch_params[ch_num].Range = 0;
        }
        m_ch_params[ch_num].Mode = AdcChModeKey::getValue(
                    adc_ch_modes, chObj[cfgkey_ch_mode].toString());
    }

    void LTR114AdcConfig::adcChSetDefault(int ch) {
        m_ch_params[ch].Mode = AdcChMode::Diff;
        m_ch_params[ch].Range = 0;
    }

    void LTR114AdcConfig::adcAdjustFreq(double &adcFreq, double &adcChFreq) {
        LTR114_FindFreqDivider(adcFreq, nullptr, &adcFreq);
        LTR114_FindInterval(adcChFreq, adcFreq, fillLChTable(nullptr),
                            correctionModeCode(),
                            nullptr, &adcChFreq);
    }

    INT LTR114AdcConfig::adcChRangeCodeU(int range_num) {
        return  range_num == typeAdcRangeNum(AdcChRange::U_10) ? LTR114_URANGE_10 :
                             typeAdcRangeNum(AdcChRange::U_2)  ? LTR114_URANGE_2 :
                                                                 LTR114_URANGE_04;
    }

    INT LTR114AdcConfig::adcChRangeCodeR(int range_num) {
        return  range_num == typeAdcRangeNum(AdcChRange::R_4000) ? LTR114_RRANGE_4000 :
                             typeAdcRangeNum(AdcChRange::R_1200) ? LTR114_RRANGE_1200 :
                                                                   LTR114_RRANGE_400;
    }

}

