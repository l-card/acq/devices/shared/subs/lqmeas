#ifndef LQMEAS_LTR114_H
#define LQMEAS_LTR114_H



#include "ltr/include/ltr114api.h"
#include "lqmeas/devs/LTR/modules/LTRModule.h"
#include "lqmeas/ifaces/in/adc/DevAdc.h"
#include <memory>

namespace LQMeas {
    class LTR114Config;
    class LTR114Info;
    class LTRModuleFrameReceiver;

    class LTR114 : public LTRModule, public DevAdc {
        Q_OBJECT
    public:
        static const quint16 typeModuleID {LTR_MID_LTR114};
        ~LTR114() override;

        bool isOpened() const override;
        /* ----------------------- интерфейс ввода  --------------------------*/
        DevAdc *devAdc() override {return static_cast<DevAdc*>(this);}
        DevSyncMarksRecv *devSyncMarkRecv() override;

        QString errorString(int err) const override;

        bool adcIsRunning() const override;

        const LTR114Config &devspecConfig() const;
        QSharedPointer<const LTR114Info> devspecInfo() const;
    protected:
        TLTR* channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn,  int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;

        INT ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) override;

        void protAdcPreStart(LQError &err) override;
        void protAdcStart(LQError &err) override;
        void protAdcStop(LQError &err) override;
        void protAdcGetData(double *data, int size, unsigned flags,
                             unsigned tout, int &recvd_size, LQError &err) override;
    private:
        explicit LTR114(LTRCrate *crate, int slot, QObject *parent = nullptr);



        mutable TLTR114 m_hnd;
        const std::unique_ptr<LTRModuleFrameReceiver> m_receiver;

        friend class LTRResolver;

        INT m_cor_mode;
    };
}

#endif // LQMEAS_LTR114_H
