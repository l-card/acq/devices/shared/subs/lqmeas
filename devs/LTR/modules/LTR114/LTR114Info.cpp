#include "LTR114Info.h"
#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String &cfgkey_mcu_ver              {StdConfigKeys::mcuVer()};
    static const QLatin1String &cfgkey_pld_ver              {StdConfigKeys::pldVer()};

    LTR114Info::LTR114Info(const LTR114TypeInfo &type, const QString &serial, int mcu_ver, int pld_ver) :
        DeviceInfo{type, serial}, m_mcu_ver{mcu_ver}, m_pld_ver{pld_ver} {

    }

    DeviceInfo *LTR114Info::clone() const {
        return new LTR114Info{*this};
    }

    QString LTR114Info::mcuVerStr() const {
        return QString("%1.%2").arg((m_mcu_ver >> 8) & 0xFF).arg(m_mcu_ver & 0xFF);
    }

    QString LTR114Info::pldVerStr() const {
        return QString::number(m_pld_ver);
    }

    void LTR114Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_mcu_ver] = m_mcu_ver;
        infoObj[cfgkey_pld_ver] = m_pld_ver;
    }

    void LTR114Info::protLoad(const QJsonObject &infoObj) {
        m_mcu_ver = infoObj[cfgkey_mcu_ver].toInt();
        m_pld_ver = infoObj[cfgkey_pld_ver].toInt();
    }

    LTR114Info::LTR114Info(const LTR114Info &info) : DeviceInfo(info),
        m_mcu_ver{info.m_mcu_ver}, m_pld_ver{info.m_pld_ver} {
    }
}
