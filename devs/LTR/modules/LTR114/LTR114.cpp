#include "LTR114.h"
#include "LTR114Info.h"
#include "LTR114Config.h"
#include "LTR114AdcConfig.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "lqmeas/devs/LTR/modules/LTRModuleFrameReceiver.h"
#include "lqmeas/log/Log.h"
#include "lqmeas/StdErrors.h"

namespace LQMeas {
    LTR114::LTR114(LTRCrate *crate, int slot, QObject *parent) :
        LTRModule{crate, slot, typeModuleID, new LTR114Config{}, LTR114Info{}, parent},
        DevAdc{this, LTR114AdcConfig::adc_channels_cnt},
        m_receiver{new LTRModuleFrameReceiver{this}},
        m_cor_mode{LTR114_CORRECTION_MODE_INIT} {

        LTR114_Init(&m_hnd);
    }


    LTR114::~LTR114() {
        LTR114_Close(&m_hnd);
    }

    bool LTR114::isOpened() const {
        return LTR114_IsOpened(&m_hnd) == LTR_OK;
    }

    DevSyncMarksRecv *LTR114::devSyncMarkRecv() {
        return m_receiver.get();
    }

    QString LTR114::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTR114_GetErrorString(err));
    }

    bool LTR114::adcIsRunning() const {
        return m_hnd.Active != FALSE;
    }

    const LTR114Config &LTR114::devspecConfig() const {
        return static_cast<const LTR114Config &>(config());
    }

    QSharedPointer<const LTR114Info> LTR114::devspecInfo() const {
        return devInfo().staticCast<const LTR114Info>();
    }

    TLTR *LTR114::channel() const {
        return &m_hnd.Channel;
    }

    int LTR114::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        Q_UNUSED(flags)
        int ltr_err = LTR114_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, slot);
        if (ltr_err == LTR_OK) {
            ltr_err = LTR114_GetConfig(&m_hnd);
        }
        if (ltr_err == LTR_OK) {
            setDeviceInfo(LTR114Info(LTR114TypeInfo::defaultTypeInfo(),
                                     QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                     m_hnd.ModuleInfo.VerMCU, m_hnd.ModuleInfo.VerPLD));
        }
        return ltr_err;
    }

    void LTR114::protClose(LQError &err) {
        getError(LTR114_Close(&m_hnd), err);
    }

    void LTR114::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTR114Config *setCfg {qobject_cast<const LTR114Config *>(&cfg)};
        if (setCfg) {
            m_cor_mode = setCfg->adc().correctionModeCode();
            m_hnd.LChQnt = setCfg->adc().fillLChTable(m_hnd.LChTbl);
            LTR114_FindFreqDivider(setCfg->adc().adcFreq(), &m_hnd.FreqDivider, nullptr);
            LTR114_FindInterval(setCfg->adc().adcChFreq(), setCfg->adc().adcFreq(), m_hnd.LChQnt,
                                m_cor_mode, &m_hnd.Interval, nullptr);

            m_hnd.SyncMode = setCfg->adc().syncMode() == LTR114AdcConfig::SyncMode::Internal ? LTR114_SYNCMODE_INTERNAL :
                             setCfg->adc().syncMode() == LTR114AdcConfig::SyncMode::Master ? LTR114_SYNCMODE_MASTER :
                                                                                           LTR114_SYNCMODE_EXTERNAL;
            getError(LTR114_SetADC(&m_hnd), err);
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }

    INT LTR114::ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) {
        return LTR114_Recv(&m_hnd, wrds, marks, size, tout);
    }

    void LTR114::protAdcPreStart(LQError &err) {
        getError(LTR114_Calibrate(&m_hnd), err);
    }

    void LTR114::protAdcStart(LQError &err) {
        getError(LTR114_Start(&m_hnd), err);
        if (err.isSuccess()) {
            m_receiver->start(m_hnd.FrameLength, 2);
        }
    }

    void LTR114::protAdcStop(LQError &err) {
        getError(LTR114_Stop(&m_hnd), err);
        m_receiver->clear();
    }

    void LTR114::protAdcGetData(double *data, int size, unsigned flags,
                                unsigned tout, int &recvd_size, LQError &err) {
        int recvd_words {0};
        DWORD *wrds;
        INT proc_size {0};
        int frames_cnt {size/adcConfig().adcEnabledChCnt()};


        m_receiver->getFrames(frames_cnt * static_cast<int>(m_hnd.FrameLength),
                              tout, wrds, recvd_words, err);

        if (err.isSuccess() && (recvd_words != 0)) {
            proc_size = static_cast<INT>(recvd_words);

            getError(LTR114_ProcessData(&m_hnd, wrds, data, &proc_size, m_cor_mode,
                                        LTR114_PROCF_VALUE | LTR114_PROCF_AVGR), err);
            if (!err.isSuccess()) {
                LQMeasLog->error(tr("Process data error"), err, this);
            }
        }

        if (err.isSuccess()) {
            recvd_size = proc_size;
        }
    }
}
