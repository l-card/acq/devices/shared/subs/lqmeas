#include "LTRModule.h"
#include "LTRModuleRawWordsTracker.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/devs/LTR/LTRNativeError.h"
#include "lqmeas/devs/LTR/service/LTRServiceRef.h"
#include "lqmeas/devs/LTR/service/LTRServiceInfo.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/devs/DeviceConfig.h"
#include "lqmeas/devs/LTR/LTRModuleRef.h"
#include "lqmeas/devs/DeviceInfo.h"
#include <QCoreApplication>

namespace LQMeas {
    LTRModule::LTRModule(LTRCrate *crate,
                         int slot, unsigned mid, DeviceConfig *defaultCfg,
                         const DeviceInfo &info, QObject *parent) :
        Device{defaultCfg, info, LQMeas::DeviceInterfaces::ltrModule(), parent},
        m_svcRef{new LTRServiceRef{crate->serviceRef()}},
        m_crateRef{crate->createRef()},
        m_slot{slot},
        m_mid{mid} {

        connect(crate, &LTRCrate::configChanged, this, &LTRModule::setParentConfig);
        setParentConfig(crate->configPtr());
    }

    void LTRModule::protOpen(OpenFlags flags, LQError &err) {
        int err_code = ltrModuleOpen(
            serviceRef().ltrdIPv4Addr(),
            serviceRef().ltrdTcpPort(),
            QSTRING_TO_CSTR(ltrCrateRef().devInfo().serial()),
            slot(),
            flags);
        if (err_code != LTR_OK) {
            /* В случае ошибки открытия принудительно закрываем соединение, так как
             * при ошибоках, которые все еще позволяют работать с соединением, оно
             * не закрывается автоматом.
             * Но для флага Force при таких ошибках оставляем соединение открытым
             * для дальнейшей работы, за исключением WARNING_MODULE_IN_USE,
             * т.к. если модуль кем то используется, нормально с ним работать нельзя */
            if (!(flags & OpenFlag::Force) || (err_code == LTR_WARNING_MODULE_IN_USE)) {
                LQError close_err;
                protClose(close_err);
            }
            getError(err_code, err);
        }
    }


    LTRModule::~LTRModule() {
        delete m_crateRef;
        delete m_svcRef;
    }


    void LTRModule::getError(int err_code, LQError &err) const {
        if (err_code != LTR_OK) {
            err = error(err_code);
        }
    }

    LQError LTRModule::error(int err_code) const {
        return err_code == LTR_OK ? LQError::Success() : LTRNativeError::error(err_code, errorString(err_code));
    }

    void LTRModule::setRawWordsTracker(LTRModuleRawWordsTracker *tracker) {
        m_rawWordsTracker = tracker;
    }

    QString LTRModule::errorString(int err) const {
        return  QSTRING_FROM_CSTR(LTR_GetErrorString(err));
    }


    INT LTRModule::ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) {
        return LTR_Recv(channel(), wrds, marks, size, tout);
    }

    INT LTRModule::ltrRawWordsSend(const DWORD *wrds, DWORD size, DWORD tout) {
        return LTR_Send(channel(), wrds, size, tout);
    }

    void LTRModule::rawWordsWithMraksReceive(DWORD *wrds, DWORD *marks, int size,
                                             unsigned tout, int &recvd_size, LQError &err) {
        INT recv_res {ltrRawWordsReceive(wrds, marks, static_cast<DWORD>(size), tout)};
        if (recv_res >= 0) {
            recvd_size = recv_res;
            if (m_rawWordsTracker)
                m_rawWordsTracker->procRecvdRawWords(wrds, marks, recvd_size);
        } else {
            recvd_size = 0;
            getError(recv_res, err);
        }
    }


    void LTRModule::rawWordsSend(const DWORD *wrds, int size, unsigned tout,
                                 int &sent_size, LQError &err) {
        int send_res {ltrRawWordsSend(wrds, static_cast<DWORD>(size), tout)};
        if (send_res >= 0) {
            sent_size = send_res;
            if (m_rawWordsTracker)
                m_rawWordsTracker->procSentRawWords(wrds, sent_size);
        } else {
            sent_size = 0;
            getError(send_res, err);
        }
    }

    void LTRModule::updateCrateInfo(const DeviceInfo &crateInfo, const LTRServiceInfo &svcInfo) {
        m_crateRef->setInfo(crateInfo);
        m_svcRef->setInfo(svcInfo);
    }


    DeviceRef *LTRModule::createRef() const {
        return new LTRModuleRef{*devInfo(), ltrCrateRef(), m_slot};
    }

}


