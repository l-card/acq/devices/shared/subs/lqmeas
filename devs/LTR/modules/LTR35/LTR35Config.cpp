#include "LTR35Config.h"
#include "LTR35TypeInfo.h"
#include "LTR35OutConfig.h"
#include "LTR35SyncConfig.h"
#include "LTR35InConfig.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvConfig.h"

namespace LQMeas {
    const QString &LQMeas::LTR35Config::typeConfigName() {
        return LTR35TypeInfo::name();
    }

    const DevOutConfig *LTR35Config::outConfig() const {
        return  m_out.get();
    }

    const DevInDigConfig *LTR35Config::inDigConfig() const {
        return m_din.get();
    }

    const DevOutSyncConfig *LTR35Config::outSyncConfig() const {
        return  m_out.get();
    }

    const DevSyncMarksRecvConfig *LTR35Config::syncMarksRecvConfig() const  {
        return m_syncRecv.get();
    }

    LTR35Config::LTR35Config() :
        m_out{new LTR35OutConfig{*this, LTR35TypeInfo::defaultTypeInfo()}},
        m_din{new LTR35InConfig{*this, LTR35TypeInfo::defaultTypeInfo()}},
        m_sync{new LTR35SyncConfig{}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this}} {

        addManualConfigIface(m_sync.get());
        init();
    }

    LTR35Config::LTR35Config(const LTR35Config &cfg) :
        m_out{new LTR35OutConfig{*this, cfg.out()}},
        m_din{new LTR35InConfig{*this, cfg.din()}},
        m_sync{new LTR35SyncConfig{cfg.sync()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this, cfg.marksRecv()}} {

        addManualConfigIface(m_sync.get());
        init();
    }

    LTR35Config::~LTR35Config() {

    }

    void LTR35Config::protUpdate() {
        if (din().inDigChsAvailable() && din().inDigSyncModeEnabled()) {
            m_syncRecv->updateSyncMarksRecvFreq(din().inDigFreq()/din().inDigPackedModeWordBitCount());
        } else if (out().outSyncEchoEnabled()) {
            m_syncRecv->updateSyncMarksRecvFreq(out().outSyncGenFreq());
        } else {
            m_syncRecv->updateSyncMarksRecvFreq(0);
        }
    }
}
