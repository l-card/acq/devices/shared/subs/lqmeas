#ifndef LQMEAS_LTR35SYNCCONFIG_H
#define LQMEAS_LTR35SYNCCONFIG_H

#include "lqmeas/devs/DeviceConfigIface.h"

namespace LQMeas {
    class LTR35SyncConfig : public DeviceConfigIface {
        Q_OBJECT
    public:
        QString cfgIfaceName() const override {return  QStringLiteral("LTR35Sync");}

        enum class SyncStartMode {
            Internal = 0,
            Master,
            Slave
        };

        enum class SyncSlaveSrc {
            DI2_Rise = 0,
            DI2_Fall
        };

        SyncStartMode syncStartMode() const {return params.startMode;}
        void setSyncStartMode(SyncStartMode mode);

        SyncSlaveSrc syncSlaveSource() const {return params.slaveSrc;}
        void setSyncSlaveSource(SyncSlaveSrc src);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        LTR35SyncConfig();
        LTR35SyncConfig(const LTR35SyncConfig &cfg);

        struct {
            SyncStartMode startMode;
            SyncSlaveSrc  slaveSrc;
        } params;

        friend class LTR35Config;
    };
}

Q_DECLARE_METATYPE(LQMeas::LTR35SyncConfig::SyncStartMode);
Q_DECLARE_METATYPE(LQMeas::LTR35SyncConfig::SyncSlaveSrc);

#endif // LQMEAS_LTR35SYNCCONFIG_H
