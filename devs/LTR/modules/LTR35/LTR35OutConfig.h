#ifndef LQMEAS_LTR35OUTCONFIG_H
#define LQMEAS_LTR35OUTCONFIG_H

#include "lqmeas/ifaces/out/DevOutSyncConfig.h"

namespace LQMeas {
    class LTR35Config;

    class LTR35OutConfig: public DevOutSyncConfig {
        Q_OBJECT
    public:
        enum class HardSrcOutput {
            Sin = 0,
            Cos
        };

        enum class OutDataFormat {
            Unspecified = 0, /* Формат явно не определен. Определятся
                                         автоматически по режиму */
            Format_20,
            Format_24
        };

        enum class OutStdGenFreq {
            Freq_192K = 0,
            Freq_96K,
            Freq_48K
        };


        bool outSyncExternalStart() const override;

        bool outSyncGenModeEnabled() const {return m_params.syncMode;}
        void outSetSyncGenModeEnabled(bool en);

        void outSyncSetManualGenFreq(double val);
        double outSyncManualGenFreq() const {return m_params.outFreq.manual_val;}

        void outSyncSetStdGenFreq(OutStdGenFreq freq);
        OutStdGenFreq outSyncStdGenFreq() const {return m_params.outFreq.std_val;}

        void outSyncSetManualGenFreqEnabled(bool en);
        bool outSyncManualGenFreqEnabled() const {return m_params.outFreq.manual_en;}

        double outSyncConfiguredGenFreq() const;
        double outSyncGenFreq() const override {return m_results.out_freq;}

        void outDacSetChRangeNum(int ch, int range);
        int outDacChRangeNum(int ch) const override {return m_params.ch[ch].range;}

        bool outDacChSyncModeEnabled(int ch) const  override { Q_UNUSED(ch) return m_params.syncMode;}
        bool outDigChSyncModeEnabled(int ch) const override { Q_UNUSED(ch) return m_params.syncMode;}


        void outDacChSetHardSourceNumber(int ch, int src_num);
        void outDacChSetHardSourceOutput(int ch, HardSrcOutput out);

        int  outDacChHardSourceNumber(int ch) const {return m_params.ch[ch].hard_src_num;}
        HardSrcOutput  outDacChHardSourceOutput(int ch) const {return m_params.ch[ch].hard_src_out;}


        OutDataFormat outDacDataFormat() const {return m_params.dacDataFmt;}
        void outDacSetDataFormat(OutDataFormat fmt);

        bool outSyncEchoEnabled() const override {return m_params.echoEnabled;}
        void outSyncSetEchoEnabled(bool en);

        int outSyncEchoChannel() const {return m_params.echoChannel;}
        void outSyncSetEchoChannel(int ch);

        bool outDacGenRawCodeMode() const {return  m_params.dacRawCodeMode;}
        void outDacSetGenRawCodeMode(bool en);

        bool outDacAfcCorEnabled() const {return m_params.dacAfcCorEn;}
        void outDacSetAfcCorEnabled(bool en);

        bool outSyncPeriodicStatusEnabled() const {return m_params.periodicStatusEn;}
        void outSyncSetPeriodicStatusEnabled(bool en);

        static constexpr bool defaultOutSyncManualGenFreqEnabled() {return false;}
        static constexpr double defaultOutGenFreqValue() {return 192000;}
        static constexpr OutStdGenFreq defaultOutSyncStdGenFreq() {return OutStdGenFreq::Freq_192K;}

        static const int max_dac_ch_cnt {8};

        static int typeStdGenFreqNum(OutStdGenFreq freq) { return static_cast<int>(freq);}
        static double typeStdGenFreqVal(OutStdGenFreq freq);

        int outSyncDacSampleSize() const override {return outDacDataFormat() == OutDataFormat::Format_24 ? 8 : 4;}
        int outSyncDigSampleSize() const override {return 4;}
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
        void protUpdate() override;
    private:


        friend class LTR35Config;
        LTR35OutConfig(const LTR35Config &ltr35cfg, const DevOutInfo &info);
        LTR35OutConfig(const LTR35Config &ltr35cfg, const LTR35OutConfig &outCfg);


        struct {
            struct {
                int range;
                int  hard_src_num;
                HardSrcOutput hard_src_out;
            } ch[max_dac_ch_cnt];

            struct {
                bool manual_en;
                double manual_val;
                OutStdGenFreq std_val;
            } outFreq;

            OutDataFormat dacDataFmt;
            bool syncMode;
            bool dacRawCodeMode;
            bool dacAfcCorEn;
            bool periodicStatusEn;
            bool echoEnabled;
            int echoChannel;
        } m_params;


        struct {
            double out_freq;
        } m_results;

        const LTR35Config *m_ltr35cfg;
    };
}

Q_DECLARE_METATYPE(LQMeas::LTR35OutConfig::HardSrcOutput);
Q_DECLARE_METATYPE(LQMeas::LTR35OutConfig::OutDataFormat);
Q_DECLARE_METATYPE(LQMeas::LTR35OutConfig::OutStdGenFreq);

#endif // LQMEAS_LTR35OUTCONFIG_H

