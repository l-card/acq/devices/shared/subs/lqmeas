#ifndef LQMEAS_LTR35TYPEINFO_H
#define LQMEAS_LTR35TYPEINFO_H

#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"
#include "lqmeas/ifaces/out/DevOutInfo.h"
#include "lqmeas/ifaces/in/dig/DevInDigInfo.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvInfo.h"

namespace LQMeas {
    class LTR35TypeInfo : public LTRModuleTypeInfo, public DevOutInfo, public DevInDigInfo, public LTRSyncMarksRecvInfo {
    public:
        static const QString &name();
        QString deviceTypeName() const override {return name();}
        QString deviceModificationName() const override;

        static const LTR35TypeInfo &defaultTypeInfo();
        static const LTR35TypeInfo &typeInfoMod1();
        static const LTR35TypeInfo &typeInfoMod1_8();
        static const LTR35TypeInfo &typeInfoMod1_4();
        static const LTR35TypeInfo &typeInfoMod2();
        static const LTR35TypeInfo &typeInfoMod2_4();
        static const LTR35TypeInfo &typeInfoMod2_8();
        static const LTR35TypeInfo &typeInfoMod3();


        /* ----------------- DevOutInfo -------------------------------------*/
        const DevOutInfo *out() const override {return static_cast<const DevOutInfo *>(this);}
        const DevInDigInfo *digin() const override {return static_cast<const DevInDigInfo *>(this);}
        const DevSyncMarksRecvInfo *syncMarkRecv() const override {
            return static_cast<const DevSyncMarksRecvInfo *>(this);
        }

        bool outDacSyncSupport() const override {return outDacChannelsCnt() != 0;}
        bool outDigSyncSupport() const override {return true;}

        bool outDacAsyncSupport() const override {return outDacChannelsCnt() != 0;}
        bool outDigAsyncSupport() const override {return true;}

        bool outDacSyncModeCfgPerCh() const override {return false;}

        bool outSyncRamModeIsConfigurable() const override {return true;}
        bool outSyncDacChGenModeIsConfigurable() const override {return true;}
        bool outSyncDigChGenModeIsConfigurable() const override {return false;}


        double outSyncGenFreqMax() const override;
        bool outSyncPresetSupport() const override {return false;}

        int outDacChannelsCnt() const override {return m_dac_ch_cnt;}


        /* --------------- DevInDigInfo --------------------------------------*/
        int inDigChannelsCnt() const override {return 2;}
        bool inDigSyncSupport(void) const override {return true;}
        double inDigSyncFreqMax(void) const override;

        QList<const DeviceTypeInfo *> modificationList() const override;
        QList<const DeviceTypeInfo *> visibleModificationList() const override;

        virtual int modificationNum() const = 0;

        static double typeStdGenFreqVal(int idx);
        static int typeStdGenFreqsCnt();
    protected:
        LTR35TypeInfo(int dac_ch_cnt);
    private:
        int m_dac_ch_cnt;
    };
}

#endif // LQMEAS_LTR35TYPEINFO_H
