#ifndef LQMEAS_LTR35CONFIG_H
#define LQMEAS_LTR35CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"
#include <memory>

namespace LQMeas {
    class LTR35OutConfig;
    class LTR35SyncConfig;
    class LTR35InConfig;
    class LTRSyncMarksRecvConfig;

    class LTR35Config : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        const DevOutConfig *outConfig() const override;
        const DevInDigConfig *inDigConfig() const override;
        const DevOutSyncConfig *outSyncConfig() const override;
        const DevSyncMarksRecvConfig *syncMarksRecvConfig() const override;

        LTR35OutConfig &out() {return  *m_out;}
        const LTR35OutConfig &out() const {return  *m_out;}

        LTR35InConfig &din() {return *m_din;}
        const LTR35InConfig &din() const {return *m_din;}

        LTR35SyncConfig &sync() {return  *m_sync;}
        const LTR35SyncConfig &sync() const {return  *m_sync;}

        LTRSyncMarksRecvConfig &marksRecv() {return  *m_syncRecv;}
        const LTRSyncMarksRecvConfig &marksRecv() const {return  *m_syncRecv;}

        DeviceConfig *clone() const override {
            return new LTR35Config{*this};
        }

        LTR35Config();
        LTR35Config(const LTR35Config &cfg);
        ~LTR35Config() override;
    protected:
        void protUpdate() override;
    private:
        const std::unique_ptr<LTR35OutConfig> m_out;
        const std::unique_ptr<LTR35InConfig> m_din;
        const std::unique_ptr<LTR35SyncConfig> m_sync;
        const std::unique_ptr<LTRSyncMarksRecvConfig> m_syncRecv;

    };
}

#endif // LQMEAS_LTR35CONFIG_H
