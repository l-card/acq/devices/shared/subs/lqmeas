#include "LTR35InConfig.h"
#include "LTR35OutConfig.h"
#include "LTR35Config.h"
#include "lqmeas/ifaces/in/dig/DevInDigInfo.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdConfigKeys.h"
#include "ltr/include/ltr35api.h"
#include <QJsonObject>
#include <QJsonArray>

namespace LQMeas {
    LTR35InConfig::LTR35InConfig(LTR35Config &devCfg, const LQMeas::DevInDigInfo &info) :
        DevInDigSyncBaseConfig{info}, m_devCfg{&devCfg} {
    }

    LTR35InConfig::LTR35InConfig(LTR35Config &devCfg, const LTR35InConfig &inCfg) :
        DevInDigSyncBaseConfig{inCfg}, m_devCfg{&devCfg} {

    }

    bool LTR35InConfig::inDigChsAvailable() const {
        const LTR35OutConfig &outCfg {m_devCfg->out()};
        return (!outCfg.outSyncGenModeEnabled() || (outCfg.outSyncRamMode() == DevOutSyncConfig::OutSyncRamMode::Stream)) &&
                !outCfg.outSyncEchoEnabled();
    }


    bool LTR35InConfig::inDigSyncExternalStart() const {
        const LTR35OutConfig &outCfg {m_devCfg->out()};
        return outCfg.outSyncExternalStart();
    }

    int LTR35InConfig::inDigPackedModeWordBitCount() const {
        return inDigEnabledSyncChCnt() == 2 ? 12 : 24;
    }

    int LTR35InConfig::inDigPackedModeWordBitOffset(int ch_num, int bitnum) const  {
        /* биты упкаованы в одном слове 24 бита:
         * - если разрешен только один вход, то все 24 бита соответствуют этому одному входу
         * - если разрешено два входа, то первый занимает младшие 12 бит, второй - старшие.
         * при этом саммый ранний бит от входа всегда в старшем разраде из битов данного входа */
        return (inDigEnabledSyncChCnt() == 2 ? ((ch_num == 0) ? 11 : 23) : 23) - bitnum;
    }

    void LTR35InConfig::dinAdjustFreq(double &dinFreq) {

        const LTR35OutConfig &outCfg {m_devCfg->out()};

        TLTR35_CONFIG rawCfg;
        memset(&rawCfg, 0, sizeof(rawCfg));
        rawCfg.InStream.InStreamMode = LTR35_IN_STREAM_MODE_DI;
        rawCfg.InStream.DIChEnMask = rawInChMask();

        LTR35_FillOutFreq(&rawCfg, outCfg.outSyncGenFreq(), nullptr);
        LTR35_FillDIAcqFreq(&rawCfg, dinFreq, &dinFreq);
    }

    quint8 LTR35InConfig::rawInChMask() const {
        return (inDigChSyncModeEnabled(0) ? LTR35_IN_STREAM_DI1 : 0) |
               (inDigChSyncModeEnabled(1) ? LTR35_IN_STREAM_DI2 : 0);
    }
}
