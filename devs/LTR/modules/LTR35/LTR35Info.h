#ifndef LQMEAS_LTR35INFO_H
#define LQMEAS_LTR35INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTR35TypeInfo.h"

namespace LQMeas {
    class LTR35Info : public DeviceInfo {
    public:
        explicit LTR35Info(const LTR35TypeInfo &type = LTR35TypeInfo::defaultTypeInfo(),
                           const QString &serial = QString{}, int fpga_ver = 0, int pld_ver = 0);
        LTR35Info(const LTR35Info &info);

        int pldVer() const {return m_pld_ver;}
        int fpgaVer() const {return m_fpga_ver;}

        QString pldVerStr() const;
        QString fpgaVerStr() const;

        DeviceInfo *clone() const override {return new LTR35Info{*this};}
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        int m_fpga_ver;
        int m_pld_ver;
    };
}

#endif // LQMEAS_LTR35INFO_H
