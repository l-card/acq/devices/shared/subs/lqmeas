#include "LTR35SyncConfig.h"
#include "lqmeas/EnumConfigKey.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_syncstart_mode    {"StartMode"};
    static const QLatin1String cfgkey_sync_slave_src    {"SlaveSrc"};


    typedef EnumConfigKey<LTR35SyncConfig::SyncStartMode> LTR35SyncStartKey;
    static const LTR35SyncStartKey sync_start_modes[] = {
        {QStringLiteral("int"),              LTR35SyncConfig::SyncStartMode::Internal},
        {QStringLiteral("master"),           LTR35SyncConfig::SyncStartMode::Master},
        {QStringLiteral("slave"),            LTR35SyncConfig::SyncStartMode::Slave},
        {LTR35SyncStartKey::defaultKey(),    LTR35SyncConfig::SyncStartMode::Internal}
    };

    typedef EnumConfigKey<LTR35SyncConfig::SyncSlaveSrc> LTR35SyncSlaveSrcKey;
    static const LTR35SyncSlaveSrcKey sync_slave_srcs[] = {
        {QStringLiteral("di2_rise"),           LTR35SyncConfig::SyncSlaveSrc::DI2_Rise},
        {QStringLiteral("di2_fall"),           LTR35SyncConfig::SyncSlaveSrc::DI2_Fall},
        {LTR35SyncSlaveSrcKey::defaultKey(),   LTR35SyncConfig::SyncSlaveSrc::DI2_Rise}
    };


    void LTR35SyncConfig::setSyncStartMode(LTR35SyncConfig::SyncStartMode mode) {
        if (params.startMode != mode) {
            params.startMode = mode;
            notifyConfigChanged();
        }
    }

    void LTR35SyncConfig::setSyncSlaveSource(LTR35SyncConfig::SyncSlaveSrc src) {
        if (params.slaveSrc != src) {
            params.slaveSrc = src;
            notifyConfigChanged();
        }
    }

    void LTR35SyncConfig::protSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_syncstart_mode] = LTR35SyncStartKey::getKey(sync_start_modes, params.startMode);
        cfgObj[cfgkey_sync_slave_src] = LTR35SyncSlaveSrcKey::getKey(sync_slave_srcs, params.slaveSrc);
    }

    void LTR35SyncConfig::protLoad(const QJsonObject &cfgObj) {
        params.startMode = LTR35SyncStartKey::getValue(sync_start_modes, cfgObj[cfgkey_syncstart_mode].toString());
        params.slaveSrc = LTR35SyncSlaveSrcKey::getValue(sync_slave_srcs, cfgObj[cfgkey_sync_slave_src].toString());
    }

    LTR35SyncConfig::LTR35SyncConfig() {
        params.startMode = SyncStartMode::Internal;
        params.slaveSrc = SyncSlaveSrc::DI2_Rise;
    }

    LTR35SyncConfig::LTR35SyncConfig(const LTR35SyncConfig &cfg) {
        params = cfg.params;
    }
}
