#include "LTR35TypeInfo.h"
#include "ltr/include/ltr35api.h"
#include <QStringBuilder>

namespace LQMeas {

    static const struct {
        double max;
        double min;
    } f_mod1_ranges[] {{10.,-10.}, {1, -1}};

    static const struct {
        double max;
        double min;
    } f_mod2_ranges[] {{20.,-2.}};

    static const double f_std_gen_freqs[] {192000, 96000, 48000};


    const QString &LTR35TypeInfo::name()  {
        static const QString str {QStringLiteral("LTR35") };
        return str;
    }

    class LTR35TypeInfoM1 : public LTR35TypeInfo {
    public:
        LTR35TypeInfoM1(int dac_ch_cnt)  : LTR35TypeInfo{dac_ch_cnt} {}

        int modificationNum() const override {return 1;}
        int outDigChannelsCnt() const override {return 8;}
        int outDacChRangesCnt(int ch) const override {Q_UNUSED(ch) return sizeof(f_mod1_ranges)/sizeof(f_mod1_ranges[0]);}
        double outDacChRangeMaxVal(int ch, int range) const override {Q_UNUSED(ch) return f_mod1_ranges[range].max;}
        double outDacChRangeMinVal(int ch, int range) const override {Q_UNUSED(ch) return f_mod1_ranges[range].min;}
    };

    class LTR35TypeInfoM2 : public LTR35TypeInfo {
    public:
        LTR35TypeInfoM2(int dac_ch_cnt)  : LTR35TypeInfo{dac_ch_cnt} {}

        int modificationNum() const override {return 2;}
        int outDigChannelsCnt() const override {return 8;}
        int outDacChRangesCnt(int ch) const override {Q_UNUSED(ch) return sizeof(f_mod2_ranges)/sizeof(f_mod2_ranges[0]);}
        double outDacChRangeMaxVal(int ch, int range) const override {Q_UNUSED(ch) return f_mod2_ranges[range].max;}
        double outDacChRangeMinVal(int ch, int range) const override {Q_UNUSED(ch) return f_mod2_ranges[range].min;}
    };

    class LTR35TypeInfoM3 : public LTR35TypeInfo {
    public:
        LTR35TypeInfoM3()  : LTR35TypeInfo{0} {}

        int modificationNum() const override {return 3;}
        int outDigChannelsCnt() const override {return 16;}
        int outDacChRangesCnt(int ch) const override {Q_UNUSED(ch) return 0;}
        double outDacChRangeMaxVal(int ch, int range) const override {Q_UNUSED(ch) Q_UNUSED(range) return 0;}
        double outDacChRangeMinVal(int ch, int range) const override {Q_UNUSED(ch) Q_UNUSED(range) return 0;}
    };





    QString LTR35TypeInfo::deviceModificationName() const {
        return outDacChannelsCnt() > 0 ?
                    QString{"%1-%2-%3"}.arg(deviceTypeName()).arg(modificationNum()).arg(outDacChannelsCnt()) :
                    QString{"%1-%2"}.arg(deviceTypeName()).arg(modificationNum());
    }
    
    const LTR35TypeInfo &LTR35TypeInfo::defaultTypeInfo() {
        return typeInfoMod1_8();
    }

    const LTR35TypeInfo &LTR35TypeInfo::typeInfoMod1() {
        static const LTR35TypeInfoM1 info{0};
        return info;
    }

    const LTR35TypeInfo &LTR35TypeInfo::typeInfoMod1_4() {
        static const LTR35TypeInfoM1 info{4};
        return info;
    }

    const LTR35TypeInfo &LTR35TypeInfo::typeInfoMod1_8() {
        static const LTR35TypeInfoM1 info{8};
        return info;
    }

    const LTR35TypeInfo &LTR35TypeInfo::typeInfoMod2() {
        static const LTR35TypeInfoM2 info{0};
        return info;
    }

    const LTR35TypeInfo &LTR35TypeInfo::typeInfoMod2_4() {
        static const LTR35TypeInfoM2 info{4};
        return info;
    }

    const LTR35TypeInfo &LTR35TypeInfo::typeInfoMod2_8() {
        static const LTR35TypeInfoM2 info{8};
        return info;
    }

    const LTR35TypeInfo &LTR35TypeInfo::typeInfoMod3() {
        static const LTR35TypeInfoM3 info{};
        return info;
    }

    double LTR35TypeInfo::outSyncGenFreqMax() const {
        return LTR35_DAC_FREQ_MAX;
    }

    double LTR35TypeInfo::inDigSyncFreqMax() const {
        return LTR35_SYNT_FREQ_STD / LTR35_DIN_SYNT_FREQ_DIV;
    }

    QList<const DeviceTypeInfo *> LTR35TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list {            
            &typeInfoMod1_8(),
            &typeInfoMod1_4(),
            &typeInfoMod1(),
            &typeInfoMod2_8(),
            &typeInfoMod2_4(),
            &typeInfoMod2(),
            &typeInfoMod3()
        };
        return list;
    }

    QList<const DeviceTypeInfo *> LTR35TypeInfo::visibleModificationList() const {
        static const QList<const DeviceTypeInfo *> list {
            &typeInfoMod1_8(),
            &typeInfoMod1_4(),
            &typeInfoMod2_8(),
            &typeInfoMod3()
        };
        return list;
    }

    double LTR35TypeInfo::typeStdGenFreqVal(int idx) {
        return f_std_gen_freqs[idx];
    }

    int LTR35TypeInfo::typeStdGenFreqsCnt() {
        return sizeof(f_std_gen_freqs)/sizeof(f_std_gen_freqs[0]);
    }

    LTR35TypeInfo::LTR35TypeInfo(int dac_ch_cnt) : m_dac_ch_cnt{dac_ch_cnt} {
    }
}



