#include "LTR35Info.h"
#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String cfgkey_pld_ver           {StdConfigKeys::pldVer()};
    static const QLatin1String cfgkey_fpga_ver          {StdConfigKeys::fpgaVer()};


    LTR35Info::LTR35Info(const LTR35TypeInfo &type, const QString &serial,
                         int fpga_ver, int pld_ver) :
        DeviceInfo{type, serial}, m_fpga_ver{fpga_ver}, m_pld_ver{pld_ver} {

    }

    LTR35Info::LTR35Info(const LTR35Info &info) : DeviceInfo{info},
        m_fpga_ver{info.m_fpga_ver}, m_pld_ver{info.m_pld_ver} {

    }

    QString LTR35Info::pldVerStr() const {
        return QString::number(m_pld_ver);
    }

    QString LTR35Info::fpgaVerStr() const {
        return QString::number(m_fpga_ver);
    }

    void LTR35Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_pld_ver] = m_pld_ver;
        infoObj[cfgkey_fpga_ver] = m_fpga_ver;
    }

    void LTR35Info::protLoad(const QJsonObject &infoObj) {
        m_pld_ver = infoObj[cfgkey_pld_ver].toInt();
        m_fpga_ver = infoObj[cfgkey_fpga_ver].toInt();
    }
}

