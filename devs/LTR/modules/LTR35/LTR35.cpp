#include "LTR35.h"
#include "LTR35Config.h"
#include "LTR35OutConfig.h"
#include "LTR35InConfig.h"
#include "LTR35SyncConfig.h"
#include "LTR35Info.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdErrors.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/devs/LTR/modules/LTRModuleFrameReceiver.h"

#include "lqmeas/ifaces/out/Signals/Analog/Sin/AnalogSignalSinConfig.h"
#include "lqmeas/ifaces/out/Signals/Analog/Const/AnalogSignalConstConfig.h"
#include "lqmeas/ifaces/out/SyncModes/stream/DevOutSyncModeImplStream.h"
#include "lqmeas/ifaces/out/SyncModes/cycle/DevOutSyncModeImplCycle.h"

namespace LQMeas {
    LTR35::LTR35(LTRCrate *crate, int slot, QObject *parent) :
        LTRModule{crate, slot, typeModuleID, new LTR35Config{}, LTR35Info{}, parent},
        DevInSyncDig{this},
        DevOutSync{this},
        DevOutAsyncDac{this},
        DevOutDacConverter{this},
        m_sender{this},
        m_receiver{new LTRModuleFrameReceiver{this}} {

        LTR35_Init(&m_hnd);

        outSyncAddMode(new DevOutSyncModeImplCycle{this, this});
        outSyncAddMode(new DevOutSyncModeImplStream{this, this});

        for (int hard_src_idx {0}; hard_src_idx < hard_sources_cnt; ++hard_src_idx) {
            m_hard_src_gen[hard_src_idx].freq = 0;
            m_hard_src_gen[hard_src_idx].phase = 0;
        }
    }

    INT LTR35::ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) {
        INT ret = LTR_OK;
        /* мы не должны начинать принимать слова, пока не дождемся прихода ответа
         * на запуск потока (иначе примем этот ответ).
         * При этом ожидание запуска генерации может быть из другого потока, чем
         * прием данных */
        bool is_running {false};
        {
            LQError err;
            QMutexLocker lock{&m_streamLock};
            is_running = streamStartWait(tout, err);
        }

        if (is_running) {
            ret = LTR35_RecvInStreamData(&m_hnd, reinterpret_cast<INT*>(wrds),
                                         marks, size, tout);
        }
        return ret;
    }

    INT LTR35::ltrRawWordsSend(const DWORD *wrds, DWORD size, DWORD tout) {
        return LTR35_Send(&m_hnd, wrds, size, tout);
    }

    void LTR35::outSyncEchoTrackStart(LQError &err) {
        Q_UNUSED(err)
        m_echo_pos = 0;
        m_receiver->start(1, 1);
    }

    void LTR35::outSyncEchoTrackStop(LQError &err) {
        Q_UNUSED(err)
    }

    void LTR35::outSyncEchoTrackWait(quint64 wt_pos, unsigned tout, quint64 &fnd_pos, LQError &err) {
        if (m_echo_pos < wt_pos) {
            DWORD *frame;
            int recv = static_cast<int>(wt_pos - m_echo_pos);
            int recvd;
            m_receiver->getFrames(recv*m_receiver->frameSize(),
                                      tout, frame, recvd, err);
            if (err.isSuccess()) {
                m_echo_pos += static_cast<quint64>(recvd);
            }
        }

        fnd_pos = m_echo_pos;
    }

    void LTR35::protInAsyncDig(unsigned &val, LQError &err) {
        BYTE di_vals;
        getError(LTR35_DIAsyncIn(&m_hnd, &di_vals), err);
        if (err.isSuccess())
            val = di_vals;
    }

    void LTR35::protInDigStart(LQError &err) {
        QMutexLocker lock{&m_streamLock};
        streamStartReq(err, m_dinRunning);
        m_receiver->start(1, 1);
    }

    void LTR35::protInDigStop(LQError &err) {
        QMutexLocker lock{&m_streamLock};
        streamStop(default_stop_tout, m_dinRunning, err);
    }

    void LTR35::protInDigGetData(quint32 *data, int size, unsigned tout, int &recvd_size, LQError &err) {
        DWORD *frame;
        m_receiver->getFrames(size, tout, frame, recvd_size, err);
        if (err.isSuccess() && (recvd_size > 0)) {
            memcpy(data, frame, recvd_size * sizeof(frame[0]));
        }
    }

    LTR35::~LTR35() {
        LTR35_Close(&m_hnd);
    }

    bool LTR35::isOpened() const {
        return LTR35_IsOpened(&m_hnd) == LTR_OK;
    }

    DevSyncMarksRecv *LTR35::devSyncMarkRecv() {
        return m_receiver.get();
    }

    QString LTR35::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTR35_GetErrorString(err));
    }

    const LTR35Config &LTR35::devspecConfig() const {
        return static_cast<const LTR35Config &>(config());
    }

    QSharedPointer<const LTR35Info> LTR35::devspecInfo() const {
        return devInfo().staticCast<const LTR35Info>();
    }

    void LTR35::setHardGenParams(int src_num, double freq, double phase, LQError &err) {
        m_hard_src_gen[src_num].freq = freq;
        m_hard_src_gen[src_num].phase = phase;
        if (outSyncGenRunning()) {
            TLTR35_ARITH_PHASE delta;
            LTR35_FillArithPhaseDegree(&delta, 360*freq/(config().outSyncConfig()->outSyncGenFreq()), NULL);
            getError(LTR35_SetArithSrcDelta(&m_hnd, src_num & 0xFF, &delta), err);
        }
    }

    TLTR *LTR35::channel() const {
        return &m_hnd.Channel;
    }

    int LTR35::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        Q_UNUSED(flags)

        INT ltr_err = LTR35_Open(&m_hnd,ltrd_ip, ltrd_port, crateSn, static_cast<WORD>(slot));
        if (LTR35_IsOpened(&m_hnd) == LTR_OK) {
            const LTR35TypeInfo *type_info {&LTR35TypeInfo::defaultTypeInfo()};
            switch (m_hnd.ModuleInfo.Modification) {
            case LTR35_MOD_1:
                type_info = m_hnd.ModuleInfo.DacChCnt == 8 ? &LTR35TypeInfo::typeInfoMod1_8() :
                                m_hnd.ModuleInfo.DacChCnt == 4 ? &LTR35TypeInfo::typeInfoMod1_4() :
                                &LTR35TypeInfo::typeInfoMod1();
                break;
            case LTR35_MOD_2:
                type_info = m_hnd.ModuleInfo.DacChCnt == 8 ? &LTR35TypeInfo::typeInfoMod2_8() :
                                m_hnd.ModuleInfo.DacChCnt == 4 ? &LTR35TypeInfo::typeInfoMod2_4() :
                                &LTR35TypeInfo::typeInfoMod2();
                break;
            case LTR35_MOD_3:
                type_info = &LTR35TypeInfo::typeInfoMod3();
                break;
            }
            setDeviceInfo(LTR35Info{*type_info, QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                    m_hnd.ModuleInfo.VerFPGA, m_hnd.ModuleInfo.VerPLD});

            memset(m_last_async_vals, 0, sizeof(m_last_async_vals));
            m_last_async_dout = 0;
            m_dinRunning = m_outRunning = false;
            m_stream_state = StreamState::Stopped;
        }
        return ltr_err;
    }


    void LTR35::protClose(LQError &err) {
        getError(LTR35_Close(&m_hnd), err);
    }

    void LTR35::protConfigure(const DeviceConfig &cfg, LQError &err)  {
        const LTR35Config *setCfg {qobject_cast<const LTR35Config *>(&cfg)};
        if (setCfg) {
            const int dac_ch_cnt {devTypeInfo().out()->outDacChannelsCnt()};
            double dac_freq {setCfg->out().outSyncGenFreq()};

            if (m_hnd.State.RunState != LTR35_RUNSTATE_STOPPED)
                LTR35_Stop(&m_hnd, 0);


            if (setCfg->out().outSyncGenModeEnabled()) {
                if (setCfg->out().outSyncRamMode() == DevOutSyncConfig::OutSyncRamMode::Stream) {
                    m_hnd.Cfg.OutDataFmt = LTR35_OUTDATA_FORMAT_20;
                    m_hnd.Cfg.OutMode = LTR35_OUT_MODE_STREAM;
                } else {
                    m_hnd.Cfg.OutDataFmt = LTR35_OUTDATA_FORMAT_24;
                    m_hnd.Cfg.OutMode = LTR35_OUT_MODE_CYCLE;
                }
            } else {
                m_hnd.Cfg.OutDataFmt = LTR35_OUTDATA_FORMAT_24;
                m_hnd.Cfg.OutMode = LTR35_OUT_MODE_STREAM;
            }

            if (setCfg->out().outDacDataFormat() != LTR35OutConfig::OutDataFormat::Unspecified) {
                if (setCfg->out().outDacDataFormat() == LTR35OutConfig::OutDataFormat::Format_20) {
                    m_hnd.Cfg.OutDataFmt = LTR35_OUTDATA_FORMAT_20;
                } else if (setCfg->out().outDacDataFormat() == LTR35OutConfig::OutDataFormat::Format_24) {
                    m_hnd.Cfg.OutDataFmt = LTR35_OUTDATA_FORMAT_24;
                }
            }

            m_hnd.Cfg.Flags = 0;
            if (!setCfg->out().outDacAfcCorEnabled())
                m_hnd.Cfg.Flags |= LTR35_CFG_FLAG_DISABLE_AFC_COR;
            if (!setCfg->out().outSyncPeriodicStatusEnabled())
                m_hnd.Cfg.Flags |= LTR35_CFG_FLAG_DISABLE_PERIODIC_STATUS;


            m_hnd.Cfg.Sync.SyncMode = !setCfg->out().outSyncExternalStart() ? LTR35_SYNC_MODE_INTERNAL :
                                   setCfg->sync().syncStartMode() == LTR35SyncConfig::SyncStartMode::Master ? LTR35_SYNC_MODE_MASTER :
                                   setCfg->sync().syncStartMode() == LTR35SyncConfig::SyncStartMode::Slave ? LTR35_SYNC_MODE_SLAVE :
                                                                                                  LTR35_SYNC_MODE_INTERNAL;
            m_hnd.Cfg.Sync.SlaveSrc = setCfg->sync().syncSlaveSource() == LTR35SyncConfig::SyncSlaveSrc::DI2_Fall ?
                        LTR35_SYNC_SLAVE_SRC_DI2_FALL : LTR35_SYNC_SLAVE_SRC_DI2_RISE;

            if (err.isSuccess()) {
                getError(LTR35_FillOutFreq(&m_hnd.Cfg, dac_freq, &dac_freq), err);
            }

            if (err.isSuccess()) {
                if (setCfg->din().inDigChsAvailable() && setCfg->din().inDigSyncModeEnabled()) {
                    m_hnd.Cfg.InStream.InStreamMode = LTR35_IN_STREAM_MODE_DI;
                    m_hnd.Cfg.InStream.DIChEnMask = setCfg->din().rawInChMask();
                    LTR35_FillDIAcqFreq(&m_hnd.Cfg, setCfg->din().inDigFreq(), nullptr);
                }  else if (setCfg->out().outSyncEchoEnabled()) {
                    m_hnd.Cfg.InStream.InStreamMode = LTR35_IN_STREAM_MODE_CH_ECHO;
                    m_hnd.Cfg.InStream.EchoChannel = static_cast<BYTE>(setCfg->out().outSyncEchoChannel());
                } else {
                    m_hnd.Cfg.InStream.InStreamMode = LTR35_IN_STREAM_MODE_OFF;
                }
            }


            for (int hard_src_idx {0}; hard_src_idx < hard_sources_cnt; ++hard_src_idx) {
                LTR35_FillArithPhaseDegree(&m_hnd.Cfg.ArithSrc[hard_src_idx].Delta, 360*m_hard_src_gen[hard_src_idx].freq/dac_freq, NULL);
                LTR35_FillArithPhaseDegree(&m_hnd.Cfg.ArithSrc[hard_src_idx].Phase, m_hard_src_gen[hard_src_idx].phase, NULL);
            }


            if (err.isSuccess()) {
                for (int ch {0}; (ch < dac_ch_cnt) && err.isSuccess(); ++ch) {
                    m_hnd.Cfg.Ch[ch].Output = static_cast<BYTE>(setCfg->out().outDacChRangeNum(ch));

                    if (!setCfg->out().outSyncGenModeEnabled() || setCfg->out().outSyncDacChEnabled(ch)) {
                        m_hnd.Cfg.Ch[ch].Enabled = 1;
                        BYTE src = LTR35_CH_SRC_SDRAM;
                        INT ltr_err = LTR_OK;


                        if (setCfg->out().outSyncDacChGenMode(ch) == DevOutSyncConfig::OutSyncChGenMode::Hard) {
                            int src_num {setCfg->out().outDacChHardSourceNumber(ch)};
                            const LTR35OutConfig::HardSrcOutput out {setCfg->out().outDacChHardSourceOutput(ch)};
                            if (src_num < hard_sources_cnt) {
                                if (out == LTR35OutConfig::HardSrcOutput::Sin) {
                                    src = static_cast<BYTE>(LTR35_CH_SRC_SIN1 + 2 * src_num);
                                } else if (out == LTR35OutConfig::HardSrcOutput::Cos) {
                                    src = static_cast<BYTE>(LTR35_CH_SRC_COS1 + 2 * src_num);
                                } else {
                                    ltr_err = LTR35_ERR_INVALID_CH_SOURCE;
                                }
                            } else {
                                ltr_err = LTR35_ERR_INVALID_CH_SOURCE;
                            }

                            if (ltr_err == LTR_OK) {
                                if (!getHardAmp(setCfg, ch, outSyncDacGenSignalMutable(ch).data(),
                                                m_hnd.Cfg.Ch[ch].ArithAmp,
                                                m_hnd.Cfg.Ch[ch].ArithOffs)) {
                                    err = StdErrors::UnsupportedSignalType();
                                }
                            } else {
                                getError(ltr_err, err);
                            }
                        }

                        m_hnd.Cfg.Ch[ch].Source = src;
                    } else {
                        m_hnd.Cfg.Ch[ch].Enabled  = 0;
                    }
                }

                if (err.isSuccess()) {
                    getError(LTR35_Configure(&m_hnd), err);
                }
            }
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }

    void LTR35::privOutSyncSendData(const double *dac_data, int dac_size,
                                    const unsigned *dig_data, int dig_size,
                                    unsigned flags, unsigned tout, LQError &err) {
        Q_UNUSED(flags)

        if (m_sender.unsentWordsCnt()) {
            m_sender.flushData(tout, err);
        }

        if (err.isSuccess()) {
            if (m_sender.unsentWordsCnt()!=0) {
                err = StdErrors::SendBusy();
            } else {
                DWORD snd_dac_req_size {static_cast<DWORD>(dac_size)};
                DWORD snd_dig_req_size {static_cast<DWORD>(dig_size)};
                DWORD snd_size {(snd_dac_req_size  + snd_dig_req_size) *
                               (m_hnd.Cfg.OutDataFmt == LTR35_OUTDATA_FORMAT_20 ? 1 : 2)};
                QScopedArrayPointer<DWORD> wrds {new DWORD[static_cast<size_t>(snd_size)]};

                getError(LTR35_PrepareData(&m_hnd, dac_data, &snd_dac_req_size,
                                           reinterpret_cast<const DWORD*>(dig_data), &snd_dig_req_size,
                                           devspecConfig().out().outDacGenRawCodeMode() ?
                                               0 : LTR35_PREP_FLAGS_VOLT, wrds.data(), &snd_size), err);

                if (!err.isSuccess()) {
                    LQMeasLog->error(tr("Prepare data error"), err, this);
                } else {
                    m_sender.setFrameSize(static_cast<int>(snd_size));
                    m_sender.putFrames(wrds.data(), static_cast<int>(snd_size), tout, nullptr, err);
                }
            }
        }
    }

    bool LTR35::privOutSyncHasDefferedData() const {
        return m_sender.unsentWordsCnt() > 0;
    }

    void LTR35::privOutSyncTryFlushData(unsigned tout, LQError &err) {
        m_sender.flushData(tout, err);
    }

    void LTR35::privOutSyncSendFinish(LQError &err) {
        Q_UNUSED(err)
        m_sender.setFrameSize(0);
    }

    bool LTR35::privOutSyncUnderflowOccured() const {
        return false;
    }

    int LTR35::outCycleGenMaxSize(const OutRamSignalGenerator &generator) const {
        const int chCnt {generator.dacSignalsCount() + (generator.digSignalsCount() == 0 ? 0 : 1)};
        return chCnt == 0 ? 0 : LTR35_MAX_POINTS_PER_PAGE/chCnt;
    }

    void LTR35::privOutCycleLoadStart(const OutRamSignalGenerator &generator, int size, LQError &err) {
        Q_UNUSED(size)
        if (generator.digSignalsCount() == 0) {
            const unsigned dig_data {LTR35_DIGOUT_WORD_DIS_H | LTR35_DIGOUT_WORD_DIS_L};
            privOutSyncSendData(nullptr, 0, &dig_data, 1, 0, 1000, err);
        }
    }

    void LTR35::privOutCycleLoadFinish(LQError &err) {
        Q_UNUSED(err)
    }
    
    void LTR35::privOutCycleGenStartRequest(LQError &err) {
        QMutexLocker lock{&m_streamLock};
        protOutSyncHardGenUpdate(err);
        if (err.isSuccess()) {
            getError(LTR35_SwitchCyclePageRequest(&m_hnd, 0), err);
        }
        if (err.isSuccess()) {
            m_stream_state = StreamState::StartAckWaiting;
            m_outRunning = true;
        }
    }

    bool LTR35::privOutCycleGenStartWaitDone(unsigned tout, LQError &err) {
        QMutexLocker lock{&m_streamLock};
        bool ok {false};
        INT ltr_err {LTR35_SwitchCyclePageWaitDone(&m_hnd, tout)};
        if (ltr_err == LTR_OK) {
            ok = true;
            m_stream_state = StreamState::Running;
        } else if (ltr_err != LTR_ERROR_OP_DONE_WAIT_TOUT) {
            getError(ltr_err, err);
        }
        return ok;
    }

    void LTR35::privOutCycleGenUpdateRequest(LQError &err) {
        return privOutCycleGenStartRequest(err);
    }

    bool LTR35::privOutCycleGenUpdateWaitDone(unsigned tout, LQError &err)  {
        return privOutCycleGenStartWaitDone(tout, err);
    }
    
    void LTR35::privOutCycleGenStopRequest(unsigned tout, LQError &err) {
        genStop(tout, err);
    }
    
    void LTR35::privOutCycleGenStop(LQError &err) {
        Q_UNUSED(err)
    }

    void LTR35::privOutStreamInit(LQError &err) {
        Q_UNUSED(err)
    }


    void LTR35::privOutStreamStartRequest(LQError &err) {
        QMutexLocker lock{&m_streamLock};
        streamStartReq(err, m_outRunning);
    }

    bool LTR35::privOutStreamStartWaitDone(unsigned tout, LQError &err) {
        QMutexLocker lock{&m_streamLock};
        return streamStartWait(tout, err);
    }

    void LTR35::privOutStreamStop(unsigned tout, LQError &err) {
        return genStop(tout, err);
    }

    void LTR35::protOutSyncHardOnlyGenStartRequest(LQError &err) {
        return privOutCycleGenStartRequest(err);
    }

    bool LTR35::protOutSyncHardOnlyGenStartWaitDone(unsigned tout, LQError &err) {
        return privOutCycleGenStartWaitDone(tout, err);
    }

    void LTR35::protOutSyncHardOnlyGenStop(unsigned tout, LQError &err) {
        return genStop(tout, err);
    }

    void LTR35::protOutSyncHardGenUpdate(LQError &err) {
        const int dac_ch_cnt {devTypeInfo().out()->outDacChannelsCnt()};
        for (BYTE ch {0}; (ch < dac_ch_cnt) && err.isSuccess(); ++ch) {
            if (m_hnd.Cfg.Ch[ch].Enabled && (config().outSyncConfig()->outSyncDacChGenMode(ch) ==
                                             DevOutSyncConfig::OutSyncChGenMode::Hard)) {
                double amp, offs;

                if (getHardAmp(&static_cast<const LTR35Config &>(config()), ch,
                               outSyncDacGenSignalMutable(ch).data(), amp, offs)) {
                    getError(LTR35_SetArithAmp(&m_hnd, ch, amp, offs), err);
                }
            }
        }
    }

    void LTR35::protOutAsyncDac(int ch, double val, LQError &err) {
        m_last_async_vals[ch] = val;
        protOutAsyncCur(err);
    }

    void LTR35::protOutAsyncDig(unsigned val, unsigned mask, LQError &err) {
        m_last_async_dout = (m_last_async_dout & mask) | (val & ~mask);
        protOutAsyncCur(err);
    }

    void LTR35::protOutAsyncCur(LQError &err) {
        privOutSyncSendData(m_last_async_vals, m_hnd.ModuleInfo.DacChCnt,
                            &m_last_async_dout, 1, 0, 0, err);
        if (err.isSuccess())
            privOutSyncFlushData(100, err);
        if (err.isSuccess() && !m_hnd.State.Run) {
            getError(LTR35_StreamStart(&m_hnd, 0), err);
        }
    }

    void LTR35::streamStartReq(LQError &err, bool &runFlag) {
        if (m_stream_state == StreamState::Stopped) {
            protOutSyncHardGenUpdate(err);
            if (err.isSuccess()) {
                getError(LTR35_StreamStartRequest(&m_hnd, 0), err);
            }
            if (err.isSuccess()) {
                m_stream_state = StreamState::StartAckWaiting;
            }
        }
        if (err.isSuccess()) {
            runFlag = true;
        }
    }

    bool LTR35::streamStartWait(unsigned tout, LQError &err)  {
        bool ok {false};
        if (m_stream_state == StreamState::StartAckWaiting) {
            INT ltr_err {LTR35_StreamStartWaitDone(&m_hnd, tout)};
            if (ltr_err == LTR_OK) {
                ok = true;
                m_stream_state = StreamState::Running;
            } else if (ltr_err != LTR_ERROR_OP_DONE_WAIT_TOUT) {
                getError(ltr_err, err);
            }
        } else if (m_stream_state == StreamState::Running) {
            ok = true;
        }
        return ok;
    }

    void LTR35::streamStop(unsigned tout, bool &runFlag, LQError &err)  {
        runFlag = false;
        if ((m_stream_state != StreamState::Stopped) && !m_dinRunning && !m_outRunning) {
            getError(LTR35_StopWithTout(&m_hnd, 0, tout), err);
            if (err.isSuccess()) {
                m_stream_state = StreamState::Stopped;
            }
        }
    }


    void LTR35::genStop(unsigned tout, LQError &err) {
        if (m_sender.unsentWordsCnt()) {
            m_sender.flushData(tout, err);
            m_sender.clearData();
        }
        QMutexLocker lock{&m_streamLock};
        streamStop(tout, m_outRunning, err);
    }

    bool LTR35::getHardAmp(const LTR35Config *cfg, int ch, SignalConfig *sig, double &amp, double &offs) {
        bool ok { false };
        AnalogSignalSinConfig *sin_cfg {qobject_cast<AnalogSignalSinConfig *>(sig)};
        if (sin_cfg) {
            amp = sin_cfg->amplitude();
            offs = sin_cfg->offset();
            if (cfg)
                sin_cfg->setRealParams(m_hard_src_gen[cfg->out().outDacChHardSourceNumber(ch)].freq, 0);
            ok = true;
        } else {
            AnalogSignalConstConfig *const_cfg {qobject_cast<AnalogSignalConstConfig *>(sig)};
            if (const_cfg) {
                amp = 0;
                offs = const_cfg->amplitude();
                ok = true;
            }
        }
        return  ok;
    }
}

