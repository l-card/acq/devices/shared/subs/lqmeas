#include "LTR35OutConfig.h"
#include "LTR35Config.h"
#include "LTR35SyncConfig.h"
#include "LTR35TypeInfo.h"
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/StdConfigKeys.h"
#include "ltr/include/ltr35api.h"
#include "lqmeas/lqtdefs.h"
#include <QJsonObject>
#include <QJsonArray>

namespace LQMeas {

    static const QLatin1String cfgkey_out_freq_obj          {StdConfigKeys::outFreq()};
    static const QLatin1String cfgkey_out_freq_std_val      {"StdVal"};
    static const QLatin1String cfgkey_out_freq_manual_val   {"ManualVal"};
    static const QLatin1String cfgkey_out_freq_manual_en    {"ManualEn"};
    static const QLatin1String cfgkey_sync_mode_en          {StdConfigKeys::syncModeEn()};
    static const QLatin1String cfgkey_ch_range_num          {StdConfigKeys::rangeNum()};
    static const QLatin1String cfgkey_ch_hard_src_num       {"HardSrcNum"};
    static const QLatin1String cfgkey_ch_hard_src_out       {"HardSrcOut"};
    static const QLatin1String cfgkey_dac_channels_arr      {StdConfigKeys::dacChannels()};
    static const QLatin1String cfgkey_dac_data_fmt          {StdConfigKeys::dacDataFmt()};
    static const QLatin1String cfgkey_dac_raw_code_mode     {"DacRawCodeMode"};
    static const QLatin1String cfgkey_dac_afc_cor_en        {"AfcCorEn"};
    static const QLatin1String cfgkey_periodic_status_en    {"PeriodicStatusEn"};

    static const QLatin1String cfgkey_echo_obj              {"Echo"};
    static const QLatin1String cfgkey_echo_en               {StdConfigKeys::enabled()};
    static const QLatin1String cfgkey_echo_channel          {StdConfigKeys::channel()};
    static const QLatin1String cfgkey_dev_obj               {StdConfigKeys::dev()};


    typedef EnumConfigKey<LTR35OutConfig::HardSrcOutput> HardSrcOutKey;
    static const HardSrcOutKey hard_outputs[] = {
        {QStringLiteral("sin"),                 LTR35OutConfig::HardSrcOutput::Sin},
        {QStringLiteral("cos"),                 LTR35OutConfig::HardSrcOutput::Cos},
        {HardSrcOutKey::defaultKey(),           LTR35OutConfig::HardSrcOutput::Sin}
    };

    typedef EnumConfigKey<LTR35OutConfig::OutDataFormat> LTR35DacDataFmtKey;
    static const LTR35DacDataFmtKey dac_data_fmts[] = {
        {QStringLiteral("20"),                  LTR35OutConfig::OutDataFormat::Format_20},
        {QStringLiteral("24"),                  LTR35OutConfig::OutDataFormat::Format_24},
        {LTR35DacDataFmtKey::defaultKey(),      LTR35OutConfig::OutDataFormat::Unspecified},
    };

    typedef EnumConfigKey<LTR35OutConfig::OutStdGenFreq> LTR35OutStdGenFreqKey;
    static const LTR35OutStdGenFreqKey out_std_gen_freqs[] = {
        {QStringLiteral("192K"),                LTR35OutConfig::OutStdGenFreq::Freq_192K},
        {QStringLiteral("96K"),                 LTR35OutConfig::OutStdGenFreq::Freq_96K},
        {QStringLiteral("48K"),                 LTR35OutConfig::OutStdGenFreq::Freq_48K},
        {LTR35OutStdGenFreqKey::defaultKey(),   LTR35OutConfig::OutStdGenFreq::Freq_192K},
    };




    bool LTR35OutConfig::outSyncExternalStart() const {return outSyncGenModeEnabled() &&
                (m_ltr35cfg->sync().syncStartMode() != LTR35SyncConfig::SyncStartMode::Internal);}

    void LTR35OutConfig::outSetSyncGenModeEnabled(bool en) {
        if (m_params.syncMode != en) {
            m_params.syncMode = en;
            notifyDacChSyncEnableStateChanged(-1);
            notifyConfigChanged();
        }
    }


    void LTR35OutConfig::outSyncSetManualGenFreq(double val) {
        if (!LQREAL_IS_EQUAL(m_params.outFreq.manual_val, val)) {
            m_params.outFreq.manual_val = val;
            if (m_params.outFreq.manual_en) {
                notifyDacChFreqChanged(-1);
            }
            notifyConfigChanged();
        }
    }

    void LTR35OutConfig::outSyncSetStdGenFreq(LTR35OutConfig::OutStdGenFreq freq) {
        if (m_params.outFreq.std_val != freq) {
            m_params.outFreq.std_val = freq;
            if (!m_params.outFreq.manual_en) {
                notifyDacChFreqChanged(-1);
            }
            notifyConfigChanged();
        }
    }

    void LTR35OutConfig::outSyncSetManualGenFreqEnabled(bool en) {
        if (m_params.outFreq.manual_en != en) {
            m_params.outFreq.manual_en = en;
            notifyDacChFreqChanged(-1);
            notifyConfigChanged();
        }
    }

    double LTR35OutConfig::outSyncConfiguredGenFreq() const {
        return outSyncManualGenFreqEnabled() ? outSyncManualGenFreq() : typeStdGenFreqVal(outSyncStdGenFreq());
    }

    void LTR35OutConfig::outDacSetChRangeNum(int ch, int range) {
        if (m_params.ch[ch].range != range) {
            m_params.ch[ch].range = range;
            notifyDacChRangeChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR35OutConfig::outDacChSetHardSourceNumber(int ch, int src_num)     {
        if (m_params.ch[ch].hard_src_num != src_num) {
            m_params.ch[ch].hard_src_num = src_num;
            notifyConfigChanged();
        }
    }

    void LTR35OutConfig::outDacChSetHardSourceOutput(int ch, LTR35OutConfig::HardSrcOutput out) {
        if (m_params.ch[ch].hard_src_out != out) {
            m_params.ch[ch].hard_src_out = out;
            notifyConfigChanged();
        }
    }

    void LTR35OutConfig::outDacSetDataFormat(LTR35OutConfig::OutDataFormat fmt) {
        if (m_params.dacDataFmt != fmt) {
            m_params.dacDataFmt = fmt;
            notifyConfigChanged();
        }
    }

    void LTR35OutConfig::outSyncSetEchoEnabled(bool en) {
        if (m_params.echoEnabled != en) {
            m_params.echoEnabled = en;
            notifyConfigChanged();
        }
    }

    void LTR35OutConfig::outSyncSetEchoChannel(int ch) {
        if (m_params.echoChannel != ch) {
            m_params.echoChannel = ch;
            notifyConfigChanged();
        }
    }

    void LTR35OutConfig::outDacSetGenRawCodeMode(bool en) {
        if (m_params.dacRawCodeMode != en) {
            m_params.dacRawCodeMode = en;
            notifyConfigChanged();
        }
    }

    void LTR35OutConfig::outDacSetAfcCorEnabled(bool en) {
        if (m_params.dacAfcCorEn != en) {
            m_params.dacAfcCorEn = en;
            notifyConfigChanged();
        }
    }

    void LTR35OutConfig::outSyncSetPeriodicStatusEnabled(bool en) {
        if (m_params.periodicStatusEn != en) {
            m_params.periodicStatusEn = en;
            notifyConfigChanged();
        }
    }

    double LTR35OutConfig::typeStdGenFreqVal(OutStdGenFreq freq)  {
        return LTR35TypeInfo::typeStdGenFreqVal(typeStdGenFreqNum(freq));
    }



    void LTR35OutConfig::protSave(QJsonObject &cfgObj) const   {
        DevOutSyncConfig::protSave(cfgObj);

        QJsonObject devObj;

        QJsonObject outFreqObj;
        outFreqObj[cfgkey_out_freq_manual_en] = m_params.outFreq.manual_en;
        outFreqObj[cfgkey_out_freq_manual_val] = m_params.outFreq.manual_val;
        outFreqObj[cfgkey_out_freq_std_val] =  LTR35OutStdGenFreqKey::getKey(
                    out_std_gen_freqs, m_params.outFreq.std_val);
        devObj[cfgkey_out_freq_obj] = outFreqObj;

        devObj[cfgkey_sync_mode_en] = m_params.syncMode;
        QJsonArray dacChArray;
        for (int ch_idx {0}; ch_idx < max_dac_ch_cnt; ++ch_idx) {
            QJsonObject dacChObj;
            dacChObj[cfgkey_ch_range_num] = m_params.ch[ch_idx].range;
            dacChObj[cfgkey_ch_hard_src_num] = m_params.ch[ch_idx].hard_src_num;
            dacChObj[cfgkey_ch_hard_src_out] = HardSrcOutKey::getKey(
                        hard_outputs, m_params.ch[ch_idx].hard_src_out);
            dacChArray.append(dacChObj);
        }
        devObj[cfgkey_dac_channels_arr] = dacChArray;

        devObj[cfgkey_dac_data_fmt] = LTR35DacDataFmtKey::getKey(dac_data_fmts, m_params.dacDataFmt);
        devObj[cfgkey_dac_raw_code_mode] = m_params.dacRawCodeMode;
        devObj[cfgkey_dac_afc_cor_en] = m_params.dacAfcCorEn;
        devObj[cfgkey_periodic_status_en] = m_params.periodicStatusEn;

        QJsonObject echoObj;
        echoObj[cfgkey_echo_en] = m_params.echoEnabled;
        echoObj[cfgkey_echo_channel] = m_params.echoChannel;
        devObj[cfgkey_echo_obj] = echoObj;


        cfgObj[cfgkey_dev_obj] = devObj;
    }

    void LTR35OutConfig::protLoad(const QJsonObject &cfgObj) {
        DevOutSyncConfig::protLoad(cfgObj);

        const QJsonObject &devObj {cfgObj[cfgkey_dev_obj].toObject()};
        {
            const QJsonObject &outFreqObj {devObj[cfgkey_out_freq_obj].toObject()};
            m_params.outFreq.manual_en = outFreqObj[cfgkey_out_freq_manual_en].toBool(defaultOutSyncManualGenFreqEnabled());
            m_params.outFreq.manual_val = outFreqObj[cfgkey_out_freq_manual_val].toDouble(defaultOutGenFreqValue());
            m_params.outFreq.std_val = LTR35OutStdGenFreqKey::getValue(
                        out_std_gen_freqs, outFreqObj[cfgkey_out_freq_std_val].toString());
        }

        m_params.syncMode = devObj[cfgkey_sync_mode_en].toBool();

        const QJsonArray &dacChArray {devObj[cfgkey_dac_channels_arr].toArray()};
        for (int ch_idx {0}; (ch_idx < dacChArray.size()) && (ch_idx < max_dac_ch_cnt); ++ch_idx) {
            const QJsonObject &dacChObj {dacChArray.at(ch_idx).toObject()};
            m_params.ch[ch_idx].range = dacChObj[cfgkey_ch_range_num].toInt();
            m_params.ch[ch_idx].hard_src_num = dacChObj[cfgkey_ch_hard_src_num].toInt();
            m_params.ch[ch_idx].hard_src_out = HardSrcOutKey::getValue(
                        hard_outputs, dacChObj[cfgkey_ch_hard_src_out].toString());
        }

        m_params.dacDataFmt = LTR35DacDataFmtKey::getValue(
                    dac_data_fmts, devObj[cfgkey_dac_data_fmt].toString());
        m_params.dacRawCodeMode = devObj[cfgkey_dac_raw_code_mode].toBool();
        m_params.dacAfcCorEn = devObj[cfgkey_dac_afc_cor_en].toBool(true);
        m_params.periodicStatusEn = devObj[cfgkey_periodic_status_en].toBool(true);


        const QJsonObject &echoObj {devObj[cfgkey_echo_obj].toObject()};
        m_params.echoEnabled = echoObj[cfgkey_echo_en].toBool();
        m_params.echoChannel = echoObj[cfgkey_echo_channel].toInt();
    }

    void LTR35OutConfig::protUpdate() {
        DevOutSyncConfig::protUpdate();

        TLTR35_CONFIG cfg;
        LTR35_FillOutFreq(&cfg, outSyncConfiguredGenFreq(), &m_results.out_freq);
    }

    LTR35OutConfig::LTR35OutConfig(const LTR35Config &ltr35cfg, const DevOutInfo &info) :
        DevOutSyncConfig{info}, m_ltr35cfg{&ltr35cfg} {

        memset(&m_params, 0, sizeof(m_params));
        m_params.outFreq.manual_val = m_results.out_freq = defaultOutGenFreqValue();
        m_params.outFreq.std_val = defaultOutSyncStdGenFreq();
        m_params.dacAfcCorEn = true;
        m_params.periodicStatusEn = true;
    }

    LTR35OutConfig::LTR35OutConfig(const LTR35Config &ltr35cfg, const LTR35OutConfig &outCfg)  :
        DevOutSyncConfig{outCfg}, m_ltr35cfg{&ltr35cfg} {
        memcpy(&m_params, &outCfg.m_params, sizeof(outCfg.m_params));
        memcpy(&m_results, &outCfg.m_results, sizeof(outCfg.m_results));
    }
}
