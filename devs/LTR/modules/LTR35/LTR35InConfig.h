#ifndef LTR35INCONFIG_H
#define LTR35INCONFIG_H

#include "lqmeas/ifaces/in/dig/DevInDigSyncBaseConfig.h"

namespace LQMeas {
    class LTR35;
    class LTR35Config;

    class LTR35InConfig : public DevInDigSyncBaseConfig {
        Q_OBJECT
    public:
        bool inDigSyncModeEnabled() const override {return inDigChsAvailable() && DevInDigSyncBaseConfig::inDigSyncModeEnabled();}
        bool inDigChSyncModeAvailable(int ch_num) const override { return inDigChAvailable(ch_num); }
        bool inDigChAvailable(int ch_num) const override { Q_UNUSED(ch_num) return inDigChsAvailable();}

        bool inDigChsAvailable() const;

        bool inDigSyncExternalStart() const override;

        int inDigSampleSize() const override {return 4;}

        bool inDigPackedModeEnabled() const override {return true;}
        int inDigPackedModeWordBitCount() const override;
        int inDigPackedModeWordBitOffset(int ch_num, int bitnum) const override;
    protected:
        void dinAdjustFreq(double &dinFreq) override;
    private:
        quint8 rawInChMask() const;

        explicit LTR35InConfig(LTR35Config &devCfg, const DevInDigInfo &info);
        explicit LTR35InConfig(LTR35Config &devCfg, const LTR35InConfig &inCfg);

        friend class LTR35Config;
        friend class LTR35;


        LTR35Config *m_devCfg;
    };
}

#endif // LTR35INCONFIG_H
