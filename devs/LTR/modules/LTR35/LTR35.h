#ifndef LQMEAS_LTR35_H
#define LQMEAS_LTR35_H

#include "ltr/include/ltr35api.h"
#include "lqmeas/devs/LTR/modules/LTRModule.h"

#include "lqmeas/devs/DeviceFrameSender.h"
#include "lqmeas/ifaces/out/DevOutSync.h"
#include "lqmeas/ifaces/out/SyncModes/stream/DevOutSyncIfaceStream.h"
#include "lqmeas/ifaces/out/SyncModes/cycle/DevOutSyncIfaceCycle.h"
#include "lqmeas/ifaces/out/DevOutSyncEchoTracker.h"
#include "lqmeas/ifaces/out/DevOutAsyncDac.h"
#include "lqmeas/ifaces/out/DevOutAsyncDig.h"
#include "lqmeas/ifaces/out/SignalConverter/DevOutDacConverter.h"
#include "lqmeas/ifaces/in/dig/DevInAsyncDig.h"
#include "lqmeas/ifaces/in/dig/DevInSyncDig.h"
#include <QMutex>
#include <memory>

namespace LQMeas {
    class LTR35Config;
    class LTR35Info;
    class LTRModuleFrameReceiver;

    class LTR35 : public LTRModule, public DevInAsyncDig, public DevInSyncDig,
            public DevOutSync,  public DevOutAsyncDac, public DevOutAsyncDig,
            public DevOutSyncEchoTracker, public DevOutDacConverter,
            DevOutSyncIfaceCycle, DevOutSyncIfaceStream {
        Q_OBJECT
    public:
        static const quint16 typeModuleID {LTR_MID_LTR35};

        ~LTR35() override;
        bool isOpened() const override;

        DevOutSync *devOutSync() override {return static_cast<DevOutSync*>(this);}
        DevOutSyncEchoTracker *devOutSyncEchoTracker() override {
            return static_cast<DevOutSyncEchoTracker *>(this);
        }
        DevOutAsyncDac *devOutAsyncDac() override {return static_cast<DevOutAsyncDac *>(this);}
        DevOutAsyncDig *devOutAsyncDig() override {return static_cast<DevOutAsyncDig *>(this);}
        DevOutDacConverter *devOutDacConverter() override {return static_cast<DevOutDacConverter *>(this);}
        DevInAsyncDig *devInAsyncDig() override {return static_cast<DevInAsyncDig*>(this);}
        DevInSyncDig *devInSyncDig() override   {return static_cast<DevInSyncDig*>(this);}
        DevSyncMarksRecv *devSyncMarkRecv() override;

        QString errorString(int err) const override;

        const LTR35Config &devspecConfig() const;
        QSharedPointer<const LTR35Info> devspecInfo() const;

        static const int hard_sources_cnt {4};

        void setHardGenParams(int src_num, double freq, double phase, LQError &err);

        TLTR35 *rawHandle() {return &m_hnd;}
        const TLTR35 *rawHandle() const {return &m_hnd;}
    protected:
        TLTR *channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn,  int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;

        INT ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) override;
        INT ltrRawWordsSend(const DWORD *wrds, DWORD size, DWORD tout) override;

        void outSyncEchoTrackStart(LQError &err) override;
        void outSyncEchoTrackStop(LQError &err) override;
        void outSyncEchoTrackWait(quint64 wt_pos, unsigned tout, quint64 &fnd_pos, LQError &err) override;

        void protInAsyncDig(unsigned &val, LQError &err) override;

        void protInDigStart(LQError &err) override;
        void protInDigStop(LQError &err) override;
        void protInDigGetData(quint32 *data, int size,  unsigned tout, int &recvd_size, LQError &err) override;
    private:
        explicit LTR35(LTRCrate *crate, int slot, QObject *parent = nullptr);

        void privOutSyncSendData(const double *dac_data, int dac_size,
                                 const unsigned *dig_data, int dig_size,
                                 unsigned flags, unsigned tout, LQError &err) override;
        bool privOutSyncHasDefferedData() const override;
        void privOutSyncTryFlushData(unsigned tout, LQError &err) override;
        void privOutSyncSendFinish(LQError &err) override;
        bool privOutSyncUnderflowOccured() const override;


        int outCycleGenMaxSize(const OutRamSignalGenerator &generator) const override;
        void privOutCycleLoadStart(const OutRamSignalGenerator &generator, int size, LQError &err) override;
        void privOutCycleLoadFinish(LQError &err) override;
        void privOutCycleGenStartRequest(LQError &err) override;
        bool privOutCycleGenStartWaitDone(unsigned tout, LQError &err) override;
        void privOutCycleGenUpdateRequest(LQError &err) override;
        bool privOutCycleGenUpdateWaitDone(unsigned tout, LQError &err) override;
        void privOutCycleGenStopRequest(unsigned tout, LQError &err) override;
        void privOutCycleGenStop(LQError &err) override;

        void privOutStreamInit(LQError &err) override;
        void privOutStreamStartRequest(LQError &err) override;
        bool privOutStreamStartWaitDone(unsigned tout, LQError &err) override;
        void privOutStreamStop(unsigned tout, LQError &err) override;

        void protOutSyncHardOnlyGenStartRequest(LQError &err) override;
        bool protOutSyncHardOnlyGenStartWaitDone(unsigned tout, LQError &err) override;
        void protOutSyncHardOnlyGenStop(unsigned tout, LQError &err) override;
        void protOutSyncHardGenUpdate(LQError &err) override;

        void protOutAsyncDac(int ch, double val, LQError &err) override;
        void protOutAsyncDig(unsigned val, unsigned mask, LQError &err) override;

        void protOutAsyncCur(LQError &err);

        void streamStartReq(LQError &err, bool &runFlag);
        bool streamStartWait(unsigned tout, LQError &err);
        void streamStop(unsigned tout, bool &runFlag, LQError &err);

        void genStop(unsigned tout, LQError &err);

        bool getHardAmp(const LTR35Config *cfg, int ch, SignalConfig *sig, double &amp, double &offs);



        mutable TLTR35 m_hnd;
        DeviceFrameSender<LTR35, DWORD> m_sender;
        const std::unique_ptr<LTRModuleFrameReceiver> m_receiver;
        quint64 m_echo_pos;
        static const unsigned default_stop_tout {30000};

        double m_last_async_vals[LTR35_DAC_CHANNEL_CNT];
        unsigned m_last_async_dout;

        bool m_dinRunning {false};
        bool m_outRunning {false};
        enum class StreamState {
            Stopped,
            StartAckWaiting,
            Running
        } m_stream_state {StreamState::Stopped};
        QMutex m_streamLock;

        friend class LTRResolver;
        friend class DeviceFrameSender<LTR35, DWORD>;

        struct {
            double freq;
            double phase;
        } m_hard_src_gen[hard_sources_cnt];
    };
}

#endif // LTR35_H
