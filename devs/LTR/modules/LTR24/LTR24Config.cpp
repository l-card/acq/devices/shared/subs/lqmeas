#include "LTR24Config.h"
#include "LTR24AdcConfig.h"
#include "LTR24TypeInfo.h"
#include "LTR24AdcICPConfig.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvConfig.h"

namespace LQMeas {
    const QString &LTR24Config::typeConfigName() {
        return LTR24TypeInfo::name();
    }

    const DevAdcConfig *LTR24Config::adcConfig() const  {
        return m_adc.get();
    }

    const DevAdcICPConfig *LTR24Config::adcICPConfig() const  {
        return m_icp.get();
    }

    const DevSyncMarksRecvConfig *LTR24Config::syncMarksRecvConfig() const {
        return m_syncRecv.get();
    }

    LTR24Config::LTR24Config() :
        m_adc{new LTR24AdcConfig{LTR24TypeInfo::defaultTypeInfo()}},
        m_icp{new LTR24AdcICPConfig{LTR24TypeInfo::defaultTypeInfo()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this}} {

        init();
    }

    LTR24Config::LTR24Config(const LTR24Config &cfg) :
        m_adc{new LTR24AdcConfig{cfg.adc()}},
        m_icp{new LTR24AdcICPConfig{cfg.icp()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this, cfg.marksRecv()}} {

        init();
    }

    LTR24Config::~LTR24Config() {

    }

    void LTR24Config::protUpdate() {
        m_syncRecv->updateSyncMarksRecvFreq(m_adc->adcChFreq());
    }
}
