#ifndef LQMEAS_LTR24ADCCONFIG_H
#define LQMEAS_LTR24ADCCONFIG_H

#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"

namespace LQMeas {
    class LTR24AdcConfig : public DevAdcConfig {
        Q_OBJECT
    public:
        enum class ChMode {
            Diff,
            ICP
        };

        enum class AcMode {
            AcDc,
            AcOnly
        };

        enum class AdcDataFormat {
            Format_20,
            Format_24
        };

        enum class AdcChRange {
            Diff_10 = 0,
            Diff_2 = 1,

            ICP_5 = 0,
            ICP_1 = 1,
        };

        enum class AdcFreq {
            Freq_117K = 0,
            Freq_78K  = 1,
            Freq_58K  = 2,
            Freq_39K  = 3,
            Freq_29K  = 4,
            Freq_19K  = 5,
            Freq_14K  = 6,
            Freq_9K7  = 7,
            Freq_7K3  = 8,
            Freq_4K8  = 9,
            Freq_3K6  = 10,
            Freq_2K4  = 11,
            Freq_1K8  = 12,
            Freq_1K2  = 13,
            Freq_915  = 14,
            Freq_610  = 15
        };

        static const int adc_channels_cnt {4};

        double adcFreq() const override;
        bool adcChEnabled(int ch) const override {return m_params.ch[ch].enabled;}
        int adcChRangeNum(int ch) const override { return m_params.ch[ch].range; }
        AdcChRange adcChRange(int ch) const {return static_cast<AdcChRange>(adcChRangeNum(ch));}
        ChMode adcChMode(int ch) const {return m_params.ch[ch].mode;}
        AcMode adcChAcMode(int ch) const {return m_params.ch[ch].ac_mode;}
        int adcFreqNum() const {return m_params.freq_num;}
        AdcFreq adcFreqCode() const {return static_cast<AdcFreq>(adcFreqNum());}
        AdcDataFormat adcDataFormat() const {return m_params.data_fmt;}
        bool adcTestModesEnabled() const {return m_params.test_modes_en;}

        /* возвращает максимально количество каналов, которое можно разрешить
         * для текущих настроек модуля */
        int adcMaxEnabledChCnt() const;

        bool adcChSupportMeasDC(int ch) const override;

        int adcSampleSize() const override {return adcDataFormat() == AdcDataFormat::Format_24 ? 8 : 4;}

        static int typeAdcRangeNum(AdcChRange range) {return static_cast<int>(range);}
        static int typeAdcFreqNum(AdcFreq freq) { return static_cast<int>(freq);}
        static double typeAdcFreqVal(AdcFreq freq);
    public Q_SLOTS:
        void adcSetChEnabled(int ch, bool en);
        void adcSetChRange(int ch, LTR24AdcConfig::AdcChRange range);
        void adcSetChRangeNum(int ch, int range_num);
        void adcSetChMode(int ch, LTR24AdcConfig::ChMode mode);
        void adcSetChAcMode(int ch, LTR24AdcConfig::AcMode mode);
        void adcSetFreqNum(int freq_num);
        void adcSetFreqCode(LTR24AdcConfig::AdcFreq freq_code) {
            adcSetFreqNum(typeAdcFreqNum(freq_code));
        }
        void adcSetDataFormat(LTR24AdcConfig::AdcDataFormat format);
        void adcSetTestModesEnabled(bool en);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
        void protUpdateInfo(const DeviceTypeInfo *info) override ;
    private:
        explicit LTR24AdcConfig(const DevAdcInfo &info);
        LTR24AdcConfig(const LTR24AdcConfig &cfg);

        static int defaultRangeNum() {return typeAdcRangeNum(AdcChRange::Diff_10);}
        static int defaultAdcFreqNum() {return typeAdcFreqNum(AdcFreq::Freq_9K7);}

        struct {
            int           freq_num;
            AdcDataFormat data_fmt;
            bool          test_modes_en;
            struct {
                bool    enabled;
                int     range;
                ChMode  mode;
                AcMode  ac_mode;
            } ch[adc_channels_cnt];
        } m_params;

        friend class LTR24Config;
    };
}


Q_DECLARE_METATYPE(LQMeas::LTR24AdcConfig::ChMode);
Q_DECLARE_METATYPE(LQMeas::LTR24AdcConfig::AcMode);
Q_DECLARE_METATYPE(LQMeas::LTR24AdcConfig::AdcDataFormat);
Q_DECLARE_METATYPE(LQMeas::LTR24AdcConfig::AdcChRange);
Q_DECLARE_METATYPE(LQMeas::LTR24AdcConfig::AdcFreq);

#endif // LQMEAS_LTR24ADCCONFIG_H
