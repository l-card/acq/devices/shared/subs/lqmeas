#include "LTR24AdcConfig.h"
#include "LTR24TypeInfo.h"
#include "LTR24.h"
#include "ltr/include/ltr24api.h"
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>
#include <QJsonArray>


namespace LQMeas {
    static const QLatin1String &cfgkey_ch_enabled        {StdConfigKeys::enabled()};
    static const QLatin1String &cfgkey_ch_range_num      {StdConfigKeys::rangeNum()};
    static const QLatin1String &cfgkey_ch_mode           {StdConfigKeys::mode()};
    static const QLatin1String &cfgkey_ch_ac_mode        {StdConfigKeys::acMode()};
    static const QLatin1String &cfgkey_channels_arr      {StdConfigKeys::channels()};
    static const QLatin1String &cfgkey_adc_data_fmt      {StdConfigKeys::adcDataFmt()};
    static const QLatin1String &cfgkey_adc_freq_code     {StdConfigKeys::adcFreqCode()};
    static const QLatin1String cfgkey_adc_test_mode      {"TestMode"};

    typedef EnumConfigKey<LTR24AdcConfig::ChMode> LTR24ChModeKey;
    static const LTR24ChModeKey adc_ch_modes[] = {
        {QStringLiteral("diff"),            LTR24AdcConfig::ChMode::Diff},
        {QStringLiteral("icp"),             LTR24AdcConfig::ChMode::ICP},
        {LTR24ChModeKey::defaultKey(),      LTR24AdcConfig::ChMode::Diff},
    };

    typedef EnumConfigKey<LTR24AdcConfig::AcMode> LTR24ChAcModeKey;
    static const LTR24ChAcModeKey adc_ch_acmodes[] = {
        {QStringLiteral("acdc"),            LTR24AdcConfig::AcMode::AcDc},
        {QStringLiteral("ac"),              LTR24AdcConfig::AcMode::AcOnly},
        {LTR24ChAcModeKey::defaultKey(),    LTR24AdcConfig::AcMode::AcDc},
    };

    typedef EnumConfigKey<LTR24AdcConfig::AdcDataFormat> LTR24AdcDataFmtKey;
    static const LTR24AdcDataFmtKey adc_data_fmts[] = {
        {QStringLiteral("20"),              LTR24AdcConfig::AdcDataFormat::Format_20},
        {QStringLiteral("24"),              LTR24AdcConfig::AdcDataFormat::Format_24},
        {LTR24AdcDataFmtKey::defaultKey(),  LTR24AdcConfig::AdcDataFormat::Format_20},
    };

    double LTR24AdcConfig::adcFreq() const {
        return LTR24TypeInfo::typeAdcFreqVal(m_params.freq_num);
    }

    int LTR24AdcConfig::adcMaxEnabledChCnt() const {
        int max_ch_cnt = 4;
        if (m_params.data_fmt == AdcDataFormat::Format_24) {
            if (m_params.freq_num == typeAdcFreqNum(AdcFreq::Freq_117K)) {
                max_ch_cnt = 2;
            } else if (m_params.freq_num == typeAdcFreqNum(AdcFreq::Freq_78K)) {
                max_ch_cnt = 3;
            }
        }
        return max_ch_cnt;
    }

    bool LTR24AdcConfig::adcChSupportMeasDC(int ch) const {
        return (adcChMode(ch) == ChMode::Diff) && (adcChAcMode(ch) == AcMode::AcDc);
    }

    double LTR24AdcConfig::typeAdcFreqVal(LTR24AdcConfig::AdcFreq freq) {
        return LTR24TypeInfo::typeAdcFreqVal(typeAdcFreqNum(freq));
    }


    void LTR24AdcConfig::adcSetChEnabled(int ch, bool en) {
        if (m_params.ch[ch].enabled != en) {
            m_params.ch[ch].enabled = en;
            notifyChEnableStateChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR24AdcConfig::adcSetChRange(int ch, AdcChRange range) {
        adcSetChRangeNum(ch, typeAdcRangeNum(range));
    }

    void LTR24AdcConfig::adcSetChRangeNum(int ch, int range_num) {
        if (m_params.ch[ch].range != range_num) {
            m_params.ch[ch].range = range_num;
            notifyChRangeChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR24AdcConfig::adcSetChMode(int ch, ChMode mode) {
        if (m_params.ch[ch].mode != mode) {
            m_params.ch[ch].mode = mode;
            notifyChModeChanged(ch);
            notifyChRangeChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR24AdcConfig::adcSetChAcMode(int ch, AcMode mode) {
        if (m_params.ch[ch].ac_mode != mode) {
            m_params.ch[ch].ac_mode = mode;
            notifyChModeChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR24AdcConfig::adcSetFreqNum(int freq_num) {
        if (m_params.freq_num != freq_num) {
            m_params.freq_num = freq_num;
            notifyAdcFreqChanged();
            notifyChFreqChanged();
            notifyConfigChanged();
        }
    }

    void LTR24AdcConfig::adcSetDataFormat(AdcDataFormat format) {
        if (m_params.data_fmt != format) {
            m_params.data_fmt = format;
            notifyConfigChanged();
        }
    }

    void LTR24AdcConfig::adcSetTestModesEnabled(bool en) {
        if (m_params.test_modes_en != en) {
            m_params.test_modes_en = en;
            notifyConfigChanged();
        }
    }

    void LTR24AdcConfig::protSave(QJsonObject &cfgObj) const     {
        QJsonArray chArray;
        for (int ch {0}; ch < adc_channels_cnt; ++ch) {
            QJsonObject chObj;
            chObj[cfgkey_ch_enabled] = m_params.ch[ch].enabled;
            chObj[cfgkey_ch_range_num] = m_params.ch[ch].range;
            chObj[cfgkey_ch_mode] =LTR24ChModeKey::getKey(adc_ch_modes, m_params.ch[ch].mode);
            chObj[cfgkey_ch_ac_mode] =LTR24ChAcModeKey::getKey(adc_ch_acmodes, m_params.ch[ch].ac_mode);
            chArray.append(chObj);
        }
        cfgObj[cfgkey_channels_arr] = chArray;
        cfgObj[cfgkey_adc_data_fmt] = LTR24AdcDataFmtKey::getKey(adc_data_fmts, m_params.data_fmt);
        cfgObj[cfgkey_adc_freq_code] = static_cast<int>(m_params.freq_num);
        cfgObj[cfgkey_adc_test_mode] = m_params.test_modes_en;
    }

    void LTR24AdcConfig::protLoad(const QJsonObject &cfgObj)     {
        const QJsonArray &chArray {cfgObj[cfgkey_channels_arr].toArray()};
        const int ch_cnt {chArray.size()};

        for (int ch {0}; (ch < ch_cnt) && (ch < adc_channels_cnt); ++ch) {
            const QJsonObject chObj {chArray.at(ch).toObject()};

            m_params.ch[ch].enabled = chObj[cfgkey_ch_enabled].toBool();

            const int range {chObj[cfgkey_ch_range_num].toInt(defaultRangeNum())};
            if ((range >= 0) || (range < adcInfo().adcChRangesCnt(ch, adcUnitMode(ch)))) {
                m_params.ch[ch].range = range;
            } else {
                m_params.ch[ch].range =  defaultRangeNum();
            }

            m_params.ch[ch].mode = LTR24ChModeKey::getValue(
                        adc_ch_modes, chObj[cfgkey_ch_mode].toString());
            m_params.ch[ch].ac_mode = LTR24ChAcModeKey::getValue(
                        adc_ch_acmodes, chObj[cfgkey_ch_ac_mode].toString());
        }

        for (int ch=ch_cnt; ch < adc_channels_cnt; ++ch) {
            m_params.ch[ch].enabled = false;
            m_params.ch[ch].range = defaultRangeNum();
            m_params.ch[ch].mode = ChMode::Diff;
            m_params.ch[ch].ac_mode = AcMode::AcDc;
        }

        m_params.data_fmt = LTR24AdcDataFmtKey::getValue(
                    adc_data_fmts, cfgObj[cfgkey_adc_data_fmt].toString());
        m_params.freq_num = cfgObj[cfgkey_adc_freq_code].toInt(defaultAdcFreqNum());
        m_params.test_modes_en = cfgObj[cfgkey_adc_test_mode].toBool(false);
    }

    void LTR24AdcConfig::protUpdateInfo(const DeviceTypeInfo *info) {
        DevAdcConfig::protUpdateInfo(info);
        /* без поддержки ICP сбрасываем настроенный режим на Diff*/
        const LTR24TypeInfo *ltr24info {static_cast<const LTR24TypeInfo *>(info)};
        if (!ltr24info->supportICP()) {
            for (int ch {0}; ch < adc_channels_cnt; ++ch) {
                m_params.ch[ch].mode = ChMode::Diff;
            }
        }
    }

    LTR24AdcConfig::LTR24AdcConfig(const DevAdcInfo &info) : DevAdcConfig{info} {
        memset(&m_params, 0, sizeof(m_params));
    }

    LTR24AdcConfig::LTR24AdcConfig(const LTR24AdcConfig &cfg) : DevAdcConfig{cfg} {
        memcpy(&m_params, &cfg.m_params, sizeof (m_params));
    }

}
