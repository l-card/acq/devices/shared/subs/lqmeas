#ifndef LQMEAS_LTR24_H
#define LQMEAS_LTR24_H

#include "ltr/include/ltr24api.h"
#include "lqmeas/devs/LTR/modules/LTRModule.h"
#include "lqmeas/ifaces/in/adc/DevAdc.h"
#include <memory>

namespace LQMeas {
    class LTR24Config;
    class LTR24Info;
    class LTRModuleFrameReceiver;

    class LTR24 : public LTRModule, public DevAdc {
        Q_OBJECT
    public:
        static const quint16 typeModuleID {LTR_MID_LTR24};

        ~LTR24() override;
        bool isOpened() const override;

        /* ----------------------- интерфейс ввода  --------------------------*/
        DevAdc *devAdc() override {return static_cast<DevAdc*>(this);}
        DevSyncMarksRecv *devSyncMarkRecv() override;

        QString errorString(int err) const override;

        bool adcIsRunning() const override;

        const LTR24Config &devspecConfig() const;
        QSharedPointer<const LTR24Info> devspecInfo() const;
    protected:
        TLTR* channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn,  int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;

        INT ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) override;

        void protAdcStart(LQError &err) override;
        void protAdcStop(LQError &err) override;
        void protAdcGetData(double *data, int size, unsigned flags,
                            unsigned tout, int &recvd_size, LQError &err) override;

    private:
        void rawAdcGetData(double *data, int size, unsigned flags,
                           unsigned tout, int &recvd_size, LQError &err);

        explicit LTR24(LTRCrate *crate, int slot, QObject *parent = nullptr);
        mutable TLTR24 m_hnd;
        const std::unique_ptr<LTRModuleFrameReceiver> m_receiver;

        bool m_corIcpPhase;
        int m_drop_rem_samples {0};
        static const int drop_samples_max {6*4};

        friend class LTRResolver;
    };
}

#endif // LQMEAS_LTR24_H
