#include "LTR24.h"
#include "LTR24Config.h"
#include "LTR24AdcConfig.h"
#include "LTR24AdcICPConfig.h"
#include "LTR24Info.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "lqmeas/devs/LTR/modules/LTRModuleFrameReceiver.h"
#include "lqmeas/log/Log.h"
#include "lqmeas/StdErrors.h"

namespace LQMeas {
    LTR24::LTR24(LTRCrate *crate, int slot, QObject *parent) :
        LTRModule{crate, slot, typeModuleID, new LTR24Config{}, LTR24Info{}, parent},
        DevAdc{this, LTR24AdcConfig::adc_channels_cnt},
        m_receiver{new LTRModuleFrameReceiver{this}},
        m_corIcpPhase{true} {

        LTR24_Init(&m_hnd);
    }


    LTR24::~LTR24() {
        LTR24_Close(&m_hnd);
    }

    bool LTR24:: isOpened() const {
        return LTR24_IsOpened(&m_hnd) == LTR_OK;
    }

    DevSyncMarksRecv *LTR24::devSyncMarkRecv() {
        return m_receiver.get();
    }

    bool LTR24::adcIsRunning() const {
        return m_hnd.Run ? true : false;
    }

    const LTR24Config &LTR24::devspecConfig() const {
        return static_cast<const LTR24Config &>(config());
    }

    QSharedPointer<const LTR24Info> LTR24::devspecInfo() const {
        return devInfo().staticCast<const LTR24Info>();
    }

    TLTR *LTR24::channel() const {
        return &m_hnd.Channel;
    }

    int LTR24::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        Q_UNUSED(flags)

        INT ltr_err = LTR24_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, slot);
        if (ltr_err == LTR_OK) {
            ltr_err = LTR24_GetConfig(&m_hnd);
        }

        if (ltr_err == LTR_OK) {
            setDeviceInfo(LTR24Info{m_hnd.ModuleInfo.SupportICP ? LTR24TypeInfo::typeInfoMod2() : LTR24TypeInfo::typeInfoMod1(),
                                    QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                    m_hnd.ModuleInfo.VerPLD,
                                    (m_hnd.ModuleInfo.SupportedFeatures & LTR24_FEATURE_ICP_EXT_BANDWIDTH_LF) != 0});
        }
        return ltr_err;
    }

    void LTR24::protClose(LQError &err) {
        getError(LTR24_Close(&m_hnd), err);
    }


    void LTR24::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTR24Config *setCfg {qobject_cast<const LTR24Config *>(&cfg)};
        if (setCfg) {
            m_hnd.ADCFreqCode = static_cast<BYTE>(setCfg->adc().adcFreqCode());
            m_hnd.DataFmt =   setCfg->adc().adcDataFormat() == LTR24AdcConfig::AdcDataFormat::Format_20 ? LTR24_FORMAT_20 : LTR24_FORMAT_24;
            m_hnd.ISrcValue = setCfg->icp().adcISrcValue() == LTR24AdcICPConfig::ISrcValue::I_10mA ? LTR24_I_SRC_VALUE_10 : LTR24_I_SRC_VALUE_2_86;
            m_hnd.TestMode = setCfg->adc().adcTestModesEnabled();
            for (int ch {0}; ch < LTR24AdcConfig::adc_channels_cnt; ++ch) {
                m_hnd.ChannelMode[ch].Enable = setCfg->adc().adcChEnabled(ch);
                m_hnd.ChannelMode[ch].AC = setCfg->adc().adcChAcMode(ch) == LTR24AdcConfig::AcMode::AcOnly;
                m_hnd.ChannelMode[ch].Range = setCfg->adc().adcChRange(ch) == LTR24AdcConfig::AdcChRange::Diff_2 ?
                            LTR24_RANGE_2 : LTR24_RANGE_10;
                m_hnd.ChannelMode[ch].ICPMode = setCfg->adc().adcChMode(ch) == LTR24AdcConfig::ChMode::ICP;
                m_hnd.ChannelMode[ch].SensorROut = static_cast<float>(setCfg->icp().adcICPChSensorROut(ch));
            }
            m_corIcpPhase = setCfg->icp().adcPhaseCorEnabled();

            getError(LTR24_SetADC(&m_hnd), err);
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }

    INT LTR24::ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) {
        return LTR24_Recv(&m_hnd, wrds, marks, size, tout);
    }

    void LTR24::protAdcStart(LQError &err) {
        getError(LTR24_Start(&m_hnd), err);
        if (err.isSuccess()) {
            int frameSize = adcConfig().adcEnabledChCnt() *
                    (m_hnd.DataFmt == LTR24_FORMAT_20 ? 1 : 2);
            /* для частот 39K и ниже (код больше) используется коррекция АЧХ, с
             * помощью фильтра с временем установления в 6 отсчетов.
             * в связи с этим превые 6 отсчетов каждого канала необходимо откинуть
             * после обработки ProcessData */
            int drop_frames =  (m_hnd.ADCFreqCode >= LTR24_FREQ_39K) ? 6 : 0;
            m_drop_rem_samples = drop_frames * adcConfig().adcEnabledChCnt();

            m_receiver->start(frameSize, frameSize);
        }
    }

    void LTR24::protAdcStop(LQError &err) {
        getError(LTR24_Stop(&m_hnd), err);
        m_receiver->clear();
    }

    void LTR24::protAdcGetData(double *data, int size, unsigned flags, unsigned tout,
                               int &recvd_size, LQError &err) {
        if (m_drop_rem_samples > 0) {
            double drop_data[drop_samples_max];
            int recvd;
            rawAdcGetData(drop_data, m_drop_rem_samples, flags, tout, recvd, err);
            if (err.isSuccess()) {
                m_drop_rem_samples -= recvd;
            }
        }

        if (m_drop_rem_samples == 0) {
            rawAdcGetData(data, size, flags, tout, recvd_size, err);
        }
    }

    void LTR24::rawAdcGetData(double *data, int size, unsigned flags, unsigned tout, int &recvd_size, LQError &err) {
        const int wrds_cnt {m_hnd.DataFmt == LTR24_FORMAT_20 ? size : 2*size};
        int recvd_words {0};
        DWORD *wrds;
        QScopedArrayPointer<BOOL> ovrlds{new BOOL[static_cast<size_t>(size)]};
        INT proc_size {0};

        m_receiver->getFrames(wrds_cnt, tout, wrds, recvd_words, err);

        if (err.isSuccess() && (recvd_words != 0)) {
            DWORD proc_flags {LTR24_PROC_FLAG_VOLT
                        | LTR24_PROC_FLAG_CALIBR
                        | LTR24_PROC_FLAG_AFC_COR_EX};
            if (m_corIcpPhase)
                proc_flags |= LTR24_PROC_FLAG_ICP_PHASE_COR;
            proc_size = static_cast<INT>(recvd_words);

            getError(LTR24_ProcessData(&m_hnd, wrds, data, &proc_size,
                                       proc_flags, ovrlds.data()), err);
            if (!err.isSuccess()) {
                LQMeasLog->error(tr("Process data error"), err, this);
            } else {
                const int en_ch_cnt {adcConfig().adcEnabledChCnt()};
                for (int ch_idx {0}; ch_idx < en_ch_cnt; ++ch_idx) {
                    ChannelStatus status {ChannelStatus::Ok};
                    if (m_hnd.DataFmt == LTR24_FORMAT_24) {
                        for (int i {ch_idx}; (i < proc_size) && (status == ChannelStatus::Ok); i+=en_ch_cnt) {
                            if (ovrlds[i]) {
                                status = ChannelStatus::Overload;
                            }
                        }
                    }
                    adcSetChStatus(adcChNum(ch_idx), status);
                }
            }
        }

        if (err.isSuccess()) {
            recvd_size = static_cast<int>(proc_size);
        }
    }

    QString LTR24::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTR24_GetErrorString(err));
    }
}
