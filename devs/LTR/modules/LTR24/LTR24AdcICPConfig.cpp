#include "LTR24AdcICPConfig.h"
#include "LTR24AdcConfig.h"
#include "LTR24Config.h"
#include "LTR24TypeInfo.h"
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_isrc_value     {StdConfigKeys::iSrcValue()};
    static const QLatin1String cfgkey_phase_cor_en   {StdConfigKeys::phaseCorEn()};

    typedef EnumConfigKey<LTR24AdcICPConfig::ISrcValue> LTR24IsrValKey;
    static const LTR24IsrValKey isrc_vals[] = {
        {QStringLiteral("2.86"),        LTR24AdcICPConfig::ISrcValue::I_2_86mA},
        {QStringLiteral("10"),          LTR24AdcICPConfig::ISrcValue::I_10mA},
        {LTR24IsrValKey::defaultKey(),  LTR24AdcICPConfig::ISrcValue::I_2_86mA},
    };


    bool LTR24AdcICPConfig::adcChSupportICP(int ch) const {
        Q_UNUSED(ch)
        return  static_cast<LTR24Config *>(devConfig())->adc().adcChMode(ch) == LTR24AdcConfig::ChMode::ICP;
    }

    bool LTR24AdcICPConfig::adcChSupportTEDS(int ch) const  {
        Q_UNUSED(ch)
        return false;
    }

    double LTR24AdcICPConfig::adcICPChISrcValue_mA(int ch) const {
        Q_UNUSED(ch)
        return  m_i_src_value == ISrcValue::I_2_86mA ? 2.86 : 10;
    }

    void LTR24AdcICPConfig::adcSetISrcValue(LTR24AdcICPConfig::ISrcValue val)  {
        if (val != m_i_src_value) {
            m_i_src_value = val;
            notifyISrcValChanged(-1);
            notifyConfigChanged();
        }
    }

    void LTR24AdcICPConfig::adcSetPhaseCorEnabled(bool en) {
        if (en != m_phase_cor) {
            m_phase_cor = en;
            notifyConfigChanged();
        }
    }

    void LTR24AdcICPConfig::protInit(DeviceConfig *cfg) {
         DevAdcICPConfig::protInit(cfg);
         connect(&static_cast<LTR24Config *>(devConfig())->adc(), &LTR24AdcConfig::adcChModeChanged,
                 this, &LTR24AdcICPConfig::adcICPChICPSupportChanged);
    }

    void LTR24AdcICPConfig::protSave(QJsonObject &cfgObj) const  {
        DevAdcICPConfig::protSave(cfgObj);
        cfgObj[cfgkey_isrc_value] = LTR24IsrValKey::getKey(isrc_vals, m_i_src_value);
        cfgObj[cfgkey_phase_cor_en] = m_phase_cor;
    }

    void LTR24AdcICPConfig::protLoad(const QJsonObject &cfgObj) {
        DevAdcICPConfig::protLoad(cfgObj);
        adcSetISrcValue(LTR24IsrValKey::getValue(isrc_vals, cfgObj[cfgkey_isrc_value].toString()));
        adcSetPhaseCorEnabled(cfgObj[cfgkey_phase_cor_en].toBool(true));
    }

    LTR24AdcICPConfig::LTR24AdcICPConfig(const DevAdcInfo &info) : DevAdcICPConfig{info} {
    }

    LTR24AdcICPConfig::LTR24AdcICPConfig(const LTR24AdcICPConfig &cfg) :
        DevAdcICPConfig{cfg},
        m_i_src_value{cfg.m_i_src_value},
        m_phase_cor{cfg.m_phase_cor} {

    }
}
