#ifndef LQMEAS_LTR24TYPEINFO_H
#define LQMEAS_LTR24TYPEINFO_H

#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"
#include "lqmeas/ifaces/in/adc/DevAdcInfo.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvInfo.h"

namespace LQMeas {
    class LTR24TypeInfo : public LTRModuleTypeInfo, public DevAdcInfo, public LTRSyncMarksRecvInfo {
    public:
        static const QString &name();

        static const LTR24TypeInfo &typeInfoMod1();
        static const LTR24TypeInfo &typeInfoMod2();
        static const LTR24TypeInfo &defaultTypeInfo();

        static const QList<const LTR24TypeInfo *> &types();

        const DevAdcInfo *adc() const override {
            return static_cast<const DevAdcInfo *>(this);
        }
        const DevSyncMarksRecvInfo *syncMarkRecv() const override {
            return static_cast<const DevSyncMarksRecvInfo *>(this);
        }

        QString deviceTypeName() const override {return name();}
        QString deviceModificationName() const override;

        AdcType adcType() const override {return AdcType::Parallel;}
        int adcChannelsCnt() const override;
        double adcFreqMax() const override;

        bool adcIsChFreqConfigurable() const override {return false;}

        int adcChRangesCnt(int ch_num, int unit_mode_num = 0) const override;
        double adcChRangeMaxVal(int ch_num, int range, int unit_mode_num = 0) const override;
        double adcChRangeMinVal(int ch_num, int range, int unit_mode_num = 0) const override;

        static double typeAdcFreqVal(int idx);
        static int typeAdcFreqsCnt();

        bool supportICP() const {return m_supportICP;}

        QList<const DeviceTypeInfo *> modificationList() const override;
    private:
        LTR24TypeInfo(bool supportICP) : m_supportICP{supportICP} {
        }

        bool m_supportICP;
    };
}


#endif // LQMEAS_LTR24TYPEINFO_H
