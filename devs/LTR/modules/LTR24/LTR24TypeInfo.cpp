#include "LTR24TypeInfo.h"
#include "LTR24AdcConfig.h"

static const double f_ranges[] = {10., 2.};

static const double f_adc_freqs[] = {
        117.1875e3,  78.125e3,   58.59375e3,   39.0625e3,
        29.296875e3, 19.53125e3, 14.6484375e3, 9.765625e3,
        7.32421875e3, 4.8828125e3, 3.662109375e3, 2.44140625e3,
        1.8310546875e3, 1.220703125e3, 0.91552734375e3, 0.6103515625e3
};

namespace LQMeas {
    const QString &LTR24TypeInfo::name() {
        static const QString str { QStringLiteral("LTR24") };
        return str;
    }

    double LTR24TypeInfo::adcFreqMax() const {
        return f_adc_freqs[0];
    }

    int LTR24TypeInfo::adcChRangesCnt(int ch_num, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return sizeof(f_ranges)/sizeof(f_ranges[0]);
    }

    double LTR24TypeInfo::adcChRangeMaxVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
         return f_ranges[range];
    }

    double LTR24TypeInfo::adcChRangeMinVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
                return -f_ranges[range];
    }

    const LTR24TypeInfo &LTR24TypeInfo::typeInfoMod1() {
        static const LTR24TypeInfo info{false};
        return info;
    }

    const LTR24TypeInfo &LTR24TypeInfo::typeInfoMod2() {
        static const LTR24TypeInfo info{true};
        return info;
    }

    const LTR24TypeInfo &LTR24TypeInfo::defaultTypeInfo() {
        return typeInfoMod1();
    }

    const QList<const LTR24TypeInfo *> &LTR24TypeInfo::types() {
        static const QList<const LTR24TypeInfo *> list {
            &typeInfoMod1(),
            &typeInfoMod2()
        };
        return list;
    }

    QString LTR24TypeInfo::deviceModificationName() const {
       return  QString("%1-%2")
               .arg(deviceTypeName())
               .arg(m_supportICP ? QLatin1String("2") : QLatin1String("1"));
    }

    int LTR24TypeInfo::adcChannelsCnt() const {
        return LTR24AdcConfig::adc_channels_cnt;
    }

    double LTR24TypeInfo::typeAdcFreqVal(int idx) {
        return f_adc_freqs[idx];
    }

    int LTR24TypeInfo::typeAdcFreqsCnt() {
        return sizeof(f_adc_freqs)/sizeof(f_adc_freqs[0]);
    }

    QList<const DeviceTypeInfo *> LTR24TypeInfo::modificationList() const {
        QList<const DeviceTypeInfo *> ret;
        const QList<const LTR24TypeInfo*> &ltr24_types = types();
        for (const LTR24TypeInfo* type : ltr24_types) {
            ret.append(type);
        }
        return ret;
    }
}
