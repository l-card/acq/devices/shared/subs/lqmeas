#ifndef LQMEAS_LTR24ADCICPCONFIG_H
#define LQMEAS_LTR24ADCICPCONFIG_H

#include "lqmeas/ifaces/in/adc/icp/DevAdcICPConfig.h"

namespace LQMeas {
    class LTR24AdcICPConfig : public DevAdcICPConfig   {
        Q_OBJECT
    public:
        enum class ISrcValue {
            I_2_86mA,
            I_10mA,
        };

        bool adcChSupportICP(int ch) const override;
        bool adcChSupportTEDS(int ch) const override;
        double adcICPChISrcValue_mA(int ch) const  override;

        ISrcValue  adcISrcValue() const {return m_i_src_value;}
        bool adcPhaseCorEnabled() const {return m_phase_cor;}
    public Q_SLOTS:
        void adcSetISrcValue(LTR24AdcICPConfig::ISrcValue val);
        void adcSetPhaseCorEnabled(bool en);
    protected:
        void protInit(DeviceConfig *cfg) override;
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        LTR24AdcICPConfig(const DevAdcInfo &info);
        LTR24AdcICPConfig(const LTR24AdcICPConfig &cfg);
    private:
        ISrcValue m_i_src_value {ISrcValue::I_2_86mA};
        bool m_phase_cor {true};

        friend class LTR24Config;
    };
}

Q_DECLARE_METATYPE(LQMeas::LTR24AdcICPConfig::ISrcValue);




#endif // LTR24ADCICPCONFIG_H
