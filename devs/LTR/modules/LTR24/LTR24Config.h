#ifndef LQMEAS_LTR24CONFIG_H
#define LQMEAS_LTR24CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"
#include <memory>

namespace LQMeas {
    class LTR24AdcConfig;
    class LTR24AdcICPConfig;
    class LTRSyncMarksRecvConfig;

    class LTR24Config : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        const DevAdcConfig *adcConfig() const  override;
        const DevAdcICPConfig *adcICPConfig() const  override;
        const DevSyncMarksRecvConfig *syncMarksRecvConfig() const override;

        LTR24AdcConfig &adc() {return *m_adc;}
        const LTR24AdcConfig &adc() const {return *m_adc;}
        LTR24AdcICPConfig &icp() {return *m_icp;}
        const LTR24AdcICPConfig &icp() const {return *m_icp;}
        LTRSyncMarksRecvConfig &marksRecv() {return *m_syncRecv;}
        const LTRSyncMarksRecvConfig &marksRecv() const {return *m_syncRecv;}

        DeviceConfig *clone() const override {
            return new LTR24Config{*this};
        }

        LTR24Config();
        LTR24Config(const LTR24Config &cfg);
        ~LTR24Config() override;

    protected:
        void protUpdate() override;
    private:
        const std::unique_ptr<LTR24AdcConfig> m_adc;
        const std::unique_ptr<LTR24AdcICPConfig> m_icp;
        const std::unique_ptr<LTRSyncMarksRecvConfig> m_syncRecv;
    };
}

#endif // LQMEAS_LTR24CONFIG_H
