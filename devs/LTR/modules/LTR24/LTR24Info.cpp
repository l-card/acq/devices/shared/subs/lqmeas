#include "LTR24Info.h"
#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
     static const QLatin1String &cfgkey_pld_ver             {StdConfigKeys::pldVer()};
     static const QLatin1String &cfgkey_ext_bf_lf           {StdConfigKeys::ipcExtBwLF()};

    LTR24Info::LTR24Info(const LTR24TypeInfo &type, const QString &serial, int pld_ver, bool icp_ext_bw) :
        DeviceInfo{type, serial}, m_pld_ver{pld_ver}, m_icp_ext_bw{icp_ext_bw} {

    }

    void LTR24Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_pld_ver] = m_pld_ver;
        infoObj[cfgkey_ext_bf_lf] = m_icp_ext_bw;
    }

    void LTR24Info::protLoad(const QJsonObject &infoObj) {
        m_pld_ver = infoObj[cfgkey_pld_ver].toInt();
        m_icp_ext_bw = infoObj[cfgkey_ext_bf_lf].toBool(true);
    }

    LTR24Info::LTR24Info(const LTR24Info &info) : DeviceInfo{info},
        m_pld_ver{info.m_pld_ver},
        m_icp_ext_bw{info.m_icp_ext_bw} {
    }
}

