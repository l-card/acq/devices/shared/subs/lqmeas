#ifndef LQMEAS_LTR24INFO_H
#define LQMEAS_LTR24INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTR24TypeInfo.h"

namespace LQMeas {
    class LTR24Info : public DeviceInfo {
    public:
        LTR24Info(const LTR24TypeInfo &type = LTR24TypeInfo::defaultTypeInfo(),
                  const QString &serial = QString{}, int pld_ver = 0, bool icp_ext_bw = true);


        DeviceInfo *clone() const override {return new LTR24Info{*this};}
        int pldVer() const {return m_pld_ver;}
        QString pldVerStr() const {return QString::number(m_pld_ver);}
        bool supportICP() const {return devspecTypeInfo().supportICP();}
        bool icpExtendedBandwidthLF() const {return m_icp_ext_bw;}

        const LTR24TypeInfo &devspecTypeInfo() const {
            return static_cast<const LTR24TypeInfo &>(type());
        }
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        LTR24Info(const LTR24Info &info);

        int m_pld_ver;
        bool m_icp_ext_bw;
    };
}


#endif // LTR24INFO_H
