#include "LTR01.h"

namespace LQMeas {
    LTR01::LTR01(LTRCrate *crate, int slot, e_LTR01_SUBID subid,
                 DeviceConfig *defaultCfg, const DeviceInfo &info, QObject *parent) :
        LTRModule{crate, slot, typeModuleID, defaultCfg, info, parent},
        m_subid{subid} {

    }

}
