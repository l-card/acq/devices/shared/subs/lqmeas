#include "LTR43TypeInfo.h"

namespace LQMeas {
    const QString &LTR43TypeInfo::name() {
        static const QString str {QStringLiteral("LTR43")};
        return str;
    }
    
    const LTR43TypeInfo &LTR43TypeInfo::defaultTypeInfo() {
        static const LTR43TypeInfo info;
        return info;
    }

    QList<const DeviceTypeInfo *> LTR43TypeInfo::modificationList() const     {
        static const QList<const DeviceTypeInfo *> list { &defaultTypeInfo() };
        return list;
    }
}
