#include "LTR43Info.h"
#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String &cfgkey_mcu_ver              {StdConfigKeys::mcuVer()};

    LTR43Info::LTR43Info(const LTR43TypeInfo &type, const QString &serial, const QString &mcu_ver_str) :
        DeviceInfo{type, serial}, m_mcu_ver_str{mcu_ver_str} {

    }

    LTR43Info::LTR43Info(const LTR43Info &info) : DeviceInfo{info}, m_mcu_ver_str{info.m_mcu_ver_str} {
    }

    DeviceInfo *LTR43Info::clone() const {
        return new LTR43Info{*this};
    }


    void LTR43Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_mcu_ver] = m_mcu_ver_str;
    }

    void LTR43Info::protLoad(const QJsonObject &infoObj) {
        m_mcu_ver_str = infoObj[cfgkey_mcu_ver].toString();
    }
}
