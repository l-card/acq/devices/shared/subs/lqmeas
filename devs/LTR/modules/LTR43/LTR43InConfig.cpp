#include "LTR43InConfig.h"
#include "LTR43Config.h"
#include "ltr/include/ltr43api.h"

namespace LQMeas {
    bool LTR43InConfig::inDigChAvailable(int ch) const  {
        return m_devcfg->channelDirection(ch) == LTR43Config::Direction::In;
    }

    void LTR43InConfig::dinAdjustFreq(double &dinFreq) {
        LTR43_CalcStreamReadFreq(dinFreq, &dinFreq);
    }

    LTR43InConfig::LTR43InConfig(LTR43Config *devcfg, const DevInDigInfo &info) :
        DevInDigSyncBaseConfig{info}, m_devcfg{devcfg} {

    }

    LTR43InConfig::LTR43InConfig(LTR43Config *devcfg, const LTR43InConfig &inCfg) :
        DevInDigSyncBaseConfig{inCfg}, m_devcfg{devcfg} {

    }

}
