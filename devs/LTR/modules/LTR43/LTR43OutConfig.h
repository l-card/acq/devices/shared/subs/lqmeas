#ifndef LQMEAS_LTR43OUTCONFIG_H
#define LQMEAS_LTR43OUTCONFIG_H


#include "lqmeas/ifaces/out/DevOutConfig.h"

namespace LQMeas {
    class LTR43Config;

    class LTR43OutConfig : public DevOutConfig   {
        Q_OBJECT
    public:
        int outDacChRangeNum(int ch) const override {Q_UNUSED(ch) return 0;}
        bool outDigChAvailable(int ch) const override;

    private:
        LTR43OutConfig(LTR43Config *devcfg, const DevOutInfo &info);
        LTR43OutConfig(LTR43Config *devcfg, const LTR43OutConfig &outCfg);

        LTR43Config *m_devcfg;

        friend class LTR43Config;
    };
}

#endif // LTR43OUTCONFIG_H
