#include "LTR43Config.h"
#include "LTR43.h"
#include "LTR43TypeInfo.h"
#include <QJsonObject>
#include <QJsonArray>
#include "LTR43InConfig.h"
#include "LTR43OutConfig.h"
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvConfig.h"

namespace LQMeas {
    static const QLatin1String cfgkey_ports_arr {"Ports"};
    static const QLatin1String cfgkey_port_dir  {"Dir"};


    typedef EnumConfigKey<LTR43Config::Direction> LTR43PortDirKey;
    static const LTR43PortDirKey port_dirs[] = {
        {QStringLiteral("in"),          LTR43Config::Direction::In},
        {QStringLiteral("out"),         LTR43Config::Direction::Out},
        {LTR43PortDirKey::defaultKey(), LTR43Config::Direction::In},
    };

    const DevInDigConfig *LTR43Config::inDigConfig() const {
        return m_in.get();
    }

    const DevOutConfig *LTR43Config::outConfig() const {
        return m_out.get();
    }

    const DevSyncMarksRecvConfig *LTR43Config::syncMarksRecvConfig() const {
        return m_syncRecv.get();
    }

    const QString &LTR43Config::typeConfigName() {
        return LTR43TypeInfo::name();
    }

    LTR43Config::LTR43Config() :
        m_in{new LTR43InConfig{this, *LTR43TypeInfo::defaultTypeInfo().digin()}},
        m_out{new LTR43OutConfig{this, *LTR43TypeInfo::defaultTypeInfo().out()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this}} {

        for (int port_idx {0}; port_idx < ports_count; ++port_idx) {
            m_ports_dir[port_idx] = LTR43Config::Direction::In;
        }

        init();
    }

    LTR43Config::LTR43Config(const LTR43Config &cfg) :
        m_in{new LTR43InConfig{this, cfg.din()}},
        m_out{new LTR43OutConfig{this, cfg.out()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this, cfg.marksRecv()}} {

        for (int port_idx {0}; port_idx < ports_count; ++port_idx) {
            m_ports_dir[port_idx] = cfg.m_ports_dir[port_idx];
        }

        init();
    }

    LTR43Config::~LTR43Config() {

    }

    LTR43Config::Direction LTR43Config::channelDirection(int chNum) const {
        return portDirection(chNum / channels_per_ports);
    }

    void LTR43Config::setPortDirection(int portNum, LTR43Config::Direction dir) {
        if ((dir == Direction::In) || (dir == Direction::Out)) {
            m_ports_dir[portNum] = dir;
            notifyConfigChanged();
            /** @todo update port availability ! */
        }
    }

    void LTR43Config::protUpdate() {
         m_syncRecv->updateSyncMarksRecvFreq(din().inDigFreq());
    }



    void LTR43Config::protSave(QJsonObject &cfgObj) const  {
        QJsonArray portsArr;
        for (int port_idx {0}; port_idx < ports_count; ++port_idx) {
            QJsonObject portObj;
            portObj[cfgkey_port_dir] = LTR43PortDirKey::getKey(port_dirs, m_ports_dir[port_idx]);
            portsArr.append(portObj);
        }
        cfgObj[cfgkey_ports_arr] = portsArr;
    }

    void LTR43Config::protLoad(const QJsonObject &cfgObj) {
        const QJsonArray &portsArr {cfgObj[cfgkey_ports_arr].toArray()};
        for (int port_idx {0}; port_idx < ports_count; ++port_idx) {
            if (port_idx < portsArr.size()) {
                const QJsonObject &portObj {portsArr.at(port_idx).toObject()};
                m_ports_dir[port_idx] = LTR43PortDirKey::getValue(
                            port_dirs, portObj[cfgkey_port_dir].toString());
            } else {
                m_ports_dir[port_idx] = LTR43Config::Direction::In;
            }
        }
    }
}
