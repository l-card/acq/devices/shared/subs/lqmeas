#include "LTR43.h"
#include "LTR43Info.h"
#include "LTR43Config.h"
#include "LTR43InConfig.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdErrors.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/devs/LTR/modules/LTRModuleFrameReceiver.h"

namespace LQMeas {
    LTR43::LTR43(LTRCrate *crate, int slot, QObject *parent) :
        LTRModule{crate, slot, typeModuleID, new LTR43Config{}, LTR43Info{}, parent},
        DevInSyncDig{this},
        m_receiver{new LTRModuleFrameReceiver{this}} {

        LTR43_Init(&m_hnd);
    }

    LTR43::~LTR43() {
        LTR43_Close(&m_hnd);
    }

    bool LTR43::isOpened() const {
        QMutexLocker lock(&m_io_mutex);
        return LTR43_IsOpened(&m_hnd) == LTR_OK;
    }

    DevSyncMarksRecv *LTR43::devSyncMarkRecv() {
        return m_receiver.get();
    }

    QString LTR43::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTR43_GetErrorString(err));
    }

    const LTR43Config &LTR43::devspecConfig() const {
        return static_cast<const LTR43Config &>(config());
    }

    QSharedPointer<const LTR43Info> LTR43::devspecInfo() const {
        return devInfo().staticCast<const LTR43Info>();
    }

    TLTR *LTR43::channel() const {
        return &m_hnd.Channel;
    }

    int LTR43::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        QMutexLocker lock{&m_io_mutex};
        int ltr_err = LTR43_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, slot);
        if (ltr_err == LTR_OK) {
            m_stream_run = false;
            setDeviceInfo(LTR43Info{LTR43TypeInfo::defaultTypeInfo(),
                                    QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                    QSTRING_FROM_CSTR(m_hnd.ModuleInfo.FirmwareVersion)});
        }
        return ltr_err;
    }

    void LTR43::protClose(LQError &err) {
        QMutexLocker lock{&m_io_mutex};
        getError(LTR43_Close(&m_hnd), err);
    }

    void LTR43::protConfigure(const DeviceConfig &cfg, LQError &err) {
        QMutexLocker lock{&m_io_mutex};
        const LTR43Config *setCfg {qobject_cast<const LTR43Config *>(&cfg)};
        if (setCfg) {
            m_hnd.IO_Ports.Port1 = portCfgWord(setCfg, 0);
            m_hnd.IO_Ports.Port2 = portCfgWord(setCfg, 1);
            m_hnd.IO_Ports.Port3 = portCfgWord(setCfg, 2);
            m_hnd.IO_Ports.Port4 = portCfgWord(setCfg, 3);
            m_hnd.StreamReadRate = setCfg->din().inDigFreq();

            getError(LTR43_Config(&m_hnd), err);
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }

    void LTR43::protInAsyncDig(unsigned &val, LQError &err) {
        QMutexLocker lock{&m_io_mutex};
        DWORD rd_data;
        INT err_code = LTR43_ReadPort(&m_hnd, &rd_data);
        if (err_code == LTR_OK) {
            val = rd_data;
        } else {
            getError(err_code, err);
        }
    }

    void LTR43::protInDigStart(LQError &err) {
        QMutexLocker lock{&m_io_mutex};
        if (!m_stream_run) {
            getError(LTR43_StartStreamRead(&m_hnd), err);
            if (err.isSuccess()) {
                m_stream_run = true;
                m_receiver->start(2, 2);
            }
        }
    }

    void LTR43::protInDigStop(LQError &err) {
        QMutexLocker lock{&m_io_mutex};
        if (m_stream_run) {
            getError(LTR43_StopStreamRead(&m_hnd), err);
            if (err.isSuccess()) {
                m_stream_run = false;
            }
        }
    }

    void LTR43::protInDigGetData(quint32 *data, int size, unsigned tout, int &recvd_size, LQError &err) {
        int wrds_cnt {2*size};
        int recvd_words {0};
        DWORD *wrds;
        DWORD proc_size {0};

        m_receiver->getFrames(wrds_cnt, tout, wrds, recvd_words, err);
        if (err.isSuccess() && (recvd_words > 0)) {
            proc_size = static_cast<DWORD>(recvd_words);
            getError(LTR43_ProcessData(&m_hnd, wrds, reinterpret_cast<DWORD*>(data), &proc_size), err);
        }

        if (err.isSuccess())
            recvd_size = static_cast<int>(proc_size);
    }

    INT LTR43::ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) {
        INT ret {LTR_OK};
        QMutexLocker lock{&m_io_mutex};
        if (m_stream_run) {
            ret = LTR43_Recv(&m_hnd, wrds, marks, size, tout);
        }
        return ret;
    }

    void LTR43::protOutAsyncDigAll(unsigned val, LQError &err) {
        QMutexLocker lock{&m_io_mutex};
        getError(LTR43_WritePort(&m_hnd, val), err);
    }

    int LTR43::portCfgWord(const LTR43Config *cfg, int port_idx) {
        return cfg->portDirection(port_idx) == LTR43Config::Direction::Out ?
                    LTR43_PORT_DIR_OUT : LTR43_PORT_DIR_IN;
    }
}
