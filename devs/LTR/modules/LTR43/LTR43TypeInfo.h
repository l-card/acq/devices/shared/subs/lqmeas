#ifndef LQMEAS_LTR43TYPEINFO_H
#define LQMEAS_LTR43TYPEINFO_H


#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"
#include "lqmeas/ifaces/out/DevOutInfo.h"
#include "lqmeas/ifaces/in/dig/DevInDigInfo.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvInfo.h"

namespace LQMeas {
    class LTR43TypeInfo : public LTRModuleTypeInfo, public DevInDigInfo, public DevOutInfo, public LTRSyncMarksRecvInfo {
    public:
        static const QString &name();
        static const LTR43TypeInfo &defaultTypeInfo();

        const DevOutInfo *out() const override {return static_cast<const DevOutInfo *>(this);}
        const DevInDigInfo *digin() const override {return static_cast<const DevInDigInfo *>(this);}
        const DevSyncMarksRecvInfo *syncMarkRecv() const override {
            return static_cast<const DevSyncMarksRecvInfo *>(this);
        }

        QString deviceTypeName() const override {return name();}
        QList<const DeviceTypeInfo *> modificationList() const override;

        /* --------------- DevInDigInfo --------------------------------------*/
        int inDigChannelsCnt() const override {return 32;}
        bool inDigSyncSupport(void) const override {return true;}
        double inDigSyncFreqMax(void) const override {return 100 * 1000;}

        /* --------------- DevOutInfo ----------------------------------------*/
        bool outDacSyncSupport() const override {return false;}
        bool outDigSyncSupport() const override {return false;}
        bool outDacAsyncSupport() const override {return false;}
        bool outDigAsyncSupport() const override {return true;}
        bool outDacSyncModeCfgPerCh() const override {return false;}
        bool outSyncRamModeIsConfigurable() const override {return false;}
        bool outSyncDacChGenModeIsConfigurable() const override {return false;}
        bool outSyncDigChGenModeIsConfigurable() const override {return false;}
        double outSyncGenFreqMax() const override {return 0;}
        bool outSyncPresetSupport() const override {return false;}
        int outDacChannelsCnt() const override {return 0;}
        int outDigChannelsCnt() const override {return 32;}
        int outDacChRangesCnt(int ch) const override { Q_UNUSED(ch) return 0;}
        double outDacChRangeMaxVal(int ch, int range) const override {Q_UNUSED(ch) Q_UNUSED(range) return 0;}
        double outDacChRangeMinVal(int ch, int range) const override {Q_UNUSED(ch) Q_UNUSED(range) return 0;}
    };
}

#endif // LQMEAS_LTR43TYPEINFO_H
