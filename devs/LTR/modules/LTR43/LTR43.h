#ifndef LQMEAS_LTR43_H
#define LQMEAS_LTR43_H

#include "ltr/include/ltr43api.h"
#include "lqmeas/devs/LTR/modules/LTRModule.h"
#include "lqmeas/ifaces/in/dig/DevInAsyncDig.h"
#include "lqmeas/ifaces/in/dig/DevInSyncDig.h"
#include "lqmeas/ifaces/out/DevOutAsyncDigSwMask.h"
#include <QMutex>
#include <memory>

namespace LQMeas {
    class LTR43Config;
    class LTR43Info;
    class LTRModuleFrameReceiver;

    class LTR43 : public LTRModule, public DevInAsyncDig, public DevInSyncDig,
            public DevOutAsyncDigSwMask {
        Q_OBJECT
    public:
        static const quint16 typeModuleID {LTR_MID_LTR43};

        ~LTR43() override;
        bool isOpened() const override;

        DevOutAsyncDig *devOutAsyncDig() override { return static_cast<DevOutAsyncDig*>(this); }
        DevInAsyncDig *devInAsyncDig() override { return static_cast<DevInAsyncDig*>(this); }
        DevInSyncDig *devInSyncDig() override {return static_cast<DevInSyncDig*>(this);}
        DevSyncMarksRecv *devSyncMarkRecv() override;

        QString errorString(int err) const override;

        const LTR43Config &devspecConfig() const;
        QSharedPointer<const LTR43Info> devspecInfo() const;

        TLTR43 *rawHandle() {return &m_hnd;}
        const TLTR43 *rawHandle() const {return &m_hnd;}
    protected:
        TLTR* channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn,  int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;



        void protInAsyncDig(unsigned &val, LQError &err) override;
        void protInDigStart(LQError &err) override;
        void protInDigStop(LQError &err) override;
        void protInDigGetData(quint32 *data, int size,  unsigned tout, int &recvd_size, LQError &err) override;

        INT ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) override;

        void protOutAsyncDigAll(unsigned val, LQError &err) override;
    private:
        static int portCfgWord(const LTR43Config *cfg, int port_idx);

        explicit LTR43(LTRCrate *crate, int slot, QObject *parent = nullptr);
        mutable TLTR43 m_hnd;
        const std::unique_ptr<LTRModuleFrameReceiver> m_receiver;

        friend class LTRResolver;
        /* Используем мьютекс для возможности читать значения цифровых линий
         * из одного потока, а писать - из другого */
        mutable QMutex m_io_mutex;
        bool m_stream_run {false};
    };
}

#endif // LQMEAS_LTR43_H
