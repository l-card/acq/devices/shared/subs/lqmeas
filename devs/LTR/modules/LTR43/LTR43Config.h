#ifndef LQMEAS_LTR43CONFIG_H
#define LQMEAS_LTR43CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"
#include <memory>

namespace LQMeas {
    class LTR43OutConfig;
    class LTR43InConfig;
    class LTRSyncMarksRecvConfig;

    class LTR43Config : public DeviceConfig {
        Q_OBJECT
    public:


        enum class Direction {
            In,
            Out
        };

        const DevInDigConfig *inDigConfig() const override;
        const DevOutConfig *outConfig() const override;
        const DevSyncMarksRecvConfig *syncMarksRecvConfig() const override;

        LTR43OutConfig &out() {return  *m_out;}
        const LTR43OutConfig &out() const {return *m_out;}
        LTR43InConfig &din() {return  *m_in;}
        const LTR43InConfig &din() const {return *m_in;}
        LTRSyncMarksRecvConfig &marksRecv() {return  *m_syncRecv;}
        const LTRSyncMarksRecvConfig &marksRecv() const {return  *m_syncRecv;}

        static int portsCount() {return ports_count;}


        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        DeviceConfig *clone() const override {
            return new LTR43Config{*this};
        }

        LTR43Config();
        LTR43Config(const LTR43Config &cfg);
        ~LTR43Config() override;

        Direction portDirection(int portNum) const {return m_ports_dir[portNum];}
        Direction channelDirection(int chNum) const;

        void setPortDirection(int portNum, Direction dir);
    protected:
        void protUpdate() override;
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        static const int ports_count {4};
        static const int channels_per_ports {8};


        Direction m_ports_dir[ports_count];
        const std::unique_ptr<LTR43InConfig> m_in;
        const std::unique_ptr<LTR43OutConfig> m_out;
        const std::unique_ptr<LTRSyncMarksRecvConfig> m_syncRecv;
    };
}

Q_DECLARE_METATYPE(LQMeas::LTR43Config::Direction)

#endif // LQMEAS_LTR43CONFIG_H
