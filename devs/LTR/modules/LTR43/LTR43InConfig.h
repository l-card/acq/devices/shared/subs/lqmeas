#ifndef LQMEAS_LTR43INCONFIG_H
#define LQMEAS_LTR43INCONFIG_H

#include "lqmeas/ifaces/in/dig/DevInDigSyncBaseConfig.h"

namespace LQMeas {
    class LTR43Config;

    class LTR43InConfig : public DevInDigSyncBaseConfig {
        Q_OBJECT
    public:
        bool inDigChAvailable(int ch) const override;
        int inDigSampleSize() const override {return 8;}
    protected:
        void dinAdjustFreq(double &dinFreq) override;
    private:
        explicit LTR43InConfig(LTR43Config *devcfg, const DevInDigInfo &info);
        explicit LTR43InConfig(LTR43Config *devcfg, const LTR43InConfig &inCfg);

        LTR43Config *m_devcfg;

        friend class LTR43Config;
    };
}

#endif // LTR43INCONFIG_H
