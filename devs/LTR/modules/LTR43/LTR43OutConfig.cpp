#include "LTR43OutConfig.h"
#include "LTR43Config.h"

namespace LQMeas {
    bool LTR43OutConfig::outDigChAvailable(int ch) const {
        return m_devcfg->channelDirection(ch) == LTR43Config::Direction::Out;
    }

    LTR43OutConfig::LTR43OutConfig(LTR43Config *devcfg, const DevOutInfo &info) :
        DevOutConfig{info}, m_devcfg{devcfg} {

    }

    LTR43OutConfig::LTR43OutConfig(LTR43Config *devcfg, const LTR43OutConfig &outCfg) :
        DevOutConfig{outCfg}, m_devcfg{devcfg} {

    }
}
