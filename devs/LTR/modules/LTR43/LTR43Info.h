#ifndef LQMEAS_LTR43INFO_H
#define LQMEAS_LTR43INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTR43TypeInfo.h"

namespace LQMeas {
    class LTR43Info : public DeviceInfo {
    public:
        LTR43Info(const LTR43TypeInfo &type = LTR43TypeInfo::defaultTypeInfo(),
                  const QString &serial = QString{},
                  const QString &mcu_ver_str = QString{});

        DeviceInfo *clone() const override;
        const QString &mcuVerStr() const {return m_mcu_ver_str;}


        const LTR43TypeInfo &devspecTypeInfo() const {
            return static_cast<const LTR43TypeInfo &>(type());
        }
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        LTR43Info(const LTR43Info &info);

        QString m_mcu_ver_str;
    };
}

#endif // LQMEAS_LTR43INFO_H
