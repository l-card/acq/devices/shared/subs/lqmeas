#ifndef LQMEAS_LTRS412TYPEINFO_H
#define LQMEAS_LTRS412TYPEINFO_H


#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"
#include "lqmeas/ifaces/out/DevOutInfo.h"

namespace LQMeas {
    class LTRS412TypeInfo : public LTRModuleTypeInfo, public DevOutInfo {
    public:
        static const QString &name();
        QString deviceTypeName() const override {return name();}

        static const LTRS412TypeInfo &defaultTypeInfo();

        QList<const DeviceTypeInfo *> modificationList() const override;

        bool outDacSyncSupport() const override {return false;}
        bool outDigSyncSupport() const override {return false;}
        bool outDacAsyncSupport() const override {return false;}
        bool outDigAsyncSupport() const override {return true;}
        bool outDacSyncModeCfgPerCh() const override {return false;}
        bool outSyncRamModeIsConfigurable() const override {return false;}
        bool outSyncDacChGenModeIsConfigurable() const override {return false;}
        bool outSyncDigChGenModeIsConfigurable() const override {return false;}
        double outSyncGenFreqMax() const override {return 0;}
        bool outSyncPresetSupport() const override {return false;}
        int outDacChannelsCnt() const override {return 0;}
        int outDigChannelsCnt() const override;
        int outDacChRangesCnt(int ch) const override {Q_UNUSED(ch) return 0;}
        double outDacChRangeMaxVal(int ch, int range) const override { Q_UNUSED(ch) Q_UNUSED(range) return 0;}
        double outDacChRangeMinVal(int ch, int range) const override { Q_UNUSED(ch) Q_UNUSED(range) return 0;}
    private:
        LTRS412TypeInfo();
    };
}


#endif // LQMEAS_LTRS412TYPEINFO_H
