#ifndef LQMEAS_LTRS412_H
#define LQMEAS_LTRS412_H

#include "ltr/include/ltrs412api.h"
#include "lqmeas/devs/LTR/modules/LTR01.h"
#include "lqmeas/ifaces/out/DevOutAsyncDigSwMask.h"

namespace LQMeas {
    class LTRS412Config;
    class LTRS412Info;

    class LTRS412  : public LTR01, public DevOutAsyncDigSwMask {
        Q_OBJECT
    public:
        static const e_LTR01_SUBID typeModuleSubID {LTR01_SUBID_LTRS412};


        ~LTRS412() override;
        bool isOpened() const override;

        DevOutAsyncDig *devOutAsyncDig() override {
            return static_cast<DevOutAsyncDig*>(this);
        }


        QString errorString(int err) const override;

        const LTRS412Config &devspecConfig() const;
        QSharedPointer<const LTRS412Info> devspecInfo() const;

        TLTRS412 *rawHandle() {return &m_hnd;}
        const TLTRS412 *rawHandle() const {return &m_hnd;}
    protected:
        TLTR *channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;

        void protOutAsyncDigAll(unsigned val, LQError &err) override;
    private:
        explicit LTRS412(LTRCrate *crate, int slot, QObject *parent = nullptr);

        mutable TLTRS412 m_hnd;

        friend class LTRResolver;
    };
}

#endif // LQMEAS_LTRS412_H
