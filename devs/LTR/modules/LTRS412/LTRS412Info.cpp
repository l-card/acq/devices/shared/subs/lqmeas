#include "LTRS412Info.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_pld_ver           {StdConfigKeys::pldVer()};

    LTRS412Info::LTRS412Info(const LTRS412TypeInfo &type, const QString &serial, int pld_ver) :
        DeviceInfo{type, serial}, m_pld_ver{pld_ver} {

    }

    LTRS412Info::LTRS412Info(const LTRS412Info &info) :
        DeviceInfo{info},
        m_pld_ver{info.m_pld_ver} {

    }

    QString LTRS412Info::pldVerStr() const {
        return QString::number(m_pld_ver);
    }

    void LTRS412Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_pld_ver] = m_pld_ver;
    }

    void LTRS412Info::protLoad(const QJsonObject &infoObj) {
        m_pld_ver = infoObj[cfgkey_pld_ver].toInt();
    }

}
