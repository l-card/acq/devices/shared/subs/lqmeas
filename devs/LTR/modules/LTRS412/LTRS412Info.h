#ifndef LQMEAS_LTRS412INFO_H
#define LQMEAS_LTRS412INFO_H


#include "lqmeas/devs/DeviceInfo.h"
#include "LTRS412TypeInfo.h"

namespace LQMeas {
    class LTRS412Info: public DeviceInfo {
    public:
        explicit LTRS412Info(const LTRS412TypeInfo &type = LTRS412TypeInfo::defaultTypeInfo(),
                            const QString &serial = QString{}, int pld_ver = 0);
        LTRS412Info(const LTRS412Info &info);

        int pldVer() const {return m_pld_ver;}
        QString pldVerStr() const;

        DeviceInfo *clone() const override {return new LTRS412Info{*this};}
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        int m_pld_ver;
    };
}


#endif // LQMEAS_LTRS412INFO_H
