#include "LTRS412.h"
#include "LTRS412Config.h"
#include "LTRS412Info.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdErrors.h"


namespace LQMeas {
    LTRS412::~LTRS412() {
        LTRS412_Close(&m_hnd);
    }

    bool LTRS412::isOpened() const  {
        return LTRS412_IsOpened(&m_hnd) == LTR_OK;
    }

    QString LTRS412::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTRS412_GetErrorString(err));;
    }

    const LTRS412Config &LTRS412::devspecConfig() const {
        return static_cast<const LTRS412Config &>(config());
    }


    QSharedPointer<const LTRS412Info> LTRS412::devspecInfo() const {
        return devInfo().staticCast<const LTRS412Info>();
    }

    TLTR *LTRS412::channel() const {
        return &m_hnd.Channel;
    }

    int LTRS412::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        INT ltr_err = LTRS412_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, static_cast<WORD>(slot));
        if (LTRS412_IsOpened(&m_hnd) == LTR_OK) {
            setDeviceInfo(LTRS412Info{LTRS412TypeInfo::defaultTypeInfo(),
                                      QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                      m_hnd.ModuleInfo.VerPLD});
        }
        return ltr_err;
    }

    void LTRS412::protClose(LQError &err) {
        getError(LTRS412_Close(&m_hnd), err);
    }

    void LTRS412::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTRS412Config *setCfg {qobject_cast<const LTRS412Config *>(&cfg)};
        if (!setCfg) {
            err += StdErrors::InvalidConfigType();
        }
    }

    void LTRS412::protOutAsyncDigAll(unsigned val, LQError &err) {
        getError(LTRS412_SetRelayStates(&m_hnd, val & 0xFFFF), err);
    }

    LTRS412::LTRS412(LTRCrate *crate, int slot, QObject *parent) :
        LTR01{crate, slot, typeModuleSubID, new LTRS412Config{}, LTRS412Info{}, parent} {

        LTRS412_Init(&m_hnd);
    }

}
