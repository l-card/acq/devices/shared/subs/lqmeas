#include "LTRS412TypeInfo.h"
#include "ltr/include/ltrs412api.h"

namespace LQMeas {
    const QString &LTRS412TypeInfo::name() {
        static const QString str {QStringLiteral("LTRS412") };
        return str;
    }
    
    const LTRS412TypeInfo &LTRS412TypeInfo::defaultTypeInfo() {
        static const LTRS412TypeInfo type;
        return type;
    }

    QList<const DeviceTypeInfo *> LTRS412TypeInfo::modificationList() const  {
        static const QList<const DeviceTypeInfo *> list {
                                                        &defaultTypeInfo()
        };
        return list;
    }

    int LTRS412TypeInfo::outDigChannelsCnt() const {
        return LTRS412_RELAY_CNT;
    }

    LTRS412TypeInfo::LTRS412TypeInfo() {

    }
}
