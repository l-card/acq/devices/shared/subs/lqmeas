#ifndef LQMEAS_LTRS412CONFIG_H
#define LQMEAS_LTRS412CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"

namespace LQMeas {
    class LTRS412Config: public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        DeviceConfig *clone() const override {
            return new LTRS412Config{*this};
        }


        explicit LTRS412Config();
        LTRS412Config(const LTRS412Config &cfg);
    private:
    };
}

#endif // LQMEAS_LTRS412CONFIG_H
