#include "LTR12AdcConfig.h"
#include "LTR12Config.h"
#include "LTR12TypeInfo.h"
#include "ltr/include/ltr12api.h"
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_sync_start_mode   {StdConfigKeys::syncStartMode()};
    static const QLatin1String cfgkey_sync_conv_mode    {StdConfigKeys::syncConvMode()};
    static const QLatin1String cfgkey_zero_meas_mode    {"ZeroMeasMode"};
    static const QLatin1String cfgkey_bg_ch_cnt         {"BgChCnt"};
    static const QLatin1String cfgkey_adc_cbr_dis       {"AdcCbrDis"};
    static const QLatin1String cfgkey_adc_raw_code_mode {"AdcRawCodeMode"};

    typedef EnumConfigKey<LTR12AdcConfig::StartMode> SyncModeKey;
    static const SyncModeKey start_modes[] = {
        {QStringLiteral("int"),     LTR12AdcConfig::StartMode::Internal},
        {QStringLiteral("extrise"), LTR12AdcConfig::StartMode::ExtRise},
        {QStringLiteral("extfall"), LTR12AdcConfig::StartMode::ExtFall},
        {SyncModeKey::defaultKey(), LTR12AdcConfig::StartMode::Internal},
    };

    typedef EnumConfigKey<LTR12AdcConfig::ConvMode> ConvModeKey;
    static const ConvModeKey conv_modes[] = {
        {QStringLiteral("int"),     LTR12AdcConfig::ConvMode::Internal},
        {QStringLiteral("extrise"), LTR12AdcConfig::ConvMode::ExtRise},
        {QStringLiteral("extfall"), LTR12AdcConfig::ConvMode::ExtFall},
        {ConvModeKey::defaultKey(), LTR12AdcConfig::ConvMode::Internal},
    };

    typedef EnumConfigKey<LTR12AdcConfig::ZeroMeasMode> ZeroMeasModeKey;
    static const ZeroMeasModeKey zeromeas_modes[] = {
        {QStringLiteral("off"),         LTR12AdcConfig::ZeroMeasMode::Off},
        {QStringLiteral("onstart"),     LTR12AdcConfig::ZeroMeasMode::OnStart},
        {QStringLiteral("onoperation"), LTR12AdcConfig::ZeroMeasMode::OnOperation},
        {ZeroMeasModeKey::defaultKey(), LTR12AdcConfig::ZeroMeasMode::OnStart},
    };


    LTR12AdcConfig::LTR12AdcConfig(const DevAdcInfo &info) :
        DevAdcStdSeq32Config{info} {

    }

    LTR12AdcConfig::LTR12AdcConfig(const LTR12AdcConfig &adcCfg) :
        DevAdcStdSeq32Config{adcCfg},
        m_convMode{adcCfg.m_convMode},
        m_startMode{adcCfg.m_startMode},
        m_zeroMeasMode{adcCfg.m_zeroMeasMode},
        m_bg_ch_cnt{adcCfg.m_bg_ch_cnt},
        m_adc_cbr_dis{adcCfg.m_adc_cbr_dis},
        m_adc_raw_code{adcCfg.m_adc_raw_code} {

    }

    bool LQMeas::LTR12AdcConfig::adcExternalStart() const {
        return (m_startMode != LTR12AdcConfig::StartMode::Internal) ||
                (m_convMode != LTR12AdcConfig::ConvMode::Internal);
    }

    int LTR12AdcConfig::minBgChCnt() const {
        return ((m_zeroMeasMode == ZeroMeasMode::OnOperation) && !adcHasZeroChannels()) ? 1 : 0;
    }

    void LTR12AdcConfig::setStartMode(LTR12AdcConfig::StartMode mode) {
        if (m_startMode != mode) {
            m_startMode = mode;
            notifyConfigChanged();
        }
    }

    void LTR12AdcConfig::setConvMode(LTR12AdcConfig::ConvMode mode)  {
        if (m_convMode != mode) {
            m_convMode = mode;
            notifyConfigChanged();
        }
    }

    void LTR12AdcConfig::setZeroMeasMode(LTR12AdcConfig::ZeroMeasMode mode) {
        if (m_zeroMeasMode != mode) {
            m_zeroMeasMode = mode;
            if (m_bg_ch_cnt < minBgChCnt())
                setBgChCnt(minBgChCnt());
            notifyConfigChanged();
        }
    }

    void LTR12AdcConfig::setBgChCnt(int cnt) {
        if (cnt < 0)
            cnt = 0;
        if (cnt > LTR12_MAX_LCHANNELS_CNT)
            cnt = LTR12_MAX_LCHANNELS_CNT;
        if (cnt != m_bg_ch_cnt) {
            m_bg_ch_cnt = cnt;
            notifyConfigChanged();
            Q_EMIT bgChCountChanged();
        }
    }

    void LTR12AdcConfig::adcSetCbrDisabled(bool dis) {
        if (m_adc_cbr_dis != dis) {
            m_adc_cbr_dis = dis;
            notifyConfigChanged();
        }
    }

    void LTR12AdcConfig::adcSetRawCodeMode(bool rawCode) {
        if (m_adc_raw_code != rawCode) {
            m_adc_raw_code = rawCode;
            notifyConfigChanged();
        }
    }

    void LTR12AdcConfig::protSave(QJsonObject &cfgObj) const {
        DevAdcStdSeq32Config::protSave(cfgObj);

        cfgObj[cfgkey_sync_start_mode] = SyncModeKey::getKey(start_modes, m_startMode);
        cfgObj[cfgkey_sync_conv_mode] = ConvModeKey::getKey(conv_modes, m_convMode);
        cfgObj[cfgkey_zero_meas_mode] = ZeroMeasModeKey::getKey(zeromeas_modes, m_zeroMeasMode);
        cfgObj[cfgkey_bg_ch_cnt] = m_bg_ch_cnt;
        cfgObj[cfgkey_adc_cbr_dis] = m_adc_cbr_dis;
        cfgObj[cfgkey_adc_raw_code_mode] = m_adc_raw_code;
    }

    void LTR12AdcConfig::protLoad(const QJsonObject &cfgObj) {
        DevAdcStdSeq32Config::protLoad(cfgObj);

        m_startMode = SyncModeKey::getValue(start_modes, cfgObj[cfgkey_sync_start_mode].toString());
        m_convMode = ConvModeKey::getValue(conv_modes, cfgObj[cfgkey_sync_conv_mode].toString());
        m_zeroMeasMode = ZeroMeasModeKey::getValue(zeromeas_modes, cfgObj[cfgkey_zero_meas_mode].toString());
        m_bg_ch_cnt = cfgObj[cfgkey_bg_ch_cnt].toInt();
        m_adc_cbr_dis = cfgObj[cfgkey_adc_cbr_dis].toBool();
        m_adc_raw_code = cfgObj[cfgkey_adc_raw_code_mode].toBool();
    }

    void LTR12AdcConfig::adcAdjustFreq(double &adcFreq, double &adcChFreq) {
        LTR12_FindAdcFreqParams(adcFreq, nullptr, &adcFreq);
        if (adcEnabledChCnt() > 0) {
            adcChFreq = adcFreq/(adcEnabledChCnt() + m_bg_ch_cnt);
        }
    }
}
