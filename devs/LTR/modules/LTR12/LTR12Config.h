#ifndef LQMEAS_LTR12CONFIG_H
#define LQMEAS_LTR12CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"
#include <memory>

namespace LQMeas {
    class LTRSyncMarksRecvConfig;
    class LTR12AdcConfig;

    class LTR12Config : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        const DevAdcConfig *adcConfig() const override;
        const DevSyncMarksRecvConfig *syncMarksRecvConfig() const override;

        LTR12AdcConfig &adc() {return  *m_adc;}
        const LTR12AdcConfig &adc() const {return  *m_adc;}

        LTRSyncMarksRecvConfig &marksRecv() {return  *m_syncRecv;}
        const LTRSyncMarksRecvConfig &marksRecv() const {return  *m_syncRecv;}

        DeviceConfig *clone() const override {
            return new LTR12Config{*this};
        }

        LTR12Config();
        LTR12Config(const LTR12Config &cfg);
        ~LTR12Config() override;
    protected:
        void protUpdate() override;
    private:
        std::unique_ptr<LTR12AdcConfig> m_adc;
        std::unique_ptr<LTRSyncMarksRecvConfig> m_syncRecv;
    };
}

#endif // LQMEAS_LTR12CONFIG_H
