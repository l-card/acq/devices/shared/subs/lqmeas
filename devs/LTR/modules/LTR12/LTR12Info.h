#ifndef LQMEAS_LTR12INFO_H
#define LQMEAS_LTR12INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTR12TypeInfo.h"

namespace LQMeas {
    class LTR12Info : public DeviceInfo {
    public:
        LTR12Info(const LTR12TypeInfo &type = LTR12TypeInfo::defaultTypeInfo(),
                  const QString &serial = QString{}, int mcu_ver = 0);

        DeviceInfo *clone() const override;
        int mcuVer() const {return m_mcu_ver;}
        QString mcuVerStr() const;


        const LTR12TypeInfo &devspecTypeInfo() const {
            return static_cast<const LTR12TypeInfo &>(type());
        }
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        LTR12Info(const LTR12Info &info);

        int m_mcu_ver;
    };
}

#endif // LQMEAS_LTR12INFO_H
