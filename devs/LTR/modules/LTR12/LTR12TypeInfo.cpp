#include "LTR12TypeInfo.h"
#include "ltr/include/ltr12api.h"
#include "lqmeas/units/std/Current.h"

namespace LQMeas {
    const QString &LTR12TypeInfo::name() {
        static const QString str {QStringLiteral("LTR12")};
        return str;
    }

    int LTR12TypeInfo::adcChRangesCnt(int ch_num, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return LTR12_RANGES_CNT;
    }

    double LTR12TypeInfo::adcChRangeMaxVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(range) Q_UNUSED(unit_mode_num)
         return LTR12_ADC_RANGE_MA;
    }

    double LTR12TypeInfo::adcChRangeMinVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(range) Q_UNUSED(unit_mode_num)
        return -LTR12_ADC_RANGE_MA;
    }

    const Unit &LTR12TypeInfo::adcChRangeUnit(int ch_num, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return Units::Current::mA();
    }

    QList<const DeviceTypeInfo *> LTR12TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list {
                                                        &defaultTypeInfo()
        };
        return list;
    }
    
    const LTR12TypeInfo &LTR12TypeInfo::defaultTypeInfo() {
        static const LTR12TypeInfo info;
        return info;
    }
}
