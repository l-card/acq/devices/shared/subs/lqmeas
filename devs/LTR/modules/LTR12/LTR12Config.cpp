#include "LTR12Config.h"
#include "LTR12TypeInfo.h"
#include "LTR12AdcConfig.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvConfig.h"

namespace LQMeas {

    const QString &LTR12Config::typeConfigName() {
        return LTR12TypeInfo::name();
    }

    const DevAdcConfig *LTR12Config::adcConfig() const {
        return m_adc.get();
    }

    const DevSyncMarksRecvConfig *LTR12Config::syncMarksRecvConfig() const {
        return m_syncRecv.get();
    }

    LTR12Config::LTR12Config() :
        m_adc{new LTR12AdcConfig{LTR12TypeInfo::defaultTypeInfo()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this}} {

        init();
    }

    LTR12Config::LTR12Config(const LTR12Config &cfg) :
        m_adc{new LTR12AdcConfig{cfg.adc()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this, cfg.marksRecv()}} {

        init();
    }

    void LTR12Config::protUpdate() {
        m_syncRecv->updateSyncMarksRecvFreq(m_adc->adcFreq());
    }

    LQMeas::LTR12Config::~LTR12Config() {

    }
}
