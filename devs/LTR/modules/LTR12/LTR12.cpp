#include "LTR12.h"
#include "LTR12Info.h"
#include "LTR12Config.h"
#include "LTR12AdcConfig.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "lqmeas/devs/LTR/modules/LTRModuleFrameReceiver.h"
#include "lqmeas/log/Log.h"
#include "lqmeas/StdErrors.h"


namespace LQMeas {
    LTR12::LTR12(LTRCrate *crate, int slot, QObject *parent) :
        LTRModule{crate, slot, typeModuleID, new LTR12Config{}, LTR12Info{}, parent},
        DevAdc{this, LTR12AdcConfig::adc_channels_cnt},
        m_receiver{new LTRModuleFrameReceiver{this}} {

        LTR12_Init(&m_hnd);
    }

    LTR12::~LTR12() {
        LTR12_Close(&m_hnd);
    }

    bool LTR12::isOpened() const {
        return LTR12_IsOpened(&m_hnd) == LTR_OK;
    }

    DevSyncMarksRecv *LTR12::devSyncMarkRecv() {
        return m_receiver.get();
    }

    QString LTR12::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTR12_GetErrorString(err));
    }

    const LTR12Config &LTR12::devspecConfig() const {
        return static_cast<const LTR12Config &>(config());
    }

    QSharedPointer<const LTR12Info> LTR12::devspecInfo() const {
        return devInfo().staticCast<const LTR12Info>();
    }

    TLTR *LTR12::channel() const {
        return &m_hnd.Channel;
    }

    int LTR12::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        Q_UNUSED(flags)
        INT ltr_err = LTR12_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, slot);
        if (LTR12_IsOpened(&m_hnd) == LTR_OK) {
            setDeviceInfo(LTR12Info(LTR12TypeInfo::defaultTypeInfo(),
                                    QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                    m_hnd.ModuleInfo.FirmwareVersion));
        }
        return ltr_err;
    }


    void LTR12::protClose(LQError &err) {
        return getError(LTR12_Close(&m_hnd), err);
    }

    void LTR12::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTR12Config *setCfg {qobject_cast<const LTR12Config *>(&cfg)};
        if (setCfg) {
            LTR12_FillAdcFreqParams(&m_hnd, setCfg->adc().adcFreq(), nullptr);
            m_hnd.Cfg.ConvStartSrc = setCfg->adc().convMode() == LTR12AdcConfig::ConvMode::ExtFall ? LTR12_CONV_START_SRC_EXT_FALL :
                                     setCfg->adc().convMode() == LTR12AdcConfig::ConvMode::ExtRise ? LTR12_CONV_START_SRC_EXT_RISE :
                                                                                                     LTR12_CONV_START_SRC_INT ;
            m_hnd.Cfg.AcqStartSrc = setCfg->adc().startMode() == LTR12AdcConfig::StartMode::ExtFall ? LTR12_ACQ_START_SRC_EXT_FALL :
                                    setCfg->adc().startMode() == LTR12AdcConfig::StartMode::ExtRise ? LTR12_ACQ_START_SRC_EXT_RISE :
                                                                                                      LTR12_ACQ_START_SRC_INT;

            int ch_cnt {0};
            for (int ch {0}; ch < LTR12AdcConfig::adc_channels_cnt; ++ch) {
                if (setCfg->adc().adcChEnabled(ch)) {
                    LTR12AdcConfig::AdcChMode mode = setCfg->adc().adcChMode(ch);
                    BYTE mode_code = mode == LTR12AdcConfig::AdcChMode::Comm ? LTR12_CH_MODE_COMM :
                                     (mode == LTR12AdcConfig::AdcChMode::Zero ? LTR12_CH_MODE_ZERO : LTR12_CH_MODE_DIFF);

                    LTR12_FillLChannel(&m_hnd, ch_cnt++, static_cast<BYTE>(ch), mode_code);
                }
            }
            m_hnd.Cfg.LChCnt = ch_cnt;
            m_hnd.Cfg.BgLChCnt = setCfg->adc().bgChCnt();


            DWORD flags {0};
            if (!setCfg->adc().adcRawCodeMode())
                flags |= LTR12_PROC_FLAG_CONV_UNIT;
            if (!setCfg->adc().adcCbrDisabled())
                flags |= LTR12_PROC_FLAG_CALIBR;
            if (setCfg->adc().zeroMeasMode() != LTR12AdcConfig::ZeroMeasMode::Off)
                flags |= LTR12_PROC_FLAG_ZERO_OFFS_COR;
            if (setCfg->adc().zeroMeasMode() == LTR12AdcConfig::ZeroMeasMode::OnOperation)
                flags |= LTR12_PROC_FLAG_ZERO_OFFS_AUTORECALC;

            m_adc_flags = flags;

            getError(LTR12_SetADC(&m_hnd), err);
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }

    INT LTR12::ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) {
        return LTR12_Recv(&m_hnd, wrds, marks, size, tout);
    }

    void LTR12::protAdcPreStart(LQError &err) {
        if (m_adc_flags & LTR12_PROC_FLAG_ZERO_OFFS_COR) {
            getError(LTR12_MeasAdcZeroOffset(&m_hnd, 0), err);
        }
    }

    void LTR12::protAdcStart(LQError &err) {
        getError(LTR12_Start(&m_hnd), err);
        if (err.isSuccess()) {
            m_run = true;
            m_receiver->start(adcConfig().adcEnabledChCnt(), 1);
        }
    }

    void LTR12::protAdcStop(LQError &err) {
        getError(LTR12_Stop(&m_hnd), err);
        m_run = false;
        m_receiver->clear();
    }

    void LTR12::protAdcGetData(double *data, int size, unsigned flags,
                               unsigned tout, int &recvd_size, LQError &err) {
        Q_UNUSED(flags)
        int recvd_words {0};
        DWORD *wrds;
        INT proc_size {0};

        m_receiver->getFrames(size, tout, wrds, recvd_words, err);

        if (err.isSuccess() && (recvd_words > 0)) {
            proc_size = static_cast<INT>(recvd_words);
            getError(LTR12_ProcessData(&m_hnd, wrds, data, &proc_size, m_adc_flags), err);
            if (!err.isSuccess()) {
                LQMeasLog->error(tr("Process data error"), err, this);
            }
        }

        if (err.isSuccess())
            recvd_size = proc_size;
    }
}
