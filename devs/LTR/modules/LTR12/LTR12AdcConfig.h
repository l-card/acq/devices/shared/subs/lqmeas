#ifndef LQMEAS_LTR12ADCCONFIG_H
#define LQMEAS_LTR12ADCCONFIG_H

#include "lqmeas/ifaces/in/adc/DevAdcStdSeq32Config.h"

namespace LQMeas {
    class LTR12Config;

    class LTR12AdcConfig : public DevAdcStdSeq32Config {
        Q_OBJECT
    public:
        enum class StartMode {
            Internal,
            ExtRise,
            ExtFall
        };

        enum class ConvMode {
            Internal,
            ExtRise,
            ExtFall
        };

        enum class ZeroMeasMode {
            Off,
            OnStart,
            OnOperation
        };


        bool adcExternalStart() const override;

        StartMode startMode() const {return m_startMode;}
        ConvMode convMode() const {return m_convMode;}
        ZeroMeasMode zeroMeasMode() const {return m_zeroMeasMode;}
        int bgChCnt() const {return m_bg_ch_cnt;}
        int minBgChCnt() const;
        bool adcCbrDisabled() const {return m_adc_cbr_dis;}
        bool adcRawCodeMode() const {return m_adc_raw_code;}

        int adcSampleSize() const override {return 4;}
    public Q_SLOTS:
        void setStartMode(StartMode mode);
        void setConvMode(ConvMode mode);
        void setZeroMeasMode(ZeroMeasMode mode);
        void setBgChCnt(int cnt);
        void adcSetCbrDisabled(bool dis);
        void adcSetRawCodeMode(bool rawCode);
    Q_SIGNALS:
        void bgChCountChanged();
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
        void adcAdjustFreq(double &adcFreq, double &adcChFreq) override;
    private:
        explicit LTR12AdcConfig(const DevAdcInfo &info);
        LTR12AdcConfig(const LTR12AdcConfig &adcCfg);

        ConvMode m_convMode {ConvMode::Internal};
        StartMode m_startMode {StartMode::Internal};
        ZeroMeasMode m_zeroMeasMode {ZeroMeasMode::OnStart};
        int m_bg_ch_cnt {0};
        bool m_adc_cbr_dis {false};
        bool m_adc_raw_code {false};

        friend class LTR12Config;
    };
}


Q_DECLARE_METATYPE(LQMeas::LTR12AdcConfig::StartMode);
Q_DECLARE_METATYPE(LQMeas::LTR12AdcConfig::ConvMode);
Q_DECLARE_METATYPE(LQMeas::LTR12AdcConfig::ZeroMeasMode);

#endif // LQMEAS_LTR12ADCCONFIG_H
