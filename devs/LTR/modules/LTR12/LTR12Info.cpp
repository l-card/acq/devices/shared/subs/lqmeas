#include "LTR12Info.h"
#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String &cfgkey_mcu_ver              {StdConfigKeys::mcuVer()};

    LTR12Info::LTR12Info(const LTR12TypeInfo &type, const QString &serial, int mcu_ver) :
        DeviceInfo{type, serial}, m_mcu_ver{mcu_ver} {

    }

    LTR12Info::LTR12Info(const LTR12Info &info) : DeviceInfo{info}, m_mcu_ver{info.m_mcu_ver} {
    }

    DeviceInfo *LTR12Info::clone() const {
        return new LTR12Info{*this};
    }

    QString LTR12Info::mcuVerStr() const {
        return QString("%1.%2").arg((m_mcu_ver >> 8) & 0xFF).arg(m_mcu_ver & 0xFF);
    }

    void LTR12Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_mcu_ver] = m_mcu_ver;
    }

    void LTR12Info::protLoad(const QJsonObject &infoObj) {
        m_mcu_ver = infoObj[cfgkey_mcu_ver].toInt();
    }
}
