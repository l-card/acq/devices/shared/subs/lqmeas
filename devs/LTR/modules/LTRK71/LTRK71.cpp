#include "LTRK71.h"
#include "LTRK71Config.h"
#include "LTRK71Info.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdErrors.h"

namespace LQMeas {
    LTRK71::~LTRK71() {
        LTRK71_Close(&m_hnd);
    }

    bool LTRK71::isOpened() const  {
        return LTRK71_IsOpened(&m_hnd) == LTR_OK;
    }

    QString LTRK71::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTRK71_GetErrorString(err));;
    }

    const LTRK71Config &LTRK71::devspecConfig() const {
        return static_cast<const LTRK71Config &>(config());
    }


    QSharedPointer<const LTRK71Info> LTRK71::devspecInfo() const {
        return devInfo().staticCast<const LTRK71Info>();
    }

    TLTR *LTRK71::channel() const {
        return &m_hnd.Channel;
    }

    int LTRK71::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        INT ltr_err = LTRK71_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, static_cast<WORD>(slot));
        if (LTRK71_IsOpened(&m_hnd) == LTR_OK) {
            setDeviceInfo(LTRK71Info{LTRK71TypeInfo::defaultInfo(),
                                     QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                     m_hnd.ModuleInfo.VerPLD});
        }
        return ltr_err;
    }

    void LTRK71::protClose(LQError &err) {
        getError(LTRK71_Close(&m_hnd), err);
    }

    void LTRK71::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTRK71Config *setCfg {qobject_cast<const LTRK71Config *>(&cfg)};
        if (!setCfg) {
            err += StdErrors::InvalidConfigType();
        }
    }


    LTRK71::LTRK71(LTRCrate *crate, int slot, QObject *parent) :
        LTR01{crate, slot, typeModuleSubID, new LTRK71Config{}, LTRK71Info{}, parent} {

        LTRK71_Init(&m_hnd);
    }

}
