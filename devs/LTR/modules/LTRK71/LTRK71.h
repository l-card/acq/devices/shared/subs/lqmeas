#ifndef LQMEAS_LTRK71_H
#define LQMEAS_LTRK71_H


#include "ltr/include/ltrk71api.h"
#include "lqmeas/devs/LTR/modules/LTR01.h"

namespace LQMeas {
    class LTRK71Config;
    class LTRK71Info;

    class LTRK71  : public LTR01 {
        Q_OBJECT
    public:
        static const e_LTR01_SUBID typeModuleSubID {LTR01_SUBID_LTRK71};

        ~LTRK71() override;
        bool isOpened() const override;

        QString errorString(int err) const override;

        const LTRK71Config &devspecConfig() const;
        QSharedPointer<const LTRK71Info> devspecInfo() const;

        TLTRK71 *rawHandle() {return &m_hnd;}
        const TLTRK71 *rawHandle() const {return &m_hnd;}

    protected:
        TLTR *channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;
    private:
        explicit LTRK71(LTRCrate *crate, int slot, QObject *parent = nullptr);

        mutable TLTRK71 m_hnd;

        friend class LTRResolver;
    };
}


#endif // LQMEAS_LTRK71_H
