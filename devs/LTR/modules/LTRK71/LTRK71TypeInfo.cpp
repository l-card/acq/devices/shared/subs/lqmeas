#include "LTRK71TypeInfo.h"
#include "ltr/include/ltrk71api.h"
#include <QStringBuilder>

namespace LQMeas {
    const QString &LTRK71TypeInfo::name() {
        static const QString str {QStringLiteral("LTRK71") };
        return str;
    }

    const LTRK71TypeInfo &LTRK71TypeInfo::defaultInfo() {
        static const LTRK71TypeInfo type;
        return type;
    }



    QList<const DeviceTypeInfo *> LTRK71TypeInfo::modificationList() const  {
        static const QList<const DeviceTypeInfo *> list {
            &defaultInfo()
        };
        return list;
    }


    LTRK71TypeInfo::LTRK71TypeInfo() {

    }
}
