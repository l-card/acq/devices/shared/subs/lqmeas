#ifndef LQMEAS_LTRK71TYPEINFO_H
#define LQMEAS_LTRK71TYPEINFO_H


#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"

namespace LQMeas {
    class LTRK71TypeInfo : public LTRModuleTypeInfo {
    public:
        static const QString &name();
        QString deviceTypeName() const override {return name();}

        static const LTRK71TypeInfo &defaultInfo();

        QList<const DeviceTypeInfo *> modificationList() const override;
    private:
        LTRK71TypeInfo();
    };
}
#endif // LQMEAS_LTRK71TYPEINFO_H
