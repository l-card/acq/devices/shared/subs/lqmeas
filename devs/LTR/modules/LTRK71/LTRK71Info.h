#ifndef LQMEAS_LTRK71INFO_H
#define LQMEAS_LTRK71INFO_H


#include "lqmeas/devs/DeviceInfo.h"
#include "LTRK71TypeInfo.h"

namespace LQMeas {
    class LTRK71Info: public DeviceInfo {
    public:
        explicit LTRK71Info(const LTRK71TypeInfo &type = LTRK71TypeInfo::defaultInfo(),
                            const QString &serial = QString{}, int pld_ver = 0);
        LTRK71Info(const LTRK71Info &info);

        int pldVer() const {return m_pld_ver;}
        QString pldVerStr() const;

        const LTRK71TypeInfo &devspecType() const {return static_cast<const LTRK71TypeInfo &>(type());}

        DeviceInfo *clone() const override {return new LTRK71Info{*this};}
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        int m_pld_ver;
    };
}

#endif // LQMEAS_LTRK71INFO_H
