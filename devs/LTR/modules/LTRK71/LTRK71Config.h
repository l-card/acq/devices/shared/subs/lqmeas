#ifndef LQMEAS_LTRK71CONFIG_H
#define LQMEAS_LTRK71CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"

namespace LQMeas {
    class LTRK71Config: public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        DeviceConfig *clone() const override {
            return new LTRK71Config{*this};
        }


        explicit LTRK71Config();
        LTRK71Config(const LTRK71Config &cfg);
    private:
    };
}

#endif // LQMEAS_LTRK71CONFIG_H
