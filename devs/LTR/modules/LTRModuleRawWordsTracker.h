#ifndef LQMEAS_LTRMODULERAWWORDSTRACKER_H
#define LQMEAS_LTRMODULERAWWORDSTRACKER_H

#include "ltr/include/ltrapitypes.h"

namespace LQMeas {
    class LTRModuleRawWordsTracker {
    public:
        virtual ~LTRModuleRawWordsTracker();
    protected:
        virtual void procRecvdRawWords(const DWORD *wrds, const DWORD *marks, int size) = 0;
        virtual void procSentRawWords(const DWORD *wrds, int size) = 0;

        friend class LTRModule;
    };
}

#endif // LQMEAS_LTRMODULERAWWORDSTRACKER_H
