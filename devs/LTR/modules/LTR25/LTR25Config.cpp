#include "LTR25Config.h"
#include "LTR25AdcConfig.h"
#include "LTR25AdcICPConfig.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvConfig.h"
namespace LQMeas {
    const QString &LTR25Config::typeConfigName() {
        return LTR25TypeInfo::name();
    }

    const DevAdcConfig *LTR25Config::adcConfig() const  {
        return m_adc.get();
    }

    const DevAdcICPConfig *LTR25Config::adcICPConfig() const  {
        return m_icp.get();
    }

    const DevSyncMarksRecvConfig *LTR25Config::syncMarksRecvConfig() const {
        return m_syncRecv.get();
    }

    LTR25Config::LTR25Config() :
        m_adc{new LTR25AdcConfig{LTR25TypeInfo::defaultTypeInfo()}},
        m_icp{new LTR25AdcICPConfig{LTR25TypeInfo::defaultTypeInfo()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this}} {

        init();
    }

    LTR25Config::LTR25Config(const LTR25Config &cfg) :
        m_adc{new LTR25AdcConfig{cfg.adc()}},
        m_icp{new LTR25AdcICPConfig{cfg.icp()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this, cfg.marksRecv()}} {

        init();
    }

    void LTR25Config::protUpdate() {
        m_syncRecv->updateSyncMarksRecvFreq(m_adc->adcChFreq());
    }

    LQMeas::LTR25Config::~LTR25Config() {

    }

}
