#include "LTR25AdcConfig.h"
#include "LTR25.h"
#include "ltr/include/ltr25api.h"
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>
#include <QJsonArray>

namespace LQMeas {
    static const QLatin1String cfgkey_ch_enabled         {StdConfigKeys::enabled()};
    static const QLatin1String cfgkey_channels_arr       {StdConfigKeys::channels()};
    static const QLatin1String cfgkey_adc_data_fmt       {StdConfigKeys::adcDataFmt()};
    static const QLatin1String cfgkey_adc_freq_code      {StdConfigKeys::adcFreqCode()};

    typedef EnumConfigKey<LTR25AdcConfig::AdcDataFormat> LTR25AdcDataFmtKey;
    static const LTR25AdcDataFmtKey adc_data_fmts[] = {
        {QStringLiteral("20"),              LTR25AdcConfig::AdcDataFormat::Format_20},
        {QStringLiteral("24"),              LTR25AdcConfig::AdcDataFormat::Format_24},
        {LTR25AdcDataFmtKey::defaultKey(),  LTR25AdcConfig::AdcDataFormat::Format_20},
    };


    LTR25AdcConfig::LTR25AdcConfig(const DevAdcInfo &info) : DevAdcConfig{info} {
        memset(&m_params, 0, sizeof(m_params));
    }

    LTR25AdcConfig::LTR25AdcConfig(const LTR25AdcConfig &cfg) : DevAdcConfig{cfg} {
        memcpy(&m_params, &cfg.m_params, sizeof (m_params));
    }

    double LTR25AdcConfig::adcFreq() const {
        return LTR25TypeInfo::typeAdcFreqVal(m_params.freq_num);
    }

    bool LTR25AdcConfig::adcChSupportMeasDC(int ch) const {
        Q_UNUSED(ch)
        return  false;
    }

    int LTR25AdcConfig::adcMaxEnabledChCnt() const {
        int max_ch_cnt = 8;
        if (m_params.freq_num == typeAdcFreqNum(AdcFreq::Freq_39K)) {
            max_ch_cnt = m_params.data_fmt == AdcDataFormat::Format_24 ? 6 : 8;
        } else if (m_params.freq_num == typeAdcFreqNum(AdcFreq::Freq_78K)) {
            max_ch_cnt = m_params.data_fmt == AdcDataFormat::Format_24 ? 3 : 6;
        }
        return max_ch_cnt;
    }

    void LTR25AdcConfig::adcSetChEnabled(int ch, bool en)  {
        if (m_params.ch[ch].enabled != en) {
            m_params.ch[ch].enabled = en;
            notifyChEnableStateChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR25AdcConfig::adcSetFreqNum(int freq_num) {
        if (m_params.freq_num != freq_num) {
            m_params.freq_num = freq_num;
            notifyAdcFreqChanged();
            notifyChFreqChanged();
            notifyConfigChanged();
        }
    }

    void LTR25AdcConfig::adcSetDataFormat(LTR25AdcConfig::AdcDataFormat format) {
        if (m_params.data_fmt != format) {
            m_params.data_fmt = format;
            notifyConfigChanged();
        }
    }



    void LTR25AdcConfig::protSave(QJsonObject &cfgObj) const {
        QJsonArray chArray;
        for (int ch {0}; ch < LTR25TypeInfo::adc_channels_cnt; ++ch) {
            QJsonObject chObj;
            chObj[cfgkey_ch_enabled] = m_params.ch[ch].enabled;
            chArray.append(chObj);
        }
        cfgObj[cfgkey_channels_arr] = chArray;
        cfgObj[cfgkey_adc_data_fmt] = LTR25AdcDataFmtKey::getKey(adc_data_fmts, m_params.data_fmt);
        cfgObj[cfgkey_adc_freq_code] = m_params.freq_num;

    }

    void LTR25AdcConfig::protLoad(const QJsonObject &cfgObj) {
        const QJsonArray &chArray {cfgObj[cfgkey_channels_arr].toArray()};
        const int ch_cnt {chArray.size()};

        for (int ch {0}; (ch < ch_cnt) && (ch < LTR25TypeInfo::adc_channels_cnt); ++ch) {
            const QJsonObject &chObj {chArray.at(ch).toObject()};
            m_params.ch[ch].enabled = chObj[cfgkey_ch_enabled].toBool();
        }

        for (int ch {ch_cnt}; ch < LTR25TypeInfo::adc_channels_cnt; ++ch) {
            m_params.ch[ch].enabled = false;
        }

        m_params.data_fmt = LTR25AdcDataFmtKey::getValue(adc_data_fmts, cfgObj[cfgkey_adc_data_fmt].toString());
        m_params.freq_num = cfgObj[cfgkey_adc_freq_code].toInt(defaultAdcFreqNum());
    }
}
