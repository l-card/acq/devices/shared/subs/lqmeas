#include "LTR25Info.h"
#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String &cfgkey_pld_ver             {StdConfigKeys::pldVer()};
    static const QLatin1String &cfgkey_fpga_ver            {StdConfigKeys::fpgaVer()};
    static const QLatin1String &cfgkey_board_rev           {StdConfigKeys::boardRev()};
    static const QLatin1String &cfgkey_ext_bf_lf           {StdConfigKeys::ipcExtBwLF()};

    LTR25Info::LTR25Info(const LTR25TypeInfo &type, const QString &serial, int pld_ver,
                         int fpga_ver, int board_rev, bool icp_ext_bw) :
        DeviceInfo{type, serial},
        m_pld_ver{pld_ver},
        m_fpga_ver{fpga_ver},
        m_board_revision{board_rev},
        m_icp_ext_bw{icp_ext_bw} {

    }

    LTR25Info::LTR25Info(const LTR25Info &info) :
        DeviceInfo{info},
        m_pld_ver{info.m_pld_ver},
        m_fpga_ver{info.m_fpga_ver},
        m_board_revision{info.m_board_revision},
        m_icp_ext_bw{info.m_icp_ext_bw} {

    }

    void LTR25Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_pld_ver] = m_pld_ver;
        infoObj[cfgkey_fpga_ver] = m_fpga_ver;
        infoObj[cfgkey_board_rev] = m_board_revision;
        infoObj[cfgkey_ext_bf_lf] = m_icp_ext_bw;
    }

    void LTR25Info::protLoad(const QJsonObject &infoObj) {
        m_pld_ver = infoObj[cfgkey_pld_ver].toInt();
        m_fpga_ver = infoObj[cfgkey_fpga_ver].toInt();
        m_board_revision = infoObj[cfgkey_board_rev].toInt();
        m_icp_ext_bw = infoObj[cfgkey_ext_bf_lf].toBool(true);
    }


}


bool LQMeas::LTR25Info::supportTEDS() const {
    return !((m_board_revision < 2) || (m_fpga_ver < 8));
}
