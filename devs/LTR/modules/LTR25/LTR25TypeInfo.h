#ifndef LQMEAS_LTR25TYPEINFO_H
#define LQMEAS_LTR25TYPEINFO_H


#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"
#include "lqmeas/ifaces/in/adc/DevAdcInfo.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvInfo.h"

namespace LQMeas {
    class LTR25TypeInfo : public LTRModuleTypeInfo, public DevAdcInfo, public LTRSyncMarksRecvInfo {
    public:
        static const QString &name();
        static const LTR25TypeInfo &typeCommercialRev1();
        static const LTR25TypeInfo &typeIndustrialRev1();
        static const LTR25TypeInfo &typeCommercial();
        static const LTR25TypeInfo &typeIndustrial();
        static const LTR25TypeInfo &defaultTypeInfo();

        static const int adc_channels_cnt = 8;

        const DevAdcInfo *adc() const override {
            return static_cast<const DevAdcInfo *>(this);
        }
        const DevSyncMarksRecvInfo *syncMarkRecv() const override {
            return static_cast<const DevSyncMarksRecvInfo *>(this);
        }

        QString deviceTypeName() const override {return name();}
        QString deviceModificationName() const override;

        AdcType adcType() const override {return AdcType::Parallel;}
        int adcChannelsCnt() const override {return adc_channels_cnt;}
        double adcFreqMax() const override;

        bool adcIsChFreqConfigurable() const override {return false;}

        int adcChRangesCnt(int ch_num, int unit_mode_num = 0) const override;
        double adcChRangeMaxVal(int ch_num, int range, int unit_mode_num = 0) const override;
        double adcChRangeMinVal(int ch_num, int range, int unit_mode_num = 0) const override;

        static double typeAdcFreqVal(int idx);
        static int typeAdcFreqsCnt();

        bool industrial() const {return m_industrial;}
        bool supportTEDS() const {return  m_teds_support;}

        QList<const DeviceTypeInfo *> modificationList() const override;
        QList<const DeviceTypeInfo *> visibleModificationList() const override;
    private:
        LTR25TypeInfo(bool industrial, bool tedsSupport) :
            m_industrial(industrial), m_teds_support(tedsSupport) {
        }

        bool m_industrial;
        bool m_teds_support;
    };
}

#endif // LQMEAS_LTR25TYPEINFO_H
