#ifndef LQMEAS_LTR25CONFIG_H
#define LQMEAS_LTR25CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"
#include <memory>

namespace LQMeas {
    class LTR25AdcConfig;
    class LTR25AdcICPConfig;
    class LTRSyncMarksRecvConfig;

    class LTR25Config : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        const DevAdcConfig *adcConfig() const  override;
        const DevAdcICPConfig *adcICPConfig() const  override;
        const DevSyncMarksRecvConfig *syncMarksRecvConfig() const override;

        LTR25AdcConfig &adc() {return  *m_adc;}
        const LTR25AdcConfig &adc() const {return  *m_adc;}
        LTR25AdcICPConfig &icp() {return  *m_icp;}
        const LTR25AdcICPConfig &icp() const {return  *m_icp;}
        LTRSyncMarksRecvConfig &marksRecv() {return  *m_syncRecv;}
        const LTRSyncMarksRecvConfig &marksRecv() const {return  *m_syncRecv;}

        DeviceConfig *clone() const override {
            return new LTR25Config{*this};
        }

        LTR25Config();
        LTR25Config(const LTR25Config &cfg);
        ~LTR25Config() override;
    protected:
        void protUpdate() override;
    private:
        const std::unique_ptr<LTR25AdcConfig> m_adc;
        const std::unique_ptr<LTR25AdcICPConfig> m_icp;
        const std::unique_ptr<LTRSyncMarksRecvConfig> m_syncRecv;
    };
}

#endif // LQMEAS_LTR25CONFIG_H

