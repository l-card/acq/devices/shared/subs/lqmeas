#ifndef LQMEAS_LTR25ADCICPCONFIG_H
#define LQMEAS_LTR25ADCICPCONFIG_H

#include "lqmeas/ifaces/in/adc/icp/DevAdcICPConfig.h"

namespace LQMeas {
    class LTR25AdcICPConfig : public DevAdcICPConfig   {
        Q_OBJECT
    public:
        enum class ISrcValue {
            I_2_86mA,
            I_10mA,
        };

        bool adcChSupportICP(int ch) const override;
        bool adcChSupportTEDS(int ch) const override;
        double adcICPChISrcValue_mA(int ch) const  override;

        ISrcValue  adcISrcValue() const {return m_i_src_value;}
        bool adcPhaseCorEnabled() const {return m_phase_cor;}

    public Q_SLOTS:
        void adcSetISrcValue(LTR25AdcICPConfig::ISrcValue val);
        void adcSetPhaseCorEnabled(bool en);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        LTR25AdcICPConfig(const DevAdcInfo &info);
        LTR25AdcICPConfig(const LTR25AdcICPConfig &cfg);
    private:
        ISrcValue m_i_src_value {ISrcValue::I_2_86mA};
        bool m_phase_cor {true};

        friend class LTR25Config;
    };
}

Q_DECLARE_METATYPE(LQMeas::LTR25AdcICPConfig::ISrcValue);

#endif // LQMEAS_LTR25ADCICPCONFIG_H
