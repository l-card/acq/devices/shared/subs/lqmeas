#ifndef LQMEAS_LTR25INFO_H
#define LQMEAS_LTR25INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTR25TypeInfo.h"

namespace LQMeas {
    class LTR25Info : public DeviceInfo {
    public:
        LTR25Info(const LTR25TypeInfo &type = LTR25TypeInfo::defaultTypeInfo(),
                  const QString &serial = QString{}, int pld_ver = 0, int fpga_ver = 0,
                  int board_rev = 2, bool icp_ext_bw = true);


        DeviceInfo *clone() const override {return new LTR25Info{*this};}
        int pldVer() const {return m_pld_ver;}
        QString pldVerStr() const {return QString::number(m_pld_ver);}
        int fpgaVer() const {return m_fpga_ver;}
        QString fpgaVerStr() const {return QString::number(m_fpga_ver);}
        int boardRevision() const {return m_board_revision;}
        QString boardRevisionStr() const {return QString::number(m_board_revision);}

        bool icpExtendedBandwidthLF() const {return m_icp_ext_bw;}


        bool supportTEDS() const;

        const LTR25TypeInfo &devspecTypeInfo() const {
            return static_cast<const LTR25TypeInfo &>(type());
        }
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        LTR25Info(const LTR25Info &info);

        int m_pld_ver;
        int m_fpga_ver;
        int m_board_revision;
        bool m_icp_ext_bw;
    };
}


#endif // LTR25INFO_H
