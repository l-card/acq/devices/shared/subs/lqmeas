#include "LTR25.h"
#include "LTR25Config.h"
#include "LTR25AdcConfig.h"
#include "LTR25AdcICPConfig.h"
#include "LTR25Info.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "lqmeas/StdErrors.h"
#include "lqmeas/devs/LTR/modules/LTRModuleFrameReceiver.h"
#include "lqmeas/log/Log.h"

namespace LQMeas {
    LTR25::LTR25(LTRCrate *crate, int slot, QObject *parent) :
        LTRModule{crate, slot, typeModuleID, new LTR25Config{}, LTR25Info{}, parent},
        DevAdc{this, LTR25TypeInfo::adc_channels_cnt},
        m_receiver{new LTRModuleFrameReceiver{this}},
        m_corIcpPhase{true} {

        LTR25_Init(&m_hnd);
    }


    LTR25::~LTR25() {
        LTR25_Close(&m_hnd);
    }

    bool LTR25:: isOpened() const {
        return LTR25_IsOpened(&m_hnd) == LTR_OK;
    }

    DevSyncMarksRecv *LTR25::devSyncMarkRecv() {
        return m_receiver.get();
    }

    bool LTR25::adcIsRunning() const {
        return m_hnd.State.Run ? true : false;
    }

    const LTR25Config &LTR25::devspecConfig() const {
        return static_cast<const LTR25Config &>(config());
    }

    QSharedPointer<const LTR25Info> LTR25::devspecInfo() const {
        return devInfo().staticCast<const LTR25Info>();
    }

    TLTR *LTR25::channel() const {
        return &m_hnd.Channel;
    }

    int LTR25::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        Q_UNUSED(flags)
        INT ltr_err = LTR25_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, slot);
        if (ltr_err == LTR_OK) {
            ltr_err = LTR25_GetConfig(&m_hnd);
        }

        if (ltr_err == LTR_OK) {
            const LTR25TypeInfo *type = nullptr;
            if (m_hnd.ModuleInfo.BoardRev >= 2) {
                type = m_hnd.ModuleInfo.Industrial ? &LTR25TypeInfo::typeIndustrial()
                                                   : &LTR25TypeInfo::typeCommercial();
            } else {
                type = m_hnd.ModuleInfo.Industrial ? &LTR25TypeInfo::typeIndustrialRev1()
                                                   : &LTR25TypeInfo::typeCommercialRev1();
            }

            setDeviceInfo(LTR25Info(*type,
                                    QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                    m_hnd.ModuleInfo.VerPLD,
                                    m_hnd.ModuleInfo.VerFPGA,
                                    m_hnd.ModuleInfo.BoardRev,
                                    (m_hnd.ModuleInfo.SupportedFeatures & LTR25_FEATURE_EXT_BANDWIDTH_LF) != 0));
        }
        return ltr_err;
    }

    void LTR25::protClose(LQError &err) {
        getError(LTR25_Close(&m_hnd), err);
    }


    void LTR25::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTR25Config *setCfg {qobject_cast<const LTR25Config *>(&cfg)};
        if (setCfg) {
            m_hnd.Cfg.FreqCode = static_cast<BYTE>(setCfg->adc().adcFreqCode());
            m_hnd.Cfg.DataFmt =   setCfg->adc().adcDataFormat() == LTR25AdcConfig::AdcDataFormat::Format_20 ? LTR25_FORMAT_20 : LTR25_FORMAT_32;
            m_hnd.Cfg.ISrcValue = setCfg->icp().adcISrcValue() == LTR25AdcICPConfig::ISrcValue::I_10mA ? LTR25_I_SRC_VALUE_10 : LTR25_I_SRC_VALUE_2_86;
            for (int ch=0; ch < LTR25TypeInfo::adc_channels_cnt; ++ch) {
                m_hnd.Cfg.Ch[ch].Enabled = setCfg->adc().adcChEnabled(ch);
                m_hnd.Cfg.Ch[ch].SensorROut = static_cast<float>(setCfg->icp().adcICPChSensorROut(ch));
            }
            m_corIcpPhase = setCfg->icp().adcPhaseCorEnabled();
            getError(LTR25_SetADC(&m_hnd), err);
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }

    INT LTR25::ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) {
        return LTR25_Recv(&m_hnd, wrds, marks, size, tout);
    }

    void LTR25::protAdcStart(LQError &err) {
        getError(LTR25_Start(&m_hnd), err);
        if (err.isSuccess()) {
            const int frameSize = adcConfig().adcEnabledChCnt() *
                    (m_hnd.Cfg.DataFmt == LTR25_FORMAT_20 ? 1 : 2);
            m_receiver->start(frameSize, frameSize);
        }
    }

    void LTR25::protAdcStop(LQError &err) {
        getError(LTR25_Stop(&m_hnd), err);
        m_receiver->clear();
    }

    void LTR25::protAdcGetData(double *data, int size, unsigned flags,
                               unsigned tout, int &recvd_size, LQError &err) {

        const int wrds_cnt {m_hnd.Cfg.DataFmt == LTR25_FORMAT_20 ? size : 2*size};
        const int en_ch_cnt {adcConfig().adcEnabledChCnt()};
        int recvd_words {0};
        DWORD *wrds;
        DWORD ch_statuses[LTR25TypeInfo::adc_channels_cnt];
        INT proc_size {0};

        m_receiver->getFrames(wrds_cnt, tout, wrds, recvd_words, err);

        if (err.isSuccess() && (recvd_words != 0)) {
            DWORD proc_flags = LTR25_PROC_FLAG_VOLT | LTR25_PROC_FLAG_SIGN_COR;
            if (m_corIcpPhase)
                proc_flags |= LTR25_PROC_FLAG_PHASE_COR;


            proc_size = static_cast<INT>(recvd_words);

            getError(LTR25_ProcessData(&m_hnd, wrds, data, &proc_size,
                                       proc_flags, ch_statuses), err);
            if (!err.isSuccess()) {
                LQMeasLog->error(tr("Process data error"), err, this);
            } else {
                for (int ch_idx {0}; ch_idx < en_ch_cnt; ++ch_idx) {
                    ChannelStatus status {ChannelStatus::Ok};
                    if (ch_statuses[ch_idx] != LTR25_CH_STATUS_OK) {
                        if (ch_statuses[ch_idx] == LTR25_CH_STATUS_SHORT) {
                            status = ChannelStatus::Short;
                        } else if (ch_statuses[ch_idx] == LTR25_CH_STATUS_OPEN) {
                            status = ChannelStatus::Open;
                        } else {
                            status = ChannelStatus::Unknown;
                        }
                    }
                    adcSetChStatus(adcChNum(ch_idx), status);
                }
            }
        }

        if (err.isSuccess()) {
            recvd_size = proc_size;
        }
    }

    void LTR25::protTedsAccessStart(unsigned ch_mask, LQError &err) {
        Q_UNUSED(ch_mask)
        getError(LTR25_SetSensorsPowerMode(&m_hnd, LTR25_SENSORS_POWER_MODE_TEDS), err);
    }

    void LTR25::protTedsAccessFinish(LQError &err) {
        getError(LTR25_SetSensorsPowerMode(&m_hnd, LTR25_SENSORS_POWER_MODE_ICP), err);
    }

    QByteArray LTR25::protTedsMemRead(int ch, LQError &err) {
        QByteArray tedsRdArray;
        TLTR25_TEDS_NODE_INFO nodeInfo;
        getError(LTR25_TEDSNodeDetect(&m_hnd, ch, &nodeInfo), err);
        if (!err.isSuccess() && nodeInfo.Valid) {
            err = StdErrors::UnsupportedTEDSNodeFamily(nodeInfo.DevFamilyCode);
        }

        if (err.isSuccess()) {
            DWORD rd_size = 0;
            tedsRdArray.resize(static_cast<int>(nodeInfo.TEDSDataSize));

            getError(LTR25_TEDSReadData(&m_hnd, ch,
                                        reinterpret_cast<BYTE*>(tedsRdArray.data()),
                                        static_cast<DWORD>(tedsRdArray.size()), &rd_size), err);
            if (err.isSuccess()) {
                if (static_cast<int>(rd_size) != tedsRdArray.size()) {
                    tedsRdArray.resize(static_cast<int>(rd_size));
                }
            }
        }
        return  tedsRdArray;
    }

    QString LTR25::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTR25_GetErrorString(err));
    }
}
