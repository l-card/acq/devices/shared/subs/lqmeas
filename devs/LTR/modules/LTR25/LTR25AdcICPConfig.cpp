#include "LTR25AdcICPConfig.h"
#include "LTR25TypeInfo.h"
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_isrc_value     {StdConfigKeys::iSrcValue()};
    static const QLatin1String cfgkey_phase_cor_en   {StdConfigKeys::phaseCorEn()};



    typedef EnumConfigKey<LTR25AdcICPConfig::ISrcValue> LTR25IsrValKey;
    static const LTR25IsrValKey isrc_vals[] {
        {QStringLiteral("2.86"),        LTR25AdcICPConfig::ISrcValue::I_2_86mA},
        {QStringLiteral("10"),          LTR25AdcICPConfig::ISrcValue::I_10mA},
        {LTR25IsrValKey::defaultKey(),  LTR25AdcICPConfig::ISrcValue::I_2_86mA},
    };


    bool LTR25AdcICPConfig::adcChSupportICP(int ch) const {
        Q_UNUSED(ch)
        return  true;
    }

    bool LTR25AdcICPConfig::adcChSupportTEDS(int ch) const  {
        Q_UNUSED(ch)
        return static_cast<const LTR25TypeInfo *>(adcInfo())->supportTEDS();
    }

    double LTR25AdcICPConfig::adcICPChISrcValue_mA(int ch) const {
        Q_UNUSED(ch)
        return  m_i_src_value == ISrcValue::I_2_86mA ? 2.86 : 10;
    }

    void LTR25AdcICPConfig::adcSetISrcValue(LTR25AdcICPConfig::ISrcValue val)  {
        if (val != m_i_src_value) {
            m_i_src_value = val;
            notifyISrcValChanged(-1);
            notifyConfigChanged();
        }
    }

    void LTR25AdcICPConfig::adcSetPhaseCorEnabled(bool en) {
        if (en != m_phase_cor) {
            m_phase_cor = en;
            notifyConfigChanged();
        }
    }

    void LTR25AdcICPConfig::protSave(QJsonObject &cfgObj) const  {
        DevAdcICPConfig::protSave(cfgObj);
        cfgObj[cfgkey_isrc_value] = LTR25IsrValKey::getKey(isrc_vals, m_i_src_value);
        cfgObj[cfgkey_phase_cor_en] = m_phase_cor;
    }

    void LTR25AdcICPConfig::protLoad(const QJsonObject &cfgObj) {
        DevAdcICPConfig::protLoad(cfgObj);
        adcSetISrcValue(LTR25IsrValKey::getValue(isrc_vals, cfgObj[cfgkey_isrc_value].toString()));
        adcSetPhaseCorEnabled(cfgObj[cfgkey_phase_cor_en].toBool(true));
    }

    LTR25AdcICPConfig::LTR25AdcICPConfig(const DevAdcInfo &info) :
        DevAdcICPConfig{info} {

    }

    LTR25AdcICPConfig::LTR25AdcICPConfig(const LTR25AdcICPConfig &cfg) :
        DevAdcICPConfig{cfg},
        m_i_src_value{cfg.m_i_src_value},
        m_phase_cor{cfg.m_phase_cor} {
    }
}
