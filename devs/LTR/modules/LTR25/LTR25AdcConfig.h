#ifndef LQMEAS_LTR25ADCCONFIG_H
#define LQMEAS_LTR25ADCCONFIG_H

#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "LTR25TypeInfo.h"

namespace LQMeas {
    class LTR25AdcConfig : public DevAdcConfig {
        Q_OBJECT
    public:
        enum class AdcDataFormat {
            Format_20,
            Format_24
        };

        enum class AdcFreq {
            Freq_78K  = 0,
            Freq_39K  = 1,
            Freq_19K  = 2,
            Freq_9K7  = 3,
            Freq_4K8  = 4,
            Freq_2K4  = 5,
            Freq_1K2  = 6,
            Freq_610  = 7
        };


        double adcFreq() const override;
        bool adcChEnabled(int ch) const override {return m_params.ch[ch].enabled;}
        int adcChRangeNum(int ch) const override { Q_UNUSED(ch) return 0; }

        bool adcChSupportMeasDC(int ch) const override;


        int adcFreqNum() const {return m_params.freq_num;}
        AdcFreq adcFreqCode() const {return static_cast<AdcFreq>(adcFreqNum());}
        AdcDataFormat adcDataFormat() const {return m_params.data_fmt;}

        /* возвращает максимально количество каналов, которое можно разрешить
         * для текущих настроек модуля */
        int adcMaxEnabledChCnt() const;

        int adcSampleSize() const override {return adcDataFormat() == AdcDataFormat::Format_24 ? 8 : 4;}

        static int typeAdcFreqNum(AdcFreq freq) {return static_cast<int>(freq);}
    public Q_SLOTS:
        void adcSetChEnabled(int ch, bool en);
        void adcSetFreqNum(int freq_num);
        void adcSetFreqCode(LTR25AdcConfig::AdcFreq freq_code) {
            adcSetFreqNum(typeAdcFreqNum(freq_code));
        }
        void adcSetDataFormat(LTR25AdcConfig::AdcDataFormat format);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        LTR25AdcConfig(const DevAdcInfo &info);
        LTR25AdcConfig(const LTR25AdcConfig &cfg);

        static int defaultAdcFreqNum() {return typeAdcFreqNum(AdcFreq::Freq_9K7);}

        struct {
            int           freq_num;
            AdcDataFormat data_fmt;
            struct {
                bool      enabled;
            } ch[LTR25TypeInfo::adc_channels_cnt];
        } m_params;

        friend class LTR25Config;
    };
}

Q_DECLARE_METATYPE(LQMeas::LTR25AdcConfig::AdcDataFormat);
Q_DECLARE_METATYPE(LQMeas::LTR25AdcConfig::AdcFreq);


#endif // LQMEAS_LTR25ADCCONFIG_H
