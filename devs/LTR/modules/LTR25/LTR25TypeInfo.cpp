#include "LTR25TypeInfo.h"


static const double f_ranges[] = {10.};

static const double f_adc_freqs[] = {
        78.125e3, 39.0625e3, 19.53125e3, 9.765625e3,
        4.8828125e3, 2.44140625e3, 1.220703125e3, 0.6103515625e3
};

namespace LQMeas {
    const QString &LTR25TypeInfo::name() {
        static const QString str {QStringLiteral("LTR25")};
        return str;
    }

    double LTR25TypeInfo::adcFreqMax() const {
        return f_adc_freqs[0];
    }

    int LTR25TypeInfo::adcChRangesCnt(int ch_num, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return sizeof(f_ranges)/sizeof(f_ranges[0]);
    }

    double LTR25TypeInfo::adcChRangeMaxVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return f_ranges[range];
    }

    double LTR25TypeInfo::adcChRangeMinVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return -f_ranges[range];
    }



    const LTR25TypeInfo &LTR25TypeInfo::typeCommercialRev1() {
        static const LTR25TypeInfo info{false, false};
        return info;
    }

    const LTR25TypeInfo &LTR25TypeInfo::typeIndustrialRev1() {
        static const LTR25TypeInfo info{true, false};
        return info;
    }

    const LTR25TypeInfo &LTR25TypeInfo::typeCommercial() {
        static const LTR25TypeInfo info{false, true};
        return info;
    }

    const LTR25TypeInfo &LTR25TypeInfo::typeIndustrial() {
        static const LTR25TypeInfo info{true, true};
        return info;
    }

    const LTR25TypeInfo &LTR25TypeInfo::defaultTypeInfo() {
        return typeCommercial();
    }

    QString LTR25TypeInfo::deviceModificationName() const {        
        return  QString("%1%2%3")
                .arg(deviceTypeName())
                .arg(m_industrial ? QLatin1String("-I") : QLatin1String())
                .arg(m_teds_support ? QLatin1String() : QLatin1String("v1"));
    }

    double LTR25TypeInfo::typeAdcFreqVal(int idx) {
        return f_adc_freqs[idx];
    }

    int LTR25TypeInfo::typeAdcFreqsCnt() {
        return sizeof(f_adc_freqs)/sizeof(f_adc_freqs[0]);
    }

    QList<const DeviceTypeInfo *> LTR25TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list {
            &typeCommercial(),
            &typeCommercialRev1(),
            &typeIndustrial(),
            &typeIndustrialRev1()
        };
        return list;
    }

    QList<const DeviceTypeInfo *> LTR25TypeInfo::visibleModificationList() const {
        static const QList<const DeviceTypeInfo *> list {
            &typeCommercial(),
            &typeCommercialRev1(),
        };
        return list;
    }
}
