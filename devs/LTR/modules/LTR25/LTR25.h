#ifndef LQMEAS_LTR25_H
#define LQMEAS_LTR25_H

#include "ltr/include/ltr25api.h"
#include "lqmeas/devs/LTR/modules/LTRModule.h"
#include "lqmeas/ifaces/in/adc/DevAdc.h"
#include "lqmeas/ifaces/teds/DevTedsAccess.h"
#include <memory>


namespace LQMeas {
    class LTR25Config;
    class LTR25Info;
    class LTRModuleFrameReceiver;

    class LTR25 : public LTRModule, public DevAdc, public DevTedsAccess {
        Q_OBJECT
    public:
        static const quint16 typeModuleID {LTR_MID_LTR25};

        ~LTR25() override;
        bool isOpened() const override;

        /* ----------------------- интерфейс ввода  --------------------------*/
        DevAdc *devAdc() override {return static_cast<DevAdc*>(this);}
        DevTedsAccess *devTedsAccess() override {return static_cast<DevTedsAccess*>(this);}
        DevSyncMarksRecv *devSyncMarkRecv() override;

        QString errorString(int err) const override;

        bool adcIsRunning() const override;

        const LTR25Config &devspecConfig() const;
        QSharedPointer<const LTR25Info> devspecInfo() const;

        TLTR25 *devHandle() {return &m_hnd;}
    protected:
        TLTR* channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;

        INT ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) override;

        void protAdcStart(LQError &err) override;
        void protAdcStop(LQError &err) override;
        void protAdcGetData(double *data, int size, unsigned flags,
                            unsigned tout, int &recvd_size, LQError &err) override;

        void protTedsAccessStart(unsigned ch_mask, LQError &err) override;
        void protTedsAccessFinish(LQError &err) override;
        QByteArray protTedsMemRead(int ch, LQError &err) override;

    private:
        explicit LTR25(LTRCrate *crate, int slot, QObject *parent = nullptr);
        mutable TLTR25 m_hnd;
        const std::unique_ptr<LTRModuleFrameReceiver> m_receiver;

        bool m_corIcpPhase;

        friend class LTRResolver;

    };
}

#endif // LQMEAS_LTR25_H
