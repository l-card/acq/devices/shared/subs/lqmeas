#include "LTR210AdcConfig.h"
#include "LTR210TypeInfo.h"
#include "LTR210.h"
#include "ltr/include/ltr210api.h"
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/StdConfigKeys.h"
#include "lqmeas/lqtdefs.h"
#include <QJsonObject>
#include <QJsonArray>


namespace LQMeas {
    static const QLatin1String &cfgkey_ch_enabled        {StdConfigKeys::enabled()};
    static const QLatin1String &cfgkey_ch_range_num      {StdConfigKeys::rangeNum()};
    static const QLatin1String &cfgkey_ch_mode           {StdConfigKeys::mode()};
    static const QLatin1String &cfgkey_ch_thresh_group   {StdConfigKeys::threshold()};
    static const QLatin1String &cfgkey_ch_thresh_high    {StdConfigKeys::high()};
    static const QLatin1String &cfgkey_ch_thresh_low     {StdConfigKeys::low()};
    static const QLatin1String &cfgkey_channels_arr      {StdConfigKeys::channels()};

    static const QLatin1String &cfgkey_adc_freq          {StdConfigKeys::adcFreq()};
    static const QLatin1String &cfgkey_sync_mode         {StdConfigKeys::syncMode()};
    static const QLatin1String cfgkey_group_mode         {"GroupMode"};
    static const QLatin1String &cfgkey_frame_freq        {StdConfigKeys::frameFreq()};
    static const QLatin1String &cfgkey_frame_size        {StdConfigKeys::frameSize()};
    static const QLatin1String cfgkey_hist_size          {"HistSize"};
    static const QLatin1String cfgkey_transf_rate        {"TransfRate"};
    static const QLatin1String cfgkey_keep_alive_en      {"KeepAliveEn"};
    static const QLatin1String cfgkey_wr_auto_susp_en    {"WrAutoSuspEn"};

    typedef EnumConfigKey<LTR210AdcConfig::ChMode> LTR210ChModeKey;
    static const LTR210ChModeKey adc_ch_modes[] = {
        {QStringLiteral("AcDc"),            LTR210AdcConfig::ChMode::AcDc},
        {QStringLiteral("AcOnly"),          LTR210AdcConfig::ChMode::AcOnly},
        {QStringLiteral("Zero"),            LTR210AdcConfig::ChMode::Zero},
        {LTR210ChModeKey::defaultKey(),     LTR210AdcConfig::ChMode::AcDc},
    };


    typedef EnumConfigKey<LTR210AdcConfig::SyncMode> LTR210SyncModeKey;
    static const LTR210SyncModeKey adc_sync_modes[] = {
        {QStringLiteral("Internal"),        LTR210AdcConfig::SyncMode::Internal},
        {QStringLiteral("Ch1Rise"),         LTR210AdcConfig::SyncMode::Ch1Rise},
        {QStringLiteral("Ch1Fall"),         LTR210AdcConfig::SyncMode::Ch1Fall},
        {QStringLiteral("Ch2Rise"),         LTR210AdcConfig::SyncMode::Ch2Rise},
        {QStringLiteral("Ch2Fall"),         LTR210AdcConfig::SyncMode::Ch2Fall},
        {QStringLiteral("SyncFall"),        LTR210AdcConfig::SyncMode::SyncInFall},
        {QStringLiteral("SyncRise"),        LTR210AdcConfig::SyncMode::SyncInRise},
        {QStringLiteral("Periodic"),        LTR210AdcConfig::SyncMode::Periodic},
        {QStringLiteral("Cont"),            LTR210AdcConfig::SyncMode::Continuous},
        {LTR210SyncModeKey::defaultKey(),   LTR210AdcConfig::SyncMode::Internal},
    };

    typedef EnumConfigKey<LTR210AdcConfig::GroupMode> LTR210GroupModeKey;
    static const LTR210GroupModeKey adc_group_modes[] = {
        {QStringLiteral("Indiv"),           LTR210AdcConfig::GroupMode::Individual},
        {QStringLiteral("Master"),          LTR210AdcConfig::GroupMode::Master},
        {QStringLiteral("Slave"),           LTR210AdcConfig::GroupMode::Slave},
        {LTR210SyncModeKey::defaultKey(),   LTR210AdcConfig::GroupMode::Individual},
    };

    typedef EnumConfigKey<LTR210AdcConfig::IfaceTransfRate> LTR210GroupModeTransfRateKey;
    static const LTR210GroupModeTransfRateKey adc_transf_rate[] = {
        {QStringLiteral("500K"),           LTR210AdcConfig::IfaceTransfRate::Rate_500K},
        {QStringLiteral("200K"),           LTR210AdcConfig::IfaceTransfRate::Rate_200K},
        {QStringLiteral("100K"),           LTR210AdcConfig::IfaceTransfRate::Rate_100K},
        {QStringLiteral("50K"),            LTR210AdcConfig::IfaceTransfRate::Rate_50K},
        {QStringLiteral("25K"),            LTR210AdcConfig::IfaceTransfRate::Rate_25K},
        {QStringLiteral("10K"),            LTR210AdcConfig::IfaceTransfRate::Rate_10K},
        {LTR210GroupModeTransfRateKey::defaultKey(),   LTR210AdcConfig::IfaceTransfRate::Rate_500K},
    };
    static const double f_transf_rates[] = {
        500 * 1000,
        200 * 1000,
        100 * 1000,
        50 * 1000,
        25 * 1000,
        10 * 1000
    };


    double LTR210AdcConfig::adcFreq() const {
        return m_results.adc_freq;
    }

    double LTR210AdcConfig::adcChTransfRate() const {
        return adcFrameMode() ? qMin(adcChFreq(), adcIfaceTransfRateValueWords()/adcChCntDiv()) : adcChFreq();
    }

    bool LTR210AdcConfig::adcChSupportMeasDC(int ch) const {
        return (adcChMode(ch) != ChMode::AcOnly);
    }

    double LTR210AdcConfig::adcIfaceTransfRateValueWords() const {
        return f_transf_rates[static_cast<int>(m_params.transf_rate)];
    }

    int LTR210AdcConfig::adcFrameSizeMax() {
        return LTR210_FRAME_SIZE_MAX;
    }

    double LTR210AdcConfig::adcCurConfigFreqMax() const {
        return adcFrameMode() ? LTR210TypeInfo::typeAdcFreqMax() : LTR210TypeInfo::typeAdcContinuousFreqMax() / adcChCntDiv();
    }

    int LTR210AdcConfig::adcCurConfigFrameSizeMax() const  {
        return adcFrameSizeMax() / adcChCntDiv();
    }

    void LTR210AdcConfig::adcSetChEnabled(int ch, bool en) {
        if (m_params.ch[ch].enabled != en) {
            m_params.ch[ch].enabled = en;
            notifyChEnableStateChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR210AdcConfig::adcSetChRange(int ch, AdcChRange range) {
        adcSetChRangeNum(ch, typeAdcRangeNum(range));
    }

    void LTR210AdcConfig::adcSetChRangeNum(int ch, int range_num) {
        if (m_params.ch[ch].range != range_num) {
            m_params.ch[ch].range = range_num;
            notifyChRangeChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR210AdcConfig::adcSetChMode(int ch, ChMode mode) {
        if (m_params.ch[ch].mode != mode) {
            m_params.ch[ch].mode = mode;
            notifyChModeChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR210AdcConfig::adcSetChSyncThresholdHigh(int ch, double value) {
        if (!LQREAL_IS_EQUAL(m_params.ch[ch].syncThresholdHigh, value)) {
            m_params.ch[ch].syncThresholdHigh = value;
            notifyChModeChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR210AdcConfig::adcSetChSyncThresholdLow(int ch, double value)     {
        if (!LQREAL_IS_EQUAL(m_params.ch[ch].syncThresholdLow, value)) {
            m_params.ch[ch].syncThresholdLow = value;
            notifyChModeChanged(ch);
            notifyConfigChanged();
        }
    }

    void LTR210AdcConfig::adcSetFreq(double freq) {
        if (!LQREAL_IS_EQUAL(m_params.adc_freq, freq)) {
            m_params.adc_freq = freq;
            notifyAdcFreqChanged();
            notifyChFreqChanged();
            notifyConfigChanged();
        }
    }

    void LTR210AdcConfig::adcSetFrameFreq(double freq) {
        if (!LQREAL_IS_EQUAL(m_params.frame_freq, freq)) {
            m_params.frame_freq = freq;
            notifyAdcFreqChanged();
            notifyChFreqChanged();
            notifyConfigChanged();
        }
    }

    void LTR210AdcConfig::adcSetFrameSize(int size) {
        if (size > adcFrameSizeMax())
            size = adcFrameSizeMax();
        if (size < 1)
            size = 1;
        if (size != m_params.frame_size) {
            m_params.frame_size = size;
            notifyConfigChanged();
        }
    }

    void LTR210AdcConfig::adcSetHistorySize(int size) {
        if (size > adcFrameSizeMax())
            size = adcFrameSizeMax();
        if (size < 0)
            size = 0;
        if (size != m_params.hist_size) {
            m_params.hist_size = size;
            notifyConfigChanged();
        }
    }

    void LTR210AdcConfig::setAdcIfaceTransfRate(IfaceTransfRate rate) {
        if (m_params.transf_rate != rate) {
            m_params.transf_rate = rate;
            notifyConfigChanged();
        }
    }

    void LTR210AdcConfig::setAdcKeepAliveEnabled(bool en) {
        if (m_params.keep_alive_en != en) {
            m_params.keep_alive_en = en;
            notifyConfigChanged();
        }
    }

    void LTR210AdcConfig::setAdcWriteAutoSuspendEnabled(bool en) {
        if (m_params.wr_auto_susp_en != en) {
            m_params.wr_auto_susp_en = en;
            notifyConfigChanged();
        }
    }

    void LTR210AdcConfig::adcSetSyncMode(SyncMode mode) {
        if (m_params.sync_mode != mode) {
            m_params.sync_mode = mode;
            notifyConfigChanged();
            /* свойства каналов (непрерывный/покадровый) могут зависеть от
             * режима синхронизации и работы в группе, поэтому дополнительно
             * оповещаем об изменении параметров каналов */
            notifyChConfigChanged(-1);
        }
    }

    void LTR210AdcConfig::adcSetGroupMode(GroupMode mode) {
        if (m_params.group_mode != mode) {
            m_params.group_mode = mode;
            notifyConfigChanged();
            /* свойства каналов (непрерывный/покадровый) могут зависеть от
             * режима синхронизации и работы в группе, поэтому дополнительно
             * оповещаем об изменении параметров каналов */
            notifyChConfigChanged(-1);
        }
    }

    void LTR210AdcConfig::protUpdate() {
        double adc_freq, frame_freq;


        const int frame_size {qMin(m_params.frame_size, adcCurConfigFrameSizeMax())};
        const int hist_size  {qMin(m_params.hist_size, frame_size)};

        LTR210_FillAdcFreq(nullptr, qMin(m_params.adc_freq, adcCurConfigFreqMax()), 0, &adc_freq);
        LTR210_FillFrameFreq(nullptr, m_params.frame_freq, &frame_freq);

        if (!LQREAL_IS_EQUAL(adc_freq,m_results.adc_freq)
                || !LQREAL_IS_EQUAL(frame_freq, m_results.frame_freq)
                || (frame_size != m_results.frame_size)
                || (hist_size != m_results.hist_size)) {
            m_results.adc_freq   = adc_freq;
            m_results.frame_freq = frame_freq;
            m_results.frame_size = frame_size;
            m_results.hist_size = hist_size;
            notifyAdcFreqChanged();
            notifyConfigChanged();
        }
        DevAdcConfig::protUpdate();
    }

    void LTR210AdcConfig::protSave(QJsonObject &cfgObj) const {
        QJsonArray chArray;
        for (int ch {0}; ch < adc_channels_cnt; ++ch) {
            QJsonObject chObj;
            chObj[cfgkey_ch_enabled] = m_params.ch[ch].enabled;
            chObj[cfgkey_ch_range_num] = m_params.ch[ch].range;
            chObj[cfgkey_ch_mode] =LTR210ChModeKey::getKey(adc_ch_modes, m_params.ch[ch].mode);
            QJsonObject threshObj;
            threshObj[cfgkey_ch_thresh_high]  = m_params.ch[ch].syncThresholdHigh;
            threshObj[cfgkey_ch_thresh_low]   = m_params.ch[ch].syncThresholdLow;
            chObj[cfgkey_ch_thresh_group] = threshObj;
            chArray.append(chObj);
        }
        cfgObj[cfgkey_channels_arr] = chArray;

        cfgObj[cfgkey_adc_freq] = m_params.adc_freq;
        cfgObj[cfgkey_sync_mode] = LTR210SyncModeKey::getKey(adc_sync_modes, m_params.sync_mode);
        cfgObj[cfgkey_group_mode] = LTR210GroupModeKey::getKey(adc_group_modes, m_params.group_mode);

        cfgObj[cfgkey_frame_freq] = m_params.frame_freq;
        cfgObj[cfgkey_frame_size] = m_params.frame_size;
        cfgObj[cfgkey_hist_size]  = m_params.hist_size;

        cfgObj[cfgkey_transf_rate] = LTR210GroupModeTransfRateKey::getKey(adc_transf_rate, m_params.transf_rate);
        cfgObj[cfgkey_keep_alive_en] = m_params.keep_alive_en;
        cfgObj[cfgkey_wr_auto_susp_en] = m_params.wr_auto_susp_en;
    }

    void LTR210AdcConfig::protLoad(const QJsonObject &cfgObj) {
        const QJsonArray &chArray {cfgObj[cfgkey_channels_arr].toArray()};
        const int ch_cnt {chArray.size()};

        for (int ch {0}; (ch < ch_cnt) && (ch < adc_channels_cnt); ++ch) {
            const QJsonObject chObj {chArray.at(ch).toObject()};

            m_params.ch[ch].enabled = chObj[cfgkey_ch_enabled].toBool();

            const int range {chObj[cfgkey_ch_range_num].toInt(defaultRangeNum())};
            if ((range >= 0) || (range < adcInfo().adcChRangesCnt(ch, adcUnitMode(ch)))) {
                m_params.ch[ch].range = range;
            } else {
                m_params.ch[ch].range = defaultRangeNum();
            }

            m_params.ch[ch].mode = LTR210ChModeKey::getValue(
                        adc_ch_modes, chObj[cfgkey_ch_mode].toString());
            const QJsonObject &threshObj {chObj[cfgkey_ch_thresh_group].toObject()};
            m_params.ch[ch].syncThresholdHigh  = threshObj[cfgkey_ch_thresh_high].toDouble(defaultSyncThreshHigh());
            m_params.ch[ch].syncThresholdLow   = threshObj[cfgkey_ch_thresh_low].toDouble(defaultSyncThreshLow());
        }

        for (int ch=ch_cnt; ch < adc_channels_cnt; ++ch) {
            m_params.ch[ch].enabled = false;
            m_params.ch[ch].range = defaultRangeNum();
            m_params.ch[ch].mode = ChMode::AcDc;
        }

        m_params.adc_freq = cfgObj[cfgkey_adc_freq].toDouble(adcDefaultFreq());
        m_params.sync_mode  = LTR210SyncModeKey::getValue(adc_sync_modes, cfgObj[cfgkey_sync_mode].toString());
        m_params.group_mode = LTR210GroupModeKey::getValue(adc_group_modes, cfgObj[cfgkey_group_mode].toString());
        m_params.frame_freq = cfgObj[cfgkey_frame_freq].toDouble(defaultFrameFreq());
        m_params.frame_size = cfgObj[cfgkey_frame_size].toInt(defaultFrameSize());
        m_params.hist_size  = cfgObj[cfgkey_hist_size].toInt();

        m_params.transf_rate = LTR210GroupModeTransfRateKey::getValue(adc_transf_rate, cfgObj[cfgkey_transf_rate].toString());
        m_params.keep_alive_en = cfgObj[cfgkey_keep_alive_en].toBool(defaultKeepAliveEn());
        m_params.wr_auto_susp_en = cfgObj[cfgkey_wr_auto_susp_en].toBool(defaultWriteAutoSuspEn());
    }

    LTR210AdcConfig::LTR210AdcConfig(const DevAdcInfo &info) : DevAdcConfig{info} {
        memset(&m_params, 0, sizeof(m_params));
        m_params.sync_mode = defaultSyncMode();
        m_params.group_mode = defaultGrouoMode();
        m_params.frame_size = m_results.frame_size = defaultFrameSize();
        m_params.keep_alive_en = defaultKeepAliveEn();
        m_params.wr_auto_susp_en = defaultWriteAutoSuspEn();
        m_params.adc_freq = m_results.adc_freq = adcDefaultFreq();
        m_params.frame_freq = m_results.frame_freq = defaultFrameFreq();
        for (int ch{0}; ch < adc_channels_cnt; ++ch) {
            m_params.ch[ch].syncThresholdHigh = defaultSyncThreshHigh();
            m_params.ch[ch].syncThresholdLow  = defaultSyncThreshLow();
        }


    }

    LTR210AdcConfig::LTR210AdcConfig(const LTR210AdcConfig &cfg)  : DevAdcConfig{cfg} {
        memcpy(&m_params, &cfg.m_params, sizeof (m_params));
        memcpy(&m_results, &cfg.m_results, sizeof (m_results));
    }

    int LTR210AdcConfig::adcChCntDiv() const {
        return qMax(adcEnabledChCnt(), 1);
    }

}
