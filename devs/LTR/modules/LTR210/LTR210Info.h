#ifndef LTR210INFO_H
#define LTR210INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "LTR210TypeInfo.h"

namespace LQMeas {
    class LTR210Info : public DeviceInfo {
    public:
        LTR210Info(const LTR210TypeInfo &type = LTR210TypeInfo::defaultTypeInfo(),
                  const QString &serial = QString{}, int pld_ver = 0);


        DeviceInfo *clone() const override {return new LTR210Info{*this};}
        int pldVer() const {return m_pld_ver;}
        QString pldVerStr() const {return QString::number(m_pld_ver);}

        const LTR210TypeInfo &devspecTypeInfo() const {
            return static_cast<const LTR210TypeInfo &>(type());
        }
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        LTR210Info(const LTR210Info &info);

        int m_pld_ver;
    };
}


#endif // LTR210INFO_H
