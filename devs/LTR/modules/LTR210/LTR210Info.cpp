#include "LTR210Info.h"
#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String &cfgkey_pld_ver             {StdConfigKeys::pldVer()};

    LTR210Info::LTR210Info(const LTR210TypeInfo &type, const QString &serial, int pld_ver) :
        DeviceInfo{type, serial}, m_pld_ver{pld_ver} {

    }

    void LTR210Info::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_pld_ver] = m_pld_ver;
    }

    void LTR210Info::protLoad(const QJsonObject &infoObj) {
        m_pld_ver = infoObj[cfgkey_pld_ver].toInt();
    }

    LTR210Info::LTR210Info(const LTR210Info &info) : DeviceInfo{info},
        m_pld_ver{info.m_pld_ver} {
    }
}
