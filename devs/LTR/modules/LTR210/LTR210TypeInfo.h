#ifndef LTR210TYPEINFO_H
#define LTR210TYPEINFO_H

#include "lqmeas/devs/LTR/modules/LTRModuleTypeInfo.h"
#include "lqmeas/ifaces/in/adc/DevAdcInfo.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvInfo.h"

namespace LQMeas {
    class LTR210TypeInfo : public LTRModuleTypeInfo, public DevAdcInfo, public LTRSyncMarksRecvInfo {
    public:
        static const QString &name();
        static const LTR210TypeInfo &defaultTypeInfo();

        const DevAdcInfo *adc() const override {
            return static_cast<const DevAdcInfo *>(this);
        }
        const DevSyncMarksRecvInfo *syncMarkRecv() const override {
            return static_cast<const DevSyncMarksRecvInfo *>(this);
        }

        QString deviceTypeName() const override {return name();}

        AdcType adcType() const override {return AdcType::Parallel;}
        int adcChannelsCnt() const override;
        double adcFreqMax() const override {return 10e6;}
        double adcFreqDefault() const override {return 10e6;}

        static constexpr double typeAdcFreqMax() {return 10e6;}
        static constexpr double typeFrameFreqMax() {return 1e6;}
        static constexpr double typeAdcContinuousFreqMax() {return 500e3;}

        bool adcIsChFreqConfigurable() const override {return false;}

        int adcChRangesCnt(int ch_num, int unit_mode_num = 0) const override;
        double adcChRangeMaxVal(int ch_num, int range, int unit_mode_num = 0) const override;
        double adcChRangeMinVal(int ch_num, int range, int unit_mode_num = 0) const override;

        const Unit &adcChRangeUnit(int ch_num, int unit_mode_num = 0) const override;

        bool adcIsFrameModeSupported() const override {return true;}
        bool adcIsFrameReqSupported() const override {return true;}

        QList<const DeviceTypeInfo *> modificationList() const override;
    };
}

#endif // LTR210TYPEINFO_H
