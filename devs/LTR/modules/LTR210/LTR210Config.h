#ifndef LTR210CONFIG_H
#define LTR210CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"
#include <memory>

namespace LQMeas {
    class LTR210AdcConfig;
    class LTRSyncMarksRecvConfig;

    class LTR210Config : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        const DevAdcConfig *adcConfig() const  override;
        const DevSyncMarksRecvConfig *syncMarksRecvConfig() const override;

        LTR210AdcConfig &adc() {return *m_adc;}
        const LTR210AdcConfig &adc() const {return *m_adc;}
        LTRSyncMarksRecvConfig &marksRecv() {return *m_syncRecv;}
        const LTRSyncMarksRecvConfig &marksRecv() const {return *m_syncRecv;}

        DeviceConfig *clone() const override {
            return new LTR210Config{*this};
        }

        LTR210Config();
        LTR210Config(const LTR210Config &cfg);
        ~LTR210Config() override;
    protected:
        void protUpdate() override;
    private:
        const std::unique_ptr<LTR210AdcConfig> m_adc;
        const std::unique_ptr<LTRSyncMarksRecvConfig> m_syncRecv;
    };
}



#endif // LTR210CONFIG_H
