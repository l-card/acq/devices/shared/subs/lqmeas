#ifndef LTR210ADCCONFIG_H
#define LTR210ADCCONFIG_H


#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "LTR210TypeInfo.h"

namespace LQMeas {
    class LTR210AdcConfig: public DevAdcConfig {
        Q_OBJECT
    public:

        enum class ChMode {
            AcDc,
            AcOnly,
            Zero
        };

        enum class AdcChRange {
            U_10 = 0,
            U_5,
            U_2,
            U_1,
            U_0_5,
        };


        enum class SyncMode {
            Internal = 0,
            Ch1Rise,
            Ch1Fall,
            Ch2Rise,
            Ch2Fall,
            SyncInRise,
            SyncInFall,
            Periodic,
            Continuous
        };

        enum class GroupMode {
            Individual = 0,
            Master = 1,
            Slave = 2
        };

        enum class IfaceTransfRate {
            Rate_500K = 0,
            Rate_200K,
            Rate_100K,
            Rate_50K,
            Rate_25K,
            Rate_10K
        };


        static const int adc_channels_cnt {2};

        double adcFreq() const override;
        double adcConfiguredFreq() const {return m_params.adc_freq;}
        bool adcChEnabled(int ch) const override {return m_params.ch[ch].enabled;}
        int adcChRangeNum(int ch) const override { return m_params.ch[ch].range; }
        bool adcFrameMode() const override {return !((m_params.sync_mode == SyncMode::Continuous) && (m_params.group_mode != GroupMode::Slave));}
        int adcFrameSize() const override {return  m_results.frame_size;}
        double adcFrameFreq() const override {return m_params.sync_mode == SyncMode::Periodic ? m_results.frame_freq : 0;}
        bool adcFrameReqSupported() const override {return true;}
        bool adcFrameReqEnabled() const override {return (m_params.sync_mode == SyncMode::Internal) && (m_params.group_mode != GroupMode::Slave);}
        double adcConfiguredFrameFreq() const {return m_params.frame_freq;}
        double adcConfiguredResFrameFreq() const {return m_results.frame_freq;}
        bool adcExternalStart() const override {return m_params.group_mode == GroupMode::Slave;}
        double adcChTransfRate() const override;


        AdcChRange adcChRange(int ch) const {return static_cast<AdcChRange>(adcChRangeNum(ch));}
        ChMode adcChMode(int ch) const {return m_params.ch[ch].mode;}
        double adcChSyncThresholdHigh(int ch) const {return m_params.ch[ch].syncThresholdHigh;}
        double adcChSyncThresholdLow(int ch)  const {return m_params.ch[ch].syncThresholdLow;}

        bool adcChSupportMeasDC(int ch) const override;

        int adcSampleSize() const override {return 4;}


        SyncMode adcSyncMode() const {return m_params.sync_mode;}
        GroupMode adcGroupMode() const {return m_params.group_mode;}
        int adcConfiguredFrameSize() const {return m_params.frame_size;}
        int adcConfiguredHistorySize() const {return m_params.hist_size;}
        int adcHistorySize() const {return m_results.hist_size;}
        IfaceTransfRate adcIfaceTransfRate() const {return m_params.transf_rate;}
        double adcIfaceTransfRateValueWords() const;
        bool adcKeepAliveEnabled() const {return m_params.keep_alive_en;}
        bool adcWriteAutoSuspendEnabled() const {return m_params.wr_auto_susp_en;}


        static int typeAdcRangeNum(AdcChRange range) {return static_cast<int>(range);}
        static int adcFrameSizeMax();

        double adcCurConfigFreqMax() const;
        int adcCurConfigFrameSizeMax() const;

    public Q_SLOTS:

        void adcSetChEnabled(int ch, bool en);
        void adcSetChRange(int ch, LTR210AdcConfig::AdcChRange range);
        void adcSetChRangeNum(int ch, int range_num);
        void adcSetChMode(int ch, LTR210AdcConfig::ChMode mode);
        void adcSetChSyncThresholdHigh(int ch, double value);
        void adcSetChSyncThresholdLow(int ch, double value);

        void adcSetFreq(double freq);
        void adcSetFrameFreq(double freq);

        void adcSetSyncMode(LTR210AdcConfig::SyncMode mode);
        void adcSetGroupMode(LTR210AdcConfig::GroupMode mode);

        void adcSetFrameSize(int size);
        void adcSetHistorySize(int size);

        void setAdcIfaceTransfRate(LTR210AdcConfig::IfaceTransfRate rate);
        void setAdcKeepAliveEnabled(bool en);
        void setAdcWriteAutoSuspendEnabled(bool en);
    protected:
        void protUpdate() override;
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        explicit LTR210AdcConfig(const DevAdcInfo &info);
        LTR210AdcConfig(const LTR210AdcConfig &cfg);

        int adcChCntDiv() const;


        static int defaultRangeNum() {return typeAdcRangeNum(AdcChRange::U_10);}
        static constexpr double defaultSyncThreshHigh() {return 0.1;}
        static constexpr double defaultSyncThreshLow()  {return -0.1;}
        static constexpr double defaultFrameFreq()      {return 1.;}
        static constexpr int defaultFrameSize()         {return 1000000;}
        static constexpr SyncMode defaultSyncMode()     {return SyncMode::Periodic;}
        static constexpr GroupMode defaultGrouoMode()   {return GroupMode::Individual;}

        static constexpr bool defaultKeepAliveEn()      {return true;}
        static constexpr bool defaultWriteAutoSuspEn()  {return true;}

        struct {
            SyncMode sync_mode;
            GroupMode group_mode;
            double adc_freq;
            double frame_freq;
            int frame_size;
            int hist_size;
            IfaceTransfRate transf_rate;
            bool keep_alive_en;
            bool wr_auto_susp_en;
            struct {
                bool    enabled;
                int     range;
                ChMode  mode;
                double  syncThresholdHigh;
                double  syncThresholdLow;
            } ch[adc_channels_cnt];
        } m_params;

        struct {
            double adc_freq;
            double frame_freq;
            int frame_size;
            int hist_size;
        } m_results;

        friend class LTR210Config;
    };
}

Q_DECLARE_METATYPE(LQMeas::LTR210AdcConfig::ChMode);
Q_DECLARE_METATYPE(LQMeas::LTR210AdcConfig::AdcChRange);
Q_DECLARE_METATYPE(LQMeas::LTR210AdcConfig::SyncMode);
Q_DECLARE_METATYPE(LQMeas::LTR210AdcConfig::GroupMode);
Q_DECLARE_METATYPE(LQMeas::LTR210AdcConfig::IfaceTransfRate);


#endif // LTR210ADCCONFIG_H
