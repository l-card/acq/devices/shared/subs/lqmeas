#ifndef LTR210_H
#define LTR210_H


#include "ltr/include/ltr210api.h"
#include "lqmeas/devs/LTR/modules/LTRModule.h"
#include "lqmeas/ifaces/in/adc/DevAdc.h"
#include "lqmeas/ifaces/in/frame/DevInFrameReq.h"
#include "lqmeas/ifaces/fw/DevFwLoader.h"
#include <memory>

namespace LQMeas {
    class LTR210Config;
    class LTR210Info;
    class LTRModuleFrameReceiver;

    class LTR210 : public LTRModule, public DevAdc, public DevInFrameReq, public DevFwLoader {
        Q_OBJECT
    public:
        static const quint16 typeModuleID {LTR_MID_LTR210};

        ~LTR210() override;
        bool isOpened() const override;

        /* ----------------------- интерфейс ввода  --------------------------*/
        DevAdc *devAdc() override {return static_cast<DevAdc*>(this);}
        DevInFrameReq *devInFrameReq() override {return static_cast<DevInFrameReq*>(this);}
        DevFwLoader *devFwLoader() override {return static_cast<DevFwLoader*>(this);}
        DevSyncMarksRecv *devSyncMarkRecv() override;



        QString errorString(int err) const override;

        bool adcIsRunning() const override;
        DevInFrameStatus adcFrameStatus() const override {
            return m_frame_status;
        }

        const LTR210Config &devspecConfig() const;
        QSharedPointer<const LTR210Info> devspecInfo() const;

        /* ---------------------- интерфейс загрузки прошивки ----------------*/
        QStringList devFwTypes() const override;
        bool devFwIsLoaded(const QString &type, LQError &err) const override;
        void devFwLoad(const QString &type, const QString &filename, DevFwLoadProgressIndicator *loadIndicator, LQError &err) override;

    protected:
        TLTR* channel() const override;

        int ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn,  int slot, OpenFlags flags) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;

        INT ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) override;

        void protAdcStart(LQError &err) override;
        void protAdcStop(LQError &err) override;
        void protAdcGetData(double *data, int size, unsigned flags,
                            unsigned tout, int &recvd_size, LQError &err) override;

        void protInFrameRequest(LQError &err) override;

    private:
        void rawAdcGetData(double *data, int size, unsigned flags,
                           unsigned tout, int &recvd_size, LQError &err);

        explicit LTR210(LTRCrate *crate, int slot, QObject *parent = nullptr);
        mutable TLTR210 m_hnd;
        const std::unique_ptr<LTRModuleFrameReceiver> m_receiver;
        bool m_frame_mode {false};
        int  m_frame_rem_size {0};
        DevInFrameStatus m_frame_status;
        QMutex m_mutex;

        static const unsigned keepalive_tout {5000};

        friend class LTRResolver;
    };
}

#endif // LTR210_H
