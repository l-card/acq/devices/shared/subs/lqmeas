#include "LTR210.h"
#include "LTR210Config.h"
#include "LTR210AdcConfig.h"
#include "LTR210Info.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "lqmeas/ifaces/fw/DevFwStdTypes.h"
#include "lqmeas/ifaces/fw/DevFwLoadProgressIndicator.h"
#include "lqmeas/devs/LTR/modules/LTRModuleFrameReceiver.h"
#include "lqmeas/log/Log.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdErrors.h"
#include <QElapsedTimer>

namespace LQMeas {
    struct LTR210FwUpdCtx {
        LTR210 *dev;
        DevFwLoadProgressIndicator *indication;
    };

    /* индикация процесса загрузки */
    static void APIENTRY f_firm_upd_cb(void *cb_data, TLTR210* hnd, DWORD done_size, DWORD full_size) {
        LTR210FwUpdCtx *ctx {reinterpret_cast<LTR210FwUpdCtx *>(cb_data)};
        if (ctx->indication) {
            ctx->indication->devFwLoadProgressIndication(*ctx->dev, done_size, full_size, QString{});
        }
    }

    LTR210::LTR210(LTRCrate *crate, int slot, QObject *parent) :
        LTRModule{crate, slot, typeModuleID, new LTR210Config{}, LTR210Info{}, parent},
        DevAdc{this, LTR210AdcConfig::adc_channels_cnt},
        m_receiver{new LTRModuleFrameReceiver{this}} {

        LTR210_Init(&m_hnd);
    }


    LTR210::~LTR210() {
        LTR210_Close(&m_hnd);
    }

    bool LTR210:: isOpened() const {
        return LTR210_IsOpened(&m_hnd) == LTR_OK;
    }

    DevSyncMarksRecv *LTR210::devSyncMarkRecv() {
        return m_receiver.get();
    }

    bool LTR210::adcIsRunning() const {
        return m_hnd.State.Run ? true : false;
    }

    const LTR210Config &LTR210::devspecConfig() const {
        return static_cast<const LTR210Config &>(config());
    }

    QSharedPointer<const LTR210Info> LTR210::devspecInfo() const {
        return devInfo().staticCast<const LTR210Info>();
    }

    QStringList LTR210::devFwTypes() const     {
        static const QStringList types {DevFwStdTypes::fpga()};
        return types;
    }

    bool LTR210::devFwIsLoaded(const QString &type, LQError &err) const {
        INT ltr_code {LTR210_FPGAIsLoaded(&m_hnd)};
        if ((ltr_code != LTR_OK) && (ltr_code != LTR_ERROR_FPGA_IS_NOT_LOADED)) {
            getError(ltr_code, err);
        }
        return ltr_code == LTR_OK;

    }

    void LTR210::devFwLoad(const QString &type, const QString &filename, DevFwLoadProgressIndicator *loadIndicator, LQError &err) {
        LTR210FwUpdCtx ctx;
        ctx.dev = this;
        ctx.indication = loadIndicator;

        getError(LTR210_LoadFPGA(&m_hnd, filename.isEmpty() ? nullptr : QSTRING_TO_CSTR(filename), f_firm_upd_cb, &ctx), err);
    }

    TLTR *LTR210::channel() const {
        return &m_hnd.Channel;
    }

    int LTR210::ltrModuleOpen(quint32 ltrd_ip, quint16 ltrd_port, const QByteArray &crateSn, int slot, OpenFlags flags) {
        Q_UNUSED(flags)

        INT ltr_err = LTR210_Open(&m_hnd, ltrd_ip, ltrd_port, crateSn, slot);

        if (ltr_err == LTR_OK) {
            setDeviceInfo(LTR210Info{LTR210TypeInfo::defaultTypeInfo(),
                                     QSTRING_FROM_CSTR(m_hnd.ModuleInfo.Serial),
                                     m_hnd.ModuleInfo.VerPLD});
        }
        return ltr_err;
    }

    void LTR210::protClose(LQError &err) {
        getError(LTR210_Close(&m_hnd), err);
    }


    void LTR210::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTR210Config *setCfg {qobject_cast<const LTR210Config *>(&cfg)};
        if (setCfg) {
            LTR210_FillAdcFreq(&m_hnd.Cfg, setCfg->adc().adcConfiguredFreq(), 0, nullptr);
            LTR210_FillFrameFreq(&m_hnd.Cfg, setCfg->adc().adcConfiguredFrameFreq(), nullptr);
            m_hnd.Cfg.FrameSize = setCfg->adc().adcFrameSize();
            m_hnd.Cfg.HistSize  = setCfg->adc().adcHistorySize();            
            m_hnd.Cfg.GroupMode = static_cast<BYTE>(setCfg->adc().adcGroupMode());
            m_hnd.Cfg.SyncMode  = m_hnd.Cfg.GroupMode != LTR210_GROUP_MODE_SLAVE ?
                        static_cast<BYTE>(setCfg->adc().adcSyncMode()) : LTR210_SYNC_MODE_INTERNAL;

            const bool frameMode {setCfg->adc().adcFrameMode()};
            m_hnd.Cfg.IntfTransfRate =  frameMode ? static_cast<BYTE>(setCfg->adc().adcIfaceTransfRate())
                                                  : LTR210_INTF_TRANSF_RATE_500K;
            m_hnd.Cfg.Flags = 0;
            if (frameMode && setCfg->adc().adcKeepAliveEnabled()) {
                m_hnd.Cfg.Flags |= LTR210_CFG_FLAGS_KEEPALIVE_EN;
            }
            if (frameMode && setCfg->adc().adcWriteAutoSuspendEnabled()) {
                m_hnd.Cfg.Flags |= LTR210_CFG_FLAGS_WRITE_AUTO_SUSP;
            }
            for (int ch_num {0}; ch_num < LTR210AdcConfig::adc_channels_cnt; ++ch_num) {
                m_hnd.Cfg.Ch[ch_num].Enabled = setCfg->adc().adcChEnabled(ch_num);
                m_hnd.Cfg.Ch[ch_num].Range = setCfg->adc().adcChRangeNum(ch_num);
                m_hnd.Cfg.Ch[ch_num].Mode = static_cast<BYTE>(setCfg->adc().adcChMode(ch_num));
                m_hnd.Cfg.Ch[ch_num].DigBitMode = LTR210_DIG_BIT_MODE_ZERO; /** @todo */
                m_hnd.Cfg.Ch[ch_num].SyncLevelL = setCfg->adc().adcChSyncThresholdLow(ch_num);
                m_hnd.Cfg.Ch[ch_num].SyncLevelH = setCfg->adc().adcChSyncThresholdHigh(ch_num);
            }
            getError(LTR210_SetADC(&m_hnd), err);
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }

    INT LTR210::ltrRawWordsReceive(DWORD *wrds, DWORD *marks, DWORD size, DWORD tout) {
        return LTR210_Recv(&m_hnd, wrds, marks, size, tout);
    }

    void LTR210::protAdcStart(LQError &err) {
        getError(LTR210_Start(&m_hnd), err);
        if (err.isSuccess()) {
            int frameSize = adcConfig().adcEnabledChCnt();
            m_receiver->start(1, frameSize);
            m_frame_mode = adcConfig().adcFrameMode();
            m_frame_rem_size = 0;
            m_frame_status = DevInFrameStatus{};
        }
    }

    void LTR210::protAdcStop(LQError &err) {
        getError(LTR210_Stop(&m_hnd), err);
        m_receiver->clear();
    }

    void LTR210::protAdcGetData(double *data, int size, unsigned flags, unsigned tout,
                               int &recvd_size, LQError &err) {
        rawAdcGetData(data, size, flags, tout, recvd_size, err);
    }

    void LTR210::protInFrameRequest(LQError &err) {
        QMutexLocker lock(&m_mutex);
        getError(LTR210_FrameStart(&m_hnd), err);
    }

    void LTR210::rawAdcGetData(double *data, int size, unsigned flags, unsigned tout, int &recvd_size, LQError &err) {
        DWORD proc_flags {LTR210_PROC_FLAG_VOLT
                    | LTR210_PROC_FLAG_AFC_COR
                    | LTR210_PROC_FLAG_ZERO_OFFS_COR};
        if (m_frame_mode) {
            m_frame_status = DevInFrameStatus{};
            if (m_frame_rem_size == 0) {
                DWORD event;
                QElapsedTimer tmr; tmr.start();

                /* Нужно учитывать, что в параллель с WaitEvent может вызываться LTR210_FrameStart().
                 * По какой-то причине на Win это может приводить к потерям первого слова при приеме,
                 * из-за чего необходимо делать защиту мьютексом.
                 * Чтобы не было сильно длительного ожидания с захваченным мьютексом, ограничиваем время
                 * ожидания и просто многократно вызываем Wait до появления события или
                 * истечения основного таймаута */
                do {
                    QMutexLocker lock{&m_mutex};
                    getError(LTR210_WaitEvent(&m_hnd, &event, nullptr, qMin<int>(tout, 25)), err);
                } while (err.isSuccess() && (event == LTR210_RECV_EVENT_TIMEOUT) && (tmr.elapsed() < tout));


                if (err.isSuccess()) {
                    if (event == LTR210_RECV_EVENT_SOF) {
                        m_frame_rem_size = m_hnd.State.RecvFrameSize;
                    } else if (event == LTR210_RECV_EVENT_TIMEOUT) {
                        if (m_hnd.Cfg.Flags & LTR210_CFG_FLAGS_KEEPALIVE_EN) {
                            /* если ничего не пришло, то проверяем, не превышен ли таймаут на ожидание
                             * сигналов жизни */
                            DWORD interval;
                            getError(LTR210_GetLastWordInterval(&m_hnd, &interval), err);
                            if (err.isSuccess()) {
                                if (interval > keepalive_tout) {
                                    err = error(LTR210_ERR_KEEPALIVE_TOUT_EXCEEDED);
                                }
                            }
                        }
                    }
                }
            }

            if (err.isSuccess() && (m_frame_rem_size > 0)) {
                DWORD *wrds;
                int recvd_words {0};
                size = qMin(size, m_frame_rem_size);
                if (size == (m_frame_rem_size + 1)) {
                    size = m_frame_rem_size + 1;
                }
                m_receiver->getFrames(size, tout, wrds, recvd_words, err);
                if (err.isSuccess() && (recvd_words != 0)) {

                    INT proc_size = static_cast<INT>(recvd_words);
                    TLTR210_FRAME_STATUS frameSt;

                    getError(LTR210_ProcessData(&m_hnd, wrds, data, &proc_size,
                                               proc_flags, &frameSt, nullptr), err);
                    if (!err.isSuccess()) {
                        LQMeasLog->error(tr("Process data error"), err, this);
                    } else {
                        if (frameSt.Result != LTR210_FRAME_RESULT_PENDING) {
                            const bool data_valid {frameSt.Result != LTR210_FRAME_RESULT_OK};
                            DevInFrameStatus::Flag flags {DevInFrameStatus::Flag::None};
                            if (frameSt.Flags & LTR210_STATUS_FLAG_OVERLAP) {
                                flags |= DevInFrameStatus::Flag::Overlapped;
                            }
                            if (frameSt.Flags & LTR210_STATUS_FLAG_SYNC_SKIP) {
                                flags |= DevInFrameStatus::Flag::SyncSkipped;
                            }
                            if (frameSt.Flags & LTR210_STATUS_FLAG_INVALID_HIST) {
                                flags |= DevInFrameStatus::Flag::InvalidHistory;
                            }
                            m_frame_status = DevInFrameStatus{data_valid, flags};
                            m_frame_rem_size = 0;
                        } else {
                            m_frame_rem_size -= proc_size;
                        }


                        recvd_size = static_cast<int>(proc_size);
                    }
                }
            }
        } else {
            DWORD *wrds;
            int recvd_words {0};
            m_receiver->getFrames(size, tout, wrds, recvd_words, err);
            if (err.isSuccess() && (recvd_words != 0)) {
                INT proc_size = static_cast<INT>(recvd_words);
                getError(LTR210_ProcessData(&m_hnd, wrds, data, &proc_size,
                                           proc_flags, nullptr, nullptr), err);
                if (!err.isSuccess()) {
                    LQMeasLog->error(tr("Process data error"), err, this);
                } else {
                    recvd_size = static_cast<int>(proc_size);
                }
            }
        }
    }

    QString LTR210::errorString(int err) const {
        return QSTRING_FROM_CSTR(LTR210_GetErrorString(err));
    }
}
