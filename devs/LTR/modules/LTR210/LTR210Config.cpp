#include "LTR210Config.h"
#include "LTR210AdcConfig.h"
#include "LTR210TypeInfo.h"
#include "lqmeas/devs/LTR/marks/LTRSyncMarksRecvConfig.h"

namespace LQMeas {
    const QString &LTR210Config::typeConfigName() {
        return LTR210TypeInfo::name();
    }

    const DevAdcConfig *LTR210Config::adcConfig() const  {
        return m_adc.get();
    }


    const DevSyncMarksRecvConfig *LTR210Config::syncMarksRecvConfig() const {
        return m_syncRecv.get();
    }


    LTR210Config::LTR210Config() :
        m_adc{new LTR210AdcConfig{LTR210TypeInfo::defaultTypeInfo()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this}} {

        init();
    }

    LTR210Config::LTR210Config(const LTR210Config &cfg) :
        m_adc{new LTR210AdcConfig{cfg.adc()}},
        m_syncRecv{new LTRSyncMarksRecvConfig{*this, cfg.marksRecv()}} {

        init();
    }

    LTR210Config::~LTR210Config() {

    }

    void LTR210Config::protUpdate() {
        m_syncRecv->updateSyncMarksRecvFreq(m_adc->adcChFreq());
    }
}
