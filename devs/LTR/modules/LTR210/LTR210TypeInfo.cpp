#include "LTR210TypeInfo.h"
#include "ltr/include/ltr210api.h"
#include "lqmeas/units/std/Voltage.h"

namespace LQMeas {
    static const double f_ranges[LTR210_RANGE_CNT] = {10., 5., 2., 1., 0.5};

    const QString &LTR210TypeInfo::name() {
        static const QString str {QStringLiteral("LTR210")};
        return str;
    }

    int LTR210TypeInfo::adcChannelsCnt() const {
        return LTR210_CHANNEL_CNT;
    }

    int LTR210TypeInfo::adcChRangesCnt(int ch_num, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return LTR210_RANGE_CNT;
    }

    double LTR210TypeInfo::adcChRangeMaxVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
         return f_ranges[range];
    }

    double LTR210TypeInfo::adcChRangeMinVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return -f_ranges[range];
    }

    const Unit &LTR210TypeInfo::adcChRangeUnit(int ch_num, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return Units::Voltage::V();
    }

    QList<const DeviceTypeInfo *> LTR210TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list {
                                                        &defaultTypeInfo()
        };
        return list;
    }
    
    const LTR210TypeInfo &LTR210TypeInfo::defaultTypeInfo() {
        static const LTR210TypeInfo info;
        return info;
    }


}
