#ifndef LQMEAS_LTRSYNCMARKS_H
#define LQMEAS_LTRSYNCMARKS_H

#include <QObject>
#include "lqmeas/ifaces/sync/DevSyncMark.h"

namespace LQMeas {
    class LTRSyncMarkTypes : public QObject {
        Q_OBJECT
    public:
        static const DevSyncMarkType &start();
        static const DevSyncMarkType &second();
    };
}

#endif // LTRSYNCMARKS_H
