#ifndef LQMEAS_LTRSYNCMARKSRECVINFO_H
#define LQMEAS_LTRSYNCMARKSRECVINFO_H

#include "lqmeas/ifaces/sync/DevSyncMarksRecvInfo.h"

namespace LQMeas {
    class LTRSyncMarksRecvInfo : public DevSyncMarksRecvInfo {
    public:
        QList<const DevSyncMarkType *> syncMarkRecvTypes() const override;
    };
}

#endif // LTRSYNCMARKSRECVINFO_H
