#include "LTRSyncMarksRecvConfig.h"
#include "LTRSyncMarks.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdConfigKeys.h"
#include "lqmeas/devs/LTR/crates/LTRCrateConfig.h"
#include "lqmeas/ifaces/in/adc/DevAdcConfig.h"
#include "lqmeas/ifaces/in/dig/DevInDigConfig.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_enabled               {"En"};
    static const QLatin1String cfgkey_start_mark_wait_obj   {"StartMarkWait"};
    static const QLatin1String cfgkey_marks_obj             {StdConfigKeys::marks()};
    static const QLatin1String cfgkey_mark_start_obj        {StdConfigKeys::markStart()};
    static const QLatin1String cfgkey_mark_second_obj       {StdConfigKeys::markSecond()};
    static const QLatin1String cfgkey_mark_recv_en          {"RecvEn"};



    void LTRSyncMarksRecvConfig::setStartMarksRecvEnabled(bool en) {
        if (m_par.recv_start_marks_en != en) {
            m_par.recv_start_marks_en = en;
            notifyConfigChanged();
        }
    }

    void LTRSyncMarksRecvConfig::setSecondMarksRecvEnabled(bool en) {
        if (m_par.recv_second_marks_en != en) {
            m_par.recv_second_marks_en = en;
            notifyConfigChanged();
        }
    }

    void LTRSyncMarksRecvConfig::updateSyncMarksRecvFreq(double freq) {
        if (!LQREAL_IS_EQUAL(freq, m_res.recv_freq)) {
            m_res.recv_freq = freq;
            syncMarksRecvFreqChangedNotify();
            notifyConfigChanged();
        }
    }

    void LTRSyncMarksRecvConfig::protSave(QJsonObject &cfgObj) const {
        {
            QJsonObject startWtObj;
            startWtObj[cfgkey_enabled] = m_par.wait_start_mark;
            cfgObj[cfgkey_start_mark_wait_obj] = startWtObj;
        }

        {
            QJsonObject marksObj;
            {
                QJsonObject startMarkObj;
                startMarkObj[cfgkey_mark_recv_en] = m_par.recv_start_marks_en;
                marksObj[cfgkey_mark_start_obj] = startMarkObj;
            }
            {
                QJsonObject secMarkObj;
                secMarkObj[cfgkey_mark_recv_en] = m_par.recv_second_marks_en;
                marksObj[cfgkey_mark_second_obj] = secMarkObj;
            }
            cfgObj[cfgkey_marks_obj] = marksObj;
        }
    }

    void LTRSyncMarksRecvConfig::protLoad(const QJsonObject &cfgObj) {
        {
            const QJsonObject &startWtObj {cfgObj[cfgkey_start_mark_wait_obj].toObject()};
            m_par.wait_start_mark = startWtObj[cfgkey_enabled].toBool();
        }

        {
            const QJsonObject &marksObj {cfgObj[cfgkey_marks_obj].toObject()};
            {
                const QJsonObject &startMarkObj {marksObj[cfgkey_mark_start_obj].toObject()};
                m_par.recv_start_marks_en = startMarkObj[cfgkey_mark_recv_en].toBool();
            }
            {
                const QJsonObject &secMarkObj {marksObj[cfgkey_mark_second_obj].toObject()};
                m_par.recv_second_marks_en = secMarkObj[cfgkey_mark_recv_en].toBool();
            }
        }
    }

    void LTRSyncMarksRecvConfig::protUpdateParentConfig(const DeviceConfig *parentCfg) {
        const LTRCrateConfig *crateCfg {qobject_cast<const LTRCrateConfig *>(parentCfg)};
        if (crateCfg != m_crate_cfg) {
            m_crate_cfg = crateCfg;
            notifyConfigChanged();
            if (m_crate_cfg) {
                connect(m_crate_cfg, &LTRCrateConfig::configChanged,
                        this, &LTRSyncMarksRecvConfig::notifyConfigChanged);
            }
        }
    }

    bool LTRSyncMarksRecvConfig::hasRecvChannels() const {
        return ((m_dev_cfg->adcConfig() && (m_dev_cfg->adcConfig()->adcEnabledChCnt() > 0)) ||
                (m_dev_cfg->inDigConfig() && (m_dev_cfg->inDigConfig()->inDigEnabledSyncChCnt() > 0)));
    }

    LTRSyncMarksRecvConfig::LTRSyncMarksRecvConfig(DeviceConfig &devCfg) : m_dev_cfg{&devCfg} {
        m_par.wait_start_mark = false;
        m_par.recv_start_marks_en = false;
        m_par.recv_second_marks_en = false;
        m_res.recv_freq = 0;
    }

    LTRSyncMarksRecvConfig::LTRSyncMarksRecvConfig(DeviceConfig &devCfg, const LTRSyncMarksRecvConfig &cfg)
        : m_dev_cfg{&devCfg} {
        memcpy(&m_par, &cfg.m_par, sizeof(m_par));
        memcpy(&m_res, &cfg.m_res, sizeof(m_res));
    }

    bool LTRSyncMarksRecvConfig::syncMarksRecvStartOnMark() const {
         return syncMarksRecvStartOnMarkAvailable() &&
                 (m_par.wait_start_mark || syncMarksRecvStartOnMarkForced());
    }

    bool LTRSyncMarksRecvConfig::syncMarksRecvEnabled(const DevSyncMarkType &type) const {
        return &type == &LTRSyncMarkTypes::start() ? startMarksRecvEnabled() :
              &type == &LTRSyncMarkTypes::second() ? secondMarksRecvEnabled() :
                                                     false;
    }

    bool LTRSyncMarksRecvConfig::syncMarksRecvAvailable(const DevSyncMarkType &type) const {
        return hasRecvChannels() && (&type == &LTRSyncMarkTypes::start() ? startMarksRecvAvailable() :
                                     &type == &LTRSyncMarkTypes::second() ? secondMarksRecvAvailable() :
                                     false);
    }

    bool LTRSyncMarksRecvConfig::syncMarksRecvStartOnMarkAvailable() const {        
        return m_crate_cfg && m_crate_cfg->sync().childrenSyncStartAvailable();
    }

    bool LTRSyncMarksRecvConfig::syncMarksRecvStartOnMarkForced() const {
        return m_crate_cfg && m_crate_cfg->sync().childrenSyncStartEnabled();
    }

    void LTRSyncMarksRecvConfig::syncMarksSetRecvEnabled(const DevSyncMarkType &type, bool en) {
        if (&type == &LTRSyncMarkTypes::start()) {
            setStartMarksRecvEnabled(en);
        } else if (&type == &LTRSyncMarkTypes::second()) {
            setSecondMarksRecvEnabled(en);
        }
    }

    void LTRSyncMarksRecvConfig::syncMarksSetWiatStartMark(bool en) {
        if (m_par.wait_start_mark != en) {
            m_par.wait_start_mark = en;
            notifyConfigChanged();
        }
    }

    bool LTRSyncMarksRecvConfig::startMarksRecvEnabled() const {
        /** @todo убрано syncMarksRecvStartOnMarkAvailable(), т.к.
         * возможна генерация меток от модулей, что сейчас не проверяется */
        return m_par.recv_start_marks_en;
    }

    bool LTRSyncMarksRecvConfig::secondMarksRecvEnabled() const {
        /** @todo см. startMarksRecvEnabled() */
        return m_par.recv_second_marks_en;
    }

    bool LTRSyncMarksRecvConfig::startMarksRecvAvailable() const {
        /** @todo нужно учесть также резрешение генерации у дочерних устройств */
        return m_crate_cfg && m_crate_cfg->sync().startMarkGenEnabled();
    }

    bool LTRSyncMarksRecvConfig::secondMarksRecvAvailable() const {
        /** @todo нужно учесть также резрешение генерации у дочерних устройств */
        return m_crate_cfg && m_crate_cfg->sync().secondMarkGenEnabled();
    }
}
