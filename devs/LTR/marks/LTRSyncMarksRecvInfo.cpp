#include "LTRSyncMarksRecvInfo.h"
#include "LTRSyncMarks.h"

namespace LQMeas {
    QList<const DevSyncMarkType *> LTRSyncMarksRecvInfo::syncMarkRecvTypes() const {
        static const QList<const DevSyncMarkType *> list {
            &LTRSyncMarkTypes::start(),
            &LTRSyncMarkTypes::second()
        };
        return list;
    }
}
