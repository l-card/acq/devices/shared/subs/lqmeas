#include "LTRSyncMarks.h"

namespace LQMeas {
    const DevSyncMarkType &LTRSyncMarkTypes::start() {
        static const class LTRSyncMarkTypeStart : public DevSyncMarkType {
        public:
            QString id() const override {return QStringLiteral("start");}
            QString displayName() const override { return tr("START");}
        } marktype;
        return marktype;
    }

    const DevSyncMarkType &LTRSyncMarkTypes::second() {
        static const class LTRSyncMarkTypeSecond : public DevSyncMarkType {
        public:
            QString id() const override {return QStringLiteral("second");}
            QString displayName() const override { return tr("SECOND");}
        } marktype;
        return marktype;
    }
}
