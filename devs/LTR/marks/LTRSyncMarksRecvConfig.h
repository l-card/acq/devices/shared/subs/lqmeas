#ifndef LQMEAS_LTRSYNCMARKSRECVCONFIG_H
#define LQMEAS_LTRSYNCMARKSRECVCONFIG_H

#include "lqmeas/ifaces/sync/DevSyncMarksRecvConfig.h"

namespace LQMeas {
    class DeviceConfig;
    class LTRCrateConfig;

    class LTRSyncMarksRecvConfig : public DevSyncMarksRecvConfig {
        Q_OBJECT
    public:
        explicit LTRSyncMarksRecvConfig(DeviceConfig &devCfg);
        LTRSyncMarksRecvConfig(DeviceConfig &devCfg, const LTRSyncMarksRecvConfig &cfg);



        bool syncMarksRecvStartOnMark() const override;

        bool syncMarksRecvEnabled(const DevSyncMarkType &type) const override;
        bool syncMarksRecvAvailable(const DevSyncMarkType &type) const override;
        double syncMarksRecvFreq(const DevSyncMarkType &type) const override {
            Q_UNUSED(type)
            return syncMarksRecvFreq();
        }
        double syncMarksRecvFreq() const {return m_res.recv_freq;}

        bool syncMarksRecvStartOnMarkAvailable() const;
        bool syncMarksRecvStartOnMarkForced() const;
        void syncMarksSetRecvEnabled(const DevSyncMarkType &type, bool en);
        void syncMarksSetWiatStartMark(bool en);


        bool startMarksRecvEnabled() const;
        bool secondMarksRecvEnabled() const;
        bool startMarksRecvAvailable() const;
        bool secondMarksRecvAvailable() const;
        void setStartMarksRecvEnabled(bool en);
        void setSecondMarksRecvEnabled(bool en);

        void updateSyncMarksRecvFreq(double freq);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
        void protUpdateParentConfig(const DeviceConfig *parentCfg) override;
    private:
        bool hasRecvChannels() const;

        struct {
            bool wait_start_mark;
            bool recv_start_marks_en;
            bool recv_second_marks_en;
        } m_par;

        struct {
            double recv_freq;
        } m_res;

        const DeviceConfig *m_dev_cfg;
        const LTRCrateConfig *m_crate_cfg {nullptr};
    };
}

#endif // LTRSYNCMARKSRECVCONFIG_H
