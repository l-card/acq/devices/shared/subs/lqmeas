#include "LTRResolver.h"
#include "LQError.h"
#include "lqmeas_config.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/devs/LTR/service/LTRService.h"
#include "lqmeas/devs/LTR/service/LTRServiceRef.h"
#include "ltr/include/ltrapi.h"
#include "lqmeas/devs/DeviceValidator.h"
#include "lqmeas/devs/DeviceNameValidator.h"
#include "lqmeas/lqtdefs.h"

#ifdef LQMEAS_DEV_LTR11
    #include "modules/LTR11/LTR11.h"
#endif
#ifdef LQMEAS_DEV_LTR12
    #include "modules/LTR12/LTR12.h"
#endif
#ifdef LQMEAS_DEV_LTR22
    #include "modules/LTR22/LTR22.h"
#endif
#ifdef LQMEAS_DEV_LTR24
    #include "modules/LTR24/LTR24.h"
#endif
#ifdef LQMEAS_DEV_LTR25
    #include "modules/LTR25/LTR25.h"
#endif
#ifdef LQMEAS_DEV_LTR27
    #include "modules/LTR27/LTR27.h"
#endif
#ifdef LQMEAS_DEV_LTR34
    #include "modules/LTR34/LTR34.h"
#endif
#ifdef LQMEAS_DEV_LTR35
    #include "modules/LTR35/LTR35.h"
#endif
#ifdef LQMEAS_DEV_LTR41
    #include "modules/LTR41/LTR41.h"
#endif
#ifdef LQMEAS_DEV_LTR42
    #include "modules/LTR42/LTR42.h"
#endif
#ifdef LQMEAS_DEV_LTR43
    #include "modules/LTR43/LTR43.h"
#endif
#ifdef LQMEAS_DEV_LTR51
    #include "modules/LTR51/LTR51.h"
#endif
#ifdef LQMEAS_DEV_LTR114
    #include "modules/LTR114/LTR114.h"
#endif
#ifdef LQMEAS_DEV_LTR210
    #include "modules/LTR210/LTR210.h"
#endif
#ifdef LQMEAS_DEV_LTR212
    #include "modules/LTR212/LTR212.h"
#endif
#ifdef LQMEAS_DEV_LTR216
    #include "modules/LTR216/LTR216.h"
#endif
#ifdef LQMEAS_DEV_LTR01
    #include "modules/LTR01.h"
    #ifdef LQMEAS_DEV_LTRT13
        #include "modules/LTRT13/LTRT13.h"
    #endif
    #ifdef LQMEAS_DEV_LTRS412
        #include "modules/LTRS412/LTRS412.h"
    #endif
    #ifdef LQMEAS_DEV_LTRK511
        #include "modules/LTRK511/LTRK511.h"
    #endif
    #ifdef LQMEAS_DEV_LTRK416
        #include "modules/LTRK416/LTRK416.h"
    #endif
    #ifdef LQMEAS_DEV_LTRK415
        #include "modules/LTRK415/LTRK415.h"
    #endif
    #ifdef LQMEAS_DEV_LTRK71
        #include "modules/LTRK71/LTRK71.h"
    #endif
#endif


namespace LQMeas {
    QList<QSharedPointer<LTRModule> > LTRResolver::getModuleList(const QStringList &nameFilter, LQError &err) {
        return getModuleList(DeviceNameValidator{nameFilter}, err);
    }

    QList<QSharedPointer<LTRModule> > LTRResolver::getModuleList(const DeviceValidator &validator, LQError &err) {
        QList<QSharedPointer<LTRModule> > fndList;

        const QList<QSharedPointer<LTRCrate> > crates {getCratesList(err)};
        for (const QSharedPointer<LTRCrate> &crate : crates) {
            fndList.append(crate->moduleList(validator));
        }
        return fndList;
    }

    QSharedPointer<LTRService> LTRResolver::service(const LTRServiceRef &ref) const {
        auto it = std::find_if(m_svcList.constBegin(), m_svcList.constEnd(),
                               [&ref](const auto &svc) {return svc->ref() == ref;});
        return it == m_svcList.constEnd() ? QSharedPointer<LTRService>{} : *it;
    }

    QSharedPointer<LTRService> LTRResolver::addService(const LTRServiceRef &ref) {
        QSharedPointer<LTRService> ret {service(ref)};
        if (!ret) {
            ret = QSharedPointer<LTRService>{new LTRService{ref}};
            m_svcList.append(ret);
        }
        return ret;
    }

    void LTRResolver::clearServices() {
        m_svcList.clear();
        m_svcList.append(m_defaultSvc);
    }

    QSharedPointer<LTRCrate> LTRResolver::crate(const DeviceRef &crateRef) const {
        QSharedPointer<LTRCrate> ret;
        for (const QSharedPointer<LTRService> &svc : m_svcList) {
            QSharedPointer<LTRCrate> fndCrate = svc->crate(crateRef);
            if (fndCrate) {
                ret = fndCrate;
                break;
            }
        }
        return ret;
    }

    QList<QSharedPointer<Device> > LTRResolver::protGetDevList(const QList<const DeviceRef *> &devRefs, ResolveFlags flags) {
        Q_UNUSED(flags)
        QList<QSharedPointer<Device> > retlist;

        QList<const LTRServiceRef *> svcRefs;

        for (const DeviceRef *ref : devRefs) {
            const LTRServiceRef *newSvcRef = getSvcRef(*ref);
            if (newSvcRef && !newSvcRef->isDefualt()) {
                bool present {false};
                for (const LTRServiceRef *svcRef : svcRefs) {
                    if (svcRef->sameDevice(*newSvcRef)) {
                        present = true;
                        break;
                    }
                }
                if (!present) {
                    svcRefs.append(newSvcRef);
                }
            }
        }

        QList< QSharedPointer<LTRService> > svcList;
        svcList.append(m_defaultSvc);

        for (const LTRServiceRef *svcRef : svcRefs) {
            svcList.append(addService(*svcRef));
        }


        for (const QSharedPointer<LTRService> &svc : svcList) {
            retlist.append(svc);
        }

        if (flags & ResolveFlag::Recursive) {
            for (const QSharedPointer<LTRService> &svc : svcList) {
                retlist.append(svc->childResolver()->getDevList(devRefs, flags));
            }
        }

        return retlist;
    }


    QList<QSharedPointer<LTRCrate> > LTRResolver::getCratesList(LQError &err) {
        return getCratesList(m_svcList, err);
    }

    QList<QSharedPointer<LTRCrate> > LTRResolver::getCratesList(
            const QList<QSharedPointer<LTRService> > &svcList, LQError &err) {
        QList<QSharedPointer<LTRCrate> > retlist;

        for (const QSharedPointer<LTRService> &svc : svcList) {
            LQError svc_err;
            retlist.append(svc->updateDevicesList(svc_err));
            if (!svc_err.isSuccess())
                err += svc_err;
        }
        return retlist;
    }

    LTRResolver &LTRResolver::resolver() {
        static LTRResolver resolver;
        return resolver;
    }

    LTRResolver::LTRResolver() {
        m_svcList.append(m_defaultSvc = QSharedPointer<LTRService>{new LTRService{}});
    }

    LTRResolver::~LTRResolver() {

    }

    const LTRServiceRef *LTRResolver::getSvcRef(const DeviceRef &devref) {
        const LTRServiceRef *svcRef = nullptr;
        if (devref.refTypeID() == LTRServiceRef::typeRefTypeID()) {
            svcRef = static_cast<const LTRServiceRef *>(&devref);
        } else if (devref.parentRef()) {
            svcRef = getSvcRef(*devref.parentRef());
        }
        return svcRef;
    }

    QSharedPointer<LTRModule> LTRResolver::createModule(LTRCrate *crate, int slot, quint16 mid) {
        switch (mid) {
    #ifdef LQMEAS_DEV_LTR11
            case LTR11::typeModuleID:
                return QSharedPointer<LTRModule>{new LTR11{crate, slot}};
    #endif
#ifdef LQMEAS_DEV_LTR12
            case LTR12::typeModuleID:
            return QSharedPointer<LTRModule>{new LTR12{crate, slot}};
#endif
    #ifdef LQMEAS_DEV_LTR24
            case LTR24::typeModuleID:
                return QSharedPointer<LTRModule>{new LTR24{crate, slot}};
    #endif
    #ifdef LQMEAS_DEV_LTR22
            case LTR22::typeModuleID:
                return QSharedPointer<LTRModule>{new LTR22{crate, slot}};
    #endif
    #ifdef LQMEAS_DEV_LTR25
            case LTR25::typeModuleID:
                return QSharedPointer<LTRModule>{new LTR25{crate, slot}};
    #endif
    #ifdef LQMEAS_DEV_LTR27
            case LTR27::typeModuleID:
                return QSharedPointer<LTRModule>{new LTR27{crate, slot}};
    #endif
    #ifdef LQMEAS_DEV_LTR34
        case LTR34::typeModuleID:
            return QSharedPointer<LTRModule>{new LTR34{crate, slot}};
    #endif
    #ifdef LQMEAS_DEV_LTR35
        case LTR35::typeModuleID:
            return QSharedPointer<LTRModule>{new LTR35{crate, slot}};
    #endif
    #ifdef LQMEAS_DEV_LTR41
        case LTR41::typeModuleID:
            return QSharedPointer<LTRModule>{new LTR41{crate, slot}};
    #endif
    #ifdef LQMEAS_DEV_LTR42
        case LTR42::typeModuleID:
            return QSharedPointer<LTRModule>{new LTR42{crate, slot}};
    #endif
    #ifdef LQMEAS_DEV_LTR43
        case LTR43::typeModuleID:
            return QSharedPointer<LTRModule>{new LTR43{crate, slot}};
    #endif
    #ifdef LQMEAS_DEV_LTR51
        case LTR51::typeModuleID:
            return QSharedPointer<LTRModule>{new LTR51{crate, slot}};
    #endif
    #ifdef LQMEAS_DEV_LTR114
            case LTR114::typeModuleID:
                return QSharedPointer<LTRModule>{new LTR114{crate, slot}};
    #endif
    #ifdef LQMEAS_DEV_LTR210
        case LTR210::typeModuleID:
            return QSharedPointer<LTRModule>{new LTR210{crate, slot}};
    #endif
    #ifdef LQMEAS_DEV_LTR212
            case LTR212::typeModuleID:
                return QSharedPointer<LTRModule>{new LTR212{crate, slot}};
    #endif
    #ifdef LQMEAS_DEV_LTR216
            case LTR216::typeModuleID:
                return QSharedPointer<LTRModule>{new LTR216{crate, slot}};
    #endif
    #ifdef LQMEAS_DEV_LTR01
            case LTR01::typeModuleID: {
                WORD subid;
                INT ltr_err = LTR01_GetSubID(crate->serviceRef().ltrdIPv4Addr(),
                                             crate->serviceRef().ltrdTcpPort(),
                                             QSTRING_TO_CSTR(crate->serial()),
                                             slot, &subid, NULL);
                if (ltr_err == LTR_OK) {
                    switch (subid) {
        #ifdef LQMEAS_DEV_LTRT13
                        case LTRT13::typeModuleSubID:
                           return QSharedPointer<LTRModule>{new LTRT13{crate, slot}};
        #endif
        #ifdef LQMEAS_DEV_LTRS412
                        case LTRS412::typeModuleSubID:
                           return QSharedPointer<LTRModule>{new LTRS412{crate, slot}};
        #endif
        #ifdef LQMEAS_DEV_LTRK511
                        case LTRK511::typeModuleSubID:
                           return QSharedPointer<LTRModule>{new LTRK511{crate, slot}};
        #endif
        #ifdef LQMEAS_DEV_LTRK416
                        case LTRK416::typeModuleSubID:
                            return QSharedPointer<LTRModule>{new LTRK416{crate, slot}};
        #endif
        #ifdef LQMEAS_DEV_LTRK415
                        case LTRK415::typeModuleSubID:
                            return QSharedPointer<LTRModule>{new LTRK415{crate, slot}};
        #endif
        #ifdef LQMEAS_DEV_LTRK71
                        case LTRK71::typeModuleSubID:
                            return QSharedPointer<LTRModule>{new LTRK71{crate, slot}};
        #endif
                    }
                }
            }
            break;
    #endif
        }
        return QSharedPointer<LTRModule>{};
    }
}

