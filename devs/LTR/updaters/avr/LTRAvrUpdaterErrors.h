#ifndef LTRAVRUPDATERERRORS_H
#define LTRAVRUPDATERERRORS_H

#include <QObject>
#include "LQError.h"

namespace LQMeas {
    class LTRAvrUpdaterErrors : public QObject {
        Q_OBJECT
    public:
        static LQError fileOpen(const QString &name);
        static LQError unsupportedMid(unsigned mid);
        static LQError invalidFirmwareSize(int size);
        static LQError fwVerify(unsigned addr, quint8 wrval, quint8 rdval);
        static LQError fuseVerify(unsigned reqFuses, unsigned fuses);
    private:

        static LQError error(int code, const QString &msg);
    };
}



#endif // LTRAVRUPDATERERRORS_H
