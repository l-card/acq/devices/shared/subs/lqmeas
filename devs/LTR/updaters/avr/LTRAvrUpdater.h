#ifndef LTRAVRUPDATER_H
#define LTRAVRUPDATER_H

#include <QObject>
#include <QSharedPointer>
#include "ltr/include/ltravrapi.h"

class LQError;
namespace LQMeas {
    class LTRModule;

    class LTRAvrUpdater : public QObject {
        Q_OBJECT
    public:
        LTRAvrUpdater(LTRModule *module, QObject *parent = nullptr);


        void writeFirmwareFromFile(const QString &filename, const QString &wrSerial, LQError &err);
        void writeFirmware(const QByteArray &fwdata, const QString &wrSerial, LQError &err);
        void writeFuses(bool ls, unsigned wrfuses, LQError &err);
        unsigned readFuses(LQError &err);
        unsigned checkFuses(unsigned reqFuses, LQError &err);

    Q_SIGNALS:
        void progress(QString stage, int perc);
    private:
        LQMeas::LTRModule *m_module;

        void showProgress(const QString &stage, int perc);

        static void APIENTRY wr_progr_cb(void* cb_data, TLTRAVR* hnd,
                                         DWORD done_size, DWORD full_size);

        void getDevError(int err_code, LQError &err);
        void getCrateError(int err_code, LQError &err);

        static QString crateFirmFile();
    };
}
#endif // LTRAVRUPDATER_H
