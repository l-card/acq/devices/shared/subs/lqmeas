#include "LTRAvrUpdaterErrors.h"

namespace LQMeas {
    LQError LTRAvrUpdaterErrors::fileOpen(const QString &name) {
        return error(-1, tr("Cannot open firmware file (%1)").arg(name));
    }

    LQError LTRAvrUpdaterErrors::unsupportedMid(unsigned int mid) {
        return error(-2, tr("Unsupported updated LTR module ID (0x%1)").arg(mid, 4, 16, QChar('0')));
    }

    LQError LTRAvrUpdaterErrors::invalidFirmwareSize(int size) {
        return error(-3, tr("Invalid firmware file size (%1 bytes)").arg(size));
    }

    LQError LTRAvrUpdaterErrors::fwVerify(unsigned int addr, quint8 wrval, quint8 rdval) {
        return error(-4, tr("Firmware verifycation error. Byte %1: write 0x%2, read 0x%3").
                         arg(addr).arg(wrval, 2, 16, QChar('0')).arg(rdval, 2, 16, QChar('0')));
    }

    LQError LTRAvrUpdaterErrors::fuseVerify(unsigned int reqFuses, unsigned int fuses) {
        return error(-5, tr("Written AVR fuse verifycation error. Write 0x%1, read 0x%2").
                         arg(reqFuses, 4, 16, QChar('F')).arg(fuses, 4, 16, QChar('F')));
    }

    LQError LTRAvrUpdaterErrors::error(int code, const QString &msg) {
        static const QString err_type {QStringLiteral("ltravr")};
        return LQError{code, msg, err_type};
    }
}
