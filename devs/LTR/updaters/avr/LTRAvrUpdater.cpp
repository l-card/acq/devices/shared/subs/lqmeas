#include "LTRAvrUpdater.h"
#include "LTRAvrUpdaterErrors.h"
#include "lqmeas/devs/LTR/modules/LTRModule.h"
#include "lqmeas/devs/LTR/service/LTRServiceRef.h"
#include "lqmeas/devs/LTR/LTRNativeError.h"
#include "lqmeas/devs/DeviceInfo.h"
#include "lqmeas/lqtdefs.h"

#include "ltr/include/ltr010api.h"
#include "crc.h"
#include <QLocale>
#include <QFile>
#include <QCoreApplication>


static const int ltravr_max_info_size       = (4800 + 128);
static const int ltravr_max_userdata_size   = 0x800;

typedef enum {
    MODULE_FLAGS_CRC_BE    = 0x1, /* признак, что CRC хранится в big-endian формате */
    MODULE_FLAGS_VER_BE    = 0x2, /* признак, что версия хранится в big-endian формате */
    MODULE_FLAGS_CRC_FFFF  = 0x4, /* признак, что вместо crc хранятся все 1 */
} t_module_flags;

typedef struct {
    WORD mid;
    const char* devname;
    DWORD info_offs;
    DWORD info_size;
    DWORD devname_offset;
    DWORD serial_offset;
    DWORD serial_size;
    DWORD cbr_offset;
    DWORD cbr_size;
    DWORD firm_ver_offset;
    DWORD firm_ver_size;
    DWORD firm_date_offset;
    DWORD firm_date_size;
    DWORD userdata_offset;
    DWORD userdata_size;
    DWORD flash_words;
    DWORD flags;
} t_module_param;


static t_module_param f_param_tbl[] = {
    {LTR_MID_LTR11,  "LTR11",   0x80,   75,   17, 25, 16, 41, 32,  1, 2, 3, 14, 0, 0, ATMEGA8515_FLASH_SIZE, MODULE_FLAGS_CRC_BE | MODULE_FLAGS_VER_BE},
    {LTR_MID_LTR12,  "LTR12",   0x80,  320,   20, 28, 16, 52, 320 - 52 - 2,  4, 2, 6, 14, 0x1000, 0x800, ATMEGA8515_FLASH_SIZE, 0},
    {LTR_MID_LTR114, "LTR114",  0x80,   79,   17, 25, 16, 41, 36,  1, 2, 3, 14, 0, 0, ATMEGA128_FLASH_SIZE, MODULE_FLAGS_CRC_BE | MODULE_FLAGS_VER_BE},
    {LTR_MID_LTR27,  "LTR27", 0x1F80, 0x80,   16, 32, 16,  0,  0, 68, 4, 0, 0,  0, 0, ATMEGA128_FLASH_SIZE, 0},
    {LTR_MID_LTR41,  "LTR41",   0x80,   44,   17, 25, 17,  0,  0,  1, 2, 3, 14, 0, 0, ATMEGA128_FLASH_SIZE, MODULE_FLAGS_VER_BE},
    {LTR_MID_LTR42,  "LTR42",   0x80,   44,   17, 25, 17,  0,  0,  1, 2, 3, 14, 0, 0, ATMEGA128_FLASH_SIZE, MODULE_FLAGS_VER_BE},
    {LTR_MID_LTR43,  "LTR43",   0x80,   44,   17, 25, 17,  0,  0,  1, 2, 3, 14, 0, 0, ATMEGA128_FLASH_SIZE, MODULE_FLAGS_VER_BE},
    {LTR_MID_LTR22,  "LTR22", 0x1F80 - 4800, 4800+0x80, 4816, 4832, 16,  0,  4800, 4868, 4, 0, 0,  0, 0, ATMEGA128_FLASH_SIZE, MODULE_FLAGS_CRC_FFFF},
    };

namespace LQMeas {
    LTRAvrUpdater::LTRAvrUpdater(LQMeas::LTRModule *module, QObject *parent) :
        QObject{parent}, m_module{module} {



    }

    void LTRAvrUpdater::writeFirmwareFromFile(const QString &filename, const QString &wrSerial, LQError &err) {
        QFile file{filename};
        if (!file.open(QFile::OpenModeFlag::ReadOnly)) {
            err = LTRAvrUpdaterErrors::fileOpen(filename);
        } else {
            QByteArray data {file.readAll()};
            file.close();
            writeFirmware(data, wrSerial, err);
        }
    }

    void LTRAvrUpdater::writeFirmware(const QByteArray &fwdata, const QString &wrSerial, LQError &err) {
        bool module_info_valid {false};
        const t_module_param *mpar {nullptr};
        for (size_t i {0}; !mpar && (i < sizeof f_param_tbl / sizeof f_param_tbl[0]); ++i) {
            if (f_param_tbl[i].mid == m_module->mid()) {
                mpar = &f_param_tbl[i];
            }
        }

        if (!mpar) {
            err = LTRAvrUpdaterErrors::unsupportedMid(m_module->mid());
        } else {
            bool was_opened = m_module->isOpened();
            if (was_opened) {
                m_module->close();
            }

            TLTRAVR hnd;
            LTRAVR_Init(&hnd);
            showProgress(tr("Open module"), 0);
            getDevError(LTRAVR_Open(&hnd, m_module->serviceRef().ltrdIPv4Addr(),
                                    m_module->serviceRef().ltrdTcpPort(),
                                    QSTRING_TO_CSTR(m_module->ltrCrateRef().devInfo().serial()),
                                    m_module->slot()),
                        err);
            if (err.isSuccess()) {
                QScopedArrayPointer<quint8> info(new quint8[ltravr_max_info_size]);
                QScopedArrayPointer<quint8> userdata(new quint8[ltravr_max_userdata_size]);

                showProgress(tr("Read module information"), 2);
                /* считываем информацию о модуле, которая хранится в его flash-памяти вместе с прошивкой */
                getDevError(LTRAVR_ReadProgrammMemory(&hnd, reinterpret_cast<WORD*>(info.data()),
                                                      (mpar->info_size+1)/2, mpar->info_offs/2),
                            err);
                if (err.isSuccess() && ((mpar->userdata_size > 0) && (mpar->userdata_size <= ltravr_max_userdata_size))) {
                    getDevError(LTRAVR_ReadProgrammMemory(&hnd, reinterpret_cast<WORD*>(userdata.data()),
                                                          (mpar->userdata_size+1)/2, mpar->userdata_offset/2),
                                err);
                }

                if (err.isSuccess()) {
                    /* проверяем ее CRC */
                    WORD crc_ev;
                    WORD crc = (WORD)(((WORD)info[mpar->info_size-1] << 8) | info[mpar->info_size-2]);
                    if (mpar->flags & MODULE_FLAGS_CRC_FFFF) {
                        crc_ev = 0xFFFF;
                    } else {
                        crc_ev = eval_crc16(0, info.data(), mpar->info_size-2);
                        if (mpar->flags & MODULE_FLAGS_CRC_BE) {
                            crc = ((crc >> 8) & 0xFF) | ((crc << 8) &0xFF00);
                        }
                    }
                    module_info_valid = crc == crc_ev;
                }

                if (err.isSuccess()) {
                    int fw_size {fwdata.size()};
                    if ((fw_size < (mpar->info_offs + mpar->info_size)) ||
                        (fw_size > mpar->flash_words*2)) {
                        err = LTRAvrUpdaterErrors::invalidFirmwareSize(fw_size);
                    }
                }

                if (err.isSuccess()) {
                    int buf_size {fwdata.size()};
                    /* если пользовательская область больше файла прошивки, то должны расширить
                     * буфер, чтобы вычитать в него также и пользовательскую область, так
                     * как стирается всегда весь чип целиком */
                    if ((mpar->userdata_offset + mpar->userdata_size) > buf_size) {
                        buf_size = mpar->userdata_offset + mpar->userdata_size;
                    }

                    /* Так как прошивки небольшие, то выделяем для простоты
                     * память сразу под всю прошивку и считываем ее из файла за раз.
                     * Второй буфер используется для проверки правильности записи,
                     * путем считывания содержимого памяти AVR и сравнения с тем что было записано */
                    QScopedArrayPointer<quint8> buf(new quint8[buf_size]);
                    QScopedArrayPointer<quint8> rbuf(new quint8[buf_size]);


                    memcpy(buf.data(), fwdata.data(), fwdata.size());


                    if (module_info_valid) {
                        /* Копируем серийный номер и калибровочные коэффициенты, чтобы они остались неизменными
                         * после перезаписи в случае, если они присутствовали в модуле */
                        if (mpar->serial_size)
                            memcpy(&buf[mpar->info_offs + mpar->serial_offset], &info[mpar->serial_offset], mpar->serial_size);
                        if (mpar->cbr_size)
                            memcpy(&buf[mpar->info_offs + mpar->cbr_offset], &info[mpar->cbr_offset], mpar->cbr_size);
                    }

                    if (!wrSerial.isEmpty()) {
                        strncpy(reinterpret_cast<char*>(&buf[mpar->info_offs + mpar->serial_offset]),
                                QSTRING_TO_CSTR(wrSerial), mpar->serial_size);
                        buf[mpar->info_offs + mpar->serial_offset + mpar->serial_size-1] = 0;
                    }

                    WORD crc;
                    if (mpar->flags & MODULE_FLAGS_CRC_FFFF) {
                        crc = 0xFFFF;
                    } else {
                        /* рассчитываем CRC для полученной информации */
                        crc = eval_crc16(0, &buf[mpar->info_offs], mpar->info_size-2);

                        if (mpar->flags & MODULE_FLAGS_CRC_BE) {
                            crc = ((crc >> 8) & 0xFF) | ((crc << 8) &0xFF00);
                        }
                    }

                    buf[mpar->info_offs + mpar->info_size - 2] = crc & 0xFF;
                    buf[mpar->info_offs + mpar->info_size - 1] = (crc>>8) & 0xFF;


                    /* если есть область пользовательских данных, то подменяем значения на считаные
                     * из flash-памяти контроллера */
                    if ((mpar->userdata_size > 0) && (mpar->userdata_size <= ltravr_max_userdata_size))  {
                        memcpy(&buf[mpar->userdata_offset], userdata.data(), mpar->userdata_size);
                    }


                    if (err.isSuccess()) {
                        showProgress(tr("Erase module flash"), 5);
                        getDevError(LTRAVR_ChipErase(&hnd), err);
                    }

                    if (err.isSuccess()) {
                        showProgress(tr("Write firmware to module flash"), 20);
                        /* запись самой прошивки (мб разбить на части, чтобы видеть прогресс) */
                        getDevError(LTRAVR_WriteProgrammMemoryCb(
                                        &hnd, reinterpret_cast<const WORD*>(buf.data()),
                                        buf_size/2, 0,
                                        wr_progr_cb, this),
                                    err);
                    }

                    if (err.isSuccess()) {
                        showProgress(tr("Read firmware from module flash"), 80);
                        getDevError(LTRAVR_ReadProgrammMemory(
                                        &hnd, reinterpret_cast<WORD*>(rbuf.data()),
                                        buf_size/2, 0),
                                    err);
                    }

                    if (err.isSuccess()) {
                        showProgress(tr("Verify firmware"), 95);
                        for (WORD i {0}; err.isSuccess() && (i < buf_size); ++i) {
                            if (rbuf[i] != buf[i]) {
                                err = LTRAvrUpdaterErrors::fwVerify(i, buf[i], rbuf[i]);
                            }
                        }
                    }

                    if (err.isSuccess()) {
                        showProgress(tr("Update finish"), 99);
                    }
                }
            }
            LTRAVR_Close(&hnd);

            if (was_opened) {
                LQError openErr;
                m_module->open(openErr);
                err += openErr;
            }
        }
    }

    void LTRAvrUpdater::writeFuses(bool ls, unsigned wrfuses, LQError &err) {

        TLTRAVR hnd;
        LTRAVR_Init(&hnd);

        showProgress(tr("Open module"), 0);
        getDevError(LTRAVR_Open(&hnd, m_module->serviceRef().ltrdIPv4Addr(),
                                m_module->serviceRef().ltrdTcpPort(),
                                QSTRING_TO_CSTR(m_module->ltrCrateRef().devInfo().serial()),
                                m_module->slot()),
                    err);
        if (err.isSuccess()) {
            showProgress(tr("Setup low speed mode for module interface"), 10);
            getDevError(LTRAVR_SetSpeedFlag(&hnd, ls), err);
        }
        if (err.isSuccess()) {
            DWORD fuses = wrfuses;
            showProgress(tr("Write module AVR Fuses"), 20);
            getDevError(LTRAVR_WriteFuseBits(&hnd, reinterpret_cast<BYTE*>(&fuses)), err);
            if (ls)
                err = LQError();
        }
        LTRAVR_Close(&hnd);

        if (ls) {
            TLTR010 hcrate;
            LTR010_Init(&hcrate);

            showProgress(tr("Restart crate"), 70);
            getCrateError(LTR010_Open(&hcrate, m_module->serviceRef().ltrdIPv4Addr(),
                                      m_module->serviceRef().ltrdTcpPort(),
                                      QSTRING_TO_CSTR(m_module->ltrCrateRef().devInfo().serial())),
                          err);
            if (err.isSuccess()) {
                getCrateError(LTR010_LoadFPGA(&hcrate, QSTRING_TO_CSTR(crateFirmFile()), 7, 0), err);
            }
            LTR010_Close(&hcrate);
        }
    }

    unsigned LTRAvrUpdater::readFuses(LQError &err) {
        unsigned ret {0};
        TLTRAVR hnd;
        LTRAVR_Init(&hnd);
        getDevError(LTRAVR_Open(&hnd, m_module->serviceRef().ltrdIPv4Addr(),
                                m_module->serviceRef().ltrdTcpPort(),
                                QSTRING_TO_CSTR(m_module->ltrCrateRef().devInfo().serial()),
                                m_module->slot()),
                    err);
        if (err.isSuccess()) {
            DWORD fuses = 0;
            getDevError(LTRAVR_ReadFuseBits(&hnd, reinterpret_cast<BYTE*>(&fuses)), err);
            if (err.isSuccess()) {
                ret = fuses;
            }
        }
        LTRAVR_Close(&hnd);
        return ret;
    }

    unsigned LTRAvrUpdater::checkFuses(unsigned reqFuses, LQError &err) {
        const unsigned fuses {readFuses(err)};
        if (err.isSuccess() && (fuses != reqFuses)) {
            err = LTRAvrUpdaterErrors::fuseVerify(reqFuses, fuses);
        }
        return fuses;
    }

    void LTRAvrUpdater::showProgress(const QString &stage, int perc) {
        Q_EMIT progress(stage, perc);
    }

    void LTRAvrUpdater::wr_progr_cb(void *cb_data, TLTRAVR *hnd, DWORD done_size, DWORD full_size) {
        static_cast<LTRAvrUpdater*>(cb_data)->showProgress(
            tr("Write firmware to module flash"), 20 + 60 * done_size / full_size);
    }

    void LTRAvrUpdater::getDevError(int err_code, LQError &err) {
        if (err_code != LTR_OK) {
            err = LQMeas::LTRNativeError::error(err_code, QSTRING_FROM_CSTR(LTRAVR_GetErrorString(err_code)));
        }
    }

    void LTRAvrUpdater::getCrateError(int err_code, LQError &err) {
        if (err_code != LTR_OK) {
            err = LQMeas::LTRNativeError::error(err_code, QSTRING_FROM_CSTR(LTR010_GetErrorString(err_code)));
        }
    }


    QString LTRAvrUpdater::crateFirmFile() {
        return QString("%1/%2").arg(qApp->applicationDirPath(), "ltr010v3_0_5.ttf");
    }
}
