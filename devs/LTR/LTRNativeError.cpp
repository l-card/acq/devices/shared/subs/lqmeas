#include "LTRNativeError.h"

namespace LQMeas {
    const QString &LTRNativeError::errorType() {
        static const QString err_type {QStringLiteral("ltr")};
        return err_type;
    }

    LQError LTRNativeError::error(int code, const QString &msg) {
        return LQError{code, msg, errorType()};
    }
}
