#include "LTRModuleRef.h"
#include "modules/LTRModule.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/devs/DeviceRefMethod.h"
#include "lqmeas/devs/DeviceInfo.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_slot {"Slot"};

    LTRModuleRef::LTRModuleRef(const LQMeas::DeviceInfo &info, const DeviceRef &crate, int slot_num) :
        DeviceRef{info, DeviceInterfaces::ltrModule(), DeviceRefMethods::crateSlot(), Flag::None, &crate},
        m_slot{slot_num} {
    }

    const QString &LTRModuleRef::typeRefTypeID() {
        static const QString str {QStringLiteral("ltrmodule")};
        return str;
    }

    QString LTRModuleRef::displayName() const {
        return QString{"%1 %2: %3"}.arg(LTRModule::tr("Slot"))
                .arg(moduleSlot(), 2)
                .arg(DeviceRef::displayName());
    }

    QString LTRModuleRef::protKey() const {
        return QString{"%1_Slot%2_%3"}
                .arg(parentRef()->key())
                .arg(moduleSlot())
                .arg(devInfo().type().deviceModificationName());
    }

    bool LTRModuleRef::sameDevice(const DeviceRef &ref) const {
        bool ok {ref.refTypeID() == typeRefTypeID()};
        if (ok) {
            const LTRModuleRef &ltrRef {static_cast<const LTRModuleRef &>(ref)};
            ok = DeviceRef::sameDevice(ref) && (ltrRef.parentPos() == parentPos());
        }
        return ok;
    }

    bool LTRModuleRef::sameDeviceLocation(const DeviceRef &ref) const {
        bool ok {ref.refTypeID() == typeRefTypeID()};
        if (ok) {
            const LTRModuleRef &ltrRef {static_cast<const LTRModuleRef &>(ref)};
            ok = ltrRef.parentRef()->sameDevice(*parentRef()) &&
                 (ltrRef.parentPos() == parentPos());
        }
        return ok;
    }

    DeviceRef::DevCheckResult LTRModuleRef::checkDevice(const Device &device) const  {
        const LTRModule *module {qobject_cast<const LTRModule *>(&device)};
        if (!module)
            return DevCheckResult::BadDevice;
        if (m_slot != module->slot())
            return DevCheckResult::BadDevice;
        if (!parentRef()->sameDevice(module->ltrCrateRef()))
            return DevCheckResult::BadDevice;
        return DeviceRef::checkDevice(device);
    }

    void LTRModuleRef::save(QJsonObject &refObj) const {
        DeviceRef::save(refObj);
        refObj[cfgkey_slot] = moduleSlot();
    }

    void LTRModuleRef::load(const QJsonObject &refObj) {
        DeviceRef::load(refObj);
        m_slot = loadSlot(refObj);
    }

    int LTRModuleRef::loadSlot(const QJsonObject &refObj) {
        return refObj[cfgkey_slot].toInt();
    }

    LTRModuleRef::LTRModuleRef(const LTRModuleRef &ref) : DeviceRef{ref}, m_slot{ref.m_slot} {

    }
}
