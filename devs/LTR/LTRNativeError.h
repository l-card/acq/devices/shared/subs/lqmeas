#ifndef LQMEAS_LTRNATIVEERROR_H
#define LQMEAS_LTRNATIVEERROR_H

#include "LQError.h"

namespace LQMeas {
    class LTRNativeError {
    public:
        static const QString &errorType();

        static LQError error(int code, const QString &msg);
    };
};

#endif // LQMEAS_LTRNATIVEERROR_H
