#ifndef LQMEAS_LTRSERVICE_H
#define LQMEAS_LTRSERVICE_H

#include <QList>
#include <QStringList>
#include <QSharedPointer>
#include "LTRServiceRef.h"
#include "lqmeas/devs/Device.h"
#include "lqmeas/devs/resolver/DevicesResolver.h"
#include "ltr/include/ltrapi.h"

namespace LQMeas {
    class LTRCrate;
    class LTRModule;
    class LTRServiceInfo;

    class LTRService : public Device, public DevicesResolver {
        Q_OBJECT
    public:
        ~LTRService() override;

        const LTRServiceRef &ref() const {return m_ref;}


        bool isOpened() const override;
        bool checkConnection() const override;

        DevicesResolver *childResolver() override {return static_cast<DevicesResolver *>(this); }

        DeviceRef *createRef() const override;

        QList<QSharedPointer<LTRCrate> > updateDevicesList(LQError &err);
        QList<QSharedPointer<LTRCrate> > creates() const {return m_crates;}
        QSharedPointer<LTRCrate> crate(const DeviceRef &ref) const;


        QList<QSharedPointer<LTRModule> > getModuleList(const QStringList &nameFilter, LQError &err);
        QList<QSharedPointer<LTRModule> > getModuleList(const DeviceValidator &validator, LQError &err);

        QSharedPointer<const LTRServiceInfo> devspecInfo() const;
    protected:
        QList<QSharedPointer<Device> > protGetDevList(const QList<const DeviceRef *> &devRefs, ResolveFlags flags) override;
        void protOpen(OpenFlags flags, LQError &err) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;
    private:
        void setParam(unsigned param, int val, LQError &err);

        explicit LTRService(const LTRServiceRef &ref = LTRServiceRef{});

        static void getError(INT err_code, LQError &err);

        QSharedPointer<LTRCrate> getCrate(const QString &serial, const DeviceInterface &deviface);
        QSharedPointer<LTRCrate> getCrate(const LTRCrate *crate);


        LTRServiceRef m_ref;
        mutable TLTR m_hsrv;
        QList< QSharedPointer<LTRCrate> > m_crates;

        friend class LTRResolver;
    };
}

#endif // LQMEAS_LTRSERVICE_H
