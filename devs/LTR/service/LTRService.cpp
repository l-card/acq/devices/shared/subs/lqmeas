#include "LTRService.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/devs/LTR/crates/LTRCrate.h"
#include "lqmeas/devs/LTR/LTRNativeError.h"
#include "lqmeas/devs/DeviceValidator.h"
#include "lqmeas/devs/DeviceNameValidator.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/StdErrors.h"
#include "lqmeas/lqtdefs.h"
#include "LTRServiceConfig.h"
#include "LTRServiceInfo.h"


namespace LQMeas {

    LTRService::LTRService(const LTRServiceRef &ref) :
        Device{new LTRServiceConfig{}, LTRServiceInfo{},
               ref.isDefualt() ? DeviceInterfaces::local() :
                                 DeviceInterfaces::ethernet()},
        m_ref{ref} {
        LTR_Init(&m_hsrv);
    }

    LTRService::~LTRService() {
        close();
    }

    void LTRService::protOpen(OpenFlags flags, LQError &err) {
        Q_UNUSED(flags)
        INT ltr_err {LTR_OpenSvcControl(&m_hsrv, m_ref.ltrdIPv4Addr(), m_ref.ltrdTcpPort())};
        if (ltr_err == LTR_OK) {
            DWORD srv_ver {0};
            ltr_err = LTR_GetServerVersion(&m_hsrv, &srv_ver);
             if (ltr_err == LTR_OK) {
                setDeviceInfo(LTRServiceInfo{srv_ver});
                m_ref.setInfo(*devspecInfo());
             }
        }
        getError(ltr_err, err);
    }

    void LTRService::protClose(LQError &err) {
        getError(LTR_Close(&m_hsrv), err);
    }

    void LTRService::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const LTRServiceConfig *setCfg {qobject_cast<const LTRServiceConfig *>(&cfg)};
        if (setCfg) {
            if (setCfg->moduleBuf().isEnabled()) {
                if (err.isSuccess()) {
                    setParam(LTRD_PARAM_MODULE_RECV_BUF_SIZE,
                             setCfg->moduleBuf().recvBufSize(), err);
                }
                if (err.isSuccess()) {
                    setParam(LTRD_PARAM_MODULE_SEND_BUF_SIZE,
                             setCfg->moduleBuf().sendBufSize(), err);
                }
            }
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }

    void LTRService::setParam(unsigned param, int val, LQError &err) {
        DWORD size = sizeof(val);
        getError(LTR_SetServerParameter(&m_hsrv, param, &val, size), err);
    }

    bool LTRService::isOpened() const {
        return LTR_IsOpened(&m_hsrv) == LTR_OK;
    }

    bool LTRService::checkConnection() const {
        DWORD srv_ver = 0;
        return isOpened() && (LTR_GetServerVersion(&m_hsrv, &srv_ver) == LTR_OK);
    }

    DeviceRef *LTRService::createRef() const {
        return m_ref.clone();
    }

    QList<QSharedPointer<LTRCrate> > LTRService::updateDevicesList(LQError &err) {        
        bool req_close = false;

        if (!isOpened()) {
            open(err);
            if (err.isSuccess()) {
                req_close = true;
            }
        }

        if (err.isSuccess()) {
            if (!checkConnection()) {
                close();
                open(err);
            }
        }

        if (err.isSuccess()) {
            INT err_code {LTR_OK};
            QList<QSharedPointer<LTRCrate> >  newCrateList;
            QList<QSharedPointer<LTRCrate> >  notFndCrates {m_crates};
            if (devspecInfo()->version() >= 0x02000000) {
                DWORD fnd_crates {0};
                err_code = LTR_GetCratesEx(&m_hsrv, 0, 0, &fnd_crates, nullptr, nullptr, nullptr);
                if (err_code == LTR_OK) {
                    QScopedArrayPointer<TLTR_CRATE_INFO> info_list{new TLTR_CRATE_INFO[fnd_crates]};
                    QScopedArrayPointer<char[LTR_CRATE_SERIAL_SIZE]> serials{new char[fnd_crates][LTR_CRATE_SERIAL_SIZE]};

                    err_code = LTR_GetCratesEx(&m_hsrv, fnd_crates, 0, nullptr, &fnd_crates,
                                               serials.data(), info_list.data());
                    if (err_code == LTR_OK) {
                        for (int i=0; i < static_cast<int>(fnd_crates); ++i) {
                            TLTR_CRATE_DESCR crate_descr;
                            TLTR_CRATE_STATISTIC crate_stat;
                            TLTR_MODULE_STATISTIC modstats[LTR_MODULES_PER_CRATE_MAX];
                            INT crate_err {LTR_OK};
                            BYTE crate_iface {info_list[i].CrateInterface};
                            const char *crate_serial {serials[i]};

                            crate_err = LTR_GetCrateDescr(&m_hsrv, crate_iface, crate_serial,
                                                          &crate_descr, sizeof(crate_descr));
                            if (crate_err == LTR_OK) {
                                crate_err = LTR_GetCrateStatistic(&m_hsrv, crate_iface, crate_serial,
                                                                  &crate_stat, sizeof(crate_stat));
                            }
                            if (crate_err == LTR_OK) {
                                for (int slot {0}; slot < LTR_MODULES_PER_CRATE_MAX; ++slot) {
                                    modstats[slot].mid = LTR_MID_EMPTY;
                                    if ((slot < crate_stat.modules_cnt)
                                            && (crate_stat.mids[slot] != LTR_MID_EMPTY)
                                            && (crate_stat.mids[slot] != LTR_MID_IDENTIFYING)) {
                                        (void)LTR_GetModuleStatistic(&m_hsrv, crate_iface, crate_serial,
                                                                     slot + LTR_CC_CHNUM_MODULE1,
                                                                     &modstats[slot], sizeof(modstats[slot]));
                                    }
                                }
                            }

                            if ((crate_err == LTR_OK) && (crate_stat.crate_mode == LTR_CRATE_MODE_WORK)) {
                                const QString serial {QSTRING_FROM_CSTR(serials[i])};
                                QSharedPointer<LTRCrate> crate = getCrate(serial, DeviceInterfaces::unknown());
                                if (!crate) {
                                    crate = QSharedPointer<LTRCrate>{
                                            new LTRCrate{m_ref, serial, info_list[i]}};
                                } else {
                                    notFndCrates.removeOne(crate);
                                }

                                crate->updateInfo(crate_descr, crate_stat, *devspecInfo());
                                crate->updateModulesInfo(modstats);

                                newCrateList.append(crate);
                            }
                        }
                    }
                }
            } else {
                char serials[LTR_CRATES_MAX][LTR_CRATE_SERIAL_SIZE];
                err_code = LTR_GetCrates(&m_hsrv, reinterpret_cast<BYTE*>(serials));
                if (err_code == LTR_OK) {
                    for (unsigned i = 0; i < LTR_CRATES_MAX; ++i) {
                        if (serials[i][0]!='\0') {
                            TLTR chnd;
                            INT crate_err = 0;
                            LTR_Init(&chnd);
                            chnd.cc = LTR_CC_CHNUM_CONTROL;
                            memcpy(chnd.csn, serials[i], LTR_CRATE_SERIAL_SIZE);
                            crate_err = LTR_Open(&chnd);
                            if (crate_err == LTR_OK) {
                                TLTR_CRATE_INFO crate_info;
                                QSharedPointer<LTRCrate> crate;
                                const QString serial {QSTRING_FROM_CSTR(serials[i])};
                                crate_err = LTR_GetCrateInfo(&chnd, &crate_info);

                                LTR_Close(&chnd);

                                if (crate_err == LTR_OK) {
                                    crate = getCrate(serial, DeviceInterfaces::unknown());
                                    if (!crate) {
                                        crate = QSharedPointer<LTRCrate>{
                                                new LTRCrate{m_ref, serial, crate_info}};
                                    } else {
                                        notFndCrates.removeOne(crate);
                                    }

                                    LQError crate_err;
                                    crate->updateModules(crate_err);
                                    newCrateList.append(crate);
                                }
                            }
                        }
                    }
                }
            }

            getError(err_code, err);
            if (err.isSuccess()) {
                m_crates = newCrateList;
                for(const QSharedPointer<LTRCrate> &crate : notFndCrates) {
                    crate->clear();
                }
            }
        }

        if (req_close)
            close();

        return m_crates;
    }

    QSharedPointer<LTRCrate> LTRService::crate(const DeviceRef &ref) const {
        QSharedPointer<LTRCrate> ret;
        for (const QSharedPointer<LTRCrate> &crate : m_crates) {
            DeviceRef::DevCheckResult checkRes {ref.checkDevice(*crate)};

            if (checkRes != DeviceRef::DevCheckResult::BadDevice) {
                if (checkRes == DeviceRef::DevCheckResult::OpenRequired) {
                    LQError err;
                    crate->open(err);
                    checkRes = ref.checkDevice(*crate);
                    crate->close(err);
                }

                if (checkRes == DeviceRef::DevCheckResult::GoodDevice) {
                    ret = crate;
                    break;
                }
            }
        }
        return ret;
    }

    QList<QSharedPointer<LTRModule> > LTRService::getModuleList(const QStringList &nameFilter, LQError &err) {
        return getModuleList(DeviceNameValidator{nameFilter}, err);
    }

    QList<QSharedPointer<LTRModule> > LTRService::getModuleList(const DeviceValidator &validator, LQError &err) {
        QList<QSharedPointer<LTRModule> > fndList;

        QList<QSharedPointer<LTRCrate> > crates {updateDevicesList(err)};
        for (const QSharedPointer<LTRCrate> &crate : crates) {
            fndList.append(crate->moduleList(validator));
        }
        return fndList;
    }

    QSharedPointer<const LTRServiceInfo> LTRService::devspecInfo() const {
        return devInfo().staticCast<const LTRServiceInfo>();
    }

    QList<QSharedPointer<Device> > LTRService::protGetDevList(const QList<const DeviceRef *> &devRefs,
                                                              DevicesResolver::ResolveFlags flags) {
        LQError err;
        Q_UNUSED(devRefs)
        Q_UNUSED(flags)

        const QList<QSharedPointer<LTRCrate> > crates {updateDevicesList(err)};
        QList<QSharedPointer<Device> > retlist;
        for (const QSharedPointer<LTRCrate> &crate : crates) {
            retlist.append(crate);
        }

        if (flags & ResolveFlag::Recursive) {
            for (const QSharedPointer<LTRCrate> &crate : crates) {
                retlist.append(crate->childResolver()->getDevList(devRefs, flags));
            }
        }
        return retlist;
    }

    void LTRService::getError(INT err_code, LQError &err)  {
        if (err_code != LTR_OK) {
            err = LTRNativeError::error(err_code,
                        QSTRING_FROM_CSTR(LTR_GetErrorString(err_code)));
        }
    }

    QSharedPointer<LTRCrate> LTRService::getCrate(const QString &serial, const DeviceInterface &deviface) {
        QSharedPointer<LTRCrate> ret;
        for (const QSharedPointer<LTRCrate> &curCrate : qAsConst(m_crates)) {
            if (curCrate->serial() == serial) {
                if ((deviface == DeviceInterfaces::unknown()) || (deviface == curCrate->iface())) {
                    ret = curCrate;
                    break;
                }
            }
        }
        return ret;
    }

    QSharedPointer<LTRCrate> LTRService::getCrate(const LTRCrate *crate) {
        QSharedPointer<LTRCrate> ret;
        for (const QSharedPointer<LTRCrate> &curCrate : qAsConst(m_crates)) {
            if (curCrate.data()==crate) {
                ret = curCrate;
                break;
            }
        }
        return ret;
    }
}
