#ifndef LQMEAS_LTRSERVICEMODULEBUFCONFIG_H
#define LQMEAS_LTRSERVICEMODULEBUFCONFIG_H

#include "lqmeas/devs/DeviceConfigIface.h"
namespace LQMeas {
    class LTRServiceModuleBufConfig : public DeviceConfigIface {
        Q_OBJECT
    public:
        QString cfgIfaceName() const override {return  QStringLiteral("LtrdModulesBufs");}

        explicit LTRServiceModuleBufConfig();
        explicit LTRServiceModuleBufConfig(const LTRServiceModuleBufConfig &cfg);


        bool isEnabled() const {return m_params.enabled;}
        int sendBufSize() const {return m_params.snd_buf_size;}
        int recvBufSize() const {return m_params.rcv_buf_size;}

        static int defaultSendBufSize() {return 512 * 1024;}
        static int defaultRecvBufSize() {return 1024 * 1024;}
    public Q_SLOTS:
        void setEnabled(bool en);
        void setSendBufSize(int wrds);
        void setRecvBufSize(int wrds);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        struct {
            bool enabled;
            int snd_buf_size;
            int rcv_buf_size;
        } m_params;
    };
}
#endif // LQMEAS_LTRSERVICEMODULEBUFCONFIG_H
