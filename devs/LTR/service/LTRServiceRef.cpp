#include "LTRServiceRef.h"
#include "LTRServiceInfo.h"
#include "LTRService.h"
#include "lqmeas/StdConfigKeys.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/devs/DeviceRefMethod.h"
#include "ltr/include/ltrapi.h"
#include <QJsonObject>
#include <QHostAddress>

namespace LQMeas {
    static const QLatin1String cfgkey_custom_tcp_port {"CustomTcpPort"};
    static const QLatin1String cfgkey_custom_tcp_port_en {"UseCustomTcpPort"};

    quint16 LTRServiceRef::defaultTcpPort() {
        return LTRD_PORT_DEFAULT;
    }

    quint32 LTRServiceRef::defaultIPv4Addr() {
        return LTRD_ADDR_DEFAULT;
    }

    LTRServiceRef::LTRServiceRef(quint32 ltrd_addr) :
        DeviceRef{LTRServiceInfo{}, DeviceInterfaces::unknown(), DeviceRefMethods::ipAddr()},
        m_ipv4_addr{ltrd_addr},
        m_custom_tcp_port{defaultTcpPort()},
        m_use_custom_tcp_port{false} {

        updateIface();
    }

    const QString &LTRServiceRef::typeRefTypeID() {
        static const QString str {QStringLiteral("ltrd_ipaddr")};
        return  str;
    }

    QString LTRServiceRef::displayName() const {
        const QString name {LTRService::tr("ltrd Service")};
        QString substr;
        if (isDefualt()) {
            substr = LTRService::tr("local");
        } else {
            substr = QString{"%1.%2.%3.%4"}
                    .arg((m_ipv4_addr >> 24) & 0xFF)
                    .arg((m_ipv4_addr >> 16) & 0xFF)
                    .arg((m_ipv4_addr >> 8) & 0xFF)
                    .arg((m_ipv4_addr >> 0) & 0xFF);
            if (m_use_custom_tcp_port) {
                substr.append(QString{":%1"}.arg(m_custom_tcp_port));
            }
        }
        return QString{"%1 (%2)"}.arg(name, substr);
    }

    DeviceRef::DevCheckResult LTRServiceRef::checkDevice(const Device &device) const {
        DeviceRef::DevCheckResult res {DeviceRef::checkDevice(device)};
        if (res == DeviceRef::DevCheckResult::GoodDevice) {
            const LTRService &svc {static_cast<const LTRService &>(device)};
            if (!sameDevice(svc.ref())) {
                res = DeviceRef::DevCheckResult::BadDevice;
            }
        }
        return  res;
    }

    bool LTRServiceRef::sameDevice(const DeviceRef &ref) const {
        bool same {DeviceRef::sameDevice(ref)};
        if (same) {
            const LTRServiceRef &svcRef {static_cast<const LTRServiceRef &>(ref)};
            same = (ltrdIPv4Addr() == svcRef.ltrdIPv4Addr()) &&
                    (ltrdTcpPort() == svcRef.ltrdTcpPort());
        }
        return same;
    }

    bool LTRServiceRef::operator==(const LTRServiceRef &ref) const {
        return  (m_ipv4_addr == ref.m_ipv4_addr) &&
                (ltrdTcpPort() == ref.ltrdTcpPort());
    }

    const LTRServiceInfo &LTRServiceRef::serviceInfo() const {
        return static_cast<const LTRServiceInfo &>(devInfo());
    }

    bool LTRServiceRef::isDefualt() const {
        return (ltrdIPv4Addr() == defaultIPv4Addr()) && (ltrdTcpPort() == defaultTcpPort());
    }

    bool LTRServiceRef::isLocal() const {
        return (ltrdIPv4Addr() == defaultIPv4Addr());
    }

    void LTRServiceRef::setIPv4Addr(quint32 addr) {
        m_ipv4_addr = addr;
        updateIface();
    }

    void LTRServiceRef::save(QJsonObject &refObj) const {
        DeviceRef::save(refObj);
        refObj[StdConfigKeys::ipv4Addr()] = QHostAddress{m_ipv4_addr}.toString();
        refObj[cfgkey_custom_tcp_port] = m_custom_tcp_port;
        refObj[cfgkey_custom_tcp_port_en] = m_use_custom_tcp_port;
    }

    void LTRServiceRef::load(const QJsonObject &refObj) {
        DeviceRef::load(refObj);

        QHostAddress hostAddr{refObj[StdConfigKeys::ipv4Addr()].toString()};
        m_ipv4_addr = hostAddr.isNull() ? defaultIPv4Addr() : hostAddr.toIPv4Address();
        m_custom_tcp_port = refObj[cfgkey_custom_tcp_port].toInt(defaultTcpPort()) & 0xFFFF;
        m_use_custom_tcp_port = refObj[cfgkey_custom_tcp_port_en].toBool(false);
        updateIface();
    }

    QString LTRServiceRef::protKey() const {
        return QString("ltrd_ip_%1_%2")
                .arg(ltrdIPv4Addr(), 8, 16, QChar('0'))
                .arg(ltrdTcpPort());
    }

    void LTRServiceRef::updateIface() {
        setIface(isLocal() ? DeviceInterfaces::unknown() : DeviceInterfaces::ethernet());
    }
}

