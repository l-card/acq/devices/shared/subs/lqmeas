#ifndef LQMEAS_LTRSERVICETYPEINFO_H
#define LQMEAS_LTRSERVICETYPEINFO_H


#include "lqmeas/devs/DeviceTypeInfo.h"

namespace LQMeas {
    class LTRServiceTypeInfo : public DeviceTypeInfo {
    public:
        static const LTRServiceTypeInfo &defaultTypeInfo();

        QString deviceTypeName() const override;
        const DeviceTypeSeries &deviceTypeSeries() const override;
        int maxChildDevicesCount() const override {return  INT_MAX;}

        QList<const DeviceTypeInfo *> modificationList() const override;
        QList<const DeviceInterface *> supportedConInterfaces() const override;
    };
}

#endif // LQMEAS_LTRSERVICETYPEINFO_H
