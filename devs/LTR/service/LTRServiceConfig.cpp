#include "LTRServiceConfig.h"

namespace LQMeas {
    const QString &LTRServiceConfig::typeConfigName() {
        static const QString str {QStringLiteral("LTRService")};
        return str;
    }

    LTRServiceConfig::LTRServiceConfig() {

        addManualConfigIface(&m_modbufs);
        init();
    }

    LTRServiceConfig::LTRServiceConfig(const LTRServiceConfig &cfg) :
        m_modbufs{cfg.moduleBuf()} {

        addManualConfigIface(&m_modbufs);
        init();
    }
}
