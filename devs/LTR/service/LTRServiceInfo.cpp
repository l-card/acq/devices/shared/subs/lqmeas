#include "LTRServiceInfo.h"
#include "LTRServiceTypeInfo.h"
#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"


namespace LQMeas {
    LTRServiceInfo::LTRServiceInfo(quint32 ver) :
    DeviceInfo{LTRServiceTypeInfo::defaultTypeInfo(), QString{}},
        m_ver{ver} {

    }

    QString LTRServiceInfo::versionStr() const {
        return m_ver > 0 ? QString("%1.%2.%3.%4")
                           .arg((m_ver >> 24) & 0xFF)
                           .arg((m_ver >> 16) & 0xFF)
                           .arg((m_ver >>  8) & 0xFF)
                           .arg((m_ver >>  0) & 0xFF) : QString();
    }

    LTRServiceInfo::LTRServiceInfo(const LTRServiceInfo &info) :
        DeviceInfo{info},
        m_ver{info.m_ver} {

    }

    void LTRServiceInfo::protSave(QJsonObject &infoObj) const {
        infoObj[StdConfigKeys::version()] = versionStr();
    }

    void LTRServiceInfo::protLoad(const QJsonObject &infoObj)     {
        QStringList verStrParts {infoObj[StdConfigKeys::version()].toString().split(QStringLiteral("."))};
        if (verStrParts.size() == 4) {
            m_ver = 0;
            for (int i = 0; i < verStrParts.size(); ++i) {
                m_ver <<= 8;
                m_ver |= verStrParts.at(i).toInt() & 0xFF;
            }
        }
    }

}
