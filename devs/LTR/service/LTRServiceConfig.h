#ifndef LQMEAS_LTRSERVICECONFIG_H
#define LQMEAS_LTRSERVICECONFIG_H

#include "lqmeas/devs/DeviceConfig.h"
#include "LTRServiceModuleBufConfig.h"

namespace LQMeas {
    class LTRServiceConfig : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();
        QString configDevTypeName() const override {return typeConfigName();}

        LTRServiceModuleBufConfig &moduleBuf() {return  m_modbufs;}
        const LTRServiceModuleBufConfig &moduleBuf() const {return  m_modbufs;}

        DeviceConfig *clone() const override {
            return new LTRServiceConfig(*this);
        }

        LTRServiceConfig();
        LTRServiceConfig(const LTRServiceConfig &cfg);

    private:
        LTRServiceModuleBufConfig m_modbufs;
    };
}

#endif // LQMEAS_LTRSERVICECONFIG_H
