#include "LTRServiceModuleBufConfig.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>



namespace LQMeas {
    static const QLatin1String cfgkey_bufcfg_en     {StdConfigKeys::enabled()};
    static const QLatin1String cfgkey_send_buf_size {"SendBufSize"};
    static const QLatin1String cfgkey_recv_buf_size {"RecvBufSize"};

    LTRServiceModuleBufConfig::LTRServiceModuleBufConfig() {
        memset(&m_params, 0, sizeof(m_params));
        m_params.enabled = false;
        m_params.snd_buf_size = defaultSendBufSize();
        m_params.rcv_buf_size = defaultRecvBufSize();
    }

    LTRServiceModuleBufConfig::LTRServiceModuleBufConfig(const LTRServiceModuleBufConfig &cfg)     {
        memcpy(&m_params, &cfg.m_params, sizeof(m_params));
    }

    void LTRServiceModuleBufConfig::setEnabled(bool en) {
        if (m_params.enabled != en) {
            m_params.enabled = en;
            notifyConfigChanged();
        }
    }

    void LTRServiceModuleBufConfig::setSendBufSize(int wrds) {
        if (m_params.snd_buf_size != wrds) {
            m_params.snd_buf_size = wrds;
            notifyConfigChanged();
        }
    }

    void LTRServiceModuleBufConfig::setRecvBufSize(int wrds) {
        if (m_params.rcv_buf_size != wrds) {
            m_params.rcv_buf_size = wrds;
            notifyConfigChanged();
        }
    }

    void LTRServiceModuleBufConfig::protSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_bufcfg_en] = m_params.enabled;
        cfgObj[cfgkey_send_buf_size] = m_params.snd_buf_size;
        cfgObj[cfgkey_recv_buf_size] = m_params.rcv_buf_size;
    }

    void LTRServiceModuleBufConfig::protLoad(const QJsonObject &cfgObj) {
        m_params.enabled = cfgObj[cfgkey_bufcfg_en].toBool(false);
        m_params.snd_buf_size = cfgObj[cfgkey_send_buf_size].toInt(defaultSendBufSize());
        m_params.rcv_buf_size = cfgObj[cfgkey_recv_buf_size].toInt(defaultRecvBufSize());
    }
}
