#include "LTRServiceTypeInfo.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/devs/DeviceTypeStdSeries.h"

namespace LQMeas {
const LTRServiceTypeInfo &LTRServiceTypeInfo::defaultTypeInfo() {
        static const LTRServiceTypeInfo info;
        return info;
    }

    QString LTRServiceTypeInfo::deviceTypeName() const {
        static const QString str {QStringLiteral("ltrd")};
        return str;
    }

    const DeviceTypeSeries &LTRServiceTypeInfo::deviceTypeSeries() const {
        return DeviceTypeStdSeries::ltr();
    }

    QList<const DeviceTypeInfo *> LTRServiceTypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list { &defaultTypeInfo() };
        return list;
    }

    QList<const DeviceInterface *> LTRServiceTypeInfo::supportedConInterfaces() const  {
        static const QList<const DeviceInterface *> ifaces {
            &DeviceInterfaces::local(),
            &DeviceInterfaces::ethernet()
        };
        return ifaces;
    }
}
