#ifndef LQMEAS_LTRSERVICEINFO_H
#define LQMEAS_LTRSERVICEINFO_H

#include "lqmeas/devs/DeviceInfo.h"


namespace LQMeas {
    class LTRServiceInfo : public DeviceInfo {
    public:
        LTRServiceInfo(quint32 ver = 0);

        DeviceInfo *clone() const override {
            return new LTRServiceInfo(*this);
        }

        quint32 version() const {return m_ver;}
        QString versionStr() const;
    protected:
        LTRServiceInfo(const LTRServiceInfo &info);

        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        quint32 m_ver;
    };
}

#endif // LQMEAS_LTRSERVICEINFO_H
