#ifndef LQMEAS_LTRSERVICEREF_H
#define LQMEAS_LTRSERVICEREF_H

#include "lqmeas/devs/DeviceRef.h"

class QJsonObject;

namespace LQMeas {
    class LTRServiceInfo;

    class LTRServiceRef : public DeviceRef {
    public:
        static quint16 defaultTcpPort();
        static quint32 defaultIPv4Addr();

        LTRServiceRef(quint32 ltrd_addr = defaultIPv4Addr());

        DeviceRef *clone() const override {return new LTRServiceRef{*this};}


        static const QString &typeRefTypeID();
        QString refTypeID() const override {return typeRefTypeID();}

        QString displayName() const override;

        bool serialUsed() const  override {return false;}
        bool isAutoResolved() const override {return false;}

        DevCheckResult checkDevice(const Device &device) const override;
        bool sameDevice(const DeviceRef &ref) const override;

        bool operator==(const LTRServiceRef &ref) const;

        const LTRServiceInfo &serviceInfo() const;

        bool isDefualt() const;
        bool isLocal() const;

        quint32 ltrdIPv4Addr() const {return m_ipv4_addr;}
        quint16 ltrdTcpPort() const {return m_use_custom_tcp_port ? m_custom_tcp_port : defaultTcpPort(); }

        void setIPv4Addr(quint32 addr);
        void setCustomTcpPort(quint16 port) {m_custom_tcp_port = port;}
        void setUseCustomTcpPort(bool en) {m_use_custom_tcp_port = en;}
        quint16 customTcpPort() const {return m_custom_tcp_port;}
        bool useCustomTcpPort() const {return m_use_custom_tcp_port;}

        void save(QJsonObject &refObj) const override;
        void load(const QJsonObject &refObj) override;
    protected:
        bool checkDevIface() const override {return false;}
        QString protKey() const override;
    private:
        void updateIface();

        quint32 m_ipv4_addr;
        quint16 m_custom_tcp_port;
        bool m_use_custom_tcp_port;
    };
}

#endif // LQMEAS_LTRSERVICEREF_H
