#ifndef DEVICEINTERFACE_H
#define DEVICEINTERFACE_H

#include "lqmeas/EnumNamedValue.h"
#include <QObject>

namespace LQMeas {
    class  DeviceInterface : public EnumNamedValue<QString> {
    public:
        bool operator ==(const DeviceInterface &other) const;
        bool operator !=(const DeviceInterface &other) const;
    };

    class DeviceInterfaces : public QObject {
        Q_OBJECT
    public:
        static const DeviceInterface &unknown();
        static const DeviceInterface &usb();
        static const DeviceInterface &pci();
        static const DeviceInterface &pcie();
        static const DeviceInterface &ethernet();
        static const DeviceInterface &ltrModule();
        static const DeviceInterface &local();

        static const QList<const DeviceInterface *> &fullList();
        static const DeviceInterface &getByID(const QString &id, const DeviceInterface &defaultIface = unknown());
    };

}

#endif // DEVICEINTERFACE_H
