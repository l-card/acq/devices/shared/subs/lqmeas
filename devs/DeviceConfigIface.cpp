#include "DeviceConfigIface.h"

namespace LQMeas {

    DeviceConfigIface::DeviceConfigIface(QObject *parent) : DeviceConfigItem{parent} {

    }


    DeviceConfigIface::~DeviceConfigIface() {

    }



    void DeviceConfigIface::init(DeviceConfig *cfg) {
        m_devCfg = cfg;
        protInit(cfg);
    }
}
