#ifndef LQMEAS_DEVICETYPESERIES_H
#define LQMEAS_DEVICETYPESERIES_H

#include <QObject>
#include "lqmeas/EnumNamedValue.h"

namespace LQMeas {
    class DeviceTypeSeries : public EnumNamedValue<QString> {
    public:
        virtual QString displayFullName() const = 0;
    };
}

Q_DECLARE_METATYPE(const LQMeas::DeviceTypeSeries *);

#endif // LQMEAS_DEVICETYPESERIES_H
