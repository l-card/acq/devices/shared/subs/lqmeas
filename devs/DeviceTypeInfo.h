#ifndef DEVICETYPEINFO_H
#define DEVICETYPEINFO_H

#include <QString>
#include <QList>

namespace LQMeas {
    class DevOutInfo;
    class DevAdcInfo;
    class DevAdcTareInfo;
    class DevInDigInfo;
    class DevInCntrInfo;
    class DevSyncMarksGenInfo;
    class DevSyncMarksRecvInfo;
    class DeviceInterface;
    class DeviceTypeSeries;

    /* Информация о типе устройства или конкретной модификации.
       Включает в себя общее название типа, название конкретной модификации,
       а также информацию о поддерживаемых возможностях для данной модфицикации.

       Есть общие классы, описывающие возможности для поддержки стандартных программных
       интерфейсов (АЦП, вывод) - если они поддерживаются устройством, то должны
       быть реализованы методы, возвращающие указазатель на класс с данной
       стандартной информацией (это может быть указатель объекта на себя).

       Каждый класс устройства реализует свой класс информации типа <тип устройтсва>TypeInfo.
       Как правило в нем реализуется статический метод defaultInfo, возвращающий
       информацию о модификации по умолчанию (которая используется до ее определения).
       Если есть несколько модификаций, то для каждой может быть создан свой статический
       метод для ее получения и метод types() для общего списка поддерживаемых
       модификаций.
       Также в случае, если модификаций может быть не ограниченное количество
       (или одна модификация с поддержкой изменяемых параметров, как вставленных
       мазанинов), то она может быть создана динамически (в этом случае
       dynamicAllocated() должна вернуть true). Для корректного очищения
       для передачи информации в другой объект нужно использовать clone() (который
       должен быть для этого случая переопределен), а когда информация больше
       не нужна --- release().

    */
    class DeviceTypeInfo {
    public:
        /* название типа устройства (без модификаций) */
        virtual QString deviceTypeName() const = 0;
        /* используемое в API устройства имя типа, на случай отличия от deviceTypeName() */
        virtual QString deviceTypeApiName() const {return deviceTypeName();}
        /* название устройства с учетом модификаций */
        virtual QString deviceModificationName() const;
        /* серия устройств к которой относится данный тип устройств */
        virtual const DeviceTypeSeries &deviceTypeSeries() const = 0;


        /* если устройство может иметь дочерние устройства (крейт),
         * то данный метод должен быть переопределен и указывать
         * их максимальное количество */
        virtual int maxChildDevicesCount() const {return 0;}

        /* Интерфейс для получения информации о возможностях АЦП */
        virtual const DevAdcInfo *adc() const {return nullptr;}
        /* Интерфейс для получения информации о возможностях аппаратной тарировки АЦП */
        virtual const DevAdcTareInfo *adcTare() const {return nullptr;}
        /* Интерфейс предоставляет информацию о возможностях вывода устройства
           (как цифровых линий, так и аналоговых сигналов) */
        virtual const DevOutInfo *out() const {return nullptr;}
        /* Интерфейс предоставляет информацию о возможностях ввода устройства
         * по цифровым линиям */
        virtual const DevInDigInfo *digin() const {return nullptr;}
        /* Интерфейс предоставляет информацию о возможностях устройства
         * принимать уже вычисленные значения счетчиков импульсов */
        virtual const DevInCntrInfo *inCntr() const {return nullptr;}
        /* Интерфейс предоставляет информацию о возможностях генерации устройством
           синхрометок */
        virtual const DevSyncMarksGenInfo *syncMarkGen() const {return nullptr;}
        /* Интерфейс предоставляет информацию о возможностях приема устройством
           синхрометок */
        virtual const DevSyncMarksRecvInfo *syncMarkRecv() const {return  nullptr;}

        /* метод вызывается для передачи копии информации. Если информация может
         * быть выделена динамически, то должен быть переопределен вместе с
         * isDynamicAllocated() */
        virtual const DeviceTypeInfo *clone() const {return this;}
        /* метод вызывается, как только информация перестает быть нужна. */
        void release() const { if (isDynamicAllocated()) delete this;}

        virtual ~DeviceTypeInfo() {}

        /* полный списко модификаций устройств */
        virtual QList<const DeviceTypeInfo *> modificationList() const = 0;
        /* списко видимых модификаций (не включает устаревшие устройства) */
        virtual QList<const DeviceTypeInfo *> visibleModificationList() const {return modificationList();}
        /* проверка, совместим ли текущий тип устройства с указанным */
        virtual bool isCompatible(const DeviceTypeInfo &type) const {
            return type.deviceTypeName() == deviceTypeName();
        }

        virtual QList<const DeviceInterface *> supportedConInterfaces() const = 0;

    protected:
        /* должно возвращать true если память под информацию выделена динамически */
        virtual bool isDynamicAllocated() const {return false;}
    };
}

#endif // DEVICETYPEINFO_H
