#include "DeviceInfo.h"
#include <QJsonObject>
#include <QList>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String cfgkey_modification{"Mod"};


    DeviceInfo::DeviceInfo(const DeviceTypeInfo &type, const QString &serial) :
        m_type{type.clone()}, m_serial{serial} {
    }

    DeviceInfo::~DeviceInfo() {
        m_type->release();
    }

    bool DeviceInfo::sameDevice(const DeviceInfo &info) const {
        return (m_type->deviceModificationName() == info.m_type->deviceModificationName()) &&
                ((info.serial() == m_serial) || m_serial.isEmpty() || info.serial().isEmpty());
    }

    DeviceInfo *DeviceInfo::clone() const {
        return new DeviceInfo{type(), m_serial};
    }

    void DeviceInfo::save(QJsonObject &infoObj) const {
        saveBaseInfo(infoObj, m_type->deviceTypeName(), m_type->deviceModificationName(), m_serial);
        protSave(infoObj);
    }

    void DeviceInfo::load(const QJsonObject &infoObj) {
        const QString modification_name {infoObj[cfgkey_modification].toString()};
        if (modification_name != m_type->deviceModificationName()) {
            QList<const DeviceTypeInfo *> modList = m_type->modificationList();
            for (const DeviceTypeInfo *mod : modList) {
                if (mod->deviceModificationName() == modification_name) {
                    setType(mod);
                    break;
                }
            }
        }

        m_serial = infoObj[StdConfigKeys::serialNumber()].toString();
        protLoad(infoObj);
    }

    void DeviceInfo::saveBaseInfo(QJsonObject &infoObj, const QString &dev_type,
                                  const QString &dev_mod, const QString &dev_serial)  {
        infoObj[StdConfigKeys::serialNumber()] = dev_serial;
        infoObj[StdConfigKeys::type()] = dev_type;
        infoObj[cfgkey_modification] = dev_mod;
    }

    QString DeviceInfo::loadDeviceModification(const QJsonObject &infoObj) {
        return infoObj[cfgkey_modification].toString();
    }

    DeviceInfo::DeviceInfo(const DeviceInfo &info) {
        m_serial = info.m_serial;
        m_type = info.m_type->clone();
    }

    void DeviceInfo::setType(const DeviceTypeInfo *type) {
        m_type->release();
        m_type = type;
    }
}
