#ifndef LQMEAS_X502TYPEINFO_H
#define LQMEAS_X502TYPEINFO_H

#include "lqmeas/devs/DeviceTypeInfo.h"
#include "lqmeas/ifaces/in/adc/DevAdcInfo.h"
#include "lqmeas/ifaces/in/dig/DevInDigInfo.h"
#include "lqmeas/ifaces/out/DevOutInfo.h"
#include "x502Config.h"

namespace LQMeas {
    class x502TypeInfo : public DeviceTypeInfo, public DevAdcInfo, public DevOutInfo, public DevInDigInfo {
public:
        static const QString &name();

        static const x502TypeInfo &defaultTypeInfo();
        QString deviceTypeName() const override {return name();}
        const DeviceTypeSeries &deviceTypeSeries() const override;

        bool galPresent() const {return m_gal_present;}
        bool dacPresent() const {return m_dac_present;}
        bool bfPresent() const {return m_bf_present;}
        bool gdPresent() const {return m_gd_present;}
        bool ethPresent() const {return m_eth_present;}
        bool industrial() const {return m_industrial;}


        virtual x502Config::PullResistors supportedPullResistors() const {
            return x502Config::PullResistor::NoResistors;
        }

        /* ----------------- DevAdcInfo -------------------------------------*/
        const DevAdcInfo *adc() const override {return static_cast<const DevAdcInfo *>(this);}

        AdcType adcType() const override {return AdcType::Sequential;}
        int adcChannelsCnt() const override;

        double adcFreqMax() const override {return 2000000;}
        bool adcIsChFreqConfigurable() const override {return true;}

        int adcChRangesCnt(int ch_num, int unit_mode_num = 0) const override;
        double adcChRangeMaxVal(int ch_num, int range, int unit_mode_num = 0) const override;
        double adcChRangeMinVal(int ch_num, int range, int unit_mode_num = 0) const override;

        /* ----------------- DevOutInfo -------------------------------------*/
        const DevOutInfo *out() const override {return static_cast<const DevOutInfo *>(this);}

        bool outDacSyncSupport() const override {return m_dac_present;}
        bool outDigSyncSupport() const override {return true;}

        bool outDacAsyncSupport() const override {return m_dac_present;}
        bool outDigAsyncSupport() const override {return true;}

        bool outDacSyncModeCfgPerCh() const override {return true;}

        bool outSyncRamModeIsConfigurable() const override {return true;}
        bool outSyncDacChGenModeIsConfigurable() const override {return false;}
        bool outSyncDigChGenModeIsConfigurable() const override {return false;}


        double outSyncGenFreqMax() const override;
        bool outSyncPresetSupport() const override {return true;}

        int outDacChannelsCnt() const override;
        int outDigChannelsCnt() const override;

        int outDacChRangesCnt(int ch) const override {Q_UNUSED(ch) return 1;}
        double outDacChRangeMaxVal(int ch, int range) const override;
        double outDacChRangeMinVal(int ch, int range) const override;

        /* ---------------- DevInDigInfo -------------------------------------*/
        const DevInDigInfo *digin() const override {return static_cast<const DevInDigInfo *>(this);}

        int inDigChannelsCnt() const override;
        bool inDigSyncSupport(void) const override {return true;}
        double inDigSyncFreqMax(void) const override;

        /* -------------------------------------------------------------------*/

        virtual double extRefFreqMax() const;
        virtual QList<x502Config::RefFreq> supportedIntRefFreqs() const {return {x502Config::RefFreq::Freq_2000KHz, x502Config::RefFreq::Freq_1500KHz};}
        virtual x502Config::RefFreq intRefFreqDefault() const {return supportedIntRefFreqs().at(0);}
        virtual bool intRefFreqIsConfigurable() const {return supportedIntRefFreqs().size() > 1;}
        virtual QList<x502Config::SyncMode> supportedSyncFreqModes() const {
            return {
                x502Config::SyncMode::Internal,
                x502Config::SyncMode::ExternalMaster,
                x502Config::SyncMode::SYN1Rise,
                x502Config::SyncMode::SYN1Fall,
                x502Config::SyncMode::SYN2Rise,
                x502Config::SyncMode::SYN2Fall
            };
        }
        virtual QList<x502Config::SyncMode> supportedSyncStartModes() const {
            return {
                x502Config::SyncMode::Internal,
                x502Config::SyncMode::ExternalMaster,
                x502Config::SyncMode::SYN1Rise,
                x502Config::SyncMode::SYN1Fall,
                x502Config::SyncMode::SYN2Rise,
                x502Config::SyncMode::SYN2Fall
            };
        }

        virtual bool adcChAvgSupport() const {return true;}

        virtual unsigned int adcFreqDivMax() const;        
        virtual unsigned int inDigFreqDivMax() const {return adcFreqDivMax();}
        virtual unsigned int outSyncGenFreqDivMax() const;

        virtual bool outSyncGenCycleModeSupport() const {return true;}

        virtual bool syncAnalogModeSupport() const {return false;}
        virtual bool syncIoModeConfigSupport() const {return false;} /* поддержка настройки направления и сигнала выдачи для линий синхронизации */
        virtual bool extUSrcConfigSupport() const {return false;} /* поддержка настройки источников питания доп. устройств */

        virtual int extUSrcRangesCnt() const {return 0;}
        virtual double extUSrcRangeMaxVal(int range) const {return 0;}
        virtual double extUSrcRangeMinVal(int range) const {return 0;}
        virtual const LQMeas::Unit &extUSrcRangeUnit() const;

        virtual bool mcuAvailable() const {return false;}
        virtual bool pldaAvailable() const {return true;}
        virtual bool ethOptionApplicable() const {return false;}
        virtual bool bfOptionApplicable() const {return true;}
        virtual bool galOptionApplicable() const {return true;}


        QList<const DeviceTypeInfo *> modificationList() const override;
        QList<const DeviceInterface *> supportedConInterfaces() const override {
            return QList<const DeviceInterface *>{};
        }
    protected:
        explicit x502TypeInfo(unsigned flags);


    private:
        bool m_gal_present;
        bool m_dac_present;
        bool m_bf_present;
        bool m_eth_present;
        bool m_industrial;
        bool m_gd_present;
    };
}

#endif // LQMEAS_X502TYPEINFO_H
