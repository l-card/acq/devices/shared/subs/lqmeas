#include "x502.h"
#include "x502Info.h"
#include "x502Config.h"
#include "x502NativeError.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdErrors.h"
#include "lqmeas/log/Log.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/devs/DeviceRef.h"
#include "lqmeas/devs/DeviceRefMethod.h"
#include "lqmeas/ifaces/out/SyncModes/stream/DevOutSyncModeImplStream.h"
#include "lqmeas/ifaces/out/SyncModes/cycle/DevOutSyncModeImplCycle.h"
#include "lqmeas/lqtdefs.h"
#include <QMutexLocker>
#include <QStringBuilder>
#include <QThread>
#include <memory>


namespace LQMeas {
    static uint32_t f_adc_ch_mode(x502AdcConfig::AdcChMode mode) {
        return mode == x502AdcConfig::AdcChMode::Comm ? X502_LCH_MODE_COMM :
               (mode == x502AdcConfig::AdcChMode::Zero ? X502_LCH_MODE_ZERO :
                                                         X502_LCH_MODE_DIFF);
    }

    static t_x502_sync_mode f_conv_sync_mode(x502Config::SyncMode syncMode) {
        t_x502_sync_mode ret = X502_SYNC_INTERNAL;
        if (syncMode == x502Config::SyncMode::Internal) {
            ret = X502_SYNC_INTERNAL;
        } else if (syncMode == x502Config::SyncMode::ExternalMaster) {
            ret = X502_SYNC_EXTERNAL_MASTER;
        } else if (syncMode == x502Config::SyncMode::SYN1Rise) {
            ret = X502_SYNC_DI_SYN1_RISE;
        } else if (syncMode == x502Config::SyncMode::SYN1Fall) {
            ret = X502_SYNC_DI_SYN1_FALL;
        } else if (syncMode == x502Config::SyncMode::SYN2Rise) {
            ret = X502_SYNC_DI_SYN2_RISE;
        } else if (syncMode == x502Config::SyncMode::SYN2Fall) {
            ret = X502_SYNC_DI_SYN2_FALL;
        } else if (syncMode == x502Config::SyncMode::TRIGRise) {
            ret = E16_SYNC_TRIG_RISE;
        } else if (syncMode == x502Config::SyncMode::TRIGFall) {
            ret = E16_SYNC_TRIG_FALL;
        } else if (syncMode == x502Config::SyncMode::INTFall) {
            ret = E16_SYNC_INT_FALL;
        } else if (syncMode == x502Config::SyncMode::INTFall) {
            ret = E16_SYNC_INT_FALL;
        } else if (syncMode == x502Config::SyncMode::AdcChRise) {
            ret = E16_SYNC_ADC_EDGE_RISE;
        } else if (syncMode == x502Config::SyncMode::AdcChFall) {
            ret = E16_SYNC_ADC_EDGE_FALL;
        } else if (syncMode == x502Config::SyncMode::AdcChHigh) {
            ret = E16_SYNC_ADC_ABOVE_LEVEL;
        } else if (syncMode == x502Config::SyncMode::AdcChLow) {
            ret = E16_SYNC_ADC_BELOW_LEVEL;
        }
        return ret;
    }

    static t_x502_ref_freq f_conv_ref_freq(x502Config::RefFreq refFreq) {
        return  refFreq == x502Config::RefFreq::Freq_1500KHz ? X502_REF_FREQ_1500KHZ : X502_REF_FREQ_2000KHZ;
    }


    x502::x502(const t_x502_devrec &devrec, const x502Info &info) :
        Device{new x502Config{}, info, devrecIface(devrec)},
        DevAdc{this, info.devspecTypeInfo().adcChannelsCnt()},
        DevOutSync{this},
        DevOutAsyncDac{this},
        DevInSyncDig{this},
        DevOutDacConverter{this},
        m_sender{this} {

        m_devrec = devrec;
        m_hnd = X502_Create();

        outSyncAddMode(new DevOutSyncModeImplStream{this, this});
        outSyncAddMode(new DevOutSyncModeImplCycle{this, this});
    }

    void x502::setNewDevRecord(const t_x502_devrec &devrec) {
        X502_FreeDevRecordList(&m_devrec,1);
        m_devrec = devrec;
    }

    void x502::startStream(bool &flag, LQError &err) {
        if (!streamInOutRunning()) {
            getError(X502_StreamsStart(m_hnd), err);
            if (err.isSuccess())
                flag = true;
        } else {
            flag = true;
        }
    }

    void x502::stopStream(bool &flag, LQError &err)  {
        if (flag) {
            flag = false;
            if (!streamInOutRunning()) {
                getError(X502_StreamsStop(m_hnd), err);
                m_wrdsBuf.clear();
            }
        }
    }


    x502::~x502() {
        X502_Free(m_hnd);
        X502_FreeDevRecordList(&m_devrec,1);
    }

    QString x502::location() const {
        return QString::fromUtf8(m_devrec.location);
    }

    bool x502::isOpened() const {
        return m_opened;
    }

    bool x502::checkConnection() const {
        bool ok {isOpened()};
        if (ok && (X502_IsRunning(m_hnd) != X502_ERR_OK)) {
            uint8_t fbype;
            ok = X502_FlashRead(m_hnd, 0, &fbype, sizeof(fbype)) == X502_ERR_OK;
        }
        return ok;
    }

    bool x502::supportCustomOutFreq() const {
        return X502_CheckFeature(m_hnd, X502_FEATURE_OUT_FREQ_DIV) == X502_ERR_OK;
    }

    const x502Config &x502::devspecConfig() const {
        return static_cast<const x502Config &>(config());
    }

    QSharedPointer<const x502Info> x502::devspecInfo() const {
        return devInfo().staticCast<const x502Info>();
    }

    void x502::getError(int err_code, LQError &err) {
        return getError(err_code, QString{}, err);
    }

    void x502::getError(int err_code, const QString &descr, LQError &err) {
        if (err_code != X502_ERR_OK) {
            err = error(err_code, descr);
        }
    }

    LQError x502::error(int err_code) {
        return error(err_code, QString{});
    }

    LQError LQMeas::x502::error(int err_code, const QString &descr) {
        return err_code == X502_ERR_OK ? LQError::Success() : x502NativeError::error(
                    err_code, (descr.isEmpty() ? descr : descr % QLatin1String(": "))
                    % QSTRING_FROM_CSTR(X502_GetErrorString(err_code)));
    }

    DeviceRef *x502::createRef() const {
        return new DeviceRef{*devInfo(), iface(), DeviceRefMethods::serialNumber()};
    }

    void x502::protOpen(OpenFlags flags, LQError &err) {
        Q_UNUSED(flags)

        t_x502_info devinfo;
        int32_t err_code {X502_OpenByDevRecord(m_hnd, &m_devrec)};
        if (err_code == X502_ERR_OK) {
            err_code = X502_GetDevInfo(m_hnd, &devinfo);
        }
        if (err_code == X502_ERR_OK) {
            m_opened = true;
            m_outStreams = 0;
            m_outRunning = false;
            m_dinRunning = false;
            m_adcRunning = false;
            m_dinEnabled = false;
            m_adcEnabled = false;
            m_outStreams = 0;

            const std::unique_ptr<const x502Info> info{createInfo(devinfo)};
            setDeviceInfo(*info);
        } else {
            X502_Close(m_hnd);
            getError(err_code, err);
        }
    }

    void x502::protClose(LQError &err) {
        getError(X502_Close(m_hnd), err);
        m_opened = false;
    }




    void x502::protConfigure(const DeviceConfig &cfg, LQError &err) {
        const x502Config *setCfg {qobject_cast<const x502Config *>(&cfg)};
        if (setCfg) {
            unsigned lch_cnt {0};
            QString errDescr;
            int err_code;


            if (setCfg->bfIsEnabled() && devspecInfo()->bfPresent()) {
                err_code = X502_BfLoadFirmware(m_hnd, QSTRING_TO_CSTR(setCfg->bfFirmwareFilename()));
                if (err_code != X502_ERR_OK) {
                    errDescr = tr("Load blackfin firmware");
                }
            } else {
                err_code = X502_SetMode(m_hnd, X502_MODE_FPGA);
                if (err_code != X502_ERR_OK) {
                    errDescr = tr("Setup FPGA mode");
                }
            }

            for (int ch {0}; (ch < devTypeInfo().adc()->adcChannelsCnt()) && (err_code == X502_ERR_OK); ++ch) {
                if (setCfg->adc().adcChEnabled(ch)) {
                    x502AdcConfig::AdcChMode mode = setCfg->adc().adcChMode(ch);
                    err_code = X502_SetLChannel(m_hnd, lch_cnt, static_cast<uint32_t>(ch),
                                                 f_adc_ch_mode(mode),
                                                 static_cast<uint32_t>(setCfg->adc().adcChRangeNum(ch)),
                                                 static_cast<uint32_t>(setCfg->adc().adcChAvg(ch)));
                    ++lch_cnt;
                    if (err_code != X502_ERR_OK) {
                        errDescr = tr("Setup channel %1 parameters").arg(QString::number(ch+1));
                    }
                }
            }

            if (err_code == X502_ERR_OK) {
                err_code = X502_SetLChannelCount(m_hnd, lch_cnt);
                if (err_code != X502_ERR_OK) {
                    errDescr = tr("Setup channels count");
                }
            }

            if (err_code == X502_ERR_OK) {
                err_code = X502_SetSyncMode(m_hnd, f_conv_sync_mode(setCfg->syncFreqMode()));
                if (err_code != X502_ERR_OK) {
                    errDescr = tr("Setup sync mode");
                }
            }

            if (err_code == X502_ERR_OK) {
                err_code = X502_SetSyncStartMode(m_hnd, f_conv_sync_mode(setCfg->syncStartMode()));
                if (err_code != X502_ERR_OK) {
                    errDescr = tr("Setup sync start mode");
                }
            }

            if ((err_code == X502_ERR_OK) && devspecInfo()->devspecTypeInfo().intRefFreqIsConfigurable()) {
                err_code = X502_SetRefFreq(m_hnd, f_conv_ref_freq(setCfg->refFreqInternal()));
                if (err_code != X502_ERR_OK) {
                    errDescr = tr("Setup reference frequency");
                }
            }

            if ((err_code == X502_ERR_OK) && devspecInfo()->devspecTypeInfo().syncAnalogModeSupport()) {
                err_code = X502_SetAdcSyncStartValue(m_hnd, setCfg->syncAdcChNum(),
                                                     f_adc_ch_mode(setCfg->syncAdcChMode()),
                                                     setCfg->syncAdcChRangeNum(),
                                                     setCfg->syncAdcThreshold());
            }

            if (err_code == X502_ERR_OK) {
                err_code = X502_SetExtRefFreqValue(m_hnd, qMin(setCfg->refFreqExternalValue(), devspecInfo()->devspecTypeInfo().extRefFreqMax()));
                if (err_code != X502_ERR_OK) {
                    errDescr = tr("Setup external reference frequency value");
                }
            }

            if (err_code == X502_ERR_OK) {
                double adc_freq {setCfg->adc().adcFreq()};
                double frame_freq {setCfg->adc().adcChFreq()};
                err_code = X502_SetAdcFreq(m_hnd, &adc_freq, &frame_freq);
                if (err_code != X502_ERR_OK) {
                    errDescr = tr("Setup adc frequency");
                }


                if (err_code == X502_ERR_OK) {
                    double din_freq {setCfg->din().inDigFreq()};
                    err_code = X502_SetDinFreq(m_hnd, &din_freq);
                    if (err_code != X502_ERR_OK) {
                        errDescr = tr("Setup din frequency");
                    }

                    if (err_code == X502_ERR_OK) {
                        m_adcDinMultipler = frame_freq * setCfg->adc().adcEnabledChCnt() / din_freq;
                    }
                }
            }



            /* Если нет поддержек подтяжек, то не вызываем X502_SetDigInPullup, т.к.
             * эта функция в этих устройствах может использоваться для настройки линий синхронизации (E16) */
            if (err_code == X502_ERR_OK) {
                uint32_t pullups {0};
                if (devspecInfo()->devspecTypeInfo().supportedPullResistors() != 0) {
                    pullups = setCfg->pullResistors();
                }

                if (devspecInfo()->devspecTypeInfo().syncIoModeConfigSupport() != 0) {
                    const x502Config::SyncIoMode int_mode {setCfg->syncIoIntMode()};
                    const x502Config::SyncIoMode trig_mode {setCfg->syncIoTrigMode()};
                    if (int_mode != x502Config::SyncIoMode::In) {
                        pullups |= E16_MODE_INT_OUTPUT;
                        if (int_mode == x502Config::SyncIoMode::OutStartSig) {
                            pullups |= E16_MODE_INT_START_OUTPUT;
                        }
                    }
                    if (trig_mode != x502Config::SyncIoMode::In) {
                        pullups |= E16_MODE_TRIG_OUTPUT;
                        if (trig_mode == x502Config::SyncIoMode::OutStartSig) {
                            pullups |= E16_MODE_TRIG_START_OUTPUT;
                        }
                    }
                }

                if (devspecInfo()->devspecTypeInfo().extUSrcConfigSupport() != 0) {
                    if (setCfg->extUSrcRange() != 0) {
                        pullups |= E16_MODE_RELAY_ON;
                    }
                }



                int32_t err_code {X502_SetDigInPullup(m_hnd, pullups)};

                /* Изначально данная возможность не была реализована в прошивке
                   blackfin, не считаем этот случай ошибокой */
                if (err_code == X502_ERR_NOT_IMPLEMENTED)
                    err_code = X502_ERR_OK;
                if (err_code != X502_ERR_OK) {
                    errDescr = tr("Setup pullup resistors");
                }

            }

            if (err_code == X502_ERR_OK) {
                double out_freq {setCfg->out().outSyncGenFreq()};
                err_code = X502_SetOutFreq(m_hnd, &out_freq);
                if (err_code != X502_ERR_OK) {
                    errDescr = tr("Setup output frequency");
                }
            }

            if (err_code == X502_ERR_OK) {
                err_code = X502_Configure(m_hnd, 0);
            }



            if (err_code != X502_ERR_OK) {
                getError(err_code, errDescr, err);
            }
        } else {
            err = StdErrors::InvalidConfigType();
        }
    }

    void x502::protAdcEnable(LQError &err) {
        getError(X502_StreamsEnable(m_hnd, X502_STREAM_ADC), err);
        if (err.isSuccess()) {
            m_adcEnabled = true;
        }
    }

    void x502::protAdcDisable(LQError &err) {
        getError(X502_StreamsDisable(m_hnd, X502_STREAM_ADC), err);
        if (err.isSuccess()) {
            m_adcEnabled = false;
        }
    }

    void x502::protAdcStart(LQError &err) {
        QMutexLocker lock{&m_streamLock};
        startStream(m_adcRunning, err);
    }

    void x502::protAdcStop(LQError &err) {
        QMutexLocker lock{&m_streamLock};
        stopStream(m_adcRunning, err);
        m_lastAdcData.clear();
        m_lastAdcWrdsCnt = 0;
    }

    void x502::protAdcGetData(double *data, int size, unsigned flags,
                              unsigned tout, int &recvd_size, LQError &err) {
        QMutexLocker lock{&m_streamLock};
        /* при нулевом таймауте если данные уже есть в буфере, то просто их
         * сразу возвращаем, не пробуя еще принять */
        if ((m_lastAdcWrdsCnt < size) && !((tout == 0) && (m_lastAdcWrdsCnt > 0))) {
            QElapsedTimer tmr; tmr.start();
            do {
                int rx_size = size - m_lastAdcWrdsCnt;
                if (m_dinEnabled) {
                    rx_size += (rx_size / m_adcDinMultipler);
                }

                qint64 elapsed {tmr.elapsed()};
                rxNewData(rx_size, static_cast<unsigned>(elapsed > tout ? 0 : tout - elapsed), err);
            } while (err.isSuccess() && (m_lastAdcWrdsCnt < size) && (tmr.elapsed() < tout));
        }

        if (m_lastAdcWrdsCnt > 0) {
            const int frame_size {adcConfig().adcEnabledChCnt()};
            const int cpy_wrds {m_lastAdcWrdsCnt < size ? (m_lastAdcWrdsCnt/frame_size) * frame_size : size};
            memcpy(data, m_lastAdcData.data(), sizeof(*data) * cpy_wrds);

            m_lastAdcWrdsCnt -= cpy_wrds;
            if (m_lastAdcWrdsCnt > 0)  {
                memmove(m_lastAdcData.data(), &m_lastAdcData.data()[cpy_wrds], sizeof(*data) * m_lastAdcWrdsCnt);
            }

            recvd_size = cpy_wrds;
        } else {
            recvd_size = 0;
        }
    }

    void x502::protInAsyncDig(unsigned &val, LQError &err) {
        getError(X502_AsyncInDig(m_hnd, &val), err);
    }

    void x502::protInDigEnable(LQError &err) {
        getError(X502_StreamsEnable(m_hnd, X502_STREAM_DIN), err);
        if (err.isSuccess()) {
            m_dinEnabled = true;
        }
    }

    void x502::protInDigDisable(LQError &err) {
        getError(X502_StreamsDisable(m_hnd, X502_STREAM_DIN), err);
        if (err.isSuccess()) {
            m_dinEnabled = false;
        }
    }

    void x502::protInDigStart(LQError &err) {
        QMutexLocker lock{&m_streamLock};
        startStream(m_dinRunning, err);
    }

    void x502::protInDigStop(LQError &err) {
        QMutexLocker lock{&m_streamLock};
        stopStream(m_dinRunning, err);
        m_lastDinData.clear();
        m_lastDinWrdsCnt = 0;
    }

    void x502::protInDigGetData(quint32 *data, int size, unsigned tout, int &recvd_size, LQError &err)  {
        QMutexLocker lock{&m_streamLock};
        /* при нулевом таймауте если данные уже есть в буфере, то просто их
         * сразу возвращаем, не пробуя еще принять */
        if ((m_lastDinWrdsCnt < size) && !((tout == 0) && (m_lastDinWrdsCnt > 0))) {
            QElapsedTimer tmr; tmr.start();
            do {
                int rx_size = size - m_lastDinWrdsCnt;
                if (m_adcEnabled) {
                    rx_size += (rx_size * m_adcDinMultipler);
                }

                qint64 elapsed {tmr.elapsed()};
                rxNewData(rx_size, static_cast<unsigned>(elapsed > tout ? 0 : tout - elapsed), err);
            } while (err.isSuccess() && (m_lastDinWrdsCnt < size) && (tmr.elapsed() < tout));
        }

        if (m_lastDinWrdsCnt > 0) {
            const int cpy_wrds {qMin(m_lastDinWrdsCnt, size)};
            memcpy(data, m_lastDinData.data(), sizeof(*data) * cpy_wrds);

            m_lastDinWrdsCnt -= cpy_wrds;
            if (m_lastDinWrdsCnt > 0)  {
                memmove(m_lastDinData.data(), &m_lastDinData.data()[cpy_wrds], sizeof(*data) * m_lastDinWrdsCnt);
            }

            recvd_size = cpy_wrds;
        } else {
            recvd_size = 0;
        }
    }


    void x502::rxNewData(int rx_size, unsigned tout, LQError &err) {
        if (m_wrdsBuf.size() < rx_size) {
            m_wrdsBuf.resize(rx_size);
        }

        int32_t recv_res {X502_Recv(m_hnd, m_wrdsBuf.data(), rx_size, tout)};
        if (recv_res < 0) {
            getError(recv_res, err);
        } else {
            uint32_t adc_new_samples_cnt {m_adcEnabled ? static_cast<uint32_t>(recv_res) : 0};
            uint32_t din_new_samples_cnt {m_dinEnabled ? static_cast<uint32_t>(recv_res) : 0};

            if (m_adcEnabled && (m_lastAdcData.size() < (m_lastAdcWrdsCnt + static_cast<int32_t>(adc_new_samples_cnt)))) {
                m_lastAdcData.resize(m_lastAdcWrdsCnt + adc_new_samples_cnt);
            }
            if (m_dinEnabled && (m_lastDinData.size() < (m_lastDinWrdsCnt + static_cast<int32_t>(din_new_samples_cnt)))) {
                m_lastDinData.resize(m_lastDinWrdsCnt + din_new_samples_cnt);
            }

            recv_res = X502_ProcessData(m_hnd, m_wrdsBuf.data(), recv_res, X502_PROC_FLAGS_VOLT,
                                        &m_lastAdcData.data()[m_lastAdcWrdsCnt], &adc_new_samples_cnt,
                                        &m_lastDinData.data()[m_lastDinWrdsCnt], &din_new_samples_cnt);
            if (recv_res < 0) {
                getError(recv_res, err);
            } else {
                m_lastAdcWrdsCnt += adc_new_samples_cnt;
                m_lastDinWrdsCnt += din_new_samples_cnt;
            }
        }

    }

    void x502::setOutStreams(LQError &err) {
        int32_t err_code {X502_ERR_OK};
        uint32_t new_streams {0};
        if (devTypeInfo().out()->outDacChannelsCnt() >= X502_DAC_CH_CNT) {
            if (outSyncDacValidRamSignal(0))
                new_streams |= X502_STREAM_DAC1;
            if (outSyncDacValidRamSignal(1))
                new_streams |= X502_STREAM_DAC2;
        }
        bool outDig {false};
        for (int ch {0}; (ch < devTypeInfo().out()->outDigChannelsCnt()) && !outDig; ++ch) {
            if (outSyncDigValidRamSignal(ch))
                outDig = true;
        }
        if (outDig)
            new_streams |= X502_STREAM_DOUT;

        uint32_t cur_stream;
        /* Явно получаем список разрешенных потоков, а не используем из
           m_outStreams, чтобы избежать возможной раcсинхронизации в случае
           работы с BlackFin */
        err_code = X502_GetEnabledStreams(m_hnd, &cur_stream);
        if (err_code == X502_ERR_OK) {
            cur_stream &= X502_STREAM_ALL_OUT;

            const uint32_t streams_en  {new_streams & ~cur_stream};
            const uint32_t streams_dis {~new_streams & cur_stream};

            if (streams_en) {
                err_code = X502_StreamsEnable(m_hnd, streams_en);
            }
            if (streams_dis && (err_code == X502_ERR_OK)) {
                err_code = X502_StreamsDisable(m_hnd, streams_dis);
            }
        }

        if (err_code == X502_ERR_OK) {
            m_outStreams = new_streams;
        } else {
            getError(err_code, err);
        }
    }

    void x502::protOutAsyncDac(int ch, double val, LQError &err) {
        getError(X502_AsyncOutDac(m_hnd, static_cast<uint32_t>(ch), val,
                                  X502_DAC_FLAGS_VOLT | X502_DAC_FLAGS_CALIBR), err);
    }

    void x502::protOutAsyncDig(unsigned val, unsigned mask, LQError &err) {
        return getError(X502_AsyncOutDig(m_hnd, val, mask), err);
    }

    void x502::rawWordsSend(const uint32_t *wrds, int size, unsigned tout,
                            int &sent_size, LQError &err) {

        const int send_res {X502_Send(m_hnd, wrds, static_cast<uint32_t>(size), tout)};
        if (send_res >= 0) {
            sent_size = send_res;
        } else {
            sent_size = 0;
            getError(send_res, err);
        }
    }

    const DeviceInterface &x502::devrecIface(const t_x502_devrec &devrec) {
        return devrec.iface == X502_IFACE_PCI ? DeviceInterfaces::pcie() :
               devrec.iface == X502_IFACE_USB ? DeviceInterfaces::usb() :
               devrec.iface == X502_IFACE_ETH ? DeviceInterfaces::ethernet() :
                                                DeviceInterfaces::unknown();
    }

    void x502::privOutStreamInit(LQError &err) {
        return setOutStreams(err);
    }

    void x502::privOutStreamStartRequest(LQError &err) {
        QMutexLocker lock{&m_streamLock};
        startStream(m_outRunning, err);
    }

    void x502::privOutStreamStop(unsigned tout, LQError &err) {
        if (m_sender.unsentWordsCnt()) {
            m_sender.flushData(tout, err);
            m_sender.clearData();
        }

        QMutexLocker lock{&m_streamLock};
        stopStream(m_outRunning, err);
    }

    void x502::privOutSyncSendData(const double *dac_data, int dac_size,
                                    const unsigned *dig_data, int dig_size,
                                    unsigned flags, unsigned tout, LQError &err) {
        Q_UNUSED(flags)
        if (tout == 0) {
            tout = 3000 + static_cast<unsigned>((dac_size + dig_size)/1000);
        }

        if (m_sender.unsentWordsCnt()) {
            m_sender.flushData(tout, err);
        }

        if (err.isSuccess()) {
            if (m_sender.unsentWordsCnt() != 0) {
                err = StdErrors::SendBusy();
            } else {
                const int snd_dac_req_size {dac_size};
                const int snd_dig_req_size {dig_size};
                const int snd_size         {snd_dac_req_size  + snd_dig_req_size};
                QScopedArrayPointer<uint32_t> wrds (new uint32_t[static_cast<size_t>(snd_size)]);

                /* Если разрешено оба ЦАП, то необходимо переформатировать данные из одного
                 * массива в два, как принимает функция Prepare */
                if ((m_outStreams & X502_STREAM_DAC1) && (m_outStreams & X502_STREAM_DAC2)) {
                    dac_size/=2;
                    QScopedArrayPointer<double> dac1 (new double[static_cast<size_t>(dac_size)]);
                    QScopedArrayPointer<double> dac2 (new double[static_cast<size_t>(dac_size)]);
                    for (int i {0}; i < dac_size; ++i) {
                        dac1[i] = *dac_data++;
                        dac2[i] = *dac_data++;
                    }
                    getError(X502_PrepareData(m_hnd, dac1.data(), dac2.data(),
                                              reinterpret_cast<const uint32_t*>(dig_data),
                                              static_cast<uint32_t>(dac_size),
                                              X502_DAC_FLAGS_CALIBR  | X502_DAC_FLAGS_VOLT,
                                              wrds.data()), err);
                } else {
                    /* Если разрешен один ЦАП или только DOUT, то можем данные использовать
                     * напрямую. Т.к. по неразрешенному каналу ЦАП данные в любом случае
                     * не используются, то можем передать в массив в качестве обоих параметров,
                     * а использоваться будут только для того канала, что разрешен.
                     * Т.е. может быть случай что разрешен только канала ЦАП, так и
                     * случай, что разрешено только DOUT и любой из размеров (dac_size и dig_size)
                     * может быть 0, то определяем его как максимум из двух параметров */
                    getError(X502_PrepareData(m_hnd, dac_data, dac_data,
                                              reinterpret_cast<const uint32_t*>(dig_data),
                                              static_cast<uint32_t>(qMax(dig_size, dac_size)),
                                              X502_DAC_FLAGS_CALIBR  | X502_DAC_FLAGS_VOLT,
                                              wrds.data()), err);
                }

                if (!err.isSuccess()) {
                    LQMeasLog->error(tr("Prepare data error"), err, this);
                } else {
                    m_sender.setFrameSize(snd_size);
                    m_sender.putFrames(wrds.data(), snd_size, tout, nullptr, err);
                }
            }
        }
    }

    bool x502::privOutSyncHasDefferedData() const {
        return m_sender.unsentWordsCnt() > 0;
    }

    void x502::privOutSyncTryFlushData(unsigned tout, LQError &err) {
        m_sender.flushData(tout, err);
    }

    void x502::privOutSyncSendFinish(LQError &err) {
        Q_UNUSED(err)
        m_sender.setFrameSize(0);
    }

    bool x502::privOutSyncUnderflowOccured() const {
        bool ret {false};
        uint32_t status;
        int32_t res {X502_OutGetStatusFlags(m_hnd, &status)};
        if (res == X502_ERR_OK) {
            if (status & X502_OUT_STATUS_FLAG_BUF_WAS_EMPTY)
                ret = true;
        }
        return ret;
    }

    void x502::privOutCycleLoadStart(const OutRamSignalGenerator &generator, int size, LQError &err) {
        setOutStreams(err);
        if (err.isSuccess()) {
            int chCnt = generator.dacSignalsCount() + (generator.digSignalsCount() == 0 ? 0 : 1);
            getError(X502_OutCycleLoadStart(m_hnd, static_cast<uint32_t>(size*chCnt)), err);
        }
    }

    void x502::privOutCycleLoadFinish(LQError &err) {
        return getError(X502_OutCycleSetup(m_hnd, !m_outRunning ? X502_OUT_CYCLE_FLAGS_WAIT_DONE : 0), err);
    }

    void x502::privOutCycleGenStartRequest(LQError &err) {
        QMutexLocker lock{&m_streamLock};
        startStream(m_outRunning, err);
    }

    void x502::privOutCycleGenUpdateRequest(LQError &err) {
        privOutCycleGenStartRequest(err);
    }

    void x502::privOutCycleGenStopRequest(unsigned tout, LQError &err) {
        m_outStopTout = tout;
        m_outStopTmr.start();
        getError(X502_OutCycleStop(m_hnd, 0), err);
    }

    void x502::privOutCycleGenStop(LQError &err) {
        QMutexLocker lock{&m_streamLock};

        /* если был задан таймаут на остановку, то ждем завершения вывода
         * в течение этого таймаута */
        if (m_outStopTout) {
             uint32_t done {0};
            while (!done && err.isSuccess() && (m_outStopTmr.elapsed() < m_outStopTout)) {
                getError(X502_OutCycleCheckSetupDone(m_hnd, &done), err);
                if (!done && err.isSuccess()) {
                    QThread::msleep(1);
                }
            }
        }

        stopStream(m_outRunning, err);
    }
}

