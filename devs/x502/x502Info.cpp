#include "x502Info.h"
#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"


static const QLatin1String cfgkey_plda_ver {"PldaVer"};

namespace LQMeas {
    x502Info::x502Info(const x502TypeInfo &type, const QString &serial, unsigned fpga_ver, unsigned plda_ver, unsigned mcu_ver)
        : DeviceInfo{type, serial},
          m_fpga_ver{fpga_ver},
          m_plda_ver{plda_ver},
          m_mcu_ver{mcu_ver} {
    }

    QString x502Info::pldaVerStr() const {
        return QString::number(m_plda_ver);
    }

    QString x502Info::fpgaVerStr() const {
        return QString{"%0.%1"}
                .arg(QString::number((m_fpga_ver >> 8) & 0xFF),
                     QString::number(m_fpga_ver&0xFF));
    }

    QString x502Info::mcuVerStr() const {
        return QString{"%0.%1.%2"}
                .arg(QString::number((m_mcu_ver >> 24) & 0xFF),
                     QString::number((m_mcu_ver >> 16) & 0xFF),
                     QString::number((m_mcu_ver >>  8) & 0xFF));
    }

    x502Info::x502Info(const x502Info &info) : DeviceInfo{info},
        m_fpga_ver{info.m_fpga_ver},
        m_plda_ver{info.m_plda_ver},
        m_mcu_ver{info.m_mcu_ver} {
    }

    void x502Info::protSave(QJsonObject &infoObj) const     {
        infoObj[cfgkey_plda_ver] = static_cast<int>(m_plda_ver);
        infoObj[StdConfigKeys::fpgaVer()] = static_cast<int>(m_fpga_ver);
        if (mcuPresent()) {
            infoObj[StdConfigKeys::mcuVer()] = static_cast<int>(m_mcu_ver);
        }
    }

    void x502Info::protLoad(const QJsonObject &infoObj) {
        m_plda_ver = infoObj[cfgkey_plda_ver].toInt();
        m_fpga_ver = infoObj[StdConfigKeys::fpgaVer()].toInt();
        if (mcuPresent()) {
            m_mcu_ver = infoObj[StdConfigKeys::mcuVer()].toInt();
        }
    }
}

