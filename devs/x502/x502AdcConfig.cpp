#include "x502AdcConfig.h"
#include "x502Config.h"
#include "modules/E16/E16TypeInfo.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_ch_hw_avg   {"DevAvg"};

    static int32_t f_calc_adc_freq(double ref_freq, uint32_t lch_cnt, double *f_acq,
                       double *f_frame, uint32_t *adc_freq_div, uint32_t *adc_frame_delay, uint32_t adcfreq_div_max,
                       double adc_freq_max, double adc_freq_def)  {
        int32_t err = (f_acq==NULL) ? X502_ERR_INVALID_POINTER : X502_ERR_OK;
        if (err == X502_ERR_OK) {
            uint32_t cur_adc_freq_div, cur_frame_delay = 0;

            double set_freq = *f_acq;
            if (set_freq <= 0) {
                set_freq = adc_freq_def;
            } else if  (set_freq > adc_freq_max) {
                set_freq = adc_freq_max;
            }
            if (set_freq > ref_freq) {
                set_freq = ref_freq;
            }
            cur_adc_freq_div = (uint32_t)(ref_freq/set_freq+0.49);
            if (cur_adc_freq_div == 0)
                cur_adc_freq_div = 1;
            if (cur_adc_freq_div > adcfreq_div_max)
                cur_adc_freq_div = adcfreq_div_max;
            set_freq = ref_freq/cur_adc_freq_div;

            *f_acq = set_freq;

            if (f_frame==NULL) {
                cur_frame_delay = 0;
            } else {
                if (lch_cnt == 0)
                    lch_cnt = 1;

                if (*f_frame <= 0) {
                    cur_frame_delay = 0;
                } else {
                    int32_t frame_div = (int32_t)((ref_freq/(*f_frame)
                                                    - lch_cnt*ref_freq/set_freq)+0.49);

                    cur_frame_delay = frame_div <=0 ? 0 :
                                          frame_div > X502_ADC_INTERFRAME_DELAY_MAX ?
                                          X502_ADC_INTERFRAME_DELAY_MAX : frame_div;
                }
                *f_frame = 1./(lch_cnt/set_freq +
                                 cur_frame_delay/ref_freq);
            }

            if (adc_freq_div != NULL)
                *adc_freq_div = cur_adc_freq_div;
            if (adc_frame_delay != NULL)
                *adc_frame_delay = cur_frame_delay;
        }
        return err;
    }

    x502AdcConfig::x502AdcConfig(x502Config *devCfg, const DevAdcInfo &info) :
        DevAdcStdSeq32Config{info},
        m_devCfg{devCfg} {

        memset(&m_params, 0, sizeof(m_params));

        for (unsigned ch = 0; ch < adc_channels_cnt; ++ch) {
            m_params.Ch[ch].Avg = 1;
        }
    }

    x502AdcConfig::x502AdcConfig(x502Config *devCfg, const x502AdcConfig &adcCfg) :
        DevAdcStdSeq32Config{adcCfg},
        m_devCfg{devCfg} {

        memcpy(&m_params, &adcCfg.m_params, sizeof(m_params));
    }

    bool x502AdcConfig::adcExternalStart() const {
        return (m_devCfg->syncFreqMode() != x502Config::SyncMode::Internal) ||
                (m_devCfg->syncStartMode() != x502Config::SyncMode::Internal);
    }

    void x502AdcConfig::adcSetChAvg(int ch, int avg_cnt) {
        if (avg_cnt < 0)
            avg_cnt = 0;
        if (avg_cnt > adcChAvgMaxVal())
            avg_cnt = adcChAvgMaxVal();
        if (avg_cnt != m_params.Ch[ch].Avg) {
            m_params.Ch[ch].Avg = avg_cnt;
            notifyAdcFreqChanged();
            notifyConfigChanged();
        }
    }

    void x502AdcConfig::protUpdateInfo(const DeviceTypeInfo *info) {

    }

    int x502AdcConfig::adcChAvgMaxVal() const {
        return X502_LCH_AVG_SIZE_MAX;
    }

    void x502AdcConfig::adcChSave(int ch_num, QJsonObject &chObj) const {
        DevAdcStdSeq32Config::adcChSave(ch_num, chObj);
        if (m_devCfg->x502DevTypeInfo().adcChAvgSupport()) {
            chObj[cfgkey_ch_hw_avg] = m_params.Ch[ch_num].Avg;
        }
    }

    void x502AdcConfig::adcChLoad(int ch_num, const QJsonObject &chObj) {
        DevAdcStdSeq32Config::adcChLoad(ch_num, chObj);
        if (m_devCfg->x502DevTypeInfo().adcChAvgSupport()) {
            int avg {chObj[cfgkey_ch_hw_avg].toInt(1)};
            if (avg < 1)
                avg = 1;
            if (avg > adcChAvgMaxVal())
                avg = adcChAvgMaxVal();
            m_params.Ch[ch_num].Avg = avg;
        } else {
            m_params.Ch[ch_num].Avg = 1;
        }
    }

    void x502AdcConfig::adcAdjustFreq(double &adcFreq, double &adcChFreq) {
        const x502TypeInfo &x502Info {m_devCfg->x502DevTypeInfo()};
        const double ref_freq {m_devCfg->refFreq()};

        int max_avg {1};
        if (x502Info.adcChAvgSupport()) {
            for (int ch {0}; ch < adc_channels_cnt; ++ch) {
                if (adcChEnabled(ch)) {
                    int avg = m_params.Ch[ch].Avg;
                    if (avg > max_avg)
                        max_avg = avg;
                }
            }
        }

        if (adcFreq > ref_freq/max_avg)
            adcFreq = ref_freq/max_avg;


        f_calc_adc_freq(ref_freq, static_cast<uint32_t>(adcEnabledChCnt()),
                        &adcFreq, &adcChFreq, nullptr, nullptr,
                        x502Info.adcFreqDivMax(), x502Info.adcFreqMax(), x502Info.adcFreqDefault());
    }
}
