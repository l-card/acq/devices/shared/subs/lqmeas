#ifndef LQMEAS_X502INDIGCONFIG_H
#define LQMEAS_X502INDIGCONFIG_H

#include "lqmeas/ifaces/in/dig/DevInDigSyncBaseConfig.h"


namespace LQMeas {
    class x502Config;

    class x502InDigConfig : public DevInDigSyncBaseConfig {
    public:
        static const int din_max_channels_cnt {18};

        bool inDigSyncExternalStart() const override;
        int inDigSampleSize() const override {return 4;}
    protected:
        void dinAdjustFreq(double &dinFreq) override;
    private:
        friend class x502Config;
        explicit x502InDigConfig(x502Config *devCfg, const DevInDigInfo &info);
        explicit x502InDigConfig(x502Config *devCfg, const x502InDigConfig &inCfg);

        x502Config *m_devCfg;
    };
}

#endif // LQMEAS_X502INDIGCONFIG_H
