#ifndef LQMEAS_X502NATIVEERROR_H
#define LQMEAS_X502NATIVEERROR_H

#include "LQError.h"

namespace LQMeas {
    class x502NativeError {
    public:
        static LQError error(int code, const QString &msg);
    };
};


#endif // LQMEAS_X502NATIVEERROR_H
