#ifndef LQMEAS_X502CONFIG_H
#define LQMEAS_X502CONFIG_H

#include "lqmeas/devs/DeviceConfig.h"
#include "lqmeas/EnumNamedValue.h"
#include "x502AdcConfig.h"
#include "x502OutConfig.h"
#include "x502InDigConfig.h"
#include "x502api.h"



namespace LQMeas {
    class x502TypeInfo;

    class x502Config : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();


        enum class SyncMode {
            Internal,
            ExternalMaster,
            SYN1Rise,
            SYN1Fall,
            SYN2Rise,
            SYN2Fall,
            TRIGRise,
            TRIGFall,
            INTRise,
            INTFall,
            AdcChRise,
            AdcChFall,
            AdcChHigh,
            AdcChLow,
        };

        enum class RefFreq {
            Freq_2000KHz  = 2000000,
            Freq_1500KHz  = 1500000,
            Freq_48MHz = 48000000
        };

        enum class PullResistor {
            NoResistors     = 0,
            DI_H_Up         = X502_PULLUPS_DI_H,
            DI_L_Up         = X502_PULLUPS_DI_L,
            DI_SYN1_Up      = X502_PULLUPS_DI_SYN1,
            DI_SYN2_Up      = X502_PULLUPS_DI_SYN2,
            CONV_IN_Down    = X502_PULLDOWN_CONV_IN,
            START_IN_Down   = X502_PULLDOWN_START_IN,
        };
        Q_DECLARE_FLAGS(PullResistors, PullResistor);

        enum class SyncIoMode {
            In = 0,
            OutRefFreq,
            OutStartSig,
        };





        const DevAdcConfig *adcConfig() const override {
            return &m_adc;
        }
        const DevOutConfig *outConfig() const override {
            return &m_out;
        }
        const DevOutSyncConfig *outSyncConfig() const override {
            return &m_out;
        }
        const DevInDigConfig *inDigConfig() const override {
            return &m_din;
        }

        x502AdcConfig &adc() {return  m_adc;}
        const x502AdcConfig &adc() const {return  m_adc;}
        x502OutConfig &out() {return  m_out;}
        const x502OutConfig &out() const {return  m_out;}
        x502InDigConfig &din() {return m_din;}
        const x502InDigConfig &din() const {return m_din;}


        QString configDevTypeName() const override {return typeConfigName();}
        DeviceConfig *clone() const override {
            return new x502Config{*this};
        }

        x502Config();
        x502Config(const x502TypeInfo &info);
        x502Config(const x502Config &cfg);

        const x502TypeInfo &x502DevTypeInfo() const {return *m_devTypeInfo;}

        double externalRefFreqMax() const {return m_ext_ref_freq_max;}

        SyncMode syncFreqMode() const {return params.sync.freqMode;}
        SyncMode syncStartMode() const {return params.sync.startMode;}
        RefFreq  refFreqInternal() const {return params.sync.refFreqInternal;}
        double   refFreqInternalValue() const {return typeInternalRefFreqVal(params.sync.refFreqInternal);}
        double   refFreqExternalValue() const {return params.sync.refFreqExternal;}

        bool isRefFreqInternal() const {return params.sync.freqMode == SyncMode::Internal;}


        /* текущее значение опорной частоты (на основе настроек
         * внутренней или внешней опорной частоты в зависимости от freqMode) */
        double refFreq() const;

        int syncAdcChNum() const {return params.sync.adc.ch.num;}
        int syncAdcChRangeNum() const {return params.sync.adc.ch.range;}
        DevAdcStdSeq32Config::AdcChMode syncAdcChMode() const {return params.sync.adc.ch.mode;}
        double syncAdcThreshold() const {return params.sync.adc.threshold;}

        PullResistors pullResistors() const {return params.pullResistors;}

        SyncIoMode syncIoIntMode() const {return params.sync.io.int_mode;}
        SyncIoMode syncIoTrigMode() const {return params.sync.io.trig_mode;}

        int extUSrcRange() const {return params.ext_usrc_range;}

        bool bfIsEnabled() const {return params.blackfin.enabled;}

        QString bfFirmwareFilename() const { return params.blackfin.fileName;}

        static double typeInternalRefFreqVal(RefFreq freq) {
            return static_cast<double>(freq);
        }
    public Q_SLOTS:
        void setSyncFreqMode(SyncMode mode);
        void setSyncStartMode(SyncMode mode);
        void setRefFreqInternal(RefFreq val);
        void setRefFreqExternal(double val);

        void setSyncAdcChNum(int ch_num);
        void setSyncAdcChRangeNum(int range);
        void setSyncAdcChMode(DevAdcStdSeq32Config::AdcChMode mode);
        void setSyncAdcThreshold(double lvl);

        void setSyncIoIntMode(SyncIoMode mode);
        void setSyncIoTrigMode(SyncIoMode mode);

        void setExtUSrcRange(int num);

        void setPullResistors(LQMeas::x502Config::PullResistors pullups_flags);

        void setBfEnalbed(bool en);
        void setBfFirmwareFilename(const QString &filename);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
        void protUpdateInfo(const DeviceTypeInfo *info) override;
        void protUpdate() override;

    private:
        x502AdcConfig m_adc;
        x502OutConfig m_out;
        x502InDigConfig m_din;
        const x502TypeInfo *m_devTypeInfo;

        double m_ext_ref_freq_max {1500000};
        bool m_ref_freq_is_configurable {false};

        struct {
            struct {
                SyncMode freqMode;
                SyncMode startMode;
                RefFreq  refFreqInternal;
                double   refFreqExternal;

                struct  {
                    struct {
                        int num;
                        int range;
                        DevAdcStdSeq32Config::AdcChMode mode;
                    } ch;
                    double threshold;
                } adc;
                struct {
                    SyncIoMode int_mode;
                    SyncIoMode trig_mode;
                } io;
            } sync;

            int ext_usrc_range;
            PullResistors pullResistors;

            struct {
                bool enabled;
                QString fileName;
            } blackfin;
        } params;

        struct {
            double dinFreq;
        } results;
    };
}

Q_DECLARE_METATYPE(LQMeas::x502Config::SyncMode);
Q_DECLARE_METATYPE(LQMeas::x502Config::RefFreq);
Q_DECLARE_OPERATORS_FOR_FLAGS(LQMeas::x502Config::PullResistors);

#endif // LQMEAS_X502CONFIG_H

