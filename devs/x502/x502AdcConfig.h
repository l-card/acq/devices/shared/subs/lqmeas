#ifndef LQMEAS_X502ADCCONFIG_H
#define LQMEAS_X502ADCCONFIG_H

#include "lqmeas/ifaces/in/adc/DevAdcStdSeq32Config.h"

namespace LQMeas {
    class x502Config;

    class x502AdcConfig : public DevAdcStdSeq32Config  {
        Q_OBJECT
    public:
        enum class AdcChRange {
            U_10  = 0,
            U_5   = 1,
            U_2   = 2,
            U_1   = 3,
            U_0_5 = 4,
            U_0_2 = 5,
        };


        bool adcExternalStart() const override;

        AdcChRange adcChRange(int ch_num) const {return static_cast<AdcChRange>(adcChRangeNum(ch_num));}
        int adcChAvg(int ch_num) const {return m_params.Ch[ch_num].Avg;}
        int adcChAvgMaxVal() const;

        int adcSampleSize() const override {return 4;}

        static int typeAdcRangeNum(AdcChRange range) {return static_cast<int>(range);}
    public Q_SLOTS:
        void adcSetChRange(int ch_num, AdcChRange range) {
            adcSetChRangeNum(ch_num, typeAdcRangeNum(range));
        }
        void adcSetChAvg(int ch_num, int avg_cnt);
    protected:
        void protUpdateInfo(const DeviceTypeInfo *info) override;
        void adcAdjustFreq(double &adcFreq, double &adcChFreq) override;

        void adcChSave(int ch_num, QJsonObject &chObj) const override;
        void adcChLoad(int ch_num, const QJsonObject &chObj) override;
    private:
        explicit x502AdcConfig(x502Config *devCfg, const DevAdcInfo &info);
        explicit x502AdcConfig(x502Config *devCfg, const x502AdcConfig &adcCfg);

        struct {
            struct {
                int Avg;
            } Ch[adc_channels_cnt];
        } m_params;


        x502Config *m_devCfg;

        friend class x502Config;
    };


}

#endif // LQMEAS_X502ADCCONFIG_H
