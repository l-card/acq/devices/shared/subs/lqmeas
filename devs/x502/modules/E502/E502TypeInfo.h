#ifndef LQMEAS_E502TYPEINFO_H
#define LQMEAS_E502TYPEINFO_H

#include "lqmeas/devs/x502/x502TypeInfo.h"

namespace LQMeas {
    class E502TypeInfo : public x502TypeInfo {
    public:
        static const QString &name();
        static const QString &apiName();

        static const E502TypeInfo &defaultTypeInfo();

        static const E502TypeInfo &typeInfoP1EUD2I();
        static const E502TypeInfo &typeInfoP1EUXI();

        static const E502TypeInfo &typeInfoPEUD();
        static const E502TypeInfo &typeInfoXUD();
        static const E502TypeInfo &typeInfoXEUX();
        static const E502TypeInfo &typeInfoXUX();

        static const E502TypeInfo &typeInfoPEUDI();
        static const E502TypeInfo &typeInfoXUXI();
        static const E502TypeInfo &typeInfoXEUXI();
        static const E502TypeInfo &typeInfoXUDI();

        static const QList<const E502TypeInfo *> &types();

        static QList<const E502TypeInfo *> ethTypes();

        bool mcuAvailable() const override {return true;}
        bool pldaAvailable() const override {return true;}
        bool ethOptionApplicable() const override {return true;}
        bool bfOptionApplicable() const override {return true;}
        bool galOptionApplicable() const override {return true;}


        x502Config::PullResistors supportedPullResistors() const override;
        int inDigChannelsCnt() const override;
        QString inDigChName(int ch_num) const override;

        QString deviceTypeName() const override {return name();}
        QString deviceTypeApiName() const override {return apiName();}
        QString deviceModificationName() const override {return m_mod_string;}
        const DeviceTypeSeries &deviceTypeSeries() const override;

        bool isDynamicAllocated() const override {return m_dynamic_alloc;}
        const DeviceTypeInfo *clone() const override;

        QList<const DeviceTypeInfo *> modificationList() const override;
        QList<const DeviceInterface *> supportedConInterfaces() const override;
    private:
        E502TypeInfo(unsigned devflags, bool dynamic_alloc);

        QString m_mod_string;

        unsigned m_devflags;
        bool m_dynamic_alloc;

        friend class E502;
    };
}


#endif // LQMEAS_E502TYPEINFO_H
