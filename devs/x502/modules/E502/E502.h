#ifndef LQMEAS_E502_H
#define LQMEAS_E502_H

#include "lqmeas/devs/x502/net/x502NetDevice.h"


namespace LQMeas {
    class x502Resolver;


    class E502 : public x502NetDevice {
        Q_OBJECT
    public:
        ~E502() override;

        int outCycleGenMaxSize(const OutRamSignalGenerator &generator) const override;

    protected:
        const x502Info *createInfo(const t_x502_info &info) const override;
    private:
        explicit E502(const t_x502_devrec &devrec);
        explicit E502(const x502DeviceRefIPAddr &ipRef);
        explicit E502(const QSharedPointer<const x502NetSvcRecord> &svcRec, int conTout);
        explicit E502(const QSharedPointer<const x502NetSvcRecord> &svcRec, const x502DeviceRefNetSvc &netRef);

        friend class x502Resolver;
    };
}
#endif // E502_H
