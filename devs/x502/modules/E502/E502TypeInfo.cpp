#include "E502TypeInfo.h"
#include "x502api.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/devs/DeviceTypeStdSeries.h"
#include <QStringBuilder>

namespace LQMeas {
    const QString &E502TypeInfo::name() {
        static const QString str {QStringLiteral("E-502")};
        return str;
    }

    const QString &E502TypeInfo::apiName() {
        static const QString str {QStringLiteral("E502")};
        return str;
    }

    E502TypeInfo::E502TypeInfo(unsigned devflags, bool dynamic_alloc) :
        x502TypeInfo{devflags},
        m_devflags{devflags},
        m_dynamic_alloc{dynamic_alloc} {

        if (gdPresent()) {
            m_mod_string = name() % QString{"-P1-%1-%2-A1"}
                            .arg(ethPresent() ? QLatin1String("EU") : QLatin1String("U"))
                            .arg(dacPresent() ? QLatin1String("D2")  : QLatin1String("X"));
        } else {
            m_mod_string = name() % QString{"-%1-%2-%3"}
                    .arg(bfPresent()  ? QLatin1String("P")  : QLatin1String("X"))
                    .arg(ethPresent() ? QLatin1String("EU") : QLatin1String("U"))
                    .arg(dacPresent() ? QLatin1String("D")  : QLatin1String("X"));
        }
        if (industrial())
            m_mod_string += QLatin1String("-I");
    }
    
    
    
    const E502TypeInfo &E502TypeInfo::defaultTypeInfo() {
        return typeInfoPEUD();
    }

    const E502TypeInfo &E502TypeInfo::typeInfoP1EUD2I() {
        static const E502TypeInfo info{X502_DEVFLAGS_GD_PRESENT |
                                           X502_DEVFLAGS_IFACE_SUPPORT_USB | X502_DEVFLAGS_GAL_PRESENT |
                                           X502_DEVFLAGS_INDUSTRIAL |
                                           X502_DEVFLAGS_BF_PRESENT |
                                           X502_DEVFLAGS_IFACE_SUPPORT_ETH |
                                           X502_DEVFLAGS_DAC_PRESENT, false};
        return info;
    }

    const E502TypeInfo &E502TypeInfo::typeInfoP1EUXI() {
        static const E502TypeInfo info{X502_DEVFLAGS_GD_PRESENT |
                                           X502_DEVFLAGS_IFACE_SUPPORT_USB | X502_DEVFLAGS_GAL_PRESENT |
                                           X502_DEVFLAGS_INDUSTRIAL |
                                           X502_DEVFLAGS_BF_PRESENT |
                                           X502_DEVFLAGS_IFACE_SUPPORT_ETH, false};
        return info;
    }

    const E502TypeInfo &E502TypeInfo::typeInfoPEUDI() {
        static const E502TypeInfo info{X502_DEVFLAGS_IFACE_SUPPORT_USB | X502_DEVFLAGS_GAL_PRESENT |
                                       X502_DEVFLAGS_INDUSTRIAL |
                                       X502_DEVFLAGS_BF_PRESENT |
                                       X502_DEVFLAGS_IFACE_SUPPORT_ETH |
                                       X502_DEVFLAGS_DAC_PRESENT, false};
        return info;
    }

    const E502TypeInfo &E502TypeInfo::typeInfoXUXI() {
        static const E502TypeInfo info{X502_DEVFLAGS_IFACE_SUPPORT_USB | X502_DEVFLAGS_GAL_PRESENT |
                                       X502_DEVFLAGS_INDUSTRIAL, false};
        return info;
    }

    const E502TypeInfo &E502TypeInfo::typeInfoXEUXI() {
        static const E502TypeInfo info{X502_DEVFLAGS_IFACE_SUPPORT_USB | X502_DEVFLAGS_GAL_PRESENT |
                                       X502_DEVFLAGS_INDUSTRIAL |
                                       X502_DEVFLAGS_IFACE_SUPPORT_ETH, false};
        return info;
    }

    const E502TypeInfo &E502TypeInfo::typeInfoXUDI() {
        static const E502TypeInfo info(X502_DEVFLAGS_IFACE_SUPPORT_USB | X502_DEVFLAGS_GAL_PRESENT |
                                       X502_DEVFLAGS_INDUSTRIAL |
                                       X502_DEVFLAGS_DAC_PRESENT, false);
        return info;
    }

    const E502TypeInfo &E502TypeInfo::typeInfoPEUD() {
        static const E502TypeInfo info{X502_DEVFLAGS_IFACE_SUPPORT_USB | X502_DEVFLAGS_GAL_PRESENT |
                                       X502_DEVFLAGS_BF_PRESENT |
                                       X502_DEVFLAGS_IFACE_SUPPORT_ETH |
                                       X502_DEVFLAGS_DAC_PRESENT, false};
        return info;
    }

    const E502TypeInfo &E502TypeInfo::typeInfoXUD() {
        static const E502TypeInfo info{X502_DEVFLAGS_IFACE_SUPPORT_USB | X502_DEVFLAGS_GAL_PRESENT |
                                       X502_DEVFLAGS_DAC_PRESENT, false};
        return info;
    }

    const E502TypeInfo &E502TypeInfo::typeInfoXEUX() {
        static const E502TypeInfo info{X502_DEVFLAGS_IFACE_SUPPORT_USB | X502_DEVFLAGS_GAL_PRESENT |
                                       X502_DEVFLAGS_IFACE_SUPPORT_ETH, false};
        return info;
    }

    const E502TypeInfo &E502TypeInfo::typeInfoXUX() {
        static const E502TypeInfo info{X502_DEVFLAGS_IFACE_SUPPORT_USB | X502_DEVFLAGS_GAL_PRESENT, false};
        return info;
    }

    const QList<const E502TypeInfo *> &E502TypeInfo::types() {
        static const QList<const E502TypeInfo *> list {
            &typeInfoP1EUD2I(),
            &typeInfoP1EUXI(),
            &typeInfoPEUDI(),
            &typeInfoPEUD(),            
            &typeInfoXEUX(),
            &typeInfoXUD(),
            &typeInfoXUX()
        };
        return list;
    }

    QList<const E502TypeInfo *> E502TypeInfo::ethTypes() {
        QList<const E502TypeInfo *> ret;
        const QList<const E502TypeInfo *> &allTypes {types()};
        for (const E502TypeInfo *type : allTypes) {
            if (type->ethPresent())
                ret.append(type);
        }
        return  ret;
    }

    x502Config::PullResistors E502TypeInfo::supportedPullResistors() const {
        return x502Config::PullResistor::DI_SYN1_Up |
               x502Config::PullResistor::DI_SYN2_Up |
               x502Config::PullResistor::CONV_IN_Down |
                x502Config::PullResistor::START_IN_Down;
    }

    int E502TypeInfo::inDigChannelsCnt() const {
        return 17;
    }

    QString E502TypeInfo::inDigChName(int ch_num) const {
        return ch_num == 13 ? QStringLiteral("DI14/DI_SYN2") :
               ch_num == 14 ? QStringLiteral("DI15/CONV_IN") :
               ch_num == 15 ? QStringLiteral("DI16/START_IN") :
               ch_num == 16 ? QStringLiteral("DI_SYN1") :
                              x502TypeInfo::inDigChName(ch_num);

    }

    const DeviceTypeSeries &E502TypeInfo::deviceTypeSeries() const {
        return DeviceTypeStdSeries::emodules();
    }

    const DeviceTypeInfo *E502TypeInfo::clone() const {
        return m_dynamic_alloc ? new E502TypeInfo{m_devflags, true} : DeviceTypeInfo::clone();
    }

    QList<const DeviceTypeInfo *> E502TypeInfo::modificationList() const  {
        QList<const DeviceTypeInfo *> ret;
        const QList<const E502TypeInfo*> &e502_types {types()};
        for (const E502TypeInfo* type : e502_types) {
            ret.append(type);
        }
        return ret;
    }

    QList<const DeviceInterface *> E502TypeInfo::supportedConInterfaces() const {
        static const QList<const DeviceInterface *> usbeth {
            &DeviceInterfaces::usb(),
            &DeviceInterfaces::ethernet()
        };
        static const QList<const DeviceInterface *> usb {
            &DeviceInterfaces::usb()
        };
        return ethPresent() ? usbeth : usb;
    }
}
