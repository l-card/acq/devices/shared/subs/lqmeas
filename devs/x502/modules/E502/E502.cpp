#include "E502.h"
#include "E502Info.h"
#include "lqmeas/lqtdefs.h"



namespace LQMeas {

    E502::~E502() {

    }


    int E502::outCycleGenMaxSize(const OutRamSignalGenerator &generator) const {
        int chCnt {generator.dacSignalsCount() + (generator.digSignalsCount() == 0 ? 0 : 1)};
        return  chCnt == 0 ? 0 : static_cast<int>(3.*1024*1024/chCnt);
    }


    const x502Info *E502::createInfo(const t_x502_info &info) const {
        return new E502Info{
            E502TypeInfo{info.devflags, true},
            QSTRING_FROM_CSTR(info.serial),
            info.fpga_ver,
            info.plda_ver,
            info.mcu_firmware_ver
        };
    }

    /* На старых версиях gcc (в Debian 10 и ниже) использование {} для вызова конструктора
     * x502 приводит по какой-то причине к ошибке сборке */
    E502::E502(const t_x502_devrec &devrec) :
        x502NetDevice(devrec, E502Info{E502TypeInfo{devrec.flags, true}, QSTRING_FROM_CSTR(devrec.serial)}) {

    }

    E502::E502(const x502DeviceRefIPAddr &ipRef) : x502NetDevice{ipRef, E502Info{}} {

    }

    E502::E502(const QSharedPointer<const x502NetSvcRecord> &svcRec, int conTout) :
        x502NetDevice(svcRec, conTout, E502Info{}) {

    }

    E502::E502(const QSharedPointer<const x502NetSvcRecord> &svcRec, const x502DeviceRefNetSvc &netRef)  :
        x502NetDevice{svcRec, netRef} {

    }
}
