#include "E16.h"
#include "E16Info.h"
#include "lqmeas/lqtdefs.h"

namespace LQMeas {

    E16::~E16() {

    }

    int E16::outCycleGenMaxSize(const OutRamSignalGenerator &generator) const {
        return -1;
    }



    const x502Info *E16::createInfo(const t_x502_info &info) const {
        return new E16Info{
            E16TypeInfo::defaultTypeInfo(),
            QSTRING_FROM_CSTR(info.serial),
            info.fpga_ver,
            info.mcu_firmware_ver
        };
    }

    E16::E16(const t_x502_devrec &devrec) :
        x502NetDevice(devrec, E16Info{E16TypeInfo::defaultTypeInfo(), QSTRING_FROM_CSTR(devrec.serial)}) {

    }


    E16::E16(const x502DeviceRefIPAddr &ipRef) : x502NetDevice{ipRef, E16Info{}} {

    }

    E16::E16(const QSharedPointer<const x502NetSvcRecord> &svcRec, int conTout) :  x502NetDevice{svcRec, conTout, E16Info{}} {

    }

    E16::E16(const QSharedPointer<const x502NetSvcRecord> &svcRec, const x502DeviceRefNetSvc &netRef) :  x502NetDevice{svcRec, netRef}{

    }
}
