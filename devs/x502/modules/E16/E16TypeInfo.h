#ifndef LQMEAS_E16TYPEINFO_H
#define LQMEAS_E16TYPEINFO_H

#include "lqmeas/devs/x502/x502TypeInfo.h"

namespace LQMeas {
    class E16TypeInfo : public x502TypeInfo {
    public:
        static const QString &name();

        static const E16TypeInfo &defaultTypeInfo();

        static const QList<const E16TypeInfo *> &types();


        double adcFreqMax() const override;
        double adcFreqDefault() const override;
        bool adcIsChFreqConfigurable() const override {return true;}

        int adcChRangesCnt(int ch_num, int unit_mode_num = 0) const override;
        double adcChRangeMaxVal(int ch_num, int range, int unit_mode_num = 0) const override;
        double adcChRangeMinVal(int ch_num, int range, int unit_mode_num = 0) const override;


        bool adcChAvgSupport() const override {return false;}
        bool syncAnalogModeSupport() const override {return true;}
        unsigned adcFreqDivMax() const override;

        double inDigSyncFreqDefault() const override;

        double outSyncGenFreqMax() const override;
        double outSyncGenFreqDefault() const override;
        double outDacChRangeMaxVal(int ch, int range) const override;
        double outDacChRangeMinVal(int ch, int range) const override;
        bool outSyncGenCycleModeSupport() const override {return false;}
        unsigned int outSyncGenFreqDivMax() const override;



        bool syncIoModeConfigSupport() const override {return true;}
        bool extUSrcConfigSupport() const override {return true;}

        int extUSrcRangesCnt() const override;
        double extUSrcRangeMaxVal(int range) const override;
        double extUSrcRangeMinVal(int range) const override;

        bool mcuAvailable() const override {return true;}
        bool pldaAvailable() const override {return false;}
        bool ethOptionApplicable() const override {return true;}
        bool bfOptionApplicable() const override {return false;}
        bool galOptionApplicable() const override {return false;}

        int inDigChannelsCnt() const override;
        QString inDigChName(int ch_num) const override;

        QList<x502Config::RefFreq> supportedIntRefFreqs() const override {return {x502Config::RefFreq::Freq_48MHz};}
        x502Config::PullResistors supportedPullResistors() const override {return x502Config::PullResistor::NoResistors;}

        QList<x502Config::SyncMode> supportedSyncFreqModes() const override {return {
                                                                                     x502Config::SyncMode::Internal,
                                                                                     x502Config::SyncMode::TRIGRise ,
                                                                                     x502Config::SyncMode::TRIGFall,
                                                                                     x502Config::SyncMode::INTRise ,
                                                                                     x502Config::SyncMode::INTFall};}
        QList<x502Config::SyncMode> supportedSyncStartModes() const override {return {
                                                                                      x502Config::SyncMode::Internal,
                                                                                      x502Config::SyncMode::TRIGRise ,
                                                                                      x502Config::SyncMode::TRIGFall,
                                                                                      x502Config::SyncMode::INTRise ,
                                                                                      x502Config::SyncMode::INTFall,
                                                                                      x502Config::SyncMode::AdcChRise,
                                                                                      x502Config::SyncMode::AdcChFall,
                                                                                      x502Config::SyncMode::AdcChHigh,
                                                                                      x502Config::SyncMode::AdcChLow};}

        QString deviceTypeName() const override {return name();}
        const DeviceTypeSeries &deviceTypeSeries() const override;

        QList<const DeviceInterface *> supportedConInterfaces() const override;


        QList<const DeviceTypeInfo *> modificationList() const override;
    private:
        E16TypeInfo();

        friend class E16;
    };
}

#endif // LQMEAS_E16TYPEINFO_H
