#include "E16TypeInfo.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/devs/DeviceTypeStdSeries.h"

static double f_ranges[] = {10.,2.5, 0.625, 0.156};
static struct {
    double min;
    double max;
} f_usrc_ranges[] = {{0, 3.3}, {-15, 15}};

namespace LQMeas {
    const QString &E16TypeInfo::name() {
        static const QString str {QStringLiteral("E16")};
        return str;
    }

    const E16TypeInfo &E16TypeInfo::defaultTypeInfo() {
        static const E16TypeInfo t;
        return t;
    }

    const QList<const E16TypeInfo *> &E16TypeInfo::types() {
        static const QList<const E16TypeInfo *> list {&defaultTypeInfo()};
        return list;
    }

    double E16TypeInfo::adcFreqMax() const  {
        return 1000000;
    }

    double E16TypeInfo::adcFreqDefault() const {
        return 500000;
    }

    double E16TypeInfo::outSyncGenFreqMax() const {
        return 1000000;
    }

    double E16TypeInfo::outSyncGenFreqDefault() const  {
        return 500000;
    }

    double E16TypeInfo::outDacChRangeMaxVal(int ch, int range) const {
        Q_UNUSED(ch) Q_UNUSED(range)
        return E16_DAC_RANGE;
    }

    double E16TypeInfo::outDacChRangeMinVal(int ch, int range) const {
        Q_UNUSED(ch) Q_UNUSED(range)
        return -E16_DAC_RANGE;
    }

    unsigned int E16TypeInfo::outSyncGenFreqDivMax() const {
        return 0x800;
    }

    int E16TypeInfo::extUSrcRangesCnt() const {
        return sizeof(f_usrc_ranges)/sizeof(f_usrc_ranges[0]);
    }

    double E16TypeInfo::extUSrcRangeMaxVal(int range) const  {
        return f_usrc_ranges[range].max;
    }

    double E16TypeInfo::extUSrcRangeMinVal(int range) const {
        return f_usrc_ranges[range].min;
    }

    int E16TypeInfo::adcChRangesCnt(int ch_num, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return sizeof(f_ranges)/sizeof(f_ranges[0]);
    }

    double E16TypeInfo::adcChRangeMaxVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return f_ranges[range];
    }

    double E16TypeInfo::adcChRangeMinVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return -f_ranges[range];
    }

    unsigned E16TypeInfo::adcFreqDivMax() const {
        return 16000000;
    }

    double E16TypeInfo::inDigSyncFreqDefault() const {
        return 500000;
    }



    int E16TypeInfo::inDigChannelsCnt() const {
        return 18;
    }

    QString E16TypeInfo::inDigChName(int ch_num) const {
        return ch_num == 16 ? QStringLiteral("INT") :
               ch_num == 17 ? QStringLiteral("TRIG") :
               x502TypeInfo::inDigChName(ch_num);
    }

    const DeviceTypeSeries &E16TypeInfo::deviceTypeSeries() const {
        return DeviceTypeStdSeries::emodules();
    }

    QList<const DeviceInterface *> E16TypeInfo::supportedConInterfaces() const {
        static const QList<const DeviceInterface *> ifaces {
            &DeviceInterfaces::usb(),
            &DeviceInterfaces::ethernet()
        };
        return ifaces;
    }


    QList<const DeviceTypeInfo *> E16TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list { &defaultTypeInfo() };
        return list;
    }

    E16TypeInfo::E16TypeInfo() : x502TypeInfo{
            X502_DEVFLAGS_IFACE_SUPPORT_USB |
            X502_DEVFLAGS_IFACE_SUPPORT_ETH |
            X502_DEVFLAGS_INDUSTRIAL |
            X502_DEVFLAGS_DAC_PRESENT} {

    }
}
