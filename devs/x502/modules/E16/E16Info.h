#ifndef LQMEAS_E16INFO_H
#define LQMEAS_E16INFO_H

#include "lqmeas/devs/x502/x502Info.h"
#include "E16TypeInfo.h"

namespace LQMeas {
    class E16Info : public x502Info {
    public:
        E16Info(const E16TypeInfo &type = E16TypeInfo::defaultTypeInfo(),
                 const QString &serial = QString{},
                 unsigned fpga_ver = 0, unsigned mcu_ver = 0) :
            x502Info{type, serial, fpga_ver, 0, mcu_ver} {}
        E16Info(const E16Info &info) : x502Info{info} {}

        DeviceInfo *clone() const override {return new E16Info{*this};}
    };
}

#endif // LQMEAS_E16INFO_H
