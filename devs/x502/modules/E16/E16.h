#ifndef LQMEAS_E16_H
#define LQMEAS_E16_H

#include "lqmeas/devs/x502/net/x502NetDevice.h"

namespace LQMeas {
    class x502Resolver;

    class E16 : public x502NetDevice {
        Q_OBJECT
    public:
        ~E16() override;

        int outCycleGenMaxSize(const OutRamSignalGenerator &generator) const override;

    protected:
        const x502Info *createInfo(const t_x502_info &info) const override;
    private:
        explicit E16(const t_x502_devrec &devrec);
        explicit E16(const x502DeviceRefIPAddr &ipRef);
        explicit E16(const QSharedPointer<const x502NetSvcRecord> &svcRec, int conTout);
        explicit E16(const QSharedPointer<const x502NetSvcRecord> &svcRec, const x502DeviceRefNetSvc &netRef);

        friend class x502Resolver;
    };
}

#endif // LQMEAS_E16_H
