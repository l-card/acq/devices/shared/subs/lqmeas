#ifndef LQMEAS_L502TYPEINFO_H
#define LQMEAS_L502TYPEINFO_H

#include "lqmeas/devs/x502/x502TypeInfo.h"

namespace LQMeas {
    class L502TypeInfo : public x502TypeInfo {
    public:
        static const QString &name();
        static const QString &apiName();

        static const L502TypeInfo &defaultTypeInfo();

        static const L502TypeInfo &typeInfoPGD();
        static const L502TypeInfo &typeInfoXGD();
        static const L502TypeInfo &typeInfoXG();
        static const L502TypeInfo &typeInfoPGDI();
        static const L502TypeInfo &typeInfoXGDI();
        static const L502TypeInfo &typeInfoXGI();
        /* obsoleted */
        static const L502TypeInfo &typeInfoPG();
        static const L502TypeInfo &typeInfoXXD();
        static const L502TypeInfo &typeInfoXX();

        static const QList<const L502TypeInfo *> &types();

        bool mcuAvailable() const override {return false;}
        bool pldaAvailable() const override {return true;}
        bool ethOptionApplicable() const override {return false;}
        bool bfOptionApplicable() const override {return true;}
        bool galOptionApplicable() const override {return true;}

        x502Config::PullResistors supportedPullResistors() const override;
        int inDigChannelsCnt() const override;
        QString inDigChName(int ch_num) const override;

        QString deviceTypeName() const override {return name();}
        QString deviceTypeApiName() const override {return apiName();}
        QString deviceModificationName() const override {return m_mod_string;}
        const DeviceTypeSeries &deviceTypeSeries() const override;

        bool isDynamicAllocated() const override {return m_dynamic_alloc;}
        const DeviceTypeInfo *clone() const override;

        QList<const DeviceTypeInfo *> modificationList() const override;
        QList<const DeviceInterface *> supportedConInterfaces() const override;
    private:
        explicit L502TypeInfo(unsigned devflags, bool dynamic_alloc);

        unsigned m_devflags;
        bool m_dynamic_alloc;

        QString m_mod_string;
        friend class L502;
    };
}

#endif // LQMEAS_L502TYPEINFO_H
