#include "L502TypeInfo.h"
#include "x502api.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/devs/DeviceTypeStdSeries.h"
#include <QStringBuilder>


namespace LQMeas {
    const QString &L502TypeInfo::name() {
        static const QString str {QStringLiteral("L-502")};
        return str;
    }

    const QString &L502TypeInfo::apiName() {
        static const QString str {QStringLiteral("L502")};
        return str;
    }

    L502TypeInfo::L502TypeInfo(unsigned devflags, bool dynamic_alloc) :
        x502TypeInfo{devflags},
        m_devflags{devflags},
        m_dynamic_alloc{dynamic_alloc} {

        m_mod_string = name() % QString{"-%1-%2"}
                .arg(bfPresent()  ? QLatin1String("P") : QLatin1String("X"))
                .arg(galPresent() ? QLatin1String("G") : QLatin1String("X"));
        if (dacPresent())
            m_mod_string += QLatin1String("-D");
        if (industrial())
            m_mod_string += QLatin1String("-I");
    }
    
    
    
    const L502TypeInfo &L502TypeInfo::defaultTypeInfo() {
        return typeInfoPGD();
    }

    const L502TypeInfo &L502TypeInfo::typeInfoPGD() {
        static const L502TypeInfo info{X502_DEVFLAGS_IFACE_SUPPORT_PCI |
                    X502_DEVFLAGS_BF_PRESENT | X502_DEVFLAGS_GAL_PRESENT |
                    X502_DEVFLAGS_DAC_PRESENT, false};
        return info;
    }

    const L502TypeInfo &L502TypeInfo::typeInfoPG() {
        static const L502TypeInfo info{X502_DEVFLAGS_IFACE_SUPPORT_PCI |
                                        X502_DEVFLAGS_BF_PRESENT | X502_DEVFLAGS_GAL_PRESENT, false};
        return info;
    }

    const L502TypeInfo &L502TypeInfo::typeInfoXGD() {
        static const L502TypeInfo info{X502_DEVFLAGS_IFACE_SUPPORT_PCI |
                                       X502_DEVFLAGS_GAL_PRESENT |
                                       X502_DEVFLAGS_DAC_PRESENT, false};
        return info;
    }

    const L502TypeInfo &L502TypeInfo::typeInfoXG() {
        static const L502TypeInfo info{X502_DEVFLAGS_IFACE_SUPPORT_PCI |
                                       X502_DEVFLAGS_GAL_PRESENT, false};
        return info;
    }

    const L502TypeInfo &L502TypeInfo::typeInfoPGDI()     {
        static const L502TypeInfo info{X502_DEVFLAGS_IFACE_SUPPORT_PCI |
                                       X502_DEVFLAGS_INDUSTRIAL |
                                       X502_DEVFLAGS_BF_PRESENT | X502_DEVFLAGS_GAL_PRESENT |
                                       X502_DEVFLAGS_DAC_PRESENT, false};
        return info;
    }

    const L502TypeInfo &L502TypeInfo::typeInfoXGDI()     {
        static const L502TypeInfo info(X502_DEVFLAGS_IFACE_SUPPORT_PCI |
                                       X502_DEVFLAGS_INDUSTRIAL |
                                       X502_DEVFLAGS_GAL_PRESENT |
                                       X502_DEVFLAGS_DAC_PRESENT, false);
        return info;
    }

    const L502TypeInfo &L502TypeInfo::typeInfoXGI() {
        static const L502TypeInfo info{X502_DEVFLAGS_IFACE_SUPPORT_PCI |
                                       X502_DEVFLAGS_INDUSTRIAL |
                                       X502_DEVFLAGS_GAL_PRESENT, false};
        return info;
    }

    const L502TypeInfo &L502TypeInfo::typeInfoXXD() {
        static const L502TypeInfo info{X502_DEVFLAGS_IFACE_SUPPORT_PCI |
                                       X502_DEVFLAGS_DAC_PRESENT, false};
        return info;
    }

    const L502TypeInfo &L502TypeInfo::typeInfoXX() {
        static const L502TypeInfo info{X502_DEVFLAGS_IFACE_SUPPORT_PCI, false};
        return info;
    }

    const QList<const L502TypeInfo *> &L502TypeInfo::types() {
        static const QList<const L502TypeInfo *> list {
            &typeInfoPGD(),
            &typeInfoXGD(),
            &typeInfoXG(),
            &typeInfoPGDI(),
            &typeInfoXGDI(),
            &typeInfoXGI(),
            &typeInfoPG(),
            &typeInfoXXD(),
            &typeInfoXX(),
        };
        return list;
    }

    x502Config::PullResistors L502TypeInfo::supportedPullResistors() const {
        return x502Config::PullResistor::DI_H_Up |
               x502Config::PullResistor::DI_L_Up |
               x502Config::PullResistor::DI_SYN1_Up |
                x502Config::PullResistor::DI_SYN2_Up;
    }

    int L502TypeInfo::inDigChannelsCnt() const {
        return 18;
    }

    QString L502TypeInfo::inDigChName(int ch_num) const {
        return ch_num == 16 ? QStringLiteral("DI_SYN1") :
               ch_num == 17 ? QStringLiteral("DI_SYN2") :
                              x502TypeInfo::inDigChName(ch_num);
    }

    const DeviceTypeSeries &L502TypeInfo::deviceTypeSeries() const {
        return DeviceTypeStdSeries::lboards();
    }

    const DeviceTypeInfo *L502TypeInfo::clone() const {
        return m_dynamic_alloc ? new L502TypeInfo{m_devflags, true} : DeviceTypeInfo::clone();
    }

    QList<const DeviceTypeInfo *> L502TypeInfo::modificationList() const {
        QList<const DeviceTypeInfo *> ret;
        const QList<const L502TypeInfo*> &l502_types {types()};
        for (const L502TypeInfo* type : l502_types) {
            ret.append(type);
        }
        return ret;
    }

    QList<const DeviceInterface *> L502TypeInfo::supportedConInterfaces() const {
        static const QList<const DeviceInterface *> ifaces {
            &DeviceInterfaces::pcie()
        };
        return ifaces;
    }

}
