#include "x502InDigConfig.h"
#include "x502Config.h"
#include "x502TypeInfo.h"
#include "lqmeas/ifaces/in/dig/DevInDigInfo.h"
#include <QJsonObject>
#include <QJsonArray>

namespace LQMeas {

    static int32_t f_calc_din_freq(double ref_freq, double *f_din,
                                   uint32_t *freq_div, uint32_t freq_div_max,
                               double freq_max, double freq_default)  {
        int32_t err = (f_din==NULL) ? X502_ERR_INVALID_POINTER : X502_ERR_OK;
        if (err == X502_ERR_OK) {
            uint32_t cur_freq_div = 0;

            double set_freq = *f_din;
            if (set_freq <= 0) {
                set_freq = freq_default;
            } else if (set_freq > freq_max) {
                set_freq = freq_max;
            }
            if (set_freq > ref_freq) {
                set_freq = ref_freq;
            }
            cur_freq_div = (uint32_t)(ref_freq/set_freq+0.49);
            if (cur_freq_div == 0)
                cur_freq_div = 1;
            if (cur_freq_div > freq_div_max)
                cur_freq_div = freq_div_max;
            set_freq = ref_freq/cur_freq_div;

            *f_din = set_freq;


            if (freq_div != NULL)
                *freq_div = cur_freq_div;
        }
        return err;
    }


    bool x502InDigConfig::inDigSyncExternalStart() const {
        return m_devCfg->adc().adcExternalStart();
    }

    void x502InDigConfig::dinAdjustFreq(double &dinFreq) {
        const x502TypeInfo &x502Info {m_devCfg->x502DevTypeInfo()};
        const double ref_freq {m_devCfg->refFreq()};

        f_calc_din_freq(ref_freq, &dinFreq, nullptr,
                        x502Info.inDigFreqDivMax(), x502Info.inDigSyncFreqMax(),
                        x502Info.inDigSyncFreqDefault());
    }


    x502InDigConfig::x502InDigConfig(x502Config *devCfg, const DevInDigInfo &info) :
        DevInDigSyncBaseConfig{info},
        m_devCfg{devCfg} {
    }

    x502InDigConfig::x502InDigConfig(x502Config *devCfg, const x502InDigConfig &inCfg) :
        DevInDigSyncBaseConfig{inCfg},
        m_devCfg{devCfg} {

    }
}
