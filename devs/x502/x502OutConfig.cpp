#include "x502OutConfig.h"
#include "x502Config.h"
#include "x502TypeInfo.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>
#include <QJsonArray>

namespace LQMeas {
    static const QLatin1String cfgkey_dev_obj           {StdConfigKeys::dev()};
    static const QLatin1String cfgkey_dac_obj           {StdConfigKeys::dac()};
    static const QLatin1String cfgkey_dac_chs_array     {StdConfigKeys::channels()};
    static const QLatin1String cfgkey_dac_sync_mode     {StdConfigKeys::syncMode()};
    static const QLatin1String cfgkey_dout_obj          {StdConfigKeys::dout()};
    static const QLatin1String cfgkey_dout_sync_mode    {StdConfigKeys::syncMode()};
    static const QLatin1String cfgkey_out_freq          {StdConfigKeys::outFreq()};

    static int32_t f_calc_out_freq(double ref_freq, double *f_out,
                                   uint32_t *freq_div, uint32_t freq_div_max,
                                   double freq_max, double freq_default)  {
        int32_t err = (f_out==NULL) ? X502_ERR_INVALID_POINTER : X502_ERR_OK;
        if (err == X502_ERR_OK) {
            uint32_t cur_freq_div = 0;

            double set_freq = *f_out;
            if (set_freq <= 0) {
                set_freq = freq_default;
            } else if (set_freq > freq_max) {
                set_freq = freq_max;
            }
            if (set_freq > ref_freq) {
                set_freq = ref_freq;
            }
            cur_freq_div = (uint32_t)(ref_freq/set_freq+0.49);
            if (cur_freq_div == 0)
                cur_freq_div = 1;
            if (cur_freq_div > freq_div_max)
                cur_freq_div = freq_div_max;
            set_freq = ref_freq/cur_freq_div;

            *f_out = set_freq;


            if (freq_div != NULL)
                *freq_div = cur_freq_div;
        }
        return err;
    }

    bool x502OutConfig::outDacChSyncModeEnabled(int ch) const {
        return m_params.Dac.Ch[ch].syncMode;
    }

    bool x502OutConfig::outDigChSyncModeEnabled(int ch) const     {
        Q_UNUSED(ch)
        return outDigSyncModeEnabled();
    }

    bool x502OutConfig::outDigSyncModeEnabled() const {
        return m_params.DOut.syncMode;
    }

    void x502OutConfig::outSyncSetGenFreq(double val) {
        if (!LQREAL_IS_EQUAL(m_params.OutFreq, val)) {
            m_params.OutFreq = val;
            notifyDacChFreqChanged(-1);
            notifyDigChFreqChanged(-1);
            notifyConfigChanged();
        }
    }

    void x502OutConfig::outDacChSetSyncModeEnabled(int ch, bool en) {
        if (m_params.Dac.Ch[ch].syncMode != en) {
            m_params.Dac.Ch[ch].syncMode = en;
            notifyDacChSyncEnableStateChanged(ch);
            notifyConfigChanged();
        }
    }

    void x502OutConfig::outDigSetSyncModeEnabled(bool en) {
        if (m_params.DOut.syncMode != en) {
            m_params.DOut.syncMode = en;
            notifyDigChSyncEnableStateChanged(-1);
            notifyConfigChanged();
        }
    }

    void x502OutConfig::protLoad(const QJsonObject &outObj) {
        DevOutSyncConfig::protLoad(outObj);

        const QJsonObject &devOutObj {outObj[cfgkey_dev_obj].toObject()};

        const QJsonObject &dacExObj {devOutObj[cfgkey_dac_obj].toObject()};
        const QJsonArray &dacChArray {dacExObj[cfgkey_dac_chs_array].toArray()};
        for (int ch {0}; ch < dac_max_channels_cnt; ++ch) {
            bool syncMode {false};
            if (ch < dacChArray.size()) {
                const QJsonObject &chObj {dacChArray.at(ch).toObject()};
                syncMode = chObj[cfgkey_dac_sync_mode].toBool();
            }
            m_params.Dac.Ch[ch].syncMode = syncMode;
        }

        const QJsonObject &doutObj {devOutObj[cfgkey_dout_obj].toObject()};
        m_params.DOut.syncMode = doutObj[cfgkey_dout_sync_mode].toBool();
        
        const double max_out_freq {x502TypeInfo::defaultTypeInfo().outSyncGenFreqMax()};
        double out_freq     {devOutObj[cfgkey_out_freq].toDouble(max_out_freq)};
        if ((out_freq <= 0) || (out_freq > max_out_freq))
            out_freq = max_out_freq;
        m_params.OutFreq = out_freq;
    }

    void x502OutConfig::protSave(QJsonObject &outObj) const {
        DevOutSyncConfig::protSave(outObj);

        QJsonObject devOutObj;
        QJsonObject dacExObj;
        QJsonArray dacChArray;
        for (int ch {0}; ch < dac_max_channels_cnt; ++ch) {
            QJsonObject chObj;
            chObj[cfgkey_dac_sync_mode] = m_params.Dac.Ch[ch].syncMode;
            dacChArray.append(chObj);
        }
        dacExObj[cfgkey_dac_chs_array] = dacChArray;
        devOutObj[cfgkey_dac_obj] = dacExObj;

        QJsonObject doutObj;
        doutObj[cfgkey_dout_sync_mode] = m_params.DOut.syncMode;
        devOutObj[cfgkey_dout_obj] = doutObj;

        devOutObj[cfgkey_out_freq] = m_params.OutFreq;

        outObj[cfgkey_dev_obj] = devOutObj;
    }

    void x502OutConfig::protUpdate() {
        DevOutSyncConfig::protUpdate();

        const x502TypeInfo &x502Info {m_devCfg->x502DevTypeInfo()};
        const double ref_freq {m_devCfg->refFreq()};

        m_results.OutFreq = m_params.OutFreq;

        f_calc_out_freq(ref_freq, &m_results.OutFreq, nullptr,
                        x502Info.outSyncGenFreqDivMax(), x502Info.outSyncGenFreqMax(),
                        x502Info.outSyncGenFreqDefault());

        if (!x502Info.outSyncGenCycleModeSupport() && (outSyncRamMode() == OutSyncRamMode::Cycle)) {
            outSyncSetRamMode(OutSyncRamMode::Stream);
        }
    }


    x502OutConfig::x502OutConfig(x502Config *devCfg, const DevOutInfo &info) :
        DevOutSyncConfig{info}, m_devCfg{devCfg} {

        memset(&m_params, 0, sizeof(m_params));
        m_params.OutFreq = m_results.OutFreq = info.outSyncGenFreqDefault();
    }

    x502OutConfig::x502OutConfig(x502Config *devCfg, const x502OutConfig &outCfg) :
        DevOutSyncConfig{outCfg}, m_devCfg{devCfg} {

        memcpy(&m_params, &outCfg.m_params, sizeof(m_params));
        memcpy(&m_results, &outCfg.m_results, sizeof(m_results));
    }
}

