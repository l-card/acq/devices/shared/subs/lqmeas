#include "x502Config.h"
#include "x502TypeInfo.h"
#include "x502.h"
#include "lqmeas/EnumConfigKey.h"
#include "lqmeas/StdConfigKeys.h"
#include "lqmeas/lqtdefs.h"
#include <QJsonObject>
#include <QJsonArray>

namespace LQMeas {
    static const QLatin1String cfgkey_sync_freq_mode         {"SyncFreqMode"};
    static const QLatin1String cfgkey_sync_start_mode        {StdConfigKeys::syncStartMode()};
    static const QLatin1String cfgkey_int_ref_freq           {"IntRefFreq"};
    static const QLatin1String cfgkey_ext_ref_freq           {"ExtRefFreq"};
    static const QLatin1String cfgkey_pullups                {StdConfigKeys::pullups()};
    static const QLatin1String cfgkey_bf_obj                 {"Blackfin"};
    static const QLatin1String cfgkey_bf_enabled             {StdConfigKeys::enabled()};
    static const QLatin1String cfgkey_bf_fw_filename         {"FirmwareFile"};

    static const QLatin1String cfgkey_sync_ana_obj           {"SyncAna"};
    static const QLatin1String cfgkey_sync_ana_ch_obj        {StdConfigKeys::channel()};
    static const QLatin1String cfgkey_sync_ana_ch_num        {StdConfigKeys::number()};
    static const QLatin1String cfgkey_sync_ana_ch_range      {StdConfigKeys::rangeNum()};
    static const QLatin1String cfgkey_sync_ana_ch_mode       {StdConfigKeys::mode()};
    static const QLatin1String cfgkey_sync_ana_threshold     {StdConfigKeys::threshold()};

    static const QLatin1String cfgkey_sync_io_obj            {"SyncIo"};
    static const QLatin1String cfgkey_sync_io_int_mode       {"IntMode"};
    static const QLatin1String cfgkey_sync_io_trig_mode      {"TrigMode"};

    static const QLatin1String cfgkey_usrc_obj               {"ExtUSrc"};
    static const QLatin1String cfgkey_usrc_range_num         {StdConfigKeys::rangeNum()};

    typedef EnumConfigKey<x502Config::SyncMode> X502SyncModeKey;
    static const X502SyncModeKey sync_modes[] = {
        {QStringLiteral("int"),         x502Config::SyncMode::Internal},
        {QStringLiteral("extmstr"),     x502Config::SyncMode::ExternalMaster},
        {QStringLiteral("syn1rise"),    x502Config::SyncMode::SYN1Rise},
        {QStringLiteral("syn1fall"),    x502Config::SyncMode::SYN1Fall},
        {QStringLiteral("syn2rise"),    x502Config::SyncMode::SYN2Rise},
        {QStringLiteral("syn2fall"),    x502Config::SyncMode::SYN2Fall},
        {QStringLiteral("trig_rise"),    x502Config::SyncMode::TRIGRise},
        {QStringLiteral("trig_fall"),    x502Config::SyncMode::TRIGFall},
        {QStringLiteral("int_rise"),     x502Config::SyncMode::INTRise},
        {QStringLiteral("int_fall"),    x502Config::SyncMode::INTFall},
        {QStringLiteral("adcch_rise"),     x502Config::SyncMode::AdcChRise},
        {QStringLiteral("adcch_fall"),    x502Config::SyncMode::AdcChFall},
        {QStringLiteral("adcch_high"),     x502Config::SyncMode::AdcChHigh},
        {QStringLiteral("adcch_low"),    x502Config::SyncMode::AdcChLow},
        {X502SyncModeKey::defaultKey(), x502Config::SyncMode::Internal},
    };
    typedef EnumConfigKey<x502Config::RefFreq> X502IntRefFreqKey;
    static const X502IntRefFreqKey ref_freqs[] = {
        {QStringLiteral("2000K"),       x502Config::RefFreq::Freq_2000KHz},
        {QStringLiteral("1500K"),       x502Config::RefFreq::Freq_1500KHz},
        {QStringLiteral("48M"),         x502Config::RefFreq::Freq_48MHz},
        {X502IntRefFreqKey::defaultKey(), x502Config::RefFreq::Freq_2000KHz},
    };

    typedef EnumConfigKey<DevAdcStdSeq32Config::AdcChMode> SyncAdcChModeKey;
    static const SyncAdcChModeKey sync_adc_ch_modes[] {
                                             {QStringLiteral("comm"),    DevAdcStdSeq32Config::AdcChMode::Comm},
                                             {QStringLiteral("diff"),    DevAdcStdSeq32Config::AdcChMode::Diff},
                                             {SyncAdcChModeKey::defaultKey(),DevAdcStdSeq32Config::AdcChMode::Comm},
                                             };

    typedef EnumConfigKey<x502Config::SyncIoMode> SyncIoModeKey;
    static const SyncIoModeKey sync_io_modes[] {
                                      {QStringLiteral("in"),      x502Config::SyncIoMode::In},
                                      {QStringLiteral("outfreq"), x502Config::SyncIoMode::OutRefFreq},
                                      {QStringLiteral("outstart"),x502Config::SyncIoMode::OutStartSig},
                                      {SyncIoModeKey::defaultKey(),x502Config::SyncIoMode::In}
                                      };


    const QString &x502Config::typeConfigName() {
        static const QString str {QStringLiteral("x502")};
        return str;
    }

    x502Config::x502Config() : x502Config{x502TypeInfo::defaultTypeInfo()} {

    }

    x502Config::x502Config(const x502TypeInfo &info) :
        m_devTypeInfo{&info},
        m_adc{this, info},
        m_out{this, info},
        m_din{this, info} {

        memset(&params.sync, 0, sizeof(params.sync));
        params.pullResistors = PullResistor::NoResistors;
        params.blackfin.enabled = false;
        params.ext_usrc_range = 0;


        results.dinFreq = adc().adcInfo().adcFreqMax();
        params.sync.freqMode = params.sync.startMode = SyncMode::Internal;
        params.sync.refFreqInternal = RefFreq::Freq_2000KHz;


        init();

    }

    x502Config::x502Config(const x502Config &cfg) :
        m_devTypeInfo{cfg.m_devTypeInfo},
        m_adc{this, cfg.adc()},
        m_out{this, cfg.out()},
        m_din{this, cfg.din()},
        m_ext_ref_freq_max{cfg.m_ext_ref_freq_max},
        m_ref_freq_is_configurable{cfg.m_ref_freq_is_configurable} {

        memcpy(&params.sync, &cfg.params.sync, sizeof(params.sync));
        memcpy(&results, &cfg.results, sizeof(results));
        params.pullResistors = cfg.params.pullResistors;
        params.blackfin.enabled = cfg.params.blackfin.enabled;
        params.blackfin.fileName = cfg.params.blackfin.fileName;
        params.ext_usrc_range = cfg.params.ext_usrc_range;

        init();
    }


    void x502Config::setRefFreqExternal(double val) {
        if (val > externalRefFreqMax())
            val = externalRefFreqMax();
        if (!LQREAL_IS_EQUAL(val, params.sync.refFreqExternal)) {
            params.sync.refFreqExternal = val;
            notifyConfigChanged();
        }
    }

    void x502Config::setSyncAdcChNum(int ch_num) {
        if (ch_num != params.sync.adc.ch.num) {
            params.sync.adc.ch.num = ch_num;
            notifyConfigChanged();
        }
    }

    void x502Config::setSyncAdcChRangeNum(int range) {
        if (range != params.sync.adc.ch.range) {
            params.sync.adc.ch.range = range;
            notifyConfigChanged();
        }
    }

    void x502Config::setSyncAdcChMode(DevAdcStdSeq32Config::AdcChMode mode) {
        if (mode != params.sync.adc.ch.mode) {
            params.sync.adc.ch.mode = mode;
            notifyConfigChanged();
        }
    }

    void x502Config::setSyncAdcThreshold(double lvl) {
        if (!LQREAL_IS_EQUAL(lvl, params.sync.adc.threshold)) {
            params.sync.adc.threshold = lvl;
            notifyConfigChanged();
        }
    }

    void x502Config::setSyncIoIntMode(SyncIoMode mode)  {
        if (mode != params.sync.io.int_mode) {
            params.sync.io.int_mode = mode;
            notifyConfigChanged();
        }
    }

    void x502Config::setSyncIoTrigMode(SyncIoMode mode) {
        if (mode != params.sync.io.trig_mode) {
            params.sync.io.trig_mode = mode;
            notifyConfigChanged();
        }
    }

    void x502Config::setExtUSrcRange(int num) {
        if (num != params.ext_usrc_range) {
            params.ext_usrc_range = num;
            notifyConfigChanged();
        }
    }

    void x502Config::setBfFirmwareFilename(const QString &filename) {
        if (params.blackfin.fileName != filename) {
            params.blackfin.fileName = filename;
            notifyConfigChanged();
        }
    }

    double x502Config::refFreq() const {
        return params.sync.freqMode == SyncMode::Internal ?
                    typeInternalRefFreqVal(params.sync.refFreqInternal) : params.sync.refFreqExternal;
    }


    void x502Config::setPullResistors(PullResistors pullups_flags) {
        if (params.pullResistors != pullups_flags) {
            params.pullResistors = pullups_flags;
            notifyConfigChanged();
        }
    }

    void x502Config::setBfEnalbed(bool en) {
        if (params.blackfin.enabled != en) {
            params.blackfin.enabled = en;
            notifyConfigChanged();
        }
    }

    void x502Config::setSyncFreqMode(x502Config::SyncMode mode) {
        if (params.sync.freqMode != mode) {
            params.sync.freqMode = mode;
            notifyConfigChanged();
        }
    }

    void x502Config::setSyncStartMode(x502Config::SyncMode mode) {
        if (params.sync.startMode != mode) {
            params.sync.startMode = mode;
            notifyConfigChanged();
        }
    }

    void x502Config::setRefFreqInternal(x502Config::RefFreq val) {
        if ((params.sync.refFreqInternal != val) && m_ref_freq_is_configurable) {
            params.sync.refFreqInternal = val;
            notifyConfigChanged();
        }
    }

    void x502Config::protSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_sync_freq_mode]  = X502SyncModeKey::getKey(sync_modes, params.sync.freqMode);
        cfgObj[cfgkey_sync_start_mode] = X502SyncModeKey::getKey(sync_modes, params.sync.startMode);
        if (m_ref_freq_is_configurable) {
            cfgObj[cfgkey_int_ref_freq]    = X502IntRefFreqKey::getKey(ref_freqs, params.sync.refFreqInternal);
        }
        cfgObj[cfgkey_ext_ref_freq]    = params.sync.refFreqExternal;

        cfgObj[cfgkey_pullups] =  static_cast<int>(params.pullResistors);

        if (m_devTypeInfo->syncAnalogModeSupport()) {
            QJsonObject syncAnaObj;
            QJsonObject syncAnaChObj;
            syncAnaChObj[cfgkey_sync_ana_ch_num] = params.sync.adc.ch.num;
            syncAnaChObj[cfgkey_sync_ana_ch_range] = params.sync.adc.ch.range;
            syncAnaChObj[cfgkey_sync_ana_ch_mode] = SyncAdcChModeKey::getKey(sync_adc_ch_modes, params.sync.adc.ch.mode);
            syncAnaObj[cfgkey_sync_ana_ch_obj] = syncAnaChObj;
            syncAnaObj[cfgkey_sync_ana_threshold] = params.sync.adc.threshold;
            cfgObj[cfgkey_sync_ana_obj] = syncAnaObj;
        }

        if (m_devTypeInfo->syncIoModeConfigSupport()) {
            QJsonObject syncIoObj;
            syncIoObj[cfgkey_sync_io_int_mode] = SyncIoModeKey::getKey(sync_io_modes, params.sync.io.int_mode);
            syncIoObj[cfgkey_sync_io_trig_mode] = SyncIoModeKey::getKey(sync_io_modes, params.sync.io.trig_mode);
            cfgObj[cfgkey_sync_io_obj] = syncIoObj;
        }

        if (m_devTypeInfo->extUSrcConfigSupport()) {
            QJsonObject extUSrcObj;
            extUSrcObj[cfgkey_usrc_range_num] = params.ext_usrc_range;
            cfgObj[cfgkey_usrc_obj] = extUSrcObj;
        }

        if (m_devTypeInfo->bfOptionApplicable()) {
            QJsonObject bfObj;
            bfObj[cfgkey_bf_enabled]   = params.blackfin.enabled;
            bfObj[cfgkey_bf_fw_filename] = params.blackfin.fileName;
            cfgObj[cfgkey_bf_obj] = bfObj;
        }
    }

    void x502Config::protLoad(const QJsonObject &cfgObj) {
        params.sync.freqMode = X502SyncModeKey::getValue(sync_modes, cfgObj[cfgkey_sync_freq_mode].toString());
        params.sync.startMode = X502SyncModeKey::getValue(sync_modes, cfgObj[cfgkey_sync_start_mode].toString());
        if (m_ref_freq_is_configurable) {
            params.sync.refFreqInternal = X502IntRefFreqKey::getValue(ref_freqs, cfgObj[cfgkey_int_ref_freq].toString());
        }

        double ext_ref_freq {cfgObj[cfgkey_ext_ref_freq].toDouble(externalRefFreqMax())};
        if ((ext_ref_freq <= 0) || (ext_ref_freq > externalRefFreqMax()))
            ext_ref_freq = externalRefFreqMax();
        params.sync.refFreqExternal = ext_ref_freq;
        params.pullResistors = static_cast<PullResistor>(cfgObj[cfgkey_pullups].toInt());


        if (m_devTypeInfo->syncAnalogModeSupport()) {
            const QJsonObject &syncAnaObj {cfgObj[cfgkey_sync_ana_obj].toObject()};
            const QJsonObject &syncAnaChObj {syncAnaObj[cfgkey_sync_ana_ch_obj].toObject()};
            params.sync.adc.ch.num = syncAnaChObj[cfgkey_sync_ana_ch_num].toInt();
            params.sync.adc.ch.range = syncAnaChObj[cfgkey_sync_ana_ch_range].toInt();

            params.sync.adc.ch.mode = SyncAdcChModeKey::getValue(
                sync_adc_ch_modes, syncAnaChObj[cfgkey_sync_ana_ch_mode].toString());

            params.sync.adc.threshold = syncAnaObj[cfgkey_sync_ana_threshold].toDouble();
        }

        if (m_devTypeInfo->syncIoModeConfigSupport()) {
            const QJsonObject &syncIoObj {cfgObj[cfgkey_sync_io_obj].toObject()};
            params.sync.io.int_mode = SyncIoModeKey::getValue(
                sync_io_modes, syncIoObj[cfgkey_sync_io_int_mode].toString());
            params.sync.io.trig_mode = SyncIoModeKey::getValue(
                sync_io_modes, syncIoObj[cfgkey_sync_io_trig_mode].toString());
        }

        if (m_devTypeInfo->extUSrcConfigSupport()) {
            const QJsonObject &extUSrcObj {cfgObj[cfgkey_usrc_obj].toObject()};
            params.ext_usrc_range = extUSrcObj[cfgkey_usrc_range_num].toInt();
        }

        if (m_devTypeInfo->bfOptionApplicable()) {
            const QJsonObject &bfObj {cfgObj[cfgkey_bf_obj].toObject()};
            params.blackfin.enabled = bfObj[cfgkey_bf_enabled].toBool(false);
            params.blackfin.fileName = bfObj[cfgkey_bf_fw_filename].toString();
        }
    }

    void x502Config::protUpdateInfo(const DeviceTypeInfo *info)  {
        m_devTypeInfo = static_cast<const x502TypeInfo *>(info);
        bool intRefFreqValid {false};
        if (m_devTypeInfo->supportedIntRefFreqs().indexOf(params.sync.refFreqInternal) < 0) {
            params.sync.refFreqInternal = m_devTypeInfo->intRefFreqDefault();
        }
        m_ref_freq_is_configurable = m_devTypeInfo->intRefFreqIsConfigurable();
        m_ext_ref_freq_max = m_devTypeInfo->extRefFreqMax();

    }

    void x502Config::protUpdate() {
        const double ref_freq {refFreq()};

        results.dinFreq = ref_freq;

        X502_CalcDinFreq(ref_freq, &results.dinFreq, nullptr);
    }


}
