#ifndef LQMEAS_X502INFO_H
#define LQMEAS_X502INFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "x502TypeInfo.h"

namespace LQMeas {
    class x502Info : public DeviceInfo {
    public:
        x502Info(const x502TypeInfo &type, const QString &serial, unsigned fpga_ver,
                 unsigned plda_ver, unsigned mcu_ver);

        unsigned pldaVer() const {return m_plda_ver;}
        unsigned fpgaVer() const {return m_fpga_ver;}
        unsigned mcuVer() const {return  m_mcu_ver;}


        QString pldaVerStr() const;
        QString fpgaVerStr() const;
        QString mcuVerStr() const;

        bool galPresent() const {return devspecTypeInfo().galPresent();}
        bool dacPresent() const {return devspecTypeInfo().dacPresent();}
        bool bfPresent() const  {return devspecTypeInfo().bfPresent();}
        bool ethPresent() const {return devspecTypeInfo().ethPresent();}
        bool industrial() const {return devspecTypeInfo().industrial();}

        bool mcuPresent() const {return devspecTypeInfo().mcuAvailable();}
        bool pldaPresent() const {return devspecTypeInfo().pldaAvailable();}
        bool ethOptionApplicable() const {return devspecTypeInfo().ethOptionApplicable();}
        bool bfOptionApplicable() const {return devspecTypeInfo().bfOptionApplicable();}
        bool galOptionApplicable() const {return devspecTypeInfo().galOptionApplicable();}

        const x502TypeInfo &devspecTypeInfo() const {
            return static_cast<const x502TypeInfo &>(type());
        }

    protected:
        x502Info(const x502Info &info);


        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        unsigned m_fpga_ver;
        unsigned m_plda_ver;
        unsigned m_mcu_ver;
    };
}


#endif // LQMEAS_X502INFO_H
