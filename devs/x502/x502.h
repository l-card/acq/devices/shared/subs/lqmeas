#ifndef LQMEAS_X502_H
#define LQMEAS_X502_H

#include "x502api.h"
#include "lqmeas/devs/Device.h"
#include "lqmeas/devs/DeviceFrameSender.h"
#include "lqmeas/ifaces/in/adc/DevAdc.h"
#include "lqmeas/ifaces/in/dig/DevInAsyncDig.h"
#include "lqmeas/ifaces/in/dig/DevInSyncDig.h"
#include "lqmeas/ifaces/out/DevOutSync.h"
#include "lqmeas/ifaces/out/DevOutAsyncDac.h"
#include "lqmeas/ifaces/out/DevOutAsyncDig.h"
#include "lqmeas/ifaces/out/SignalConverter/DevOutDacConverter.h"

#include "lqmeas/ifaces/out/SyncModes/stream/DevOutSyncIfaceStream.h"
#include "lqmeas/ifaces/out/SyncModes/cycle/DevOutSyncIfaceCycle.h"

#include <QMutex>
#include <QElapsedTimer>



namespace LQMeas {
    class x502Info;
    class x502Config;

    class x502 : public Device, public DevAdc, public DevOutSync,
            public DevOutAsyncDac, public DevOutAsyncDig,
            public DevInAsyncDig, public DevInSyncDig,
            public DevOutDacConverter,
            public DevOutSyncIfaceStream, public DevOutSyncIfaceCycle {
        Q_OBJECT
    public:
        ~x502() override;

        QString location() const override;
        bool isOpened() const override;
        bool checkConnection() const override;

        bool supportCustomOutFreq() const;


        /* ----------------------- интерфейс ввода  --------------------------*/
        DevAdc *devAdc() override {return static_cast<DevAdc*>(this);}
        DevInAsyncDig *devInAsyncDig() override {return static_cast<DevInAsyncDig*>(this);}
        DevInSyncDig *devInSyncDig() override   {return static_cast<DevInSyncDig*>(this);}


        /* ------------------- интерфейс вывода --------------------------------*/        
        DevOutSync *devOutSync() override {return static_cast<DevOutSync*>(this);}
        DevOutAsyncDac *devOutAsyncDac() override {return static_cast<DevOutAsyncDac*>(this);}
        DevOutAsyncDig *devOutAsyncDig() override {return static_cast<DevOutAsyncDig*>(this);}
        DevOutDacConverter *devOutDacConverter() override {return static_cast<DevOutDacConverter *>(this);}

        /* ------------------- дополнительные функции --------------------------*/
        t_x502_hnd rawHandle() const {return m_hnd;}

        const x502Config &devspecConfig() const;
        QSharedPointer<const x502Info> devspecInfo() const;

        static void getError(int err_code, LQError &err);
        static void getError(int err_code, const QString &descr, LQError &err);

        static LQError error(int err_code);
        static LQError error(int err_code, const QString &descr);

        DeviceRef *createRef() const override;
    protected:
        x502(const t_x502_devrec &devrec, const x502Info &info);


        void protOpen(OpenFlags flags, LQError &err) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;

        void protAdcEnable(LQError &err) override;
        void protAdcDisable(LQError &err) override;
        void protAdcStart(LQError &err) override;
        void protAdcStop(LQError &err) override;
        void protAdcGetData(double *data, int size, unsigned flags,
                            unsigned tout, int &recvd_size, LQError &err) override;

        void protInAsyncDig(unsigned &val, LQError &err) override;


        void protInDigEnable (LQError &err) override;
        void protInDigDisable (LQError &err) override;
        void protInDigStart(LQError &err) override;
        void protInDigStop(LQError &err) override;
        void protInDigGetData(quint32 *data, int size,  unsigned tout, int &recvd_size, LQError &err) override;


        void protOutAsyncDac(int ch, double val, LQError &err) override;
        void protOutAsyncDig(unsigned val, unsigned mask, LQError &err) override;

        void rawWordsSend(const uint32_t *wrds, int size, unsigned tout, int &sent_size, LQError &err);


        virtual const x502Info *createInfo(const t_x502_info &info) const = 0;
    private:
        static const DeviceInterface &devrecIface(const t_x502_devrec &devrec);

        void privOutStreamInit(LQError &err) override;
        void privOutStreamStartRequest(LQError &err) override;
        void privOutStreamStop(unsigned tout, LQError &err) override;

        void privOutSyncSendData(const double *dac_data, int dac_size,
                                 const unsigned *dig_data, int dig_size,
                                 unsigned flags, unsigned tout, LQError &err) override;
        bool privOutSyncHasDefferedData() const override;
        void privOutSyncTryFlushData(unsigned tout, LQError &err) override;
        void privOutSyncSendFinish(LQError &err) override;
        bool privOutSyncUnderflowOccured() const override;

        void privOutCycleLoadStart(const OutRamSignalGenerator &generator, int size, LQError &err) override;
        void privOutCycleLoadFinish(LQError &err) override;
        void privOutCycleGenStartRequest(LQError &err) override;
        void privOutCycleGenUpdateRequest(LQError &err) override;
        void privOutCycleGenStopRequest(unsigned tout, LQError &err) override;
        void privOutCycleGenStop(LQError &err) override;


        void rawWordsReceive(double *data, int size, unsigned tout, int &recvd_size, LQError &err);
        void rxNewData(int rx_size, unsigned tout, LQError &err);

        void setOutStreams(LQError &err);
        void setNewDevRecord(const t_x502_devrec &devrec);


        bool streamInOutRunning()  const {return m_adcRunning || m_dinRunning || m_outRunning;}
        void startStream(bool &flag, LQError &err);
        void stopStream(bool &flag, LQError &err);

        DeviceFrameSender<x502, uint32_t> m_sender;

        t_x502_devrec m_devrec;
        t_x502_hnd m_hnd;
        bool m_opened {false};

        bool m_adcRunning {false};
        bool m_dinRunning {false};
        bool m_outRunning {false};
        bool m_dinEnabled {false};
        bool m_adcEnabled {false};
        uint32_t m_outStreams {0};

        QVector<double> m_lastAdcData;
        QVector<quint32> m_lastDinData;
        QVector<quint32> m_wrdsBuf;
        int m_lastAdcWrdsCnt {0};
        int m_lastDinWrdsCnt {0};
        double m_adcDinMultipler {0};

        QElapsedTimer m_outStopTmr;
        unsigned m_outStopTout {0};

        QMutex m_streamLock;

        friend class DeviceFrameSender<x502, uint32_t>;
        friend class x502Resolver;

    };
}

#endif // LQMEAS_X502_H
