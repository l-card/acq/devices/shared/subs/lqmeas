#include "x502NativeError.h"

namespace LQMeas {
    LQError x502NativeError::error(int code, const QString &msg) {
        static const QString err_type {QStringLiteral("x502")};
        return LQError{code, msg, err_type};
    }
}
