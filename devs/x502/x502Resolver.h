#ifndef LQMEAS_X502RESOLVER_H
#define LQMEAS_X502RESOLVER_H

#include <QObject>
#include <QSharedPointer>
#include <QHash>
#include "x502.h"
#include "lqmeas/devs/resolver/DevicesResolver.h"
#ifdef LQMEAS_DEV_E502
    #include "net/x502NetworkBrowser.h"
#endif


namespace LQMeas {
    class x502Resolver : public DevicesResolver {
    public:
#ifdef LQMEAS_DEV_E502
        DeviceNetworkBrowser *netBrowser() override {return &m_netBrowser;}
#endif
        static x502Resolver &resolver();
    protected:
        QList<QSharedPointer<Device> > protGetDevList(const QList<const DeviceRef *> &devRefs, ResolveFlags flags) override;
    private:
        static const int m_con_tout {5000};

        explicit x502Resolver();

        QList< QSharedPointer<Device> > m_devs;
#ifdef LQMEAS_DEV_E502
        x502NetworkBrowser m_netBrowser;
#endif
    };
}

#endif // X502RESOLVER_H
