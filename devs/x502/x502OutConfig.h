#ifndef LQMEAS_X502OUTCONFIG_H
#define LQMEAS_X502OUTCONFIG_H

#include "lqmeas/ifaces/out/DevOutSyncConfig.h"

namespace LQMeas {
    class x502Config;

    class x502OutConfig : public DevOutSyncConfig  {
    public:
        static const int dac_max_channels_cnt  {2};
        static const int dout_max_channels_cnt {16};

        int outDacChRangeNum(int ch) const override {Q_UNUSED(ch) return 0;}

        double outSyncGenFreq() const override {return m_results.OutFreq;}

        bool outDacChSyncModeEnabled(int ch) const override;
        bool outDigChSyncModeEnabled(int ch) const override;
        bool outDigSyncModeEnabled() const;

        int outSyncDacSampleSize() const override {return 4;}
        int outSyncDigSampleSize() const override {return 4;}
    public Q_SLOTS:
        void outSyncSetGenFreq(double val);
        void outDacChSetSyncModeEnabled(int ch, bool en);
        void outDigSetSyncModeEnabled(bool en);

    protected:
        void protLoad(const QJsonObject &outObj) override;
        void protSave(QJsonObject &outObj) const override;
        void protUpdate() override;
    private:
        friend class x502Config;
        explicit x502OutConfig(x502Config *devCfg, const DevOutInfo &info);
        explicit x502OutConfig(x502Config *devCfg, const x502OutConfig &outCfg);

        struct {
            double OutFreq;
            struct {
                struct {
                    /* для каналов ЦАП можем отдельно выбирать по каналу,
                       использовать синхронный или асинхронный режим */
                    bool syncMode;
                } Ch[dac_max_channels_cnt];
            } Dac;

            struct {
                /* для цифровых линий можем выводить либо все
                 * синхронно, либо все асинхронно */
                bool syncMode;
            } DOut;
        } m_params;

        struct {
            double OutFreq;
        } m_results;

        x502Config *m_devCfg;
    };
}

#endif // X502OUTCONFIG_H
