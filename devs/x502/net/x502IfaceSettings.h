#ifndef LQMEAS_X502IFACESETTINGS_H
#define LQMEAS_X502IFACESETTINGS_H

#include <QHostAddress>

namespace LQMeas {
    class x502IfaceSettings {
    public:
        void setIfaceEn(bool en) {
            m_iface_en = en;
        }
        bool ifaceEn() const {return m_iface_en;}

        const QString &deviceInstanceName() const {return m_dev_instance_name;}
        void setDeviceInstanceName(const QString &name) {
            m_dev_instance_name = name;
        }

        bool autoGetIpParamsEn() const {return m_auto_ip;}
        void setAutoGetIpParamsEn(bool en) {
            m_auto_ip = en;
        }

        const QHostAddress &ipAddr() const {return m_ip_addr;}
        const QHostAddress &ipMask() const {return m_ip_mask;}
        const QHostAddress &ipGate() const {return m_gate_addr;}

        void setIpAddr(const QHostAddress &addr) {
            m_ip_addr = addr;
        }
        void setIpMask(const QHostAddress &mask) {
            m_ip_mask = mask;
        }
        void setIpGate(const QHostAddress &addr) {
            m_gate_addr = addr;
        }

        quint16 tcpCmdPort() const {return m_tcp_cmd_port;}
        quint16 tcpDataPort() const {return m_tcp_data_port;}
        void setTcpCmdPort(quint16 port) {
            m_tcp_cmd_port = port;
        }
        void setTcpDataPort(quint16 port) {
            m_tcp_data_port = port;
        }

        void setUserMacEn(bool en) {
            m_user_mac_en = en;
        }
        bool userMacEn() const {return m_user_mac_en;}
        void setUserMac(const quint8 *mac) {
            memcpy(m_user_mac, mac, sizeof(m_user_mac));
        }
        const quint8 *userMac() const {return m_user_mac;}
        const quint8 *factoryMac() const {return m_factory_mac;}

        void setChangePasswdEn(bool en) {
            m_set_new_passwd_en = en;
        }
        bool changePasswdEn() const {return m_set_new_passwd_en;}
        void setNewPasswd(const QString passwd) {
            m_new_passwd = passwd;
        }
        const QString &newPasswd() const {return m_new_passwd;}
        static const int mac_addr_len = 6;

        static constexpr quint16 defaultCmdPort() {return 11114;}
        static constexpr quint16 defaultDataPort() {return 11115;}
    private:
        bool m_iface_en {false};
        QString m_dev_instance_name;
        QHostAddress m_ip_addr {"192.168.0.1"};
        QHostAddress m_ip_mask {"255.255.255.0"};
        QHostAddress m_gate_addr;
        bool m_auto_ip {false};
        bool m_user_mac_en {false};
        quint8 m_user_mac[mac_addr_len];
        quint8 m_factory_mac[mac_addr_len];
        bool m_set_new_passwd_en {false};
        QString m_new_passwd;

        quint16 m_tcp_cmd_port {defaultCmdPort()};
        quint16 m_tcp_data_port {defaultDataPort()};


        friend class x502NetDevice;
    };
}

#endif // LQMEAS_X502IFACESETTINGS_H

