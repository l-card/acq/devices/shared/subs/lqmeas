#include "x502DeviceRefNetSvc.h"
#include "lqmeas/devs/DeviceInfo.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/devs/DeviceRefMethod.h"
#include "lqmeas/StdConfigKeys.h"
#include "x502NetDevice.h"
#include "x502api.h"
#include "x502NetSvcRecord.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String& cfgkey_net_svc_group {StdConfigKeys::netSvc()};
    static const QLatin1String& cfgkey_instance_name {StdConfigKeys::instanceName()};
    static const QLatin1String& cfgkey_con_tout {StdConfigKeys::conTout()};

    x502DeviceRefNetSvc::x502DeviceRefNetSvc(const DeviceInfo &devinfo, const QString &instanceName, int conTout) :
        DeviceRef{devinfo, DeviceInterfaces::ethernet(), DeviceRefMethods::netName()},
        m_instanceName{instanceName},
        m_conTout{conTout} {

    }

    x502DeviceRefNetSvc::x502DeviceRefNetSvc(const x502DeviceRefNetSvc &ref) :
        DeviceRef{ref},
        m_instanceName{ref.m_instanceName},
        m_conTout{ref.m_conTout} {

    }

    x502DeviceRefNetSvc::x502DeviceRefNetSvc(const DeviceInfo &devinfo, const x502DeviceRefNetSvc &ref) :
        DeviceRef{devinfo, DeviceInterfaces::ethernet(), DeviceRefMethods::netName()},
        m_instanceName{ref.m_instanceName},
        m_conTout{ref.m_conTout} {

    }

    const QString &x502DeviceRefNetSvc::typeRefTypeID() {
            /** @note используем название e502 для совместимости со старой версией */
        static const QString str { QStringLiteral("e502ethsvc") };
        return str;
    }


    bool x502DeviceRefNetSvc::checkNetSvcRecord(const x502NetSvcRecord &rec) const  {
        const QString serial {devInfo().serial()};
        const QString devname {devInfo().type().deviceTypeName()};
        return (rec.instanceName() == m_instanceName) &&
               (rec.deviceTypeName() == devname) &&
               (serial.isEmpty() || (serial == rec.deviceSerial()));
    }

    LQMeas::DeviceRef::DevCheckResult LQMeas::x502DeviceRefNetSvc::checkDevice(const LQMeas::Device &device) const {
        DevCheckResult res {DevCheckResult::BadDevice};
        const x502NetDevice *x502 {qobject_cast<const x502NetDevice *>(&device)};
        if (x502) {
            const QSharedPointer<const x502NetSvcRecord> &netRec {x502->netRecord()};
            if (netRec && checkNetSvcRecord(*netRec)) {
                res = DevCheckResult::GoodDevice;
            }
        }
        return res;
    }

    bool x502DeviceRefNetSvc::sameDevice(const DeviceRef &ref) const {
        bool same {ref.refTypeID() == refTypeID()};
        if (same) {
            const x502DeviceRefNetSvc &svcRef = static_cast<const x502DeviceRefNetSvc &>(ref);
            same = svcRef.instanceName() == m_instanceName;
        }
        return  same;
    }

    void x502DeviceRefNetSvc::save(QJsonObject &refObj) const {
        DeviceRef::save(refObj);

        QJsonObject svcObj;
        svcObj[cfgkey_instance_name] = m_instanceName;
        svcObj[cfgkey_con_tout] = m_conTout;
        refObj[cfgkey_net_svc_group] = svcObj;
    }

    void x502DeviceRefNetSvc::load(const QJsonObject &refObj) {
        DeviceRef::load(refObj);

        const QJsonObject &svcObj = refObj[cfgkey_net_svc_group].toObject();
        m_instanceName = svcObj[cfgkey_instance_name].toString();
        m_conTout = svcObj[cfgkey_con_tout].toInt();
    }

    DeviceRef *x502DeviceRefNetSvc::clone() const {
        return new x502DeviceRefNetSvc{*this};
    }


    QString x502DeviceRefNetSvc::displayName() const {
        return QString{"%1, %2 '%3'"}.arg(DeviceRef::displayName(),
                                          x502::tr("Net Name"),
                                          m_instanceName);
    }

    QString x502DeviceRefNetSvc::protKey() const {
        return QString{"%1_netsvc%2"}.arg(DeviceRef::protKey(), m_instanceName);
    }

    t_x502_devrec x502DeviceRefNetSvc::createDevrec(const x502NetSvcRecord &rec) const     {
        return rec.createDevrec(m_conTout);
    }
}
