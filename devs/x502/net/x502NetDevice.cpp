#include "x502NetDevice.h"
#include "e502api.h"
#include "lqmeas/devs/x502/x502Info.h"
#include "x502DeviceRefIPAddr.h"
#include "x502DeviceRefNetSvc.h"
#include "x502NetSvcRecord.h"
#include "x502IfaceSettings.h"

namespace LQMeas {

    x502NetDevice::~x502NetDevice() {

    }

    QHostAddress x502NetDevice::ipAddr() const {
        uint32_t addr;
        int err_code {E502_GetIpAddr(rawHandle(), &addr)};
        return err_code == X502_ERR_OK ?  QHostAddress{addr} : QHostAddress{};
    }


    DeviceRef *x502NetDevice::createRef() const {
        return m_ipAddrRef ? static_cast<DeviceRef*>(new x502DeviceRefIPAddr{*devInfo(), *m_ipAddrRef}) :
               m_netSvcRef ? static_cast<DeviceRef*>(new x502DeviceRefNetSvc{*devInfo(), *m_netSvcRef}) :
               m_svcRecord ? static_cast<DeviceRef*>(new x502DeviceRefNetSvc{*devInfo(), m_svcRecord->instanceName(), x502DeviceRefNetSvc::defaultConnectionTimeout()}) :
                            x502::createRef();
    }

    void x502NetDevice::ifaceSettingsRead(x502IfaceSettings &netset, LQError &err) {
        qint32 err_code = X502_ERR_OK;
        t_e502_eth_config_hnd cfg = E502_EthConfigCreate();
        if (!cfg) {
            err_code = X502_ERR_MEMORY_ALLOC;
        } else {
            err_code = E502_EthConfigRead(rawHandle(), cfg);
            if (err_code == X502_ERR_OK) {
                uint32_t ip_addr;
                err_code = E502_EthConfigGetIPv4Addr(cfg, &ip_addr);
                if (err_code == X502_ERR_OK) {
                    netset.setIpAddr(QHostAddress{ip_addr});
                }
            }
            if (err_code == X502_ERR_OK) {
                uint32_t ip_mask;
                err_code = E502_EthConfigGetIPv4Mask(cfg, &ip_mask);
                if (err_code == X502_ERR_OK) {
                    netset.setIpMask(QHostAddress{ip_mask});
                }
            }
            if (err_code == X502_ERR_OK) {
                uint32_t gateway;
                err_code = E502_EthConfigGetIPv4Gate(cfg, &gateway);
                if (err_code == X502_ERR_OK) {
                    netset.setIpGate(QHostAddress{gateway});
                }
            }
            if (err_code == X502_ERR_OK) {
                uint32_t en;
                err_code = E502_EthConfigGetEnabled(cfg,  &en);
                if (err_code == X502_ERR_OK) {
                    netset.setIfaceEn(en);
                }
            }
            if (err_code == X502_ERR_OK) {
                uint32_t en;
                err_code = E502_EthConfigGetAutoIPEnabled(cfg,  &en);
                if (err_code == X502_ERR_OK) {
                    netset.setAutoGetIpParamsEn(en);
                }
            }
            if (err_code == X502_ERR_OK) {
                uint32_t en;
                err_code = E502_EthConfigGetUserMACEnabled(cfg,  &en);
                if (err_code == X502_ERR_OK) {
                    netset.setUserMacEn(en);
                }
            }
            if (err_code == X502_ERR_OK) {
                err_code = E502_EthConfigGetUserMac(cfg,  netset.m_user_mac);
            }
            if (err_code == X502_ERR_OK) {
                err_code = E502_EthConfigGetFactoryMac(cfg,  netset.m_factory_mac);
            }
            if (err_code == X502_ERR_OK) {
                char inst_name[X502_INSTANCE_NAME_SIZE];
                err_code = E502_EthConfigGetInstanceName(cfg, inst_name);
                if (err_code == X502_ERR_OK) {
                    netset.setDeviceInstanceName(QString::fromUtf8(inst_name));
                }
            }
            if (err_code == X502_ERR_OK) {
                netset.setChangePasswdEn(false);
                netset.setNewPasswd({});
            }
            E502_EthConfigFree(cfg);
        }


        if (err_code != X502_ERR_MEMORY_ALLOC) {
            err = error(err_code);
        }
    }

    void x502NetDevice::ifaceSettingsWrite(const x502IfaceSettings &netset, const QString &passwd, LQError &err) {
        qint32 err_code = X502_ERR_OK;
        t_e502_eth_config_hnd cfg = E502_EthConfigCreate();
        if (!cfg) {
            err_code = X502_ERR_MEMORY_ALLOC;
        } else {
            err_code = E502_EthConfigRead(rawHandle(), cfg);

            if (err_code == X502_ERR_OK) {
                err_code = E502_EthConfigSetIPv4Addr(cfg, netset.ipAddr().toIPv4Address());
            }
            if (err_code == X502_ERR_OK) {
                err_code = E502_EthConfigSetIPv4Mask(cfg, netset.ipMask().toIPv4Address());
            }
            if (err_code == X502_ERR_OK) {
                err_code = E502_EthConfigSetIPv4Gate(cfg, netset.ipGate().toIPv4Address());
            }
            if (err_code == X502_ERR_OK) {
                err_code = E502_EthConfigSetEnabled(cfg, netset.ifaceEn() ? 1 : 0);
            }
            if (err_code == X502_ERR_OK) {
                err_code = E502_EthConfigSetAutoIPEnabled(cfg, netset.autoGetIpParamsEn() ? 1 : 0);
            }
            if (err_code == X502_ERR_OK) {
                err_code = E502_EthConfigSetTcpCmdPort(cfg, netset.tcpCmdPort());
            }
            if (err_code == X502_ERR_OK) {
                err_code = E502_EthConfigSetTcpDataPort(cfg, netset.tcpDataPort());
            }
            if (err_code == X502_ERR_OK) {
                err_code = E502_EthConfigSetUserMACEnabled(cfg, netset.userMacEn() ? 1 : 0);
            }
            if (err_code == X502_ERR_OK) {
                err_code = E502_EthConfigSetUserMac(cfg, netset.userMac());
            }
            if (err_code == X502_ERR_OK) {
                err_code = E502_EthConfigSetInstanceName(cfg, netset.deviceInstanceName().toUtf8());
            }
            if (err_code == X502_ERR_OK) {
                if (netset.changePasswdEn()) {
                    err_code = E502_EthConfigSetNewPassword(cfg, netset.newPasswd().toUtf8());
                }
            }
            if (err_code == X502_ERR_OK) {
                err_code = E502_EthConfigWrite(rawHandle(), cfg, passwd.toUtf8());
            }

            E502_EthConfigFree(cfg);
        }


        if (err_code != X502_ERR_MEMORY_ALLOC) {
            err = error(err_code);
        }
    }


    x502NetDevice::x502NetDevice(const t_x502_devrec &devrec, const x502Info &info) : x502(devrec, info) {

    }

    x502NetDevice::x502NetDevice(const x502DeviceRefIPAddr &ipRef, const x502Info &info) :
        x502(ipRef.createDevrec(), info), m_ipAddrRef{new x502DeviceRefIPAddr{ipRef}} {

    }

    x502NetDevice::x502NetDevice(const QSharedPointer<const x502NetSvcRecord> &svcRec, int conTout, const x502Info &info) :
        x502(svcRec->createDevrec(conTout), info),
        m_svcRecord{svcRec} {

    }

    x502NetDevice::x502NetDevice(const QSharedPointer<const x502NetSvcRecord> &svcRec, const x502DeviceRefNetSvc &netRef)  :
        x502(netRef.createDevrec(*svcRec), static_cast<const x502Info &>(netRef.devInfo())),
        m_netSvcRef{new x502DeviceRefNetSvc{netRef}},
        m_svcRecord{svcRec} {


    }




}
