#ifndef LQMEAS_X502NETWORKBROWSER_H
#define LQMEAS_X502NETWORKBROWSER_H

#include "lqmeas/devs/resolver/netBrowser/DeviceNetworkBrowser.h"

namespace LQMeas {
    class x502NetworkBrowser : public DeviceNetworkBrowser {
        Q_OBJECT
    public:
        x502NetworkBrowser();
    protected:
        void run() override;
    };
}
#endif // LQMEAS_X502NETWORKBROWSER_H
