#include "x502NetworkBrowser.h"
#include "x502NetSvcRecord.h"
#include "e502api.h"
#include "../x502.h"
#include "LQError.h"

namespace LQMeas {
    x502NetworkBrowser::x502NetworkBrowser() {

    }

    void x502NetworkBrowser::run() {
        t_e502_eth_svc_browse_hnd browse_hnd;
        int32_t err_code = E502_EthSvcBrowseStart(&browse_hnd, 0);
        if (err_code != X502_ERR_OK) {
            LQError err;
            x502::getError(err_code, err);
            setError(err, tr("Cannot start network browse for E502/E16 modules"));
        } else {
            while ((err_code == X502_ERR_OK) && !stopIsRequested()) {
                t_e502_eth_svc_record_hnd svc_hnd;
                uint32_t event;
                err_code = E502_EthSvcBrowseGetEvent(browse_hnd, &svc_hnd, &event, nullptr, 100);
                if (err_code != X502_ERR_OK) {
                    LQError err;
                    x502::getError(err_code, err);
                    setError(err, tr("Error occured during wait for new browse event for E502/E16 modules"));
                } else {
                    if (event != E502_ETH_SVC_EVENT_NONE) {
                        QSharedPointer<DeviceNetSvcRecord> rec{new x502NetSvcRecord(svc_hnd)};

                        if (event == E502_ETH_SVC_EVENT_ADD) {
                            addRecord(rec);
                        } else if (event == E502_ETH_SVC_EVENT_CHANGED) {
                            changeRecord(rec);
                        } else if (event == E502_ETH_SVC_EVENT_REMOVE) {
                            removeRecord(rec);
                        }
                    }
                }
            }

            E502_EthSvcBrowseStop(browse_hnd);
        }
    }
}
