#ifndef LQMEAS_X502NETSVCRECORD_H
#define LQMEAS_X502NETSVCRECORD_H

#include "lqmeas/devs/resolver/netBrowser/DeviceNetSvcRecord.h"
#include "e502api.h"

namespace LQMeas {
    class x502NetSvcRecord : public DeviceNetSvcRecord{
    public:
        QString instanceName() const override;
        QString deviceTypeName() const override;
        QString deviceSerial() const override;
        QHostAddress hostAddress() const override;
        bool isEqual(const DeviceNetSvcRecord &rec) const override;
        ~x502NetSvcRecord() override;
    private:
        x502NetSvcRecord(t_e502_eth_svc_record_hnd hnd);
        t_x502_devrec createDevrec(int connectionTout) const;

        static constexpr int defaultAddrResolveTout() {return 5000;}

        t_e502_eth_svc_record_hnd m_hnd;
        mutable QHostAddress m_addr;

        friend class x502NetworkBrowser;
        friend class x502DeviceRefNetSvc;
        friend class x502NetDevice;
    };
}

#endif // LQMEAS_X502NETSVCRECORD_H
