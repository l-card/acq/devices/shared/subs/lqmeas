#include "x502DeviceRefIPAddr.h"
#include "x502NetDevice.h"
#include "e502api.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/devs/DeviceRefMethod.h"
#include "lqmeas/devs/DeviceInfo.h"
#include "lqmeas/devs/DeviceTypeInfo.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {

static const QLatin1String& cfgkey_ip_group  {StdConfigKeys::ip()};
static const QLatin1String& cfgkey_ipv4_addr {StdConfigKeys::ipv4Addr()};
static const QLatin1String& cfgkey_con_tout  {StdConfigKeys::conTout()};
static const QLatin1String cfgkey_cmd_port {"CmdPort"};
static const QLatin1String cfgkey_data_port {"DataPort"};
static const QLatin1String cfgkey_custom_port_group {"CustomPorts"};

x502DeviceRefIPAddr::x502DeviceRefIPAddr(const DeviceInfo &devinfo,
                                         const QHostAddress &ipAddr, int conTout) :
    DeviceRef{devinfo, DeviceInterfaces::ethernet(), DeviceRefMethods::ipAddr()},
    m_ipAddr{ipAddr},
    m_conTout{conTout},
    m_customPorts{false},
    m_cmdPort{defaultCmdPort()},
    m_dataPort{defaultDataPort()} {

}

x502DeviceRefIPAddr::x502DeviceRefIPAddr(const x502DeviceRefIPAddr &ipRef) :
    DeviceRef{ipRef},
    m_ipAddr{ipRef.m_ipAddr},
    m_conTout{ipRef.m_conTout},
    m_customPorts{ipRef.m_customPorts},
    m_cmdPort{ipRef.m_cmdPort},
    m_dataPort{ipRef.dataPort()} {

}

x502DeviceRefIPAddr::x502DeviceRefIPAddr(const DeviceInfo &devinfo,
                                         const x502DeviceRefIPAddr &ipRef) :
    DeviceRef{devinfo, DeviceInterfaces::ethernet(), DeviceRefMethods::ipAddr()},
    m_ipAddr{ipRef.m_ipAddr},
    m_conTout{ipRef.m_conTout},
    m_customPorts{ipRef.m_customPorts},
    m_cmdPort{ipRef.m_cmdPort},
    m_dataPort{ipRef.dataPort()} {

}

const QString &x502DeviceRefIPAddr::typeRefTypeID() {
    /** @note используем название e502 для совместимости со старой версией */
    static const QString str { QStringLiteral("e502ipaddr") };
    return str;
}

DeviceRef *x502DeviceRefIPAddr::clone() const {
    return new x502DeviceRefIPAddr{*this};
}



DeviceRef::DevCheckResult x502DeviceRefIPAddr::checkDevice(const Device &device) const {
    DevCheckResult res {DevCheckResult::BadDevice};
    const x502NetDevice *x502 {qobject_cast<const x502NetDevice *>(&device)};
    if (x502) {
        const x502DeviceRefIPAddr *devRef {x502->ipAddrRef()};
        if (devRef && devRef->sameDevice(*this)) {
            res = DevCheckResult::GoodDevice;
        }
    }
    return res;
}

bool x502DeviceRefIPAddr::sameDevice(const DeviceRef &ref) const  {
    bool same = ref.refTypeID() == refTypeID();
    if (same) {
        const x502DeviceRefIPAddr &ipRef = static_cast<const x502DeviceRefIPAddr &>(ref);
        same = (ipRef.ipAddr() == ipAddr())
               && (ipRef.cmdPort() == cmdPort())
               && (ipRef.dataPort() == dataPort());
    }
    return  same;
}

void x502DeviceRefIPAddr::save(QJsonObject &refObj) const {
    DeviceRef::save(refObj);

    QJsonObject ipObj;
    ipObj[cfgkey_ipv4_addr] = m_ipAddr.toString();
    ipObj[cfgkey_con_tout] = m_conTout;
    QJsonObject portObj;
    portObj[StdConfigKeys::enabled()] = m_customPorts;
    portObj[cfgkey_cmd_port] = m_cmdPort;
    portObj[cfgkey_data_port] = m_dataPort;
    ipObj[cfgkey_custom_port_group] = portObj;

    refObj[cfgkey_ip_group] = ipObj;
}

void x502DeviceRefIPAddr::load(const QJsonObject &refObj) {
    DeviceRef::load(refObj);

    const QJsonObject &ipObj = refObj[cfgkey_ip_group].toObject();
    m_ipAddr = QHostAddress{ipObj[cfgkey_ipv4_addr].toString()};
    m_conTout = ipObj[cfgkey_con_tout].toInt(defaultConnectionTimeout());

    const QJsonObject &portObj = ipObj[cfgkey_custom_port_group].toObject();
    m_customPorts = portObj[StdConfigKeys::enabled()].toBool(false);
    m_cmdPort     = portObj[cfgkey_cmd_port].toInt(defaultCmdPort());
    m_dataPort    = portObj[cfgkey_data_port].toInt(defaultDataPort());
}

QString x502DeviceRefIPAddr::displayName() const {
    QString ret = QString("%1, %2 %3").arg(DeviceRef::displayName(),  x502::tr("IP"), m_ipAddr.toString());
    if (m_customPorts) {
        ret.append(QString(":%1/%2").arg(m_cmdPort).arg(m_dataPort));
    }
    return  ret;
}


QString x502DeviceRefIPAddr::protKey() const {
    QString ret = QString("%1_ip%2").arg(DeviceRef::protKey(), m_ipAddr.toString());
    if (m_customPorts) {
        ret.append(QString("_p%1_%2").arg(m_cmdPort).arg(m_dataPort));
    }
    return ret;
}

t_x502_devrec x502DeviceRefIPAddr::createDevrec() const {
    t_x502_devrec devrec;
    int32_t err = E502_MakeDevRecordByIpAddr2(&devrec, m_ipAddr.toIPv4Address(),
                                              0, static_cast<uint32_t>(m_conTout),
                                              devInfo().type().deviceTypeName().toUtf8());
    if (err == X502_ERR_OK) {
        if (m_customPorts) {
            E502_EthDevRecordSetCmdPort(&devrec, m_cmdPort & 0xFFFF);
            E502_EthDevRecordSetDataPort(&devrec, m_dataPort & 0xFFFF);
        }
    }
    return  devrec;

}
}
