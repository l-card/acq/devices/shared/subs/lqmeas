#ifndef X502NETDEVICE_H
#define X502NETDEVICE_H

#include "lqmeas/devs/x502/x502.h"
#include <QHostAddress>
#include <memory>



namespace LQMeas {
class x502Resolver;
class x502DeviceRefIPAddr;
class x502DeviceRefNetSvc;
class x502NetSvcRecord;
class x502IfaceSettings;

class x502NetDevice : public x502 {
    Q_OBJECT
public:
    ~x502NetDevice() override;
    QHostAddress ipAddr() const;

    const x502DeviceRefIPAddr *ipAddrRef() const {return m_ipAddrRef.get();}
    QSharedPointer<const x502NetSvcRecord> netRecord() const {return m_svcRecord;}
    DeviceRef *createRef() const override;

    void ifaceSettingsRead(x502IfaceSettings &netset, LQError &err);
    void ifaceSettingsWrite(const x502IfaceSettings &netset, const QString &passwd, LQError &err);
protected:
    explicit x502NetDevice(const t_x502_devrec &devrec, const x502Info &info);
    explicit x502NetDevice(const x502DeviceRefIPAddr &ipRef, const x502Info &info);
    explicit x502NetDevice(const QSharedPointer<const x502NetSvcRecord> &svcRec, int conTout, const x502Info &info);
    explicit x502NetDevice(const QSharedPointer<const x502NetSvcRecord> &svcRec, const x502DeviceRefNetSvc &netRef);
private:
    std::unique_ptr<x502DeviceRefIPAddr> m_ipAddrRef;
    std::unique_ptr<x502DeviceRefNetSvc> m_netSvcRef;
    QSharedPointer<const x502NetSvcRecord> m_svcRecord;
};
}

#endif // X502NETDEVICE_H
