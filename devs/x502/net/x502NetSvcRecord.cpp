#include "x502NetSvcRecord.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas_config.h"
#ifdef LQMEAS_DEV_E502
#include "lqmeas/devs/x502/modules/E502/E502TypeInfo.h"
#endif

namespace LQMeas {

QString x502NetSvcRecord::instanceName() const {
    char name[X502_INSTANCE_NAME_SIZE];
    QString ret;
    if (E502_EthSvcRecordGetInstanceName(m_hnd, name) == X502_ERR_OK) {
        ret = QString::fromUtf8(name);
    }
    return ret;
}

QString x502NetSvcRecord::deviceTypeName() const {
    char name[X502_DEVNAME_SIZE];
    QString ret;
    if (E502_EthSvcRecordGetDevName(m_hnd, name) == X502_ERR_OK) {
        ret = QString::fromUtf8(name);
#ifdef LQMEAS_DEV_E502
        /* Для E502 запись возвращает имя типа без дефиса, заменяем на имя корректное
         * имя модуля для возможности напрямую сравнивать в дальнейшем с типом устройства */
        if (ret == E502TypeInfo::apiName()) {
            ret = E502TypeInfo::name();
        }
#endif
    }
    return ret;
}

QString x502NetSvcRecord::deviceSerial() const {
    char serial[X502_SERIAL_SIZE];
    QString ret;
    if (E502_EthSvcRecordGetDevSerial(m_hnd, serial) == X502_ERR_OK) {
        ret = QSTRING_FROM_CSTR(serial);
    }
    return ret;

}

QHostAddress x502NetSvcRecord::hostAddress() const {
    if (m_addr.isNull()) {
        uint32_t addr;
        if (E502_EthSvcRecordResolveIPv4Addr(m_hnd, &addr, defaultAddrResolveTout()) == X502_ERR_OK) {
            m_addr = QHostAddress(addr);
        }
    }
    return m_addr;
}

bool x502NetSvcRecord::isEqual(const DeviceNetSvcRecord &rec) const {
    bool eq = false;
    if (rec.deviceTypeName() == deviceTypeName()) {
        const x502NetSvcRecord &e502_rec = static_cast<const x502NetSvcRecord &>(rec);
        eq = E502_EthSvcRecordIsSameInstance(m_hnd, e502_rec.m_hnd) == X502_ERR_OK ? true : false;
    }
    return  eq;
}

x502NetSvcRecord::x502NetSvcRecord(t_e502_eth_svc_record_hnd hnd) {
    m_hnd = hnd;
}

t_x502_devrec x502NetSvcRecord::createDevrec(int connectionTout) const {
    t_x502_devrec devrec;
    E502_MakeDevRecordByEthSvc(&devrec, m_hnd, 0, static_cast<uint32_t>(connectionTout));
    return  devrec;
}

x502NetSvcRecord::~x502NetSvcRecord() {
    E502_EthSvcRecordFree(m_hnd);
}
}
