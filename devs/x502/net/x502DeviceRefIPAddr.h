#ifndef LQMEAS_X502DEVICEREFIPADDR_H
#define LQMEAS_X502DEVICEREFIPADDR_H

#include "x502api.h"
#include "lqmeas/devs/DeviceRef.h"
#include <QHostAddress>

namespace LQMeas {

class x502DeviceRefIPAddr : public DeviceRef {
public:

    explicit x502DeviceRefIPAddr(const DeviceInfo &devinfo,
                                 const QHostAddress &ipAddr = QHostAddress{},
                                 int conTout = defaultConnectionTimeout());
    x502DeviceRefIPAddr(const x502DeviceRefIPAddr &ipRef);
    explicit x502DeviceRefIPAddr(const DeviceInfo &devinfo, const x502DeviceRefIPAddr &ipRef);

    static const QString &typeRefTypeID();
    QString refTypeID() const override {return typeRefTypeID();}

    DeviceRef *clone() const override;

    bool serialUsed() const override {return false;}



    const QHostAddress &ipAddr() const {return m_ipAddr;}
    int connectionTimeout() const {return m_conTout;}
    int cmdPort() const {return m_customPorts ? m_cmdPort : defaultCmdPort();}
    int dataPort() const {return m_customPorts ? m_dataPort : defaultDataPort(); }

    bool useCustomPorts() const {return m_customPorts;}
    int customCmdPort() const {return  m_cmdPort;}
    int customDataPort() const {return  m_dataPort;}

    void setIpAddr(const QHostAddress &addr) { m_ipAddr = addr; }
    void setConnectionTimeout(int toutMs) {m_conTout = toutMs;}
    void setUseCustomPorts(bool en) {m_customPorts = en;}
    void setCustomCmdPort(int port) {m_cmdPort = port;}
    void setCustomDataPort(int port) {m_dataPort = port;}


    static constexpr int defaultCmdPort() {return 11114;}
    static constexpr int defaultDataPort() {return 11115;}
    static int defaultConnectionTimeout() {return 3000;}

    DevCheckResult checkDevice(const Device &device) const override;
    bool sameDevice(const DeviceRef &ref) const override;
    bool isAutoResolved() const override {return false;}

    void save(QJsonObject &refObj) const override;
    void load(const QJsonObject &refObj) override;

    QString displayName() const override;
protected:
    t_x502_devrec createDevrec() const;

    QString protKey() const override;
private:


    QHostAddress m_ipAddr;
    int m_conTout;
    bool m_customPorts;
    int m_cmdPort;
    int m_dataPort;

    friend class x502NetDevice;
};
}

#endif // X502DEVICEREFIPADDR_H
