#ifndef LQMEAS_X502DEVICEREFNETSVC_H
#define LQMEAS_X502DEVICEREFNETSVC_H

#include "x502api.h"
#include "lqmeas/devs/DeviceRef.h"
#include <QSharedPointer>

namespace LQMeas {
    class x502NetSvcRecord;

    class x502DeviceRefNetSvc : public DeviceRef {
    public:
        x502DeviceRefNetSvc(const DeviceInfo &devinfo, const QString &instanceName = QString{},
                            int conTout = defaultConnectionTimeout());
        x502DeviceRefNetSvc(const x502DeviceRefNetSvc &ref);
        x502DeviceRefNetSvc(const DeviceInfo &devinfo, const x502DeviceRefNetSvc &ref);

        static const QString &typeRefTypeID();
        QString refTypeID() const override {return typeRefTypeID();}

        const QString &instanceName() const {return m_instanceName;}
        int connectionTimeout() const {return m_conTout;}
        bool serialUsed() const override {return false;}

        void setInstanceName(const QString &name) {m_instanceName = name;}
        void setConnectionTimeout(int toutMs) {m_conTout = toutMs;}

        static int defaultConnectionTimeout() {return 3000;}

        bool checkNetSvcRecord(const x502NetSvcRecord &rec) const;

        DevCheckResult checkDevice(const Device &device) const override;
        bool sameDevice(const DeviceRef &ref) const override;

        void save(QJsonObject &refObj) const override;
        void load(const QJsonObject &refObj) override;

        DeviceRef *clone() const override;
        QString displayName() const override;
    protected:
        QString protKey() const override;
    private:
        friend class x502NetDevice;

        t_x502_devrec createDevrec(const x502NetSvcRecord &rec) const;

        QString m_instanceName;
        int m_conTout;

    };
}

#endif // X502DEVICEREFNETSVC_H
