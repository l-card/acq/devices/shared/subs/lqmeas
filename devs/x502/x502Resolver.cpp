#include "x502Resolver.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas_config.h"
#include <QScopedArrayPointer>
#include "x502.h"
#include "lqmeas/devs/DeviceRef.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/devs/DeviceInfo.h"
#include "lqmeas/devs/DeviceTypeInfo.h"

#if defined LQMEAS_DEV_E502 || defined LQMEAS_DEV_E16
    #include "e502api.h"
    #include "net/x502DeviceRefIPAddr.h"
    #include "net/x502NetSvcRecord.h"
    #include "net/x502DeviceRefNetSvc.h"
#endif
#ifdef LQMEAS_DEV_E502
    #include "modules/E502/E502.h"
    #include "modules/E502/E502TypeInfo.h"
#endif
#ifdef LQMEAS_DEV_E16
    #include "modules/E16/E16.h"
    #include "modules/E16/E16TypeInfo.h"
#endif
#ifdef LQMEAS_DEV_L502
    #include "l502api.h"
    #include "modules/L502/L502.h"
    #include "modules/L502/L502TypeInfo.h"
#endif

namespace LQMeas {
    x502Resolver::x502Resolver() {
    }


    QList<QSharedPointer<Device> > x502Resolver::protGetDevList(
            const QList<const DeviceRef *> &devRefs, ResolveFlags flags) {
        Q_UNUSED(flags)

        QList<QSharedPointer<Device> > newDevices;
        QList<QSharedPointer<Device> > oldDevices {m_devs};

        /* поиск автоопределяемых устройств, подключенных по PCIe или USB */
        {
            uint32_t devcnt_usb {0};
            uint32_t devcnt_usb_e16 {0};
            uint32_t devcnt_pci {0};
            uint32_t devcnt {0};
    #ifdef LQMEAS_DEV_E502
            E502_UsbGetDevRecordsList(nullptr, 0, 0, &devcnt_usb);
    #endif
#ifdef LQMEAS_DEV_E16
            E16_UsbGetDevRecordsList(nullptr, 0, 0, &devcnt_usb_e16);
#endif
    #ifdef LQMEAS_DEV_L502
            L502_GetDevRecordsList(nullptr, 0, 0, &devcnt_pci);
    #endif
            devcnt = devcnt_usb + devcnt_usb_e16 + devcnt_pci;

            if (devcnt > 0) {
                 int fnd_devcnt {0};
                int32_t res = X502_ERR_OK;
                QScopedArrayPointer<t_x502_devrec> devrecs{new t_x502_devrec[devcnt]};
                /* получаем информацию по всем устройствам  */
    #ifdef LQMEAS_DEV_E502
                res = E502_UsbGetDevRecordsList(devrecs.data(), devcnt_usb, 0, nullptr);
                if (res >= 0) {
                    fnd_devcnt += res;
                }
    #endif
#ifdef LQMEAS_DEV_E16
                res = E16_UsbGetDevRecordsList(&devrecs.data()[fnd_devcnt], devcnt_usb_e16, 0, nullptr);
                if (res >= 0) {
                    fnd_devcnt += res;
                }
#endif
    #ifdef LQMEAS_DEV_L502
                res = L502_GetDevRecordsList(&devrecs.data()[fnd_devcnt], devcnt_pci, 0, nullptr);
                if (res >= 0) {
                    fnd_devcnt += res;
                }
    #endif
                if (fnd_devcnt > 0) {
                    for (int devrec_idx = 0; devrec_idx < fnd_devcnt; ++devrec_idx) {
                        t_x502_devrec *prec {&devrecs[devrec_idx]};
                        QString devserial {QSTRING_FROM_CSTR(prec->serial)};
                        QString devname {QSTRING_FROM_CSTR(prec->devname)};
                        QSharedPointer<Device> fndDev;
                        if (devname == E502TypeInfo::apiName()) {
                            devname =  E502TypeInfo::name();
                        } else if (devname == L502TypeInfo::apiName()) {
                            devname = L502TypeInfo::name();
                        }

                        for (const QSharedPointer<Device> &dev : oldDevices) {
                            if ((dev->serial() == devserial) &&
                                    (dev->name() == devname) &&
                                    (dev->iface() != DeviceInterfaces::ethernet())) {
                                fndDev = dev;
                                break;
                            }
                        }

                        if (fndDev) {
                            oldDevices.removeOne(fndDev);
                            newDevices.append(fndDev);

                            QSharedPointer<x502> x502dev {qSharedPointerObjectCast<x502>(fndDev)};
                            x502dev->setNewDevRecord(*prec);
                        } else {                            
                            QSharedPointer<Device> newDev;
    #ifdef LQMEAS_DEV_E502
                            if (devname == E502TypeInfo::name()) {
                                newDev = QSharedPointer<Device>(new E502{*prec});
                            }
    #endif
#ifdef LQMEAS_DEV_L502
                            if (devname == E16TypeInfo::name()) {
                                newDev = QSharedPointer<Device>(new E16{*prec});
                            }
#endif
    #ifdef LQMEAS_DEV_L502
                            if (devname == L502TypeInfo::name()) {
                                newDev = QSharedPointer<Device>(new L502{*prec});
                            }
    #endif
                            if (newDev) {
                                newDevices.append(newDev);
                            } else {
                                X502_FreeDevRecordList(prec,1);
                            }
                        }
                    }
                }
            }
        }

#if defined LQMEAS_DEV_E502 || defined LQMEAS_DEV_E16
        QList<QSharedPointer<DeviceNetSvcRecord> > netRecs {m_netBrowser.records()};

        for (const QSharedPointer<DeviceNetSvcRecord> &netRec : netRecs) {
            const QSharedPointer<const x502NetSvcRecord> x502rec {qSharedPointerCast<const x502NetSvcRecord>(netRec)};
            QSharedPointer<Device> fndDev;
            for (const QSharedPointer<Device> &dev : oldDevices) {
                x502NetDevice *x502net {qobject_cast<x502NetDevice*>(dev.data())};
                if (x502net && x502net->netRecord() && netRec->isEqual(*x502net->netRecord())) {
                    fndDev = dev;
                    break;
                }
            }

            if (fndDev) {
                oldDevices.removeOne(fndDev);
            } else {
                /* если есть ссылка на это устройство, то берем параметры подключения
                 * из ссылки (ссылка может переназначать таймаут подключения) */
                const x502DeviceRefNetSvc *fndRef = nullptr;
                for (const DeviceRef *ref : devRefs) {
                    if (ref->refTypeID() == x502DeviceRefNetSvc::typeRefTypeID()) {
                        const x502DeviceRefNetSvc *svcRef {static_cast<const x502DeviceRefNetSvc *>(ref)};
                        if (svcRef->checkNetSvcRecord(*x502rec)) {
                            fndRef = svcRef;
                            break;
                        }
                    }
                }

                QString devtypename {x502rec->deviceTypeName()};
#if defined LQMEAS_DEV_E502
                if ((devtypename == E502TypeInfo::name()) || (devtypename == E502TypeInfo::apiName())) {
                    fndDev = QSharedPointer<Device>{fndRef ? new E502{x502rec, *fndRef}
                                                           : new E502{x502rec, x502DeviceRefNetSvc::defaultConnectionTimeout()}};
                }
#endif
#if defined LQMEAS_DEV_E16
                if (devtypename == E16TypeInfo::name()) {
                    fndDev = QSharedPointer<Device>{fndRef ? new E16{x502rec, *fndRef}
                                                           : new E16{x502rec, x502DeviceRefNetSvc::defaultConnectionTimeout()}};
                }
#endif
            }

            if (fndDev)
                newDevices.append(fndDev);
        }
#endif

        for (const DeviceRef *ref : devRefs) {
            if (!ref->isAutoResolved()) {
#if defined LQMEAS_DEV_E502 || defined LQMEAS_DEV_E16
                if (ref->refTypeID() == x502DeviceRefIPAddr::typeRefTypeID()) {
                    const x502DeviceRefIPAddr *ipRef {static_cast<const x502DeviceRefIPAddr *>(ref)};
                    QSharedPointer<Device> fndDev;
                    for (const QSharedPointer<Device> &dev : oldDevices) {
                        if (ref->checkDevice(*dev) != DeviceRef::DevCheckResult::BadDevice) {
                            fndDev = dev;
                            break;
                        }
                    }

                    if (fndDev) {
                        oldDevices.removeOne(fndDev);
                    } else {
                        /* проверяем на всякий случай среди новых устройств, что это не
                         * повторная ссылка */
                        bool fnd_newDev {false};
                        if (!fndDev) {
                            for (const QSharedPointer<Device> &dev : newDevices) {
                                if (ref->checkDevice(*dev) != DeviceRef::DevCheckResult::BadDevice) {
                                    fnd_newDev = true;
                                    break;
                                }
                            }
                        }

                        if (!fndDev && !fnd_newDev) {
                            QString devtypename {ipRef->devInfo().type().deviceTypeName()};
#if defined LQMEAS_DEV_E502
                            if (devtypename == E502TypeInfo::name()) {
                                fndDev = QSharedPointer<Device>{new E502{*ipRef}};
                            }
#endif
#if defined LQMEAS_DEV_E16
                            if (devtypename == E16TypeInfo::name()) {
                                fndDev = QSharedPointer<Device>{new E16{*ipRef}};
                            }
#endif
                        }
                    }

                    if (fndDev)
                        newDevices.append(fndDev);
                }
    #endif
            }
        }

#ifdef Q_OS_WIN

        /* т.к. по крайней мере для Windows мы не можем определить наличие
         * устройства, подключенного по USB, если оно уже открыто, то все открытые
         * устройства добавляем в список присутствующих */
        for(const QSharedPointer<Device> &oldDev : oldDevices) {
            if (oldDev->isOpened() && (&oldDev->iface() == &DeviceInterfaces::usb())) {
                newDevices.append(oldDev);
            }
        }
#endif

        m_devs = newDevices;

        return newDevices;
    }

    x502Resolver &x502Resolver::resolver() {
        static x502Resolver resolver;
        return resolver;
    }
}

