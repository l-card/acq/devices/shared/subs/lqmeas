#include "x502TypeInfo.h"
#include "x502api.h"
#include "x502AdcConfig.h"
#include "lqmeas/devs/DeviceTypeStdSeries.h"
#include "lqmeas/units/std/Voltage.h"


static double f_ranges[] = {10.,5.,2.,1.,0.5, 0.2};

namespace LQMeas {

    const QString &x502TypeInfo::name() {
        static const QString str {QStringLiteral("x502")};
        return str;
    }
    
    const x502TypeInfo &x502TypeInfo::defaultTypeInfo() {
        static const x502TypeInfo info{X502_DEVFLAGS_BF_PRESENT | X502_DEVFLAGS_DAC_PRESENT | X502_DEVFLAGS_GAL_PRESENT};
        return info;
    }

    const DeviceTypeSeries &x502TypeInfo::deviceTypeSeries() const {
        return DeviceTypeStdSeries::unknown();
    }

    int x502TypeInfo::adcChannelsCnt() const {
        return x502AdcConfig::adc_channels_cnt;
    }

    int x502TypeInfo::adcChRangesCnt(int ch_num, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return sizeof(f_ranges)/sizeof(f_ranges[0]);
    }

    double x502TypeInfo::adcChRangeMaxVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return f_ranges[range];
    }

    double x502TypeInfo::adcChRangeMinVal(int ch_num, int range, int unit_mode_num) const {
        Q_UNUSED(ch_num) Q_UNUSED(unit_mode_num)
        return -f_ranges[range];
    }

    double x502TypeInfo::outSyncGenFreqMax() const {
        return adcFreqMax()/X502_OUT_FREQ_DIV_MIN;
    }

    int x502TypeInfo::outDacChannelsCnt() const {
        return m_dac_present ? x502OutConfig::dac_max_channels_cnt : 0;
    }

    int x502TypeInfo::outDigChannelsCnt() const {
        return x502OutConfig::dout_max_channels_cnt;
    }

    double x502TypeInfo::outDacChRangeMaxVal(int ch, int range) const {
        Q_UNUSED(ch) Q_UNUSED(range)
        return X502_DAC_RANGE;
    }

    double x502TypeInfo::outDacChRangeMinVal(int ch, int range) const {
        Q_UNUSED(ch) Q_UNUSED(range)
        return -X502_DAC_RANGE;
    }

    int x502TypeInfo::inDigChannelsCnt() const {
        return 16;
    }

    double x502TypeInfo::inDigSyncFreqMax() const     {
        return adcFreqMax();
    }

    double x502TypeInfo::extRefFreqMax() const {
        return m_gd_present ?  2000000 : 1500000;
    }

    unsigned x502TypeInfo::adcFreqDivMax() const {
        return 1024*1024;
    }

    unsigned int x502TypeInfo::outSyncGenFreqDivMax() const {
        return 1024;
    }

    const Unit &x502TypeInfo::extUSrcRangeUnit() const {
        return LQMeas::Units::Voltage::V();
    }

    QList<const DeviceTypeInfo *> x502TypeInfo::modificationList() const {
        return QList<const DeviceTypeInfo *>{};
    }

    x502TypeInfo::x502TypeInfo(unsigned devflags) {
        m_gd_present = (devflags & X502_DEVFLAGS_GD_PRESENT) ? true : false;
        m_gal_present = devflags & X502_DEVFLAGS_GAL_PRESENT ? true : false;
        m_dac_present = devflags & X502_DEVFLAGS_DAC_PRESENT ? true : false;
        m_bf_present  = ((devflags & X502_DEVFLAGS_BF_PRESENT) && !m_gd_present) ? true : false;
        m_eth_present = devflags & X502_DEVFLAGS_IFACE_SUPPORT_ETH ? true : false;
        m_industrial  = devflags & X502_DEVFLAGS_INDUSTRIAL ? true : false;
    }
}
