#include "DeviceTypeInfo.h"

namespace LQMeas {
    QString DeviceTypeInfo::deviceModificationName() const {
        return deviceTypeName();
    }
}
