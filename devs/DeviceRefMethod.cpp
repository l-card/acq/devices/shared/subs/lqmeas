#include "DeviceRefMethod.h"

namespace LQMeas {
    const DeviceRefMethod &DeviceRefMethods::unknown() {
        static const class DeviceRefMethodUnknown : public DeviceRefMethod {
            QString id() const override {return QStringLiteral("ref_method_unk");}
            QString displayName() const override {return tr("Unknown");}
        } reftype;
        return reftype;
    }

    const DeviceRefMethod &DeviceRefMethods::serialNumber() {
        static const class DeviceRefMethodSerial : public DeviceRefMethod {
            QString id() const override {return QStringLiteral("ref_method_serial");}
            QString displayName() const override {return tr("Serial Number");}
        } reftype;
        return reftype;
    }

    const DeviceRefMethod &DeviceRefMethods::ipAddr() {
        static const class DeviceRefMethodIpAdr : public DeviceRefMethod {
            QString id() const override {return QStringLiteral("ref_method_ipaddr");}
            QString displayName() const override {return tr("IP Address");}
        } reftype;
        return reftype;
    }

    const DeviceRefMethod &DeviceRefMethods::netName() {
        static const class DeviceRefMethodNetName : public DeviceRefMethod {
            QString id() const override {return QStringLiteral("ref_method_netname");}
            QString displayName() const override {return tr("Network name");}
        } reftype;
        return reftype;
    }

    const DeviceRefMethod &DeviceRefMethods::crateSlot() {
        static const class DeviceRefMethodCrateSlot : public DeviceRefMethod {
            QString id() const override {return QStringLiteral("ref_method_crate_slot");}
            QString displayName() const override {return tr("Crate slot");}
        } reftype;
        return reftype;
    }

    const QList<const DeviceRefMethod *> &DeviceRefMethods::fullList() {
        static const QList<const DeviceRefMethod *> list {
            &serialNumber(),
            &ipAddr(),
            &netName(),
            &crateSlot()
        };
        return list;
    }

    const DeviceRefMethod &DeviceRefMethods::getByID(const QString &id, const DeviceRefMethod &defaultMethod) {
        const QList<const DeviceRefMethod *> &reftypes = fullList();
        auto it = std::find_if(reftypes.constBegin(), reftypes.constEnd(),
                               [&id](auto reftype) {return reftype->id() == id;});
        return it == reftypes.constEnd() ? defaultMethod : *(*it);
    }

    bool DeviceRefMethod::operator ==(const DeviceRefMethod &other) const {
        return id() == other.id();
    }

    bool DeviceRefMethod::operator !=(const DeviceRefMethod &other) const {
        return id() != other.id();
    }
}
