#ifndef LQMEAS_DEVICEINFO_H
#define LQMEAS_DEVICEINFO_H

#include <QString>
#include "DeviceTypeInfo.h"
class QJsonObject;

namespace LQMeas {
    /* Информация о экземпляре устройства. Включает в себя серийный номер, а также
       информацию о типе устройства (или конкретной модфицикации).

       Каждый класс устройства реализует свой класс информации типа <тип устройтсва>Info,
       в который добавляется специфическая информация для конкретного экземпляра, такая
       как версии прошивок и т.п. */
    class DeviceInfo {
    public:
        DeviceInfo(const DeviceTypeInfo &type, const QString &serial);

        virtual ~DeviceInfo();

        const DeviceTypeInfo &type() const {return *m_type;}
        const QString &serial() const {return  m_serial;}

        /* признак, что обе информации относятся к одному устройству */
        virtual bool sameDevice(const DeviceInfo &info) const;

        void setSerial(const QString &serial) {m_serial = serial;}
        virtual DeviceInfo *clone() const;

        void save(QJsonObject &infoObj) const;
        void load(const QJsonObject &infoObj);

        static  void saveBaseInfo(QJsonObject &infoObj, const QString &dev_type,
                            const QString &dev_mod, const QString &dev_serial);
        static QString loadDeviceModification(const QJsonObject &infoObj);
    protected:
        DeviceInfo(const DeviceInfo &info);

        void setType(const DeviceTypeInfo *type);

        virtual void protSave(QJsonObject &infoObj) const {Q_UNUSED(infoObj)}
        virtual void protLoad(const QJsonObject &infoObj) {Q_UNUSED(infoObj)}
    private:
        const DeviceTypeInfo *m_type;
        QString m_serial;
    };
}

#endif // DEVICEINFO_H
