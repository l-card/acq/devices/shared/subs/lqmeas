#ifndef DEVICECONFIG_H
#define DEVICECONFIG_H

#include <QString>
#include <QObject>
#include <QPointer>
#include "lqmeas_config.h"

class QJsonObject;

namespace LQMeas {
    class DevAdcConfig;
    class DevAdcICPConfig;
    class DevInDigConfig;
    class DevInCntrConfig;
    class DevOutConfig;
    class DevOutSyncConfig;
    class DevSyncMarksGenConfig;
    class DevSyncMarksRecvConfig;
    class DeviceTypeInfo;
    class Device;
    class DeviceConfigIface;
#ifdef LQMEAS_DEV_VI_GROUP
    class ViConfig;
#endif


    /* Базовый класс конфигурации устройства. Классы конфигурации для
       конкретного устройства должны наследоваться от данного */
    class DeviceConfig : public QObject {
        Q_OBJECT
    public:
        DeviceConfig();

        virtual const DevAdcConfig *adcConfig() const {return nullptr;}
        virtual const DevAdcICPConfig *adcICPConfig() const {return  nullptr;}
        virtual const DevInDigConfig *inDigConfig() const {return nullptr;}
        virtual const DevInCntrConfig *inCntrConfig() const {return nullptr;}
        virtual const DevOutConfig *outConfig() const {return nullptr;}
        virtual const DevOutSyncConfig *outSyncConfig() const {return nullptr;}
        virtual const DevSyncMarksGenConfig *syncMarksGenConfig() const {return nullptr;}
        virtual const DevSyncMarksRecvConfig *syncMarksRecvConfig() const {return nullptr;}
#ifdef LQMEAS_DEV_VI_GROUP
        virtual const ViConfig *viConfig() const {return nullptr;}
#endif

        DevAdcConfig *adcConfig() {
            const DevAdcConfig *cfg = static_cast<const DeviceConfig *>(this)->adcConfig();
            return const_cast<DevAdcConfig *>(cfg);
        }

        DevAdcICPConfig *adcICPConfig() {
            const DevAdcICPConfig *cfg = static_cast<const DeviceConfig *>(this)->adcICPConfig();
            return const_cast<DevAdcICPConfig *>(cfg);
        }

        DevInDigConfig *inDigConfig() {
            const DevInDigConfig *cfg = static_cast<const DeviceConfig *>(this)->inDigConfig();
            return const_cast<DevInDigConfig *>(cfg);
        }

        DevInCntrConfig *inCntrConfig() {
            const DevInCntrConfig *cfg = static_cast<const DeviceConfig *>(this)->inCntrConfig();
            return const_cast<DevInCntrConfig *>(cfg);
        }

        DevOutConfig *outConfig() {
            const DevOutConfig *cfg = static_cast<const DeviceConfig *>(this)->outConfig();
            return const_cast<DevOutConfig *>(cfg);
        }

        DevOutSyncConfig *outSyncConfig() {
            const DevOutSyncConfig *cfg = static_cast<const DeviceConfig *>(this)->outSyncConfig();
            return const_cast<DevOutSyncConfig *>(cfg);
        }

        DevSyncMarksGenConfig *syncMarksGenConfig() {
            const DevSyncMarksGenConfig *cfg = static_cast<const DeviceConfig *>(this)->syncMarksGenConfig();
            return const_cast<DevSyncMarksGenConfig *>(cfg);
        }

        DevSyncMarksRecvConfig *syncMarksRecvConfig() {
            const DevSyncMarksRecvConfig *cfg = static_cast<const DeviceConfig *>(this)->syncMarksRecvConfig();
            return const_cast<DevSyncMarksRecvConfig *>(cfg);
        }
#ifdef LQMEAS_DEV_VI_GROUP
        virtual ViConfig *viConfig() {
            const ViConfig *cfg = static_cast<const DeviceConfig *>(this)->viConfig();
            return const_cast<ViConfig *>(cfg);
        }
#endif

        /* название устройства (или группы устройств с одним классом
           конфигурации), для которого предназначена конфигурация */
        virtual QString configDevTypeName() const = 0;
        /* создание новой копии текущей конфигурации */
        virtual DeviceConfig *clone() const = 0;

        /* сохранение параметров конфигурации в объект JSON */
        void save(QJsonObject &cfgObj) const;
        /* загрузка параметров конфигурации из объекта JSON */
        void load(const QJsonObject &cfgObj);


        /* Имя поля под которым сохраняется тип конфигурации */
        static const QString &configDevTypeKeyName();


        virtual ~DeviceConfig();

        void updateDevTypeInfo(const DeviceTypeInfo *info);
        void updateDevice(const Device *device);

        void setParentConfig(const DeviceConfig *parentCfg);

        /* оценка максимальной скорости передачи данных на ввод и на вывод в МБайт/с
         * для текущей конфигурации */
        virtual double inPeakSpeed() const;
        virtual double outPeakSpeed() const;
    Q_SIGNALS:
        void configChanged();
    public Q_SLOTS:
        /* обновление параметров. на основе установленных параметров
           рассчитываются реальные значения, которые могут быть установлены,
           и также может проверяться правильность конфигурации */
        void update();
    private Q_SLOTS:
        void onModuleIfaceChanged();
    protected:
        void notifyConfigChanged();
        void addManualConfigIface(DeviceConfigIface *iface);
        void init();

        virtual void protUpdate() {}
        virtual void protUpdateInfo(const DeviceTypeInfo *info) { Q_UNUSED(info)}
        virtual void protUpdateDevice(const Device *device) { Q_UNUSED(device) }
        virtual void protUpdateParentConfig(const DeviceConfig *parentCfg) {Q_UNUSED(parentCfg)}

        virtual void protSave(QJsonObject &cfgObj) const {Q_UNUSED(cfgObj) }
        virtual void protLoad(const QJsonObject &cfgObj) {Q_UNUSED(cfgObj) }

        void reportLoadWarning(const QString &msg) const;
    private:
        const QList<DeviceConfigIface *> &cfgIfaces() const {return m_cfgIfaces;}

        QList<DeviceConfigIface *> m_cfgIfaces;
        QPointer<const DeviceConfig> m_parentConfig;
        bool m_load_progr {false};
    };
}

#endif // DEVICECONFIG_H
