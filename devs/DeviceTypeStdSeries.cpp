#include "DeviceTypeStdSeries.h"

namespace LQMeas {
    const DeviceTypeSeries &DeviceTypeStdSeries::unknown() {
        static const class DeviceTypeSeriesUnknown : public DeviceTypeSeries {
            QString id() const override {return QStringLiteral("devser_unkn");}
            QString displayName() const override { return tr("Unspecified"); }
            QString displayFullName() const override { return tr("Unspecified"); }
        } s;
        return s;
    }

    const DeviceTypeSeries &DeviceTypeStdSeries::emodules() {
        static const class DeviceTypeSeriesEModules : public DeviceTypeSeries {
            QString id() const override {return QStringLiteral("devser_emodules");}
            QString displayName() const override { return tr("E Series"); }
            QString displayFullName() const override { return tr("External ADC/DAC modules (E Series)"); }
        } s;
        return s;
    }

    const DeviceTypeSeries &DeviceTypeStdSeries::lboards() {
        static const class DeviceTypeSeriesLBoards : public DeviceTypeSeries {
            QString id() const override {return QStringLiteral("devser_lboards");}
            QString displayName() const override { return tr("L Series"); }
            QString displayFullName() const override { return tr("PCI plug-in ADC/DAC boards (L Series)"); }
        } s;
        return s;
    }

    const DeviceTypeSeries &DeviceTypeStdSeries::ltr() {
        static const class DeviceTypeSeriesLTR : public DeviceTypeSeries {
            QString id() const override {return QStringLiteral("devser_ltr");}
            QString displayName() const override { return tr("LTR"); }
            QString displayFullName() const override { return tr("LTR Rack System"); }
        } s;
        return s;
    }

    const DeviceTypeSeries &DeviceTypeStdSeries::lvims() {
        static const class DeviceTypeSeriesLViMS : public DeviceTypeSeries {
            QString id() const override {return QStringLiteral("devser_lvims");}
            QString displayName() const override { return tr("L-ViMS"); }
            QString displayFullName() const override { return tr("L-ViMS System"); }
        } s;
        return s;
    }

    const DeviceTypeSeries &DeviceTypeStdSeries::vib4() {
        static const class DeviceTypeSeriesVIB4 : public DeviceTypeSeries {
            QString id() const override {return QStringLiteral("devser_vib4");}
            QString displayName() const override { return tr("VIB-4"); }
            QString displayFullName() const override { return tr("VIB-4 System"); }
        } s;
        return s;
    }

}
