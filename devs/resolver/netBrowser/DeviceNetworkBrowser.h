#ifndef LQMEAS_DEVICENETWORKBROWSER_H
#define LQMEAS_DEVICENETWORKBROWSER_H


#include <QSharedPointer>
#include <QList>
#include "DeviceNetSvcRecord.h"
#include <QMutex>
class LQError;

namespace LQMeas {
    class DeviceNetworkBrowser : public QObject {
        Q_OBJECT
    public:
        enum class Event {
            Add,
            Change,
            Removed
        };

        QList<QSharedPointer<DeviceNetSvcRecord> >  records() const;
    public Q_SLOTS:
        void stopRequest();
        bool stopWiat(unsigned long time = ULONG_MAX);
        bool stop(unsigned long time = ULONG_MAX);
        void start();
    Q_SIGNALS:
        void recordEvent(const QSharedPointer<LQMeas::DeviceNetSvcRecord> &rec, LQMeas::DeviceNetworkBrowser::Event evnt);
        void error(const LQError err, const QString msg);
        void startPorcessingRequest();

    protected:
        DeviceNetworkBrowser();
        ~DeviceNetworkBrowser() override;
        bool stopIsRequested() const {return m_stop_requested;}

        virtual void run() = 0;

        void addRecord(const QSharedPointer<DeviceNetSvcRecord> &svcRec);
        void changeRecord(const QSharedPointer<DeviceNetSvcRecord> &svcRec);
        void removeRecord(const QSharedPointer<DeviceNetSvcRecord> &svcRec);
        void setError(const LQError &err, const QString &msg);
    private Q_SLOTS:
        void startPorcessing();
    private:


        mutable QMutex m_recMutex;
        QList<QSharedPointer<DeviceNetSvcRecord> > m_records;
        bool m_stop_requested {false};
        QThread *m_thread {nullptr};
    };
}

#endif // LQMEAS_DEVICENETWORKBROWSER_H
