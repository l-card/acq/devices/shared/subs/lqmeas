#include "DeviceNetworkBrowser.h"
#include <QThread>
#include <QCoreApplication>
#include "lqmeas/log/Log.h"
#include "lqmeas/misc/LQTypeReg.h"

namespace LQMeas {

    QList<QSharedPointer<DeviceNetSvcRecord> > DeviceNetworkBrowser::records() const {
        QMutexLocker lock{&m_recMutex};
        return m_records;
    }

    void DeviceNetworkBrowser::stopRequest() {
        if (m_thread) {
            m_stop_requested = true;
            m_thread->quit();
        }
    }

    bool DeviceNetworkBrowser::stopWiat(unsigned long time) {
        bool ret = true;
        if (m_thread) {
            ret = m_thread->wait(time);
            if (ret) {
                delete m_thread;
                m_thread = nullptr;
            }
        }
        return ret;

    }

    bool DeviceNetworkBrowser::stop(unsigned long time) {
        bool ret {true};
        if (m_thread) {
            if (!stopIsRequested())
                stopRequest();
            ret =  stopWiat(time);
        }
        return  ret;
    }

    void DeviceNetworkBrowser::start() {
        m_thread = new QThread();
        m_thread->start();
        moveToThread(m_thread);
        m_stop_requested = false;

        Q_EMIT startPorcessingRequest();
    }

    DeviceNetworkBrowser::DeviceNetworkBrowser() {

        LQTYPE_REGISTER(LQError);
        LQTYPE_REGISTER(QSharedPointer<LQMeas::DeviceNetSvcRecord>);
        LQTYPE_REGISTER(LQMeas::DeviceNetworkBrowser::Event);

        connect(this, &DeviceNetworkBrowser::startPorcessingRequest, this, &DeviceNetworkBrowser::startPorcessing, Qt::QueuedConnection);
    }

    DeviceNetworkBrowser::~DeviceNetworkBrowser() {
        stop();
    }

    void DeviceNetworkBrowser::addRecord(const QSharedPointer<DeviceNetSvcRecord> &svcRec) {
        m_recMutex.lock();
        m_records.append(svcRec);
        m_recMutex.unlock();
        Q_EMIT recordEvent(svcRec, Event::Add);
    }

    void DeviceNetworkBrowser::changeRecord(const QSharedPointer<DeviceNetSvcRecord> &svcRec) {
        for (int i = 0; i < m_records.size(); ++i) {
            if (svcRec->isEqual(*m_records[i])) {
                m_recMutex.lock();
                m_records[i] = svcRec;
                m_recMutex.unlock();
                Q_EMIT recordEvent(svcRec, Event::Change);
                break;
            }
        }
    }

    void DeviceNetworkBrowser::removeRecord(const QSharedPointer<DeviceNetSvcRecord> &svcRec) {
        for (int i = 0; i < m_records.size(); ++i) {
            if (svcRec->isEqual(*m_records[i])) {
                m_recMutex.lock();
                m_records.removeAt(i);
                m_recMutex.unlock();

                Q_EMIT recordEvent(svcRec, Event::Removed);
                break;
            }
        }
    }

    void LQMeas::DeviceNetworkBrowser::setError(const LQError &err,
                                                const QString &msg) {
        LQMeasLog->error(msg, err);
        Q_EMIT error(err, msg);
    }

    void DeviceNetworkBrowser::startPorcessing() {
        run();
        moveToThread(QCoreApplication::instance()->thread());
    }

}

