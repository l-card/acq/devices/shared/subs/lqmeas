#ifndef LQMEAS_DEVICESVCRECORD_H
#define LQMEAS_DEVICESVCRECORD_H

#include <QString>
#include <QHostAddress>

namespace LQMeas {
    class DeviceNetSvcRecord {
    public:
        virtual QString instanceName() const = 0;
        virtual QString deviceTypeName() const = 0;
        virtual QString deviceSerial() const = 0;
        virtual QHostAddress hostAddress() const = 0;
        virtual bool isEqual(const DeviceNetSvcRecord &rec) const = 0;
        virtual ~DeviceNetSvcRecord();
    };
}

#endif // LQMEAS_DEVICESVCRECORD_H

