#ifndef LQMEAS_DEVICESRESOLVER_H
#define LQMEAS_DEVICESRESOLVER_H

#include <QObject>
#include <QMetaObject>
#include <QSharedPointer>
#include "lqmeas/devs/Device.h"
#include "lqmeas_config.h"

namespace LQMeas {
    class DeviceValidator;
    class DeviceRef;
    class DeviceNetworkBrowser;
    class NetAutodiscoveryServiceType;

    class DevicesResolver {
    public:
        virtual ~DevicesResolver();

        enum class ResolveFlag {
            None = 0,

            /* Флаг указывает, что ищутся не только устройства верхнего уровня,
             * но и все дочерние устройства */
            Recursive = 0x1
        };
        Q_DECLARE_FLAGS(ResolveFlags, ResolveFlag)

        /* Получение найденных устройств.
         * Возвращает все автоматически определенные устройства, а также
         * может использовать ссылки на требуемые устройства для возвращения
         * устройств, которые не могут быть определены без явного указания */
        QList<QSharedPointer<Device> > getDevList(const QList<const DeviceRef *> &devRefs = QList<const DeviceRef *>(),
                                                  ResolveFlags flags = ResolveFlag::None);
        QList<QSharedPointer<Device> > getDevList(const DeviceValidator &validator,
                                                  const QList<const DeviceRef *> &devRefs = QList<const DeviceRef *>());
        QList<QSharedPointer<Device> > getDevList(QStringList deviceNames,
                                                  const QList<const DeviceRef *> &devRefs = QList<const DeviceRef *>());

        virtual DeviceNetworkBrowser *netBrowser() {return nullptr;}
        virtual QList<const NetAutodiscoveryServiceType *> netAutoDiscSvcTypes() const {return QList<const NetAutodiscoveryServiceType *>{};}
    protected:
        virtual QList<QSharedPointer<Device> > protGetDevList(const QList<const DeviceRef *> &devRefs, ResolveFlags flags) = 0;
    };

    Q_DECLARE_OPERATORS_FOR_FLAGS(DevicesResolver::ResolveFlags)
}

#endif // LQMEAS_DEVICESRESOLVER_H
