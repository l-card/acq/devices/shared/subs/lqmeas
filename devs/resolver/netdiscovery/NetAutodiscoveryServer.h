#ifndef LQMEAS_NETAUTODISCOVERYSERVER_H
#define LQMEAS_NETAUTODISCOVERYSERVER_H

#include "qmdnsengine/cache.h"
#include "qmdnsengine/server.h"
#include <QObject>
#include <QSharedPointer>
#include <QList>
#include <QMutex>

namespace QMdnsEngine {
    class Browser;
    class Service;
}

namespace LQMeas {
    class NetAutodiscoveryRecord;
    class NetAutodiscoveryServiceType;

    class NetAutodiscoveryServer  : public QObject {
        Q_OBJECT
    public:
        explicit NetAutodiscoveryServer(QObject *parent = nullptr);

        void addService(const NetAutodiscoveryServiceType &svc);
        void addServiceList(const QList<const NetAutodiscoveryServiceType *> svcList) {
            for (const NetAutodiscoveryServiceType *svc : svcList) {
                addService(*svc);
            }
        }

        QList<QSharedPointer<NetAutodiscoveryRecord> > records() const;
    public Q_SLOTS:
        void restart();
        void stop();
    Q_SIGNALS:
        void serverStopped();
        void recordAppend(const QSharedPointer<LQMeas::NetAutodiscoveryRecord> &rec);
        void recordRemoved(const QSharedPointer<LQMeas::NetAutodiscoveryRecord> &rec);
    private Q_SLOTS:
        void onServiceAdded(const QMdnsEngine::Service &service, const QNetworkInterface &netIface);
        void onServiceUpdated(const QMdnsEngine::Service &service, const QNetworkInterface &netIface);
        void onServiceRemoved(const QMdnsEngine::Service &service);
    private:
        QSharedPointer<NetAutodiscoveryRecord> getRecord(const QMdnsEngine::Service &service) const;

        struct SvcInfo {
            SvcInfo(const NetAutodiscoveryServiceType &t, QMdnsEngine::Browser *b) :
                type{&t}, browser{b} {}

            const NetAutodiscoveryServiceType *type;
            QMdnsEngine::Browser *browser;
        };

        QMdnsEngine::Server     m_srv;
        QMdnsEngine::Cache      m_cache;


        mutable QMutex m_lock;
        QList<SvcInfo> m_sevices;
        QList<QSharedPointer<NetAutodiscoveryRecord> > m_records;
    };
}

#endif // LQMEAS_NETAUTODISCOVERYSERVER_H
