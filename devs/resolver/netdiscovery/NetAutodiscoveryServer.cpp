#include "NetAutodiscoveryServer.h"
#include "NetAutodiscoveryServiceType.h"
#include "NetAutodiscoveryRecord.h"
#include "qmdnsengine/browser.h"
#include "qmdnsengine/service.h"
#include "qmdnsengine/resolver.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/misc/LQTypeReg.h"
#include <QStringBuilder>

namespace LQMeas {
    NetAutodiscoveryServer::NetAutodiscoveryServer(QObject *parent) : QObject{parent},
        m_srv{this}, m_cache{this} {

        LQTYPE_REGISTER(QSharedPointer<LQMeas::NetAutodiscoveryRecord>);
    }

    void NetAutodiscoveryServer::addService(const NetAutodiscoveryServiceType &svc) {
        SvcInfo info{svc, new QMdnsEngine::Browser{&m_srv, svc.fullLocalName().toUtf8(), &m_cache, this}};
        connect(info.browser, &QMdnsEngine::Browser::serviceAdded, this, &NetAutodiscoveryServer::onServiceAdded);
        connect(info.browser, &QMdnsEngine::Browser::serviceUpdated, this, &NetAutodiscoveryServer::onServiceUpdated);
        connect(info.browser, &QMdnsEngine::Browser::serviceRemoved, this, &NetAutodiscoveryServer::onServiceRemoved);
        m_sevices.append(info);
    }


    QList<QSharedPointer<NetAutodiscoveryRecord> > NetAutodiscoveryServer::records() const {
        QMutexLocker lock{&m_lock};
        return m_records;
    }

    void NetAutodiscoveryServer::restart() {
        for (const SvcInfo &svcInfo : qAsConst(m_sevices)) {
            svcInfo.browser->restart();
        }
    }

    void NetAutodiscoveryServer::stop() {
        m_srv.stop();
        m_cache.stop();
        for (const SvcInfo &svcInfo : qAsConst(m_sevices)) {
            svcInfo.browser->stop();
        }
        Q_EMIT serverStopped();
    }

    void NetAutodiscoveryServer::onServiceAdded(const QMdnsEngine::Service &service, const QNetworkInterface &netIface) {
        const QString &typeName {service.type()};
        const auto it {std::find_if(m_sevices.cbegin(), m_sevices.cend(),
                                    [&typeName](const SvcInfo &info)
                                    {return info.type->fullLocalName() == typeName;})};
        if (it != m_sevices.cend()) {
            QSharedPointer<NetAutodiscoveryRecord>  rec {new NetAutodiscoveryRecord{*it->type, service, netIface}};
            {
                QMutexLocker lock{&m_lock};
                m_records.append(rec);
            }

            Q_EMIT recordAppend(rec);
            /* Resolver должен быть очищен в потоке сервера, поэтому не можем сделать его родителем
             * NetAutodiscoveryRecord, поэтому удаление его делаем через связь сигнала удаления record.
             * Также по останову сервера можем удалить resolver, т.к. после этого поток сервера будет завершен */
            QMdnsEngine::Resolver *rs{new QMdnsEngine::Resolver{&m_srv, service.hostname(), &m_cache}};
            connect(rec.data(), &NetAutodiscoveryRecord::destroyed, rs, &QMdnsEngine::Resolver::deleteLater);
            connect(this, &NetAutodiscoveryServer::serverStopped, rs, &QMdnsEngine::Resolver::deleteLater);
            connect(rs, &QMdnsEngine::Resolver::resolved, rec.data(), &NetAutodiscoveryRecord::updateHostAddress);
        }
    }

    void NetAutodiscoveryServer::onServiceUpdated(const QMdnsEngine::Service &service, const QNetworkInterface &netIface) {
        QSharedPointer<NetAutodiscoveryRecord> rec {getRecord(service)};
        if (rec) {
            rec->updateData(service, netIface);
        }
    }

    void NetAutodiscoveryServer::onServiceRemoved(const QMdnsEngine::Service &service) {
        QSharedPointer<NetAutodiscoveryRecord> rec {getRecord(service)};
        if (rec) {
            Q_EMIT recordRemoved(rec);
            {
                QMutexLocker lock{&m_lock};
                m_records.removeOne(rec);
            }
        }
    }

    QSharedPointer<NetAutodiscoveryRecord> NetAutodiscoveryServer::getRecord(const QMdnsEngine::Service &service) const {
        const QString &typeName {service.type()};
        const QString &instName {service.name()};


         auto it {std::find_if(m_records.cbegin(), m_records.cend(),
                                    [&typeName, &instName](const QSharedPointer<NetAutodiscoveryRecord> &rec)
                                    {return (rec->instanceName() == instName) && (rec->serviceType().fullLocalName() == typeName);})};
        return (it != m_records.cend()) ? *it : QSharedPointer<NetAutodiscoveryRecord>{};
    }
}
