#include "NetAutodiscoveryServiceType.h"
#include <QStringBuilder>

namespace LQMeas {
    NetAutodiscoveryServiceType::~NetAutodiscoveryServiceType() {

    }

    const QString NetAutodiscoveryServiceType::fullLocalName() const {
        return name() % QStringLiteral(".local.");
    }
}
