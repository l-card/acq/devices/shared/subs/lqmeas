#ifndef LQMEAS_NETAUTODISCOVERYSERVICETYPE_H
#define LQMEAS_NETAUTODISCOVERYSERVICETYPE_H

#include <QString>


class QObject;
namespace QMdnsEngine {
    class Service;
}

namespace LQMeas {
    class NetAutodiscoveryRecordInfo;

    class NetAutodiscoveryServiceType {
    public:
        virtual ~NetAutodiscoveryServiceType();
        virtual const QString name() const = 0;
        virtual NetAutodiscoveryRecordInfo *createRecord() const = 0;
        const QString fullLocalName() const;
    };
}

#endif // NETAUTODISCOVERYSERVICETYPE_H
