#ifndef LQMEAS_NETAUTODISCOVERYRECORDINFO_H
#define LQMEAS_NETAUTODISCOVERYRECORDINFO_H

#include <QHostAddress>
#include <QNetworkInterface>
#include <QString>

namespace QMdnsEngine {
    class Service;
}

namespace LQMeas {
    class NetAutodiscoveryServiceType;

    class NetAutodiscoveryRecordInfo {
    public:
        NetAutodiscoveryRecordInfo();
        NetAutodiscoveryRecordInfo(const NetAutodiscoveryRecordInfo &other);
        virtual ~NetAutodiscoveryRecordInfo();


        virtual NetAutodiscoveryRecordInfo *clone() const = 0;
        virtual const NetAutodiscoveryServiceType &serviceType() const = 0;

        void updateInfo(const QMdnsEngine::Service &service, const QNetworkInterface &netIface);

        void setIPv4Addr(const QHostAddress &addr);

        const QNetworkInterface &networkIface() const {return m_netIface;}
        const QHostAddress &ipv4Addr() const {return m_ipv4_addr;}
        const QString &hostname() const {return m_hostname; }
        const QString &instanceName() const {return m_instanceName;}
        quint16 port() const {return m_port;}
    protected:
        virtual void fillTypeSpecInfo(const QMap<QByteArray, QByteArray> &attributes) = 0;
    private:
        QString m_hostname;
        QString m_instanceName;        
        QHostAddress m_ipv4_addr;
        quint16 m_port;
        QNetworkInterface m_netIface;
    };
}

#endif // LQMEAS_NETAUTODISCOVERYRECORDINFO_H
