#include "NetAutodiscoveryServerList.h"
#include "lqmeas/lqtdefs.h"

namespace LQMeas {
    NetAutodiscoveryServerList::NetAutodiscoveryServerList() {

    }

    NetAutodiscoveryServerList &NetAutodiscoveryServerList::instance() {
        static NetAutodiscoveryServerList srv;
        return srv;
    }

    void NetAutodiscoveryServerList::addServer(const QSharedPointer<NetAutodiscoveryServer> &srv) {
        m_servers += srv;
        /* Рестарт и останов выполняется по сигналам, т.к. сервер может быть в отдельном потоке и
         * эти операции должны выполняться в потоке серевера */
        connect(this, &NetAutodiscoveryServerList::restartRequested, srv.data(), &NetAutodiscoveryServer::restart);
        connect(this, &NetAutodiscoveryServerList::stopRequested, srv.data(), &NetAutodiscoveryServer::stop);
    }

    QList<QSharedPointer<NetAutodiscoveryRecord> > NetAutodiscoveryServerList::records() const {
        QList<QSharedPointer<NetAutodiscoveryRecord> > ret;
        for (const QSharedPointer<NetAutodiscoveryServer> &srv : m_servers) {
            ret += srv->records();
        }
        return ret;
    }

    void NetAutodiscoveryServerList::restart() {
        Q_EMIT restartRequested();
    }

    void NetAutodiscoveryServerList::stop() {
        Q_EMIT stopRequested();
    }
}
