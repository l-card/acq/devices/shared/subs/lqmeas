#ifndef LQMEAS_NETAUTODISCOVERYRECORD_H
#define LQMEAS_NETAUTODISCOVERYRECORD_H

#include <QObject>
#include <QMutex>

class QHostAddress;
class QNetworkInterface;

namespace QMdnsEngine {
    class Service;
}

namespace LQMeas {
    class NetAutodiscoveryServiceType;
    class NetAutodiscoveryRecordInfo;

    class NetAutodiscoveryRecord : public QObject {
        Q_OBJECT
    public:
        explicit NetAutodiscoveryRecord(const NetAutodiscoveryServiceType &type,
                                        const QMdnsEngine::Service &service,
                                        const QNetworkInterface &netIface,
                                        QObject *parent = nullptr);
        ~NetAutodiscoveryRecord() override;

        const NetAutodiscoveryServiceType &serviceType() const {return *m_svc_type;}
        const QString &instanceName() const {return  m_instanceName;}

        const NetAutodiscoveryRecordInfo &takeInfo() const;
        void releaseInfo();
        NetAutodiscoveryRecordInfo *cloneInfo() const;

    Q_SIGNALS:
        void dataChanged();
        void hostAddressChanged();
    private Q_SLOTS:
        void updateHostAddress(const QHostAddress &addr);
    private:
        void updateData(const QMdnsEngine::Service &service, const QNetworkInterface &netIface);

        mutable QMutex m_lock;
        friend class NetAutodiscoveryServer;
        QString m_instanceName;
        const NetAutodiscoveryServiceType *m_svc_type;
        NetAutodiscoveryRecordInfo *m_info;
    };
}

#endif // NETAUTODISCOVERYRECORD_H
