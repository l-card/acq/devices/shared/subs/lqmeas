#ifndef NETAUTODISCOVERYSERVERLIST_H
#define NETAUTODISCOVERYSERVERLIST_H

#include <QObject>
#include <QList>
#include <QSharedPointer>
#include "NetAutodiscoveryServer.h"

namespace LQMeas {
    class NetAutodiscoveryServerList : public QObject {
        Q_OBJECT
    public:
        static NetAutodiscoveryServerList &instance();

        void addServer(const QSharedPointer <NetAutodiscoveryServer> &srv);

        QList<QSharedPointer<NetAutodiscoveryRecord> > records() const;
        const QList< QSharedPointer <NetAutodiscoveryServer> > &servers() const {return m_servers;}
    public Q_SLOTS:
        void restart();
        void stop();
    Q_SIGNALS:
        void restartRequested();
        void stopRequested();
    private:
        NetAutodiscoveryServerList();

        QList< QSharedPointer <NetAutodiscoveryServer> > m_servers;
    };
}

#endif // NETAUTODISCOVERYSERVERLIST_H
