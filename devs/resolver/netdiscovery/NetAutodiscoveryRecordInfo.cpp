#include "NetAutodiscoveryRecordInfo.h"
#include "qmdnsengine/service.h"

namespace LQMeas {
    NetAutodiscoveryRecordInfo::NetAutodiscoveryRecordInfo() {

    }

    NetAutodiscoveryRecordInfo::NetAutodiscoveryRecordInfo(const NetAutodiscoveryRecordInfo &other) :
        m_hostname{other.m_hostname},
        m_instanceName{other.m_instanceName},
        m_ipv4_addr{other.m_ipv4_addr},
        m_port{other.m_port},
        m_netIface{other.m_netIface} {

    }

    NetAutodiscoveryRecordInfo::~NetAutodiscoveryRecordInfo() {

    }

    void NetAutodiscoveryRecordInfo::updateInfo(const QMdnsEngine::Service &service, const QNetworkInterface &netIface) {
        m_hostname = service.hostname();
        m_instanceName = service.name();
        m_port = service.port();
        if (netIface.isValid())
            m_netIface = netIface;
        fillTypeSpecInfo(service.attributes());
    }

    void NetAutodiscoveryRecordInfo::setIPv4Addr(const QHostAddress &addr) {
        m_ipv4_addr = addr;
    }
}
