#include "NetAutodiscoveryRecord.h"
#include "NetAutodiscoveryRecordInfo.h"
#include "NetAutodiscoveryServiceType.h"
#include <QHostAddress>

namespace LQMeas {
    NetAutodiscoveryRecord::NetAutodiscoveryRecord(
            const NetAutodiscoveryServiceType &type,
            const QMdnsEngine::Service &service,
            const QNetworkInterface &netIface,
            QObject *parent) :
        QObject{parent},
        m_svc_type{&type},
        m_info{type.createRecord()} {

        m_info->updateInfo(service, netIface);

        m_instanceName = m_info->instanceName();
    }

    NetAutodiscoveryRecord::~NetAutodiscoveryRecord() {
        delete  m_info;
    }

    const NetAutodiscoveryRecordInfo &NetAutodiscoveryRecord::takeInfo() const {
        m_lock.lock();
        return *m_info;
    }

    void NetAutodiscoveryRecord::releaseInfo() {
        m_lock.unlock();
    }

    NetAutodiscoveryRecordInfo *NetAutodiscoveryRecord::cloneInfo() const {
        QMutexLocker lock{&m_lock};
        return m_info->clone();
    }


    void NetAutodiscoveryRecord::updateHostAddress(const QHostAddress &addr) {
        if (addr.protocol() == QAbstractSocket::NetworkLayerProtocol::IPv4Protocol) {
            {
                QMutexLocker lock{&m_lock};
                m_info->setIPv4Addr(addr);
            }

            Q_EMIT hostAddressChanged();
            Q_EMIT dataChanged();
        }
    }

    void NetAutodiscoveryRecord::updateData(const QMdnsEngine::Service &service,
                                            const QNetworkInterface &netIface) {
        {
            QMutexLocker lock{&m_lock};
            m_info->updateInfo(service, netIface);
        }
        Q_EMIT dataChanged();
    }
}
