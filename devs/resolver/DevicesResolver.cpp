#include "DevicesResolver.h"
#include "lqmeas/devs/DeviceValidator.h"
#include "lqmeas/devs/DeviceNameValidator.h"
#include "lqmeas/lqtdefs.h"

namespace LQMeas {
    DevicesResolver::~DevicesResolver() {

    }

    QList<QSharedPointer<Device> > DevicesResolver::getDevList(const QList<const DeviceRef *> &devRefs, ResolveFlags flags) {
        return  protGetDevList(devRefs, flags);
    }

    QList<QSharedPointer<Device> > DevicesResolver::getDevList(const DeviceValidator &validator, const QList<const DeviceRef *> &devRefs) {
        QList<QSharedPointer<Device> > devlist, retlist;
        devlist = getDevList(devRefs);
        for (const QSharedPointer<Device> &dev : qAsConst(devlist)) {
            if (dev && validator.deviceValid(*dev))
                retlist.append(dev);
        }
        return retlist;
    }

    QList<QSharedPointer<Device> > DevicesResolver::getDevList(QStringList deviceNames, const QList<const DeviceRef *> &devRefs) {
        return getDevList(DeviceNameValidator{deviceNames}, devRefs);
    }
}
