#ifndef LQMEAS_DEVICE_H
#define LQMEAS_DEVICE_H

#include <QObject>
#include <QSharedPointer>
#include <QMutex>
class LQError;


namespace LQMeas {
    class DeviceInterface;
    class DeviceInfo;
    class DeviceTypeInfo;
    class DeviceConfig;
    class DevicesResolver;
    class DeviceRef;

    class DevAdc;
    class DevAdcTare;
    class DevInAsyncDig;
    class DevInSyncDig;
    class DevInCntr;
    class DevInFrameReq;    
    class DevOutInfo;
    class DevOutSync;
    class DevOutSyncEchoTracker;
    class DevOutAsyncDac;
    class DevOutAsyncDig;
    class DevOutDacConverter;
    class DevTedsAccess;
    class DevSyncMarksGen;
    class DevSyncMarksRecv;
    class DevFwLoader;

    /* Базовый класс устройства, от которого наследуются остальные модули.

       Сам класс не должен создаваться вручную, а его должны создавать
       соответствующие классы поиска устройств, унаследованные от DevicesResolver.

       Включает в себя:
          - информацию о устройстве (объединенную в отдельный класс DeviceInfo)
          - методы для открытия/закрытия связи с устройством
          - методы для конфигурации устройства (см. ниже)
          - методы для получения указателей на поддерживаемые устройством общие программные
            интерфейсы

       Информация о устройстве резделена на два класса:
          - DeviceTypeInfo - включает информацию о поддерживаемых возможностях
                             самого типа или его определенной модификации. Эти
                             свойства являются постоянными для данной модификации модуля.
          - DeviceInfo     - информация о конкретном экземпляре устройства
                             (серийный номер, версии прошивок и т.д.).
       Если информация не действительна до открытия устройства, то сперва создается
            информация с пустыми параметрами устройства и типом по умолчанию, который
            потом уже заменяется на действительную информацию (как правило после открытия
            первого соединения).
       Также сам класс устройства содержит информацию о подключении, такую как
            интерфейс подключения.

       Конфигурация устройств.
          Для установки конфигурации устройства используется метод configure,
          в который передается указатель на конфигурацию. По этому методу
          вся конфигурация записывается в устройство и при успехе обновляются
          и значения конфигурации в самом классе.

          Для каждого устройства (который поддерживает конфигурацию) конфигурация
          представляет собой класс <имя устройства>Config, унаследованный
          от DeviceConfig, и содержащий все настройки устройства. Также каждый
          класс конфигурации реализует загрузку и сохранение всех настроек
          в объект Json.

          Текущую конфигурацию устройства можно получить с помощью метода
          currentConfig() (в виде базового класса DeviceConfig). Также для
          удобства классы конкретных устройств реализуют метод devspecConfig(),
          чтобы получить класс конфигурации, приведенных к специализированному
          типу.

          Изменение полученного таким образом класса конфигурации невозможно
          напрямую.
          Для изменения следует создать копию (либо с помощью метода clone(),
          либо с помощью конструктора копирования), изменить ее параметры и
          заново вызвать configure().

    ***************************************************************************/
    class Device : public QObject {
        Q_OBJECT
    public:
       /* статус выполнения операции устройством */
        enum class OpStatus {
            Start,
            Finish,
            Error,
            Progress
        };

        enum class OpenFlag {
            None  = 0x00000000,
            /* установить соединение, оставив его открытым если есть возможность, даже
             * если при установке возникли ошибки */
            Force = 0x00000001
        };
        Q_DECLARE_FLAGS(OpenFlags, OpenFlag)


        virtual ~Device();

        /* ------------------  Информация о устройстве -----------------------*/
        /* информация о типе устройства */

        const QSharedPointer<const DeviceInfo> &devInfo() const;
        const DeviceTypeInfo &devTypeInfo() const;

        /* название типа устройства (без модификаций) */
        QString name() const;
        /* название устройства с учетом модификаций */
        QString modificationName() const;
        /* серийный номер устройства */
        QString serial() const;
        /* аппаратный интерфейс по которому подключено устройство */
        const DeviceInterface &iface() const {return  *m_iface;}
        /* строка с дополнительной информацией о расположении устройства
           (содержимое зависит от аппаратного интерфейса) */
        virtual QString location() const {return QString();}
        /* строка для краткого отображения устройства (имя, сер. номер) */
        virtual QString devStr() const;
        /* строка для отображения устройства с учетом поля location() */
        virtual QString devStrFull() const;


        /* установлена ли связь с устройством */
        virtual bool isOpened() const = 0;
        /* проверка подключения. в отличие от isOpened(), которая только
         * проверяет был ли успешно выполнен Open(), может проверять текущее
         * состояние подключения запросом к устройству (если реализовано) */
        virtual bool checkConnection() const {return isOpened();}

        /* создание ссылки на текущее устройство */
        virtual DeviceRef *createRef() const;
        /* признак, что информация о устройстве действительна, то есть
         * получена от реального устройства, а не просто значение по умолчанию */
        virtual bool devInfoValid() const;

        /* ------------- Получение программных интерфейсов -------------*/

        /* если к устройству могут быть подключены дочерние устройства,
         *  то данный метод возвращает интерфейс для получения списка этих устройств */
        virtual DevicesResolver *childResolver() {return nullptr;}

        /* Интерфейс для генерации синхронный сигналов. Если поддержан, то должен
           быть поддержан и devOutInfo() */
        virtual DevOutSync *devOutSync() {return nullptr;}

        /* Интерфейс для отслеживания эхо сигнала от выведенных синхронных значений
         * с помощью devOutSync() */
        virtual DevOutSyncEchoTracker *devOutSyncEchoTracker() {return nullptr;}
        /* Указатель на интерфейс для асинхронного вывода на ЦАП. Если поддержан, то должен
           быть поддержан и devOutInfo() */
        virtual DevOutAsyncDac *devOutAsyncDac() {return nullptr;}
        /* Указатель на интерфейс для асинхронного вывода на цифровые линии. Если поддержан, то должен
           быть поддержан и devOutInfo() */
        virtual DevOutAsyncDig *devOutAsyncDig() {return nullptr;}
        /* Указатель на интерфейс для преобразования значений ЦАП перед выводом. Если поддержан, то должен
           быть поддержан и devOutInfo() */
        virtual DevOutDacConverter *devOutDacConverter() {return nullptr;}

        /* Указатель на интерфейс для потокового ввода данных (или 0, если не поддерживается) */
        virtual DevAdc *devAdc() {return nullptr;}
        /* Указатель на интерфейс для аппаратной тарировки АЦП */
        virtual DevAdcTare *devAdcTare() {return nullptr;}
        /* Указатель на интерфейс для асинхронного ввода состояния цифровых линий */
        virtual DevInAsyncDig *devInAsyncDig() {return nullptr;}
        /* Указатель на интерфейс для синхронного ввода состояния цифровых линий */
        virtual DevInSyncDig *devInSyncDig() {return nullptr;}
        /* Указатель на интерфейс для синхронного ввода занчений счетчика */
        virtual DevInCntr *devInCntr() {return nullptr;}
        /* Указатель на интерфейс для программного запроса выдачи кадра данных*/
        virtual DevInFrameReq *devInFrameReq() {return nullptr;}

        /* Интерфейс для доступа к датчикам каналов по интерфейсу TEDS */
        virtual DevTedsAccess *devTedsAccess() {return  nullptr;}
        /* Интерфейс для генерации синхрометок */
        virtual DevSyncMarksGen *devSyncMarksGen() {return nullptr;}
        /* Интерфейс для приема синхрометок */
        virtual DevSyncMarksRecv *devSyncMarkRecv() {return nullptr;}
        /* Интерфейс начальной загрузки прошивок в устройство */
        virtual DevFwLoader *devFwLoader() {return nullptr;}

        /* Получить класс с текущей конфигурацией устройства.
           Нельзя изменять параметры данного класса напрямую - для создания
           измененной конфигурации нужно создавать копию */
        const DeviceConfig &config() const {return *m_cfg.data();}
        QSharedPointer<const DeviceConfig > configPtr() const {return m_cfg;}

        /* установить соединение с устройством (до этого большинство функций не действительны) */
        void open(LQError &err);
        void open(OpenFlags flags, LQError &err);
        /* Закрыть соединение с устройством */
        void close(LQError &err);
        void close();
        /* Конфигурация устройства  в соответствии параметрами из указанного класса конфигурации..
         *  Если указан нулевой указатель, то будут просто повторно записаны последние установленные
         *  настройки модуля */
        void configure(const DeviceConfig &cfg, LQError &err);
    Q_SIGNALS:
        /* данным сигналом устройство может оповещать о ходе выполнения длительной
         *  операции (загрузка прошивки и т.п.).
         *  status - указывает статус выполнения операции
         *  descr    - строка с описанием выполняемой операции
         *  done    - указывает прогресс выполнения (относительно size)
         *  size       - значение прогресса, соответствующие завершению операции
         *  err         - значение ошибки, при status == opStatusError */
        void opProgress(LQMeas::Device::OpStatus status, QString descr, int done, int size,  LQError err);
        /* сигнал оповещает при изменении информации о устройстве */
        void devDescrChanged();

        void configChanged(QSharedPointer<const DeviceConfig> cfg);
    protected Q_SLOTS:
        void setParentConfig(const QSharedPointer<const DeviceConfig> &cfg);
    protected:
        virtual void protOpen(OpenFlags flags, LQError &err) = 0;
        virtual void protClose(LQError &err) = 0;
        virtual void protConfigure(const DeviceConfig &cfg, LQError &err) = 0;



        void setDeviceInfo(const DeviceInfo &info);
        void setIface(const DeviceInterface &iface)  { m_iface = &iface;}

        DeviceConfig &mutableConfig() const {return *m_cfg.data();}
        void sendOpProgess(OpStatus status, QString descr, int done, int size, const LQError &err);
        explicit Device(DeviceConfig *defaultCfg, const DeviceInfo &info,
                        const DeviceInterface &iface, QObject *parent = nullptr);
    private:
        mutable QMutex m_cfg_lock;
        mutable QMutex m_info_lock;
        QSharedPointer<DeviceConfig> m_cfg;
        QSharedPointer<const DeviceConfig> m_parentCfg;
        QSharedPointer<const DeviceInfo> m_info;
        const DeviceInterface *m_iface;

        friend class DevAdc;
    };
    Q_DECLARE_OPERATORS_FOR_FLAGS(Device::OpenFlags)
}


#endif // LQMEAS_DEVICE_H
