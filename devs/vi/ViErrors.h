#ifndef LQMEAS_VIERRORS_H
#define LQMEAS_VIERRORS_H

#include "LQError.h"
#include <QObject>

namespace LQMeas {
    class ViErrors: public QObject {
        Q_OBJECT
    public:
        static LQError devError(int code);
        static LQError protoError(int code, const QString &msg);

        static LQError connectionParameters();
        static LQError connectionCreate();
        static LQError connectionNotOpen();
        static LQError connectionLost();
        static LQError connectionReset();

        static LQError invalidCmdRespFmt();
        static LQError invalidCmdRespStatus();
        static LQError insufCmdRespDataSize();
        static LQError noActiveCfgExchange();
        static LQError noValidOpCfg();
        static LQError invalidRdCfgBlockDataSize();
        static LQError invalidRdCfgCRC();
        static LQError opcfgParseMeasTypeMismatch();
        static LQError opcfgWrCheckMismatch();
        static LQError noLiveBitChange();
        static LQError dataOverflow();
        static LQError dataRecvTout();
        static LQError acqChNotEnabled(int ch_num);
    private:
        static QString devErrorMsg(int code);
    };
}

#endif // LQMEAS_VIERRORS_H
