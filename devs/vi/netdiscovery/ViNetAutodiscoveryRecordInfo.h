#ifndef LQMEAS_VINETAUTODISCOVERYRECORDINFO_H
#define LQMEAS_VINETAUTODISCOVERYRECORDINFO_H

#include "lqmeas/devs/resolver/netdiscovery/NetAutodiscoveryRecordInfo.h"

namespace LQMeas {
    class ViNetAutodiscoveryRecordInfo : public NetAutodiscoveryRecordInfo {
    public:
        explicit ViNetAutodiscoveryRecordInfo();
        explicit ViNetAutodiscoveryRecordInfo(const ViNetAutodiscoveryRecordInfo &other);

        const QString &serial() const {return m_serial;}
        const QString &moduleName() const {return m_moduleName;}
        const QString &moduleType() const {return m_moduleType;}
        const unsigned moduleRev() const {return m_moduleRev;}
        const unsigned boardTypeId() const {return m_boardTypeId;}
        const QString &boardTypeName() const {return m_boardTypeName;}
        const QString &firmwareVersion() const {return m_fwVer;}
        const QString &mezzanineType() const {return m_mezType;}
        const unsigned mezzanineRev() const {return m_mezRev;}
        const QString &systemType() const {return m_sysType;}
        const QString &clkIdText() const {return m_clkidText;}

        unsigned moduleTypeID() const {return m_module_type_id;}
        unsigned moduleModID() const {return m_module_mod_id;}
        unsigned moduleSystemID() const {return m_module_sys_id;}
    protected:
        void fillTypeSpecInfo(const QMap<QByteArray, QByteArray> &attributes) override;
    private:
        QString m_serial;
        QString m_moduleName;
        QString m_moduleType;
        unsigned m_moduleRev {0};
        unsigned m_boardTypeId {0};
        QString m_boardTypeName;
        QString m_fwVer;
        QString m_mezType;
        unsigned m_mezRev {0};
        QString m_sysType;
        QString m_clkidText;

        unsigned m_module_type_id {0};
        unsigned m_module_mod_id  {0};
        unsigned m_module_sys_id  {0};
    };
}

#endif // LQMEAS_VINETAUTODISCOVERYRECORDINFO_H
