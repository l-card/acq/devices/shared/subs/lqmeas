#ifndef LQMEAS_VINETAUTODISCOVERYSERVICETYPE_H
#define LQMEAS_VINETAUTODISCOVERYSERVICETYPE_H

#include "lqmeas/devs/resolver/netdiscovery/NetAutodiscoveryServiceType.h"

namespace LQMeas {
    class ViNetAutodiscoveryServiceType : public NetAutodiscoveryServiceType {
    public:
        static const QString &texrecModuleNameOld();
        static const QString &texrecModuleName();
        static const QString &texrecModuleType();
        static const QString &texrecModuleRev();
        static const QString &texrecFwVersion();
        static const QString &texrecMezRev();
        static const QString &texrecMezType();
        static const QString &texrecModuleSerial();
        static const QString &texrecSysType();

        static const QString &texrecBoardTypeID();
        static const QString &texrecBoardTypeName();

        static const QString &texrecModuleTypeID();
        static const QString &texrecModuleModID();
        static const QString &texrecSystemTypeID();
        static const QString &texrecClkID();
    protected:
        ViNetAutodiscoveryServiceType();
    };
}

#endif // LQMEAS_VINETAUTODISCOVERYSERVICETYPE_H
