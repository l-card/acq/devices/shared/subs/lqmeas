#include "ViNetAutodiscoveryRecordInfo.h"
#include "ViNetAutodiscoveryServiceType.h"

namespace LQMeas {
    ViNetAutodiscoveryRecordInfo::ViNetAutodiscoveryRecordInfo() {

    }

    ViNetAutodiscoveryRecordInfo::ViNetAutodiscoveryRecordInfo(const ViNetAutodiscoveryRecordInfo &other) :
        NetAutodiscoveryRecordInfo{other},
        m_serial{other.m_serial},
        m_moduleName{other.m_moduleName},
        m_moduleType{other.m_moduleType},
        m_moduleRev{other.m_moduleRev},
        m_boardTypeId{other.m_boardTypeId},
        m_boardTypeName{other.m_boardTypeName},
        m_fwVer{other.m_fwVer},
        m_mezType{other.m_mezType},
        m_mezRev{other.m_mezRev},
        m_sysType{other.m_sysType},
        m_clkidText{other.m_clkidText},
        m_module_type_id{other.m_module_type_id},
        m_module_mod_id{other.m_module_mod_id},
        m_module_sys_id{other.m_module_sys_id} {

    }
    void ViNetAutodiscoveryRecordInfo::fillTypeSpecInfo(const QMap<QByteArray, QByteArray> &attributes) {
        for (auto it {attributes.cbegin()}; it != attributes.cend(); ++it) {
            const QString attrName {it.key()};
            const QString attrValue {it.value()};

            if ((attrName == ViNetAutodiscoveryServiceType::texrecModuleName())
                || (attrName == ViNetAutodiscoveryServiceType::texrecModuleNameOld())) {
                m_moduleName = attrValue;
            } else if (attrName == ViNetAutodiscoveryServiceType::texrecModuleType()) {
                m_moduleType = attrValue;
            } else if (attrName == ViNetAutodiscoveryServiceType::texrecModuleRev()) {
                m_moduleRev = attrValue.toUInt();
            } else if (attrName == ViNetAutodiscoveryServiceType::texrecBoardTypeID()) {
                m_boardTypeId = attrValue.toUInt();
            } else if (attrName == ViNetAutodiscoveryServiceType::texrecBoardTypeName()) {
                m_boardTypeName = attrValue;
            } else if (attrName == ViNetAutodiscoveryServiceType::texrecFwVersion()) {
                m_fwVer = attrValue;
            } else if (attrName == ViNetAutodiscoveryServiceType::texrecMezType()) {
                m_mezType = attrValue;
            } else if (attrName == ViNetAutodiscoveryServiceType::texrecMezRev()) {
                m_mezRev = attrValue.toUInt();
            } else if (attrName == ViNetAutodiscoveryServiceType::texrecModuleSerial()) {
                m_serial = attrValue;
            } else if (attrName == ViNetAutodiscoveryServiceType::texrecSysType()) {
                m_sysType = attrValue;
            } else if (attrName == ViNetAutodiscoveryServiceType::texrecClkID())  {
                m_clkidText = attrValue;
            } else if (attrName == ViNetAutodiscoveryServiceType::texrecModuleTypeID())  {
                m_module_type_id = attrValue.toUInt();
            } else if (attrName == ViNetAutodiscoveryServiceType::texrecModuleModID())  {
                m_module_mod_id = attrValue.toUInt();
            } else if (attrName == ViNetAutodiscoveryServiceType::texrecSystemTypeID())  {
                m_module_sys_id = attrValue.toUInt();
            }
        }
    }
}
