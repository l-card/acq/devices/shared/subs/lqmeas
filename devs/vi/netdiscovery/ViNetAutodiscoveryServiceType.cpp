#include "ViNetAutodiscoveryServiceType.h"
#include "lqmeas/devs/vi/protocol/vi_net_autodisc.h"


namespace LQMeas {
    const QString &ViNetAutodiscoveryServiceType::texrecModuleNameOld() {
        static const QString str {QStringLiteral("modtype")};
        return str;
    }

    const QString &ViNetAutodiscoveryServiceType::texrecModuleType() {
        static const QString str {QStringLiteral(VI_AUTODISC_TXTKEY_DEVTYPE)};
        return str;
    }

    const QString &ViNetAutodiscoveryServiceType::texrecModuleRev() {
        static const QString str {QStringLiteral(VI_AUTODISC_TXTKEY_DEVREV)};
        return str;
    }

    const QString &ViNetAutodiscoveryServiceType::texrecModuleName() {
        static const QString str {QStringLiteral(VI_AUTODISC_TXTKEY_DEVNAME)};
        return str;
    }

    const QString &ViNetAutodiscoveryServiceType::texrecFwVersion() {
        static const QString str {QStringLiteral(VI_AUTODISC_TXTKEY_FWVER)};
        return str;
    }

    const QString &ViNetAutodiscoveryServiceType::texrecMezRev() {
        static const QString str {QStringLiteral(VI_AUTODISC_TXTKEY_HWREV)};
        return str;
    }

    const QString &ViNetAutodiscoveryServiceType::texrecMezType() {
        static const QString str {QStringLiteral(VI_AUTODISC_TXTKEY_HWTYPE)};
        return str;
    }

    const QString &ViNetAutodiscoveryServiceType::texrecModuleSerial() {
        static const QString str {QStringLiteral(VI_AUTODISC_TXTKEY_SERIAL)};
        return str;
    }

    const QString &ViNetAutodiscoveryServiceType::texrecSysType() {
        static const QString str {QStringLiteral(VI_AUTODISC_TXTKEY_SYSTYPE)};
        return str;
    }

    const QString &ViNetAutodiscoveryServiceType::texrecBoardTypeID() {
        static const QString str {QStringLiteral(VI_AUTODISC_TXTKEY_BRDTYPE_ID)};
        return str;
    }

    const QString &ViNetAutodiscoveryServiceType::texrecBoardTypeName() {
        static const QString str {QStringLiteral(VI_AUTODISC_TXTKEY_BRDTYPE)};
        return str;
    }

    const QString &ViNetAutodiscoveryServiceType::texrecModuleTypeID() {
        static const QString str {QStringLiteral(VI_AUTODISC_TXTKEY_DEVTYPE_ID)};
        return str;
    }

    const QString &ViNetAutodiscoveryServiceType::texrecModuleModID() {
        static const QString str {QStringLiteral(VI_AUTODISC_TXTKEY_DEVMOD_ID)};
        return str;
    }

    const QString &ViNetAutodiscoveryServiceType::texrecSystemTypeID() {
        static const QString str {QStringLiteral(VI_AUTODISC_TXTKEY_SYSTEM_ID)};
        return str;
    }

    const QString &ViNetAutodiscoveryServiceType::texrecClkID() {
        static const QString str {QStringLiteral(VI_AUTODISC_TXTKEY_CLK_ID)};
        return str;
    }

    ViNetAutodiscoveryServiceType::ViNetAutodiscoveryServiceType() {

    }
}
