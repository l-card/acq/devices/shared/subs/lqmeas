#ifndef LQMEAS_VIIFACESETTINGS_H
#define LQMEAS_VIIFACESETTINGS_H

#include <QHostAddress>

namespace LQMeas {
    class ViIfaceSettings {
    public:
        explicit ViIfaceSettings() {}

        const QString &deviceInstanceName() const {return m_dev_instance_name;}
        void setDeviceInstanceName(const QString &name) {
            m_dev_instance_name = name;
        }

        bool autoGetIpParamsEn() const {return m_auto_ip;}
        void setAutoGetIpParamsEn(bool en) {
            m_auto_ip = en;
        }

        const QHostAddress &ipAddr() const {return m_ip_addr;}
        const QHostAddress &ipMask() const {return m_ip_mask;}
        const QHostAddress &ipGate() const {return m_gate_addr;}

        void setIpAddr(const QHostAddress &addr) {
            m_ip_addr = addr;
        }
        void setIpMask(const QHostAddress &mask) {
            m_ip_mask = mask;
        }
        void setIpGate(const QHostAddress &addr) {
            m_gate_addr = addr;
        }

        int mbPort() const {return m_mb_port;}
        void setMbPort(int port) {m_mb_port = port;}

    private:
        QString m_dev_instance_name;
        QHostAddress m_ip_addr {"192.168.0.1"};
        QHostAddress m_ip_mask {"255.255.255.0"};
        QHostAddress m_gate_addr;
        bool m_auto_ip {false};
        int m_mb_port {502};
    };
}

#endif // LQMEAS_VIIFACESETTINGS_H
