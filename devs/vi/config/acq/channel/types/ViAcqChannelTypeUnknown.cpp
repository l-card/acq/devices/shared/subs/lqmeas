#include "ViAcqChannelTypeUnknown.h"
#include "../ViAcqChannelConfig.h"
#include "lqmeas/devs/vi/config/acq/meas/groups/ViMeasGroupTypeSingle.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeUnknown.h"
#include "lqmeas/devs/vi/config/acq/sensor/types/unknown/ViAcqSensorTypeUnknown.h"


namespace LQMeas {
    const QString &LQMeas::ViAcqChannelTypeUnknown::typeId() {
        static const QString str{QStringLiteral("vi_chtype_unknown")};
        return str;
    }

    const ViAcqChannelTypeUnknown &LQMeas::ViAcqChannelTypeUnknown::type() {
        static const ViAcqChannelTypeUnknown t;
        return t;
    }

    QString LQMeas::ViAcqChannelTypeUnknown::displayName() const {
        return LQMeas::ViAcqChannelConfig::tr("Unknown");
    }

    QList<const ViAcqSensorType *> ViAcqChannelTypeUnknown::supportedSensorTypes(const ViModuleTypeInfo &minfo) const {
        static const QList<const ViAcqSensorType *> list {
            &ViAcqSensorTypeUnknown::type()
        };
        return list;
    }

    const LQMeas::ViMeasGroupType &LQMeas::ViAcqChannelTypeUnknown::measGroupUnknown() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeUnknown::type()};
        return g;
    }
}
