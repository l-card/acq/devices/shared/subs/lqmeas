#ifndef LQMEAS_VIACQCHANNELTYPEUNKNOWN_H
#define LQMEAS_VIACQCHANNELTYPEUNKNOWN_H

#include "../ViAcqChannelType.h"

namespace LQMeas {
    class ViAcqChannelTypeUnknown : public ViAcqChannelType {
    public:
        static const QString &typeId();
        static const ViAcqChannelTypeUnknown &type();

        QString id() const override {return typeId();}
        QString displayName() const override;

        QList<const ViMeasGroupType *> supportedMeasGroups(const ViModuleTypeInfo &minfo) const override {
            return QList<const ViMeasGroupType *>{};
        }
        QList<const ViMeasGroupType *> defaultMeasGroups(const ViModuleTypeInfo &minfo) const override {
            return QList<const ViMeasGroupType *>{};
        }
        QList<const ViAcqSensorType *> supportedSensorTypes(const ViModuleTypeInfo &minfo) const override;

        static const ViMeasGroupType &measGroupUnknown();
    private:
    };
}


#endif // LQMEAS_VIACQCHANNELTYPEUNKNOWN_H
