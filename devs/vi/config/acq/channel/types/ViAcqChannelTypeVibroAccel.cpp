#include "ViAcqChannelTypeVibroAccel.h"
#include "../ViAcqChannelConfig.h"
#include "lqmeas/devs/vi/config/acq/meas/groups/ViMeasGroupTypeSingle.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeVibroAccel.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeVibroAccelHf.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeVibroVelocity.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeVibroDisp.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeVoltageDC.h"
#include "lqmeas/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpAccel.h"
#include "lqmeas/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaAccel.h"
#include "lqmeas/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpVelocity.h"
#include "lqmeas/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaVelocity.h"
#include "lqmeas/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpDisp.h"
#include "lqmeas/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaDisp.h"
#include "lqmeas/devs/vi/config/acq/sensor/types/accelerometer/ViAcqSensorTypeAccelerometer.h"
#include "lqmeas/devs/vi/modules/ViModuleTypeInfo.h"

namespace LQMeas {
    const QString &ViAcqChannelTypeVibroAccel::typeId() {
        static const QString str{QStringLiteral("vi_chtype_vibro_accel")};
        return str;
    }

    const ViAcqChannelTypeVibroAccel &LQMeas::ViAcqChannelTypeVibroAccel::type() {
        static const ViAcqChannelTypeVibroAccel t;
        return t;
    }

    QString ViAcqChannelTypeVibroAccel::displayName() const {
        return ViAcqChannelConfig::tr("Vibroacceleration");
    }

    QList<const ViMeasGroupType *> ViAcqChannelTypeVibroAccel::supportedMeasGroups(const ViModuleTypeInfo &minfo) const {
        static const QList<const ViMeasGroupType *> list {
            &measGroupAccel(),
            &measGroupVel(),
            &measGroupDisp(),
            &measGroupDC(),
            &measGroupAccelHarmAmp(),
            &measGroupAccelHarmPha(),
            &measGroupVelHarmAmp(),
            &measGroupVelHarmPha(),
            &measGroupDispHarmAmp(),
            &measGroupDispHarmPha(),
        };
        static const QList<const ViMeasGroupType *> list_hf {
            &measGroupAccel(),
            &measGroupAccelHf(),
            &measGroupVel(),
            &measGroupDisp(),
            &measGroupDC(),
            &measGroupAccelHarmAmp(),
            &measGroupAccelHarmPha(),
            &measGroupVelHarmAmp(),
            &measGroupVelHarmPha(),
            &measGroupDispHarmAmp(),
            &measGroupDispHarmPha(),
        };
        return minfo.acqAdcHighFreqSupport() ? list_hf : list;
    }

    QList<const ViMeasGroupType *> ViAcqChannelTypeVibroAccel::defaultMeasGroups(const ViModuleTypeInfo &minfo) const {
        static const QList<const ViMeasGroupType *> list {
            &measGroupAccel(),
            &measGroupVel(),
        };
        return list;
    }

    QList<const ViAcqSensorType *> ViAcqChannelTypeVibroAccel::supportedSensorTypes(const ViModuleTypeInfo &minfo) const {
        static const QList<const ViAcqSensorType *> list {
            &ViAcqSensorTypeAccelerometer::type()
        };
        return list;
    }

    const ViMeasGroupType &ViAcqChannelTypeVibroAccel::measGroupAccel() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeVibroAccel::type()};
        return g;
    }

    const ViMeasGroupType &ViAcqChannelTypeVibroAccel::measGroupAccelHf() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeVibroAccelHf::type()};
        return g;
    }

    const ViMeasGroupType &ViAcqChannelTypeVibroAccel::measGroupVel() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeVibroVelocity::type()};
        return g;
    }

    const ViMeasGroupType &ViAcqChannelTypeVibroAccel::measGroupDisp() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeVibroDisp::type()};
        return g;
    }

    const ViMeasGroupType &ViAcqChannelTypeVibroAccel::measGroupDC() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeVoltageDC::type()};
        return g;
    }

    const ViMeasGroupType &ViAcqChannelTypeVibroAccel::measGroupAccelHarmAmp()  {
        static const ViMeasGroupTypeSingle g{ViMeasTypeHarmAmpAccel::type()};
        return g;
    }

    const ViMeasGroupType &ViAcqChannelTypeVibroAccel::measGroupAccelHarmPha() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeHarmPhaAccel::type()};
        return g;
    }

    const ViMeasGroupType &ViAcqChannelTypeVibroAccel::measGroupVelHarmAmp() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeHarmAmpVelocity::type()};
        return g;
    }

    const ViMeasGroupType &ViAcqChannelTypeVibroAccel::measGroupVelHarmPha() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeHarmPhaVelocity::type()};
        return g;
    }

    const ViMeasGroupType &ViAcqChannelTypeVibroAccel::measGroupDispHarmAmp() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeHarmAmpDisp::type()};
        return g;
    }

    const ViMeasGroupType &ViAcqChannelTypeVibroAccel::measGroupDispHarmPha() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeHarmPhaDisp::type()};
        return g;
    }
}

