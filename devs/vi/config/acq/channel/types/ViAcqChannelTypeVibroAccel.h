#ifndef LQMEAS_VIACQCHANNELTYPEVIBROACCEL_H
#define LQMEAS_VIACQCHANNELTYPEVIBROACCEL_H

#include "../ViAcqChannelType.h"

namespace LQMeas {
    class ViAcqChannelTypeVibroAccel : public ViAcqChannelType {
    public:
        static const QString &typeId();
        static const ViAcqChannelTypeVibroAccel &type();

        QString id() const override {return typeId();}
        QString displayName() const override;

        QList<const ViMeasGroupType *> supportedMeasGroups(const ViModuleTypeInfo &minfo) const override;
        QList<const ViMeasGroupType *> defaultMeasGroups(const ViModuleTypeInfo &minfo) const override;
        QList<const ViAcqSensorType *> supportedSensorTypes(const ViModuleTypeInfo &minfo) const override;

        static const ViMeasGroupType &measGroupAccel();
        static const ViMeasGroupType &measGroupAccelHf();
        static const ViMeasGroupType &measGroupVel();
        static const ViMeasGroupType &measGroupDisp();
        static const ViMeasGroupType &measGroupDC();
        static const ViMeasGroupType &measGroupAccelHarmAmp();
        static const ViMeasGroupType &measGroupAccelHarmPha();
        static const ViMeasGroupType &measGroupVelHarmAmp();
        static const ViMeasGroupType &measGroupVelHarmPha();
        static const ViMeasGroupType &measGroupDispHarmAmp();
        static const ViMeasGroupType &measGroupDispHarmPha();
    private:

    };
}

#endif // LQMEAS_VIACQCHANNELTYPEVIBROACCEL_H
