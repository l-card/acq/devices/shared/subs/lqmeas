#ifndef LQMEAS_VIACQCHANNELTYPERADIALVIBRATION_H
#define LQMEAS_VIACQCHANNELTYPERADIALVIBRATION_H

#include "../ViAcqChannelType.h"

namespace LQMeas {
    class ViAcqChannelTypeRadialVibration : public ViAcqChannelType {
    public:
        static const QString &typeId();
        static const ViAcqChannelTypeRadialVibration &type();

        QString id() const override {return typeId();}
        QString displayName() const override;

        QList<const ViAcqChannelType *> supportedSecChTypes() const override;

        QList<const ViMeasGroupType *> supportedMeasGroups(const ViModuleTypeInfo &minfo) const override;
        QList<const ViMeasGroupType *> defaultMeasGroups(const ViModuleTypeInfo &minfo) const override;
        QList<const ViAcqSensorType *> supportedSensorTypes(const ViModuleTypeInfo &minfo) const override;

        static const ViMeasGroupType &measGroupDisp();
        static const ViMeasGroupType &measGroupGap();
        static const ViMeasGroupType &measGroupDispHarmAmp();
        static const ViMeasGroupType &measGroupDispHarmPha();
        static const ViMeasGroupType &measGroupSmax();
    private:

    };
}

#endif // LQMEAS_VIACQCHANNELTYPERADIALVIBRATION_H
