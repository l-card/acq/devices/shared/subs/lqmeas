#include "ViAcqChannelTypeAxialPosition.h"
#include "../ViAcqChannelConfig.h"
#include "lqmeas/devs/vi/config/acq/sensor/types/proximeter/ViAcqSensorTypeProximeter.h"
#include "lqmeas/devs/vi/config/acq/meas/groups/ViMeasGroupTypeSingle.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypePosition.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeVoltageGap.h"

namespace LQMeas {
    const QString &ViAcqChannelTypeAxialPosition::typeId() {
        static const QString str{QStringLiteral("vi_chtype_axial_pos")};
        return str;
    }

    const ViAcqChannelTypeAxialPosition &ViAcqChannelTypeAxialPosition::type()     {
        static const ViAcqChannelTypeAxialPosition t;
        return t;
    }

    QString ViAcqChannelTypeAxialPosition::displayName() const {
        return ViAcqChannelConfig::tr("Axial Position");
    }

    QList<const ViMeasGroupType *> ViAcqChannelTypeAxialPosition::supportedMeasGroups(const ViModuleTypeInfo &minfo) const     {
        static const QList<const ViMeasGroupType *> list {
            &measGroupPosition(),
            &measGroupGap(),
        };
        return list;
    }

    QList<const ViMeasGroupType *> ViAcqChannelTypeAxialPosition::defaultMeasGroups(const ViModuleTypeInfo &minfo) const {
        static const QList<const ViMeasGroupType *> list {
            &measGroupPosition(),
            &measGroupGap(),
        };
        return list;
    }

    QList<const ViAcqSensorType *> ViAcqChannelTypeAxialPosition::supportedSensorTypes(const ViModuleTypeInfo &minfo) const     {
        static const QList<const ViAcqSensorType *> list {
            &ViAcqSensorTypeProximeter::type()
        };
        return list;
    }

    const ViMeasGroupType &ViAcqChannelTypeAxialPosition::measGroupPosition() {
        static const ViMeasGroupTypeSingle g{ViMeasTypePosition::type()};
        return g;
    }

    const ViMeasGroupType &ViAcqChannelTypeAxialPosition::measGroupGap() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeVoltageGap::type()};
        return g;
    }
}
