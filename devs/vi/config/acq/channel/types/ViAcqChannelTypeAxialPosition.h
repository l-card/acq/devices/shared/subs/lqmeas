#ifndef LQMEAS_VIACQCHANNELTYPEAXIALPOSITION_H
#define LQMEAS_VIACQCHANNELTYPEAXIALPOSITION_H

#include "../ViAcqChannelType.h"

namespace LQMeas {
    class ViAcqChannelTypeAxialPosition : public ViAcqChannelType {
    public:
        static const QString &typeId();
        static const ViAcqChannelTypeAxialPosition &type();

        bool supportPositionConfig() const override {return true;}

        QString id() const override {return typeId();}
        QString displayName() const override;

        QList<const ViMeasGroupType *> supportedMeasGroups(const ViModuleTypeInfo &minfo) const override;
        QList<const ViMeasGroupType *> defaultMeasGroups(const ViModuleTypeInfo &minfo) const override;
        QList<const ViAcqSensorType *> supportedSensorTypes(const ViModuleTypeInfo &minfo) const override;

        static const ViMeasGroupType &measGroupPosition();
        static const ViMeasGroupType &measGroupGap();
    };
}

#endif // LQMEAS_VIACQCHANNELTYPEAXIALPOSITION_H
