#include "ViAcqChannelTypeRadialVibration.h"
#include "../ViAcqChannelConfig.h"
#include "lqmeas/devs/vi/config/acq/meas/groups/ViMeasGroupTypeSingle.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeVibroDisp.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeVoltageGap.h"
#include "lqmeas/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpDisp.h"
#include "lqmeas/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaDisp.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeVibroSmax.h"
#include "lqmeas/devs/vi/config/acq/sensor/types/proximeter/ViAcqSensorTypeProximeter.h"

namespace LQMeas {
    const QString &ViAcqChannelTypeRadialVibration::typeId() {
        static const QString str{QStringLiteral("vi_chtype_radial_vibr")};
        return str;
    }

    const ViAcqChannelTypeRadialVibration &LQMeas::ViAcqChannelTypeRadialVibration::type() {
        static const ViAcqChannelTypeRadialVibration t;
        return t;
    }

    QString ViAcqChannelTypeRadialVibration::displayName() const {
        return ViAcqChannelConfig::tr("Radial vibration");
    }

    QList<const ViAcqChannelType *> ViAcqChannelTypeRadialVibration::supportedSecChTypes() const {
        static const QList<const ViAcqChannelType *> list {
            &ViAcqChannelTypeRadialVibration::type(),
        };
        return list;
    }

    QList<const ViMeasGroupType *> ViAcqChannelTypeRadialVibration::supportedMeasGroups(const ViModuleTypeInfo &minfo) const {
        static const QList<const ViMeasGroupType *> list {
            &measGroupDisp(),
            &measGroupGap(),
            &measGroupDispHarmAmp(),
            &measGroupDispHarmPha(),
            &measGroupSmax()
        };
        return list;
    }

    QList<const ViMeasGroupType *> ViAcqChannelTypeRadialVibration::defaultMeasGroups(const ViModuleTypeInfo &minfo) const {
        static const QList<const ViMeasGroupType *> list {
            &measGroupDisp(),
            &measGroupGap(),
        };
        return list;
    }

    QList<const ViAcqSensorType *> ViAcqChannelTypeRadialVibration::supportedSensorTypes(const ViModuleTypeInfo &minfo) const  {
        static const QList<const ViAcqSensorType *> list {
            &ViAcqSensorTypeProximeter::type()
        };
        return list;
    }

    const ViMeasGroupType &ViAcqChannelTypeRadialVibration::measGroupDisp() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeVibroDisp::type()};
        return g;
    }

    const ViMeasGroupType &ViAcqChannelTypeRadialVibration::measGroupGap() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeVoltageGap::type()};
        return g;
    }

    const ViMeasGroupType &ViAcqChannelTypeRadialVibration::measGroupDispHarmAmp() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeHarmAmpDisp::type()};
        return g;
    }

    const ViMeasGroupType &ViAcqChannelTypeRadialVibration::measGroupDispHarmPha() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeHarmPhaDisp::type()};
        return g;
    }

    const ViMeasGroupType &ViAcqChannelTypeRadialVibration::measGroupSmax() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeVibroSmax::type()};
        return g;
    }
}

