#include "ViAcqChannelTypePhaseTrigger.h"
#include "../ViAcqChannelConfig.h"
#include "lqmeas/devs/vi/config/acq/meas/groups/ViMeasGroupTypeSingle.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeRotSpeed.h"
#include "lqmeas/devs/vi/config/acq/sensor/types/proximeter/ViAcqSensorTypeProximeter.h"

namespace LQMeas {
    const QString &ViAcqChannelTypePhaseTrigger::typeId() {
        static const QString str{QStringLiteral("vi_chtype_phase_trig")};
        return str;
    }

    const ViAcqChannelTypePhaseTrigger &LQMeas::ViAcqChannelTypePhaseTrigger::type() {
        static const ViAcqChannelTypePhaseTrigger t;
        return t;
    }

    QString ViAcqChannelTypePhaseTrigger::displayName() const {
        return ViAcqChannelConfig::tr("Phase trigger");
    }

    QList<const ViMeasGroupType *> ViAcqChannelTypePhaseTrigger::supportedMeasGroups(const ViModuleTypeInfo &minfo) const {
        static const QList<const ViMeasGroupType *> list {
            &measGroupSpeed(),
        };
        return list;
    }

    QList<const ViMeasGroupType *> ViAcqChannelTypePhaseTrigger::defaultMeasGroups(const ViModuleTypeInfo &minfo) const {
        static const QList<const ViMeasGroupType *> list {
            &measGroupSpeed(),
        };
        return list;
    }

    QList<const ViAcqSensorType *> ViAcqChannelTypePhaseTrigger::supportedSensorTypes(const ViModuleTypeInfo &minfo) const    {
        static const QList<const ViAcqSensorType *> list {
            &ViAcqSensorTypeProximeter::type()
        };
        return list;
    }

    const ViMeasGroupType &ViAcqChannelTypePhaseTrigger::measGroupSpeed() {
        static const ViMeasGroupTypeSingle g{ViMeasTypeRotSpeed::type()};
        return g;
    }
}

