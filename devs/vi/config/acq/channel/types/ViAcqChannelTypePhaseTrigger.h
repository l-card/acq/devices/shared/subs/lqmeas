#ifndef LQMEAS_VIACQCHANNELTYPEPHASETRIGGER_H
#define LQMEAS_VIACQCHANNELTYPEPHASETRIGGER_H

#include "../ViAcqChannelType.h"

namespace LQMeas {
    class ViAcqChannelTypePhaseTrigger : public ViAcqChannelType {
    public:
        static const QString &typeId();
        static const ViAcqChannelTypePhaseTrigger &type();

        QString id() const override {return typeId();}
        QString displayName() const override;

        bool supportPhaseTrigCfg() const override {return true;}
        bool supportPhaseTrigAssign() const override {return false;}

        QList<const ViMeasGroupType *> supportedMeasGroups(const ViModuleTypeInfo &minfo) const override;
        QList<const ViMeasGroupType *> defaultMeasGroups(const ViModuleTypeInfo &minfo) const override;
        QList<const ViAcqSensorType *> supportedSensorTypes(const ViModuleTypeInfo &minfo) const override;

        static const ViMeasGroupType &measGroupSpeed();
    private:

    };
}
#endif // LQMEAS_VIACQCHANNELTYPEPHASETRIGGER_H
