#include "ViAcqChannelPositionConfig.h"
#include "ViAcqChannelPositionNormalDirection.h"
#include "lqmeas/lqtdefs.h"
#include "../ViAcqChannelConfig.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_zero_position    {"ZeroPos"};
    static const QLatin1String cfgkey_normal_direction {"NormalDir"};


    ViAcqChannelPositionConfig::ViAcqChannelPositionConfig(ViAcqChannelConfig &chCfg) :
        ViConfigItem{&chCfg}, m_ch{&chCfg} {

    }

    ViAcqChannelPositionConfig::ViAcqChannelPositionConfig(ViAcqChannelConfig &chCfg, const ViAcqChannelPositionConfig &origPosCfg) :
        ViConfigItem{&chCfg}, m_ch{&chCfg},
        m_zero_position{origPosCfg.m_zero_position},
        m_norm_direction{origPosCfg.m_norm_direction} {

    }

    void ViAcqChannelPositionConfig::setZeroPosition(double zpos) {
        if (!LQREAL_IS_EQUAL(m_zero_position, zpos)) {
            m_zero_position = zpos;
            notifyConfigChanged();
        }
    }

    void ViAcqChannelPositionConfig::setNormalDirection(const ViAcqChannelPositionNormalDirection &dir)     {
        if (m_norm_direction != &dir) {
            m_norm_direction = &dir;
            notifyConfigChanged();
        }
    }

    double ViAcqChannelPositionConfig::defaultZeroPosition() const {
        return 0;
    }

    const ViAcqChannelPositionNormalDirection &ViAcqChannelPositionConfig::defaultNormalDirection() {
        return ViAcqChannelPositionNormalDirections::towardProbe();
    }

    void ViAcqChannelPositionConfig::protSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_zero_position] = m_zero_position;
        cfgObj[cfgkey_normal_direction] = m_norm_direction->id();
    }

    void ViAcqChannelPositionConfig::protLoad(const QJsonObject &cfgObj) {
        m_zero_position = cfgObj[cfgkey_zero_position].toDouble(defaultZeroPosition());

        const QString &dirId {cfgObj[cfgkey_normal_direction].toString()};
        const QList<const ViAcqChannelPositionNormalDirection *> &dirs {ViAcqChannelPositionNormalDirections::all()};
        const auto &it {std::find_if(dirs.cbegin(), dirs.cend(),
                                    [&dirId](const ViAcqChannelPositionNormalDirection *dir) {
                                        return dir->id() == dirId;
                                    })};
        m_norm_direction = (it != dirs.cend()) ? *it : &defaultNormalDirection();
    }
}
