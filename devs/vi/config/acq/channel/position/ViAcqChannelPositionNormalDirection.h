#ifndef LQMEAS_VIACQCHANNELPOSITIONNORMALDIRECTION_H
#define LQMEAS_VIACQCHANNELPOSITIONNORMALDIRECTION_H


#include "lqmeas/EnumNamedValue.h"
#include <QMetaType>
#include <QObject>


namespace LQMeas {
    class ViAcqChannelPositionNormalDirection : public EnumNamedValue<QString> {
    public:

    };

    class ViAcqChannelPositionNormalDirections : public QObject {
        Q_OBJECT
    public:
        static const ViAcqChannelPositionNormalDirection &towardProbe();
        static const ViAcqChannelPositionNormalDirection &awayFromProbe();

        static const QList<const ViAcqChannelPositionNormalDirection *> &all();
    };
}

Q_DECLARE_METATYPE(const LQMeas::ViAcqChannelPositionNormalDirection *);

#endif // LQMEAS_VIACQCHANNELPOSITIONNORMALDIRECTION_H
