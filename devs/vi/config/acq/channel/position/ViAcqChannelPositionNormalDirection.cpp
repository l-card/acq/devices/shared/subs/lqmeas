#include "ViAcqChannelPositionNormalDirection.h"

namespace LQMeas {

    const ViAcqChannelPositionNormalDirection &ViAcqChannelPositionNormalDirections::towardProbe() {
        static const class ViAcqChannelPositionNormalDirectionTowardProbe : public ViAcqChannelPositionNormalDirection {
        public:
            QString id() const override {return QStringLiteral("vi_position_normal_dir_toward_probe");}
            QString displayName() const override {return tr("Toward Probe");}
        } t;
        return t;
    }

    const ViAcqChannelPositionNormalDirection &ViAcqChannelPositionNormalDirections::awayFromProbe() {
        static const class ViAcqChannelPositionNormalDirectionAwayFromProbe : public ViAcqChannelPositionNormalDirection {
        public:
            QString id() const override {return QStringLiteral("vi_position_normal_dir_away_probe");}
            QString displayName() const override {return tr("Away from Probe");}
        } t;
        return t;
    }

    const QList<const ViAcqChannelPositionNormalDirection *> &ViAcqChannelPositionNormalDirections::all() {
        static const QList<const ViAcqChannelPositionNormalDirection *> list {
            &towardProbe(),
            &awayFromProbe()
        };
        return list;
    }

}
