#ifndef LQMEAS_VIACQCHANNELPOSITIONCONFIG_H
#define LQMEAS_VIACQCHANNELPOSITIONCONFIG_H


#include "lqmeas/devs/vi/config/ViConfigItem.h"

namespace LQMeas {
    class ViAcqChannelConfig;
    class ViAcqChannelPositionNormalDirection;

    class ViAcqChannelPositionConfig : public ViConfigItem {
        Q_OBJECT
    public:
        explicit ViAcqChannelPositionConfig(ViAcqChannelConfig &chCfg);
        explicit ViAcqChannelPositionConfig(ViAcqChannelConfig &chCfg,
                                             const ViAcqChannelPositionConfig &origPosCfg);

        const ViAcqChannelConfig &ch() const {return *m_ch;}
        ViAcqChannelConfig &ch() {return *m_ch;}


        double zeroPosition() const {return m_zero_position;}
        void setZeroPosition(double zpos);

        const ViAcqChannelPositionNormalDirection &normalDirection() const {return *m_norm_direction;}
        void setNormalDirection(const ViAcqChannelPositionNormalDirection &dir);


        double defaultZeroPosition() const;
        static const ViAcqChannelPositionNormalDirection &defaultNormalDirection();
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        ViAcqChannelConfig *m_ch;

        double m_zero_position {defaultZeroPosition()};
        const ViAcqChannelPositionNormalDirection *m_norm_direction {&defaultNormalDirection()};
    };
}

#endif // LQMEAS_VIACQCHANNELPOSITIONCONFIG_H
