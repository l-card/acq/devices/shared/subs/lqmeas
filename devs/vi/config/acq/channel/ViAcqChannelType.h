#ifndef LQMEAS_VIACQCHANNELTYPE_H
#define LQMEAS_VIACQCHANNELTYPE_H

#include "lqmeas/EnumNamedValue.h"
#include <QMetaType>


namespace LQMeas {
    class ViModuleTypeInfo;
    class ViMeasGroupType;
    class ViAcqSensorType;

    class ViAcqChannelType : public EnumNamedValue<QString> {
    public:
        virtual bool supportPhaseTrigCfg() const {return false;}
        virtual bool supportPhaseTrigAssign() const {return true;}
        virtual bool supportPositionConfig() const {return false;}
        virtual QList<const ViAcqChannelType *> supportedSecChTypes() const {return QList<const ViAcqChannelType *>{};}

        virtual QList<const ViMeasGroupType *> supportedMeasGroups(const ViModuleTypeInfo &minfo) const = 0;
        virtual QList<const ViMeasGroupType *> defaultMeasGroups(const ViModuleTypeInfo &minfo) const = 0;

        virtual QList<const ViAcqSensorType *> supportedSensorTypes(const ViModuleTypeInfo &minfo) const = 0;
        const ViAcqSensorType &defaultSensorType(const ViModuleTypeInfo &minfo) const {
            return *supportedSensorTypes(minfo).at(0);
        };
    };

    class ViAcqChannelTypes {
    public:
        static QList<const ViAcqChannelType *> all();
    };
}

Q_DECLARE_METATYPE(const LQMeas::ViAcqChannelType *);

#endif // LQMEAS_VIACQCHANNELTYPE_H
