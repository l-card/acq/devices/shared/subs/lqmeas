#include "ViAcqChannelConfig.h"
#include "../ViAcqConfig.h"
#include "lqmeas/StdConfigKeys.h"
#include "types/ViAcqChannelTypeVibroAccel.h"
#include "types/ViAcqChannelTypeRadialVibration.h"
#include "types/ViAcqChannelTypePhaseTrigger.h"
#include "types/ViAcqChannelTypeAxialPosition.h"
#include "phasetrig/ViAcqChannelPhaseTrigConfig.h"
#include "position/ViAcqChannelPositionConfig.h"
#include "lqmeas/devs/vi/config/acq/meas/ViMeasConfig.h"
#include "lqmeas/devs/vi/config/acq/meas/ViMeasGroupType.h"
#include "lqmeas/devs/vi/config/acq/meas/ViMeasGroupConfig.h"
#include "lqmeas/devs/vi/config/acq/sensor/ViAcqSensorConfig.h"
#include "lqmeas/devs/vi/config/acq/sensor/ViAcqSensorType.h"
#include "lqmeas/devs/vi/config/acq/channel/types/ViAcqChannelTypeUnknown.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefEvent.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefAcqCh.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefCtl.h"
#include "lqmeas/devs/vi/config/ViConfig.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QStringBuilder>

namespace LQMeas {
    static const QLatin1String &cfgkey_ch_type                  {StdConfigKeys::type()};
    static const QLatin1String cfgkey_ch_sensor_group           {"Sensor"};
    static const QLatin1String cfgkey_ch_phasetrig_group        {"PhaseTrigCfg"};
    static const QLatin1String &cfgkey_ch_position_group        {StdConfigKeys::position()};
    static const QLatin1String cfgkey_ch_tm_evt_group           {"TmEvt"};
    static const QLatin1String cfgkey_ch_inh_evt_group          {"InhEvt"};
    static const QLatin1String cfgkey_ch_phasetrig_ref_group    {"PhaseTrigRef"};
    static const QLatin1String cfgkey_ch_sec_ch_ref_group       {"SecChRef"};
    static const QLatin1String cfgkey_ch_measgroups_arr         {"MeasGroups"};




    ViAcqChannelConfig::ViAcqChannelConfig(ViAcqConfig &acqCfg, int ch_num) :
        ViChannelConfig{acqCfg.viConfig(), &acqCfg, false},
        m_acqCfg{&acqCfg},
        m_ch_num{ch_num},
        m_tmEvtRef{new ViConfigRefEvent{}},
        m_inhEvtRef{new ViConfigRefEvent{}},
        m_phaseTrigRef{new ViConfigRefAcqCh{ViChannelPubInfoType::PhaseMarks, false, {&ViAcqChannelTypePhaseTrigger::type()}}},
        m_secChRef{new ViConfigRefAcqCh{ViChannelPubInfoType::State, false, {}, false}} {

        setType(defaultType());


        addSubItem(m_tmEvtRef.get());
        addSubItem(m_inhEvtRef.get());
        addSubItem(m_phaseTrigRef.get());
        addSubItem(m_secChRef.get());

        acqCfg.viConfig().refs().addRef(*m_tmEvtRef);
        acqCfg.viConfig().refs().addRef(*m_inhEvtRef);
        acqCfg.viConfig().refs().addRef(*m_phaseTrigRef);
        acqCfg.viConfig().refs().addRef(*m_secChRef);
    }

    ViAcqChannelConfig::ViAcqChannelConfig(ViAcqConfig &acqCfg, const ViAcqChannelConfig &origChCfg) :
        ViChannelConfig{origChCfg, acqCfg.viConfig(), &acqCfg},
        m_acqCfg{&acqCfg},
        m_ch_num{origChCfg.m_ch_num},
        m_type{origChCfg.m_type},
        m_sensor{new ViAcqSensorConfig{*this, *origChCfg.m_sensor}},
        m_phaseTrigCfg{origChCfg.m_phaseTrigCfg ?
                             new ViAcqChannelPhaseTrigConfig{*this, *origChCfg.m_phaseTrigCfg} : nullptr},
        m_posCfg{origChCfg.m_posCfg ?
                           new ViAcqChannelPositionConfig{*this, *origChCfg.m_posCfg} : nullptr},
        m_tmEvtRef{new ViConfigRefEvent{*origChCfg.m_tmEvtRef}},
        m_inhEvtRef{new ViConfigRefEvent{*origChCfg.m_inhEvtRef}},
        m_phaseTrigRef{new ViConfigRefAcqCh{*origChCfg.m_phaseTrigRef}},
        m_secChRef{new ViConfigRefAcqCh{*origChCfg.m_secChRef}} {

        addSubItem(m_sensor);
        if (m_phaseTrigCfg) {
            addSubItem(m_phaseTrigCfg);
            connect(m_phaseTrigCfg, &ViAcqChannelPhaseTrigConfig::pubEnableStateChanged, this, &ViAcqChannelConfig::pubEnableStateChanged);
        }
        if (m_posCfg) {
            addSubItem(m_posCfg);
        }

        addSubItem(m_tmEvtRef.get());
        addSubItem(m_inhEvtRef.get());
        addSubItem(m_phaseTrigRef.get());
        addSubItem(m_secChRef.get());

        acqCfg.viConfig().refs().addRef(*m_tmEvtRef);
        acqCfg.viConfig().refs().addRef(*m_inhEvtRef);
        acqCfg.viConfig().refs().addRef(*m_phaseTrigRef);
        acqCfg.viConfig().refs().addRef(*m_secChRef);

        for (ViMeasGroupConfig *origMeasGroup : origChCfg.m_measGroupList) {
            addMeasGroup(*new ViMeasGroupConfig{*this, *origMeasGroup});
        }
    }

    ViAcqChannelConfig::~ViAcqChannelConfig() {
        m_acqCfg->viConfig().refs().removeRef(*m_tmEvtRef);
        m_acqCfg->viConfig().refs().removeRef(*m_inhEvtRef);
        m_acqCfg->viConfig().refs().removeRef(*m_phaseTrigRef);
        m_acqCfg->viConfig().refs().removeRef(*m_secChRef);
    }


    QString ViAcqChannelConfig::origName() const {
        return m_type->displayName() % QChar(' ') % QString::number(channelNumber() + 1);
    }

    bool ViAcqChannelConfig::pubIsEnabled(ViChannelPubInfoType infoType) const {
        return infoType == ViChannelPubInfoType::PhaseMarks ? (m_phaseTrigCfg && m_phaseTrigCfg->pubIsEnabled())
                                                            : ViChannelConfig::pubIsEnabled(infoType);
    }


    QList<const ViAcqChannelType *> ViAcqChannelConfig::supportedTypes() const {
        static const QList<const ViAcqChannelType *> types {
            &ViAcqChannelTypeVibroAccel::type(),
            &ViAcqChannelTypeRadialVibration::type(),
            &ViAcqChannelTypePhaseTrigger::type(),
            &ViAcqChannelTypeAxialPosition::type(),
        };
        return types;
    }

    const ViAcqChannelType &ViAcqChannelConfig::defaultType() const {
        return ViAcqChannelTypeVibroAccel::type();
    }

    void ViAcqChannelConfig::setType(const ViAcqChannelType &type)  {
        if (&type != m_type) {
            m_type = &type;
            /* если тип датчика не задан или новый тип канала не поддерживает
             * текущий тип датчика, то пересоздаем датчик с типом по умолчанию
             * для канала нового типа */
            if (!m_sensor || !m_type->supportedSensorTypes(viModuleTypeInfo()).contains(&m_sensor->type())) {
                if (m_sensor) {
                    remSubItem(m_sensor);
                    m_sensor->deleteLater();
                }
                m_sensor = new ViAcqSensorConfig{*this, m_type->defaultSensorType(viModuleTypeInfo())};
                addSubItem(m_sensor);
            }
            if (m_type->supportPhaseTrigCfg()) {
                if (!m_phaseTrigCfg) {
                    m_phaseTrigCfg = new ViAcqChannelPhaseTrigConfig{*this};
                    addSubItem(m_phaseTrigCfg);
                    connect(m_phaseTrigCfg, &ViAcqChannelPhaseTrigConfig::pubEnableStateChanged, this, &ViAcqChannelConfig::pubEnableStateChanged);
                }
            } else {
                if (m_phaseTrigCfg) {
                    disconnect(m_phaseTrigCfg, &ViAcqChannelPhaseTrigConfig::pubEnableStateChanged, this, &ViAcqChannelConfig::pubEnableStateChanged);
                    remSubItem(m_phaseTrigCfg);
                    m_phaseTrigCfg->deleteLater();
                    m_phaseTrigCfg = nullptr;
                }
            }

            if (m_type->supportPositionConfig()) {
                if (!m_posCfg) {
                    m_posCfg = new ViAcqChannelPositionConfig{*this};
                    addSubItem(m_posCfg);
                }
            } else {
                if (m_posCfg) {
                    remSubItem(m_posCfg);
                    m_posCfg->deleteLater();
                    m_posCfg = nullptr;
                }
            }

            m_secChRef->setSupportedAcqChTypes(m_type->supportedSecChTypes());

            updateMeasList(true);
            notifyConfigChanged();
            Q_EMIT channelTypeChanged();
            if (customName().isEmpty()) {
                Q_EMIT displayNameChanged();
            }
        }
    }

    QList<ViMeasConfig *> ViAcqChannelConfig::measList() const {
        QList<ViMeasConfig *> ret;
        for (ViMeasGroupConfig *measGroup : m_measGroupList) {
            ret += measGroup->measList();
        }
        return ret;
    }

    ViMeasConfig *ViAcqChannelConfig::getMeas(int meas_num) const {
        QList<ViMeasConfig *> meas_list {measList()};
        const auto &it {std::find_if(meas_list.cbegin(), meas_list.cend(),
                                    [meas_num](ViMeasConfig *meas){return meas->measNum() == meas_num;})};
        return it != meas_list.cend() ? *it : nullptr;
    }

    QList<const ViMeasGroupType *> ViAcqChannelConfig::supportedMeasGroups() const {
        return viModuleTypeInfoValid() ? m_type->supportedMeasGroups(viModuleTypeInfo()) : QList<const ViMeasGroupType *>{};
    }

    QList<const ViMeasGroupType *> ViAcqChannelConfig::defaultMeasGoups() const {
        return viModuleTypeInfoValid() ? m_type->defaultMeasGroups(viModuleTypeInfo()) : QList<const ViMeasGroupType *>{};
    }

    QList<const ViMeasGroupType *> ViAcqChannelConfig::availableNewMeasGroups() const {
        QList<const ViMeasGroupType *>  ret;
        const QList<const ViMeasGroupType *> supGroupList {supportedMeasGroups()};
        for (const ViMeasGroupType *group : supGroupList) {
            if (group->isAvailable(*this)) {
                ret += group;
            }
        }
        return ret;
    }

    void ViAcqChannelConfig::removeAllMeas() {
        while (!m_measGroupList.isEmpty()) {
            removeGroupAt(m_measGroupList.size()-1);
        }
    }

    void ViAcqChannelConfig::removeMeasGroup(ViMeasGroupConfig &group) {
        const int idx {m_measGroupList.indexOf(&group)};
        if (idx >= 0) {
            removeGroupAt(idx);
            notifyConfigChanged();
        }
    }

    ViMeasGroupConfig *ViAcqChannelConfig::addMeasGroup(const ViMeasGroupType &groupType, bool force) {
        ViMeasGroupConfig *ret {nullptr};
        if (force || supportedMeasGroups().contains(&groupType)) {
            ViMeasGroupConfig *measGroup {new ViMeasGroupConfig{*this, groupType}};
            addMeasGroup(*measGroup);
            ret = measGroup;
        }
        return ret;
    }



    void ViAcqChannelConfig::chCfgSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_ch_type] = m_type->id();

        saveSubItem(cfgObj, *m_sensor, cfgkey_ch_sensor_group);
        if (m_phaseTrigCfg) {
            saveSubItem(cfgObj, *m_phaseTrigCfg, cfgkey_ch_phasetrig_group);
        }
        if (m_posCfg) {
            saveSubItem(cfgObj, *m_posCfg, cfgkey_ch_position_group);
        }
        saveSubItem(cfgObj, *m_tmEvtRef,  cfgkey_ch_tm_evt_group);
        saveSubItem(cfgObj, *m_inhEvtRef, cfgkey_ch_inh_evt_group);
        saveSubItem(cfgObj, *m_phaseTrigRef, cfgkey_ch_phasetrig_ref_group);
        saveSubItem(cfgObj, *m_secChRef, cfgkey_ch_sec_ch_ref_group);

        QJsonArray measGroupArray;
        for (const ViMeasGroupConfig *measGroup : m_measGroupList) {
            QJsonObject measGroupObj;
            measGroup->save(measGroupObj);
            measGroupArray.append(measGroupObj);
        }
        cfgObj[cfgkey_ch_measgroups_arr] = measGroupArray;
    }

    void ViAcqChannelConfig::chCfgLoad(const QJsonObject &cfgObj) {

        /* Загрузка типа канала */
        {
            const QString &typeID {cfgObj[cfgkey_ch_type].toString()};
            const QList<const ViAcqChannelType *> types {supportedTypes()};
            const auto &it {std::find_if(types.cbegin(), types.cend(),
                                        [&typeID](const ViAcqChannelType *type){return type->id() == typeID;})};
            const ViAcqChannelType *newType {it != types.cend() ? *it : &defaultType()};
            m_type = newType;
        }


        {
            /* удаляем старый объект датчика, т.к. тип датчика может поменяться и
             * все настройки все равно нужно сбрасывать */
            if (m_sensor) {
                remSubItem(m_sensor);
                m_sensor->deleteLater();
                m_sensor = nullptr;
            }

            /* загружаем сперва тип датчика и проверяем, подходит ли он конфигурации */
            QString sensorTypeID {ViAcqSensorConfig::loadSensorTypeID(cfgObj[cfgkey_ch_sensor_group].toObject())};
            QList<const ViAcqSensorType *> sensorTypes {m_type->supportedSensorTypes(viModuleTypeInfo())};
            const auto &it {std::find_if(sensorTypes.cbegin(), sensorTypes.cend(),
                                        [&sensorTypeID](const ViAcqSensorType *sensorType){return sensorType->id() == sensorTypeID;})};
            if (it != sensorTypes.cend()) {
                /* Если подходит, то заменяем объект настроек на настройки нужного типа,
                 * после чего загружаем параметры из конфигурации */
                const ViAcqSensorType *fndType = *it;

                m_sensor = new ViAcqSensorConfig{*this, *fndType};
                addSubItem(m_sensor);
                loadSubItem(cfgObj, *m_sensor, cfgkey_ch_sensor_group);
            } else {
                /* если нет настроек датчика или они не соответствуют каналу, то заменяем
                 * настройки на датчик по умолчанию для данного типа канала */
                m_sensor = new ViAcqSensorConfig{*this, m_type->defaultSensorType(viModuleTypeInfo())};
                addSubItem(m_sensor);
            }
        }


        if (m_type->supportPhaseTrigCfg()) {
            if (!m_phaseTrigCfg) {
                m_phaseTrigCfg = new ViAcqChannelPhaseTrigConfig{*this};
                addSubItem(m_phaseTrigCfg);
                connect(m_phaseTrigCfg, &ViAcqChannelPhaseTrigConfig::pubEnableStateChanged, this, &ViAcqChannelConfig::pubEnableStateChanged);
            }
            loadSubItem(cfgObj, *m_phaseTrigCfg, cfgkey_ch_phasetrig_group);
        } else if (m_phaseTrigCfg) {
            disconnect(m_phaseTrigCfg, &ViAcqChannelPhaseTrigConfig::pubEnableStateChanged, this, &ViAcqChannelConfig::pubEnableStateChanged);
            remSubItem(m_phaseTrigCfg);
            m_phaseTrigCfg->deleteLater();
            m_phaseTrigCfg = nullptr;
        }

        if (m_type->supportPositionConfig()) {
            if (!m_posCfg) {
                m_posCfg = new ViAcqChannelPositionConfig{*this};
                addSubItem(m_posCfg);
            }
            loadSubItem(cfgObj, *m_posCfg, cfgkey_ch_position_group);
        } else if (m_posCfg) {
            remSubItem(m_posCfg);
            m_posCfg->deleteLater();
            m_posCfg = nullptr;
        }

        m_secChRef->setSupportedAcqChTypes(m_type->supportedSecChTypes());

        loadSubItem(cfgObj, *m_tmEvtRef,  cfgkey_ch_tm_evt_group);
        loadSubItem(cfgObj, *m_inhEvtRef, cfgkey_ch_inh_evt_group);
        loadSubItem(cfgObj, *m_phaseTrigRef, cfgkey_ch_phasetrig_ref_group);
        loadSubItem(cfgObj, *m_secChRef, cfgkey_ch_sec_ch_ref_group);

        while (!m_measGroupList.isEmpty()) {
            removeGroupAt(m_measGroupList.size()-1);
        }

        const QJsonArray &measGroupArray {cfgObj[cfgkey_ch_measgroups_arr].toArray()};
        for (const auto &it : measGroupArray) {
            const QJsonObject &measGroupObj {it.toObject()};
            QList<const ViMeasGroupType *> allGrpups {supportedMeasGroups()};
            const QString measGroupTypeId {ViMeasGroupConfig::loadTypeId(measGroupObj)};
            const auto &groupIt {std::find_if(allGrpups.cbegin(), allGrpups.cend(),
                                            [&measGroupTypeId](const ViMeasGroupType *measGroupType){
                                                 return measGroupType->id() == measGroupTypeId;
            })};
            if (groupIt != allGrpups.cend()) {
                ViMeasGroupConfig *measGroup {new ViMeasGroupConfig{*this, **groupIt}};
                measGroup->load(measGroupObj);
                addMeasGroup(*measGroup);
            }
        }
    }

    void ViAcqChannelConfig::viModuleInfoUpdate(const ViModuleTypeInfo &viInfo) {
        updateMeasList(false);
    }

    void ViAcqChannelConfig::removeGroupAt(int rem_idx) {
        ViMeasGroupConfig *measGroup {m_measGroupList.takeAt(rem_idx)};
        remSubItem(measGroup);
        const QList<ViMeasConfig *> &remMeasList {measGroup->measList()};
        const int rem_meas_cnt {remMeasList.size()};
        if (rem_meas_cnt > 0) {
            for (ViMeasConfig *meas : remMeasList) {
                Q_EMIT measRemoved(meas);
            }
            m_meas_cnt -= rem_meas_cnt;
            /* обновление индексов всех измерений, идущих после удаленного */
            for (int meas_idx {rem_idx}; meas_idx < m_measGroupList.size(); ++meas_idx) {
                ViMeasGroupConfig *upGroup {m_measGroupList.at(meas_idx)};
                const QList<ViMeasConfig *> &upMeasList {upGroup->measList()};
                for (ViMeasConfig *meas : upMeasList) {
                    meas->setMeasNum(meas->measNum() - rem_meas_cnt);
                }
            }
        }

        measGroup->deleteLater();
    }

    void ViAcqChannelConfig::addMeasGroup(ViMeasGroupConfig &measGroupCfg)  {
        m_measGroupList += &measGroupCfg;
        addSubItem(&measGroupCfg);
        const QList<ViMeasConfig *> &measList {measGroupCfg.measList()};
        for (ViMeasConfig *meas : measList) {
            meas->setMeasNum(m_meas_cnt++);
            Q_EMIT measAppend(meas);
        }
    }

    void ViAcqChannelConfig::updateMeasList(bool create_defaults) {
        const QList<const ViMeasGroupType *>  fullList {supportedMeasGroups() + QList<const ViMeasGroupType *>{&ViAcqChannelTypeUnknown::measGroupUnknown()}};

        for (int measGroupIdx {m_measGroupList.size() - 1}; measGroupIdx >= 0; --measGroupIdx) {
            if (!fullList.contains(&m_measGroupList.at(measGroupIdx)->groupType())) {
                removeGroupAt(measGroupIdx);
            }
        }

        if (create_defaults) {
            const QList<const ViMeasGroupType *>  defList {defaultMeasGoups()};
            for (const ViMeasGroupType *defGroup : defList) {
                const auto &it {std::find_if(m_measGroupList.cbegin(), m_measGroupList.cend(),
                                            [defGroup](ViMeasGroupConfig *measGroup){
                                                return &measGroup->groupType() == defGroup;
                })};
                if (it == m_measGroupList.cend()) {
                    addMeasGroup(*defGroup);
                }
            }
        }
    }
}
