#ifndef LQMEAS_VIACQCHANNELCONFIG_H
#define LQMEAS_VIACQCHANNELCONFIG_H

#include "lqmeas/devs/vi/config/ViChannelConfig.h"
#include <memory>

namespace LQMeas {
    class ViAcqConfig;
    class ViAcqChannelType;
    class ViMeasConfig;
    class ViMeasGroupType;
    class ViMeasGroupConfig;
    class ViAcqSensorConfig;
    class ViConfigRefEvent;
    class ViConfigRefAcqCh;
    class ViAcqChannelPhaseTrigConfig;
    class ViAcqChannelPositionConfig;

    class ViAcqChannelConfig : public ViChannelConfig {
        Q_OBJECT
    public:
        explicit ViAcqChannelConfig(ViAcqConfig &acqCfg, int ch_num);
        explicit ViAcqChannelConfig(ViAcqConfig &acqCfg, const ViAcqChannelConfig &origChCfg);
        ~ViAcqChannelConfig() override;

        const ViAcqConfig &acq() const {return *m_acqCfg;}
        ViAcqConfig &acq() {return *m_acqCfg;}

        int channelNumber() const override {return m_ch_num;}
        QString origName() const override;

        bool pubIsEnabled(ViChannelPubInfoType infoType) const override;

        QList<const ViAcqChannelType *> supportedTypes() const;
        const ViAcqChannelType &defaultType() const;

        const ViAcqChannelType &type() const {return *m_type;}
        void setType(const ViAcqChannelType &type);

        const ViAcqSensorConfig &sensor() const {return *m_sensor;}
        ViAcqSensorConfig &sensor() {return *m_sensor;}

        bool hasPhaseTrigCfg() const {return m_phaseTrigCfg != nullptr;}
        const ViAcqChannelPhaseTrigConfig &phaseTrigCfg() const {return *m_phaseTrigCfg;}
        ViAcqChannelPhaseTrigConfig &phaseTrigCfg() {return *m_phaseTrigCfg;}

        bool hasPositionCfg() const {return m_posCfg != nullptr;}
        const ViAcqChannelPositionConfig &positionCfg() const {return *m_posCfg;}
        ViAcqChannelPositionConfig &positionCfg() {return *m_posCfg;}

        const ViConfigRefEvent &tmEventRef() const {return *m_tmEvtRef;}
        ViConfigRefEvent &tmEventRef() {return *m_tmEvtRef;}

        const ViConfigRefEvent &inhEventRef() const {return *m_inhEvtRef;}
        ViConfigRefEvent &inhEventRef() {return *m_inhEvtRef;}

        const ViConfigRefAcqCh &phaseTriggerRef() const {return *m_phaseTrigRef;}
        ViConfigRefAcqCh &phaseTriggerRef() {return *m_phaseTrigRef;}

        const ViConfigRefAcqCh &secChRef() const {return *m_secChRef;}
        ViConfigRefAcqCh &secChRef() {return *m_secChRef;}

        const QList<ViMeasGroupConfig *> measGroupList() const {return m_measGroupList;}
        QList<ViMeasConfig *> measList() const;
        ViMeasConfig *getMeas(int meas_num) const;


        QList<const ViMeasGroupType *> supportedMeasGroups() const;
        QList<const ViMeasGroupType *> defaultMeasGoups() const;
        QList<const ViMeasGroupType *> availableNewMeasGroups() const;

        void removeAllMeas();
        void removeMeasGroup(ViMeasGroupConfig &group);
        ViMeasGroupConfig *addMeasGroup(const ViMeasGroupType &groupType, bool force = false);


    Q_SIGNALS:
        void channelTypeChanged();
        void measAppend(LQMeas::ViMeasConfig *meas);
        void measRemoved(LQMeas::ViMeasConfig *meas);
    protected:
        void chCfgSave(QJsonObject &cfgObj) const override;
        void chCfgLoad(const QJsonObject &cfgObj) override;
        void viModuleInfoUpdate(const ViModuleTypeInfo &viInfo) override;
    private:
        void removeGroupAt(int rem_idx);
        void addMeasGroup(ViMeasGroupConfig &measGroupCfg);
        void updateMeasList(bool create_defaults);

        ViAcqConfig *m_acqCfg;
        int m_ch_num;
        int m_meas_cnt {0};
        const ViAcqChannelType *m_type {nullptr};
        ViAcqSensorConfig *m_sensor {nullptr};
        ViAcqChannelPhaseTrigConfig *m_phaseTrigCfg {nullptr};
        ViAcqChannelPositionConfig *m_posCfg {nullptr};

        std::unique_ptr<ViConfigRefEvent> m_tmEvtRef;
        std::unique_ptr<ViConfigRefEvent> m_inhEvtRef;
        std::unique_ptr<ViConfigRefAcqCh> m_phaseTrigRef;
        std::unique_ptr<ViConfigRefAcqCh> m_secChRef;

        QList<ViMeasGroupConfig *> m_measGroupList;
    };
}

#endif // LQMEAS_VIACQCHANNELCONFIG_H
