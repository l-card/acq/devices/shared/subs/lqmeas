#include "ViAcqChannelType.h"
#include "types/ViAcqChannelTypeVibroAccel.h"
#include "types/ViAcqChannelTypeRadialVibration.h"
#include "types/ViAcqChannelTypePhaseTrigger.h"
#include "types/ViAcqChannelTypeAxialPosition.h"

namespace LQMeas {

    QList<const ViAcqChannelType *> ViAcqChannelTypes::all() {
        static const QList<const ViAcqChannelType *> list {
            &ViAcqChannelTypeVibroAccel::type(),
            &ViAcqChannelTypeRadialVibration::type(),
            &ViAcqChannelTypePhaseTrigger::type(),
            &ViAcqChannelTypeAxialPosition::type(),
        };
        return list;
    }
}
