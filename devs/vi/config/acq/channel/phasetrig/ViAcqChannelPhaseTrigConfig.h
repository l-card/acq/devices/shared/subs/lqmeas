#ifndef LQMEAS_VIACQCHANNELPHASETRIGCONFIG_H
#define LQMEAS_VIACQCHANNELPHASETRIGCONFIG_H

#include "lqmeas/devs/vi/config/ViConfigItem.h"
#include "lqmeas/devs/vi/config/misc/ViConfigHystVal.h"

namespace LQMeas {
    class ViAcqChannelConfig;
    class ViAcqChannelPhaseTrigPolarity;

    class ViAcqChannelPhaseTrigConfig : public ViConfigItem {
        Q_OBJECT
    public:
        explicit ViAcqChannelPhaseTrigConfig(ViAcqChannelConfig &chCfg);
        explicit ViAcqChannelPhaseTrigConfig(ViAcqChannelConfig &chCfg,
                                             const ViAcqChannelPhaseTrigConfig &origPhaseTrigCfg);

        const ViAcqChannelConfig &ch() const {return *m_ch;}
        ViAcqChannelConfig &ch() {return *m_ch;}

        const ViConfigHystVal &threshold() const {return m_threshold;}
        ViConfigHystVal &threshold() {return m_threshold;}

        int eventsPerRevolution() const {return m_events_per_revolution;}
        void setEventsPerRevolution(int cnt);

        const ViAcqChannelPhaseTrigPolarity &polarity() const {return *m_polarity;}
        void setPolarity(const ViAcqChannelPhaseTrigPolarity &polarity);

        bool pubIsEnabled() const;
        bool pubIsSetEnabled() const {return m_pub_en;}
        void setPubIsEnabled(bool en);

        static constexpr int defaultEventsPerRevolution() {return 1;}
        static constexpr double defaultHysteresis() {return 1;}
        static constexpr bool defaultPubEnabled() {return false;}
        double defaultThreshold() const;
        static const ViAcqChannelPhaseTrigPolarity &defaultPolarity();
    Q_SIGNALS:
        void pubEnableStateChanged();
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        ViAcqChannelConfig *m_ch;

        ViConfigHystVal m_threshold;
        int m_events_per_revolution {defaultEventsPerRevolution()};
        const ViAcqChannelPhaseTrigPolarity *m_polarity {&defaultPolarity()} ;
        bool m_pub_en {defaultPubEnabled()};
    };
}

#endif // VIACQCHANNELPHASETRIGCONFIG_H
