#include "ViAcqChannelPhaseTrigPolarity.h"

namespace LQMeas {
    const ViAcqChannelPhaseTrigPolarity &ViAcqChannelPhaseTrigPolarities::projection() {
        static const class ViAcqChannelPhaseTrigPolarityProjection : public ViAcqChannelPhaseTrigPolarity {
        public:
            QString id() const override {return QStringLiteral("vi_phasetrig_pol_proj");}
            QString displayName() const override {return tr("Projection");}
        } t;
        return t;
    }

    const ViAcqChannelPhaseTrigPolarity &ViAcqChannelPhaseTrigPolarities::notch() {
        static const class ViAcqChannelPhaseTrigPolarityNotch : public ViAcqChannelPhaseTrigPolarity {
        public:
            QString id() const override {return QStringLiteral("vi_phasetrig_pol_notch");}
            QString displayName() const override {return tr("Notch");}
        } t;
        return t;
    }

    const QList<const ViAcqChannelPhaseTrigPolarity *> &ViAcqChannelPhaseTrigPolarities::all() {
        static const QList<const ViAcqChannelPhaseTrigPolarity *> list {
            &projection(),
            &notch()
        };
        return list;
    }
}
