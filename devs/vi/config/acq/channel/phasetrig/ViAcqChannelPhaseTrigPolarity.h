#ifndef LQMEAS_VIACQCHANNELPHASETRIGPOLARITY_H
#define LQMEAS_VIACQCHANNELPHASETRIGPOLARITY_H

#include "lqmeas/EnumNamedValue.h"
#include <QMetaType>
#include <QObject>


namespace LQMeas {
    class ViAcqChannelPhaseTrigPolarity : public EnumNamedValue<QString> {
    public:

    };

    class ViAcqChannelPhaseTrigPolarities : public QObject {
        Q_OBJECT
    public:
        static const ViAcqChannelPhaseTrigPolarity &projection();
        static const ViAcqChannelPhaseTrigPolarity &notch();

        static const QList<const ViAcqChannelPhaseTrigPolarity *> &all();
    };
}

Q_DECLARE_METATYPE(const LQMeas::ViAcqChannelPhaseTrigPolarity *);

#endif // LQMEAS_VIACQCHANNELPHASETRIGPOLARITY_H
