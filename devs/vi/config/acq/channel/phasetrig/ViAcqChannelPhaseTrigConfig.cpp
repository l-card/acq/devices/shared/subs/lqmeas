#include "ViAcqChannelPhaseTrigConfig.h"
#include "ViAcqChannelPhaseTrigPolarity.h"
#include "../ViAcqChannelConfig.h"
#include "../../sensor/ViAcqSensorConfig.h"
#include "../../sensor/ViAcqSensorConType.h"
#include "../../../ViConfig.h"
#include "../../../exch/ViExchConfig.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String &cfgkey_thresh           {StdConfigKeys::threshold()};
    static const QLatin1String &cfgkey_polarity         {StdConfigKeys::polarity()};
    static const QLatin1String cfgkey_events_per_rev    {"EventsPerRev"};
    static const QLatin1String cfgkey_pub_en            {"PubEn"};



    ViAcqChannelPhaseTrigConfig::ViAcqChannelPhaseTrigConfig(ViAcqChannelConfig &chCfg) :
        ViConfigItem{&chCfg}, m_ch{&chCfg} {

        m_threshold.setHysteresis(defaultHysteresis());
        m_threshold.setValue(defaultThreshold());

        addSubItem(&m_threshold);

        connect(&chCfg.viConfig().exch(), &ViExchConfig::pubEnableStateChanged, this, &ViAcqChannelPhaseTrigConfig::pubEnableStateChanged);
    }

    ViAcqChannelPhaseTrigConfig::ViAcqChannelPhaseTrigConfig(
        ViAcqChannelConfig &chCfg, const ViAcqChannelPhaseTrigConfig &origPhaseTrigCfg) :
        ViConfigItem{&chCfg}, m_ch{&chCfg},
        m_threshold{origPhaseTrigCfg.m_threshold},
        m_events_per_revolution{origPhaseTrigCfg.m_events_per_revolution},
        m_polarity{origPhaseTrigCfg.m_polarity},
        m_pub_en{origPhaseTrigCfg.m_pub_en} {

        addSubItem(&m_threshold);

        connect(&chCfg.viConfig().exch(), &ViExchConfig::pubEnableStateChanged, this, &ViAcqChannelPhaseTrigConfig::pubEnableStateChanged);
    }

    void ViAcqChannelPhaseTrigConfig::setEventsPerRevolution(int cnt) {
        if (m_events_per_revolution != cnt) {
            m_events_per_revolution = cnt;
            notifyConfigChanged();
        }
    }

    void ViAcqChannelPhaseTrigConfig::setPolarity(const ViAcqChannelPhaseTrigPolarity &polarity) {
        if (m_polarity != &polarity) {
            m_polarity = &polarity;
            notifyConfigChanged();
        }
    }

    bool ViAcqChannelPhaseTrigConfig::pubIsEnabled() const {
        return m_ch->pubIsEnabled(ViChannelPubInfoType::State) && m_pub_en;
    }

    void ViAcqChannelPhaseTrigConfig::setPubIsEnabled(bool en) {
        if (m_pub_en != en) {
            m_pub_en = en;
            Q_EMIT pubEnableStateChanged();
        }
    }


    double ViAcqChannelPhaseTrigConfig::defaultThreshold() const {
        return m_ch->sensor().defaultConnectionMethod().negativePolarity() ? -12 : 12;
    }

    const ViAcqChannelPhaseTrigPolarity &ViAcqChannelPhaseTrigConfig::defaultPolarity() {
        return ViAcqChannelPhaseTrigPolarities::projection();
    }

    void ViAcqChannelPhaseTrigConfig::protSave(QJsonObject &cfgObj) const {
        saveSubItem(cfgObj, m_threshold, cfgkey_thresh);
        cfgObj[cfgkey_events_per_rev] = m_events_per_revolution;
        cfgObj[cfgkey_polarity] = m_polarity->id();
        cfgObj[cfgkey_pub_en] = m_pub_en;
    }

    void ViAcqChannelPhaseTrigConfig::protLoad(const QJsonObject &cfgObj) {
        loadSubItem(cfgObj, m_threshold, cfgkey_thresh);
        m_events_per_revolution = cfgObj[cfgkey_events_per_rev].toInt(defaultEventsPerRevolution());

        const QString &polarityId {cfgObj[cfgkey_polarity].toString()};
        const QList<const ViAcqChannelPhaseTrigPolarity *> &polarities {ViAcqChannelPhaseTrigPolarities::all()};
        const auto &it {std::find_if(polarities.cbegin(), polarities.cend(),
                                    [&polarityId](const ViAcqChannelPhaseTrigPolarity *polarity) {
                                        return polarity->id() == polarityId;
                                    })};
        m_polarity = (it != polarities.cend()) ? *it : &defaultPolarity();

        m_pub_en = cfgObj[cfgkey_pub_en].toBool(defaultPubEnabled());
    }
}
