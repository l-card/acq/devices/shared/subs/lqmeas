#include "ViSetpointConditionType.h"

namespace LQMeas {
    const ViSetpointConditionType &LQMeas::ViSetpointConditionTypes::disabled() {
        static const class  ViSetpointConditionTypeDis  : public ViSetpointConditionType {
        public:
            QString id() const override {return QStringLiteral("vi_spcond_dis");}
            QString displayName() const override {return LQMeas::ViSetpointConditionTypes::tr("Disabled");}
            bool supportUpperLimit() const override {return false;}
            bool supportUnderLimit() const override {return false;}
        } t;
        return t;
    }

    const ViSetpointConditionType &ViSetpointConditionTypes::above() {
        static const class  ViSetpointConditionTypeAbove  : public ViSetpointConditionType {
        public:
            QString id() const override {return QStringLiteral("vi_spcond_above");}
            QString displayName() const override {return LQMeas::ViSetpointConditionTypes::tr("Above");}
            bool supportUpperLimit() const override {return true;}
            bool supportUnderLimit() const override {return false;}
        } t;
        return t;
    }

    const ViSetpointConditionType &ViSetpointConditionTypes::below() {
        static const class  ViSetpointConditionTypeBelow  : public ViSetpointConditionType {
        public:
            QString id() const override {return QStringLiteral("vi_spcond_below");}
            QString displayName() const override {return LQMeas::ViSetpointConditionTypes::tr("Below");}
            bool supportUpperLimit() const override {return false;}
            bool supportUnderLimit() const override {return true;}
        } t;
        return t;
    }

    const ViSetpointConditionType &ViSetpointConditionTypes::outOfBand() {
        static const class  ViSetpointConditionTypeOutOfBand  : public ViSetpointConditionType {
        public:
            QString id() const override {return QStringLiteral("vi_spcond_outofband");}
            QString displayName() const override {return LQMeas::ViSetpointConditionTypes::tr("Out of Band");}
            bool supportUpperLimit() const override {return true;}
            bool supportUnderLimit() const override {return true;}
        } t;
        return t;
    }

    const ViSetpointConditionType &ViSetpointConditionTypes::inBand() {
        static const class  ViSetpointConditionTypeInBand  : public ViSetpointConditionType {
        public:
            QString id() const override {return QStringLiteral("vi_spcond_inband");}
            QString displayName() const override {return LQMeas::ViSetpointConditionTypes::tr("In Band");}
            bool supportUpperLimit() const override {return true;}
            bool supportUnderLimit() const override {return true;}
        } t;
        return t;
    }

    const QList<const ViSetpointConditionType *> &ViSetpointConditionTypes::allTypes() {
        static const QList<const ViSetpointConditionType *> list {
            &disabled(),
            &above(),
            &below(),
            &outOfBand(),
            &inBand()
        };
        return list;
    }
}
