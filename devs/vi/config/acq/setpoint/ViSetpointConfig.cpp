#include "ViSetpointConfig.h"
#include "ViSetpointConditionType.h"
#include "../meas/ViMeasConfig.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_sp_cond_type       {"CondType"};
    static const QLatin1String cfgkey_sp_thresh_gr       {"Thresholds"};
    static const QLatin1String &cfgkey_sp_thresh_upper   {StdConfigKeys::upper()};
    static const QLatin1String &cfgkey_sp_thresh_under   {StdConfigKeys::under()};

    ViSetpointConfig::ViSetpointConfig(ViMeasConfig &measCfg, AlarmType type) :
        ViConfigItem{&measCfg},
        m_measCfg{&measCfg},
        m_alarm_type{type},
        m_condtype{&defaultConditionType()} {

        addSubItem(&m_upper_limit);
        addSubItem(&m_under_limit);
    }

    ViSetpointConfig::ViSetpointConfig(ViMeasConfig &measCfg, const ViSetpointConfig &origSetpoint) :
        ViConfigItem{&measCfg},
        m_measCfg{&measCfg},
        m_alarm_type{origSetpoint.m_alarm_type},
        m_condtype{origSetpoint.m_condtype},
        m_upper_limit{origSetpoint.m_upper_limit},
        m_under_limit{origSetpoint.m_under_limit} {

        addSubItem(&m_upper_limit);
        addSubItem(&m_under_limit);
    }

    void ViSetpointConfig::setConditionType(const ViSetpointConditionType &condType) {
        if (m_condtype != &condType) {
            m_condtype = &condType;
            notifyConfigChanged();
        }
    }

    const ViSetpointConditionType &ViSetpointConfig::defaultConditionType() const {
        return ViSetpointConditionTypes::disabled();
    }

    void ViSetpointConfig::protSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_sp_cond_type] = m_condtype->id();

        QJsonObject threshObj;
        saveSubItem(threshObj, m_upper_limit, cfgkey_sp_thresh_upper);
        saveSubItem(threshObj, m_under_limit, cfgkey_sp_thresh_under);
        cfgObj[cfgkey_sp_thresh_gr] = threshObj;
    }

    void ViSetpointConfig::protLoad(const QJsonObject &cfgObj) {
        const QString &condTypeId {cfgObj[cfgkey_sp_cond_type].toString()};
        const QList<const ViSetpointConditionType *> &condTypes {ViSetpointConditionTypes::allTypes()};
        const auto &it {std::find_if(condTypes.cbegin(), condTypes.cend(),
                         [&condTypeId](const ViSetpointConditionType *condType) {
                             return condType->id() == condTypeId;
        })};
        m_condtype = (it != condTypes.cend()) ? *it : &defaultConditionType();

        const QJsonObject &threshObj {cfgObj[cfgkey_sp_thresh_gr].toObject()};
        loadSubItem(threshObj, m_upper_limit, cfgkey_sp_thresh_upper);
        loadSubItem(threshObj, m_under_limit, cfgkey_sp_thresh_under);
    }
}
