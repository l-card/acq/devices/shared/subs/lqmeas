#ifndef LQMEAS_VISETPOINTCONFIG_H
#define LQMEAS_VISETPOINTCONFIG_H

#include "lqmeas/devs/vi/config/ViConfigItem.h"
#include "lqmeas/devs/vi/config/misc/ViConfigHystVal.h"

namespace LQMeas {
    class ViMeasConfig;
    class ViSetpointConditionType;

    class ViSetpointConfig : public ViConfigItem {
        Q_OBJECT
    public:
        enum class AlarmType {
            Alert,
            Danger
        };

        explicit ViSetpointConfig(ViMeasConfig &measCfg, AlarmType type);
        ViSetpointConfig(ViMeasConfig &measCfg, const ViSetpointConfig &origSetpoint);

        ViMeasConfig &meas() const {return *m_measCfg;}
        AlarmType alarmType() const {return m_alarm_type;}

        const ViSetpointConditionType &conditionType() const {return *m_condtype;}
        void setConditionType(const ViSetpointConditionType &condType);

        const ViConfigHystVal &upperLimit() const {return m_upper_limit;}
        ViConfigHystVal &upperLimit() {return m_upper_limit;}

        const ViConfigHystVal &underLimit() const {return m_under_limit;}
        ViConfigHystVal &underLimit() {return m_under_limit;}

        const ViSetpointConditionType &defaultConditionType() const;
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        ViMeasConfig *m_measCfg;
        AlarmType m_alarm_type;
        const ViSetpointConditionType *m_condtype;

        ViConfigHystVal m_upper_limit;
        ViConfigHystVal m_under_limit;
    };

}

#endif // VISETPOINTCONFIG_H
