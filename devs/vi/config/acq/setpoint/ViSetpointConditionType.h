#ifndef LQMEAS_VISETPOINTCONDITIONTYPE_H
#define LQMEAS_VISETPOINTCONDITIONTYPE_H


#include "lqmeas/EnumNamedValue.h"
#include <QMetaType>
#include <QObject>


namespace LQMeas {
    class ViModuleTypeInfo;
    class ViMeasGroupType;

    class ViSetpointConditionType : public EnumNamedValue<QString> {
    public:
        virtual bool supportUpperLimit() const = 0;
        virtual bool supportUnderLimit() const = 0;
    };

    class ViSetpointConditionTypes : public QObject {
        Q_OBJECT
    public:
        static const ViSetpointConditionType &disabled();
        static const ViSetpointConditionType &above();
        static const ViSetpointConditionType &below();
        static const ViSetpointConditionType &outOfBand();
        static const ViSetpointConditionType &inBand();

        static const QList<const ViSetpointConditionType *> &allTypes();
    };
}

Q_DECLARE_METATYPE(const LQMeas::ViSetpointConditionType *);


#endif // LQMEAS_VISETPOINTCONDITIONTYPE_H
