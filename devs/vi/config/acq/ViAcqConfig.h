#ifndef LQMEAS_VIACQCONFIG_H
#define LQMEAS_VIACQCONFIG_H

#include "lqmeas/devs/vi/config/ViConfigItem.h"
#include <QList>

namespace LQMeas {
    class ViAcqChannelConfig;
    class ViMeasConfig;
    class ViConfig;

    class ViAcqConfig : public ViConfigItem {
      Q_OBJECT
    public:
        explicit ViAcqConfig(ViConfig &viConfig);
        explicit ViAcqConfig(ViConfig &viConfig, const ViAcqConfig &origAcqCfg);

        const ViConfig &viConfig() const {return *m_viCfg;}
        ViConfig &viConfig() {return *m_viCfg;}

        const ViAcqChannelConfig &channel(int ch_num) const {return *m_channels.at(ch_num);}
        ViAcqChannelConfig &channel(int ch_num) {return *m_channels.at(ch_num);}
        int availableChannelsCnt() const {return m_av_chs_cnt;}
        double adcFreq() const {return m_adc_freq;}

        ViMeasConfig *getMeas(int ch_num, int meas_num) const;
        void cleanup();
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
        void viModuleInfoUpdate(const ViModuleTypeInfo &viInfo) override;
    private:
        void clearChannels();

        ViConfig *m_viCfg;
        QList<ViAcqChannelConfig *> m_channels;
        int m_av_chs_cnt {0};
        double m_adc_freq {128000};
    };
}

#endif // LQMEAS_VIACQCONFIG_H
