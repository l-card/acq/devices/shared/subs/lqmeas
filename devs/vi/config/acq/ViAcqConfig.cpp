#include "ViAcqConfig.h"
#include "../ViConfig.h"
#include "meas/ViMeasConfig.h"
#include "channel/ViAcqChannelConfig.h"
#include "lqmeas/devs/vi/modules/ViModuleTypeInfo.h"
#include "lqmeas/StdConfigKeys.h"
#include "lqmeas/lqtdefs.h"
#include <QJsonArray>
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String &cfgkey_acq_channels_arr  {StdConfigKeys::channels()};

    ViAcqConfig::ViAcqConfig(ViConfig &viConfig) : ViConfigItem{&viConfig}, m_viCfg{&viConfig} {

    }

    ViAcqConfig::ViAcqConfig(ViConfig &viConfig, const ViAcqConfig &origAcqCfg) :
        ViConfigItem{&viConfig}, m_viCfg{&viConfig}, m_av_chs_cnt{origAcqCfg.m_av_chs_cnt} {

        for (const ViAcqChannelConfig *other_ch : origAcqCfg.m_channels) {
            ViAcqChannelConfig *ch{new ViAcqChannelConfig{*this, *other_ch}};
            addSubItem(ch);
            m_channels += ch;
        }

    }

    ViMeasConfig *ViAcqConfig::getMeas(int ch_num, int meas_num) const {
        ViMeasConfig *ret {nullptr};
        if (ch_num < availableChannelsCnt()) {
            const ViAcqChannelConfig &ch {channel(ch_num)};
            QList<ViMeasConfig *> measList {ch.measList()};
            const auto &it {std::find_if(measList.cbegin(), measList.cend(),
                                        [meas_num](ViMeasConfig *meas){return meas->measNum() == meas_num;})};
            if (it != measList.cend()) {
                ret = *it;
            }
        }
        return ret;
    }

    void ViAcqConfig::cleanup() {
        clearChannels();
    }

    void ViAcqConfig::protSave(QJsonObject &cfgObj) const {
        QJsonArray chArray;
        for (const ViAcqChannelConfig *ch : m_channels) {
            QJsonObject chObj;
            ch->save(chObj);
            chArray.append(chObj);
        }
        cfgObj[cfgkey_acq_channels_arr] = chArray;
    }

    void ViAcqConfig::protLoad(const QJsonObject &cfgObj) {
        const QJsonArray &chArray {cfgObj[cfgkey_acq_channels_arr].toArray()};
        int ch_idx {0};
        for (const auto &it : chArray) {
            if (ch_idx < m_channels.size()) {
                m_channels.at(ch_idx)->load(it.toObject());
            } else {
                ViAcqChannelConfig *ch{new ViAcqChannelConfig{*this, ch_idx}};
                ch->load(it.toObject());
                addSubItem(ch);
                m_channels += ch;
            }
            ++ch_idx;
        }
    }

    void ViAcqConfig::viModuleInfoUpdate(const ViModuleTypeInfo &viInfo) {
        m_adc_freq = viInfo.acqAdcFreq();
        m_av_chs_cnt = viInfo.acqChCnt();
        for (int ch_idx {m_channels.size()}; ch_idx < m_av_chs_cnt; ++ch_idx) {
            ViAcqChannelConfig *ch{new ViAcqChannelConfig{*this, ch_idx}};
            addSubItem(ch);
            m_channels += ch;
        }
    }

    void ViAcqConfig::clearChannels() {
        for (ViAcqChannelConfig *ch : qAsConst(m_channels)) {
            remSubItem(ch);
        }
        qDeleteAll(m_channels);
        m_channels.clear();
    }
}
