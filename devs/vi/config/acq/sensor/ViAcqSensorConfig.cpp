#include "ViAcqSensorConfig.h"
#include "ViAcqSensorType.h"
#include "ViAcqSensorConType.h"
#include "lqmeas/devs/vi/config/acq/channel/ViAcqChannelConfig.h"
#include "lqmeas/devs/vi/modules/ViModuleTypeInfo.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>
#include "lqmeas/lqtdefs.h"

namespace LQMeas {
    static const QLatin1String &cfgkey_sensor_type          {StdConfigKeys::type()};
    static const QLatin1String cfgkey_sensor_con_type       {"ConType"};
    static const QLatin1String cfgkey_sensor_conv_coef_gr   {"ConvCoefs"};
    static const QLatin1String cfgkey_sensor_offset         {"Offset"};
    static const QLatin1String cfgkey_sensor_scale_factor   {"ScaleFactor"};
    static const QLatin1String cfgkey_sensor_valid_range_gr {"ValidRange"};
    static const QLatin1String &cfgkey_sensor_max_valid_val {StdConfigKeys::maxValue()};
    static const QLatin1String &cfgkey_sensor_min_valid_val {StdConfigKeys::minValue()};
    static const QLatin1String cfgkey_sensor_dc_check_gr    {"DcCheck"};
    static const QLatin1String cfgkey_sensor_dc_check_short {"Short"};
    static const QLatin1String cfgkey_sensor_dc_check_open  {"Open"};

    ViAcqSensorConfig::ViAcqSensorConfig(ViAcqChannelConfig &chCfg, const ViAcqSensorType &type) :
        ViConfigItem{chCfg.viModuleTypeInfo(), &chCfg}, m_ch{&chCfg}, m_type{&type},
        m_con_type{&defaultConnectionMethod()},
        m_scale_factor{defaultScaleFactor()},
        m_offset{defaultOffset()},
        m_max_valid_value{defaultMaxValidValue()},
        m_min_valid_value{defaultMinValidValue()},
        m_dc_check_short{defaultDcCheckShortEnabled(), defaultDcCheckShortValue()},
        m_dc_check_open{defaultDcCheckOpenEnabled(), defaultDcCheckOpenValue()} {

        updateScaleByCoefs();
        addSubItem(&m_dc_check_short);
        addSubItem(&m_dc_check_open);
    }

    ViAcqSensorConfig::ViAcqSensorConfig(ViAcqChannelConfig &chCfg, const ViAcqSensorConfig &origSensorCfg) :
        ViConfigItem{chCfg.viModuleTypeInfo(), &chCfg}, m_ch{&chCfg},
        m_type{origSensorCfg.m_type},
        m_con_type{origSensorCfg.m_con_type},
        m_scale_factor{origSensorCfg.m_scale_factor},
        m_offset{origSensorCfg.m_offset},
        m_max_valid_value{origSensorCfg.m_max_valid_value},
        m_min_valid_value{origSensorCfg.m_min_valid_value},
        m_max_phys_value{origSensorCfg.m_max_phys_value},
        m_min_phys_value{origSensorCfg.m_min_phys_value},
        m_dc_check_short{origSensorCfg.m_dc_check_short},
        m_dc_check_open{origSensorCfg.m_dc_check_open} {

        addSubItem(&m_dc_check_short);
        addSubItem(&m_dc_check_open);
    }

    void ViAcqSensorConfig::setConnectionMethod(const ViAcqSensorConType &conType) {
        if ((m_con_type != &conType) && (supportedConnectionMethods().contains(&conType))) {
            m_con_type = &conType;
            dcCheckReset();
            notifyConfigChanged();
        }
    }

    void ViAcqSensorConfig::setScaleFactor(double val) {
        if (!LQREAL_IS_EQUAL(m_scale_factor, val)) {
            m_scale_factor = val;
            updateScaleByCoefs();
            notifyConfigChanged();
        }
    }

    void ViAcqSensorConfig::setOffset(double val) {
        if (!LQREAL_IS_EQUAL(m_offset, val)) {
            m_offset = val;
            updateScaleByCoefs();
            notifyConfigChanged();
        }
    }

    void ViAcqSensorConfig::setMaxPhysValue(double val) {
        if (!LQREAL_IS_EQUAL(m_max_phys_value, val)) {
            m_max_phys_value = val;
            updateCoefsByScale();
            notifyConfigChanged();
        }
    }

    void ViAcqSensorConfig::setMinPhysValue(double val) {
        if (!LQREAL_IS_EQUAL(m_min_phys_value, val)) {
            m_min_phys_value = val;
            updateCoefsByScale();
            notifyConfigChanged();
        }
    }

    void ViAcqSensorConfig::setMaxValidValue(double val) {
        if (!LQREAL_IS_EQUAL(m_max_valid_value, val)) {
            m_max_valid_value = val;
            updateScaleByCoefs();
            notifyConfigChanged();
        }
    }

    void ViAcqSensorConfig::setMinValidValue(double val) {
        if (!LQREAL_IS_EQUAL(m_min_valid_value, val)) {
            m_min_valid_value = val;
            updateScaleByCoefs();
            notifyConfigChanged();
        }
    }

    QList<const ViAcqSensorConType *> ViAcqSensorConfig::supportedConnectionMethods() const {
        QList<const ViAcqSensorConType *> ret;
        QList<const ViAcqSensorConType *> sensorCons {m_type->supportedConnectionMethods()};
        QList<const ViAcqSensorConType *> moduleCons {viModuleTypeInfo().acqSensorConnectionMethods()};
        for (const ViAcqSensorConType *sensorCon : sensorCons) {
            if (moduleCons.contains(sensorCon)) {
                ret += sensorCon;
            }
        }
        return ret;
    }

    const ViAcqSensorConType &ViAcqSensorConfig::defaultConnectionMethod() const {
        return *supportedConnectionMethods().at(0);
    }

    bool ViAcqSensorConfig::hasScaleFactor() const {
        return m_type->hasScaleFactor(*m_con_type);
    }

    bool ViAcqSensorConfig::hasOffset() const {
        return m_type->hasOffset(*m_con_type);
    }

    bool ViAcqSensorConfig::hasScaleRange() const {
        return m_type->hasScaleRange(*m_con_type);
    }

    bool ViAcqSensorConfig::hasMaxValidValue() const {
        return m_type->hasMaxValidValue(*m_con_type);
    }

    bool ViAcqSensorConfig::hasMinValidValue() const {
        return m_type->hasMinValidValue(*m_con_type);
    }

    double ViAcqSensorConfig::defaultScaleFactor() const {
        return m_type->defaultScaleFactor(*m_con_type);
    }

    double ViAcqSensorConfig::defaultOffset() const {
        return m_type->defaultOffset(*m_con_type);
    }

    double ViAcqSensorConfig::defaultMaxValidValue() const {
        return m_type->defaultMaxValidValue(*m_con_type);
    }

    double ViAcqSensorConfig::defaultMinValidValue() const {
        return m_type->defaultMinValidValue(*m_con_type);
    }

    bool ViAcqSensorConfig::supportDcCheckShort() const {
        return m_type->supportDcCheckShort(*m_con_type);
    }

    bool ViAcqSensorConfig::supportDcCheckOpen() const {
        return m_type->supportDcCheckOpen(*m_con_type);
    }

    bool ViAcqSensorConfig::dcCheckShortIsOptional() const {
        return m_type->dcCheckShortIsOptional(*m_con_type);
    }

    bool ViAcqSensorConfig::dcCheckOpenIsOptional() const {
        return m_type->dcCheckOpenIsOptional(*m_con_type);
    }

    bool ViAcqSensorConfig::defaultDcCheckShortEnabled() const {
        return m_type->defaultDcCheckShortEnabled(*m_con_type);
    }

    double ViAcqSensorConfig::defaultDcCheckShortValue() const {
        return m_type->defaultDcCheckShortValue(*m_con_type);
    }

    bool ViAcqSensorConfig::defaultDcCheckOpenEnabled() const {
        return m_type->defaultDcCheckOpenEnabled(*m_con_type);
    }

    double ViAcqSensorConfig::defaultDcCheckOpenValue() const {
        return m_type->defaultDcCheckOpenValue(*m_con_type);
    }

    QString ViAcqSensorConfig::loadSensorTypeID(const QJsonObject &cfgObj) {
        return cfgObj[cfgkey_sensor_type].toString();
    }


    void ViAcqSensorConfig::protSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_sensor_type] = m_type->id();
        cfgObj[cfgkey_sensor_con_type] = m_con_type->id();

        if (hasScaleFactor() || hasOffset()) {
            QJsonObject convCoefObj;
            if (hasOffset()) {
                convCoefObj[cfgkey_sensor_offset] = m_offset;
            }
            if (hasScaleFactor()) {
                convCoefObj[cfgkey_sensor_scale_factor] = m_scale_factor;
            }
            cfgObj[cfgkey_sensor_conv_coef_gr] = convCoefObj;
        }

        if (hasMinValidValue() || hasMaxValidValue()) {
            QJsonObject validRangeObj;
            if (hasMaxValidValue()) {
                validRangeObj[cfgkey_sensor_max_valid_val] = m_max_valid_value;
            }
            if (hasMinValidValue()) {
                validRangeObj[cfgkey_sensor_min_valid_val] = m_min_valid_value;
            }
            cfgObj[cfgkey_sensor_valid_range_gr] = validRangeObj;
        }

        {
            QJsonObject dcCheckObj;
            saveSubItem(dcCheckObj, m_dc_check_short, cfgkey_sensor_dc_check_short);
            saveSubItem(dcCheckObj, m_dc_check_open, cfgkey_sensor_dc_check_open);
            cfgObj[cfgkey_sensor_dc_check_gr] = dcCheckObj;
        }
    }

    void ViAcqSensorConfig::protLoad(const QJsonObject &cfgObj) {
        const QString conTypeId {cfgObj[cfgkey_sensor_con_type].toString()};
        const QList<const ViAcqSensorConType *> conTypes {supportedConnectionMethods()};
        const auto it {std::find_if(conTypes.cbegin(), conTypes.cend(),
                         [&conTypeId](const ViAcqSensorConType *conType) {
                             return conType->id() == conTypeId;
        })};
        m_con_type = it !=  conTypes.cend() ? *it : &defaultConnectionMethod();
        dcCheckReset();

        if (hasScaleFactor() || hasOffset()) {
            const QJsonObject &convCoefObj {cfgObj[cfgkey_sensor_conv_coef_gr].toObject()};
            if (hasOffset()) {
                m_offset = convCoefObj[cfgkey_sensor_offset].toDouble(defaultOffset());
            }
            if (hasScaleFactor()) {
                m_scale_factor = convCoefObj[cfgkey_sensor_scale_factor].toDouble(defaultScaleFactor());
            }
        }

        if (hasMinValidValue() || hasMaxValidValue()) {
            const QJsonObject &validRangeObj {cfgObj[cfgkey_sensor_valid_range_gr].toObject()};
            if (hasMaxValidValue()) {
                m_max_valid_value = validRangeObj[cfgkey_sensor_max_valid_val].toDouble(defaultMaxValidValue());
            }
            if (hasMinValidValue()) {
                m_min_valid_value = validRangeObj[cfgkey_sensor_min_valid_val].toDouble(defaultMinValidValue());
            }
        }

        {
            const QJsonObject &dcCheckObj {cfgObj[cfgkey_sensor_dc_check_gr].toObject()};
            loadSubItem(dcCheckObj, m_dc_check_short, cfgkey_sensor_dc_check_short);
            loadSubItem(dcCheckObj, m_dc_check_open, cfgkey_sensor_dc_check_open);
        }

        if (hasScaleRange()) {
            updateScaleByCoefs();
        }
    }

    void ViAcqSensorConfig::updateScaleByCoefs() {
        m_type->coefsToScaleRange(connectionMethod(), minValidValue(), maxValidValue(), scaleFactor(), offset(), m_min_phys_value, m_max_phys_value);
    }

    void ViAcqSensorConfig::updateCoefsByScale() {
        m_type->scaleRangeToCoefs(connectionMethod(), minValidValue(), maxValidValue(), minPhysValue(), maxPhysValue(), m_scale_factor, m_offset);
    }

    void ViAcqSensorConfig::dcCheckReset() {
        m_dc_check_short.setEnabled(defaultDcCheckShortEnabled());
        m_dc_check_short.setValue(defaultDcCheckShortValue());
        m_dc_check_short.setDefaultEnabled(defaultDcCheckShortEnabled());
        m_dc_check_short.setDefaultValue(defaultDcCheckShortValue());
        m_dc_check_open.setEnabled(defaultDcCheckOpenEnabled());
        m_dc_check_open.setValue(defaultDcCheckOpenValue());
        m_dc_check_open.setDefaultEnabled(defaultDcCheckOpenEnabled());
        m_dc_check_open.setDefaultValue(defaultDcCheckOpenValue());
    }
}
