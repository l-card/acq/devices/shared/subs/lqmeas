#include "ViAcqSensorType.h"
#include "lqmeas/units/Unit.h"
#include "ViAcqSensorConType.h"

namespace LQMeas {
    double ViAcqSensorType::defaultDcCheckShortEnabled(const ViAcqSensorConType &conType) const {
        return conType == ViAcqSensorConTypes::ICP();
    }

    double ViAcqSensorType::defaultDcCheckShortValue(const ViAcqSensorConType &conType) const  {
        return conType.polarityK() * 1.5;
    }

    double ViAcqSensorType::defaultDcCheckOpenEnabled(const ViAcqSensorConType &conType) const {
        return conType == ViAcqSensorConTypes::ICP();
    }

    double ViAcqSensorType::defaultDcCheckOpenValue(const ViAcqSensorConType &conType) const {
        return conType.polarityK() * 21;
    }

    void ViAcqSensorType::coefsToScaleRange(const ViAcqSensorConType &conType,
                                        double emin, double emax, double k, double b,
                                        double &pmin, double &pmax) const {
        if (k == 0) {
            k = 1;
        }
        k/=calcKConvMul();
        if (hasOffset(conType)) {
            pmin = 1/k * (emin - b);
            pmax = 1/k * (emax - b);
        } else {
            pmax = (emax - emin)/(2*k);
            pmin = 0;
        }
    }
    void LQMeas::ViAcqSensorType::scaleRangeToCoefs(const LQMeas::ViAcqSensorConType &conType,
                                                    double emin, double emax, double pmin, double pmax,
                                                    double &k, double &b) const {
        if (hasOffset(conType)) {
            k = (emax - emin)/(pmax - pmin);
            b = emax - pmax * k;
        } else {
            k = (emax - emin)/(2 * pmax);
            b = 0;
        }
        k*=calcKConvMul();
    }

    double LQMeas::ViAcqSensorType::calcKConvMul() const {
        double mul = 1;
        if (inputUnit() != scaleInputUnit()) {
            mul *= inputUnit().multipler() / scaleInputUnit().multipler() ;
        }
        if (outputUnit() != scaleOutputUnit()) {
            mul *= scaleOutputUnit().multipler() / outputUnit().multipler();
        }
        return mul;
    }
}
