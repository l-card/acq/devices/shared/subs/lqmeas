#include "ViAcqSensorConType.h"

namespace LQMeas {
    const ViAcqSensorConType &LQMeas::ViAcqSensorConTypes::ICP() {
        static const class  ViAcqSensorConTypeIPC  : public ViAcqSensorConType {
        public:
            QString id() const override {return QStringLiteral("vi_sensor_contype_icp");}
            QString displayName() const override {return LQMeas::ViAcqSensorConTypes::tr("ICP");}
            bool dcAvailable() const override {return false;}
            bool negativePolarity() const override {return false;}
        } t;
        return t;
    }

    const ViAcqSensorConType &ViAcqSensorConTypes::VOutPosSup() {
        static const class  ViAcqSensorConTypeVOutPosSupply : public ViAcqSensorConType {
        public:
            QString id() const override {return QStringLiteral("vi_sensor_contype_vout_pos_sup");}
            QString displayName() const override {return LQMeas::ViAcqSensorConTypes::tr("Voltage out, positive supply");}
            bool dcAvailable() const override {return true;}
            bool negativePolarity() const override {return false;}
        } t;
        return t;
    }

    const ViAcqSensorConType &ViAcqSensorConTypes::VOutNegSup() {
        static const class  ViAcqSensorConTypeVOutNegSupply : public ViAcqSensorConType {
        public:
            QString id() const override {return QStringLiteral("vi_sensor_contype_vout_neg_sup");}
            QString displayName() const override {return LQMeas::ViAcqSensorConTypes::tr("Voltage out, negative supply");}
            bool dcAvailable() const override {return true;}
            bool negativePolarity() const override {return true;}
        } t;
        return t;
    }

    const QList<const ViAcqSensorConType *> &ViAcqSensorConTypes::allTypes() {
        static const QList<const ViAcqSensorConType *> list {
            &ICP(),
            &VOutPosSup(),
            &VOutNegSup()
        };
        return list;
    }
}
