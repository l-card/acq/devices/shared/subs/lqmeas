#ifndef LQMEAS_VIACQSENSORTYPE_H
#define LQMEAS_VIACQSENSORTYPE_H

#include "lqmeas/EnumNamedValue.h"
#include <QMetaType>

namespace LQMeas {
    class ViAcqSensorConType;
    class Unit;

    class ViAcqSensorType : public EnumNamedValue<QString> {
    public:
        virtual QList<const ViAcqSensorConType *> supportedConnectionMethods() const = 0;

        virtual bool hasScaleFactor(const ViAcqSensorConType &conType) const {return false;}
        virtual bool hasOffset(const ViAcqSensorConType &conType) const {return false;}
        virtual bool hasScaleRange(const ViAcqSensorConType &conType) const {
            return hasMaxValidValue(conType) && hasMinValidValue(conType) && hasScaleFactor(conType);
        }
        virtual bool hasMaxValidValue(const ViAcqSensorConType &conType) const  {return false;}
        virtual bool hasMinValidValue(const ViAcqSensorConType &conType) const  {return false;}

        virtual double defaultScaleFactor(const ViAcqSensorConType &conType) const {return 1.;}
        virtual double defaultOffset(const ViAcqSensorConType &conType) const {return 0.;}
        virtual double defaultMaxValidValue(const ViAcqSensorConType &conType) const {return 0;}
        virtual double defaultMinValidValue(const ViAcqSensorConType &conType) const {return 0;}

        virtual bool supportDcCheckShort(const ViAcqSensorConType &conType) const {return false;}
        virtual bool supportDcCheckOpen(const ViAcqSensorConType &conType) const {return false;}
        virtual bool dcCheckShortIsOptional(const ViAcqSensorConType &conType) const {return false;}
        virtual bool dcCheckOpenIsOptional(const ViAcqSensorConType &conType) const {return false;}

        virtual double defaultDcCheckShortEnabled(const ViAcqSensorConType &conType) const;
        virtual double defaultDcCheckShortValue(const ViAcqSensorConType &conType) const;
        virtual double defaultDcCheckOpenEnabled(const ViAcqSensorConType &conType) const;
        virtual double defaultDcCheckOpenValue(const ViAcqSensorConType &conType) const;

        virtual const Unit &inputUnit() const = 0;
        virtual const Unit &outputUnit() const = 0;

        virtual const Unit &scaleInputUnit() const {return inputUnit();}
        virtual const Unit &scaleOutputUnit() const {return outputUnit();}
        virtual bool inoutUnitInverseView() const {return false;} /* отображение входных/выходных величин в обратном порядке */

        virtual void coefsToScaleRange(const ViAcqSensorConType &conType, double emin, double emax, double k, double b, double &pmin, double &pmax) const;
        virtual void scaleRangeToCoefs(const ViAcqSensorConType &conType, double emin, double emax, double pmin, double pmax, double &k, double &b) const;
    private:
        double calcKConvMul() const;
    };
}

#endif // VIACQSENSORTYPE_H
