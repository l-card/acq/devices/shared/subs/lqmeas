#include "ViAcqSensorTypeUnknown.h"
#include "lqmeas/devs/vi/config/acq/sensor/ViAcqSensorConType.h"
#include "lqmeas/devs/vi/config/acq/sensor/ViAcqSensorConfig.h"
#include "lqmeas/units/std/Special.h"

namespace LQMeas {
    const ViAcqSensorTypeUnknown &ViAcqSensorTypeUnknown::type() {
        static const ViAcqSensorTypeUnknown t;
        return t;
    }

    QString ViAcqSensorTypeUnknown::id() const {
        return QStringLiteral("vi_sensor_type_unknown");
    }

    QString ViAcqSensorTypeUnknown::displayName() const {
        return LQMeas::ViAcqSensorConfig::tr("Unknown");
    }

    QList<const ViAcqSensorConType *> ViAcqSensorTypeUnknown::supportedConnectionMethods() const {
        return ViAcqSensorConTypes::allTypes();
    }

    const Unit &ViAcqSensorTypeUnknown::inputUnit() const {
        return Units::Special::unknown();
    }

    const Unit &ViAcqSensorTypeUnknown::outputUnit() const {
        return Units::Special::unknown();
    }
}
