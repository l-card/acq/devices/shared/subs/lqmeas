#ifndef LQMEAS_VIACQSENSORTYPEUNKNOWN_H
#define LQMEAS_VIACQSENSORTYPEUNKNOWN_H

#include "../../ViAcqSensorType.h"

namespace LQMeas {
    class ViAcqSensorTypeUnknown : public ViAcqSensorType {
    public:
        static const ViAcqSensorTypeUnknown &type();

        QString id() const override;
        QString displayName() const override;

        QList<const ViAcqSensorConType *> supportedConnectionMethods() const override;

        const Unit &inputUnit() const override;
        const Unit &outputUnit() const override;
    };
}




#endif // LQMEAS_VIACQSENSORTYPEUNKNOWN_H
