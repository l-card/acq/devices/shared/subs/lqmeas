#include "ViAcqSensorTypeProximeter.h"
#include "lqmeas/devs/vi/config/acq/sensor/ViAcqSensorConType.h"
#include "lqmeas/devs/vi/config/acq/sensor/ViAcqSensorConfig.h"
#include "lqmeas/units/std/Voltage.h"
#include "lqmeas/units/std/Displacement.h"


namespace LQMeas {
    const ViAcqSensorTypeProximeter &ViAcqSensorTypeProximeter::type() {
        static const ViAcqSensorTypeProximeter t;
        return t;
    }

    QString ViAcqSensorTypeProximeter::id() const {
        return QStringLiteral("vi_sensor_type_proximeter");
    }

    QString ViAcqSensorTypeProximeter::displayName() const {
        return LQMeas::ViAcqSensorConfig::tr("Proximeter");
    }

    QList<const ViAcqSensorConType *> ViAcqSensorTypeProximeter::supportedConnectionMethods() const {
        static const QList<const ViAcqSensorConType *> list {
            &ViAcqSensorConTypes::VOutPosSup(),
            &ViAcqSensorConTypes::VOutNegSup()
        };
        return list;
    }

    double ViAcqSensorTypeProximeter::defaultScaleFactor(const ViAcqSensorConType &conType) const {
        return 7.87;
    }

    double ViAcqSensorTypeProximeter::defaultMaxValidValue(const ViAcqSensorConType &conType) const {
        return conType.negativePolarity() ? -2.95 : 16.72;
    }

    double ViAcqSensorTypeProximeter::defaultMinValidValue(const ViAcqSensorConType &conType) const {
        return conType.negativePolarity() ? -16.72 : 2.95;
    }

    const Unit &ViAcqSensorTypeProximeter::inputUnit() const {
        return Units::Voltage::V();
    }

    const Unit &ViAcqSensorTypeProximeter::outputUnit() const {
        return Units::Displacement::um();
    }

    const Unit &ViAcqSensorTypeProximeter::scaleOutputUnit() const  {
        return Units::Displacement::mm();
    }
}
