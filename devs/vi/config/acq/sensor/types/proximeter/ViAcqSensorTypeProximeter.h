#ifndef LQMEAS_VIACQSENSORTYPEPROXIMETER_H
#define LQMEAS_VIACQSENSORTYPEPROXIMETER_H

#include "../../ViAcqSensorType.h"

namespace LQMeas {
    class ViAcqSensorTypeProximeter : public ViAcqSensorType {
    public:
        static const ViAcqSensorTypeProximeter &type();

        QString id() const override;
        QString displayName() const override;

        QList<const ViAcqSensorConType *> supportedConnectionMethods() const override;

        double defaultScaleFactor(const ViAcqSensorConType &conType) const override;
        double defaultMaxValidValue(const ViAcqSensorConType &conType) const override;
        double defaultMinValidValue(const ViAcqSensorConType &conType) const override;

        bool supportDcCheckShort(const ViAcqSensorConType &conType) const override {return false;}
        bool supportDcCheckOpen(const ViAcqSensorConType &conType) const override {return false;}

        bool hasScaleFactor(const ViAcqSensorConType &conType) const override {return true;}
        bool hasMaxValidValue(const ViAcqSensorConType &conType) const override {return true;}
        bool hasMinValidValue(const ViAcqSensorConType &conType) const override {return true;}

        const Unit &inputUnit() const override;
        const Unit &outputUnit() const override;
        const Unit &scaleOutputUnit() const override;
    };
}

#endif // LQMEAS_VIACQSENSORTYPEPROXIMETER_H
