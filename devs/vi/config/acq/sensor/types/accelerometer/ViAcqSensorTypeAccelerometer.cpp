#include "ViAcqSensorTypeAccelerometer.h"
#include "lqmeas/devs/vi/config/acq/sensor/ViAcqSensorConType.h"
#include "lqmeas/devs/vi/config/acq/sensor/ViAcqSensorConfig.h"
#include "lqmeas/units/std/Voltage.h"
#include "lqmeas/units/std/Acceleration.h"

namespace LQMeas {
    const ViAcqSensorTypeAccelerometer &ViAcqSensorTypeAccelerometer::type() {
        static const  ViAcqSensorTypeAccelerometer t;
        return t;
    }

    QString ViAcqSensorTypeAccelerometer::id() const {
        return QStringLiteral("vi_sensor_type_accel");
    }

    QString ViAcqSensorTypeAccelerometer::displayName() const {
        return LQMeas::ViAcqSensorConfig::tr("Accelerometer");
    }

    QList<const ViAcqSensorConType *> ViAcqSensorTypeAccelerometer::supportedConnectionMethods() const {
        static const QList<const ViAcqSensorConType *> list {
            &ViAcqSensorConTypes::ICP(),
            &ViAcqSensorConTypes::VOutPosSup(),
            &ViAcqSensorConTypes::VOutNegSup()
        };
        return list;
    }

    double ViAcqSensorTypeAccelerometer::defaultMaxValidValue(const ViAcqSensorConType &conType) const {
        return conType.negativePolarity() ? 0 : 24;
    }

    double ViAcqSensorTypeAccelerometer::defaultMinValidValue(const ViAcqSensorConType &conType) const {
        return conType.negativePolarity() ? -24 : 0;
    }

    bool ViAcqSensorTypeAccelerometer::supportDcCheckShort(const ViAcqSensorConType &conType) const {
        return &conType == &ViAcqSensorConTypes::ICP();
    }

    bool ViAcqSensorTypeAccelerometer::supportDcCheckOpen(const ViAcqSensorConType &conType) const {
        return &conType == &ViAcqSensorConTypes::ICP();
    }

    const Unit &ViAcqSensorTypeAccelerometer::inputUnit() const {
        return LQMeas::Units::Voltage::V();
    }

    const Unit &ViAcqSensorTypeAccelerometer::outputUnit() const {
        return LQMeas::Units::Acceleration::m_s2();
    }

    const Unit &ViAcqSensorTypeAccelerometer::scaleInputUnit() const {
        return LQMeas::Units::Voltage::mV();
    }

    const Unit &ViAcqSensorTypeAccelerometer::scaleOutputUnit() const {
        return LQMeas::Units::Acceleration::g();
    }
}
