#ifndef LQMEAS_VIACQSENSORCONTYPE_H
#define LQMEAS_VIACQSENSORCONTYPE_H


#include "lqmeas/EnumNamedValue.h"
#include <QMetaType>
#include <QObject>

namespace LQMeas {
    class ViAcqSensorConType : public EnumNamedValue<QString> {
    public:
        virtual bool dcAvailable() const {return true;}
        virtual bool negativePolarity() const {return false;}
        virtual int polarityK() const {return negativePolarity() ? -1 : 1;}
    };

    class ViAcqSensorConTypes : public QObject {
        Q_OBJECT
    public:
        static const ViAcqSensorConType &ICP();
        static const ViAcqSensorConType &VOutPosSup();
        static const ViAcqSensorConType &VOutNegSup();

        static const QList<const ViAcqSensorConType *> &allTypes();
    };
}

Q_DECLARE_METATYPE(const LQMeas::ViAcqSensorConType *);



#endif // LQMEAS_VIACQSENSORCONTYPE_H
