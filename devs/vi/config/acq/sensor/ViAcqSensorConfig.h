#ifndef LQMEAS_VIACQSENSORCONFIG_H
#define LQMEAS_VIACQSENSORCONFIG_H

#include "lqmeas/devs/vi/config/ViConfigItem.h"
#include "lqmeas/devs/vi/config/misc/ViConfigOptionalVal.h"

namespace LQMeas {
    class ViAcqChannelConfig;
    class ViAcqSensorType;
    class ViAcqSensorConType;

    class ViAcqSensorConfig : public ViConfigItem {
        Q_OBJECT
    public:
        ViAcqSensorConfig(ViAcqChannelConfig &chCfg, const ViAcqSensorType &type);
        ViAcqSensorConfig(ViAcqChannelConfig &chCfg, const ViAcqSensorConfig &origSensorCfg);

        const ViAcqSensorType &type() const {return *m_type;}
        const ViAcqSensorConType &connectionMethod() const {return *m_con_type;}
        void setConnectionMethod(const ViAcqSensorConType &conType);

        double scaleFactor() const {return m_scale_factor;}
        double offset() const {return m_offset;}
        double maxPhysValue() const {return m_max_phys_value;}
        double minPhysValue() const {return m_min_phys_value;}

        double maxValidValue() const {return m_max_valid_value;}
        double minValidValue() const {return m_min_valid_value;}

        void setScaleFactor(double val);
        void setOffset(double val);
        void setMaxPhysValue(double val);
        void setMinPhysValue(double val);

        void setMaxValidValue(double val);
        void setMinValidValue(double val);

        const ViConfigOptionalVal &dcCheckShort() const {return m_dc_check_short;}
        ViConfigOptionalVal &dcCheckShort() {return m_dc_check_short;}
        const ViConfigOptionalVal &dcCheckOpen() const {return m_dc_check_open;}
        ViConfigOptionalVal &dcCheckOpen() {return m_dc_check_open;}



        QList<const ViAcqSensorConType *> supportedConnectionMethods() const;
        const ViAcqSensorConType &defaultConnectionMethod() const;
        bool hasScaleFactor() const;
        bool hasOffset() const;
        bool hasScaleRange() const;
        bool hasMaxValidValue() const;
        bool hasMinValidValue() const;

        double defaultScaleFactor() const;
        double defaultOffset() const;
        double defaultMaxValidValue() const;
        double defaultMinValidValue() const;

        bool supportDcCheckShort() const;
        bool supportDcCheckOpen() const;
        bool dcCheckShortIsOptional() const;
        bool dcCheckOpenIsOptional() const;
        bool defaultDcCheckShortEnabled() const;
        double defaultDcCheckShortValue() const;
        bool defaultDcCheckOpenEnabled() const;
        double defaultDcCheckOpenValue() const;

        static QString loadSensorTypeID(const QJsonObject &cfgObj);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        void updateScaleByCoefs();
        void updateCoefsByScale();
        void dcCheckReset();

        ViAcqChannelConfig *m_ch;
        const ViAcqSensorType *m_type;
        const ViAcqSensorConType *m_con_type;

        double m_scale_factor;
        double m_offset;
        double m_max_phys_value;
        double m_min_phys_value;

        double m_max_valid_value;
        double m_min_valid_value;

        ViConfigOptionalVal m_dc_check_short;
        ViConfigOptionalVal m_dc_check_open;
    };
}

#endif // LQMEAS_VIACQSENSORCONFIG_H
