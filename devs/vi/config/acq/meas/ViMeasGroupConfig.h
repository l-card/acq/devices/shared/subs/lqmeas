#ifndef LQMEAS_VIMEASGROUPCONFIG_H
#define LQMEAS_VIMEASGROUPCONFIG_H

#include "lqmeas/devs/vi/config/ViConfigItem.h"

namespace LQMeas {
    class ViAcqChannelConfig;
    class ViMeasGroupType;
    class ViMeasConfig;
    class ViConfig;

    class ViMeasGroupConfig : public ViConfigItem {
        Q_OBJECT
    public:
        explicit ViMeasGroupConfig(ViAcqChannelConfig &chCfg, const ViMeasGroupType &type);
        explicit ViMeasGroupConfig(ViAcqChannelConfig &chCfg, const ViMeasGroupConfig &origMeasGroupCfg);

        const ViAcqChannelConfig &channel() const {return *m_ch;}
        ViAcqChannelConfig &channel() {return *m_ch;}
        const ViMeasGroupType &groupType() const {return *m_type;}
        const QList<ViMeasConfig *> &measList() const {return m_measList;}

        const ViConfig &viConfig() const;
        ViConfig &viConfig();

        static QString loadTypeId(const QJsonObject &cfgObj);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        ViAcqChannelConfig *m_ch;
        const ViMeasGroupType *m_type;
        QList<ViMeasConfig *> m_measList;
    };
}

#endif // LQMEAS_VIMEASGROUPCONFIG_H
