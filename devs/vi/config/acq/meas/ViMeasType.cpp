#include "ViMeasType.h"
#include "types/ViMeasTypeVibroAccel.h"
#include "types/ViMeasTypeVibroAccelHf.h"
#include "types/ViMeasTypeVibroVelocity.h"
#include "types/ViMeasTypeVibroDisp.h"
#include "types/ViMeasTypeRotSpeed.h"
#include "types/ViMeasTypeVoltageDC.h"
#include "types/ViMeasTypeVoltageGap.h"
#include "types/ViMeasTypePosition.h"
#include "types/ViMeasTypeVibroSmax.h"
#include "types/Harmonics/ViMeasTypeHarmAmpAccel.h"
#include "types/Harmonics/ViMeasTypeHarmPhaAccel.h"
#include "types/Harmonics/ViMeasTypeHarmAmpVelocity.h"
#include "types/Harmonics/ViMeasTypeHarmPhaVelocity.h"
#include "types/Harmonics/ViMeasTypeHarmAmpDisp.h"
#include "types/Harmonics/ViMeasTypeHarmPhaDisp.h"
#include "lqmeas/devs/vi/config/acq/channel/ViAcqChannelConfig.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefAcqCh.h"

namespace LQMeas {
    QList<const ViMeasType *> ViMeasTypes::all() {
        static const QList<const ViMeasType *> list {
            &ViMeasTypeVibroAccel::type(),
            &ViMeasTypeVibroAccelHf::type(),
            &ViMeasTypeVibroVelocity::type(),
            &ViMeasTypeVibroDisp::type(),
            &ViMeasTypeRotSpeed::type(),
            &ViMeasTypeVoltageDC::type(),
            &ViMeasTypeVoltageGap::type(),
            &ViMeasTypePosition::type(),
            &ViMeasTypeVibroSmax::type(),
            &ViMeasTypeHarmAmpAccel::type(),
            &ViMeasTypeHarmPhaAccel::type(),
            &ViMeasTypeHarmAmpVelocity::type(),
            &ViMeasTypeHarmPhaVelocity::type(),
            &ViMeasTypeHarmAmpDisp::type(),
            &ViMeasTypeHarmPhaDisp::type(),
        };
        return list;
    }

    bool ViMeasType::isAvailable(const ViAcqChannelConfig &chCfg) const {
        bool phasetrig_check = isPhaseTrigRefReqired() ? chCfg.phaseTriggerRef().isValid() : true;
        bool secch_check = isSecondChRequired() ? chCfg.secChRef().isValid() : true;
        return phasetrig_check && secch_check;
    }

}
