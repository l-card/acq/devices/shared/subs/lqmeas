#ifndef LQMEAS_VIMEASGROUPTYPESINGLE_H
#define LQMEAS_VIMEASGROUPTYPESINGLE_H

#include "../ViMeasGroupType.h"

namespace LQMeas {
    class ViMeasType;
    class ViMeasGroupTypeSingle : public ViMeasGroupType {
    public:
        explicit ViMeasGroupTypeSingle(const ViMeasType &type);

        QString id() const override;
        QString displayName() const override;

        QList<const ViMeasType *> measurementTypes() const override;

        bool isAvailable(const ViAcqChannelConfig &chCfg) const override;
    private:
        const ViMeasType *m_type;
    };
}

#endif // LQMEAS_VIMEASGROUPTYPESINGLE_H
