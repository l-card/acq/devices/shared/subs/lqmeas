#include "ViMeasGroupTypeSingle.h"
#include "../ViMeasType.h"
#include <QStringBuilder>

namespace LQMeas {
    ViMeasGroupTypeSingle::ViMeasGroupTypeSingle(const ViMeasType &type) : m_type{&type} {

    }

    QString ViMeasGroupTypeSingle::id() const {
        return QStringLiteral("measgroup_single_") % m_type->id();
    }

    QString ViMeasGroupTypeSingle::displayName() const {
        return m_type->displayName();
    }

    QList<const LQMeas::ViMeasType *> ViMeasGroupTypeSingle::measurementTypes() const {
        return QList<const LQMeas::ViMeasType *>{m_type};
    }

    bool ViMeasGroupTypeSingle::isAvailable(const ViAcqChannelConfig &chCfg) const {
        return m_type->isAvailable(chCfg);
    }
}
