#include "ViMeasConfig.h"
#include "ViMeasGroupConfig.h"
#include "ViMeasGroupType.h"
#include "ViMeasParameterType.h"
#include "ViMeasFilterConfig.h"
#include "ViMeasType.h"
#include "../setpoint/ViSetpointConfig.h"
#include "../../acq/channel/ViAcqChannelConfig.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdConfigKeys.h"
#include "lqmeas/units/std/Relative.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String &cfgkey_meas_type            {StdConfigKeys::type()};
    static const QLatin1String cfgkey_meas_param_type       {"ParamType"};
    static const QLatin1String cfgkey_meas_min_val          {"MinValue"};
    static const QLatin1String cfgkey_meas_max_val          {"MaxValue"};
    static const QLatin1String cfgkey_meas_clamp_val        {"ClampValue"};
    static const QLatin1String cfgkey_meas_nx_val           {"nx"};
    static const QLatin1String cfgkey_meas_change_thresh    {"ChangeThresh"};
    static const QLatin1String cfgkey_meas_trip_multiply    {"TripMult"};
    static const QLatin1String cfgkey_meas_filter_hp        {"FilterHP"};
    static const QLatin1String cfgkey_meas_filter_lp        {"FilterLP"};
    static const QLatin1String cfgkey_meas_db_scale_group   {"MinValue"};
    static const QLatin1String cfgkey_meas_db_scale_en      {StdConfigKeys::enabled()};
    static const QLatin1String cfgkey_meas_db_scale_base_val{"BaseValue"};
    static const QLatin1String cfgkey_meas_sp_group         {"Setpoints"};
    static const QLatin1String cfgkey_meas_sp_alert         {"Alert"};
    static const QLatin1String cfgkey_meas_sp_danger        {"Danger"};


    ViMeasConfig::ViMeasConfig(ViMeasGroupConfig &group, int group_pos) :
        ViChannelConfig{group.viConfig(), &group, true},
        m_group{&group},
        m_type{group.groupType().measurementTypes().at(group_pos)},
        m_group_pos{group_pos},
        m_db_scale_en{dbScaleDefaultEnabled()},
        m_db_scale_base_level{dbScaleDefaultBaseLevel()},
        m_min_value{defaultMinValue()},
        m_max_value{defaultMaxValue()},
        m_clamp_value{defaultClampValue()},
        m_change_thresh{defaultChangeThreshold()},
        m_nx{nxDefaultValue()},
        m_paramType{defaultMeasParam()},
        m_trip_mult{defaultTripMultiply()},
        m_hp_filter{hpFilterIsAvailable() ? new ViMeasFilterConfig{*this, ViMeasFilterConfig::FreqType::HP} : nullptr},
        m_lp_filter{lpFilterIsAvailable() ? new ViMeasFilterConfig{*this, ViMeasFilterConfig::FreqType::LP} : nullptr},
        m_sp_alert{new ViSetpointConfig{*this, ViSetpointConfig::AlarmType::Alert}},
        m_sp_danger{new ViSetpointConfig{*this, ViSetpointConfig::AlarmType::Danger}} {

        if (m_hp_filter) {
            addSubItem(m_hp_filter);
        }

        if (m_lp_filter) {
            addSubItem(m_lp_filter);
        }

        addSubItem(m_sp_alert);
        addSubItem(m_sp_danger);
    }

    ViMeasConfig::ViMeasConfig(ViMeasGroupConfig &group, const ViMeasConfig &origMeasCfg) :
        ViChannelConfig{origMeasCfg, group.viConfig(), &group}, m_group{&group},
        m_type{origMeasCfg.m_type},
        m_group_pos{origMeasCfg.m_group_pos},
        m_db_scale_en{origMeasCfg.m_db_scale_en},
        m_db_scale_base_level{origMeasCfg.m_db_scale_base_level},
        m_min_value{origMeasCfg.m_min_value},
        m_max_value{origMeasCfg.m_max_value},
        m_clamp_value{origMeasCfg.m_clamp_value},
        m_change_thresh{origMeasCfg.m_change_thresh},
        m_nx{origMeasCfg.m_nx},
        m_paramType{origMeasCfg.m_paramType},
        m_trip_mult{origMeasCfg.m_trip_mult},
        m_sp_alert{new ViSetpointConfig{*this, *origMeasCfg.m_sp_alert}},
        m_sp_danger{new ViSetpointConfig{*this, *origMeasCfg.m_sp_danger}} {

        if (origMeasCfg.m_hp_filter) {
            m_hp_filter = new ViMeasFilterConfig{*this, *origMeasCfg.m_hp_filter};
            addSubItem(m_hp_filter);
        }

        if (origMeasCfg.m_lp_filter) {
            m_lp_filter = new ViMeasFilterConfig{*this, *origMeasCfg.m_lp_filter};
            addSubItem(m_lp_filter);
        }

        addSubItem(m_sp_alert);
        addSubItem(m_sp_danger);
    }

    QString ViMeasConfig::origName() const {
        QString ret;
        /* @todo добавить groupInstanceNum */
        ret = m_type->displayName();
        if (m_paramType) {
            ret += QString(" (%1)").arg(m_paramType->displayName());
        }
        return ret;
    }

    const Unit &ViMeasConfig::resultUnit() const {
        return dbScaleIsEnabled() ? LQMeas::Units::Relative::db() : m_type->unit();
    }

    void ViMeasConfig::setDbScaleEnabled(bool en) {
        if (dbScaleSupported()) {
            if (en != m_db_scale_en) {
                m_db_scale_en = en;
                notifyConfigChanged();
            }
        }
    }

    void ViMeasConfig::setDbScaleBaseLevel(double lvl) {
        if (dbScaleSupported()) {
            if (!LQREAL_IS_EQUAL(m_db_scale_base_level, lvl)) {
                m_db_scale_base_level = lvl;
                notifyConfigChanged();
            }
        }
    }

    void ViMeasConfig::setMinValue(double val) {
        if (!LQREAL_IS_EQUAL(m_min_value, val)) {
            m_min_value = val;
            notifyConfigChanged();
        }
    }

    void ViMeasConfig::setMaxValue(double val) {
        if (!LQREAL_IS_EQUAL(m_max_value, val)) {
            m_max_value = val;
            notifyConfigChanged();
        }
    }

    void ViMeasConfig::setClampValue(double val) {
        if (!LQREAL_IS_EQUAL(m_clamp_value, val)) {
            m_clamp_value = val;
            notifyConfigChanged();
        }
    }

    void ViMeasConfig::setChangeThreshold(double val) {
        if (!LQREAL_IS_EQUAL(m_change_thresh, val)) {
            m_change_thresh = val;
            notifyConfigChanged();
        }
    }

    void ViMeasConfig::setTripMultiplty(double val) {
        if (!LQREAL_IS_EQUAL(m_trip_mult, val)) {
            m_trip_mult = val;
            notifyConfigChanged();
        }
    }

    void ViMeasConfig::setNxValue(double val) {
        if (!LQREAL_IS_EQUAL(m_nx, val)) {
            m_nx = val;
            notifyConfigChanged();
        }
    }

    void ViMeasConfig::setParamType(const ViMeasParameterType &paramType) {
        if ((m_paramType != &paramType) && (supportedMeasParams().contains(&paramType))) {
            m_paramType = &paramType;
            notifyConfigChanged();
            if (customName().isEmpty()) {
                Q_EMIT displayNameChanged();
            }
        }
    }

    QString ViMeasConfig::loadTypeId(const QJsonObject &cfgObj) {
        return cfgObj[cfgkey_meas_type].toString();
    }

    double ViMeasConfig::defaultMinValue() const {
        return m_type->defaultMinValue(m_group->channel());
    }

    double ViMeasConfig::defaultMaxValue() const {
        return m_type->defaultMaxValue(m_group->channel());
    }

    double ViMeasConfig::defaultClampValue() const {
        return m_type->defaultClampValue(m_group->channel());
    }

    QList<const ViMeasParameterType *> ViMeasConfig::supportedMeasParams() const {
        return m_type->supportedMeasParams(m_group->channel());
    }

    const ViMeasParameterType *ViMeasConfig::defaultMeasParam() const {
        return m_type->defaultMeasParam(m_group->channel());
    }

    double ViMeasConfig::valueRangeMaxLimit() const {
        return m_type->valueRangeMaxLimit(m_group->channel());
    }

    double ViMeasConfig::valueRangeMinLimit() const {
        return m_type->valueRangeMinLimit(m_group->channel());
    }

    double ViMeasConfig::valueFractDigitCnt() const {
        return m_type->valueFractDigitCnt(m_group->channel());
    }

    double ViMeasConfig::nxDefaultValue() const {
        return m_type->nxDefaultValue(m_group->channel());
    }

    double ViMeasConfig::nxMinValue() const {
        return m_type->nxMinValue(m_group->channel());
    }

    double ViMeasConfig::nxMaxValue() const {
        return m_type->nxMaxValue(m_group->channel());
    }

    bool ViMeasConfig::dbScaleSupported() const {
        return m_type->dbScaleSupported(m_group->channel());
    }

    bool ViMeasConfig::dbScaleDefaultEnabled() const {
        return m_type->dbScaleDefaultEnabled(m_group->channel());
    }

    double ViMeasConfig::dbScaleDefaultBaseLevel() const {
        return m_type->dbScaleDefaultBaseLevel(m_group->channel());
    }

    double ViMeasConfig::defaultChangeThreshold() const {
        return m_type->defaultChangeThreshold(m_group->channel());
    }

    double ViMeasConfig::defaultTripMultiply() const {
        return m_type->defaultTripMultiply(m_group->channel());
    }

    bool ViMeasConfig::hpFilterIsAvailable() const {
        return m_type->hpFilterIsAvailable(m_group->channel());
    }

    bool ViMeasConfig::lpFilterIsAvailable() const {
        return m_type->lpFilterIsAvailable(m_group->channel());
    }

    bool ViMeasConfig::hpFilterIsOptional() const {
        return m_type->hpFilterIsOptional(m_group->channel());
    }

    bool ViMeasConfig::lpFilterIsOptional() const {
        return m_type->lpFilterIsOptional(m_group->channel());
    }

    bool ViMeasConfig::hpFilterDefaultEnabled() const {
        return m_type->hpFilterDefaultEnabled(m_group->channel());
    }

    bool ViMeasConfig::lpFilterDefaultEnabled() const {
        return m_type->lpFilterDefaultEnabled(m_group->channel());
    }

    double ViMeasConfig::hpFilterFreqMin() const {
        return m_type->hpFilterFreqMin(m_group->channel());
    }

    double ViMeasConfig::hpFilterFreqDefault() const {
        return m_type->hpFilterFreqDefault(m_group->channel());
    }

    double ViMeasConfig::hpFilterFreqMax() const {
        return m_type->hpFilterFreqMax(m_group->channel());
    }

    double ViMeasConfig::hpFilterFreqStep() const {
        return m_type->hpFilterFreqStep(m_group->channel());
    }

    double ViMeasConfig::lpFilterFreqMin() const {
        return m_type->lpFilterFreqMin(m_group->channel());
    }

    double ViMeasConfig::lpFilterFreqDefault() const {
        return m_type->lpFilterFreqDefault(m_group->channel());
    }

    double ViMeasConfig::lpFilterFreqMax() const {
        return m_type->lpFilterFreqMax(m_group->channel());
    }

    double ViMeasConfig::lpFilterFreqStep() const {
        return m_type->lpFilterFreqStep(m_group->channel());
    }

    double ViMeasConfig::filtersFreqMaxRatio() const {
        return m_type->filtersFreqMaxRatio(m_group->channel());
    }


    void ViMeasConfig::chCfgSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_meas_type] = m_type->id();
        if (m_paramType) {
            cfgObj[cfgkey_meas_param_type] = m_paramType->id();
        }
        cfgObj[cfgkey_meas_min_val] = m_min_value;
        cfgObj[cfgkey_meas_max_val] = m_max_value;
        cfgObj[cfgkey_meas_clamp_val] = m_clamp_value;
        cfgObj[cfgkey_meas_change_thresh] = m_change_thresh;
        cfgObj[cfgkey_meas_trip_multiply] = m_trip_mult;

        cfgObj[cfgkey_meas_nx_val] = m_nx;

        if (dbScaleSupported()) {
            QJsonObject dbObj;
            dbObj[cfgkey_meas_db_scale_en]       = m_db_scale_en;
            dbObj[cfgkey_meas_db_scale_base_val] = m_db_scale_base_level;
            cfgObj[cfgkey_meas_db_scale_group] = dbObj;
        }


        if (m_hp_filter) {
            saveSubItem(cfgObj, *m_hp_filter, cfgkey_meas_filter_hp);
        }
        if (m_lp_filter) {
            saveSubItem(cfgObj, *m_lp_filter, cfgkey_meas_filter_lp);
        }

        QJsonObject spObj;
        saveSubItem(spObj, *m_sp_alert, cfgkey_meas_sp_alert);
        saveSubItem(spObj, *m_sp_danger, cfgkey_meas_sp_danger);
        cfgObj[cfgkey_meas_sp_group] = spObj;
    }

    void ViMeasConfig::chCfgLoad(const QJsonObject &cfgObj) {
        const QList<const ViMeasParameterType *> &paramTypes {supportedMeasParams()};
        if (!paramTypes.isEmpty()) {
            const QString paramTypeId {cfgObj[cfgkey_meas_param_type].toString()};
            const auto &it {std::find_if(paramTypes.cbegin(), paramTypes.cend(),
                            [&paramTypeId](const ViMeasParameterType *type) {
                                return type->id() == paramTypeId;
            })};
            m_paramType = it != paramTypes.cend() ? *it : defaultMeasParam();
        }
        m_min_value = cfgObj[cfgkey_meas_min_val].toDouble(defaultMinValue());
        m_max_value = cfgObj[cfgkey_meas_max_val].toDouble(defaultMaxValue());
        m_clamp_value = cfgObj[cfgkey_meas_clamp_val].toDouble(defaultClampValue());
        m_change_thresh = cfgObj[cfgkey_meas_change_thresh].toDouble(defaultChangeThreshold());
        m_trip_mult = cfgObj[cfgkey_meas_trip_multiply].toDouble(defaultTripMultiply());

        m_nx = cfgObj[cfgkey_meas_nx_val].toDouble(nxDefaultValue());

        if (dbScaleSupported()) {
            const QJsonObject &dbObj {cfgObj[cfgkey_meas_db_scale_group].toObject()};
            m_db_scale_en   = dbObj[cfgkey_meas_db_scale_en].toBool(dbScaleDefaultEnabled());
            m_db_scale_base_level = dbObj[cfgkey_meas_db_scale_base_val].toDouble(dbScaleDefaultBaseLevel());
        }

        if (m_hp_filter) {
            loadSubItem(cfgObj, *m_hp_filter, cfgkey_meas_filter_hp);
        }
        if (m_lp_filter) {
            loadSubItem(cfgObj, *m_lp_filter, cfgkey_meas_filter_lp);
        }

        const QJsonObject &spObj {cfgObj[cfgkey_meas_sp_group].toObject()};
        loadSubItem(spObj, *m_sp_alert, cfgkey_meas_sp_alert);
        loadSubItem(spObj, *m_sp_danger, cfgkey_meas_sp_danger);
    }

    void ViMeasConfig::setMeasNum(int num)  {
        if (num != m_meas_num) {
            m_meas_num = num;
            Q_EMIT measNumberChanged(m_meas_num);
            notifyConfigChanged();
        }
    }
}
