#include "ViMeasParameterType.h"

namespace LQMeas {
    const ViMeasParameterType &LQMeas::ViMeasParameterTypes::rms() {
        static const class ViMeasParameterTypeRMS : public ViMeasParameterType {
        public:
            QString id() const override {return QStringLiteral("vi_meas_partype_rms");}
            QString displayName() const override {return LQMeas::ViMeasParameterTypes::tr("RMS");}
        } t;
        return t;
    }

    const ViMeasParameterType &ViMeasParameterTypes::pp() {
        static const class ViMeasParameterTypePP : public ViMeasParameterType {
        public:
            QString id() const override {return QStringLiteral("vi_meas_partype_pp");}
            QString displayName() const override {return LQMeas::ViMeasParameterTypes::tr("Peak-to-peak");}
        } t;
        return t;
    }

    const ViMeasParameterType &ViMeasParameterTypes::peak() {
        static const class ViMeasParameterTypePeak : public ViMeasParameterType {
        public:
            QString id() const override {return QStringLiteral("vi_meas_partype_peak");}
            QString displayName() const override {return LQMeas::ViMeasParameterTypes::tr("Peak");}
        } t;
        return t;
    }
}
