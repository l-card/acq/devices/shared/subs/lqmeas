#include "ViMeasCondType.h"

namespace LQMeas {
    const ViMeasCondType &ViMeasCondTypes::alert() {
        static const class ViMeasCondTypeAlert : public ViMeasCondType {
        public:
            QString id() const override {return QStringLiteral("vi_meas_condtype_alert");}
            QString displayName() const override {return tr("Alert");}
        } t;
        return t;
    }

    const ViMeasCondType &ViMeasCondTypes::danger() {
        static const class ViMeasCondTypeDanger : public ViMeasCondType {
        public:
            QString id() const override {return QStringLiteral("vi_meas_condtype_danger");}
            QString displayName() const override {return tr("Danger");}
        } t;
        return t;
    }

    const ViMeasCondType &ViMeasCondTypes::failure() {
        static const class ViMeasCondTypeFailure : public ViMeasCondType {
        public:
            QString id() const override {return QStringLiteral("vi_meas_condtype_failure");}
            QString displayName() const override {return tr("Failure");}
        } t;
        return t;
    }

    const ViMeasCondType &ViMeasCondTypes::changeThreshold() {
        static const class ViMeasCondTypeChangeThreshold : public ViMeasCondType {
        public:
            QString id() const override {return QStringLiteral("vi_meas_condtype_change_thresh");}
            QString displayName() const override {return tr("Change Threshold");}
        } t;
        return t;
    }

    const QList<const ViMeasCondType *> &ViMeasCondTypes::all() {
        static const QList<const ViMeasCondType *> list {
            &alert(),
            &danger(),
            &failure(),
            &changeThreshold(),
        };
        return list;
    }
}
