#ifndef LQMEAS_VIACQMEASTYPE_H
#define LQMEAS_VIACQMEASTYPE_H

#include "lqmeas/EnumNamedValue.h"
#include <QList>
#include <QMetaType>

namespace LQMeas {
    class Unit;
    class ViMeasParameterType;
    class ViAcqChannelConfig;

    class ViMeasType : public EnumNamedValue<QString> {
    public:

        virtual bool   isHarmonic() const {return false;}
        virtual bool   isPhaseTrigRefReqired() const {return isHarmonic();}
        virtual bool   isSecondChRequired() const {return false;}

        virtual bool   isAvailable(const ViAcqChannelConfig &chCfg) const;

        virtual const Unit &unit() const = 0;
        virtual double defaultMinValue(const ViAcqChannelConfig &chCfg) const {return 0;}
        virtual double defaultMaxValue(const ViAcqChannelConfig &chCfg) const {return 100;}
        virtual double defaultClampValue(const ViAcqChannelConfig &chCfg) const {return 0;}

        virtual double valueRangeMaxLimit(const ViAcqChannelConfig &chCfg) const {return 9999;}
        virtual double valueRangeMinLimit(const ViAcqChannelConfig &chCfg) const {return -9999;}
        virtual double valueFractDigitCnt(const ViAcqChannelConfig &chCfg) const {return 3;}

        virtual double nxDefaultValue(const ViAcqChannelConfig &chCfg) const {return 1;}
        virtual double nxMinValue(const ViAcqChannelConfig &chCfg) const {return 0.1;}
        virtual double nxMaxValue(const ViAcqChannelConfig &chCfg) const {return 10;}

        virtual QList<const ViMeasParameterType *> supportedMeasParams(const ViAcqChannelConfig &chCfg) const {
            return QList<const ViMeasParameterType *>{};
        }
        virtual const ViMeasParameterType *defaultMeasParam(const ViAcqChannelConfig &chCfg) const {return nullptr;}



        virtual bool   dbScaleSupported(const ViAcqChannelConfig &chCfg) const {return false;}
        virtual bool   dbScaleDefaultEnabled(const ViAcqChannelConfig &chCfg) const {return false;}
        virtual double dbScaleDefaultBaseLevel(const ViAcqChannelConfig &chCfg) const {return 0.001;}
        virtual double defaultChangeThreshold(const ViAcqChannelConfig &chCfg) const {return 3;}
        virtual double defaultTripMultiply(const ViAcqChannelConfig &chCfg) const {return 3.;}
        /* набор виртуальных функций, описывающих, поддерживает ли измерение
           использование фильров */
        virtual bool hpFilterIsAvailable(const ViAcqChannelConfig &chCfg) const {return false;}
        virtual bool lpFilterIsAvailable(const ViAcqChannelConfig &chCfg) const {return false;}
        /* признак, являются ли фильтры опциональными (отключаемыми)
           имеет смысл, только если измерение вообще их использует */
        virtual bool hpFilterIsOptional(const ViAcqChannelConfig &chCfg) const {return false;}
        virtual bool lpFilterIsOptional(const ViAcqChannelConfig &chCfg) const {return false;}

        virtual bool hpFilterDefaultEnabled(const ViAcqChannelConfig &chCfg) const {return true;}
        virtual bool lpFilterDefaultEnabled(const ViAcqChannelConfig &chCfg) const {return true;}
        /* границы фильтров */
        virtual double hpFilterFreqMin(const ViAcqChannelConfig &chCfg) const {return 0;}
        virtual double hpFilterFreqDefault(const ViAcqChannelConfig &chCfg) const {return hpFilterFreqMin(chCfg);}
        virtual double hpFilterFreqMax(const ViAcqChannelConfig &chCfg) const {return 0;}
        virtual double hpFilterFreqStep(const ViAcqChannelConfig &chCfg) const {return 0.01;}
        virtual double lpFilterFreqMin(const ViAcqChannelConfig &chCfg) const {return 0;}
        virtual double lpFilterFreqDefault(const ViAcqChannelConfig &chCfg) const {return lpFilterFreqMax(chCfg);}
        virtual double lpFilterFreqMax(const ViAcqChannelConfig &chCfg) const {return 0;}
        virtual double lpFilterFreqStep(const ViAcqChannelConfig &chCfg) const {return 0.01;}
        virtual double filtersFreqMaxRatio(const ViAcqChannelConfig &chCfg) const {return 1000;}
    };

    class ViMeasTypes {
    public:
        static QList<const ViMeasType *> all();
    };
}

Q_DECLARE_METATYPE(const LQMeas::ViMeasType *);


#endif // LQMEAS_VIACQMEASTYPE_H
