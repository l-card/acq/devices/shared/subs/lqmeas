#ifndef LQMEAS_VIMEASGROUPTYPE_H
#define LQMEAS_VIMEASGROUPTYPE_H


#include "lqmeas/EnumNamedValue.h"
#include <QMetaType>
#include <QList>

namespace LQMeas {
    class ViMeasType;
class ViAcqChannelConfig;

    class ViMeasGroupType : public EnumNamedValue<QString> {
    public:
        virtual ~ViMeasGroupType();
        virtual QList<const ViMeasType *> measurementTypes() const = 0;
        virtual bool isAvailable(const ViAcqChannelConfig &chCfg) const = 0;
    };
}

#endif // LQMEAS_VIMEASGROUPTYPE_H
