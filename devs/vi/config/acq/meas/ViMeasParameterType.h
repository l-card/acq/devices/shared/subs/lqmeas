#ifndef LQMEAS_VIMEASPARAMETERTYPE_H
#define LQMEAS_VIMEASPARAMETERTYPE_H

#include "lqmeas/EnumNamedValue.h"
#include <QObject>

namespace LQMeas {
    class ViMeasParameterType : public EnumNamedValue<QString> {
    public:

    };

    class ViMeasParameterTypes : public QObject {
        Q_OBJECT
    public:

        static const ViMeasParameterType &rms();
        static const ViMeasParameterType &pp();
        static const ViMeasParameterType &peak();
    };
}

Q_DECLARE_METATYPE(const LQMeas::ViMeasParameterType *);

#endif // LQMEAS_VIMEASPARAMETERTYPE_H
