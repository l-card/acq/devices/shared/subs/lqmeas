#include "ViMeasGroupConfig.h"
#include "ViMeasConfig.h"
#include "ViMeasType.h"
#include "ViMeasGroupType.h"
#include "lqmeas/devs/vi/config/acq/channel/ViAcqChannelConfig.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>
#include <QJsonArray>

namespace LQMeas {
    static const QLatin1String cfgkey_measgrp_meas_arr    {"MeasList"};
    static const QLatin1String &cfgkey_measgrp_type       {StdConfigKeys::type()};

    ViMeasGroupConfig::ViMeasGroupConfig(ViAcqChannelConfig &chCfg, const ViMeasGroupType &type) :
        ViConfigItem{chCfg.viModuleTypeInfo(), &chCfg}, m_ch{&chCfg}, m_type{&type} {
        const QList<const ViMeasType *> mtypes {type.measurementTypes()};
        for (int meas_pos{0}; meas_pos < mtypes.size(); ++meas_pos) {
            ViMeasConfig *measCfg {new ViMeasConfig{*this, meas_pos}};
            m_measList += measCfg;
            addSubItem(measCfg);
        }
    }

    ViMeasGroupConfig::ViMeasGroupConfig(ViAcqChannelConfig &chCfg,
                                         const ViMeasGroupConfig &origMeasGroupCfg) :
        ViConfigItem{chCfg.viModuleTypeInfo(), &chCfg}, m_ch{&chCfg}, m_type{origMeasGroupCfg.m_type} {

        for (ViMeasConfig *origMeasCfg : origMeasGroupCfg.m_measList) {
            ViMeasConfig *measCfg {new ViMeasConfig{*this, *origMeasCfg}};
            m_measList += measCfg;
            addSubItem(measCfg);
        }
    }

    const ViConfig &ViMeasGroupConfig::viConfig() const {
        return m_ch->viConfig();
    }

    ViConfig &ViMeasGroupConfig::viConfig() {
        return m_ch->viConfig();
    }

    QString ViMeasGroupConfig::loadTypeId(const QJsonObject &cfgObj) {
        return cfgObj[cfgkey_measgrp_type].toString();
    }

    void ViMeasGroupConfig::protSave(QJsonObject &cfgObj) const  {
        cfgObj[cfgkey_measgrp_type] = m_type->id();
        QJsonArray measArray;
        for (const ViMeasConfig *meas : m_measList) {
            QJsonObject measObj;
            meas->save(measObj);
            measArray.append(measObj);
        }
        cfgObj[cfgkey_measgrp_meas_arr] = measArray;
    }

    void ViMeasGroupConfig::protLoad(const QJsonObject &cfgObj) {
        const QJsonArray &measArray {cfgObj[cfgkey_measgrp_meas_arr].toArray()};
        for (const auto &it : measArray) {
            const QJsonObject &measObj {it.toObject()};
            /* сопоставляем настройки измерениям группы по id типа */
            const QString measTypeId {ViMeasConfig::loadTypeId(measObj)};
            const auto &measIt {std::find_if(m_measList.cbegin(), m_measList.cend(),
                                [&measTypeId](ViMeasConfig *measCfg){return measCfg->type().id() == measTypeId;})};
            if (measIt != m_measList.cend()) {
                (*measIt)->load(measObj);
            }
        }
    }
}
