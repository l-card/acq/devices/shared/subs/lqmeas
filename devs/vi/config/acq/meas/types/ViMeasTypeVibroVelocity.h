#ifndef LQMEAS_VIMEASTYPEVIBROVELOCITY_H
#define LQMEAS_VIMEASTYPEVIBROVELOCITY_H

#include "../ViMeasType.h"

namespace LQMeas {
    class ViMeasTypeVibroVelocity : public ViMeasType {
    public:
        static const QString &typeID();
        static const ViMeasTypeVibroVelocity &type();

        QString id() const override {return typeID();}
        QString displayName() const override;
        double defaultMaxValue(const ViAcqChannelConfig &chCfg) const override {return 25;}

        bool   dbScaleSupported(const ViAcqChannelConfig &chCfg) const override {return true;}

        double valueRangeMinLimit(const ViAcqChannelConfig &chCfg) const override {return 0;}

        const Unit &unit() const override;

        QList<const ViMeasParameterType *> supportedMeasParams(const ViAcqChannelConfig &chCfg) const override;
        virtual const ViMeasParameterType *defaultMeasParam(const ViAcqChannelConfig &chCfg) const override;


        bool hpFilterIsAvailable(const ViAcqChannelConfig &chCfg) const override    {return true;}
        double hpFilterFreqMin(const ViAcqChannelConfig &chCfg) const override      {return 2;}
        double hpFilterFreqMax(const ViAcqChannelConfig &chCfg) const override      {return 4000;}
        double hpFilterFreqDefault(const ViAcqChannelConfig &chCfg) const override  {return 10;}
        bool lpFilterIsAvailable(const ViAcqChannelConfig &chCfg) const override    {return true;}
        double lpFilterFreqMin(const ViAcqChannelConfig &chCfg) const override      {return 100;}
        double lpFilterFreqMax(const ViAcqChannelConfig &chCfg) const override      {return 10000;}
        double lpFilterFreqDefault(const ViAcqChannelConfig &chCfg) const override  {return 1000;}
    };
}


#endif // LQMEAS_VIMEASTYPEVIBROVELOCITY_H
