#include "ViMeasTypeVibroAccelHf.h"
#include "../ViMeasConfig.h"
#include "../ViMeasParameterType.h"
#include "lqmeas/units/std/Acceleration.h"

namespace LQMeas {

    const QString &ViMeasTypeVibroAccelHf::typeID() {
        static const QString t{QStringLiteral("vi_meas_vibro_accel_hf")};
        return t;
    }

    const ViMeasTypeVibroAccelHf &ViMeasTypeVibroAccelHf::type() {
        static const ViMeasTypeVibroAccelHf t;
        return t;
    }

    QString ViMeasTypeVibroAccelHf::displayName() const {
        return ViMeasConfig::tr("Acceleration (HF)");
    }

    const Unit &ViMeasTypeVibroAccelHf::unit() const {
        return Units::Acceleration::m_s2();
    }

    QList<const ViMeasParameterType *> ViMeasTypeVibroAccelHf::supportedMeasParams(const ViAcqChannelConfig &chCfg) const {
        static const QList<const ViMeasParameterType *> params {
            &ViMeasParameterTypes::rms(),
            &ViMeasParameterTypes::pp(),
            &ViMeasParameterTypes::peak(),
        };
        return params;
    }

    const ViMeasParameterType *ViMeasTypeVibroAccelHf::defaultMeasParam(const ViAcqChannelConfig &chCfg) const {
        return &ViMeasParameterTypes::rms();
    }
}
