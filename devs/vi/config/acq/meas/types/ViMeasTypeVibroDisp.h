#ifndef LQMEAS_VIMEASTYPEVIBRODISP_H
#define LQMEAS_VIMEASTYPEVIBRODISP_H

#include "../ViMeasType.h"

namespace LQMeas {
    class ViMeasTypeVibroDisp : public ViMeasType {
    public:
        static const QString &typeID();
        static const ViMeasTypeVibroDisp &type();

        QString id() const override {return typeID();}
        QString displayName() const override;
        double defaultMaxValue(const ViAcqChannelConfig &chCfg) const override {return 125;}

        double valueRangeMinLimit(const ViAcqChannelConfig &chCfg) const override {return 0;}

        const Unit &unit() const override;

        QList<const ViMeasParameterType *> supportedMeasParams(const ViAcqChannelConfig &chCfg) const override;
        virtual const ViMeasParameterType *defaultMeasParam(const ViAcqChannelConfig &chCfg) const override;

        bool   dbScaleSupported(const ViAcqChannelConfig &chCfg) const override     {return true;}

        bool hpFilterIsAvailable(const ViAcqChannelConfig &chCfg) const override    {return true;}
        bool hpFilterIsOptional(const ViAcqChannelConfig &chCfg) const override     {return false;}
        double hpFilterFreqMin(const ViAcqChannelConfig &chCfg) const override      {return 2;}
        double hpFilterFreqMax(const ViAcqChannelConfig &chCfg) const override      {return 4000;}
        double hpFilterFreqDefault(const ViAcqChannelConfig &chCfg) const override;
        bool lpFilterIsAvailable(const ViAcqChannelConfig &chCfg) const override    {return true;}
        bool lpFilterIsOptional(const ViAcqChannelConfig &chCfg) const override     {return false;}
        double lpFilterFreqMin(const ViAcqChannelConfig &chCfg) const override      {return 100;}
        double lpFilterFreqMax(const ViAcqChannelConfig &chCfg) const override      {return 10000;}
        double lpFilterFreqDefault(const ViAcqChannelConfig &chCfg) const override;
    };
}


#endif // LQMEAS_VIMEASTYPEVIBRODISP_H
