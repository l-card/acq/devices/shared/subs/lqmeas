#ifndef LQMEAS_VIMEASTYPEUNKNOWN_H
#define LQMEAS_VIMEASTYPEUNKNOWN_H


#include "../ViMeasType.h"

namespace LQMeas {
    class ViMeasTypeUnknown : public ViMeasType {
    public:
        static const QString &typeID();
        static const ViMeasTypeUnknown &type();

        QString id() const override {return typeID();}
        QString displayName() const override;

        const Unit &unit() const override;
    };
}

#endif // LQMEAS_VIMEASTYPEUNKNOWN_H
