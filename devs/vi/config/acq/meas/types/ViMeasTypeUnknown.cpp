#include "ViMeasTypeUnknown.h"
#include "../ViMeasConfig.h"
#include "lqmeas/units/std/Special.h"

namespace LQMeas {

const QString &ViMeasTypeUnknown::typeID() {
    static const QString t{QStringLiteral("vi_meas_unknown")};
    return t;
}

const ViMeasTypeUnknown &ViMeasTypeUnknown::type() {
    static const ViMeasTypeUnknown t;
    return t;
}

QString ViMeasTypeUnknown::displayName() const {
    return ViMeasConfig::tr("Unknown");
}

const Unit &ViMeasTypeUnknown::unit() const {
    return Units::Special::unknown();
}

}
