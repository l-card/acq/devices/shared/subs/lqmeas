#include "ViMeasTypePosition.h"
#include "../ViMeasConfig.h"
#include "lqmeas/units/std/Displacement.h"

namespace LQMeas {
    const QString &ViMeasTypePosition::typeID(){
        static const QString t{QStringLiteral("vi_meas_position")};
        return t;
    }

    const ViMeasTypePosition &ViMeasTypePosition::type() {
        static const ViMeasTypePosition t;
        return t;
    }

    QString ViMeasTypePosition::displayName() const     {
        return ViMeasConfig::tr("Position");
    }

    double ViMeasTypePosition::defaultMinValue(const ViAcqChannelConfig &chCfg) const {
        /* согласно 7.4.2.1 */
        return -1000;
    }

    double ViMeasTypePosition::defaultMaxValue(const ViAcqChannelConfig &chCfg) const {
        /* согласно 7.4.2.1 */
        return 1000;
    }

    const Unit &ViMeasTypePosition::unit() const  {
        return Units::Displacement::um();
    }
}
