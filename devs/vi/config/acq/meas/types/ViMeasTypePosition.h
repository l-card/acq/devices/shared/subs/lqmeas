#ifndef LQMEAS_VIMEASTYPEPOSITION_H
#define LQMEAS_VIMEASTYPEPOSITION_H

#include "../ViMeasType.h"

namespace LQMeas {
    class ViMeasTypePosition : public ViMeasType {
    public:
        static const QString &typeID();
        static const ViMeasTypePosition &type();

        QString id() const override {return typeID();}
        QString displayName() const override;

        double defaultMinValue(const ViAcqChannelConfig &chCfg) const override;
        double defaultMaxValue(const ViAcqChannelConfig &chCfg) const override;

        const Unit &unit() const override;
    };
}

#endif // LQMEAS_VIMEASTYPEPOSITION_H
