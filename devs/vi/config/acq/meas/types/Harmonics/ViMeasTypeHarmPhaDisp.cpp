#include "ViMeasTypeHarmPhaDisp.h"
#include "../ViMeasTypeVibroDisp.h"
#include "../../ViMeasConfig.h"

namespace LQMeas {
    const ViMeasType &ViMeasTypeHarmPhaDisp::baseMeasType() const {
        return ViMeasTypeVibroDisp::type();
    }

    const QString &ViMeasTypeHarmPhaDisp::typeID()  {
        static const QString t{QStringLiteral("vi_meas_vibro_disp_nx_pha")};
        return t;
    }

    const ViMeasTypeHarmPhaDisp &ViMeasTypeHarmPhaDisp::type() {
        static const ViMeasTypeHarmPhaDisp t;
        return t;
    }

    QString ViMeasTypeHarmPhaDisp::displayName() const {
        return ViMeasConfig::tr("nX Phase of Displacement");
    }
}

