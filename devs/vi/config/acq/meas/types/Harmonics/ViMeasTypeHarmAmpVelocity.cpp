#include "ViMeasTypeHarmAmpVelocity.h"
#include "../ViMeasTypeVibroVelocity.h"
#include "../../ViMeasConfig.h"


namespace LQMeas {
    const ViMeasType &ViMeasTypeHarmAmpVelocity::baseMeasType() const {
        return ViMeasTypeVibroVelocity::type();
    }

    const QString &ViMeasTypeHarmAmpVelocity::typeID() {
        static const QString t{QStringLiteral("vi_meas_vibro_vel_nx_amp")};
        return t;
    }

    const ViMeasTypeHarmAmpVelocity &ViMeasTypeHarmAmpVelocity::type() {
        static const ViMeasTypeHarmAmpVelocity t;
        return t;
    }

    QString ViMeasTypeHarmAmpVelocity::displayName() const {
        return ViMeasConfig::tr("nX Amplitude of Velocity");
    }
}
