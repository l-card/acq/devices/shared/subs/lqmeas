#ifndef LQMEAS_VIMEASTYPEHARMAMPACCEL_H
#define LQMEAS_VIMEASTYPEHARMAMPACCEL_H

#include "ViMeasTypeHarmAmpBase.h"

namespace LQMeas {
    class ViMeasTypeHarmAmpAccel : public ViMeasTypeHarmAmpBase {
    public:
        const ViMeasType &baseMeasType() const override;

        static const QString &typeID();
        static const ViMeasTypeHarmAmpAccel &type();

        QString id() const override {return typeID();}
        QString displayName() const override;
    };
}

#endif // VIMEASTYPEHARMAMPACCEL_H
