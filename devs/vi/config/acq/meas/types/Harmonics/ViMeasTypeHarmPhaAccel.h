#ifndef LQMEAS_VIMEASTYPEHARMPHAACCEL_H
#define LQMEAS_VIMEASTYPEHARMPHAACCEL_H

#include "ViMeasTypeHarmPhaBase.h"

namespace LQMeas {
    class ViMeasTypeHarmPhaAccel : public ViMeasTypeHarmPhaBase {
    public:
        const ViMeasType &baseMeasType() const override;

        static const QString &typeID();
        static const ViMeasTypeHarmPhaAccel &type();

        QString id() const override {return typeID();}
        QString displayName() const override;
    };
}

#endif // LQMEAS_VIMEASTYPEHARMPHAACCEL_H
