#ifndef LQMEAS_VIMEASTYPEHARMPHAVELOCITY_H
#define LQMEAS_VIMEASTYPEHARMPHAVELOCITY_H

#include "ViMeasTypeHarmPhaBase.h"

namespace LQMeas {
    class ViMeasTypeHarmPhaVelocity : public ViMeasTypeHarmPhaBase {
    public:
        const ViMeasType &baseMeasType() const override;

        static const QString &typeID();
        static const ViMeasTypeHarmPhaVelocity &type();

        QString id() const override {return typeID();}
        QString displayName() const override;
    };
}


#endif // LQMEAS_VIMEASTYPEHARMPHAVELOCITY_H
