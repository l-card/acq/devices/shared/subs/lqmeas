#include "ViMeasTypeHarmAmpBase.h"
#include "../../ViMeasParameterType.h"

namespace LQMeas {
    QList<const LQMeas::ViMeasParameterType *> ViMeasTypeHarmAmpBase::supportedMeasParams(const ViAcqChannelConfig &chCfg) const {
        static const QList<const ViMeasParameterType *> params {
            &ViMeasParameterTypes::rms(),
            &ViMeasParameterTypes::peak(),
        };
        return params;
    }

    const ViMeasParameterType *ViMeasTypeHarmAmpBase::defaultMeasParam(const ViAcqChannelConfig &chCfg) const {
        return &ViMeasParameterTypes::peak();
    }
}
