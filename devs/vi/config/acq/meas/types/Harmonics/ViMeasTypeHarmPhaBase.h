#ifndef LQMEAS_VIMEASTYPEHARMPHABASE_H
#define LQMEAS_VIMEASTYPEHARMPHABASE_H


#include "../../ViMeasType.h"

namespace LQMeas {
    class ViMeasTypeHarmPhaBase : public ViMeasType {
    public:
        virtual const ViMeasType &baseMeasType() const = 0;

        bool isHarmonic() const override {return true;}


        const Unit &unit() const override;
        double defaultMinValue(const ViAcqChannelConfig &chCfg) const override {return 0;}
        double defaultMaxValue(const ViAcqChannelConfig &chCfg) const override {return 360;}
        double defaultClampValue(const ViAcqChannelConfig &chCfg) const override {return 0;}

        double valueRangeMaxLimit(const ViAcqChannelConfig &chCfg) const override {return 0;}
        double valueRangeMinLimit(const ViAcqChannelConfig &chCfg) const override {return 360;}
        double valueFractDigitCnt(const ViAcqChannelConfig &chCfg) const override {return 2;}
    };
}


#endif // LQMEAS_VIMEASTYPEHARMPHABASE_H
