#include "ViMeasTypeHarmAmpDisp.h"
#include "../ViMeasTypeVibroDisp.h"
#include "../../ViMeasConfig.h"


namespace LQMeas {
    const ViMeasType &ViMeasTypeHarmAmpDisp::baseMeasType() const {
        return ViMeasTypeVibroDisp::type();
    }

    const QString &ViMeasTypeHarmAmpDisp::typeID() {
        static const QString t{QStringLiteral("vi_meas_vibro_disp_nx_amp")};
        return t;
    }

    const ViMeasTypeHarmAmpDisp &ViMeasTypeHarmAmpDisp::type() {
        static const ViMeasTypeHarmAmpDisp t;
        return t;
    }

    QString ViMeasTypeHarmAmpDisp::displayName() const {
        return ViMeasConfig::tr("nX Amplitude of Displacement");
    }
}
