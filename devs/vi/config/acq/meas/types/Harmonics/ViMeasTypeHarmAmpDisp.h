#ifndef LQMEAS_VIMEASTYPEHARMAMPDISP_H
#define LQMEAS_VIMEASTYPEHARMAMPDISP_H

#include "ViMeasTypeHarmAmpBase.h"

namespace LQMeas {
    class ViMeasTypeHarmAmpDisp : public ViMeasTypeHarmAmpBase {
    public:
        const ViMeasType &baseMeasType() const override;

        static const QString &typeID();
        static const ViMeasTypeHarmAmpDisp &type();

        QString id() const override {return typeID();}
        QString displayName() const override;
    };
}

#endif // LQMEAS_VIMEASTYPEHARMAMPDISP_H
