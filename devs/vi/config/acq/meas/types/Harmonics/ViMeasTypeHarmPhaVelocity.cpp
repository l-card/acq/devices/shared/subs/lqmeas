#include "ViMeasTypeHarmPhaVelocity.h"
#include "../ViMeasTypeVibroVelocity.h"
#include "../../ViMeasConfig.h"

namespace LQMeas {
const ViMeasType &ViMeasTypeHarmPhaVelocity::baseMeasType() const {
    return ViMeasTypeVibroVelocity::type();
}

const QString &ViMeasTypeHarmPhaVelocity::typeID()  {
    static const QString t{QStringLiteral("vi_meas_vibro_vel_nx_pha")};
    return t;
}

const ViMeasTypeHarmPhaVelocity &ViMeasTypeHarmPhaVelocity::type() {
    static const ViMeasTypeHarmPhaVelocity t;
    return t;
}

QString ViMeasTypeHarmPhaVelocity::displayName() const {
    return ViMeasConfig::tr("nX Phase of Velocity");
}
}
