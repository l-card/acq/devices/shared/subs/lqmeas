#ifndef LQMEAS_VIMEASTYPEHARMAMPVELOCITY_H
#define LQMEAS_VIMEASTYPEHARMAMPVELOCITY_H

#include "ViMeasTypeHarmAmpBase.h"

namespace LQMeas {
    class ViMeasTypeHarmAmpVelocity : public ViMeasTypeHarmAmpBase {
    public:
        const ViMeasType &baseMeasType() const override;

        static const QString &typeID();
        static const ViMeasTypeHarmAmpVelocity &type();

        QString id() const override {return typeID();}
        QString displayName() const override;
    };
}

#endif // LQMEAS_VIMEASTYPEHARMAMPVELOCITY_H
