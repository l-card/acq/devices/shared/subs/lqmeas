#include "ViMeasTypeHarmAmpAccel.h"
#include "../ViMeasTypeVibroAccel.h"
#include "../../ViMeasConfig.h"

namespace LQMeas {
    const ViMeasType &ViMeasTypeHarmAmpAccel::baseMeasType() const {
        return ViMeasTypeVibroAccel::type();
    }

    const QString &ViMeasTypeHarmAmpAccel::typeID() {
        static const QString t{QStringLiteral("vi_meas_vibro_accel_nx_amp")};
        return t;
    }

    const ViMeasTypeHarmAmpAccel &ViMeasTypeHarmAmpAccel::type() {
        static const ViMeasTypeHarmAmpAccel t;
        return t;
    }

    QString ViMeasTypeHarmAmpAccel::displayName() const     {
        return ViMeasConfig::tr("nX Amplitude of Acceleration");
    }

}
