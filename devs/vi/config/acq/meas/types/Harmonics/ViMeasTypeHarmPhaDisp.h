#ifndef LQMEAS_VIMEASTYPEHARMPHADISP_H
#define LQMEAS_VIMEASTYPEHARMPHADISP_H

#include "ViMeasTypeHarmPhaBase.h"

namespace LQMeas {
    class ViMeasTypeHarmPhaDisp : public ViMeasTypeHarmPhaBase {
    public:
        const ViMeasType &baseMeasType() const override;

        static const QString &typeID();
        static const ViMeasTypeHarmPhaDisp &type();

        QString id() const override {return typeID();}
        QString displayName() const override;
    };
}

#endif // LQMEAS_VIMEASTYPEHARMPHADISP_H
