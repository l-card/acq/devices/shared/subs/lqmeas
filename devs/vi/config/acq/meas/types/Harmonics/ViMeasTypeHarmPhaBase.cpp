#include "ViMeasTypeHarmPhaBase.h"
#include "lqmeas/units/std/Angle.h"

namespace LQMeas {


    const Unit &ViMeasTypeHarmPhaBase::unit() const {
        return LQMeas::Units::Angle::degree();
    }
}
