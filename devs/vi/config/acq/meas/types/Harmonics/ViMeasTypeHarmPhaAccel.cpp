#include "ViMeasTypeHarmPhaAccel.h"
#include "../ViMeasTypeVibroAccel.h"
#include "../../ViMeasConfig.h"

namespace LQMeas {
    const ViMeasType &ViMeasTypeHarmPhaAccel::baseMeasType() const {
        return ViMeasTypeVibroAccel::type();
    }

    const QString &ViMeasTypeHarmPhaAccel::typeID()  {
        static const QString t{QStringLiteral("vi_meas_vibro_accel_nx_pha")};
        return t;
    }

    const ViMeasTypeHarmPhaAccel &ViMeasTypeHarmPhaAccel::type() {
        static const ViMeasTypeHarmPhaAccel t;
        return t;
    }

    QString ViMeasTypeHarmPhaAccel::displayName() const {
        return ViMeasConfig::tr("nX Phase of Acceleration");
    }
}

