#ifndef LQMEAS_VIMEASTYPEHARMAMPBASE_H
#define LQMEAS_VIMEASTYPEHARMAMPBASE_H

#include "../../ViMeasType.h"

namespace LQMeas {
    class ViMeasTypeHarmAmpBase : public ViMeasType {
    public:
        virtual const ViMeasType &baseMeasType() const = 0;

        bool isHarmonic() const override {return true;}

        QList<const ViMeasParameterType *> supportedMeasParams(const ViAcqChannelConfig &chCfg) const override;
        virtual const ViMeasParameterType *defaultMeasParam(const ViAcqChannelConfig &chCfg) const override;


        const Unit &unit() const override {return baseMeasType().unit();}
        double defaultMinValue(const ViAcqChannelConfig &chCfg) const override {return baseMeasType().defaultMinValue(chCfg);}
        double defaultMaxValue(const ViAcqChannelConfig &chCfg) const override {return baseMeasType().defaultMaxValue(chCfg);}
        double defaultClampValue(const ViAcqChannelConfig &chCfg) const override {return baseMeasType().defaultClampValue(chCfg);}

        double valueRangeMaxLimit(const ViAcqChannelConfig &chCfg) const override {return baseMeasType().valueRangeMaxLimit(chCfg);}
        double valueRangeMinLimit(const ViAcqChannelConfig &chCfg) const override {return baseMeasType().valueRangeMinLimit(chCfg);}
        double valueFractDigitCnt(const ViAcqChannelConfig &chCfg) const override {return baseMeasType().valueFractDigitCnt(chCfg);}
    };
}

#endif // LQMEAS_VIMEASTYPEHARMAMPBASE_H
