#ifndef LQMEAS_VIMEASTYPEVOLTAGEGAP_H
#define LQMEAS_VIMEASTYPEVOLTAGEGAP_H

#include "../ViMeasType.h"

namespace LQMeas {
    class ViMeasTypeVoltageGap : public ViMeasType {
    public:
        static const QString &typeID();
        static const ViMeasTypeVoltageGap &type();

        QString id() const override {return typeID();}
        QString displayName() const override;

        double defaultMinValue(const ViAcqChannelConfig &chCfg) const override;
        double defaultMaxValue(const ViAcqChannelConfig &chCfg) const override;

        const Unit &unit() const override;
    };
}

#endif // VIMEASTYPEVOLTAGEGAP_H
