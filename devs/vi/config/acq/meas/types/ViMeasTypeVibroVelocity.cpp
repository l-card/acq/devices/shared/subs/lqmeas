#include "ViMeasTypeVibroVelocity.h"
#include "../ViMeasConfig.h"
#include "../ViMeasParameterType.h"
#include "lqmeas/units/std/Velocity.h"

namespace LQMeas {
    const QString &ViMeasTypeVibroVelocity::typeID() {
        static const QString t{QStringLiteral("vi_meas_vibro_vel")};
        return t;
    }

    const ViMeasTypeVibroVelocity &ViMeasTypeVibroVelocity::type() {
        static const ViMeasTypeVibroVelocity t;
        return t;
    }

    QString ViMeasTypeVibroVelocity::displayName() const {
        return ViMeasConfig::tr("Velocity");
    }

    const Unit &ViMeasTypeVibroVelocity::unit() const {
        return Units::Velocity::mm_s();
    }

    QList<const ViMeasParameterType *> ViMeasTypeVibroVelocity::supportedMeasParams(const ViAcqChannelConfig &chCfg) const {
        static const QList<const ViMeasParameterType *> params {
            &ViMeasParameterTypes::rms(),
            &ViMeasParameterTypes::pp(),
            &ViMeasParameterTypes::peak(),
        };
        return params;
    }

    const ViMeasParameterType *ViMeasTypeVibroVelocity::defaultMeasParam(const ViAcqChannelConfig &chCfg) const {
        return &ViMeasParameterTypes::rms();
    }
}
