#ifndef LQMES_VIMEASTYPEVIBROSMAX_H
#define LQMES_VIMEASTYPEVIBROSMAX_H

#include "../ViMeasType.h"

namespace LQMeas {
    class ViMeasTypeVibroSmax : public ViMeasType {
    public:
        static const QString &typeID();
        static const ViMeasTypeVibroSmax &type();

        QString id() const override {return typeID();}
        QString displayName() const override;
        double defaultMaxValue(const ViAcqChannelConfig &chCfg) const override {return 125;}

        double valueRangeMinLimit(const ViAcqChannelConfig &chCfg) const override {return 0;}

        const Unit &unit() const override;

    };
}

#endif // LQMES_VIMEASTYPEVIBROSMAX_H
