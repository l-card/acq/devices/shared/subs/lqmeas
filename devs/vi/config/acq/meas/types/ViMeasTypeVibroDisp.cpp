#include "ViMeasTypeVibroDisp.h"
#include "../ViMeasConfig.h"
#include "../ViMeasParameterType.h"
#include "lqmeas/units/std/Displacement.h"
#include "lqmeas/devs/vi/config/acq/channel/ViAcqChannelConfig.h"
#include "lqmeas/devs/vi/config/acq/channel/types/ViAcqChannelTypeVibroAccel.h"

namespace LQMeas {

    const QString &ViMeasTypeVibroDisp::typeID() {
        static const QString t{QStringLiteral("vi_meas_vibro_disp")};
        return t;
    }

    const ViMeasTypeVibroDisp &ViMeasTypeVibroDisp::type() {
        static const ViMeasTypeVibroDisp t;
        return t;
    }

    QString ViMeasTypeVibroDisp::displayName() const {
        return ViMeasConfig::tr("Displacement");
    }

    const Unit &ViMeasTypeVibroDisp::unit() const {
        return Units::Displacement::um();
    }

    QList<const ViMeasParameterType *> ViMeasTypeVibroDisp::supportedMeasParams(const ViAcqChannelConfig &chCfg) const {
        static const QList<const ViMeasParameterType *> params {
            &ViMeasParameterTypes::rms(),
            &ViMeasParameterTypes::pp(),
            &ViMeasParameterTypes::peak(),
        };
        return params;
    }

    const ViMeasParameterType *ViMeasTypeVibroDisp::defaultMeasParam(const ViAcqChannelConfig &chCfg) const {
        return chCfg.type() == ViAcqChannelTypeVibroAccel::type() ? &ViMeasParameterTypes::rms() : &ViMeasParameterTypes::pp();
    }

    double ViMeasTypeVibroDisp::hpFilterFreqDefault(const ViAcqChannelConfig &chCfg) const {
        return chCfg.type() == ViAcqChannelTypeVibroAccel::type() ? 10 : 4;
    }

    double ViMeasTypeVibroDisp::lpFilterFreqDefault(const ViAcqChannelConfig &chCfg) const  {
        return chCfg.type() == ViAcqChannelTypeVibroAccel::type() ? 200 : 4000;
    }
}
