#include "ViMeasTypeVoltageGap.h"
#include "../ViMeasConfig.h"
#include "lqmeas/units/std/Voltage.h"
#include "lqmeas/devs/vi/config/acq/channel/ViAcqChannelConfig.h"
#include "lqmeas/devs/vi/config/acq/sensor/ViAcqSensorConfig.h"

namespace LQMeas {

    const QString &ViMeasTypeVoltageGap::typeID() {
        static const QString t{QStringLiteral("vi_meas_volt_gap")};
        return t;
    }

    const ViMeasTypeVoltageGap &ViMeasTypeVoltageGap::type() {
        static const ViMeasTypeVoltageGap t;
        return t;
    }

    QString ViMeasTypeVoltageGap::displayName() const {
        return ViMeasConfig::tr("Gap");
    }

    double ViMeasTypeVoltageGap::defaultMinValue(const ViAcqChannelConfig &chCfg) const {
        return chCfg.sensor().minValidValue();
    }

    double ViMeasTypeVoltageGap::defaultMaxValue(const ViAcqChannelConfig &chCfg) const {
        return chCfg.sensor().maxValidValue();
    }

    const Unit &ViMeasTypeVoltageGap::unit() const {
        return Units::Voltage::V();
    }
}
