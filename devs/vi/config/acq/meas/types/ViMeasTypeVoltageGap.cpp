#include "ViMeasTypeVoltageGap.h"
#include "../ViMeasConfig.h"
#include "lqmeas/units/std/Voltage.h"
#include "lqmeas/devs/vi/config/acq/channel/ViAcqChannelConfig.h"
#include "lqmeas/devs/vi/config/acq/sensor/ViAcqSensorConfig.h"
#include "lqmeas/devs/vi/config/acq/sensor/ViAcqSensorConType.h"

namespace LQMeas {

    const QString &ViMeasTypeVoltageGap::typeID() {
        static const QString t{QStringLiteral("vi_meas_volt_gap")};
        return t;
    }

    const ViMeasTypeVoltageGap &ViMeasTypeVoltageGap::type() {
        static const ViMeasTypeVoltageGap t;
        return t;
    }

    QString ViMeasTypeVoltageGap::displayName() const {
        return ViMeasConfig::tr("Gap");
    }

    double ViMeasTypeVoltageGap::defaultMinValue(const ViAcqChannelConfig &chCfg) const {
        return chCfg.sensor().connectionMethod().negativePolarity() ? -24 : 4;
    }

    double ViMeasTypeVoltageGap::defaultMaxValue(const ViAcqChannelConfig &chCfg) const {
        return chCfg.sensor().connectionMethod().negativePolarity() ? -4 : 24;
    }

    const Unit &ViMeasTypeVoltageGap::unit() const {
        return Units::Voltage::V();
    }
}
