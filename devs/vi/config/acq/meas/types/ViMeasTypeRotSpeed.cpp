#include "ViMeasTypeRotSpeed.h"
#include "../ViMeasConfig.h"
#include "lqmeas/units/std/RotationalSpeed.h"

namespace LQMeas {

const QString &ViMeasTypeRotSpeed::typeID() {
    static const QString t{QStringLiteral("vi_meas_rot_speed")};
    return t;
}

const ViMeasTypeRotSpeed &ViMeasTypeRotSpeed::type() {
    static const ViMeasTypeRotSpeed t;
    return t;
}

QString ViMeasTypeRotSpeed::displayName() const {
    return ViMeasConfig::tr("Rotational speed ");
}

const Unit &ViMeasTypeRotSpeed::unit() const {
    return Units::RotationalSpeed::rpm();
}
}
