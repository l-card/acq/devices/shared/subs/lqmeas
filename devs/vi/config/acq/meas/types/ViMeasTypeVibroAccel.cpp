#include "ViMeasTypeVibroAccel.h"
#include "../ViMeasConfig.h"
#include "../ViMeasParameterType.h"
#include "lqmeas/units/std/Acceleration.h"

namespace LQMeas {

    const QString &ViMeasTypeVibroAccel::typeID() {
        static const QString t{QStringLiteral("vi_meas_vibro_accel")};
        return t;
    }

    const ViMeasTypeVibroAccel &ViMeasTypeVibroAccel::type() {
        static const ViMeasTypeVibroAccel t;
        return t;
    }

    QString ViMeasTypeVibroAccel::displayName() const {
        return ViMeasConfig::tr("Acceleration");
    }

    const Unit &ViMeasTypeVibroAccel::unit() const {
        return Units::Acceleration::m_s2();
    }

    QList<const ViMeasParameterType *> ViMeasTypeVibroAccel::supportedMeasParams(const ViAcqChannelConfig &chCfg) const {
        static const QList<const ViMeasParameterType *> params {
            &ViMeasParameterTypes::rms(),
            &ViMeasParameterTypes::pp(),
            &ViMeasParameterTypes::peak(),
        };
        return params;
    }

    const ViMeasParameterType *ViMeasTypeVibroAccel::defaultMeasParam(const ViAcqChannelConfig &chCfg) const {
        return &ViMeasParameterTypes::rms();
    }
}
