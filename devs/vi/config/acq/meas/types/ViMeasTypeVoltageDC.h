#ifndef LQMEAS_VIMEASTYPEVOLTAGEDC_H
#define LQMEAS_VIMEASTYPEVOLTAGEDC_H


#include "../ViMeasType.h"

namespace LQMeas {
    class ViMeasTypeVoltageDC : public ViMeasType {
    public:
        static const QString &typeID();
        static const ViMeasTypeVoltageDC &type();

        QString id() const override {return typeID();}
        QString displayName() const override;

        double defaultMinValue(const ViAcqChannelConfig &chCfg) const override;
        double defaultMaxValue(const ViAcqChannelConfig &chCfg) const override;

        const Unit &unit() const override;
    };
}

#endif // LQMEAS_VIMEASTYPEVOLTAGEDC_H
