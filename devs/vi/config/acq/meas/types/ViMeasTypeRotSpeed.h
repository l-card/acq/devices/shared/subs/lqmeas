#ifndef LQMEAS_VIMEASTYPEROTSPEED_H
#define LQMEAS_VIMEASTYPEROTSPEED_H


#include "../ViMeasType.h"

namespace LQMeas {
    class ViMeasTypeRotSpeed : public ViMeasType {
    public:
        static const QString &typeID();
        static const ViMeasTypeRotSpeed &type();

        QString id() const override {return typeID();}
        QString displayName() const override;
        double defaultMinValue(const ViAcqChannelConfig &chCfg) const override {return 0;}
        double defaultMaxValue(const ViAcqChannelConfig &chCfg) const override {return 10000;}

        double valueRangeMaxLimit(const ViAcqChannelConfig &chCfg) const override {return 999999;}
        double valueRangeMinLimit(const ViAcqChannelConfig &chCfg) const override {return 0;}
        double valueFractDigitCnt(const ViAcqChannelConfig &chCfg) const override {return 0;}

        const Unit &unit() const override;
    };
}

#endif // LQMEAS_VIMEASTYPEROTSPEED_H
