#include "ViMeasTypeVibroSmax.h"
#include "../ViMeasConfig.h"
#include "lqmeas/units/std/Displacement.h"

namespace LQMeas {
    const QString &ViMeasTypeVibroSmax::typeID() {
        static const QString t{QStringLiteral("vi_meas_vibro_smax")};
        return t;
    }

    const ViMeasTypeVibroSmax &ViMeasTypeVibroSmax::type() {
        static const ViMeasTypeVibroSmax t;
        return t;
    }

    QString ViMeasTypeVibroSmax::displayName() const {
        return ViMeasConfig::tr("Max displacement from average (Smax)");
    }

    const Unit &ViMeasTypeVibroSmax::unit() const     {
        return Units::Displacement::um();
    }
}
