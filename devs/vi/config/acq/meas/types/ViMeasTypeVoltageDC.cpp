#include "ViMeasTypeVoltageDC.h"
#include "../ViMeasConfig.h"
#include "lqmeas/units/std/Voltage.h"
#include "lqmeas/devs/vi/config/acq/channel/ViAcqChannelConfig.h"
#include "lqmeas/devs/vi/config/acq/sensor/ViAcqSensorConfig.h"
#include "lqmeas/devs/vi/config/acq/sensor/ViAcqSensorConType.h"

namespace LQMeas {

const QString &ViMeasTypeVoltageDC::typeID() {
    static const QString t{QStringLiteral("vi_meas_volt_dc")};
    return t;
}

const ViMeasTypeVoltageDC &ViMeasTypeVoltageDC::type() {
    static const ViMeasTypeVoltageDC t;
    return t;
}

QString ViMeasTypeVoltageDC::displayName() const {
    return ViMeasConfig::tr("DC Voltage");
}

double ViMeasTypeVoltageDC::defaultMinValue(const ViAcqChannelConfig &chCfg) const {
    return chCfg.sensor().connectionMethod().negativePolarity() ? -24 : 0;
}

double ViMeasTypeVoltageDC::defaultMaxValue(const ViAcqChannelConfig &chCfg) const {
    return chCfg.sensor().connectionMethod().negativePolarity() ? 0 : 24;
}

const Unit &ViMeasTypeVoltageDC::unit() const {
    return Units::Voltage::V();
}
}
