#ifndef LQMEAS_VIMEASTYPEVIBROACCEL_H
#define LQMEAS_VIMEASTYPEVIBROACCEL_H

#include "../ViMeasType.h"

namespace LQMeas {
    class ViMeasTypeVibroAccel : public ViMeasType {
    public:
        static const QString &typeID();
        static const ViMeasTypeVibroAccel &type();

        QString id() const override {return typeID();}
        QString displayName() const override;

        const Unit &unit() const override;
        double defaultMaxValue(const ViAcqChannelConfig &chCfg) const override {return 100;}

        double valueRangeMinLimit(const ViAcqChannelConfig &chCfg) const override {return 0;}

        bool   dbScaleSupported(const ViAcqChannelConfig &chCfg) const override {return true;}

        QList<const ViMeasParameterType *> supportedMeasParams(const ViAcqChannelConfig &chCfg) const override;
        virtual const ViMeasParameterType *defaultMeasParam(const ViAcqChannelConfig &chCfg) const override;


        bool hpFilterIsAvailable(const ViAcqChannelConfig &chCfg) const override    {return true;}
        double hpFilterFreqMin(const ViAcqChannelConfig &chCfg) const override      {return 2;}
        double hpFilterFreqMax(const ViAcqChannelConfig &chCfg) const override      {return 4000;}
        double hpFilterFreqDefault(const ViAcqChannelConfig &chCfg) const override  {return 10;}
        bool lpFilterIsAvailable(const ViAcqChannelConfig &chCfg) const override    {return true;}
        double lpFilterFreqMin(const ViAcqChannelConfig &chCfg) const override      {return 100;}
        double lpFilterFreqMax(const ViAcqChannelConfig &chCfg) const override      {return 10000;}
        double lpFilterFreqDefault(const ViAcqChannelConfig &chCfg) const override  {return 3000;}

    };
}

#endif // LQMEAS_VIMEASTYPEVIBROACCEL_H
