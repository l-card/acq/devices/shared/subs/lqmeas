#ifndef LQMEAS_VIMEASFILTERCONFIG_H
#define LQMEAS_VIMEASFILTERCONFIG_H

#include "lqmeas/devs/vi/config/ViConfigItem.h"

namespace LQMeas {
    class ViMeasConfig;

    class ViMeasFilterConfig : public ViConfigItem {
        Q_OBJECT
    public:
        enum class FreqType {
            HP,
            LP
        };

        explicit ViMeasFilterConfig(ViMeasConfig &measCfg, FreqType ftype);
        explicit ViMeasFilterConfig(ViMeasConfig &measCfg, const ViMeasFilterConfig &origFiltCfg);

        bool isEnabled() const {return !isOptional() || m_en;}
        double freq() const {return m_freq;}

        void setEnabled(bool en);
        void setFreq(double freq);


        bool defaultEnabled() const;
        double defaultFreq() const;
        bool isOptional() const;
        double maxFreq() const;
        double minFreq() const;
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        const FreqType m_ftype;
        const ViMeasConfig *m_measCfg;

        double m_freq;
        bool m_en {true};
    };
}

#endif // VIMEASFILTERCONFIG_H
