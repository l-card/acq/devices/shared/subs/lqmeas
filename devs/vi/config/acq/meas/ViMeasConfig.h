#ifndef LQMEAS_VIMEASCONFIG_H
#define LQMEAS_VIMEASCONFIG_H

#include "lqmeas/devs/vi/config/ViChannelConfig.h"


namespace LQMeas {
    class ViMeasGroupConfig;
    class ViMeasParameterType;
    class ViMeasType;
    class ViMeasFilterConfig;
    class ViSetpointConfig;
    class Unit;

    class ViMeasConfig : public ViChannelConfig {
        Q_OBJECT
    public:
        explicit ViMeasConfig(ViMeasGroupConfig &group, int group_pos = 0);
        explicit ViMeasConfig(ViMeasGroupConfig &group, const ViMeasConfig &origMeasCfg);

        QString origName() const override;

        const ViMeasGroupConfig &group() const {return *m_group;}
        ViMeasGroupConfig &group() {return *m_group;}
        int measGroupPos() const {return m_group_pos;}
        int measNum() const {return m_meas_num;}
        int channelNumber() const override {return measNum();}
        const ViMeasType &type() const {return *m_type;}

        const Unit &resultUnit() const;

        bool dbScaleIsEnabled() const {return m_db_scale_en;}
        double dbScaleBaseValue() const {return m_db_scale_base_level;}

        double minValue() const {return m_min_value;}
        double maxValue() const {return m_max_value;}
        double clampValue() const {return m_clamp_value;}
        double changeThreshold() const {return m_change_thresh;}
        double tripMultiply() const {return m_trip_mult;}
        double nxValue() const {return m_nx;}

        bool hasParamType() const {return m_paramType != nullptr;}
        const ViMeasParameterType &paramType() const {return *m_paramType;}

        void setDbScaleEnabled(bool en);
        void setDbScaleBaseLevel(double lvl);

        void setMinValue(double val);
        void setMaxValue(double val);
        void setClampValue(double val);
        void setChangeThreshold(double val);
        void setTripMultiplty(double val);
        void setNxValue(double val);
        void setParamType(const ViMeasParameterType &paramType);

        bool hasHpFilter() const {return m_hp_filter != nullptr;}
        const ViMeasFilterConfig &hpFilter() const {return *m_hp_filter;}
        ViMeasFilterConfig &hpFilter() {return *m_hp_filter;}

        bool hasLpFilter() const {return m_lp_filter != nullptr;}
        const ViMeasFilterConfig &lpFilter() const {return *m_lp_filter;}
        ViMeasFilterConfig &lpFilter() {return *m_lp_filter;}

        const ViSetpointConfig &setpointAlert() const  {return *m_sp_alert;}
        ViSetpointConfig &setpointAlert() {return *m_sp_alert;}
        const ViSetpointConfig &setpointDanger() const  {return *m_sp_danger;}
        ViSetpointConfig &setpointDanger() {return *m_sp_danger;}


        static QString loadTypeId(const QJsonObject &cfgObj);

        /* свойства, определяемые типом измерения и конфигурацией канала */
        double defaultMinValue() const;
        double defaultMaxValue() const;
        double defaultClampValue() const;
        QList<const ViMeasParameterType *> supportedMeasParams() const;
        const ViMeasParameterType *defaultMeasParam() const;

        double valueRangeMaxLimit() const;
        double valueRangeMinLimit() const;
        double valueFractDigitCnt() const;

        double nxDefaultValue() const;
        double nxMinValue() const;
        double nxMaxValue() const;

        bool   dbScaleSupported() const;
        bool   dbScaleDefaultEnabled() const;
        double dbScaleDefaultBaseLevel() const;
        double defaultChangeThreshold() const;
        double defaultTripMultiply() const;

        bool hpFilterIsAvailable() const;
        bool lpFilterIsAvailable() const;
        bool hpFilterIsOptional() const;
        bool lpFilterIsOptional() const;
        bool hpFilterDefaultEnabled() const;
        bool lpFilterDefaultEnabled() const;
        double hpFilterFreqMin() const;
        double hpFilterFreqDefault() const;
        double hpFilterFreqMax() const;
        double hpFilterFreqStep() const;
        double lpFilterFreqMin() const;
        double lpFilterFreqDefault() const;
        double lpFilterFreqMax() const;
        double lpFilterFreqStep() const;
        double filtersFreqMaxRatio() const;
    Q_SIGNALS:
        void measNumberChanged(int num);
    protected:
        void chCfgSave(QJsonObject &cfgObj) const override;
        void chCfgLoad(const QJsonObject &cfgObj) override;
    private:
        void setMeasNum(int num);

        ViMeasGroupConfig *m_group;
        const ViMeasType *m_type;
        int m_group_pos;
        int m_meas_num {0};

        bool   m_db_scale_en {false};
        double m_db_scale_base_level;

        double m_nx;
        double m_min_value;
        double m_max_value;
        double m_clamp_value;
        double m_change_thresh;
        double m_trip_mult;
        const ViMeasParameterType *m_paramType {nullptr};
        ViMeasFilterConfig *m_hp_filter {nullptr};
        ViMeasFilterConfig *m_lp_filter {nullptr};

        ViSetpointConfig *m_sp_alert;
        ViSetpointConfig *m_sp_danger;

        friend class ViAcqChannelConfig;
    };
}


#endif // LQMEAS_VIMEASCONFIG_H
