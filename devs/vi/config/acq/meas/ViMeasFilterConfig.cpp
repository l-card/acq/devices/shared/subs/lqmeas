#include "ViMeasFilterConfig.h"
#include "ViMeasConfig.h"
#include "ViMeasType.h"
#include "lqmeas/lqtdefs.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String &cfgkey_filt_en        {StdConfigKeys::enabled()};
    static const QLatin1String &cfgkey_filt_freq      {StdConfigKeys::freq()};

    ViMeasFilterConfig::ViMeasFilterConfig(ViMeasConfig &measCfg, FreqType ftype) :
        ViConfigItem{&measCfg}, m_measCfg{&measCfg},
        m_ftype{ftype},
        m_en{defaultEnabled()},
        m_freq{defaultFreq()} {

    }

    ViMeasFilterConfig::ViMeasFilterConfig(ViMeasConfig &measCfg, const ViMeasFilterConfig &origFiltCfg) :
        ViConfigItem{&measCfg}, m_measCfg{&measCfg},
        m_ftype{origFiltCfg.m_ftype},
        m_en{origFiltCfg.m_en},
        m_freq{origFiltCfg.m_freq} {

    }

    void ViMeasFilterConfig::setEnabled(bool en) {
        if ((en != m_en) && isOptional()) {
            m_en = en;
            notifyConfigChanged();
        }
    }

    void ViMeasFilterConfig::setFreq(double freq) {
        const double sfreq {qMin<double>(qMax<double>(freq, minFreq()), maxFreq())};
        if (!LQREAL_IS_EQUAL(sfreq, m_freq)) {
            m_freq = sfreq;
            notifyConfigChanged();
        }
    }

    bool ViMeasFilterConfig::defaultEnabled() const {
        return m_ftype == FreqType::HP ?
                   m_measCfg->hpFilterDefaultEnabled() :
                   m_measCfg->lpFilterDefaultEnabled();
    }

    double ViMeasFilterConfig::defaultFreq() const  {
        return m_ftype == FreqType::HP ?
                   m_measCfg->hpFilterFreqDefault() :
                   m_measCfg->lpFilterFreqDefault();
    }

    bool ViMeasFilterConfig::isOptional() const {
        return m_ftype == FreqType::HP ?
                   m_measCfg->hpFilterIsOptional() :
                   m_measCfg->lpFilterIsOptional();
    }

    double ViMeasFilterConfig::maxFreq() const {
        return m_ftype == FreqType::HP ?
                   m_measCfg->hpFilterFreqMax() :
                   m_measCfg->lpFilterFreqMax();
    }

    double ViMeasFilterConfig::minFreq() const {
        return m_ftype == FreqType::HP ?
                   m_measCfg->hpFilterFreqMin() :
                   m_measCfg->lpFilterFreqMin();
    }

    void ViMeasFilterConfig::protSave(QJsonObject &cfgObj) const     {
        cfgObj[cfgkey_filt_en] = isEnabled();
        cfgObj[cfgkey_filt_freq] = freq();
    }

    void ViMeasFilterConfig::protLoad(const QJsonObject &cfgObj) {
        m_en    = cfgObj[cfgkey_filt_en].toBool(defaultEnabled());
        m_freq  = cfgObj[cfgkey_filt_freq].toDouble(defaultFreq());
    }
}
