#ifndef LQMEAS_VIMEASCONDTYPE_H
#define LQMEAS_VIMEASCONDTYPE_H

#include "lqmeas/EnumNamedValue.h"
#include <QObject>
#include <QList>

namespace LQMeas {
    class ViMeasCondType : public EnumNamedValue<QString> {
    public:

    };

    class ViMeasCondTypes : public QObject {
        Q_OBJECT
    public:
        static const ViMeasCondType &alert();
        static const ViMeasCondType &danger();
        static const ViMeasCondType &failure();
        static const ViMeasCondType &changeThreshold();

        static const QList<const ViMeasCondType *> &all();
    };
}

Q_DECLARE_METATYPE(const LQMeas::ViMeasCondType *);

#endif // LQMEAS_VIMEASCONDTYPE_H
