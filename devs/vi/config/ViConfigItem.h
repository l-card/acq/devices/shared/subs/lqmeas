#ifndef VICONFIGITEM_H
#define VICONFIGITEM_H

#include "lqmeas/devs/DeviceConfigItem.h"
#include <QList>

namespace LQMeas {
    class ViModuleTypeInfo;

    class ViConfigItem : public DeviceConfigItem {
        Q_OBJECT
    public:
        explicit ViConfigItem(QObject *parent = nullptr);
        explicit ViConfigItem(const ViModuleTypeInfo &devTypeInfo, QObject *parent = nullptr);

        const ViModuleTypeInfo &viModuleTypeInfo() const {return *m_viModuleTypeInfo;}
        bool viModuleTypeInfoValid() const {return m_viModuleTypeInfo != nullptr;}
    protected:
        virtual void viModuleInfoUpdate(const ViModuleTypeInfo &viInfo) {Q_UNUSED(viInfo)}

        void protUpdateInfo(const DeviceTypeInfo *info) override final;        
    private:
        const ViModuleTypeInfo *m_viModuleTypeInfo {nullptr};
    };
}

#endif // VICONFIGITEM_H
