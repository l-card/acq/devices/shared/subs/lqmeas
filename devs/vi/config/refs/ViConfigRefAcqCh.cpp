#include  "ViConfigRefAcqCh.h"
#include "lqmeas/StdConfigKeys.h"
#include "lqmeas/devs/vi/config/acq/channel/types/ViAcqChannelTypeUnknown.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String &cfgkey_channel_num      {StdConfigKeys::channel()};
    static const QLatin1String &cfgkey_channel_type     {StdConfigKeys::type()};

    ViConfigRefAcqCh::ViConfigRefAcqCh(ViChannelPubInfoType infoType, bool defValid,
                                       const QList<const ViAcqChannelType *> &chTypes,
                                       bool support_rem_devs, const QString &deviceId,
                                       int acq_ch_num, QObject *parent) :
        ViConfigRef{defValid, deviceId, support_rem_devs, infoType, parent},
        m_acq_ch_num{acq_ch_num},
        m_supportedAcqChTypes{chTypes} {

    }

    ViConfigRefAcqCh::ViConfigRefAcqCh(const ViConfigRefAcqCh &origRef, QObject *parent) :
        ViConfigRef{origRef, parent},
        m_acq_ch_num{origRef.m_acq_ch_num},
        m_supportedAcqChTypes{origRef.m_supportedAcqChTypes}  {

    }

    const QString &ViConfigRefAcqCh::refTypeId() {
        static const QString t{QStringLiteral("vi_reftype_acq_ch")};
        return t;
    }

    void ViConfigRefAcqCh::setAcqChNum(int num) {
        if (m_acq_ch_num != num) {
            m_acq_ch_num = num;
            notifyConfigChanged();
        }
    }

    void ViConfigRefAcqCh::setAcqChType(const ViAcqChannelType &type) {
        if (m_acq_ch_type != &type) {
            m_acq_ch_type = &type;
            notifyConfigChanged();
        }
    }

    void ViConfigRefAcqCh::setSupportedAcqChTypes(const QList<const ViAcqChannelType *> &chTypes) {
        if (m_supportedAcqChTypes != chTypes) {
            m_supportedAcqChTypes = chTypes;
            notifyConfigChanged();
        }
    }

    void ViConfigRefAcqCh::refSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_channel_num] = m_acq_ch_num;
        cfgObj[cfgkey_channel_type] = m_acq_ch_type->id();
    }

    void ViConfigRefAcqCh::refLoad(const QJsonObject &cfgObj) {
        m_acq_ch_num = cfgObj[cfgkey_channel_num].toInt();

        const QString typeId {cfgObj[cfgkey_channel_type].toString()};
        QList<const ViAcqChannelType *> types {ViAcqChannelTypes::all()};
        const auto &it {std::find_if(types.cbegin(), types.cend(),
                                    [&typeId](const ViAcqChannelType *type){return type->id() == typeId;})};
        m_acq_ch_type = it != types.cend() ? *it : &defaultType();
    }

    const ViAcqChannelType &ViConfigRefAcqCh::defaultType() {
        return ViAcqChannelTypeUnknown::type();
    }
}
