#include "ViConfigRef.h"
#include <QJsonObject>
#include "lqmeas/StdConfigKeys.h"

namespace LQMeas {
    static const QLatin1String &cfgkey_type             {StdConfigKeys::type()};
    static const QLatin1String &cfgkey_valid            {StdConfigKeys::valid()};
    static const QLatin1String cfgkey_device_id         {"DeviceID"};

    ViConfigRef::ViConfigRef(bool defaultValid, const QString &devId, bool supportRemDevs, ViChannelPubInfoType type, QObject *parent) :
        ViConfigItem{parent}, m_dev_id{devId},
        m_valid{defaultValid},
        m_default_valid{defaultValid},
        m_support_rem_dev{supportRemDevs},
        m_infoType{type}  {

    }

    ViConfigRef::ViConfigRef(const ViConfigRef &origRef, QObject *parent) :
        ViConfigItem{parent},
        m_dev_id{origRef.m_dev_id},
        m_valid{origRef.m_valid},
        m_default_valid{origRef.m_default_valid},
        m_support_rem_dev{origRef.m_support_rem_dev},
        m_infoType{origRef.m_infoType} {

    }

    ViConfigRef::~ViConfigRef() {

    }

    void ViConfigRef::setDeviceId(const QString &devId) {
        if (devId != m_dev_id) {
            m_dev_id = devId;
            notifyConfigChanged();
        }
    }

    void ViConfigRef::setValid(bool valid) {
        if (m_valid != valid) {
            m_valid = valid;
            notifyConfigChanged();
        }
    }

    void ViConfigRef::protSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_type] = typeID();
        cfgObj[cfgkey_valid] = m_valid;
        cfgObj[cfgkey_device_id] = m_dev_id;
        refSave(cfgObj);
    }

    void ViConfigRef::protLoad(const QJsonObject &cfgObj) {
        m_valid = cfgObj[cfgkey_valid].toBool(m_default_valid);
        m_dev_id = cfgObj[cfgkey_device_id].toString();
        refLoad(cfgObj);
    }
}
