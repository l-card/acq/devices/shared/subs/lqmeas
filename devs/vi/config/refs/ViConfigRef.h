 #ifndef LQMEAS_VICONFIGREF_H
#define LQMEAS_VICONFIGREF_H

#include "lqmeas/devs/vi/config/ViConfigItem.h"
#include "lqmeas/devs/vi/config/ViChannelConfig.h"


namespace LQMeas {
    class ViConfigRef : public ViConfigItem {
        Q_OBJECT
    public:
        explicit ViConfigRef(bool defaultValid, const QString &devId, bool supportRemDevs = true,
                             ViChannelPubInfoType type = ViChannelPubInfoType::State, QObject *parent = nullptr);
        ViConfigRef (const ViConfigRef &origRef, QObject *parent = nullptr);
        virtual ~ViConfigRef();

        virtual QString typeID() const = 0;
        virtual ViConfigRef *clone() const = 0;


        bool deviceIsLocal() const {return m_dev_id.isEmpty();}
        const QString &deviceExchangeId() const {return m_dev_id;}
        void setDeviceId(const QString &devId);

        bool isValid() const {return m_valid;}
        void setValid(bool valid);

        const ViChannelPubInfoType infoType() const {return m_infoType;}

        bool supportRemoteDevs() const {return m_support_rem_dev;}
        void setSupportRemoteDevs(bool en) {
            m_support_rem_dev = en;
        }
    protected:
        void protSave(QJsonObject &cfgObj) const override final;
        void protLoad(const QJsonObject &cfgObj) override final;

        virtual void refSave(QJsonObject &cfgObj) const = 0;
        virtual void refLoad(const QJsonObject &cfgObj) = 0;
    private:
        ViChannelPubInfoType m_infoType;
        bool m_valid;
        bool m_default_valid;
        bool m_support_rem_dev {true};
        QString m_dev_id;
    };
}

#endif // LQMEAS_VICONFIGREF_H
