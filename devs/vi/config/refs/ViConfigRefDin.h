#ifndef LQMEAS_VICONFIGREFDIN_H
#define LQMEAS_VICONFIGREFDIN_H

#include "ViConfigRef.h"
namespace LQMeas {
    class ViConfigRefDin : public ViConfigRef {
        Q_OBJECT
    public:
        explicit ViConfigRefDin(bool defValid = false, const QString &deviceId = QString{}, int din_ch_num = 0, QObject *parent = nullptr);
        ViConfigRefDin(const ViConfigRefDin &origRef, QObject *parent = nullptr);

        static const QString &refTypeId();
        QString typeID() const override {return refTypeId();}
        ViConfigRef *clone() const override {return new ViConfigRefDin{*this};}

        int dinChNum() const {return m_din_ch_num;}
        void setDinChNum(int num);

    protected:
        void refSave(QJsonObject &cfgObj) const override;
        void refLoad(const QJsonObject &cfgObj) override;
    private:
        int m_din_ch_num {0};
    };
}

#endif // LQMEAS_VICONFIGREFDIN_H
