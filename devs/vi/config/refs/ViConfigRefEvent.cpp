#include  "ViConfigRefEvent.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
static const QLatin1String &cfgkey_evt_num      {StdConfigKeys::channel()};

ViConfigRefEvent::ViConfigRefEvent(bool defValid, const QString &deviceId, int acq_ch_num, QObject *parent) :
    ViConfigRef{defValid, deviceId, true, ViChannelPubInfoType::State, parent}, m_evt_num{acq_ch_num} {

}

ViConfigRefEvent::ViConfigRefEvent(const ViConfigRefEvent &origRef, QObject *parent) :
    ViConfigRef{origRef, parent}, m_evt_num{origRef.m_evt_num} {

}

const QString &ViConfigRefEvent::refTypeId() {
    static const QString t{QStringLiteral("vi_reftype_evt")};
    return t;
}

void ViConfigRefEvent::setEventNum(int num) {
    if (m_evt_num != num) {
        m_evt_num = num;
        notifyConfigChanged();
    }
}

void ViConfigRefEvent::refSave(QJsonObject &cfgObj) const {
    cfgObj[cfgkey_evt_num] = m_evt_num;
}

void ViConfigRefEvent::refLoad(const QJsonObject &cfgObj) {
    m_evt_num = cfgObj[cfgkey_evt_num].toInt();
}
}
