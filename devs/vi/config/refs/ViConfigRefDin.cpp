#include "ViConfigRefDin.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String &cfgkey_channel_num      {StdConfigKeys::channel()};

    ViConfigRefDin::ViConfigRefDin(bool defValid, const QString &deviceId, int din_ch_num, QObject *parent) :
        ViConfigRef{defValid, deviceId, true, ViChannelPubInfoType::State, parent}, m_din_ch_num{din_ch_num} {

    }

    ViConfigRefDin::ViConfigRefDin(const ViConfigRefDin &origRef, QObject *parent) :
        ViConfigRef{origRef, parent}, m_din_ch_num{origRef.m_din_ch_num} {

    }

    const QString &ViConfigRefDin::refTypeId() {
        static const QString t{QStringLiteral("vi_reftype_din_ch")};
        return t;
    }

    void ViConfigRefDin::setDinChNum(int num) {
        if (m_din_ch_num != num) {
            m_din_ch_num = num;
            notifyConfigChanged();
        }
    }

    void ViConfigRefDin::refSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_channel_num] = m_din_ch_num;
    }

    void ViConfigRefDin::refLoad(const QJsonObject &cfgObj) {
        m_din_ch_num = cfgObj[cfgkey_channel_num].toInt();
    }
}
