#ifndef LQMEAS_VICONFIGREFMEAS_H
#define LQMEAS_VICONFIGREFMEAS_H

#include "ViConfigRef.h"

namespace LQMeas {
    class ViMeasType;

    class ViConfigRefMeas : public ViConfigRef {
        Q_OBJECT
    public:
        explicit ViConfigRefMeas(bool defValid, const QString &deviceId = QString{}, int acq_ch_num = 0, int meas_num = 0, QObject *parent = nullptr);
        ViConfigRefMeas(const ViConfigRefMeas &origRef, QObject *parent = nullptr);

        static const QString &refTypeId();
        QString typeID() const override {return refTypeId();}
        ViConfigRef *clone() const override {return new ViConfigRefMeas{*this};}

        int acqChNum() const {return m_acq_ch_num;}
        int measNum() const {return m_meas_num;}
        const ViMeasType &measType() const {return *m_meas_type;}
        void setAcqChNum(int num);
        void setMeasNum(int num);
        void setMeasType(const ViMeasType &type);

        QString pathPrefix() const;
    protected:
        void refSave(QJsonObject &cfgObj) const override;
        void refLoad(const QJsonObject &cfgObj) override;
    private:
        static const ViMeasType &defaultType();

        int m_acq_ch_num {0};
        int m_meas_num {0};
        const ViMeasType *m_meas_type {&defaultType()};
    };
}


#endif // LQMEAS_VICONFIGREFMEAS_H
