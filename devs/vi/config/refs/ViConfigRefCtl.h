#ifndef LQMEAS_VICONFIGREFCTL_H
#define LQMEAS_VICONFIGREFCTL_H

#include "lqmeas/devs/vi/config/ViConfigItem.h"
#include <QList>

namespace LQMeas {
    class ViConfig;
    class ViConfigRef;

    class ViConfigRefCtl : public ViConfigItem {
        Q_OBJECT
    public:
        explicit ViConfigRefCtl(ViConfig &viConfig);
        explicit ViConfigRefCtl(ViConfig &viConfig, const ViConfigRefCtl &origRefCfg);

        const QList<ViConfigRef *> &refList() const {return m_refs;}

        void addRef(ViConfigRef &ref);
        void removeRef(ViConfigRef &ref);

    Q_SIGNALS:
        void refAppend(LQMeas::ViConfigRef *ref);
        void refRemoved(LQMeas::ViConfigRef *ref);
    private:
        ViConfig *m_viCfg;
        QList<ViConfigRef *> m_refs;
    };
}

#endif // LQMEAS_VICONFIGREFCTL_H
