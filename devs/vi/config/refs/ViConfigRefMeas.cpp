#include  "ViConfigRefMeas.h"
#include "lqmeas/StdConfigKeys.h"
#include "lqmeas/devs/vi/config/acq/meas/ViMeasType.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeUnknown.h"
#include <QJsonObject>
#include <QStringBuilder>

namespace LQMeas {
    static const QLatin1String &cfgkey_channel_num      {StdConfigKeys::channel()};
    static const QLatin1String cfgkey_meas_num          {"MeasNum"};
    static const QLatin1String cfgkey_meas_type         {"MeasType"};

    ViConfigRefMeas::ViConfigRefMeas(bool defValid, const QString &deviceId, int acq_ch_num, int meas_num, QObject *parent) :
        ViConfigRef{defValid, deviceId, true, ViChannelPubInfoType::State, parent}, m_acq_ch_num{acq_ch_num}, m_meas_num{meas_num} {

    }

    ViConfigRefMeas::ViConfigRefMeas(const ViConfigRefMeas &origRef, QObject *parent) :
        ViConfigRef{origRef, parent},
        m_acq_ch_num{origRef.m_acq_ch_num},
        m_meas_num{origRef.m_meas_num},
        m_meas_type{origRef.m_meas_type} {

    }

    const QString &ViConfigRefMeas::refTypeId() {
        static const QString t{QStringLiteral("vi_reftype_meas")};
        return t;
    }

    void ViConfigRefMeas::setAcqChNum(int num) {
        if (m_acq_ch_num != num) {
            m_acq_ch_num = num;
            notifyConfigChanged();
        }
    }

    void ViConfigRefMeas::setMeasNum(int num) {
        if (m_meas_num != num) {
            m_meas_num = num;
            notifyConfigChanged();
        }
    }

    void ViConfigRefMeas::setMeasType(const ViMeasType &type) {
        if (m_meas_type != &type) {
            m_meas_type = &type;
            notifyConfigChanged();
        }
    }

    QString ViConfigRefMeas::pathPrefix() const  {
        return QString::number(acqChNum() + 1) % QChar('/') % QString::number(measNum() + 1);
    }

    void ViConfigRefMeas::refSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_channel_num] = m_acq_ch_num;
        cfgObj[cfgkey_meas_num] = m_meas_num;
        cfgObj[cfgkey_meas_type] = m_meas_type->id();
    }

    void ViConfigRefMeas::refLoad(const QJsonObject &cfgObj) {
        m_acq_ch_num = cfgObj[cfgkey_channel_num].toInt();
        m_meas_num = cfgObj[cfgkey_meas_num].toInt();

        const QString typeId {cfgObj[cfgkey_meas_type].toString()};
        QList<const ViMeasType *> types {ViMeasTypes::all()};
        const auto &it {std::find_if(types.cbegin(), types.cend(),
                                    [&typeId](const ViMeasType *type){return type->id() == typeId;})};
        m_meas_type = it != types.cend() ? *it : &defaultType();
    }

    const ViMeasType &ViConfigRefMeas::defaultType() {
        return ViMeasTypeUnknown::type();
    }
}
