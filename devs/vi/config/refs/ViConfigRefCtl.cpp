#include "ViConfigRefCtl.h"
#include "../ViConfig.h"

namespace LQMeas {
    ViConfigRefCtl::ViConfigRefCtl(ViConfig &viConfig) : ViConfigItem{&viConfig}, m_viCfg{&viConfig} {

    }

    ViConfigRefCtl::ViConfigRefCtl(ViConfig &viConfig, const ViConfigRefCtl &origRefCfg) :
        ViConfigItem{&viConfig}, m_viCfg{&viConfig} {

    }

    void ViConfigRefCtl::addRef(ViConfigRef &ref) {
        m_refs += &ref;
        Q_EMIT refAppend(&ref);
    }

    void ViConfigRefCtl::removeRef(ViConfigRef &ref) {
        const int ref_num {m_refs.indexOf(&ref)};
        if (ref_num >= 0) {
            m_refs.takeAt(ref_num);
            Q_EMIT refRemoved(&ref);
        }
    }
}
