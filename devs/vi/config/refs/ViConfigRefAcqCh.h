#ifndef LQMEAS_VICONFIGREFACQCH_H
#define LQMEAS_VICONFIGREFACQCH_H

#include "ViConfigRef.h"
namespace LQMeas {
    class ViAcqChannelType;

    class ViConfigRefAcqCh : public ViConfigRef {
        Q_OBJECT
    public:
        explicit ViConfigRefAcqCh(ViChannelPubInfoType infoType, bool defValid = false,
                                  const QList<const LQMeas::ViAcqChannelType *> &chTypes = {},
                                  bool support_rem_devs = true,
                                  const QString &deviceId = QString{}, int acq_ch_num = 0, QObject *parent = nullptr);
        ViConfigRefAcqCh(const ViConfigRefAcqCh &origRef, QObject *parent = nullptr);

        static const QString &refTypeId();
        QString typeID() const override {return refTypeId();}
        ViConfigRef *clone() const override {return new ViConfigRefAcqCh{*this};}

        int acqChNum() const {return m_acq_ch_num;}
        void setAcqChNum(int num);



        const ViAcqChannelType &acqChType() const {return *m_acq_ch_type;}
        void setAcqChType(const ViAcqChannelType &type);


        const QList<const LQMeas::ViAcqChannelType *> &supportedAcqChTypes() const {return m_supportedAcqChTypes;}
        void setSupportedAcqChTypes(const QList<const LQMeas::ViAcqChannelType *> &chTypes);
    protected:
        void refSave(QJsonObject &cfgObj) const override;
        void refLoad(const QJsonObject &cfgObj) override;
    private:
        static const ViAcqChannelType &defaultType();

        int m_acq_ch_num {0};
        const ViAcqChannelType *m_acq_ch_type {&defaultType()};
        QList<const LQMeas::ViAcqChannelType *> m_supportedAcqChTypes;
    };
}

#endif // LQMEAS_VICONFIGREFACQCH_H
