#ifndef LQMEAS_VICONFIGREFEVENT_H
#define LQMEAS_VICONFIGREFEVENT_H


#include "ViConfigRef.h"
namespace LQMeas {
    class ViConfigRefEvent : public ViConfigRef {
        Q_OBJECT
    public:
        explicit ViConfigRefEvent(bool defValid = false, const QString &deviceId = QString{}, int event_num = 0, QObject *parent = nullptr);
        ViConfigRefEvent(const ViConfigRefEvent &origRef, QObject *parent = nullptr);

        static const QString &refTypeId();
        QString typeID() const override {return refTypeId();}
        ViConfigRef *clone() const override {return new ViConfigRefEvent{*this};}

        int eventNum() const {return m_evt_num;}
        void setEventNum(int num);

    protected:
        void refSave(QJsonObject &cfgObj) const override;
        void refLoad(const QJsonObject &cfgObj) override;
    private:
        int m_evt_num {0};
    };
}


#endif // LQMEAS_VICONFIGREFEVENT_H
