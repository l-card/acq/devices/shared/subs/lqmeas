#ifndef LQMEAS_VIDINCONFIG_H
#define LQMEAS_VIDINCONFIG_H


#include "lqmeas/devs/vi/config/ViConfigItem.h"
#include <QList>

namespace LQMeas {
    class ViConfig;
    class ViDinChannelConfig;

    class ViDinConfig : public ViConfigItem {
        Q_OBJECT
    public:
        explicit ViDinConfig(ViConfig &viConfig);
        explicit ViDinConfig(ViConfig &viConfig, const ViDinConfig &origDinCfg);

        ViConfig &viConfig() const {return *m_viCfg;}

        const ViDinChannelConfig &channel(int ch_num) const {return *m_channels.at(ch_num);}
        ViDinChannelConfig &channel(int ch_num) {return *m_channels.at(ch_num);}
        int availableChannelsCnt() const {return m_ch_cnt;}
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
        void viModuleInfoUpdate(const ViModuleTypeInfo &viInfo) override;
    private:
        ViConfig *m_viCfg;
        QList<ViDinChannelConfig *> m_channels;
        int m_ch_cnt {0};
    };
}
#endif // LQMEAS_VIDINCONFIG_H
