#ifndef LQMEAS_VIDINCHANNELCONFIG_H
#define LQMEAS_VIDINCHANNELCONFIG_H

#include "lqmeas/devs/vi/config/ViChannelConfig.h"

namespace LQMeas {
    class ViDinConfig;

    class ViDinChannelConfig : public ViChannelConfig {
        Q_OBJECT
    public:
        ViDinChannelConfig(ViDinConfig &doutCfg, int ch_num);
        ViDinChannelConfig(ViDinConfig &dinCfg, const ViDinChannelConfig &origDinChCfg);

        const ViDinConfig &din() const {return *m_dinCfg;}
        ViDinConfig &din() {return *m_dinCfg;}

        int channelNumber() const override {return m_ch_num;}

        QString origName() const override;
    protected:
        void chCfgSave(QJsonObject &cfgObj) const override;
        void chCfgLoad(const QJsonObject &cfgObj) override;
    private:
        ViDinConfig *m_dinCfg;

        int m_ch_num;
    };
};



#endif // LQMEAS_VIDINCHANNELCONFIG_H
