#include "ViDinChannelConfig.h"
#include "ViDinConfig.h"
#include <QJsonObject>
#include <QStringBuilder>


namespace LQMeas {

    ViDinChannelConfig::ViDinChannelConfig(ViDinConfig &dinCfg, int ch_num) :
        ViChannelConfig{dinCfg.viConfig(), &dinCfg, false}, m_dinCfg{&dinCfg},
        m_ch_num{ch_num} {

    }

    ViDinChannelConfig::ViDinChannelConfig(ViDinConfig &dinCfg, const ViDinChannelConfig &origDinChCfg) :
        ViChannelConfig{origDinChCfg, dinCfg.viConfig(), &dinCfg}, m_dinCfg{&dinCfg},
        m_ch_num{origDinChCfg.m_ch_num} {

    }

    QString ViDinChannelConfig::origName() const {
        return tr("DIN") % QChar(' ') %  QString::number(channelNumber() + 1);
    }

    void ViDinChannelConfig::chCfgSave(QJsonObject &cfgObj) const {

    }

    void ViDinChannelConfig::chCfgLoad(const QJsonObject &cfgObj) {

    }
}
