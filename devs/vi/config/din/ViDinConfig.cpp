#include "ViDinConfig.h"
#include "../ViConfig.h"
#include "ViDinChannelConfig.h"
#include "lqmeas/devs/vi/modules/ViModuleTypeInfo.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonArray>
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String &cfgkey_acq_chs_arr  {StdConfigKeys::channels()};

    ViDinConfig::ViDinConfig(ViConfig &viConfig) : ViConfigItem{&viConfig}, m_viCfg{&viConfig} {

    }

    ViDinConfig::ViDinConfig(ViConfig &viConfig, const ViDinConfig &origDinCfg)
        : ViConfigItem{&viConfig}, m_viCfg{&viConfig},
        m_ch_cnt{origDinCfg.m_ch_cnt} {

        for (const ViDinChannelConfig *other_ch : origDinCfg.m_channels) {
            ViDinChannelConfig *ch{new ViDinChannelConfig{*this, *other_ch}};
            addSubItem(ch);
            m_channels += ch;
        }
    }

    void ViDinConfig::protSave(QJsonObject &cfgObj) const {
        {
            QJsonArray chArray;
            for (const ViDinChannelConfig *ch : m_channels) {
                QJsonObject chObj;
                ch->save(chObj);
                chArray.append(chObj);
            }
            cfgObj[cfgkey_acq_chs_arr] = chArray;
        }
    }

    void ViDinConfig::protLoad(const QJsonObject &cfgObj) {
        const QJsonArray &chArray {cfgObj[cfgkey_acq_chs_arr].toArray()};
        int ch_idx {0};
        for (const auto &it : chArray) {
            if (ch_idx < m_channels.size()) {
                m_channels.at(ch_idx)->load(it.toObject());
            } else {
                ViDinChannelConfig *ch{new ViDinChannelConfig{*this, ch_idx}};
                ch->load(it.toObject());
                addSubItem(ch);
                m_channels += ch;
            }
            ++ch_idx;
        }
    }

    void ViDinConfig::viModuleInfoUpdate(const ViModuleTypeInfo &viInfo) {
        m_ch_cnt = viInfo.dinChCnt();
        for (int ch_idx {m_channels.size()}; ch_idx < m_ch_cnt; ++ch_idx) {
            ViDinChannelConfig *ch{new ViDinChannelConfig{*this, ch_idx}};
            addSubItem(ch);
            m_channels += ch;
        }
    }
}
