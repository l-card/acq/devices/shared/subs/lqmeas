#include "ViConfigItem.h"
#include "lqmeas/devs/vi/modules/ViModuleTypeInfo.h"

namespace LQMeas {
    ViConfigItem::ViConfigItem(QObject *parent) : DeviceConfigItem{parent} {

    }

    ViConfigItem::ViConfigItem(const ViModuleTypeInfo &devTypeInfo, QObject *parent) :
        DeviceConfigItem{parent}, m_viModuleTypeInfo{&devTypeInfo} {

    }

    void ViConfigItem::protUpdateInfo(const DeviceTypeInfo *info) {
        if (info) {
            const ViModuleTypeInfo &viInfo {static_cast<const ViModuleTypeInfo &>(*info)};
            m_viModuleTypeInfo = &viInfo;
            viModuleInfoUpdate(viInfo);
        } else {
            m_viModuleTypeInfo = nullptr;
        }
        DeviceConfigItem::protUpdateInfo(info);
    }
}
