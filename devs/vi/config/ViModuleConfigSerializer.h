#ifndef LQMEAS_VIMODULEMBCONFIGSERIALIZER_H
#define LQMEAS_VIMODULEMBCONFIGSERIALIZER_H


#include "lqmeas/devs/vi/protocol/vi_module_opcfg.h"
#include <QtGlobal>
class LQError;
class QByteArray;


namespace LQMeas {
    class ViModuleConfig;
    class ViAcqChannelType;
    class ViAcqChannelConfig;
    class ViMeasConfig;    
    class ViMeasType;
    class ViSetpointConfig;
    class ViEventsConfig;
    class ViEventConfig;
    class ViDinChannelConfig;
    class ViDoutChannelConfig;
    class ViDoutChannelType;
    class ViWaveformSrcConfig;
    class ViConfigRef;
    class ViConfigRefEvent;
    class ViConfigRefDin;
    class ViConfigRefAcqCh;
    class ViConfigRefMeas;
    class ViExchConfig;

    class ViModuleMbConfigSerializer {
    public:
        static void parse(const QByteArray &data, ViModuleConfig &cfg, LQError &err);
        static void serialize(const ViModuleConfig &cfg, QByteArray &resArray, LQError &err);

        static const ViAcqChannelType &getAcqChType(quint16 type_code);
        static const quint16 getAcqChTypeCode(const ViAcqChannelType &acq_ch_type);

        static const ViMeasType &getMeasType(quint16 type_code);
        static const quint16 getMeasTypeCode(const ViMeasType &meas_type);

        static const ViDoutChannelType &getDoutChType(quint16 type_code);
        static const quint16 getDoutChTypeCode(const ViDoutChannelType &ch_type);
    private:
        static void addBlock(QByteArray &resArray, t_vi_opcfg_block_type type, const QByteArray &blockData);

        static void serializeDevRef(QByteArray &resArray, int idx, const QString &devid);
        static void pareseDevRef(const t_vi_opcfg_devref &cfgblock, unsigned size, QList<QString> &remoteDevIds, LQError &err);

        static void serializeDevName(QByteArray &resArray, const ViModuleConfig &devCfg);
        static void parseDevName(ViModuleConfig &cfg, const uint8_t *text, unsigned size, LQError &err);

        static void serializeAcqChGen(QByteArray &resArray, const ViAcqChannelConfig &acqChCfg, QList<QString> &remoteDevIds);
        static void parseAcqChGen(ViModuleConfig &cfg, const t_vi_opcfg_block_acqch_gen &cfgblock, unsigned size, const QList<QString> &remoteDevIds, LQError &err);

        static void serializeAcqChName(QByteArray &resArray, const ViAcqChannelConfig &acqChCfg);
        static void parseAcqChName(ViModuleConfig &cfg, const t_vi_opcfg_block_acqch_name &cfgblock, unsigned size, LQError &err);

        static void serializeAcqChSensor(QByteArray &resArray, const ViAcqChannelConfig &acqChCfg);
        static void parseAcqChSensor(ViModuleConfig &cfg, const t_vi_opcfg_block_acqch_sensor &cfgblock, unsigned size, LQError &err);

        static void serializeAcqChPhaseTrig(QByteArray &resArray, const ViAcqChannelConfig &acqChCfg);
        static void parseAcqChPhaseTrig(ViModuleConfig &cfg, const t_vi_opcfg_block_acqch_phasetrig &cfgblock, unsigned size, LQError &err);

        static void serializeAcqChPosition(QByteArray &resArray, const ViAcqChannelConfig &acqChCfg);
        static void parseAcqChPosition(ViModuleConfig &cfg, const t_vi_opcfg_block_acqch_position &cfgblock, unsigned size, LQError &err);



        static void serializeMeasGen(QByteArray &resArray, const ViMeasConfig &measCfg);
        static void parseMeasGen(ViModuleConfig &cfg, const t_vi_opcfg_block_meas_gen &cfgblock, unsigned size, LQError &err);

        static void serializeMeasName(QByteArray &resArray, const ViMeasConfig &measCfg);
        static void parseMeasName(ViModuleConfig &cfg, const t_vi_opcfg_block_meas_name &cfgblock, unsigned size, LQError &err);

        static void serializeMeasSetpoint(QByteArray &resArray, const ViSetpointConfig &spCfg);
        static void parseMeasSetpoint(ViModuleConfig &cfg, const t_vi_opcfg_block_meas_setpoint &cfgblock, unsigned size, LQError &err);

        static void serializeEventGen(QByteArray &resArray, const ViEventConfig &eventCfg, QList<QString> &remoteDevIds);
        static void parseEventGen(ViModuleConfig &cfg, const t_vi_opcfg_block_event_gen &cfgblock, unsigned size, const QList<QString> &remoteDevIds, LQError &err);

        static void serializeEventName(QByteArray &resArray, const ViEventConfig &eventCfg);
        static void parseEventName(ViModuleConfig &cfg, const t_vi_opcfg_block_event_name &cfgblock, unsigned size, LQError &err);

        static void serializeDinGen(QByteArray &resArray, const ViDinChannelConfig &dinChCfg);
        static void parseDinGen(ViModuleConfig &cfg, const t_vi_opcfg_block_din_gen &cfgblock, unsigned size, LQError &err);

        static void serializeDinName(QByteArray &resArray, const ViDinChannelConfig &dinChCfg);
        static void parseDinName(ViModuleConfig &cfg, const t_vi_opcfg_block_din_name &cfgblock, unsigned size, LQError &err);

        static void serializeDoutGen(QByteArray &resArray, const ViDoutChannelConfig &doutChCfg, QList<QString> &remoteDevIds);
        static void parseDoutGen(ViModuleConfig &cfg, const t_vi_opcfg_block_dout_gen &cfgblock, unsigned size, const QList<QString> &remoteDevIds, LQError &err);

        static void serializeDoutName(QByteArray &resArray, const ViDoutChannelConfig &doutChCfg);
        static void parseDoutName(ViModuleConfig &cfg, const t_vi_opcfg_block_dout_name &cfgblock, unsigned size, LQError &err);

        static void serializeWfGen(QByteArray &resArray, const ViWaveformSrcConfig &wfSrcCfg, QList<QString> &remoteDevIds);
        static void parseWfGen(ViModuleConfig &cfg, const t_vi_opcfg_block_wf_gen &cfgblock, unsigned size, const QList<QString> &remoteDevIds, LQError &err);

        static void serializeWfName(QByteArray &resArray, const ViWaveformSrcConfig &wfSrcCfg);
        static void parseWfName(ViModuleConfig &cfg, const t_vi_opcfg_block_wf_name &cfgblock, unsigned size, LQError &err);

        static void serializeExchGen(QByteArray &resArray, const ViExchConfig &exchCfg);
        static void parseExchGen(ViModuleConfig &cfg, const t_vi_opcfg_block_exch_gen &cfgblock, unsigned size, LQError &err);



        static void serializeRefHdr(t_vi_opcfg_ref_hdr &refhdr, const ViConfigRef &ref, QList<QString> &remoteDevIds);
        static void parseRefHdr(ViConfigRef &ref, const t_vi_opcfg_ref_hdr &refhdr, const QList<QString> &remoteDevIds);

        static void serializeEventRef(t_vi_opcfg_evt_ref &refblock, const ViConfigRefEvent &evtRef, QList<QString> &remoteDevIds);
        static void parseEventRef(ViConfigRefEvent &evtRef, const t_vi_opcfg_evt_ref &refblock, const QList<QString> &remoteDevIds);

        static void serializeDinRef(t_vi_opcfg_din_ref &refblock, const ViConfigRefDin &dinRef, QList<QString> &remoteDevIds);
        static void parseDinRef(ViConfigRefDin &dinRef, const t_vi_opcfg_din_ref &refblock, const QList<QString> &remoteDevIds);

        static void serializeAcqChRef(t_vi_opcfg_ch_ref &refblock, const ViConfigRefAcqCh &acqChRef, QList<QString> &remoteDevIds);
        static void parseAcqChRef(ViConfigRefAcqCh &acqChRef, const t_vi_opcfg_ch_ref &refblock, const QList<QString> &remoteDevIds);

        static void serializeMeasRef(t_vi_opcfg_meas_ref &refblock, const ViConfigRefMeas &measRef, QList<QString> &remoteDevIds);
        static void parseMeasRef(ViConfigRefMeas &measRef, const t_vi_opcfg_meas_ref &refblock, const QList<QString> &remoteDevIds);


    };
}

#endif // VIMODULEMBCONFIGSERIALIZER_H
