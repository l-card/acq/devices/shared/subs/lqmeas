#include "ViChannelConfig.h"
#include "ViConfig.h"
#include "exch/ViExchConfig.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>
#include <QStringBuilder>

namespace LQMeas {
    static const QLatin1String &cfgkey_ch_enabled         {StdConfigKeys::enabled()};
    static const QLatin1String &cfgkey_ch_name            {StdConfigKeys::customName()};


    ViChannelConfig::ViChannelConfig(ViConfig &viConfig, QObject *parent, bool default_en) :
        ViConfigItem{viConfig.viModuleTypeInfo(), parent}
        , m_enabled{default_en}
        , m_def_enabled{default_en}
        , m_viCfg{&viConfig} {

        connect(&viConfig.exch(), &ViExchConfig::pubEnableStateChanged, this, &ViChannelConfig::pubEnableStateChanged);
    }

    ViChannelConfig::ViChannelConfig(const ViChannelConfig &other, ViConfig &viConfig, QObject *parent) :
        ViConfigItem{viConfig.viModuleTypeInfo(), parent}
        , m_def_enabled{other.m_def_enabled}
        , m_enabled{other.m_enabled}
        , m_custom_name{other.m_custom_name}
        , m_viCfg{&viConfig} {

        connect(&viConfig.exch(), &ViExchConfig::pubEnableStateChanged, this, &ViChannelConfig::pubEnableStateChanged);
    }

    QString ViChannelConfig::displayName() const {
        return m_custom_name.isEmpty() ? origName() : m_custom_name;
    }

    QString ViChannelConfig::fullDisplayName() const {
        return m_custom_name.isEmpty() ? origName() : origName() % QStringLiteral(": ") % m_custom_name;
    }

    void ViChannelConfig::setEnabled(bool en)  {
        if (en != m_enabled) {
            m_enabled = en;
            notifyConfigChanged();
        }
    }

    bool ViChannelConfig::pubIsEnabled(ViChannelPubInfoType infoType) const {
        return infoType == ViChannelPubInfoType::State ? m_viCfg->exch().pubIsEnabled() : false;
    }

    void ViChannelConfig::setCustomName(const QString &name) {
        if (name != m_custom_name) {
            m_custom_name = name;
            notifyConfigChanged();
            Q_EMIT displayNameChanged();
        }
    }

    void ViChannelConfig::protSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_ch_enabled] = m_enabled;
        cfgObj[cfgkey_ch_name] = m_custom_name;
        chCfgSave(cfgObj);
    }

    void ViChannelConfig::protLoad(const QJsonObject &cfgObj) {
        m_enabled = cfgObj[cfgkey_ch_enabled].toBool(m_def_enabled);
        m_custom_name = cfgObj[cfgkey_ch_name].toString();
        chCfgLoad(cfgObj);
    }
}
