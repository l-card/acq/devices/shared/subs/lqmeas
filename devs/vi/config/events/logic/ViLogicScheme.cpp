#include "ViLogicScheme.h"
#include "../ViEventConfig.h"
#include "../ViEventsConfig.h"
#include "lqmeas/devs/vi/config/ViConfig.h"
#include "ViLogicElementType.h"
#include "ViLogicElementConfig.h"
#include "types/ViLogicElementConfigAnd.h"
#include "types/ViLogicElementConfigOr.h"
#include "types/ViLogicElementConfigNot.h"
#include "types/ViLogicElementConfigMeasCond.h"
#include "types/ViLogicElementConfigDin.h"
#include "types/ViLogicElementConfigEvent.h"
#include "lqmeas/lqtdefs.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QHash>

namespace LQMeas {
    static const QLatin1String cfgkey_scheme_base_id     {"BaseElement"};
    static const QLatin1String cfgkey_scheme_elems_arr   {"Elements"};
    static const QLatin1String cfgkey_scheme_con_arr     {"Connections"};
    static const QLatin1String cfgkey_scheme_con_in_id   {"InElement"};
    static const QLatin1String cfgkey_scheme_con_in_num  {"InNumber"};
    static const QLatin1String cfgkey_scheme_con_out_id  {"OutElement"};

    struct ViLogicElemConInfo {
        int inElementID {-1};
        int inNumber {-1};
        int outElementID {-1};

        void save(QJsonObject &cfgObj) const {
            cfgObj[cfgkey_scheme_con_in_id]  = inElementID;
            cfgObj[cfgkey_scheme_con_in_num] = inNumber;
            cfgObj[cfgkey_scheme_con_out_id] = outElementID;
        }

        void load(const QJsonObject &cfgObj) {
            inElementID  = cfgObj[cfgkey_scheme_con_in_id].toInt();
            inNumber     = cfgObj[cfgkey_scheme_con_in_num].toInt();
            outElementID = cfgObj[cfgkey_scheme_con_out_id].toInt();
        }
    };


    ViLogicScheme::ViLogicScheme(ViEventConfig &eventCfg) : ViConfigItem{&eventCfg},
        m_evntCfg{&eventCfg} {

    }

    ViLogicScheme::ViLogicScheme(ViEventConfig &eventCfg, ViLogicScheme &origScheme) : ViConfigItem{&eventCfg},
        m_evntCfg{&eventCfg} {


        if (origScheme.m_baseElem) {
            m_baseElem = origScheme.m_baseElem->clone(*this);
            addSubItem(m_baseElem);
            cloneInputElement(*m_baseElem, *origScheme.m_baseElem);
        }
    }

    ViLogicElementConfig *ViLogicScheme::createBaseElement(const ViLogicElementType &type) {
        ViLogicElementConfig *oldElem {m_baseElem};
        ViLogicElementConfig *newElem {type.create(*this, m_next_idx++)};
        m_baseElem = newElem;
        if (oldElem) {
            if (newElem->inputsCount() > 0) {
                newElem->setInElement(0, oldElem);
            } else {
                removeRecursive(*oldElem);
            }
        }
        addSubItem(m_baseElem);
        Q_EMIT elementAppend(m_baseElem);
        return m_baseElem;
    }

    ViLogicElementConfig *ViLogicScheme::createInsertElement(ViLogicElementConfig &inputElem, int input_idx, const ViLogicElementType &type) {
        ViLogicElementConfig *newElem {type.create(*this, m_next_idx++)};
        ViLogicElementConfig *oldElem {inputElem.inElement(input_idx)};
        inputElem.setInElement(input_idx, newElem);
        if (oldElem) {
            if (newElem->inputsCount() > 0) {
                newElem->setInElement(0, oldElem);
            } else {
                removeRecursive(*oldElem);
            }
        }
        addSubItem(newElem);
        Q_EMIT elementAppend(newElem);
        return newElem;
    }

    void ViLogicScheme::removeElement(ViLogicElementConfig &remElem, int saveInput) {
        if (&remElem == m_baseElem) {
            if ((saveInput >= 0) && (remElem.inElement(saveInput) != nullptr)) {
                m_baseElem = remElem.inElement(saveInput);
                remElem.setInElement(saveInput, nullptr);
            } else {
                m_baseElem = nullptr;
            }
            removeRecursive(remElem);
        } else {
            const QList<ViLogicElementConfig *> elems {allElements()};
            const auto it {std::find_if(elems.cbegin(), elems.cend(),
                                       [&remElem](ViLogicElementConfig *elem) {return elem->fullInList().contains(&remElem);})};
            if (it != elems.cend()) {
                ViLogicElementConfig *inElem = *it;
                int inElemInput = inElem->fullInList().indexOf(&remElem);
                if ((saveInput >= 0) && (remElem.inElement(saveInput) != nullptr)) {
                    inElem->setInElement(inElemInput, remElem.inElement(saveInput));
                    remElem.setInElement(saveInput, nullptr);
                } else {
                    inElem->setInElement(inElemInput, nullptr);
                }
                removeRecursive(remElem);
            }
        }
    }

    void ViLogicScheme::clearScheme()  {
        if (m_baseElem) {
            removeRecursive(*m_baseElem);
        }
    }

    ViLogicElementConfig *ViLogicScheme::getOutConElem(ViLogicElementConfig &targetElemCfg, int &input_idx) const {
        ViLogicElementConfig *ret {nullptr};
        QList<ViLogicElementConfig *> elems {allElements()};
        const auto &it {std::find_if(elems.cbegin(), elems.cend(),
                                    [&targetElemCfg](ViLogicElementConfig *elemCfg) {
                                        return elemCfg->fullInList().contains(&targetElemCfg);
        })};
        if (it != elems.cend()) {
            ret = *it;
            input_idx = ret->fullInList().indexOf(&targetElemCfg);
        }
        return ret;
    }

    QList<ViLogicElementConfig *> ViLogicScheme::allElements(bool include_empty, bool inverse_order) const {
        QList<ViLogicElementConfig *> ret;
        if (m_baseElem) {
            if (!inverse_order) {
                ret += m_baseElem;
            }
            ret += m_baseElem->getElemsRecursively(include_empty, inverse_order);
            if (inverse_order) {
                ret += m_baseElem;
            }
        } else if (include_empty) {
            ret += m_baseElem;
        }
        return ret;
    }

    QList<const ViLogicElementType *> ViLogicScheme::supportedElemTypes() const {
        static const QList<const ViLogicElementType *>  list {
            &ViLogicElementConfigAnd::type(),
            &ViLogicElementConfigOr::type(),
            &ViLogicElementConfigNot::type(),
            &ViLogicElementConfigMeasCond::type(),
            &ViLogicElementConfigDin::type(),
            &ViLogicElementConfigEvent::type(),
        };
        return list;
    }

    ViConfigRefCtl &ViLogicScheme::refs() {
        return m_evntCfg->events().viConfig().refs();
    }

    const ViConfigRefCtl &ViLogicScheme::refs() const {
        return m_evntCfg->events().viConfig().refs();
    }

    void ViLogicScheme::protSave(QJsonObject &cfgObj) const {
        if (m_baseElem) {
            QList<ViLogicElemConInfo> connections;

            cfgObj[cfgkey_scheme_base_id] = m_baseElem->id();

            QJsonArray elemsArray;
            const QList<ViLogicElementConfig *> elems {allElements()};
            for (const ViLogicElementConfig *elem : elems) {
                QJsonObject elemObj;
                elem->save(elemObj);
                elemsArray.append(elemObj);

                for (int input_idx {0}; input_idx < elem->inputsCount(); ++input_idx) {
                    const ViLogicElementConfig *conElem {elem->inElement(input_idx)};
                    if (conElem) {
                        ViLogicElemConInfo cinfo;
                        cinfo.inElementID = elem->id();
                        cinfo.inNumber = input_idx;
                        cinfo.outElementID = conElem->id();
                        connections += cinfo;
                    }
                }
            }
            cfgObj[cfgkey_scheme_elems_arr] = elemsArray;

            if (!connections.isEmpty()) {
                QJsonArray conArray;
                for (const ViLogicElemConInfo &con : qAsConst(connections)) {
                    QJsonObject conObj;
                    con.save(conObj);
                    conArray.append(conObj);
                }
                cfgObj[cfgkey_scheme_con_arr] = conArray;
            }
        }
    }

    void ViLogicScheme::protLoad(const QJsonObject &cfgObj) {
        clearScheme();

        QList<const ViLogicElementType *> elemTypes {supportedElemTypes()};
        QHash<int, ViLogicElementConfig *> elemsMap;

        const QJsonArray &elemsArray {cfgObj[cfgkey_scheme_elems_arr].toArray()};
        for (const auto &it : elemsArray) {
            const QJsonObject &elemObj {it.toObject()};

            const QString elemTypeId {ViLogicElementConfig::loadTypeId(elemObj)};
            const auto &elemTypeIt {std::find_if(elemTypes.cbegin(), elemTypes.cend(),
                                            [&elemTypeId](const ViLogicElementType *elemType){return elemType->id() == elemTypeId;})};
            if (elemTypeIt != elemTypes.cend()) {
                const ViLogicElementType *type {*elemTypeIt};
                ViLogicElementConfig *elemCfg { type->create(*this, 0) };
                elemCfg->load(elemObj);
                m_next_idx = qMax(m_next_idx, elemCfg->id() + 1);
                /** @todo check if present */
                elemsMap[elemCfg->id()] = elemCfg;
            }
        }

        if (!elemsMap.isEmpty()) {
            int base_id {cfgObj[cfgkey_scheme_base_id].toInt()};
            m_baseElem = elemsMap[base_id];


            const QJsonArray &conArray {cfgObj[cfgkey_scheme_con_arr].toArray()};
            for (const auto &it : conArray) {
                ViLogicElemConInfo cinfo;
                cinfo.load(it.toObject());
                ViLogicElementConfig *inElem {elemsMap[cinfo.inElementID]};
                ViLogicElementConfig *outElem {elemsMap[cinfo.outElementID]};
                if (inElem && outElem && (inElem != outElem) && (cinfo.inNumber >= 0) && (cinfo.inNumber < inElem->inputsCount())) {
                    inElem->setInElement(cinfo.inNumber, outElem);
                }
            }


            const QList<ViLogicElementConfig *> elems {allElements()};
            for (ViLogicElementConfig *elem : elems) {
                Q_EMIT elementAppend(elem);
                addSubItem(elem);
            }
            /** @todo delete not used */
        }
    }

    void ViLogicScheme::cloneInputElement(ViLogicElementConfig &srcElem, const ViLogicElementConfig &origSrcElem) {
        for (int input_idx {0}; input_idx < origSrcElem.inputsCount(); ++input_idx) {
            const ViLogicElementConfig *origElem {origSrcElem.inElement(input_idx)};
            if (origElem) {
                ViLogicElementConfig *newElem {origElem->clone(*this)};
                srcElem.setInElement(input_idx, newElem);
                addSubItem(newElem);
                cloneInputElement(*newElem, *origElem);
            }
        }
    }

    void ViLogicScheme::removeRecursive(ViLogicElementConfig &remElem) {
        for (int input_idx {0}; input_idx < remElem.inputsCount(); ++input_idx) {
            ViLogicElementConfig *inElem {remElem.inElement(input_idx)};
            if (inElem) {
                removeRecursive(*inElem);
            }
        }
        deleteElement(remElem);
    }

    void ViLogicScheme::deleteElement(ViLogicElementConfig &remElem) {
        remElem.cleanup();
        if (m_baseElem == &remElem) {
            m_baseElem = nullptr;
        }
        remSubItem(&remElem);
        Q_EMIT elementRemoved(&remElem);
        remElem.deleteLater();
    }
}
