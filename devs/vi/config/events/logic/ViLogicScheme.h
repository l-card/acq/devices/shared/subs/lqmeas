#ifndef LQMEAS_VILOGICSCHEME_H
#define LQMEAS_VILOGICSCHEME_H

#include "lqmeas/devs/vi/config/ViConfigItem.h"

namespace LQMeas {
    class ViEventConfig;
    class ViLogicElementConfig;
    class ViLogicElementType;
    class ViConfigRefCtl;

    class ViLogicScheme : public ViConfigItem {
        Q_OBJECT
    public:
        explicit ViLogicScheme(ViEventConfig &eventCfg);
        explicit ViLogicScheme(ViEventConfig &eventCfg, ViLogicScheme &origScheme);


        const ViEventConfig &eventCfg() const {return *m_evntCfg;}

        ViLogicElementConfig *createBaseElement(const ViLogicElementType &type);
        ViLogicElementConfig *createInsertElement(ViLogicElementConfig &inputElem, int input_idx, const ViLogicElementType &type);
        void removeElement(ViLogicElementConfig &elemCfg, int saveInput = -1);
        void clearScheme();

        ViLogicElementConfig *getOutConElem(ViLogicElementConfig &elem, int &input_idx) const;

        QList<ViLogicElementConfig *> allElements(bool include_empty = false, bool inverse_order = false) const;
        bool hasBaseElement() const {return m_baseElem != nullptr;}
        const ViLogicElementConfig &baseElement() const {return *m_baseElem;}
        ViLogicElementConfig &baseElement() {return *m_baseElem;}

        QList<const ViLogicElementType *> supportedElemTypes() const;

        ViConfigRefCtl &refs();
        const ViConfigRefCtl &refs() const;
    Q_SIGNALS:
        void elementAppend(LQMeas::ViLogicElementConfig *elem);
        void elementRemoved(LQMeas::ViLogicElementConfig *elem);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        void cloneInputElement(ViLogicElementConfig &srcElem, const ViLogicElementConfig &origSrcElem);

        void removeRecursive(ViLogicElementConfig &remElem);
        void deleteElement(ViLogicElementConfig &remElem);

        ViEventConfig *m_evntCfg;
        ViLogicElementConfig *m_baseElem {nullptr};
        int m_next_idx {0};
    };
}

#endif // LQMEAS_VILOGICSCHEME_H
