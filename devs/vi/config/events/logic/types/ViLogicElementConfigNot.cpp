#include "ViLogicElementConfigNot.h"
#include "../ViLogicElementType.h"

namespace LQMeas {
    const ViLogicElementType &ViLogicElementConfigNot::type() {
        static const class ViLogicElementTypeNot : public ViLogicElementType {
        public:
            QString id() const override {return QStringLiteral("vi_logelem_not");}
            QString displayName() const override {return tr("NOT");}
            int inputsCount() const override {return 1;}
            ViLogicElementConfig *create(ViLogicScheme &scheme, int id) const override {
                return new ViLogicElementConfigNot{scheme, id};
            }
        } t;
        return t;
    }

    ViLogicElementConfigNot::ViLogicElementConfigNot(ViLogicScheme &logicScheme, int id) :
        ViLogicElementConfig(logicScheme, id, type()) {

    }

    ViLogicElementConfig *ViLogicElementConfigNot::clone(ViLogicScheme &logicScheme) const {
        return new ViLogicElementConfigNot{logicScheme, id()};
    }
}
