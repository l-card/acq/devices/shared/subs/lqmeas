#ifndef LQMEAS_VILOGICELEMENTCONFIGAND_H
#define LQMEAS_VILOGICELEMENTCONFIGAND_H

#include "../ViLogicElementConfig.h"

namespace LQMeas {
    class ViLogicElementConfigAnd : public ViLogicElementConfig {
        Q_OBJECT
    public:
        static const ViLogicElementType &type();

        explicit ViLogicElementConfigAnd(ViLogicScheme &logicScheme, int id);

        ViLogicElementConfig *clone(ViLogicScheme &logicScheme) const override;
    };
}

#endif // LQMEAS_VILOGICELEMENTCONFIGAND_H
