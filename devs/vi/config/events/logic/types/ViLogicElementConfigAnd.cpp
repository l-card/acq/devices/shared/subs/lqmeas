#include "ViLogicElementConfigAnd.h"
#include "../ViLogicElementType.h"

namespace LQMeas {

    const ViLogicElementType &ViLogicElementConfigAnd::type() {
        static const class ViLogicElementTypeAnd : public ViLogicElementType {
        public:
            QString id() const override {return QStringLiteral("vi_logelem_and");}
            QString displayName() const override {return tr("AND");}
            int inputsCount() const override {return 2;}
            ViLogicElementConfig *create(ViLogicScheme &scheme, int id) const override {
                return new ViLogicElementConfigAnd{scheme, id};
            }
        } t;
        return t;
    }

    ViLogicElementConfigAnd::ViLogicElementConfigAnd(ViLogicScheme &logicScheme, int id) :
        ViLogicElementConfig(logicScheme, id, type()) {

    }

    ViLogicElementConfig *ViLogicElementConfigAnd::clone(ViLogicScheme &logicScheme) const {
        return new ViLogicElementConfigAnd{logicScheme, id()};
    }

}
