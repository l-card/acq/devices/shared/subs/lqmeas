#include "ViLogicElementConfigMeasCond.h"
#include "../ViLogicElementType.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefMeas.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefCtl.h"
#include "lqmeas/devs/vi/config/acq/meas/ViMeasCondType.h"
#include "lqmeas/devs/vi/config/events/logic/ViLogicScheme.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_cond_type       {"CondType"};
    static const QLatin1String cfgkey_meas_ref        {"MeasRef"};

    const ViLogicElementType &ViLogicElementConfigMeasCond::type() {
        static const class ViLogicElementTypeMeasCond : public ViLogicElementType {
        public:
            QString id() const override {return QStringLiteral("vi_logelem_meas_cond");}
            QString displayName() const override {return tr("Measurement Condition");}
            int inputsCount() const override {return 0;}
            ViLogicElementConfig *create(ViLogicScheme &scheme, int id) const override {
                return new ViLogicElementConfigMeasCond{scheme, id};
            }
        } t;
        return t;
    }

    ViLogicElementConfigMeasCond::ViLogicElementConfigMeasCond(ViLogicScheme &logicScheme, int id) :
        ViLogicElementConfig{logicScheme, id, type()},
        m_ref{new ViConfigRefMeas{true}},
        m_condType{&defaultCondType()} {

        addSubItem(m_ref.get());
        logicScheme.refs().addRef(*m_ref);
    }

    ViLogicElementConfigMeasCond::ViLogicElementConfigMeasCond(ViLogicScheme &logicScheme,
                                                               const ViLogicElementConfigMeasCond &origElem) :
        ViLogicElementConfig{logicScheme, origElem},
        m_ref{new ViConfigRefMeas{*origElem.m_ref}},
        m_condType{origElem.m_condType} {

        addSubItem(m_ref.get());
        logicScheme.refs().addRef(*m_ref);
    }

    ViLogicElementConfigMeasCond::~ViLogicElementConfigMeasCond() {
        logicScheme().refs().removeRef(*m_ref);
    }

    const ViMeasCondType &ViLogicElementConfigMeasCond::defaultCondType() {
        return ViMeasCondTypes::alert();
    }

    void ViLogicElementConfigMeasCond::setCondType(const ViMeasCondType &type) {
        if (m_condType != &type) {
            m_condType = &type;
            notifyConfigChanged();
        }
    }

    QList<const ViMeasCondType *> ViLogicElementConfigMeasCond::supportedCondTypes() const {
        return ViMeasCondTypes::all();
    }

    void ViLogicElementConfigMeasCond::elementCleanup() {
        logicScheme().refs().removeRef(*m_ref);
    }

    void ViLogicElementConfigMeasCond::elementSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_cond_type] = m_condType->id();
        saveSubItem(cfgObj, *m_ref, cfgkey_meas_ref);
    }

    void ViLogicElementConfigMeasCond::elementLoad(const QJsonObject &cfgObj) {
        const QString condTypeId {cfgObj[cfgkey_cond_type].toString()};
        const QList<const ViMeasCondType *> condTypes {supportedCondTypes()};
        const auto &it {std::find_if(condTypes.cbegin(), condTypes.cend(),
                                    [&condTypeId](const ViMeasCondType *type){return type->id() == condTypeId;})};
        m_condType = it != condTypes.cend() ? *it : &defaultCondType();

        loadSubItem(cfgObj, *m_ref, cfgkey_meas_ref);
    }
}
