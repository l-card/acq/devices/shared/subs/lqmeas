#include "ViLogicElementConfigOr.h"
#include "../ViLogicElementType.h"

namespace LQMeas {
    const ViLogicElementType &ViLogicElementConfigOr::type() {
        static const class ViLogicElementTypeOr : public ViLogicElementType {
        public:
            QString id() const override {return QStringLiteral("vi_logelem_or");}
            QString displayName() const override {return tr("OR");}
            int inputsCount() const override {return 2;}
            ViLogicElementConfig *create(ViLogicScheme &scheme, int id) const override {
                return new ViLogicElementConfigOr{scheme, id};
            }
        } t;
        return t;
    }

    ViLogicElementConfigOr::ViLogicElementConfigOr(ViLogicScheme &logicScheme, int id) :
        ViLogicElementConfig(logicScheme, id, type()) {

    }

    ViLogicElementConfig *ViLogicElementConfigOr::clone(ViLogicScheme &logicScheme) const {
        return new ViLogicElementConfigOr{logicScheme, id()};
    }
}
