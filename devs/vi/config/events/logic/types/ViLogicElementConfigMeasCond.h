#ifndef LQMEAS_VILOGICELEMENTCONFIGMEASCOND_H
#define LQMEAS_VILOGICELEMENTCONFIGMEASCOND_H

#include "../ViLogicElementConfig.h"
#include <memory>
namespace LQMeas {
    class ViConfigRefMeas;
    class ViMeasCondType;

    class ViLogicElementConfigMeasCond : public ViLogicElementConfig  {
        Q_OBJECT
    public:
        static const ViLogicElementType &type();

        explicit ViLogicElementConfigMeasCond(ViLogicScheme &logicScheme, int id);
        explicit ViLogicElementConfigMeasCond(ViLogicScheme &logicScheme, const ViLogicElementConfigMeasCond &origElem);
        ~ViLogicElementConfigMeasCond() override;

        ViLogicElementConfig *clone(ViLogicScheme &logicScheme) const override {
            return new ViLogicElementConfigMeasCond{logicScheme, *this};
        }

        static const ViMeasCondType &defaultCondType();

        const ViConfigRefMeas &measRef() const {return *m_ref;}
        ViConfigRefMeas &measRef() {return *m_ref;}

        const ViMeasCondType &condType() const {return *m_condType;}
        void setCondType(const ViMeasCondType &type);

        QList<const ViMeasCondType *> supportedCondTypes() const;
    protected:
        void elementCleanup() override;
        void elementSave(QJsonObject &cfgObj) const override;
        void elementLoad(const QJsonObject &cfgObj) override;
    private:
        std::unique_ptr<ViConfigRefMeas> m_ref;
        const ViMeasCondType *m_condType;
    };
}

#endif // LQMEAS_VILOGICELEMENTCONFIGMEASCOND_H
