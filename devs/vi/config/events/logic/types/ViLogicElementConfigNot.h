#ifndef LQMEAS_VILOGICELEMENTCONFIGNOT_H
#define LQMEAS_VILOGICELEMENTCONFIGNOT_H

#include "../ViLogicElementConfig.h"

namespace LQMeas {
    class ViLogicElementConfigNot : public ViLogicElementConfig {
        Q_OBJECT
    public:
        static const ViLogicElementType &type();

        explicit ViLogicElementConfigNot(ViLogicScheme &logicScheme, int id);

        ViLogicElementConfig *clone(ViLogicScheme &logicScheme) const override;
    };
}


#endif // LQMEAS_VILOGICELEMENTCONFIGNOT_H
