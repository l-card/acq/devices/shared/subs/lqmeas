#include "ViLogicElementConfigDin.h"
#include "../ViLogicElementType.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefDin.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefCtl.h"
#include "lqmeas/devs/vi/config/events/logic/ViLogicScheme.h"

namespace LQMeas {
    static const QLatin1String cfgkey_din_ref        {"DinRef"};

    const ViLogicElementType &ViLogicElementConfigDin::type() {
        static const class ViLogicElementTypeDin : public ViLogicElementType {
        public:
            QString id() const override {return QStringLiteral("vi_logelem_din");}
            QString displayName() const override {return tr("Discrete input state");}
            int inputsCount() const override {return 0;}
            ViLogicElementConfig *create(ViLogicScheme &scheme, int id) const override {
                return new ViLogicElementConfigDin{scheme, id};
            }
        } t;
        return t;
    }

    ViLogicElementConfigDin::ViLogicElementConfigDin(ViLogicScheme &logicScheme, int id) :
        ViLogicElementConfig{logicScheme, id, type()},
        m_ref{new ViConfigRefDin{true}} {

        addSubItem(m_ref.get());
        logicScheme.refs().addRef(*m_ref);
    }

    ViLogicElementConfigDin::ViLogicElementConfigDin(ViLogicScheme &logicScheme,
                                                     const ViLogicElementConfigDin &origElem) :
        ViLogicElementConfig{logicScheme, origElem},
        m_ref{new ViConfigRefDin{*origElem.m_ref}} {

        addSubItem(m_ref.get());
        logicScheme.refs().addRef(*m_ref);
    }

    ViLogicElementConfigDin::~ViLogicElementConfigDin() {
        logicScheme().refs().removeRef(*m_ref);
    }

    void ViLogicElementConfigDin::elementCleanup() {
        logicScheme().refs().removeRef(*m_ref);
    }

    void ViLogicElementConfigDin::elementSave(QJsonObject &cfgObj) const {
        saveSubItem(cfgObj, *m_ref, cfgkey_din_ref);
    }

    void ViLogicElementConfigDin::elementLoad(const QJsonObject &cfgObj) {
        loadSubItem(cfgObj, *m_ref, cfgkey_din_ref);
    }

}
