#ifndef LQMEAS_VILOGICELEMENTCONFIGOR_H
#define LQMEAS_VILOGICELEMENTCONFIGOR_H

#include "../ViLogicElementConfig.h"

namespace LQMeas {
    class ViLogicElementConfigOr : public ViLogicElementConfig {
        Q_OBJECT
    public:
        static const ViLogicElementType &type();

        explicit ViLogicElementConfigOr(ViLogicScheme &logicScheme, int id);

        ViLogicElementConfig *clone(ViLogicScheme &logicScheme) const override;
    };
}

#endif // LQMEAS_VILOGICELEMENTCONFIGOR_H
