#include "ViLogicElementConfigEvent.h"
#include "../ViLogicElementType.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefEvent.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefCtl.h"
#include "lqmeas/devs/vi/config/events/logic/ViLogicScheme.h"

namespace LQMeas {
static const QLatin1String cfgkey_evt_ref        {"EventRef"};

    const ViLogicElementType &LQMeas::ViLogicElementConfigEvent::type() {
        static const class ViLogicElementTypeEvent : public ViLogicElementType {
        public:
            QString id() const override {return QStringLiteral("vi_logelem_event");}
            QString displayName() const override {return tr("Event state");}
            int inputsCount() const override {return 0;}
            ViLogicElementConfig *create(ViLogicScheme &scheme, int id) const override {
                return new ViLogicElementConfigEvent{scheme, id};
            }
        } t;
        return t;
    }

    ViLogicElementConfigEvent::ViLogicElementConfigEvent(ViLogicScheme &logicScheme, int id) :
        ViLogicElementConfig{logicScheme, id, type()},
        m_ref{new ViConfigRefEvent{true}} {

        addSubItem(m_ref.get());
        logicScheme.refs().addRef(*m_ref);
    }

    ViLogicElementConfigEvent::ViLogicElementConfigEvent(ViLogicScheme &logicScheme,
                                                         const ViLogicElementConfigEvent &origElem) :
        ViLogicElementConfig{logicScheme, origElem},
        m_ref{new ViConfigRefEvent{*origElem.m_ref}} {

        addSubItem(m_ref.get());
        logicScheme.refs().addRef(*m_ref);
    }

    ViLogicElementConfigEvent::~ViLogicElementConfigEvent() {
        logicScheme().refs().removeRef(*m_ref);
    }

    void ViLogicElementConfigEvent::elementCleanup() {
        logicScheme().refs().removeRef(*m_ref);
    }

    void ViLogicElementConfigEvent::elementSave(QJsonObject &cfgObj) const {
        saveSubItem(cfgObj, *m_ref, cfgkey_evt_ref);
    }

    void ViLogicElementConfigEvent::elementLoad(const QJsonObject &cfgObj) {
        loadSubItem(cfgObj, *m_ref, cfgkey_evt_ref);
    }
}
