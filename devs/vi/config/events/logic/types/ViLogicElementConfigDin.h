#ifndef LQMEAS_VILOGICELEMENTCONFIGDIN_H
#define LQMEAS_VILOGICELEMENTCONFIGDIN_H


#include "../ViLogicElementConfig.h"
#include <memory>

namespace LQMeas {
    class ViConfigRefDin;

    class ViLogicElementConfigDin: public ViLogicElementConfig  {
        Q_OBJECT
    public:
        static const ViLogicElementType &type();

        explicit ViLogicElementConfigDin(ViLogicScheme &logicScheme, int id);
        explicit ViLogicElementConfigDin(ViLogicScheme &logicScheme, const ViLogicElementConfigDin &origElem);
        ~ViLogicElementConfigDin() override;

        ViLogicElementConfig *clone(ViLogicScheme &logicScheme) const override {
            return new ViLogicElementConfigDin{logicScheme, *this};
        }


        const ViConfigRefDin &dinRef() const {return *m_ref;}
        ViConfigRefDin &dinRef() {return *m_ref;}
    protected:
        void elementCleanup() override;
        void elementSave(QJsonObject &cfgObj) const override;
        void elementLoad(const QJsonObject &cfgObj) override;
    private:
        std::unique_ptr<ViConfigRefDin> m_ref;
    };
}

#endif // LQMEAS_VILOGICELEMENTCONFIGDIN_H
