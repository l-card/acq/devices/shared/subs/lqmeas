#ifndef LQMEAS_VILOGICELEMENTCONFIGEVENT_H
#define LQMEAS_VILOGICELEMENTCONFIGEVENT_H


#include "../ViLogicElementConfig.h"
#include <memory>

namespace LQMeas {
    class ViConfigRefEvent;

    class ViLogicElementConfigEvent: public ViLogicElementConfig  {
        Q_OBJECT
    public:
        static const ViLogicElementType &type();

        explicit ViLogicElementConfigEvent(ViLogicScheme &logicScheme, int id);
        explicit ViLogicElementConfigEvent(ViLogicScheme &logicScheme, const ViLogicElementConfigEvent &origElem);
        ~ViLogicElementConfigEvent() override;

        ViLogicElementConfig *clone(ViLogicScheme &logicScheme) const override {
            return new ViLogicElementConfigEvent{logicScheme, *this};
        }


        const ViConfigRefEvent &eventRef() const {return *m_ref;}
        ViConfigRefEvent &eventRef() {return *m_ref;}
    protected:
        void elementCleanup() override;
        void elementSave(QJsonObject &cfgObj) const override;
        void elementLoad(const QJsonObject &cfgObj) override;
    private:
        std::unique_ptr<ViConfigRefEvent> m_ref;
    };
}

#endif // LQMEAS_VILOGICELEMENTCONFIGEVENT_H
