#ifndef LQMEAS_VILOGICELEMENTCONFIG_H
#define LQMEAS_VILOGICELEMENTCONFIG_H

#include "lqmeas/devs/vi/config/ViConfigItem.h"

namespace LQMeas {
    class ViLogicElementType;
    class ViLogicScheme;

    class ViLogicElementConfig : public ViConfigItem  {
    public:
        explicit ViLogicElementConfig(ViLogicScheme &logicScheme, int id, const ViLogicElementType &type);
        ViLogicElementConfig(ViLogicScheme &logicScheme, const ViLogicElementConfig &origCfg);

        virtual ViLogicElementConfig *clone(ViLogicScheme &logicScheme) const = 0;
        ViLogicElementConfig *cloneRecursively(ViLogicScheme &logicScheme) const;

        QList<ViLogicElementConfig *> getElemsRecursively(bool include_empty = false, bool inverse_order = false) const;

        const ViLogicElementType &elemType() const {return *m_type;}
        int id() const {return m_id;}
        const ViLogicScheme &logicScheme() const {return *m_logicScheme;}
        ViLogicScheme &logicScheme() {return *m_logicScheme;}

        int inputsCount() const {return m_inElements.size();}
        const ViLogicElementConfig *inElement(int input_idx) const {return m_inElements.at(input_idx);}
        ViLogicElementConfig *inElement(int input_idx) {return m_inElements.at(input_idx);}
        void setInElement(int input_idx, ViLogicElementConfig *conElement);
        QList<ViLogicElementConfig *> fullInList() const {return m_inElements;}


        void cleanup();

        static QString loadTypeId(const QJsonObject &cfgObj);

    protected:
        void protSave(QJsonObject &cfgObj) const override final;
        void protLoad(const QJsonObject &cfgObj) override final;

        virtual void elementSave(QJsonObject &cfgObj) const {}
        virtual void elementLoad(const QJsonObject &cfgObj) {}
        virtual void elementCleanup() {}
    private:
        ViLogicScheme *m_logicScheme;
        int m_id;
        const ViLogicElementType *m_type;
        QList<ViLogicElementConfig *> m_inElements;
    };
}

#endif // LQMEAS_VILOGICELEMENTCONFIG_H
