#include "ViLogicElementConfig.h"
#include "ViLogicElementType.h"
#include "ViLogicScheme.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>


namespace LQMeas {
    static const QLatin1String &cfgkey_elem_type            {StdConfigKeys::type()};
    static const QLatin1String &cfgkey_elem_id              {StdConfigKeys::ID()};

    ViLogicElementConfig::ViLogicElementConfig(ViLogicScheme &logicScheme, int id, const ViLogicElementType &type) :
        ViConfigItem{&logicScheme}, m_logicScheme{&logicScheme}, m_id{id}, m_type{&type} {

        while (m_inElements.size() < m_type->inputsCount()) {
            m_inElements += nullptr;
        }
    }

    ViLogicElementConfig::ViLogicElementConfig(ViLogicScheme &logicScheme, const ViLogicElementConfig &origCfg) :
        ViConfigItem{&logicScheme}, m_logicScheme{&logicScheme}, m_id{origCfg.m_id}, m_type{origCfg.m_type} {

    }

    ViLogicElementConfig *ViLogicElementConfig::cloneRecursively(ViLogicScheme &logicScheme) const {
        ViLogicElementConfig *elem {clone(logicScheme)};

        return elem;
    }

    QList<ViLogicElementConfig *> ViLogicElementConfig::getElemsRecursively(bool include_empty, bool inverse_order) const {
        QList<ViLogicElementConfig *> ret;
        for (ViLogicElementConfig *elem : m_inElements) {
            if (elem) {
                if (!inverse_order) {
                    ret += elem;
                }
                ret += elem->getElemsRecursively(include_empty, inverse_order);
                if (inverse_order) {
                    ret += elem;
                }
            } else if (include_empty) {
                ret += elem;
            }
        }
        return ret;
    }

    void ViLogicElementConfig::setInElement(int input_idx, ViLogicElementConfig *conElement) {
        m_inElements[input_idx] = conElement;
        notifyConfigChanged();
    }

    void ViLogicElementConfig::cleanup() {
        elementCleanup();
    }

    QString ViLogicElementConfig::loadTypeId(const QJsonObject &cfgObj) {
        return cfgObj[cfgkey_elem_type].toString();
    }

    void ViLogicElementConfig::protSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_elem_type] = m_type->id();
        cfgObj[cfgkey_elem_id] = m_id;
        elementSave(cfgObj);
    }

    void ViLogicElementConfig::protLoad(const QJsonObject &cfgObj) {
        m_id = cfgObj[cfgkey_elem_id].toInt();
        elementLoad(cfgObj);
    }
}
