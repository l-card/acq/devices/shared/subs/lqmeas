#ifndef LQMEAS_VILOGICELEMENTTYPE_H
#define LQMEAS_VILOGICELEMENTTYPE_H


#include "lqmeas/EnumNamedValue.h"
#include <QList>
#include <QMetaType>



namespace LQMeas {
    class ViLogicElementConfig;
    class ViLogicScheme;

    class ViLogicElementType : public EnumNamedValue<QString> {
    public:
        virtual int inputsCount() const = 0;
        virtual ViLogicElementConfig *create(ViLogicScheme &scheme, int id) const = 0;
    };
}

Q_DECLARE_METATYPE(const LQMeas::ViLogicElementType *);

#endif // LQMEAS_VILOGICELEMENTTYPE_H
