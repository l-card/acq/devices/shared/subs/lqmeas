#include "ViEventConfig.h"
#include "ViEventsConfig.h"
#include "logic/ViLogicScheme.h"
#include <QStringBuilder>
#include <QJsonObject>


namespace LQMeas {
    static const QLatin1String cfgkey_ch_logic_scheme_gr {"LogicScheme"};

    ViEventConfig::ViEventConfig(ViEventsConfig &events, int event_num) :
        ViChannelConfig{events.viConfig(), &events, true},
        m_eventsCfg{&events}, m_event_num{event_num},
        m_logicScheme{new ViLogicScheme{*this}} {

        addSubItem(m_logicScheme);
    }

    ViEventConfig::ViEventConfig(ViEventsConfig &events, const ViEventConfig &origEventCfg) :
        ViChannelConfig{origEventCfg, events.viConfig(), &events},
        m_eventsCfg{&events},
        m_event_num{origEventCfg.m_event_num},
        m_logicScheme{new ViLogicScheme{*this, *origEventCfg.m_logicScheme}} {

        addSubItem(m_logicScheme);
    }

    QString ViEventConfig::origName() const {
        return tr("Event") % QChar(' ') %  QString::number(m_event_num + 1);
    }


    void ViEventConfig::chCfgSave(QJsonObject &cfgObj) const {
        saveSubItem(cfgObj, *m_logicScheme, cfgkey_ch_logic_scheme_gr);
    }

    void ViEventConfig::chCfgLoad(const QJsonObject &cfgObj) {
        loadSubItem(cfgObj, *m_logicScheme, cfgkey_ch_logic_scheme_gr);
    }
    void ViEventConfig::setEventNumber(int num) {
        if (m_event_num != num) {
            m_event_num = num;
            Q_EMIT eventNumberChanged(num);
        }
    }
} // namespace LQMeas
