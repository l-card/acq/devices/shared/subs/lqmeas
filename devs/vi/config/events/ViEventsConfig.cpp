#include "ViEventsConfig.h"
#include "../ViConfig.h"
#include "ViEventConfig.h"
#include <QJsonArray>
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_events_arr   {"Events"};

    ViEventsConfig::ViEventsConfig(ViConfig &viConfig) : ViConfigItem{&viConfig}, m_viCfg{&viConfig} {

    }

    ViEventsConfig::ViEventsConfig(ViConfig &viConfig, const ViEventsConfig &origEvtCfg) :
        ViConfigItem{&viConfig}, m_viCfg{&viConfig} {
        for (ViEventConfig *evtCfg : origEvtCfg.m_events) {
            addEvent(*new ViEventConfig{*this, *evtCfg});
        }
    }

    void ViEventsConfig::removeAllEvents() {
        while (!m_events.isEmpty()) {
            removeEventAt(m_events.size()-1);
        }
    }

    void ViEventsConfig::removeEvent(ViEventConfig &event) {
        int evt_idx {m_events.indexOf(&event)};
        if (evt_idx >= 0) {
            removeEventAt(evt_idx);
            notifyConfigChanged();
        }
    }

    ViEventConfig *ViEventsConfig::addEvent() {
        ViEventConfig *event {new ViEventConfig{*this, m_events.size()}};
        addEvent(*event);
        notifyConfigChanged();
        return event;
    }

    ViEventConfig *ViEventsConfig::eventCfg(int evtNum) const {
        const auto &it {std::find_if(m_events.cbegin(), m_events.cend(),
                                    [evtNum](ViEventConfig *evtCfg){return evtCfg->eventNumber() == evtNum;})};
        return it != m_events.cend() ? *it : nullptr;
    }

    void ViEventsConfig::cleanup() {
        qDeleteAll(m_events);
        m_events.clear();
    }

    void ViEventsConfig::protSave(QJsonObject &cfgObj) const {
        QJsonArray eventsArray;
        for (const ViEventConfig *eventCfg : m_events) {
            QJsonObject eventObj;
            eventCfg->save(eventObj);
            eventsArray.append(eventObj);
        }
        cfgObj[cfgkey_events_arr] = eventsArray;
    }

    void ViEventsConfig::protLoad(const QJsonObject &cfgObj) {
        removeAllEvents();

        const QJsonArray &eventsArray {cfgObj[cfgkey_events_arr].toArray()};
        for (const auto &it : eventsArray) {
            const QJsonObject &eventObj {it.toObject()};
            ViEventConfig *event {new ViEventConfig{*this, m_events.size()}};
            event->load(eventObj);
            addEvent(*event);
        }
    }

    void ViEventsConfig::removeEventAt(int rem_idx) {
        ViEventConfig *event {m_events.takeAt(rem_idx)};
        remSubItem(event);
        Q_EMIT eventRemoved(event);
        event->deleteLater();
        for (int idx {rem_idx}; idx < m_events.size(); ++idx) {
            m_events[idx]->setEventNumber(idx);
        }
    }

    void ViEventsConfig::addEvent(ViEventConfig &event) {
        m_events += &event;
        addSubItem(&event);
        Q_EMIT eventAppend(&event);
    }
}
