#ifndef LQMEAS_VIEVENTCONFIG_H
#define LQMEAS_VIEVENTCONFIG_H


#include "lqmeas/devs/vi/config/ViChannelConfig.h"

namespace LQMeas {
    class ViEventsConfig;
    class ViLogicScheme;

    class ViEventConfig : public ViChannelConfig {
        Q_OBJECT
    public:
        explicit ViEventConfig(ViEventsConfig &events, int event_num);
        explicit ViEventConfig(ViEventsConfig &events, const ViEventConfig &origEventCfg);

        QString origName() const override;

        int eventNumber() const {return m_event_num; }
        void setEventNumber(int num);
        int channelNumber() const override {return eventNumber();}

        const ViLogicScheme &logicScheme() const {return *m_logicScheme;}
        ViLogicScheme &logicScheme() {return *m_logicScheme;}


        const ViEventsConfig &events() const {return *m_eventsCfg;}
        ViEventsConfig &events() {return *m_eventsCfg;}
    Q_SIGNALS:
        void eventNumberChanged(int num);
    protected:
        void chCfgSave(QJsonObject &cfgObj) const override;
        void chCfgLoad(const QJsonObject &cfgObj) override;
    private:
        ViEventsConfig *m_eventsCfg;
        int m_event_num;
        ViLogicScheme *m_logicScheme;

    };
}

#endif // LQMEAS_VIEVENTCONFIG_H
