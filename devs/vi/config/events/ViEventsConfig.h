#ifndef LQMEAS_VIEVENTSCONFIG_H
#define LQMEAS_VIEVENTSCONFIG_H

#include "lqmeas/devs/vi/config/ViConfigItem.h"
#include <QList>

namespace LQMeas {
    class ViConfig;
    class ViEventConfig;

    class ViEventsConfig : public ViConfigItem {
        Q_OBJECT
    public:
        explicit ViEventsConfig(ViConfig &viConfig);
        explicit ViEventsConfig(ViConfig &viConfig, const ViEventsConfig &origEvtCfg);

        ViConfig &viConfig() const {return *m_viCfg;}

        const QList<ViEventConfig *> &eventList() const {return m_events;}

        void removeAllEvents();
        void removeEvent(ViEventConfig &event);
        ViEventConfig *addEvent();

        ViEventConfig *eventCfg(int evtNum) const;
        void cleanup();
    Q_SIGNALS:
        void eventAppend(LQMeas::ViEventConfig *event);
        void eventRemoved(LQMeas::ViEventConfig *event);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        void removeEventAt(int rem_idx);
        void addEvent(ViEventConfig &event);

        ViConfig *m_viCfg;
        QList<ViEventConfig *> m_events;
    };
}

#endif // LQMEAS_VIEVENTSCONFIG_H
