#ifndef VIEXCHCONFIG_H
#define VIEXCHCONFIG_H

#include "lqmeas/devs/vi/config/ViConfigItem.h"

namespace LQMeas {
class ViConfig;
    class ViExchConfig  : public ViConfigItem {
        Q_OBJECT
    public:
        explicit ViExchConfig(ViConfig &viConfig);
        explicit ViExchConfig(ViConfig &viConfig, const ViExchConfig &origExchCfg);

        ViConfig &viConfig() const {return *m_viCfg;}

        bool pubIsEnabled() const {return m_pub_en;}
        void setPubIsEnabled(bool en);
    Q_SIGNALS:
        void pubEnableStateChanged();
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        ViConfig *m_viCfg;
        bool m_pub_en {false};
    };
};

#endif // VIEXCHCONFIG_H
