#include "ViExchConfig.h"
#include "../ViConfig.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_pub_obj  {"pub"};
    static const QLatin1String &cfgkey_pub_en  {StdConfigKeys::enabled()};

    ViExchConfig::ViExchConfig(ViConfig &viConfig) : ViConfigItem{&viConfig}, m_viCfg{&viConfig} {

    }

    ViExchConfig::ViExchConfig(ViConfig &viConfig, const ViExchConfig &origDinCfg)
        : ViConfigItem{&viConfig}, m_viCfg{&viConfig},
        m_pub_en{origDinCfg.m_pub_en} {


    }

    void ViExchConfig::setPubIsEnabled(bool en) {
        if (m_pub_en != en) {
            m_pub_en = en;
            Q_EMIT pubEnableStateChanged();
            notifyConfigChanged();
        }
    }

    void ViExchConfig::protSave(QJsonObject &cfgObj) const {
        {
            QJsonObject pubObj;
            pubObj[cfgkey_pub_en] = m_pub_en;
            cfgObj[cfgkey_pub_obj] = pubObj;
        }
    }

    void ViExchConfig::protLoad(const QJsonObject &cfgObj) {
        {
            const QJsonObject &pubObj {cfgObj[cfgkey_pub_obj].toObject()};
            m_pub_en = pubObj[cfgkey_pub_en].toBool();
        }
    }


}


