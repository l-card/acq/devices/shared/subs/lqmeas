#include "ViConfigOptionalVal.h"
#include  "lqmeas/lqtdefs.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String &cfgkey_en           {StdConfigKeys::enabled()};
    static const QLatin1String &cfgkey_value        {StdConfigKeys::value()};

    ViConfigOptionalVal::ViConfigOptionalVal(bool def_en, double def_val, QObject *parent) :
        m_def_enabled{def_en}, m_def_value{def_val},
        m_enabled{m_def_enabled}, m_value{m_def_value} {

    }

    ViConfigOptionalVal::ViConfigOptionalVal(const ViConfigOptionalVal &origVal, QObject *parent) :\
        ViConfigItem{parent},
        m_def_enabled{origVal.m_def_enabled},
        m_def_value{origVal.m_def_value},
        m_enabled{origVal.m_enabled},
        m_value{origVal.m_value}  {

    }

    void ViConfigOptionalVal::setEnabled(bool en) {
        if (m_enabled != en) {
            m_enabled = en;
            notifyConfigChanged();
        }
    }

    void ViConfigOptionalVal::setValue(double val) {
        if (!LQREAL_IS_EQUAL(m_value, val)) {
            m_value = val;
            notifyConfigChanged();
        }
    }

    void ViConfigOptionalVal::protSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_en] = m_enabled;
        cfgObj[cfgkey_value] = m_value;

    }

    void ViConfigOptionalVal::protLoad(const QJsonObject &cfgObj) {
        m_enabled = cfgObj[cfgkey_en].toBool(m_def_enabled);
        m_value   = cfgObj[cfgkey_value].toDouble(m_def_value);
    }
}

