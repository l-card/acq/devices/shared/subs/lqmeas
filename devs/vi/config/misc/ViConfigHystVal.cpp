#include "ViConfigHystVal.h"
#include  "lqmeas/lqtdefs.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String &cfgkey_value        {StdConfigKeys::value()};
    static const QLatin1String cfgkey_hyst          {"HystValue"};

    ViConfigHystVal::ViConfigHystVal(QObject *parent) : ViConfigItem{parent} {

    }

    ViConfigHystVal::ViConfigHystVal(const ViConfigHystVal &origVal, QObject *parent) :\
        ViConfigItem{parent},
        m_value{origVal.m_value},
        m_hyst{origVal.m_hyst} {

    }

    void ViConfigHystVal::setValue(double val) {
        if (!LQREAL_IS_EQUAL(m_value, val)) {
            m_value = val;
            notifyConfigChanged();
        }
    }

    void LQMeas::ViConfigHystVal::setHysteresis(double hyst) {
        if (!LQREAL_IS_EQUAL(m_hyst, hyst)) {
            m_hyst = hyst;
            notifyConfigChanged();
        }
    }

    void ViConfigHystVal::protSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_value] = m_value;
        cfgObj[cfgkey_hyst] = m_hyst;
    }

    void ViConfigHystVal::protLoad(const QJsonObject &cfgObj) {
        m_value = cfgObj[cfgkey_value].toDouble();
        m_hyst = cfgObj[cfgkey_hyst].toDouble();
    }
}

