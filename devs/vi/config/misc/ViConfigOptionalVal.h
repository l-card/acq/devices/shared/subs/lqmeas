#ifndef LQMEAS_VICONFIGOPTIONALVAL_H
#define LQMEAS_VICONFIGOPTIONALVAL_H

#include "../ViConfigItem.h"

namespace LQMeas {
    class ViConfigOptionalVal : public ViConfigItem {
        Q_OBJECT
    public:
        explicit ViConfigOptionalVal(bool def_en, double def_val, QObject *parent = nullptr);
        ViConfigOptionalVal(const ViConfigOptionalVal &origVal, QObject *parent = nullptr);

        double isEnabled() const {return m_enabled;}
        double value() const {return m_value;}

        void setEnabled(bool en);
        void setValue(double val);

        void setDefaultEnabled(bool en) {m_def_enabled = en;}
        void setDefaultValue(double val) {m_def_value = val;}
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        double m_def_value {0};
        bool   m_def_enabled {false};
        double m_value {0};
        bool m_enabled {false};
    };
}

#endif // LQMEAS_VICONFIGOPTIONALVAL_H
