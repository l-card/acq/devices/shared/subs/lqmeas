#ifndef LQMEAS_VICONFIGHYSTVAL_H
#define LQMEAS_VICONFIGHYSTVAL_H

#include "../ViConfigItem.h"

namespace LQMeas {
    class ViConfigHystVal : public ViConfigItem {
        Q_OBJECT
    public:
        explicit ViConfigHystVal(QObject *parent = nullptr);
        ViConfigHystVal(const ViConfigHystVal &origVal, QObject *parent = nullptr);

        double value() const {return m_value;}
        double hysteresis() const {return m_hyst;}

        void setValue(double val);
        void setHysteresis(double val);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        double m_value {0};
        double m_hyst {0};
    };
}

#endif // LQMEAS_VICONFIGHYSTVAL_H
