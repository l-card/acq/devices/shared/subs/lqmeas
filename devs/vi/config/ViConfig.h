#ifndef VICONFIG_H
#define VICONFIG_H


#include "lqmeas/devs/DeviceConfigIface.h"
#include <memory>
namespace LQMeas {
    class ViConfigRefCtl;
    class ViAcqConfig;
    class ViEventsConfig;
    class ViDoutConfig;
    class ViDinConfig;
    class ViWaveformsConfig;
    class ViExchConfig;
    class ViModuleTypeInfo;

    class ViConfig : public DeviceConfigIface {
    public:
        Q_OBJECT
    public:
        explicit ViConfig(QObject *parent = nullptr);
        ViConfig(const ViConfig &cfg);
        ~ViConfig() override;

        const ViModuleTypeInfo &viModuleTypeInfo() const {return *m_viModuleTypeInfo;}
        bool viModuleTypeInfoValid() const {return m_viModuleTypeInfo != nullptr;}

        const ViConfigRefCtl &refs() const {return *m_refCfl;}
        ViConfigRefCtl &refs() {return *m_refCfl;}

        const ViAcqConfig &acq() const {return *m_acq;}
        ViAcqConfig &acq() {return *m_acq;}

        const ViEventsConfig &events() const {return *m_events;}
        ViEventsConfig &events() {return *m_events;}

        const ViDoutConfig &dout() const {return *m_dout;}
        ViDoutConfig &dout() {return *m_dout;}

        const ViDinConfig &din() const {return *m_din;}
        ViDinConfig &din() {return *m_din;}

        const ViWaveformsConfig &wf() const {return *m_wf;}
        ViWaveformsConfig &wf() {return *m_wf;}

        const ViExchConfig &exch() const {return *m_exch;}
        ViExchConfig &exch() {return *m_exch;}

        const QString &customDevName() const {return m_custom_devname;}
        void setCustomDevName(const QString &name);
    Q_SIGNALS:
        void devnameChanged(const QString &name);
    protected:
        QString cfgIfaceName() const override {return  QStringLiteral("Vi");}

        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
        void protUpdateInfo(const DeviceTypeInfo *info) override;

    private:
        const ViModuleTypeInfo *m_viModuleTypeInfo {nullptr};

        ViConfigRefCtl *m_refCfl;
        ViExchConfig *m_exch;
        ViAcqConfig *m_acq;
        ViEventsConfig *m_events;
        ViDoutConfig *m_dout;
        ViDinConfig *m_din;
        ViWaveformsConfig *m_wf;        
        QString m_custom_devname;

    };
}

#endif // VICONFIG_H
