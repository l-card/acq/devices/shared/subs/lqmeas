#ifndef LQMEAS_VIWAVEFORMSCONFIG_H
#define LQMEAS_VIWAVEFORMSCONFIG_H


#include "lqmeas/devs/vi/config/ViConfigItem.h"
#include <QList>

namespace LQMeas {
    class ViConfig;
    class ViWaveformSrcConfig;

    class ViWaveformsConfig: public ViConfigItem {
        Q_OBJECT
    public:
        explicit ViWaveformsConfig(ViConfig &viConfig);
        explicit ViWaveformsConfig(ViConfig &viConfig, const ViWaveformsConfig &origWfsCfg);

        ViConfig &viConfig() const {return *m_viCfg;}

        const QList<ViWaveformSrcConfig *> &wfSrcList() const {return m_wfSrcList;}

        void removeAllWfSrc();
        void removeWfSrc(ViWaveformSrcConfig &wfSrcCfg);
        ViWaveformSrcConfig *addWfSrc();

        ViWaveformSrcConfig *wfSrcCfg(int srcNum) const;
        void cleanup();
    Q_SIGNALS:
        void wfSrcAppend(LQMeas::ViWaveformSrcConfig *wfSrcCfg);
        void wfSrcRemoved(LQMeas::ViWaveformSrcConfig *wfSrcCfg);
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
    private:
        void removeWfSrcAt(int rem_idx);
        void addWfSrc(ViWaveformSrcConfig &wfSrcCfg);
    private:
        ViConfig *m_viCfg;
        QList<ViWaveformSrcConfig *> m_wfSrcList;
    };
}

#endif // LQMEAS_VIWAVEFORMSCONFIG_H
