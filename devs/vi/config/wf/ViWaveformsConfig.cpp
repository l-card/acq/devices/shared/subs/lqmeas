#include "ViWaveformsConfig.h"
#include "../ViConfig.h"
#include "ViWaveformSrcConfig.h"
#include <QJsonArray>
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_wfsrc_arr   {"WfSrc"};

    ViWaveformsConfig::ViWaveformsConfig(ViConfig &viConfig) : ViConfigItem{&viConfig}, m_viCfg{&viConfig} {


    }

    ViWaveformsConfig::ViWaveformsConfig(ViConfig &viConfig, const ViWaveformsConfig &origWfsCfg)
        : ViConfigItem{&viConfig}, m_viCfg{&viConfig} {

        for (ViWaveformSrcConfig *wfSrcCfg : origWfsCfg.m_wfSrcList) {
            addWfSrc(*new ViWaveformSrcConfig{*this, *wfSrcCfg});
        }

    }

    void ViWaveformsConfig::removeAllWfSrc() {
        while (!m_wfSrcList.isEmpty()) {
            removeWfSrcAt(m_wfSrcList.size()-1);
        }
    }

    void ViWaveformsConfig::removeWfSrc(ViWaveformSrcConfig &wfSrcCfg) {
        int wf_src_idx {m_wfSrcList.indexOf(&wfSrcCfg)};
        if (wf_src_idx >= 0) {
            removeWfSrcAt(wf_src_idx);
            notifyConfigChanged();
        }
    }

    ViWaveformSrcConfig *ViWaveformsConfig::addWfSrc() {
        ViWaveformSrcConfig *wfSrc {new ViWaveformSrcConfig{*this, m_wfSrcList.size()}};
        addWfSrc(*wfSrc);
        notifyConfigChanged();
        return wfSrc;
    }

    ViWaveformSrcConfig *ViWaveformsConfig::wfSrcCfg(int srcNum) const {
        const auto &it {std::find_if(m_wfSrcList.cbegin(), m_wfSrcList.cend(),
                                    [srcNum](ViWaveformSrcConfig *wfSrcCfg){return wfSrcCfg->sourceNumber() == srcNum;})};
        return it != m_wfSrcList.cend() ? *it : nullptr;
    }

    void ViWaveformsConfig::cleanup()   {
        qDeleteAll(m_wfSrcList);
        m_wfSrcList.clear();
    }

    void ViWaveformsConfig::protSave(QJsonObject &cfgObj) const {
        QJsonArray wfSrcArray;
        for (const ViWaveformSrcConfig *wfSrcCfg : m_wfSrcList) {
            QJsonObject wfSrcObj;
            wfSrcCfg->save(wfSrcObj);
            wfSrcArray.append(wfSrcObj);
        }
        cfgObj[cfgkey_wfsrc_arr] = wfSrcArray;
    }

    void ViWaveformsConfig::protLoad(const QJsonObject &cfgObj) {
        removeAllWfSrc();

        const QJsonArray &wfSrcArray {cfgObj[cfgkey_wfsrc_arr].toArray()};
        for (const auto &it : wfSrcArray) {
            const QJsonObject &wfSrcObj {it.toObject()};
            ViWaveformSrcConfig *wfSrcCfg {new ViWaveformSrcConfig{*this, m_wfSrcList.size()}};
            wfSrcCfg->load(wfSrcObj);
            addWfSrc(*wfSrcCfg);
        }
    }

    void ViWaveformsConfig::removeWfSrcAt(int rem_idx) {
        ViWaveformSrcConfig *wfSrcCfg {m_wfSrcList.takeAt(rem_idx)};
        remSubItem(wfSrcCfg);
        Q_EMIT wfSrcRemoved(wfSrcCfg);
        wfSrcCfg->deleteLater();
        for (int idx {rem_idx}; idx < m_wfSrcList.size(); ++idx) {
            m_wfSrcList[idx]->setSourceNumber(idx);
        }
    }

    void ViWaveformsConfig::addWfSrc(ViWaveformSrcConfig &wfSrcCfg) {
        m_wfSrcList += &wfSrcCfg;
        addSubItem(&wfSrcCfg);
        Q_EMIT wfSrcAppend(&wfSrcCfg);
    }
}
