#ifndef LQMEAS_VIWAVEFORMSRCCONFIG_H
#define LQMEAS_VIWAVEFORMSRCCONFIG_H


#include "lqmeas/devs/vi/config/ViChannelConfig.h"
#include <memory>

namespace LQMeas {
    class ViWaveformsConfig;
    class ViConfigRefEvent;

    class ViWaveformSrcConfig : public ViChannelConfig {
        Q_OBJECT
    public:
        explicit ViWaveformSrcConfig(ViWaveformsConfig &wfs, int wf_src_num);
        explicit ViWaveformSrcConfig(ViWaveformsConfig &wfs, const ViWaveformSrcConfig &origWfSrcCfg);
        ~ViWaveformSrcConfig() override;

        QString origName() const override;
        static QString chOrigName(int wf_src_num);

        int sourceNumber() const {return m_src_num;}
        void setSourceNumber(int num);
        int channelNumber() const override {return sourceNumber();}


        const ViWaveformsConfig &wfs() const {return *m_wfsCfg;}
        ViWaveformsConfig &wfs() {return *m_wfsCfg;}

        double durationMs() const {return m_duration_ms;}
        void setDurationMs(double ms);

        double delayMs() const {return m_delay_ms;}
        void setDelayMs(double ms);
        
        uint32_t acqChannelMask() const {return m_chs_mask;}
        void setAcqChannelMask(uint32_t mask);


        const ViConfigRefEvent &eventRef() const {return *m_evtRef;}
        ViConfigRefEvent &eventRef() {return *m_evtRef;}


        static double defaultDurationMs()  {return 5000;}
        static double defaultDelayMs()  {return 0;}
    Q_SIGNALS:
        void sourceNumberChanged(int num);
    protected:
        void chCfgSave(QJsonObject &cfgObj) const override;
        void chCfgLoad(const QJsonObject &cfgObj) override;
    private:
        ViWaveformsConfig *m_wfsCfg;
        int m_src_num;

        uint32_t m_chs_mask {0};
        double m_duration_ms {defaultDurationMs()};
        double m_delay_ms {defaultDelayMs()};

        std::unique_ptr<ViConfigRefEvent> m_evtRef;
    };
}

#endif // LQMEAS_VIWAVEFORMSRCCONFIG_H
