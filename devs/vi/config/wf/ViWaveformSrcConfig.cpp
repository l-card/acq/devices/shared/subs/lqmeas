#include "ViWaveformSrcConfig.h"
#include "ViWaveformsConfig.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefEvent.h"
#include "lqmeas/devs/vi/config/ViConfig.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefCtl.h"
#include "lqmeas/StdConfigKeys.h"
#include <QStringBuilder>
#include <QJsonObject>


namespace LQMeas {
    static const QLatin1String &cfgkey_wfsrc_duration        {StdConfigKeys::duration()};
    static const QLatin1String &cfgkey_wfsrc_delay           {StdConfigKeys::delay()};
    static const QLatin1String &cfgkey_wfsrc_event           {StdConfigKeys::event()};
    static const QLatin1String cfgkey_wfsrc_ch_msk           {"ChsMask"};

    ViWaveformSrcConfig::ViWaveformSrcConfig(ViWaveformsConfig &wfs, int wf_src_num) :
        ViChannelConfig{wfs.viConfig(), &wfs, true},
        m_wfsCfg{&wfs}, m_src_num{wf_src_num}, m_evtRef{new ViConfigRefEvent{}}  {


        addSubItem(m_evtRef.get());
        wfs.viConfig().refs().addRef(*m_evtRef);
    }

    ViWaveformSrcConfig::ViWaveformSrcConfig(ViWaveformsConfig &wfs, const ViWaveformSrcConfig &origWfSrcCfg) :
        ViChannelConfig{origWfSrcCfg, wfs.viConfig(), &wfs}
        , m_wfsCfg{&wfs}
        , m_src_num{origWfSrcCfg.m_src_num}
        , m_chs_mask{origWfSrcCfg.m_chs_mask}
        , m_duration_ms{origWfSrcCfg.m_duration_ms}
        , m_delay_ms{origWfSrcCfg.m_delay_ms}
        , m_evtRef{new ViConfigRefEvent{*origWfSrcCfg.m_evtRef}}  {

        addSubItem(m_evtRef.get());
        wfs.viConfig().refs().addRef(*m_evtRef);
    }

    ViWaveformSrcConfig::~ViWaveformSrcConfig() {
        m_wfsCfg->viConfig().refs().removeRef(*m_evtRef);
    }

    QString ViWaveformSrcConfig::origName() const {
        return chOrigName(m_src_num);
    }

    QString ViWaveformSrcConfig::chOrigName(int wf_src_num) {
        return tr("Waveform source") % QChar(' ') %  QString::number(wf_src_num + 1);
    }

    void ViWaveformSrcConfig::setSourceNumber(int num) {
        if (m_src_num != num) {
            m_src_num = num;
            Q_EMIT sourceNumberChanged(num);
        }
    }

    void ViWaveformSrcConfig::setDurationMs(double ms) {
        if (ms < 1)
            ms = 1;

        if (ms != m_duration_ms) {
            m_duration_ms = ms;
            notifyConfigChanged();
        }
    }

    void ViWaveformSrcConfig::setDelayMs(double ms) {
        if (ms < 0)
            ms = 0;
        if (ms > m_duration_ms)
            ms = m_duration_ms;

        if (ms != m_delay_ms) {
            m_delay_ms = ms;
            notifyConfigChanged();
        }
    }

    void ViWaveformSrcConfig::setAcqChannelMask(uint32_t mask) {
        if (mask != m_chs_mask) {
            m_chs_mask = mask;
            notifyConfigChanged();
        }
    }



    void ViWaveformSrcConfig::chCfgSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_wfsrc_duration] = m_duration_ms;
        cfgObj[cfgkey_wfsrc_delay] = m_delay_ms;
        cfgObj[cfgkey_wfsrc_ch_msk] = static_cast<int>(m_chs_mask);
        saveSubItem(cfgObj, *m_evtRef,  cfgkey_wfsrc_event);
    }

    void ViWaveformSrcConfig::chCfgLoad(const QJsonObject &cfgObj) {
        m_duration_ms = cfgObj[cfgkey_wfsrc_duration].toDouble(defaultDurationMs());
        m_delay_ms = cfgObj[cfgkey_wfsrc_delay].toDouble(defaultDelayMs());
        m_chs_mask = static_cast<quint32>(cfgObj[cfgkey_wfsrc_ch_msk].toInt());
        loadSubItem(cfgObj, *m_evtRef,  cfgkey_wfsrc_event);
    }
}
