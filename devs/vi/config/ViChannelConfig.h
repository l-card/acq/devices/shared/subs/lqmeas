#ifndef LQMEAS_VICHANNELCONFIG_H
#define LQMEAS_VICHANNELCONFIG_H

#include "lqmeas/devs/vi/config/ViConfigItem.h"
namespace LQMeas {
    class ViConfig;

    enum class ViChannelPubInfoType {
        State,
        PhaseMarks
    };

    class ViChannelConfig: public ViConfigItem {
        Q_OBJECT
    public:


        explicit ViChannelConfig(ViConfig &viConfig, QObject *parent, bool default_en);
        ViChannelConfig(const ViChannelConfig &other, ViConfig &viConfig, QObject *parent = nullptr);

        virtual QString displayName() const;
        virtual QString fullDisplayName() const;
        virtual QString origName() const = 0;
        virtual int channelNumber() const = 0;

        virtual bool isEnabled() const {return m_enabled;}
        virtual void setEnabled(bool en);

        virtual bool pubIsEnabled(ViChannelPubInfoType infoType) const;

        const QString &customName() const {return m_custom_name;}
        void setCustomName(const QString &name);

        const ViConfig &viConfig() const {return *m_viCfg;}
        ViConfig &viConfig() {return *m_viCfg;}
    Q_SIGNALS:
        void displayNameChanged();
        void pubEnableStateChanged();
    protected:
        void protSave(QJsonObject &cfgObj) const override final;
        void protLoad(const QJsonObject &cfgObj) override final;

        virtual void chCfgSave(QJsonObject &cfgObj) const = 0;
        virtual void chCfgLoad(const QJsonObject &cfgObj) = 0;
    private:
        bool m_def_enabled;
        bool m_enabled {false};
        QString m_custom_name;
        ViConfig *m_viCfg;
    };
}

#endif // LQMEAS_VICHANNELCONFIG_H
