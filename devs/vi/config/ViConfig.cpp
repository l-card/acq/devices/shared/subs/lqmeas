#include "ViConfig.h"
#include "lqmeas/StdConfigKeys.h"
#include "refs/ViConfigRefCtl.h"
#include "acq/ViAcqConfig.h"
#include "events/ViEventsConfig.h"
#include "dout/ViDoutConfig.h"
#include "din/ViDinConfig.h"
#include "wf/ViWaveformsConfig.h"
#include "exch/ViExchConfig.h"
#include "lqmeas/devs/vi/modules/ViModuleTypeInfo.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String &cfgkey_custom_devname   {StdConfigKeys::customName()};
    static const QLatin1String cfgkey_group_acq         {"Acq"};
    static const QLatin1String cfgkey_group_events      {"Events"};
    static const QLatin1String cfgkey_group_dout        {"Dout"};
    static const QLatin1String cfgkey_group_din         {"Din"};
    static const QLatin1String cfgkey_group_wf          {"Wf"};
    static const QLatin1String cfgkey_group_exch        {"Exch"};

    ViConfig::ViConfig(QObject *parent) :
        m_refCfl{new ViConfigRefCtl{*this}},
        m_exch{new ViExchConfig{*this}},
        m_acq{new ViAcqConfig{*this}},
        m_events{new ViEventsConfig{*this}},
        m_dout{new ViDoutConfig{*this}},
        m_din{new ViDinConfig{*this}},
        m_wf{new ViWaveformsConfig{*this}} {

        addSubItem(m_refCfl);
        addSubItem(m_acq);
        addSubItem(m_events);
        addSubItem(m_dout);
        addSubItem(m_din);
        addSubItem(m_wf);
        addSubItem(m_exch);

    }

    ViConfig::ViConfig(const ViConfig &cfg) :
        m_refCfl{new ViConfigRefCtl{*this, cfg.refs()}},
        m_exch{new ViExchConfig{*this, cfg.exch()}},
        m_acq{new ViAcqConfig{*this, cfg.acq()}},
        m_events{new ViEventsConfig{*this, cfg.events()}},
        m_dout{new ViDoutConfig{*this, cfg.dout()}},
        m_din{new ViDinConfig{*this, cfg.din()}},
        m_wf{new ViWaveformsConfig{*this, cfg.wf()}},
        m_custom_devname{cfg.m_custom_devname} {


        addSubItem(m_refCfl);
        addSubItem(m_acq);
        addSubItem(m_events);
        addSubItem(m_dout);
        addSubItem(m_din);
        addSubItem(m_wf);
        addSubItem(m_exch);
    }

    ViConfig::~ViConfig() {
        /* Удаление всех каналов, где могут быть ссылки,
         * должно идти до удаления объекта контроля ссылок */
        m_dout->cleanup();
        m_wf->cleanup();
        m_events->cleanup();
        m_acq->cleanup();

    }
    
    void ViConfig::setCustomDevName(const QString &name) {
        if (name != m_custom_devname) {
            m_custom_devname = name;
            notifyConfigChanged();
            Q_EMIT devnameChanged(m_custom_devname);
        }
    }



    void ViConfig::protSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_custom_devname] = m_custom_devname;
        saveSubItem(cfgObj, *m_acq, cfgkey_group_acq);
        saveSubItem(cfgObj, *m_events, cfgkey_group_events);
        saveSubItem(cfgObj, *m_dout, cfgkey_group_dout);
        saveSubItem(cfgObj, *m_din, cfgkey_group_din);
        saveSubItem(cfgObj, *m_wf, cfgkey_group_wf);
        saveSubItem(cfgObj, *m_exch, cfgkey_group_exch);
    }

    void ViConfig::protLoad(const QJsonObject &cfgObj) {
        m_custom_devname = cfgObj[cfgkey_custom_devname].toString();
        loadSubItem(cfgObj, *m_acq, cfgkey_group_acq);
        loadSubItem(cfgObj, *m_events, cfgkey_group_events);
        loadSubItem(cfgObj, *m_dout, cfgkey_group_dout);
        loadSubItem(cfgObj, *m_din, cfgkey_group_din);
        loadSubItem(cfgObj, *m_wf, cfgkey_group_wf);
        loadSubItem(cfgObj, *m_exch, cfgkey_group_exch);
    }

    void ViConfig::protUpdateInfo(const DeviceTypeInfo *info) {
        if (info) {
            const ViModuleTypeInfo &viInfo {static_cast<const ViModuleTypeInfo &>(*info)};
            m_viModuleTypeInfo = &viInfo;
        } else {
            m_viModuleTypeInfo = nullptr;
        }
        DeviceConfigIface::protUpdateInfo(info);
    }
}

