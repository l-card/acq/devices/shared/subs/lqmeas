#ifndef LQMEAS_VIMODULECONFIG_H
#define LQMEAS_VIMODULECONFIG_H

#include "lqmeas/devs/DeviceConfig.h"
#include <memory>

namespace LQMeas {
    class ViConfig;

    class ViModuleConfig  : public DeviceConfig {
        Q_OBJECT
    public:
        static const QString &typeConfigName();

        ViModuleConfig();
        ViModuleConfig(const ViModuleConfig &cfg);
        ~ViModuleConfig() override;

        const ViConfig *viConfig() const override {return m_vi.get();}

        const ViConfig &vi() const {return *m_vi;}
        ViConfig &vi() {return *m_vi;}

        QString configDevTypeName() const override {return typeConfigName();}
        DeviceConfig *clone() const override {return new ViModuleConfig{*this};}
        DeviceConfig *clone(const Device *device) const {
            DeviceConfig *ret {clone()};
            ret->updateDevice(device);
            return ret;
        }
    private:
        const std::unique_ptr<ViConfig> m_vi;
    };
}

#endif // LQMEAS_VIMODULECONFIG_H
