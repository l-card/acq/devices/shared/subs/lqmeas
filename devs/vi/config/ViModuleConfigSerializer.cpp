#include "ViModuleConfigSerializer.h"
#include "lqmeas/devs/vi/ViErrors.h"
#include "lqmeas/units/Unit.h"
#include "lqmeas/devs/vi/config/ViModuleConfig.h"
#include "lqmeas/devs/vi/config/ViConfig.h"
#include "lqmeas/devs/vi/config/acq/ViAcqConfig.h"
#include "lqmeas/devs/vi/config/acq/channel/ViAcqChannelConfig.h"
#include "lqmeas/devs/vi/config/acq/channel/phasetrig/ViAcqChannelPhaseTrigConfig.h"
#include "lqmeas/devs/vi/config/acq/channel/phasetrig/ViAcqChannelPhaseTrigPolarity.h"
#include "lqmeas/devs/vi/config/acq/channel/position/ViAcqChannelPositionConfig.h"
#include "lqmeas/devs/vi/config/acq/channel/position/ViAcqChannelPositionNormalDirection.h"
#include "lqmeas/devs/vi/config/acq/channel/types/ViAcqChannelTypeUnknown.h"
#include "lqmeas/devs/vi/config/acq/channel/types/ViAcqChannelTypeVibroAccel.h"
#include "lqmeas/devs/vi/config/acq/channel/types/ViAcqChannelTypeRadialVibration.h"
#include "lqmeas/devs/vi/config/acq/channel/types/ViAcqChannelTypePhaseTrigger.h"
#include "lqmeas/devs/vi/config/acq/channel/types/ViAcqChannelTypeAxialPosition.h"
#include "lqmeas/devs/vi/config/acq/sensor/ViAcqSensorConfig.h"
#include "lqmeas/devs/vi/config/acq/sensor/ViAcqSensorType.h"
#include "lqmeas/devs/vi/config/acq/sensor/ViAcqSensorConType.h"
#include "lqmeas/devs/vi/config/acq/sensor/types/accelerometer/ViAcqSensorTypeAccelerometer.h"
#include "lqmeas/devs/vi/config/acq/sensor/types/proximeter/ViAcqSensorTypeProximeter.h"
#include "lqmeas/devs/vi/config/acq/sensor/types/unknown/ViAcqSensorTypeUnknown.h"
#include "lqmeas/devs/vi/config/acq/meas/ViMeasConfig.h"
#include "lqmeas/devs/vi/config/acq/meas/ViMeasParameterType.h"
#include "lqmeas/devs/vi/config/acq/meas/ViMeasFilterConfig.h"
#include "lqmeas/devs/vi/config/acq/meas/ViMeasGroupConfig.h"
#include "lqmeas/devs/vi/config/acq/meas/ViMeasGroupType.h"
#include "lqmeas/devs/vi/config/acq/meas/ViMeasCondType.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeUnknown.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeVibroAccel.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeVibroAccelHf.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeVibroVelocity.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeVibroDisp.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeRotSpeed.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeVoltageGap.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeVoltageDC.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypePosition.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeVibroSmax.h"
#include "lqmeas/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpAccel.h"
#include "lqmeas/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaAccel.h"
#include "lqmeas/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpVelocity.h"
#include "lqmeas/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaVelocity.h"
#include "lqmeas/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmAmpDisp.h"
#include "lqmeas/devs/vi/config/acq/meas/types/Harmonics/ViMeasTypeHarmPhaDisp.h"
#include "lqmeas/devs/vi/config/acq/setpoint/ViSetpointConfig.h"
#include "lqmeas/devs/vi/config/acq/setpoint/ViSetpointConditionType.h"
#include "lqmeas/devs/vi/config/events/ViEventsConfig.h"
#include "lqmeas/devs/vi/config/events/ViEventConfig.h"
#include "lqmeas/devs/vi/config/events/logic/ViLogicScheme.h"
#include "lqmeas/devs/vi/config/events/logic/ViLogicElementConfig.h"
#include "lqmeas/devs/vi/config/events/logic/types/ViLogicElementConfigMeasCond.h"
#include "lqmeas/devs/vi/config/events/logic/types/ViLogicElementConfigDin.h"
#include "lqmeas/devs/vi/config/events/logic/types/ViLogicElementConfigEvent.h"
#include "lqmeas/devs/vi/config/events/logic/types/ViLogicElementConfigAnd.h"
#include "lqmeas/devs/vi/config/events/logic/types/ViLogicElementConfigOr.h"
#include "lqmeas/devs/vi/config/events/logic/types/ViLogicElementConfigNot.h"
#include "lqmeas/devs/vi/config/din/ViDinConfig.h"
#include "lqmeas/devs/vi/config/din/ViDinChannelConfig.h"
#include "lqmeas/devs/vi/config/dout/ViDoutConfig.h"
#include "lqmeas/devs/vi/config/dout/ViDoutChannelType.h"
#include "lqmeas/devs/vi/config/dout/ViDoutChannelConfig.h"
#include "lqmeas/devs/vi/config/dout/ViDoutNormalOpState.h"
#include "lqmeas/devs/vi/config/wf/ViWaveformSrcConfig.h"
#include "lqmeas/devs/vi/config/wf/ViWaveformsConfig.h"
#include "lqmeas/devs/vi/config/exch/ViExchConfig.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefEvent.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefDin.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefAcqCh.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefMeas.h"
#include <QByteArray>

namespace LQMeas {
    void ViModuleMbConfigSerializer::parse(const QByteArray &data, ViModuleConfig &cfg, LQError &err) {
        bool finish_cfg {false};
        const uint8_t *parse_ptr {reinterpret_cast<const uint8_t *>(data.data())};
        const uint8_t *end_ptr = parse_ptr + data.size();
        QList<QString> remoteDevIds;
        while (err.isSuccess() && (parse_ptr < (end_ptr - sizeof(t_vi_opcfg_block_hdr))) && !finish_cfg) {

            const t_vi_opcfg_block *block {reinterpret_cast<const t_vi_opcfg_block *>(parse_ptr)};
            quint32 update_size = ((sizeof(t_vi_opcfg_block_hdr) + block->hdr.size + 3)/4) * 4;
            if ((end_ptr - parse_ptr) >= update_size) {
                if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_DEVREF) {
                    pareseDevRef(block->devref, block->hdr.size, remoteDevIds, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_DEVNAME) {
                    parseDevName(cfg, block->raw_text, block->hdr.size, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_ACQCH_GEN) {
                    parseAcqChGen(cfg, block->acqch_gen, block->hdr.size, remoteDevIds, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_ACQCH_NAME) {
                    parseAcqChName(cfg, block->acqch_name, block->hdr.size, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_ACQCH_SENSOR) {
                    parseAcqChSensor(cfg, block->acqch_sensor, block->hdr.size, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_ACQCH_PHASETRIG) {
                    parseAcqChPhaseTrig(cfg, block->acqch_phasetrig, block->hdr.size, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_ACQCH_POSITION) {
                    parseAcqChPosition(cfg, block->acqch_position, block->hdr.size, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_MEAS_GEN) {
                    parseMeasGen(cfg, block->meas_gen, block->hdr.size, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_MEAS_NAME) {
                    parseMeasName(cfg, block->meas_name, block->hdr.size, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_MEAS_SP) {
                    parseMeasSetpoint(cfg, block->meas_setpoint, block->hdr.size, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_EVENT_GEN) {
                    parseEventGen(cfg, block->event_gen, block->hdr.size, remoteDevIds, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_EVENT_NAME) {
                    parseEventName(cfg, block->event_name, block->hdr.size, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_DIN_GEN) {
                    parseDinGen(cfg, block->din_gen, block->hdr.size, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_DIN_NAME) {
                    parseDinName(cfg, block->din_name, block->hdr.size, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_DOUT_GEN) {
                    parseDoutGen(cfg, block->dout_gen, block->hdr.size, remoteDevIds, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_DOUT_NAME) {
                    parseDoutName(cfg, block->dout_name, block->hdr.size, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_WF_GEN) {
                    parseWfGen(cfg, block->wf_gen, block->hdr.size, remoteDevIds, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_WF_NAME) {
                    parseWfName(cfg, block->wf_name, block->hdr.size, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_EXCH_GEN) {
                    parseExchGen(cfg, block->exch_gen, block->hdr.size, err);
                } else if (block->hdr.type == VI_OPCFG_BLOCK_TYPE_FINISH) {
                    finish_cfg = true;
                }
            }
            parse_ptr += update_size;
        }

    }

    void ViModuleMbConfigSerializer::serialize(const ViModuleConfig &cfg, QByteArray &resArray, LQError &err) {
        QList<QString> remoteDevIds;

        serializeDevName(resArray, cfg);
        serializeExchGen(resArray, cfg.vi().exch());

        const ViAcqConfig &acqCfg {cfg.vi().acq()};
        for (int ch_idx {0}; (ch_idx < acqCfg.availableChannelsCnt()) && err.isSuccess(); ++ch_idx) {
            const ViAcqChannelConfig &acqChCfg {acqCfg.channel(ch_idx)};
            serializeAcqChGen(resArray, acqChCfg, remoteDevIds);
            serializeAcqChName(resArray, acqChCfg);
            serializeAcqChSensor(resArray, acqChCfg);
            serializeAcqChPhaseTrig(resArray, acqChCfg);
            serializeAcqChPosition(resArray, acqChCfg);
            if (acqChCfg.isEnabled()) {
                const QList<ViMeasConfig *> &measList {acqChCfg.measList()};
                for (ViMeasConfig *measCfg : measList) {
                    serializeMeasGen(resArray, *measCfg);
                    serializeMeasName(resArray, *measCfg);
                    serializeMeasSetpoint(resArray, measCfg->setpointAlert());
                    serializeMeasSetpoint(resArray, measCfg->setpointDanger());
                }
            }
        }

        const QList<ViEventConfig *> events {cfg.vi().events().eventList()};
        for (const ViEventConfig *eventCfg : events) {
            serializeEventGen(resArray, *eventCfg, remoteDevIds);
            serializeEventName(resArray, *eventCfg);
        }

        const ViDinConfig &dinCfg {cfg.vi().din()};
        for (int ch_idx {0}; (ch_idx < dinCfg.availableChannelsCnt()) && err.isSuccess(); ++ch_idx) {
            const ViDinChannelConfig &dinChCfg {dinCfg.channel(ch_idx)};
            serializeDinGen(resArray, dinChCfg);
            serializeDinName(resArray, dinChCfg);
        }

        const ViDoutConfig &doutCfg {cfg.vi().dout()};
        const QList<const LQMeas::ViDoutChannelType *> outTypes {ViDoutChannelTypes::all()};
        for (const LQMeas::ViDoutChannelType *outType : outTypes) {
            for (int ch_idx {0}; (ch_idx < doutCfg.availableChannelsCnt(*outType)) && err.isSuccess(); ++ch_idx) {
                const ViDoutChannelConfig &doutChCfg {doutCfg.channel(*outType, ch_idx)};
                serializeDoutGen(resArray, doutChCfg, remoteDevIds);
                serializeDoutName(resArray, doutChCfg);
            }
        }

        const ViWaveformsConfig &wfCfg {cfg.vi().wf()};
        const QList<ViWaveformSrcConfig *> &wfs {wfCfg.wfSrcList()};
        for (ViWaveformSrcConfig *wfSrcCfg : wfs) {
            serializeWfGen(resArray, *wfSrcCfg, remoteDevIds);
            serializeWfName(resArray, *wfSrcCfg);
        }

        if (!remoteDevIds.isEmpty()) {
            QByteArray refidArray;
            for (int devidx {0}; devidx < remoteDevIds.size(); ++devidx) {
                serializeDevRef(refidArray, devidx, remoteDevIds.at(devidx));
            }
            resArray.prepend(refidArray);
        }

        addBlock(resArray, VI_OPCFG_BLOCK_TYPE_FINISH, QByteArray{});
    }

    const ViAcqChannelType &ViModuleMbConfigSerializer::getAcqChType(quint16 type_code) {
        const ViAcqChannelType *acq_type {nullptr};
        if (type_code == VI_ACQCH_TYPE_VIBRO_ACCEL) {
            acq_type = &ViAcqChannelTypeVibroAccel::type();
        } else if (type_code == VI_ACQCH_TYPE_RADIAL_VIBRATION) {
            acq_type = &ViAcqChannelTypeRadialVibration::type();
        } else if (type_code == VI_ACQCH_TYPE_PHASE_TRIGGER) {
            acq_type = &ViAcqChannelTypePhaseTrigger::type();
        } else if (type_code == VI_ACQCH_TYPE_AXIAL_POSITION) {
            acq_type = &ViAcqChannelTypeAxialPosition::type();
        } else {
            acq_type = &ViAcqChannelTypeUnknown::type();
        }
        return *acq_type;
    }

    const quint16 ViModuleMbConfigSerializer::getAcqChTypeCode(const ViAcqChannelType &acq_ch_type) {
        quint16 acq_ch_type_code;
        if (&acq_ch_type == &ViAcqChannelTypeVibroAccel::type()) {
            acq_ch_type_code = VI_ACQCH_TYPE_VIBRO_ACCEL;
        } else if (&acq_ch_type == &ViAcqChannelTypeRadialVibration::type()) {
            acq_ch_type_code = VI_ACQCH_TYPE_RADIAL_VIBRATION;
        } else if (&acq_ch_type == &ViAcqChannelTypePhaseTrigger::type()) {
            acq_ch_type_code = VI_ACQCH_TYPE_PHASE_TRIGGER;
        } else if (&acq_ch_type == &ViAcqChannelTypeAxialPosition::type()) {
            acq_ch_type_code = VI_ACQCH_TYPE_AXIAL_POSITION;
        } else {
            acq_ch_type_code = VI_ACQCH_TYPE_NONE;
        }
        return acq_ch_type_code;
    }

    const ViMeasType &ViModuleMbConfigSerializer::getMeasType(quint16 type_code) {
        const ViMeasType *meas_type {nullptr};
        if (type_code == VI_MEAS_TYPE_ACCEL) {
            meas_type = &ViMeasTypeVibroAccel::type();
        } else if (type_code == VI_MEAS_TYPE_ACCEL_HF) {
            meas_type = &ViMeasTypeVibroAccelHf::type();
        } else if (type_code == VI_MEAS_TYPE_VELOCITY) {
            meas_type = &ViMeasTypeVibroVelocity::type();
        } else if (type_code == VI_MEAS_TYPE_DISPL) {
            meas_type = &ViMeasTypeVibroDisp::type();
        } else if (type_code == VI_MEAS_TYPE_GAP) {
            meas_type = &ViMeasTypeVoltageGap::type();
        } else if (type_code == VI_MEAS_TYPE_DC) {
            meas_type = &ViMeasTypeVoltageDC::type();
        } else if (type_code == VI_MEAS_TYPE_POSITION) {
            meas_type = &ViMeasTypePosition::type();
        } else if (type_code == VI_MEAS_TYPE_ROT_SPEED) {
            meas_type = &ViMeasTypeRotSpeed::type();
        } else if (type_code == VI_MEAS_TYPE_SMAX) {
            meas_type = &ViMeasTypeVibroSmax::type();
        } else if (type_code == VI_MEAS_TYPE_HARM_ACCEL_AMP) {
            meas_type = &ViMeasTypeHarmAmpAccel::type();
        } else if (type_code == VI_MEAS_TYPE_HARM_ACCEL_PHASE) {
            meas_type = &ViMeasTypeHarmPhaAccel::type();
        } else if (type_code == VI_MEAS_TYPE_HARM_VEL_AMP) {
            meas_type = &ViMeasTypeHarmAmpVelocity::type();
        } else if (type_code == VI_MEAS_TYPE_HARM_VEL_PHASE) {
            meas_type = &ViMeasTypeHarmPhaVelocity::type();
        } else if (type_code == VI_MEAS_TYPE_HARM_DISP_AMP) {
            meas_type = &ViMeasTypeHarmAmpDisp::type();
        } else if (type_code == VI_MEAS_TYPE_HARM_DISP_PHASE) {
            meas_type = &ViMeasTypeHarmPhaDisp::type();
        } else {
            meas_type = &ViMeasTypeUnknown::type();
        }
        return *meas_type;
    }

    const quint16 ViModuleMbConfigSerializer::getMeasTypeCode(const ViMeasType &meas_type) {
        quint16 meas_code;
        if (&meas_type == &ViMeasTypeVibroAccel::type()) {
            meas_code = VI_MEAS_TYPE_ACCEL;
        } else if (&meas_type == &ViMeasTypeVibroAccelHf::type()) {
            meas_code = VI_MEAS_TYPE_ACCEL_HF;
        } else if (&meas_type ==  &ViMeasTypeVibroVelocity::type()) {
            meas_code = VI_MEAS_TYPE_VELOCITY;
        } else if (&meas_type ==  &ViMeasTypeVibroDisp::type()) {
            meas_code = VI_MEAS_TYPE_DISPL;
        } else if (&meas_type ==  &ViMeasTypeVoltageGap::type()) {
            meas_code = VI_MEAS_TYPE_GAP;
        } else if (&meas_type ==  &ViMeasTypeVoltageDC::type()) {
            meas_code = VI_MEAS_TYPE_DC;
        } else if (&meas_type ==  &ViMeasTypePosition::type()) {
            meas_code = VI_MEAS_TYPE_POSITION;
        } else if (&meas_type ==  &ViMeasTypeRotSpeed::type()) {
            meas_code = VI_MEAS_TYPE_ROT_SPEED;
        } else if (&meas_type ==  &ViMeasTypeVibroSmax::type()) {
            meas_code = VI_MEAS_TYPE_SMAX;
        } else if (&meas_type ==  &ViMeasTypeHarmAmpAccel::type()) {
            meas_code = VI_MEAS_TYPE_HARM_ACCEL_AMP;
        } else if (&meas_type ==  &ViMeasTypeHarmPhaAccel::type()) {
            meas_code = VI_MEAS_TYPE_HARM_ACCEL_PHASE;
        } else if (&meas_type ==  &ViMeasTypeHarmAmpVelocity::type()) {
            meas_code = VI_MEAS_TYPE_HARM_VEL_AMP;
        } else if (&meas_type ==  &ViMeasTypeHarmPhaVelocity::type()) {
            meas_code = VI_MEAS_TYPE_HARM_VEL_PHASE;
        } else if (&meas_type ==  &ViMeasTypeHarmAmpDisp::type()) {
            meas_code = VI_MEAS_TYPE_HARM_DISP_AMP;
        } else if (&meas_type ==  &ViMeasTypeHarmPhaDisp::type()) {
            meas_code = VI_MEAS_TYPE_HARM_DISP_PHASE;
        } else {
            meas_code = VI_MEAS_TYPE_NONE;
        }
        return meas_code;
    }

    const ViDoutChannelType &ViModuleMbConfigSerializer::getDoutChType(quint16 type_code) {
        const ViDoutChannelType *dout_type {nullptr};
        if (type_code == VI_DOUT_TYPE_RELAY_HP) {
            dout_type = &ViDoutChannelTypes::relayHP();
        } else {
            dout_type = &ViDoutChannelTypes::relayLP();
        }
        return *dout_type;
    }

    const quint16 ViModuleMbConfigSerializer::getDoutChTypeCode(const ViDoutChannelType &ch_type) {
        quint16 type_code;
        if (ch_type == ViDoutChannelTypes::relayHP()) {
            type_code = VI_DOUT_TYPE_RELAY_HP;
        } else {
            type_code = VI_DOUT_TYPE_RELAY_LP;
        }
        return type_code;
    }

    void ViModuleMbConfigSerializer::addBlock(QByteArray &resArray, t_vi_opcfg_block_type type, const QByteArray &blockData) {
        t_vi_opcfg_block_hdr blockhdr;
        blockhdr.type = type;
        blockhdr.size = blockData.size();
        QByteArray pad;
        if ((blockData.size() % 4) != 0) {
            int size = 4 - blockData.size() % 4;
            pad.resize(size);
            for (int i = 0; i < size; ++i) {
                pad[i] = 0;
            }
        }
        resArray +=  QByteArray{reinterpret_cast<const char *>(&blockhdr), sizeof(blockhdr)} + blockData + pad;
    }

    void ViModuleMbConfigSerializer::serializeDevRef(QByteArray &resArray, int idx, const QString &devid) {
        if (!devid.isEmpty()) {
            t_vi_opcfg_devref cfgblock;
            memset(&cfgblock, 0, sizeof(cfgblock));

            cfgblock.ref_num = idx;
            addBlock(resArray, VI_OPCFG_BLOCK_TYPE_DEVREF,
                     QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)} +
                         devid.toUtf8());
        }
    }

    void ViModuleMbConfigSerializer::pareseDevRef(const t_vi_opcfg_devref &cfgblock, unsigned int size, QList<QString> &remoteDevIds, LQError &err) {
        if (size <= sizeof(t_vi_opcfg_devref)) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else {
            const int str_len {static_cast<int>(size - sizeof(t_vi_opcfg_devref))};
            QString exchangeId {QString::fromUtf8(reinterpret_cast<const char *>(cfgblock.exchange_id), str_len)};
            if (!exchangeId.isEmpty()) {
                int idx = cfgblock.ref_num;
                while (remoteDevIds.size() <= idx) {
                    remoteDevIds += QString{};
                }
                remoteDevIds[idx] = exchangeId;
            }
        }
    }

    void ViModuleMbConfigSerializer::serializeDevName(QByteArray &resArray, const ViModuleConfig &devCfg) {
        QString name {devCfg.vi().customDevName()};
        if (!name.isEmpty()) {
             addBlock(resArray, VI_OPCFG_BLOCK_TYPE_DEVNAME, name.toUtf8());
        }
    }

    void ViModuleMbConfigSerializer::parseDevName(ViModuleConfig &devCfg, const uint8_t *text, unsigned int size, LQError &err) {
        devCfg.vi().setCustomDevName(QString::fromUtf8(reinterpret_cast<const char *>(text), size));
    }

    void ViModuleMbConfigSerializer::serializeAcqChGen(QByteArray &resArray, const ViAcqChannelConfig &acqChCfg, QList<QString> &remoteDevIds) {
        t_vi_opcfg_block_acqch_gen cfgblock;
        memset(&cfgblock, 0, sizeof(cfgblock));
        cfgblock.ch_num = acqChCfg.channelNumber();
        cfgblock.flags = 0;
        if (acqChCfg.isEnabled()) {
            cfgblock.flags |= VI_OPCFG_ACQCH_GEN_FLAG_EN;
        }

        cfgblock.ch_type = getAcqChTypeCode(acqChCfg.type());

        serializeEventRef(cfgblock.inh_evt, acqChCfg.inhEventRef(), remoteDevIds);
        serializeEventRef(cfgblock.tm_evt, acqChCfg.tmEventRef(), remoteDevIds);

        serializeAcqChRef(cfgblock.phase_trig_ch, acqChCfg.phaseTriggerRef(), remoteDevIds);
        serializeAcqChRef(cfgblock.sec_ch, acqChCfg.secChRef(), remoteDevIds);


        addBlock(resArray, VI_OPCFG_BLOCK_TYPE_ACQCH_GEN,
                 QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)});
    }

    void ViModuleMbConfigSerializer::parseAcqChGen(ViModuleConfig &cfg, const t_vi_opcfg_block_acqch_gen &cfgblock, unsigned int size,
                                                   const QList<QString> &remoteDevIds, LQError &err) {
        if (size < VI_OPCFG_BLOCK_ACQCH_GEN_SIZE_MIN) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else if (cfgblock.ch_num < cfg.vi().acq().availableChannelsCnt()) {
            ViAcqChannelConfig &acqChCfg {cfg.vi().acq().channel(cfgblock.ch_num)};
            acqChCfg.setEnabled(cfgblock.flags & VI_OPCFG_ACQCH_GEN_FLAG_EN);
            acqChCfg.setType(getAcqChType(cfgblock.ch_type));


            parseEventRef(acqChCfg.inhEventRef(), cfgblock.inh_evt, remoteDevIds);
            parseEventRef(acqChCfg.tmEventRef(), cfgblock.tm_evt, remoteDevIds);

            parseAcqChRef(acqChCfg.phaseTriggerRef(), cfgblock.phase_trig_ch, remoteDevIds);
            parseAcqChRef(acqChCfg.secChRef(), cfgblock.sec_ch, remoteDevIds);

            /* очищаем измерения, созданные по умолчанию при назначении канала,
             * т.к. все измерения должны содержаться явно в настройках модуля */
            acqChCfg.removeAllMeas();
        }
    }



    void ViModuleMbConfigSerializer::serializeAcqChName(QByteArray &resArray, const ViAcqChannelConfig &acqChCfg) {
        if (!acqChCfg.customName().isEmpty()) {
            t_vi_opcfg_block_acqch_name cfgblock;
            memset(&cfgblock, 0, sizeof(cfgblock));

            cfgblock.ch_num = acqChCfg.channelNumber();
            addBlock(resArray, VI_OPCFG_BLOCK_TYPE_ACQCH_NAME,
                     QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)} +
                     acqChCfg.customName().toUtf8());
        }
    }

    void ViModuleMbConfigSerializer::parseAcqChName(ViModuleConfig &cfg, const t_vi_opcfg_block_acqch_name &cfgblock, unsigned int size, LQError &err)     {
        if (size < VI_OPCFG_BLOCK_ACQCH_NAME_SIZE_MIN) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else if (cfgblock.ch_num < cfg.vi().acq().availableChannelsCnt()) {
            ViAcqChannelConfig &chCfg {cfg.vi().acq().channel(cfgblock.ch_num)};
            const int str_len {static_cast<int>(size - sizeof(t_vi_opcfg_block_acqch_name))};
            chCfg.setCustomName(QString::fromUtf8(reinterpret_cast<const char *>(cfgblock.text), str_len));
        }
    }

    void ViModuleMbConfigSerializer::serializeAcqChSensor(QByteArray &resArray, const ViAcqChannelConfig &acqChCfg) {
        t_vi_opcfg_block_acqch_sensor cfgblock;
        memset(&cfgblock, 0, sizeof(cfgblock));

        const ViAcqSensorConfig &sensorCfg {acqChCfg.sensor()};

        cfgblock.ch_num                 = acqChCfg.channelNumber();

        if (sensorCfg.type() == ViAcqSensorTypeAccelerometer::type()) {
            cfgblock.sensor_type = VI_SENSOR_TYPE_ACCELEROMETER;
        } else if (sensorCfg.type() == ViAcqSensorTypeProximeter::type()) {
            cfgblock.sensor_type = VI_SENSOR_TYPE_PROXIMETER;
        }

        if (sensorCfg.connectionMethod() == ViAcqSensorConTypes::ICP()) {
            cfgblock.con_type = VI_SENSOR_CON_TYPE_ICP;
        } else if (sensorCfg.connectionMethod() == ViAcqSensorConTypes::VOutPosSup()) {
            cfgblock.con_type = VI_SENSOR_CON_TYPE_VOUT_USUP_POS;
        } else if (sensorCfg.connectionMethod() == ViAcqSensorConTypes::VOutNegSup()) {
            cfgblock.con_type = VI_SENSOR_CON_TYPE_VOUT_USUP_NEG;
        }

        cfgblock.conv.conv_type             = VI_SENSOR_CONV_TYPE_SENS;
        cfgblock.conv.scale_factor          = sensorCfg.scaleFactor();
        cfgblock.conv.offset                = sensorCfg.offset();
        cfgblock.conv.input_scale_multipler = sensorCfg.type().inputUnit().multipler()/sensorCfg.type().scaleInputUnit().multipler();
        cfgblock.conv.output_scale_multipler = sensorCfg.type().scaleOutputUnit().multipler()/sensorCfg.type().outputUnit().multipler();
        cfgblock.valid_range.max_value      = sensorCfg.maxValidValue();
        cfgblock.valid_range.min_value      = sensorCfg.minValidValue();

        if (sensorCfg.supportDcCheckShort() &&
            (!sensorCfg.dcCheckShortIsOptional() || sensorCfg.dcCheckShort().isEnabled())) {
            cfgblock.flags |= VI_OPCFG_SENSOR_FLAG_DC_CHECK_SHORT_EN;
        }
        cfgblock.dc_check_short = sensorCfg.dcCheckShort().value();

        if (sensorCfg.supportDcCheckOpen() &&
            (!sensorCfg.dcCheckOpenIsOptional() ||sensorCfg.dcCheckOpen().isEnabled())) {
            cfgblock.flags |= VI_OPCFG_SENSOR_FLAG_DC_CHECK_OPEN_EN;
        }
        cfgblock.dc_check_open = sensorCfg.dcCheckOpen().value();

        addBlock(resArray, VI_OPCFG_BLOCK_TYPE_ACQCH_SENSOR,
                 QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)});
    }


    void ViModuleMbConfigSerializer::parseAcqChSensor(
        ViModuleConfig &cfg, const t_vi_opcfg_block_acqch_sensor &cfgblock, unsigned int size, LQError &err) {

        if (size < VI_OPCFG_BLOCK_ACQCH_SENSOR_SIZE_MIN) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else if (cfgblock.ch_num < cfg.vi().acq().availableChannelsCnt()) {
            ViAcqSensorConfig &sensorCfg {cfg.vi().acq().channel(cfgblock.ch_num).sensor()};
            if (cfgblock.con_type == VI_SENSOR_CON_TYPE_ICP) {
                sensorCfg.setConnectionMethod(ViAcqSensorConTypes::ICP());
            } else if (cfgblock.con_type == VI_SENSOR_CON_TYPE_VOUT_USUP_POS) {
                sensorCfg.setConnectionMethod(ViAcqSensorConTypes::VOutPosSup());
            } else if (cfgblock.con_type == VI_SENSOR_CON_TYPE_VOUT_USUP_NEG) {
                sensorCfg.setConnectionMethod(ViAcqSensorConTypes::VOutNegSup());
            }

            if (cfgblock.conv.conv_type == VI_SENSOR_CONV_TYPE_SENS) {
                sensorCfg.setScaleFactor(cfgblock.conv.scale_factor);
                sensorCfg.setOffset(cfgblock.conv.offset);
            }

            sensorCfg.setMaxValidValue(cfgblock.valid_range.max_value);
            sensorCfg.setMinValidValue(cfgblock.valid_range.min_value);

            sensorCfg.dcCheckShort().setEnabled(cfgblock.flags & VI_OPCFG_SENSOR_FLAG_DC_CHECK_SHORT_EN);
            sensorCfg.dcCheckShort().setValue(cfgblock.dc_check_short);

            sensorCfg.dcCheckOpen().setEnabled(cfgblock.flags & VI_OPCFG_SENSOR_FLAG_DC_CHECK_OPEN_EN);
            sensorCfg.dcCheckOpen().setValue(cfgblock.dc_check_open);
        }
    }

    void ViModuleMbConfigSerializer::serializeAcqChPhaseTrig(QByteArray &resArray, const ViAcqChannelConfig &acqChCfg) {
        if (acqChCfg.hasPhaseTrigCfg()) {
            t_vi_opcfg_block_acqch_phasetrig cfgblock;
            memset(&cfgblock, 0, sizeof(cfgblock));

            const ViAcqChannelPhaseTrigConfig &phaseTrigCfg {acqChCfg.phaseTrigCfg()};
            cfgblock.ch_num                 = acqChCfg.channelNumber();
            cfgblock.events_per_rev         = phaseTrigCfg.eventsPerRevolution();
            cfgblock.threshold.value        = phaseTrigCfg.threshold().value();
            cfgblock.threshold.hysteresis   = phaseTrigCfg.threshold().hysteresis();

            if (phaseTrigCfg.polarity() == ViAcqChannelPhaseTrigPolarities::projection()) {
                cfgblock.polarity = VI_OPCFG_PHASETRIG_POLARITY_PROJECTION;
            } else if (phaseTrigCfg.polarity() == ViAcqChannelPhaseTrigPolarities::notch()) {
                cfgblock.polarity = VI_OPCFG_PHASETRIG_POLARITY_NOTCH;
            }

            if (phaseTrigCfg.pubIsSetEnabled()) {
                cfgblock.flags |= VI_OPCFG_PHASETRIG_FLAG_PUB_EN;
            }

            addBlock(resArray, VI_OPCFG_BLOCK_TYPE_ACQCH_PHASETRIG,
                     QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)});
        }
    }

    void ViModuleMbConfigSerializer::parseAcqChPhaseTrig(ViModuleConfig &cfg, const t_vi_opcfg_block_acqch_phasetrig &cfgblock, unsigned int size, LQError &err) {
        if (size < VI_OPCFG_BLOCK_ACQCH_PHASETRIG_SIZE_MIN) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else if (cfgblock.ch_num < cfg.vi().acq().availableChannelsCnt()) {
            ViAcqChannelConfig &acqChCfg {cfg.vi().acq().channel(cfgblock.ch_num)};
            if (acqChCfg.hasPhaseTrigCfg()) {
                ViAcqChannelPhaseTrigConfig &phaseTrigCfg {acqChCfg.phaseTrigCfg()};
                phaseTrigCfg.setEventsPerRevolution(cfgblock.events_per_rev);
                phaseTrigCfg.threshold().setValue(cfgblock.threshold.value);
                phaseTrigCfg.threshold().setHysteresis(cfgblock.threshold.hysteresis);

                if (cfgblock.polarity == VI_OPCFG_PHASETRIG_POLARITY_PROJECTION) {
                    phaseTrigCfg.setPolarity(ViAcqChannelPhaseTrigPolarities::projection());
                } else if (cfgblock.polarity == VI_OPCFG_PHASETRIG_POLARITY_NOTCH) {
                    phaseTrigCfg.setPolarity(ViAcqChannelPhaseTrigPolarities::notch());
                }

                if (size >= (offsetof(t_vi_opcfg_block_acqch_phasetrig, flags) + sizeof(cfgblock.flags))) {
                    phaseTrigCfg.setPubIsEnabled(cfgblock.flags & VI_OPCFG_PHASETRIG_FLAG_PUB_EN);
                }
            }
        }
    }

    void ViModuleMbConfigSerializer::serializeAcqChPosition(QByteArray &resArray, const ViAcqChannelConfig &acqChCfg) {
        if (acqChCfg.hasPositionCfg()) {
            t_vi_opcfg_block_acqch_position cfgblock;
            memset(&cfgblock, 0, sizeof(cfgblock));

            const ViAcqChannelPositionConfig &posCfg {acqChCfg.positionCfg()};
            cfgblock.ch_num                 = acqChCfg.channelNumber();
            cfgblock.zero_pos               = posCfg.zeroPosition();


            if (posCfg.normalDirection() == ViAcqChannelPositionNormalDirections::towardProbe()) {
                cfgblock.normal_dir = VI_OPCFG_POSITION_NORMAL_DIR_TOWARD_PROBE;
            } else if (posCfg.normalDirection() == ViAcqChannelPositionNormalDirections::awayFromProbe()) {
                cfgblock.normal_dir = VI_OPCFG_POSITION_NORMAL_DIR_AWAY_FROM_PROBE;
            }

            addBlock(resArray, VI_OPCFG_BLOCK_TYPE_ACQCH_POSITION,
                     QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)});
        }
    }

    void ViModuleMbConfigSerializer::parseAcqChPosition(ViModuleConfig &cfg, const t_vi_opcfg_block_acqch_position &cfgblock, unsigned int size, LQError &err) {
        if (size < VI_OPCFG_BLOCK_ACQCH_POSITION_SIZE_MIN) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else if (cfgblock.ch_num < cfg.vi().acq().availableChannelsCnt()) {
            ViAcqChannelConfig &acqChCfg {cfg.vi().acq().channel(cfgblock.ch_num)};
            if (acqChCfg.hasPositionCfg()) {
                ViAcqChannelPositionConfig &posCfg {acqChCfg.positionCfg()};
                posCfg.setZeroPosition(cfgblock.zero_pos);

                if (cfgblock.normal_dir == VI_OPCFG_POSITION_NORMAL_DIR_TOWARD_PROBE) {
                    posCfg.setNormalDirection(ViAcqChannelPositionNormalDirections::towardProbe());
                } else if (cfgblock.normal_dir == VI_OPCFG_POSITION_NORMAL_DIR_AWAY_FROM_PROBE) {
                    posCfg.setNormalDirection(ViAcqChannelPositionNormalDirections::awayFromProbe());
                }
            }
        }
    }



    void ViModuleMbConfigSerializer::serializeMeasGen(QByteArray &resArray, const ViMeasConfig &measCfg) {
        t_vi_opcfg_block_meas_gen cfgblock;
        memset(&cfgblock, 0, sizeof(cfgblock));
        cfgblock.ch_num = measCfg.group().channel().channelNumber();
        cfgblock.meas_num = measCfg.measNum();
        cfgblock.meas_type = getMeasTypeCode(measCfg.type());

        if (measCfg.hasParamType()) {
            if (&measCfg.paramType() == &ViMeasParameterTypes::rms()) {
                cfgblock.param_type = VI_MEAS_PARAM_TYPE_RMS;
            } else if (&measCfg.paramType() == &ViMeasParameterTypes::pp()) {
                cfgblock.param_type = VI_MEAS_PARAM_TYPE_PP;
            } else if (&measCfg.paramType() == &ViMeasParameterTypes::peak()) {
                cfgblock.param_type = VI_MEAS_PARAM_TYPE_PEAK;
            }
        }

        cfgblock.min_value = measCfg.minValue();
        cfgblock.max_value = measCfg.maxValue();
        cfgblock.clamp_value = measCfg.clampValue();
        if (measCfg.dbScaleIsEnabled()) {
            cfgblock.flags |= VI_OPCFG_MEAS_GEN_FLAG_DB_SCALE;
        }
        cfgblock.db_scale_base_lvl = measCfg.dbScaleBaseValue();
        cfgblock.change_thresh = measCfg.changeThreshold();
        cfgblock.trip_mult = measCfg.tripMultiply();

        if (measCfg.type().isHarmonic()) {
            cfgblock.nx = measCfg.nxValue();
        }

        if (measCfg.hasHpFilter() && measCfg.hpFilter().isEnabled()) {
            cfgblock.hp_filter.flags |= VI_OPCFG_MEAS_FILTER_FLAG_EN;
            cfgblock.hp_filter.freq = measCfg.hpFilter().freq();
        }

        if (measCfg.hasLpFilter() && measCfg.lpFilter().isEnabled()) {
            cfgblock.lp_filter.flags |= VI_OPCFG_MEAS_FILTER_FLAG_EN;
            cfgblock.lp_filter.freq = measCfg.lpFilter().freq();
        }

        addBlock(resArray, VI_OPCFG_BLOCK_TYPE_MEAS_GEN,
                 QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)});
    }

    void ViModuleMbConfigSerializer::parseMeasGen(ViModuleConfig &cfg, const t_vi_opcfg_block_meas_gen &cfgblock, unsigned int size, LQError &err) {
        if (size < VI_OPCFG_BLOCK_MEAS_GEN_SIZE_MIN) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else if (cfgblock.ch_num < cfg.vi().acq().availableChannelsCnt()) {
            ViAcqChannelConfig &chCfg {cfg.vi().acq().channel(cfgblock.ch_num)};
            const unsigned meas_num {cfgblock.meas_num};

            const ViMeasType &meas_type {getMeasType(cfgblock.meas_type)};

            /* проверяем, добавлено ли уже измерение, на случай группы из нескольких измерений */
            ViMeasConfig *measCfg {chCfg.getMeas(meas_num)};
            if (!measCfg) {
                /* Если нет, то добавляем измерение через группу, найдя тип поддерживаемой группы
                 * с требуемым типом измерения */
                const QList<const ViMeasGroupType *> &groups {chCfg.supportedMeasGroups()};
                const auto &it {std::find_if(groups.cbegin(), groups.cend(),
                                 [&meas_type](const ViMeasGroupType *group){
                                     return group->measurementTypes().contains(&meas_type);
                })};

                const ViMeasGroupType *set_group = (it != groups.cend()) ? *it : &ViAcqChannelTypeUnknown::measGroupUnknown();
                chCfg.addMeasGroup(*set_group, true);
                /* повторяем поиск измерения, после добавления группы */
                measCfg = chCfg.getMeas(meas_num);
            }
            /* проверяем, что нашли измерения и оно того же типа */
            if (!measCfg || ((measCfg->type() != meas_type) && !(&measCfg->type() == &ViMeasTypeUnknown::type()))) {
                err = ViErrors::opcfgParseMeasTypeMismatch();
            } else {
                if (measCfg->hasParamType() && (cfgblock.meas_type != VI_MEAS_PARAM_TYPE_NONE)) {
                    const ViMeasParameterType *setType {nullptr};
                    if (cfgblock.param_type == VI_MEAS_PARAM_TYPE_RMS) {
                        setType = &ViMeasParameterTypes::rms();
                    } else if (cfgblock.param_type == VI_MEAS_PARAM_TYPE_PP) {
                        setType = &ViMeasParameterTypes::pp();
                    } else if (cfgblock.param_type == VI_MEAS_PARAM_TYPE_PEAK) {
                        setType = &ViMeasParameterTypes::peak();
                    }
                    if (setType) {
                        measCfg->setParamType(*setType);
                    }
                }

                measCfg->setMinValue(cfgblock.min_value);
                measCfg->setMaxValue(cfgblock.max_value);
                measCfg->setClampValue(cfgblock.clamp_value);
                if (measCfg->dbScaleSupported()) {
                    measCfg->setDbScaleBaseLevel(cfgblock.db_scale_base_lvl);
                    measCfg->setDbScaleEnabled(cfgblock.flags & VI_OPCFG_MEAS_GEN_FLAG_DB_SCALE);
                }
                measCfg->setChangeThreshold(cfgblock.change_thresh);
                measCfg->setTripMultiplty(cfgblock.trip_mult);


                if (measCfg->hasHpFilter()) {
                    measCfg->hpFilter().setEnabled(cfgblock.hp_filter.flags & VI_OPCFG_MEAS_FILTER_FLAG_EN);
                    measCfg->hpFilter().setFreq(cfgblock.hp_filter.freq);
                }
                if (measCfg->hasLpFilter()) {
                    measCfg->lpFilter().setEnabled(cfgblock.lp_filter.flags & VI_OPCFG_MEAS_FILTER_FLAG_EN);
                    measCfg->lpFilter().setFreq(cfgblock.lp_filter.freq);
                }

                if (measCfg->type().isHarmonic() && (cfgblock.nx > 0)) {
                    measCfg->setNxValue(cfgblock.nx);
                }
            }
        }
    }

    void ViModuleMbConfigSerializer::serializeMeasName(QByteArray &resArray, const ViMeasConfig &measCfg) {
        if (!measCfg.customName().isEmpty()) {
            t_vi_opcfg_block_meas_name cfgblock;
            memset(&cfgblock, 0, sizeof(cfgblock));
            cfgblock.ch_num = measCfg.group().channel().channelNumber();
            cfgblock.meas_num = measCfg.measNum();
            addBlock(resArray, VI_OPCFG_BLOCK_TYPE_MEAS_NAME,
                     QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)} +
                         measCfg.customName().toUtf8());
        }
    }

    void ViModuleMbConfigSerializer::parseMeasName(ViModuleConfig &cfg, const t_vi_opcfg_block_meas_name &cfgblock, unsigned int size, LQError &err) {
        if (size < VI_OPCFG_BLOCK_MEAS_NAME_SIZE_MIN) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else {
            ViAcqChannelConfig &chCfg {cfg.vi().acq().channel(cfgblock.ch_num)};
            const unsigned meas_num {cfgblock.meas_num};

            /* проверяем, добавлено ли уже измерение, на случай группы из нескольких измерений */
            ViMeasConfig *measCfg {chCfg.getMeas(meas_num)};
            if (measCfg) {
                const int str_len {static_cast<int>(size - sizeof(t_vi_opcfg_block_meas_name))};
                measCfg->setCustomName(QString::fromUtf8(reinterpret_cast<const char *>(cfgblock.text), str_len));
            }
        }
    }

    void ViModuleMbConfigSerializer::serializeMeasSetpoint(QByteArray &resArray, const ViSetpointConfig &spCfg) {
        t_vi_opcfg_block_meas_setpoint cfgblock;
        memset(&cfgblock, 0, sizeof(cfgblock));
        cfgblock.ch_num = spCfg.meas().group().channel().channelNumber();
        cfgblock.meas_num = spCfg.meas().measNum();
        if (spCfg.alarmType() == ViSetpointConfig::AlarmType::Alert) {
            cfgblock.alarm_type = VI_SETPOINT_ALARM_TYPE_ALERT;
        } else if (spCfg.alarmType() == ViSetpointConfig::AlarmType::Danger) {
            cfgblock.alarm_type = VI_SETPOINT_ALARM_TYPE_DANGER;
        }

        if (spCfg.conditionType() == ViSetpointConditionTypes::above()) {
            cfgblock.cond_type = VI_SETPOINT_COND_TYPE_ABOVE;
        } else if (spCfg.conditionType() == ViSetpointConditionTypes::below()) {
            cfgblock.cond_type = VI_SETPOINT_COND_TYPE_BELOW;
        } else if (spCfg.conditionType() == ViSetpointConditionTypes::outOfBand()) {
            cfgblock.cond_type = VI_SETPOINT_COND_TYPE_OUT_OF_BAND;
        } else if (spCfg.conditionType() == ViSetpointConditionTypes::inBand()) {
            cfgblock.cond_type = VI_SETPOINT_COND_TYPE_IN_BAND;
        }

        cfgblock.upper_lvl.value = spCfg.upperLimit().value();
        cfgblock.upper_lvl.hysteresis = spCfg.upperLimit().hysteresis();
        cfgblock.under_lvl.value = spCfg.underLimit().value();
        cfgblock.under_lvl.hysteresis = spCfg.underLimit().hysteresis();

        addBlock(resArray, VI_OPCFG_BLOCK_TYPE_MEAS_SP,
                 QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)});
    }

    void ViModuleMbConfigSerializer::parseMeasSetpoint(ViModuleConfig &cfg, const t_vi_opcfg_block_meas_setpoint &cfgblock, unsigned int size, LQError &err) {
        if (size < VI_OPCFG_BLOCK_MEAS_SETPOINT_SIZE_MIN) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else {
            ViMeasConfig *measCfg {cfg.vi().acq().getMeas(cfgblock.ch_num, cfgblock.meas_num)};
            if (measCfg) {
                ViSetpointConfig *spCfg {nullptr};
                if (cfgblock.alarm_type == VI_SETPOINT_ALARM_TYPE_ALERT) {
                    spCfg = &measCfg->setpointAlert();
                } else if (cfgblock.alarm_type == VI_SETPOINT_ALARM_TYPE_DANGER) {
                    spCfg = &measCfg->setpointDanger();
                }
                if (spCfg) {
                    if (cfgblock.cond_type == VI_SETPOINT_COND_TYPE_ABOVE) {
                        spCfg->setConditionType(ViSetpointConditionTypes::above());
                    } else if (cfgblock.cond_type == VI_SETPOINT_COND_TYPE_BELOW) {
                        spCfg->setConditionType(ViSetpointConditionTypes::below());
                    } else if (cfgblock.cond_type == VI_SETPOINT_COND_TYPE_OUT_OF_BAND) {
                        spCfg->setConditionType(ViSetpointConditionTypes::outOfBand());
                    } else if (cfgblock.cond_type == VI_SETPOINT_COND_TYPE_IN_BAND) {
                        spCfg->setConditionType(ViSetpointConditionTypes::inBand());
                    } else {
                        spCfg->setConditionType(ViSetpointConditionTypes::disabled());
                    }

                    spCfg->upperLimit().setValue(cfgblock.upper_lvl.value);
                    spCfg->upperLimit().setHysteresis(cfgblock.upper_lvl.hysteresis);
                    spCfg->underLimit().setValue(cfgblock.under_lvl.value);
                    spCfg->underLimit().setHysteresis(cfgblock.under_lvl.hysteresis);
                }
            }

        }

    }

    void ViModuleMbConfigSerializer::serializeEventGen(QByteArray &resArray, const ViEventConfig &eventCfg, QList<QString> &remoteDevIds) {
        t_vi_opcfg_block_event_gen cfgblock;
        memset(&cfgblock, 0, sizeof(cfgblock));
        cfgblock.evt_num = eventCfg.eventNumber();
        if (eventCfg.isEnabled()) {
            cfgblock.flags |= VI_OPCFG_EVT_FLAG_EN;
        }

        QList<ViLogicElementConfig *> elems = eventCfg.logicScheme().allElements(true, true);
        cfgblock.elem_cnt = elems.size();
        if (cfgblock.elem_cnt < VI_OPCFG_EVT_LOGIC_ELEM_MAX) {
            cfgblock.flags |= VI_OPCFG_EVT_FLAG_VALID;
            /** @todo check meas assignment */
        }


        std::unique_ptr<t_vi_opcfg_logic_elem[]> elems_block {new t_vi_opcfg_logic_elem[cfgblock.elem_cnt]};
        memset(elems_block.get(), 0, sizeof(t_vi_opcfg_logic_elem) * cfgblock.elem_cnt);
        int elem_num {0};
        for (const ViLogicElementConfig *elemCfg :elems) {
            t_vi_opcfg_logic_elem *elem = &elems_block[elem_num];
            if (!elemCfg) {
                elem->type = VI_LOGIC_ELEM_TYPE_NONE;
            } else if (&elemCfg->elemType() == &ViLogicElementConfigMeasCond::type()) {
                const ViLogicElementConfigMeasCond *measElemCfg {static_cast<const ViLogicElementConfigMeasCond *>(elemCfg)};
                if (&measElemCfg->condType() == &ViMeasCondTypes::failure()) {
                    elem->type = VI_LOGIC_ELEM_TYPE_MEAS_COND_FAILURE;
                } else if (&measElemCfg->condType() == &ViMeasCondTypes::alert()) {
                    elem->type = VI_LOGIC_ELEM_TYPE_MEAS_COND_ALERT;
                } else if (&measElemCfg->condType() == &ViMeasCondTypes::danger()) {
                    elem->type = VI_LOGIC_ELEM_TYPE_MEAS_COND_DANGER;
                } else if (&measElemCfg->condType() == &ViMeasCondTypes::changeThreshold()) {
                    elem->type = VI_LOGIC_ELEM_TYPE_MEAS_COND_THRESHOLD_EXC;
                }
                serializeMeasRef(elem->meas_ref, measElemCfg->measRef(), remoteDevIds);
            } else if (&elemCfg->elemType() == &ViLogicElementConfigDin::type()) {
                const ViLogicElementConfigDin *dinElemCfg {static_cast<const ViLogicElementConfigDin *>(elemCfg)};
                elem->type = VI_LOGIC_ELEM_TYPE_DIN;
                serializeDinRef(elem->din_ref, dinElemCfg->dinRef(), remoteDevIds);
            } else if (&elemCfg->elemType() == &ViLogicElementConfigEvent::type()) {
                const ViLogicElementConfigEvent *evtElemCfg {static_cast<const ViLogicElementConfigEvent *>(elemCfg)};
                elem->type = VI_LOGIC_ELEM_TYPE_EVENT;
                serializeEventRef(elem->evt_ref, evtElemCfg->eventRef(), remoteDevIds);
            } else if (&elemCfg->elemType() == &ViLogicElementConfigAnd::type()) {
                elem->type = VI_LOGIC_ELEM_TYPE_AND;
            } else if (&elemCfg->elemType() == &ViLogicElementConfigOr::type()) {
                elem->type = VI_LOGIC_ELEM_TYPE_OR;
            } else if (&elemCfg->elemType() == &ViLogicElementConfigNot::type()) {
                elem->type = VI_LOGIC_ELEM_TYPE_NOT;
            }
            ++elem_num;
        }

        addBlock(resArray, VI_OPCFG_BLOCK_TYPE_EVENT_GEN,
             QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)} +
                                                              QByteArray{reinterpret_cast<const char *>(elems_block.get()), static_cast<int>(sizeof(elems_block[0]) * cfgblock.elem_cnt)});
    }

    void ViModuleMbConfigSerializer::parseEventGen(ViModuleConfig &cfg, const t_vi_opcfg_block_event_gen &cfgblock, unsigned int size, const QList<QString> &remoteDevIds, LQError &err) {
        if (size < VI_OPCFG_BLOCK_EVENT_GEN_SIZE_MIN) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else if (size < (VI_OPCFG_BLOCK_EVENT_GEN_SIZE_MIN + cfgblock.elem_cnt * sizeof(t_vi_opcfg_logic_elem))) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else {
            ViEventsConfig &evtsCfg {cfg.vi().events()};
            ViEventConfig *evtCfg {evtsCfg.eventCfg(cfgblock.evt_num)};
            if (!evtCfg) {
                evtCfg = evtsCfg.addEvent();
                evtCfg->setEventNumber(cfgblock.evt_num);
            } else {
                evtCfg->logicScheme().clearScheme();
            }

            evtCfg->setEnabled(cfgblock.flags & VI_OPCFG_EVT_FLAG_EN);

            ViLogicScheme &scheme {evtCfg->logicScheme()};
            ViLogicElementConfig *curInElem {nullptr};
            int input_idx {0};
            for (int elem_idx = cfgblock.elem_cnt-1; elem_idx >= 0; --elem_idx) {
                const t_vi_opcfg_logic_elem *cfgelem = &cfgblock.elems[elem_idx];
                const ViLogicElementType *type {nullptr};
                if ((cfgelem->type == VI_LOGIC_ELEM_TYPE_MEAS_COND_FAILURE)
                    || (cfgelem->type == VI_LOGIC_ELEM_TYPE_MEAS_COND_ALERT)
                    || (cfgelem->type == VI_LOGIC_ELEM_TYPE_MEAS_COND_DANGER)
                    || (cfgelem->type == VI_LOGIC_ELEM_TYPE_MEAS_COND_THRESHOLD_EXC)) {
                    type = &ViLogicElementConfigMeasCond::type();
                } else if (cfgelem->type == VI_LOGIC_ELEM_TYPE_DIN) {
                    type = &ViLogicElementConfigDin::type();
                } else if (cfgelem->type == VI_LOGIC_ELEM_TYPE_EVENT) {
                    type = &ViLogicElementConfigEvent::type();
                } else if (cfgelem->type == VI_LOGIC_ELEM_TYPE_AND) {
                    type = &ViLogicElementConfigAnd::type();
                } else if (cfgelem->type == VI_LOGIC_ELEM_TYPE_OR) {
                    type = &ViLogicElementConfigOr::type();
                } else if (cfgelem->type == VI_LOGIC_ELEM_TYPE_NOT) {
                    type = &ViLogicElementConfigNot::type();
                }

                if (type) {

                    ViLogicElementConfig *elemCfg {curInElem ?
                                                      scheme.createInsertElement(*curInElem, input_idx, *type) :
                                                      scheme.createBaseElement(*type)};
                    if (type ==  &ViLogicElementConfigMeasCond::type()) {
                        ViLogicElementConfigMeasCond *measElemCfg{static_cast<ViLogicElementConfigMeasCond *>(elemCfg)};
                        if (cfgelem->type == VI_LOGIC_ELEM_TYPE_MEAS_COND_FAILURE) {
                            measElemCfg->setCondType(ViMeasCondTypes::failure());
                        } else if (cfgelem->type == VI_LOGIC_ELEM_TYPE_MEAS_COND_ALERT) {
                            measElemCfg->setCondType(ViMeasCondTypes::alert());
                        } else if (cfgelem->type == VI_LOGIC_ELEM_TYPE_MEAS_COND_DANGER) {
                            measElemCfg->setCondType(ViMeasCondTypes::danger());
                        } else if (cfgelem->type == VI_LOGIC_ELEM_TYPE_MEAS_COND_THRESHOLD_EXC) {
                            measElemCfg->setCondType(ViMeasCondTypes::changeThreshold());
                        }

                        parseMeasRef(measElemCfg->measRef(), cfgelem->meas_ref, remoteDevIds);
                    } else if (type == &ViLogicElementConfigDin::type()) {
                        ViLogicElementConfigDin *dinElemCfg {static_cast<ViLogicElementConfigDin *>(elemCfg)};
                        parseDinRef(dinElemCfg->dinRef(), cfgelem->din_ref, remoteDevIds);
                    } else if (type == &ViLogicElementConfigEvent::type()) {
                        ViLogicElementConfigEvent *eventElemCfg {static_cast<ViLogicElementConfigEvent *>(elemCfg)};
                        parseEventRef(eventElemCfg->eventRef(), cfgelem->evt_ref, remoteDevIds);
                    }

                    if (elem_idx > 0) {
                        if (elemCfg->inputsCount() > 0) {
                            curInElem = elemCfg;
                            input_idx = elemCfg->inputsCount() - 1;
                        } else if (curInElem) {
                            --input_idx;
                            while (curInElem && (input_idx < 0)) {
                                curInElem = scheme.getOutConElem(*curInElem, input_idx);
                                if (curInElem) {
                                    --input_idx;
                                }
                            }
                        }
                    }
                } else {
                    /** @todo error */
                }
            }
        }
    }

    void ViModuleMbConfigSerializer::serializeEventName(QByteArray &resArray, const ViEventConfig &eventCfg) {
        if (!eventCfg.customName().isEmpty()) {
            t_vi_opcfg_block_event_name cfgblock;
            memset(&cfgblock, 0, sizeof(cfgblock));

            cfgblock.evt_num = eventCfg.eventNumber();
            addBlock(resArray, VI_OPCFG_BLOCK_TYPE_EVENT_NAME,
                     QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)} +
                         eventCfg.customName().toUtf8());
        }
    }

    void ViModuleMbConfigSerializer::parseEventName(ViModuleConfig &cfg, const t_vi_opcfg_block_event_name &cfgblock, unsigned int size, LQError &err) {
        if (size < VI_OPCFG_BLOCK_EVENT_NAME_SIZE_MIN) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else {
            ViEventConfig *evtCfg {cfg.vi().events().eventCfg(cfgblock.evt_num)};
            if (evtCfg) {
                const int str_len {static_cast<int>(size - sizeof(t_vi_opcfg_block_event_name))};
                evtCfg->setCustomName(QString::fromUtf8(reinterpret_cast<const char *>(cfgblock.text), str_len));
            }
        }
    }

    void ViModuleMbConfigSerializer::serializeDinGen(QByteArray &resArray, const ViDinChannelConfig &dinChCfg) {
        t_vi_opcfg_block_din_gen cfgblock;
        memset(&cfgblock, 0, sizeof(cfgblock));
        cfgblock.ch_num = dinChCfg.channelNumber();
        if (dinChCfg.isEnabled()) {
            cfgblock.flags |= VI_OPCFG_DIN_FLAG_EN;
        }
        addBlock(resArray, VI_OPCFG_BLOCK_TYPE_DIN_GEN,
                 QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)});
    }

    void ViModuleMbConfigSerializer::parseDinGen(ViModuleConfig &cfg, const t_vi_opcfg_block_din_gen &cfgblock, unsigned int size, LQError &err) {
        if (size < VI_OPCFG_BLOCK_DIN_GEN_SIZE_MIN) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else if (cfgblock.ch_num < cfg.vi().din().availableChannelsCnt()) {
            ViDinChannelConfig &chCfg {cfg.vi().din().channel(cfgblock.ch_num)};
            chCfg.setEnabled(cfgblock.flags & VI_OPCFG_DIN_FLAG_EN);
        }
    }

    void ViModuleMbConfigSerializer::serializeDinName(QByteArray &resArray, const ViDinChannelConfig &dinChCfg) {
        if (!dinChCfg.customName().isEmpty()) {
            t_vi_opcfg_block_din_name cfgblock;
            memset(&cfgblock, 0, sizeof(cfgblock));

            cfgblock.ch_num = dinChCfg.channelNumber();
            addBlock(resArray, VI_OPCFG_BLOCK_TYPE_DIN_NAME,
                     QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)} +
                         dinChCfg.customName().toUtf8());
        }
    }

    void ViModuleMbConfigSerializer::parseDinName(ViModuleConfig &cfg, const t_vi_opcfg_block_din_name &cfgblock, unsigned int size, LQError &err) {
        if (size < VI_OPCFG_BLOCK_DIN_NAME_SIZE_MIN) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else if (cfgblock.ch_num < cfg.vi().din().availableChannelsCnt()) {
            ViDinChannelConfig &dinChCfg {cfg.vi().din().channel(cfgblock.ch_num)};
            const int str_len {static_cast<int>(size - sizeof(t_vi_opcfg_block_din_name))};
            dinChCfg.setCustomName(QString::fromUtf8(reinterpret_cast<const char *>(cfgblock.text), str_len));
        }
    }

    void ViModuleMbConfigSerializer::serializeDoutGen(QByteArray &resArray, const ViDoutChannelConfig &doutChCfg, QList<QString> &remoteDevIds) {
        t_vi_opcfg_block_dout_gen cfgblock;
        memset(&cfgblock, 0, sizeof(cfgblock));
        cfgblock.ch_num = doutChCfg.channelNumber();
        cfgblock.ch_type = getDoutChTypeCode(doutChCfg.type());
        if (doutChCfg.isEnabled()) {
            cfgblock.flags |= VI_OPCFG_DOUT_FLAG_EN;
        }
        if (doutChCfg.normalOpState() == ViDoutNormalOpStates::normEnergized()) {
            cfgblock.flags |= VI_OPCFG_DOUT_FLAG_NORM_ENERGIZED;
        }
        if (doutChCfg.initialState()) {
            cfgblock.flags |= VI_OPCFG_DOUT_FLAG_INITIAL_ALARM;
        }
        if (doutChCfg.isLatching()) {
            cfgblock.flags |= VI_OPCFG_DOUT_FLAG_LATCHING;
        }
        if (doutChCfg.isManualControlEnabled()) {
            cfgblock.flags |= VI_OPCFG_DOUT_FLAG_MANUAL;
        }
        cfgblock.on_delay = doutChCfg.onDelay();
        cfgblock.off_delay = doutChCfg.offDelay();
        serializeEventRef(cfgblock.ctl_evt, doutChCfg.ctlEventRef(), remoteDevIds);
        serializeEventRef(cfgblock.rst_evt, doutChCfg.rstEventRef(), remoteDevIds);

        addBlock(resArray, VI_OPCFG_BLOCK_TYPE_DOUT_GEN,
                 QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)});
    }

    void ViModuleMbConfigSerializer::parseDoutGen(ViModuleConfig &cfg, const t_vi_opcfg_block_dout_gen &cfgblock, unsigned int size, const QList<QString> &remoteDevIds, LQError &err) {
        if (size < VI_OPCFG_BLOCK_DOUT_GEN_SIZE_MIN) {
            /* Для совместимости со старым форматом настроек, не выдаем ошибку, если размер меньше
             * на 4, но и не разбираем их */
            if (size < (VI_OPCFG_BLOCK_DOUT_GEN_SIZE_MIN - 4)) {
                err = ViErrors::invalidRdCfgBlockDataSize();
            }
        } else {
            const ViDoutChannelType &type {getDoutChType(cfgblock.ch_type)};
            if (cfgblock.ch_num < cfg.vi().dout().availableChannelsCnt(type)) {
                ViDoutChannelConfig &doutChCfg {cfg.vi().dout().channel(type, cfgblock.ch_num)};
                doutChCfg.setEnabled(cfgblock.flags & VI_OPCFG_DOUT_FLAG_EN);
                doutChCfg.setNormalOpState(cfgblock.flags & VI_OPCFG_DOUT_FLAG_NORM_ENERGIZED ?
                                               ViDoutNormalOpStates::normEnergized() : ViDoutNormalOpStates::normDeEnergized());
                doutChCfg.setInitialState(cfgblock.flags & VI_OPCFG_DOUT_FLAG_INITIAL_ALARM);
                doutChCfg.setLatching(cfgblock.flags & VI_OPCFG_DOUT_FLAG_LATCHING);
                doutChCfg.setManualControlEnabled(cfgblock.flags & VI_OPCFG_DOUT_FLAG_MANUAL);
                doutChCfg.setOnDelay(cfgblock.on_delay);
                doutChCfg.setOffDelay(cfgblock.off_delay);
                parseEventRef(doutChCfg.ctlEventRef(), cfgblock.ctl_evt, remoteDevIds);
                parseEventRef(doutChCfg.rstEventRef(), cfgblock.rst_evt, remoteDevIds);
            }
        }
    }

    void ViModuleMbConfigSerializer::serializeDoutName(QByteArray &resArray, const ViDoutChannelConfig &doutChCfg) {
        if (!doutChCfg.customName().isEmpty()) {
            t_vi_opcfg_block_dout_name cfgblock;
            memset(&cfgblock, 0, sizeof(cfgblock));

            cfgblock.ch_num = doutChCfg.channelNumber();
            cfgblock.ch_type = getDoutChTypeCode(doutChCfg.type());
            addBlock(resArray, VI_OPCFG_BLOCK_TYPE_DOUT_NAME,
                     QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)} +
                         doutChCfg.customName().toUtf8());
        }
    }

    void ViModuleMbConfigSerializer::parseDoutName(ViModuleConfig &cfg, const t_vi_opcfg_block_dout_name &cfgblock, unsigned int size, LQError &err) {
        if (size < VI_OPCFG_BLOCK_DOUT_NAME_SIZE_MIN) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else {
            const ViDoutChannelType &type {getDoutChType(cfgblock.ch_type)};
            if (cfgblock.ch_num < cfg.vi().dout().availableChannelsCnt(type)) {
                ViDoutChannelConfig &doutChCfg {cfg.vi().dout().channel(type, cfgblock.ch_num)};
                const int str_len {static_cast<int>(size - sizeof(t_vi_opcfg_block_dout_name))};
                doutChCfg.setCustomName(QString::fromUtf8(reinterpret_cast<const char *>(cfgblock.text), str_len));
            }
        }
    }

    void ViModuleMbConfigSerializer::serializeWfGen(QByteArray &resArray, const ViWaveformSrcConfig &wfSrcCfg, QList<QString> &remoteDevIds) {
        t_vi_opcfg_block_wf_gen cfgblock;
        memset(&cfgblock, 0, sizeof(cfgblock));
        if (wfSrcCfg.isEnabled()) {
            cfgblock.flags |= VI_OPCFG_WF_FLAG_EN;
        }
        cfgblock.src_num = wfSrcCfg.sourceNumber();
        cfgblock.src_type = VI_OPCFG_WF_SRC_TYPE_EVENT;
        cfgblock.type = VI_OPCFG_WF_TYPE_TIME;
        cfgblock.ch_mask = wfSrcCfg.acqChannelMask();
        cfgblock.duration_ms = wfSrcCfg.durationMs();
        cfgblock.delay_ms = wfSrcCfg.delayMs();

        serializeEventRef(cfgblock.src.evt, wfSrcCfg.eventRef(), remoteDevIds);

        addBlock(resArray, VI_OPCFG_BLOCK_TYPE_WF_GEN,
                 QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)});
    }

    void ViModuleMbConfigSerializer::parseWfGen(ViModuleConfig &cfg, const t_vi_opcfg_block_wf_gen &cfgblock, unsigned int size, const QList<QString> &remoteDevIds, LQError &err) {
        if (size < VI_OPCFG_BLOCK_WF_GEN_SIZE_MIN) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else {
            if ((cfgblock.src_type == VI_OPCFG_WF_SRC_TYPE_EVENT)
                && (cfgblock.type == VI_OPCFG_WF_TYPE_TIME)) {
                ViWaveformSrcConfig *wfCfg {cfg.vi().wf().addWfSrc()};
                wfCfg->setSourceNumber(cfgblock.src_num);
                wfCfg->setEnabled(cfgblock.flags & VI_OPCFG_WF_FLAG_EN);
                wfCfg->setAcqChannelMask(cfgblock.ch_mask);
                wfCfg->setDurationMs(cfgblock.duration_ms);
                wfCfg->setDelayMs(cfgblock.delay_ms);
                parseEventRef(wfCfg->eventRef(), cfgblock.src.evt, remoteDevIds);
            }
        }
    }

    void ViModuleMbConfigSerializer::serializeWfName(QByteArray &resArray, const ViWaveformSrcConfig &wfSrcCfg) {
        if (!wfSrcCfg.customName().isEmpty()) {
            t_vi_opcfg_block_wf_name cfgblock;
            memset(&cfgblock, 0, sizeof(cfgblock));

            cfgblock.src_num = wfSrcCfg.sourceNumber();
            addBlock(resArray, VI_OPCFG_BLOCK_TYPE_WF_NAME,
                     QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)} +
                         wfSrcCfg.customName().toUtf8());
        }
    }

    void ViModuleMbConfigSerializer::parseWfName(ViModuleConfig &cfg, const t_vi_opcfg_block_wf_name &cfgblock, unsigned int size, LQError &err) {
        if (size < VI_OPCFG_BLOCK_WF_NAME_SIZE_MIN) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else {
            ViWaveformSrcConfig *wfSrcCfg {cfg.vi().wf().wfSrcCfg(cfgblock.src_num)};
            if (wfSrcCfg) {
                const int str_len {static_cast<int>(size - sizeof(t_vi_opcfg_block_wf_name))};
                wfSrcCfg->setCustomName(QString::fromUtf8(reinterpret_cast<const char *>(cfgblock.text), str_len));
            }
        }
    }

    void ViModuleMbConfigSerializer::serializeExchGen(QByteArray &resArray, const ViExchConfig &exchCfg) {
        t_vi_opcfg_block_exch_gen cfgblock;
        memset(&cfgblock, 0, sizeof(cfgblock));
        if (exchCfg.pubIsEnabled()) {
            cfgblock.flags |= VI_OPCFG_EXCH_FLAG_PUB_EN;
            if (!exchCfg.viConfig().customDevName().isEmpty()) {
                cfgblock.pubid_type = VI_OPCFG_EXCH_PUBID_TYPE_DEVNAME;
            } else {
                cfgblock.pubid_type = VI_OPCFG_EXCH_PUBID_TYPE_NETNAME;
            }
        }
        addBlock(resArray, VI_OPCFG_BLOCK_TYPE_EXCH_GEN, QByteArray{reinterpret_cast<const char *>(&cfgblock), sizeof(cfgblock)});
    }

    void ViModuleMbConfigSerializer::parseExchGen(ViModuleConfig &cfg, const t_vi_opcfg_block_exch_gen &cfgblock, unsigned int size, LQError &err) {
        if (size < VI_OPCFG_BLOCK_ECH_GEN_SIZE_MIN) {
            err = ViErrors::invalidRdCfgBlockDataSize();
        } else {
            ViExchConfig &exchCfg {cfg.vi().exch()};
            exchCfg.setPubIsEnabled(cfgblock.flags & VI_OPCFG_EXCH_FLAG_PUB_EN);
        }
    }

    void ViModuleMbConfigSerializer::serializeRefHdr(t_vi_opcfg_ref_hdr &refhdr, const ViConfigRef &ref, QList<QString> &remoteDevIds) {
        if (ref.isValid()) {
            refhdr.flags |= VI_OPCFG_REF_FLAG_VALID;

            if (!ref.deviceIsLocal()) {
                QString devid {ref.deviceExchangeId()};
                if (devid.isEmpty()) {
                    refhdr.flags &= ~VI_OPCFG_REF_FLAG_VALID;
                } else  {
                    int devid_idx {remoteDevIds.indexOf(devid)};
                    if (devid_idx < 0) {
                        devid_idx = remoteDevIds.size();
                        remoteDevIds += devid;
                    }

                    refhdr.flags |= VI_OPCFG_REF_FLAG_DEV_REMOTE;
                    refhdr.dev_idx = devid_idx;
                }
            }
        }
    }

    void ViModuleMbConfigSerializer::parseRefHdr(ViConfigRef &ref, const t_vi_opcfg_ref_hdr &refhdr, const QList<QString> &remoteDevIds) {
        ref.setValid(refhdr.flags & VI_OPCFG_REF_FLAG_VALID);
        if (ref.isValid()) {
            if (refhdr.flags & VI_OPCFG_REF_FLAG_DEV_REMOTE) {
                QString devid;
                if (refhdr.dev_idx < remoteDevIds.size()) {
                    devid = remoteDevIds.at(refhdr.dev_idx);
                }
                if (!devid.isEmpty()) {
                    ref.setDeviceId(devid);
                } else {
                    ref.setValid(false);
                }
            }
        }
    }

    void ViModuleMbConfigSerializer::serializeEventRef(t_vi_opcfg_evt_ref &refblock, const ViConfigRefEvent &evtRef, QList<QString> &remoteDevIds) {
        serializeRefHdr(refblock.hdr, evtRef, remoteDevIds);
        if (evtRef.isValid()) {
            refblock.evt_num = evtRef.eventNum();
        }
    }

    void ViModuleMbConfigSerializer::parseEventRef(ViConfigRefEvent &evtRef, const t_vi_opcfg_evt_ref &refblock, const QList<QString> &remoteDevIds) {
        parseRefHdr(evtRef, refblock.hdr, remoteDevIds);
        if (evtRef.isValid()) {
            evtRef.setEventNum(refblock.evt_num);
        }
    }

    void ViModuleMbConfigSerializer::serializeDinRef(t_vi_opcfg_din_ref &refblock, const ViConfigRefDin &dinRef, QList<QString> &remoteDevIds) {
        serializeRefHdr(refblock.hdr, dinRef, remoteDevIds);
        if (dinRef.isValid()) {
            refblock.din_num = dinRef.dinChNum();
        }
    }

    void ViModuleMbConfigSerializer::parseDinRef(ViConfigRefDin &dinRef, const t_vi_opcfg_din_ref &refblock, const QList<QString> &remoteDevIds) {
        parseRefHdr(dinRef, refblock.hdr, remoteDevIds);
        if (dinRef.isValid()) {
            dinRef.setDinChNum(refblock.din_num);
        }
    }

    void ViModuleMbConfigSerializer::serializeAcqChRef(t_vi_opcfg_ch_ref &refblock, const ViConfigRefAcqCh &acqChRef, QList<QString> &remoteDevIds) {
        serializeRefHdr(refblock.hdr, acqChRef, remoteDevIds);
        if (acqChRef.isValid()) {
            refblock.ch_num = acqChRef.acqChNum();
            refblock.ch_type = getAcqChTypeCode(acqChRef.acqChType());
        }
    }

    void ViModuleMbConfigSerializer::parseAcqChRef(ViConfigRefAcqCh &acqChRef, const t_vi_opcfg_ch_ref &refblock, const QList<QString> &remoteDevIds)  {
        parseRefHdr(acqChRef, refblock.hdr, remoteDevIds);
        if (acqChRef.isValid()) {
            acqChRef.setAcqChNum(refblock.ch_num);
            acqChRef.setAcqChType(getAcqChType(refblock.ch_type));
        }
    }

    void ViModuleMbConfigSerializer::serializeMeasRef(t_vi_opcfg_meas_ref &refblock, const ViConfigRefMeas &measRef, QList<QString> &remoteDevIds) {
        serializeRefHdr(refblock.hdr, measRef, remoteDevIds);
        if (measRef.isValid()) {
            refblock.ch_num = measRef.acqChNum() & 0xFF;
            refblock.meas_num = measRef.measNum() & 0xFF;
            refblock.meas_type = getMeasTypeCode(measRef.measType());
        }
    }

    void ViModuleMbConfigSerializer::parseMeasRef(ViConfigRefMeas &measRef, const t_vi_opcfg_meas_ref &refblock, const QList<QString> &remoteDevIds) {
        parseRefHdr(measRef, refblock.hdr, remoteDevIds);
        if (measRef.isValid()) {
            measRef.setAcqChNum(refblock.ch_num);
            measRef.setMeasNum(refblock.meas_num);
            measRef.setMeasType(getMeasType(refblock.meas_type));
        }
    }
}

