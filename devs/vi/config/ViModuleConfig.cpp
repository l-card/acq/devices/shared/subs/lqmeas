#include "ViModuleConfig.h"
#include "ViConfig.h"

namespace LQMeas {
    const QString &ViModuleConfig::typeConfigName() {
        static const QString str {QStringLiteral("VI")};
        return str;
    }

    ViModuleConfig::ViModuleConfig() : m_vi{new ViConfig{}} {
        init();
    }

    ViModuleConfig::ViModuleConfig(const ViModuleConfig &cfg) : m_vi{new ViConfig{cfg.vi()}} {
        init();
    }

    ViModuleConfig::~ViModuleConfig() {

    }


}
