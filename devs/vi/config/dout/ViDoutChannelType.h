#ifndef LQMEAS_VIDOUTCHANNELTYPE_H
#define LQMEAS_VIDOUTCHANNELTYPE_H

#include "lqmeas/EnumNamedValue.h"
#include <QObject>
#include <QList>

namespace LQMeas {
    class ViDoutChannelType : public EnumNamedValue<QString> {
    public:
        virtual QString displayGroupName() const = 0;
    };

    class ViDoutChannelTypes : public QObject {
        Q_OBJECT
    public:
        static const ViDoutChannelType &relayLP();
        static const ViDoutChannelType &relayHP();

        static const QList<const ViDoutChannelType *> &all();
    };
}

Q_DECLARE_METATYPE(const LQMeas::ViDoutChannelType *);

#endif // LQMEAS_VIDOUTCHANNELTYPE_H
