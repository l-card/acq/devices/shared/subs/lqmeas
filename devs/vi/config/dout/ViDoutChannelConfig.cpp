#include "ViDoutChannelConfig.h"
#include "ViDoutConfig.h"
#include "ViDoutChannelType.h"
#include "ViDoutNormalOpState.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefEvent.h"
#include "lqmeas/devs/vi/config/ViConfig.h"
#include "lqmeas/devs/vi/config/refs/ViConfigRefCtl.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>
#include <QStringBuilder>

namespace LQMeas {
    static const QLatin1String &cfgkey_ch_type            {StdConfigKeys::type()};
    static const QLatin1String cfgkey_ch_norm_opstate     {"NormOpState"};
    static const QLatin1String cfgkey_ch_manual_ctl       {"ManualCtl"};
    static const QLatin1String cfgkey_ch_initial_state    {"InitialState"};
    static const QLatin1String cfgkey_ch_latching         {"Latching"};
    static const QLatin1String cfgkey_ch_on_delay         {"OnDelay"};
    static const QLatin1String cfgkey_ch_off_delay        {"OffDelay"};
    static const QLatin1String cfgkey_ch_ctl_evt_gr       {"CtlEvt"};
    static const QLatin1String cfgkey_ch_rst_evt_gr       {"RstEvt"};


    ViDoutChannelConfig::ViDoutChannelConfig(ViDoutConfig &doutCfg,
                                             const ViDoutChannelType &type, int ch_num) :
        ViChannelConfig{doutCfg.viConfig(), &doutCfg, defaultIsEnabled()},
        m_doutCfg{&doutCfg},
        m_type{&type}, m_ch_num{ch_num},
        m_ctlEvtRef{new ViConfigRefEvent{}},
        m_rstEvtRef{new ViConfigRefEvent{}} {

        addSubItem(m_ctlEvtRef.get());
        addSubItem(m_rstEvtRef.get());

        doutCfg.viConfig().refs().addRef(*m_ctlEvtRef);
        doutCfg.viConfig().refs().addRef(*m_rstEvtRef);
    }

    ViDoutChannelConfig::ViDoutChannelConfig(ViDoutConfig &doutCfg, const ViDoutChannelConfig &origDoutCfg) :
        ViChannelConfig{origDoutCfg, doutCfg.viConfig(), &doutCfg}, m_doutCfg{&doutCfg},
        m_type{origDoutCfg.m_type},
        m_ch_num{origDoutCfg.m_ch_num},
        m_normalOpState{origDoutCfg.m_normalOpState},
        m_manual_ctl_en{origDoutCfg.m_manual_ctl_en},
        m_initial_state{origDoutCfg.m_initial_state},
        m_latching{origDoutCfg.m_latching},
        m_on_delay{origDoutCfg.m_on_delay},
        m_off_delay{origDoutCfg.m_off_delay},
        m_ctlEvtRef{new ViConfigRefEvent{*origDoutCfg.m_ctlEvtRef}},
        m_rstEvtRef{new ViConfigRefEvent{*origDoutCfg.m_rstEvtRef}} {


        addSubItem(m_ctlEvtRef.get());
        addSubItem(m_rstEvtRef.get());

        doutCfg.viConfig().refs().addRef(*m_ctlEvtRef);
        doutCfg.viConfig().refs().addRef(*m_rstEvtRef);
    }

    ViDoutChannelConfig::~ViDoutChannelConfig() {
        m_doutCfg->viConfig().refs().removeRef(*m_ctlEvtRef);
        m_doutCfg->viConfig().refs().removeRef(*m_rstEvtRef);
    }

    QString ViDoutChannelConfig::origName() const {
        return m_type->displayName() % QChar(' ') %  QString::number(channelNumber() + 1);
    }

    void ViDoutChannelConfig::setNormalOpState(const ViDoutNormalOpState &state) {
        if (&state != m_normalOpState) {
            m_normalOpState = &state;
            notifyConfigChanged();
        }
    }

    void ViDoutChannelConfig::setManualControlEnabled(bool en) {
        if (en != m_manual_ctl_en) {
            m_manual_ctl_en = en;
            notifyConfigChanged();
        }
    }

    void ViDoutChannelConfig::setInitialState(bool state) {
        if (state != m_initial_state) {
            m_initial_state = state;
            notifyConfigChanged();
        }
    }

    void ViDoutChannelConfig::setLatching(bool on) {
        if (on != m_latching) {
            m_latching = on;
            notifyConfigChanged();
        }
    }

    void ViDoutChannelConfig::setOnDelay(int delay) {
        if (delay != m_on_delay) {
            m_on_delay = delay;
            notifyConfigChanged();
        }
    }

    void ViDoutChannelConfig::setOffDelay(int delay) {
        if (delay != m_off_delay) {
            m_off_delay = delay;
            notifyConfigChanged();
        }
    }

    const ViDoutNormalOpState &ViDoutChannelConfig::defaultNormalOpState() {
        return ViDoutNormalOpStates::normDeEnergized();
    }

    void ViDoutChannelConfig::chCfgSave(QJsonObject &cfgObj) const {
        cfgObj[cfgkey_ch_norm_opstate] = m_normalOpState->id();
        cfgObj[cfgkey_ch_manual_ctl] = m_manual_ctl_en;
        cfgObj[cfgkey_ch_initial_state] = m_initial_state;        
        cfgObj[cfgkey_ch_latching] = m_latching;
        cfgObj[cfgkey_ch_on_delay] = m_on_delay;
        cfgObj[cfgkey_ch_off_delay] = m_off_delay;
        saveSubItem(cfgObj, *m_ctlEvtRef, cfgkey_ch_ctl_evt_gr);
        saveSubItem(cfgObj, *m_rstEvtRef, cfgkey_ch_rst_evt_gr);
    }

    void ViDoutChannelConfig::chCfgLoad(const QJsonObject &cfgObj) {
        const QString &normOpStateId {cfgObj[cfgkey_ch_norm_opstate].toString()};
        const QList<const ViDoutNormalOpState *>  &states {ViDoutNormalOpStates::all()};
        const auto &it {std::find_if(states.cbegin(), states.cend(),
                                    [&normOpStateId](const ViDoutNormalOpState *outState) {
                                        return outState->id() == normOpStateId;
        })};
        m_normalOpState = it != states.cend() ? *it : &defaultNormalOpState();

        m_manual_ctl_en = cfgObj[cfgkey_ch_manual_ctl].toBool(defaultManualCtl());
        m_initial_state = cfgObj[cfgkey_ch_initial_state].toBool(defaultInitialState());
        m_latching = cfgObj[cfgkey_ch_latching].toBool(defaultIsLatching());
        m_on_delay = cfgObj[cfgkey_ch_on_delay].toInt(defaultOnDelay());
        m_off_delay = cfgObj[cfgkey_ch_off_delay].toInt(defaultOffDelay());
        loadSubItem(cfgObj, *m_ctlEvtRef, cfgkey_ch_ctl_evt_gr);
        loadSubItem(cfgObj, *m_rstEvtRef, cfgkey_ch_rst_evt_gr);
    }
}
