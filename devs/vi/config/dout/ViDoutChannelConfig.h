#ifndef LQMEAS_VIDOUTCHANNELCONFIG_H
#define LQMEAS_VIDOUTCHANNELCONFIG_H

#include "lqmeas/devs/vi/config/ViChannelConfig.h"
#include <memory>

namespace LQMeas {
    class ViDoutChannelType;
    class ViDoutConfig;
    class ViDoutNormalOpState;
    class ViConfigRefEvent;

    class ViDoutChannelConfig : public ViChannelConfig {
        Q_OBJECT
    public:
        ViDoutChannelConfig(ViDoutConfig &doutCfg, const ViDoutChannelType &type, int ch_num);
        ViDoutChannelConfig(ViDoutConfig &doutCfg, const ViDoutChannelConfig &origDoutCfg);
        ~ViDoutChannelConfig() override;

        const ViDoutConfig &dout() const {return *m_doutCfg;}
        ViDoutConfig &dout() {return *m_doutCfg;}

        const ViDoutChannelType &type() const {return *m_type;}

        int channelNumber() const override {return m_ch_num;}
        QString origName() const override;

        const ViDoutNormalOpState &normalOpState() const {return *m_normalOpState;}
        void setNormalOpState(const ViDoutNormalOpState &state);

        bool isManualControlEnabled() const {return m_manual_ctl_en;}
        void setManualControlEnabled(bool en);

        bool initialState() const {return m_initial_state;}
        void setInitialState(bool state);

        bool isLatching() const {return m_latching;}
        void setLatching(bool on);

        int onDelay() const {return m_on_delay;}
        void setOnDelay(int delay);

        int offDelay() const {return m_off_delay;}
        void setOffDelay(int delay);

        static constexpr int defaultOnDelay() {return 1000;}
        static constexpr int defaultOffDelay() {return 1000;}
        static constexpr bool defaultIsEnabled() {return false;}
        static constexpr bool defaultIsLatching() {return false;}
        static constexpr bool defaultInitialState() {return false;}
        static constexpr bool defaultManualCtl() {return false;}
        static const ViDoutNormalOpState &defaultNormalOpState();

        const ViConfigRefEvent &ctlEventRef() const {return *m_ctlEvtRef;}
        ViConfigRefEvent &ctlEventRef() {return *m_ctlEvtRef;}

        const ViConfigRefEvent &rstEventRef() const {return *m_rstEvtRef;}
        ViConfigRefEvent &rstEventRef() {return *m_rstEvtRef;}
    protected:
        void chCfgSave(QJsonObject &cfgObj) const override;
        void chCfgLoad(const QJsonObject &cfgObj) override;
    private:
        ViDoutConfig *m_doutCfg;

        int m_ch_num;
        const ViDoutChannelType *m_type;

        const ViDoutNormalOpState *m_normalOpState {&defaultNormalOpState()};
        bool m_initial_state {defaultInitialState()};
        bool m_manual_ctl_en {defaultManualCtl()};
        bool m_latching {defaultIsLatching()};
        int m_on_delay {defaultOnDelay()};
        int m_off_delay {defaultOffDelay()};
        std::unique_ptr<ViConfigRefEvent> m_ctlEvtRef;
        std::unique_ptr<ViConfigRefEvent> m_rstEvtRef;
    };
};

#endif // LQMEAS_VIDOUTCHANNELCONFIG_H
