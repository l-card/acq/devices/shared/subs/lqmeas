#ifndef LQMEAS_VIDOUTNORMALOPSTATE_H
#define LQMEAS_VIDOUTNORMALOPSTATE_H

#include "lqmeas/EnumNamedValue.h"
#include <QObject>
#include <QList>

namespace LQMeas {
    class ViDoutNormalOpState : public EnumNamedValue<QString> {
    public:

    };

    class ViDoutNormalOpStates : public QObject {
        Q_OBJECT
    public:
        static const ViDoutNormalOpState &normDeEnergized();
        static const ViDoutNormalOpState &normEnergized();

        static const QList<const ViDoutNormalOpState *> &all();
    };
}

Q_DECLARE_METATYPE(const LQMeas::ViDoutNormalOpState *);

#endif // LQMEAS_VIDOUTNORMALOPSTATE_H
