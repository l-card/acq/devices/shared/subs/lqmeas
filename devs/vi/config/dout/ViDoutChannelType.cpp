#include "ViDoutChannelType.h"

namespace LQMeas {
const ViDoutChannelType &LQMeas::ViDoutChannelTypes::relayLP() {
        static const class ViDoutChannelTypeLP : public ViDoutChannelType {
        public:
            QString id() const override {return QStringLiteral("vi_douttype_lp");}
            QString displayName() const override {return LQMeas::ViDoutChannelTypes::tr("Low Power Relay");}
            QString displayGroupName() const override {return LQMeas::ViDoutChannelTypes::tr("Low Power Relays");}
        } t;
        return t;
    }

    const ViDoutChannelType &ViDoutChannelTypes::relayHP() {
        static const class ViDoutChannelTypeHP : public ViDoutChannelType {
        public:
            QString id() const override {return QStringLiteral("vi_douttype_hp");}
            QString displayName() const override {return LQMeas::ViDoutChannelTypes::tr("High Power Relay");}
            QString displayGroupName() const override {return LQMeas::ViDoutChannelTypes::tr("High Power Relays");}
        } t;
        return t;
    }

    const QList<const ViDoutChannelType *> &ViDoutChannelTypes::all() {
        static const QList<const ViDoutChannelType *> list {
            &relayLP(),
            &relayHP()
        };
        return list;
    }
}
