#include "ViDoutNormalOpState.h"

namespace LQMeas {
    const ViDoutNormalOpState &LQMeas::ViDoutNormalOpStates::normDeEnergized() {
        static const class ViDoutNormalOpStateNormallyDeEnergized : public ViDoutNormalOpState {
        public:
            QString id() const override {return QStringLiteral("vi_dout_notmstate_den");}
            QString displayName() const override {return LQMeas::ViDoutNormalOpStates::tr("Normally De-Energized");}
        } t;
        return t;
    }

    const ViDoutNormalOpState &LQMeas::ViDoutNormalOpStates::normEnergized() {
        static const class ViDoutNormalOpStateNormallyEnergized : public ViDoutNormalOpState {
        public:
            QString id() const override {return QStringLiteral("vi_dout_notmstate_en");}
            QString displayName() const override {return LQMeas::ViDoutNormalOpStates::tr("Normally Energized");}
        } t;
        return t;
    }

    const QList<const ViDoutNormalOpState *> &ViDoutNormalOpStates::all() {
        static const QList<const ViDoutNormalOpState *> list {
            &normDeEnergized(),
            &normEnergized()
        };
        return list;
    }
}
