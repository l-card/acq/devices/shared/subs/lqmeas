#ifndef LQMEAS_VIDOUTCONFIG_H
#define LQMEAS_VIDOUTCONFIG_H

#include "lqmeas/devs/vi/config/ViConfigItem.h"
#include <QList>

namespace LQMeas {
    class ViConfig;
    class ViDoutChannelConfig;
    class ViDoutChannelType;

    class ViDoutConfig : public ViConfigItem {
        Q_OBJECT
    public:
        explicit ViDoutConfig(ViConfig &viConfig);
        explicit ViDoutConfig(ViConfig &viConfig, const ViDoutConfig &origDoutCfg);

        ViConfig &viConfig() const {return *m_viCfg;}


        int availableHpChannelsCnt() const {return m_hp_ch_cnt;}
        int availableLpChannelsCnt() const {return m_lp_ch_cnt;}

        int availableChannelsCnt(const ViDoutChannelType &type) const;

        const QList<ViDoutChannelConfig *> &channels(const ViDoutChannelType &type);
        const QList<const ViDoutChannelConfig *> &channels(const ViDoutChannelType &type) const;
        ViDoutChannelConfig &channel(const ViDoutChannelType &type, int ch_num);
        const ViDoutChannelConfig &channel(const ViDoutChannelType &type, int ch_num) const;

        void cleanup();
    protected:
        void protSave(QJsonObject &cfgObj) const override;
        void protLoad(const QJsonObject &cfgObj) override;
        void viModuleInfoUpdate(const ViModuleTypeInfo &viInfo) override;
    private:
        ViConfig *m_viCfg;
        QList<ViDoutChannelConfig *> m_hp_channels;
        int m_hp_ch_cnt {0};
        QList<ViDoutChannelConfig *> m_lp_channels;
        int m_lp_ch_cnt {0};
    };
}

#endif // LQMEAS_VIDOUTCONFIG_H
