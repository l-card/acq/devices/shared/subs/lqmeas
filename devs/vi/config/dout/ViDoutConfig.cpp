#include "ViDoutConfig.h"
#include "../ViConfig.h"
#include "ViDoutChannelConfig.h"
#include "ViDoutChannelType.h"
#include "lqmeas/devs/vi/modules/ViModuleTypeInfo.h"
#include <QJsonArray>
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_acq_chs_hp_arr  {"hp"};
    static const QLatin1String cfgkey_acq_chs_lp_arr  {"lp"};

    ViDoutConfig::ViDoutConfig(ViConfig &viConfig) : ViConfigItem{&viConfig}, m_viCfg{&viConfig} {

    }

    ViDoutConfig::ViDoutConfig(ViConfig &viConfig, const ViDoutConfig &origDoutCfg)
        : ViConfigItem{&viConfig}, m_viCfg{&viConfig},
        m_hp_ch_cnt{origDoutCfg.m_hp_ch_cnt},
        m_lp_ch_cnt{origDoutCfg.m_lp_ch_cnt} {

        for (const ViDoutChannelConfig *other_ch : origDoutCfg.m_hp_channels) {
            ViDoutChannelConfig *ch{new ViDoutChannelConfig{*this, *other_ch}};
            addSubItem(ch);
            m_hp_channels += ch;
        }

        for (const ViDoutChannelConfig *other_ch : origDoutCfg.m_lp_channels) {
            ViDoutChannelConfig *ch{new ViDoutChannelConfig{*this, *other_ch}};
            addSubItem(ch);
            m_lp_channels += ch;
        }
    }

    int ViDoutConfig::availableChannelsCnt(const ViDoutChannelType &type) const {
        return  &type == &ViDoutChannelTypes::relayHP() ? m_hp_ch_cnt : m_lp_ch_cnt;
    }

    const QList<ViDoutChannelConfig *> &ViDoutConfig::channels(const ViDoutChannelType &type)     {
        return  &type == &ViDoutChannelTypes::relayHP() ? m_hp_channels : m_lp_channels;
    }

    const QList<const ViDoutChannelConfig *> &ViDoutConfig::channels(const ViDoutChannelType &type) const {
        return  reinterpret_cast<const QList<const ViDoutChannelConfig *> &>(
            &type == &ViDoutChannelTypes::relayHP() ? m_hp_channels : m_lp_channels);
    }

    ViDoutChannelConfig &ViDoutConfig::channel(const ViDoutChannelType &type, int ch_num) {
        const QList<ViDoutChannelConfig *> chs {channels(type)};
        return *chs.at(ch_num); /** @todo возможно лучше явно сделать поиск по номеру */
    }

    const ViDoutChannelConfig &ViDoutConfig::channel(const ViDoutChannelType &type, int ch_num) const {
        const QList<const ViDoutChannelConfig *> chs {channels(type)};
        return *chs.at(ch_num); /** @todo возможно лучше явно сделать поиск по номеру */
    }

    void ViDoutConfig::cleanup() {
        qDeleteAll(m_hp_channels);
        m_hp_channels.clear();
        qDeleteAll(m_lp_channels);
        m_lp_channels.clear();
    }

    void ViDoutConfig::protSave(QJsonObject &cfgObj) const {
        {
            QJsonArray hpChArray;
            for (const ViDoutChannelConfig *ch : m_hp_channels) {
                QJsonObject chObj;
                ch->save(chObj);
                hpChArray.append(chObj);
            }
            cfgObj[cfgkey_acq_chs_hp_arr] = hpChArray;
        }

        {
            QJsonArray lpChArray;
            for (const ViDoutChannelConfig *ch : m_lp_channels) {
                QJsonObject chObj;
                ch->save(chObj);
                lpChArray.append(chObj);
            }
            cfgObj[cfgkey_acq_chs_lp_arr] = lpChArray;
        }
    }

    void ViDoutConfig::protLoad(const QJsonObject &cfgObj) {
        {
            const QJsonArray &hpChArray {cfgObj[cfgkey_acq_chs_hp_arr].toArray()};
            int hp_ch_idx {0};
            for (const auto &it : hpChArray) {
                if (hp_ch_idx < m_hp_channels.size()) {
                    m_hp_channels.at(hp_ch_idx)->load(it.toObject());
                } else {
                    ViDoutChannelConfig *ch{new ViDoutChannelConfig{*this, ViDoutChannelTypes::relayHP(), hp_ch_idx}};
                    ch->load(it.toObject());
                    addSubItem(ch);
                    m_hp_channels += ch;
                }
                ++hp_ch_idx;
            }
        }

        {
            const QJsonArray &lpChArray {cfgObj[cfgkey_acq_chs_lp_arr].toArray()};
            int lp_ch_idx {0};
            for (const auto &it : lpChArray) {
                if (lp_ch_idx < m_lp_channels.size()) {
                    m_lp_channels.at(lp_ch_idx)->load(it.toObject());
                } else {
                    ViDoutChannelConfig *ch{new ViDoutChannelConfig{*this, ViDoutChannelTypes::relayLP(), lp_ch_idx}};
                    ch->load(it.toObject());
                    addSubItem(ch);
                    m_lp_channels += ch;
                }
                ++lp_ch_idx;
            }
        }


    }

    void ViDoutConfig::viModuleInfoUpdate(const ViModuleTypeInfo &viInfo) {
        m_hp_ch_cnt = viInfo.doutHpChCnt();
        for (int ch_idx {m_hp_channels.size()}; ch_idx < m_hp_ch_cnt; ++ch_idx) {
            ViDoutChannelConfig *ch{new ViDoutChannelConfig{*this, ViDoutChannelTypes::relayHP(), ch_idx}};
            addSubItem(ch);
            m_hp_channels += ch;
        }

        m_lp_ch_cnt = viInfo.doutLpChCnt();
        for (int ch_idx {m_lp_channels.size()}; ch_idx < m_lp_ch_cnt; ++ch_idx) {
            ViDoutChannelConfig *ch{new ViDoutChannelConfig{*this, ViDoutChannelTypes::relayLP(), ch_idx}};
            addSubItem(ch);
            m_lp_channels += ch;
        }
    }
}
