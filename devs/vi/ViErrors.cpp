#include "ViErrors.h"
#include "lqmeas/devs/vi/protocol/vi_module_errs.h"


namespace LQMeas {
    LQError ViErrors::devError(int code) {
        static const QString err_type {QStringLiteral("vi_dev")};
        QString msg = tr("Device error: %1").arg(devErrorMsg(code));

        return LQError{code, msg, err_type};
    }

    LQError ViErrors::protoError(int code, const QString &msg) {
        static const QString err_type {QStringLiteral("vi_proto")};
        return LQError{code, msg, err_type};
    }

    LQError ViErrors::connectionParameters() {
        return protoError(-2, tr("Invalid connection parameters"));
    }

    LQError ViErrors::connectionCreate() {
        return protoError(-3, tr("Cannot setup connection with device"));
    }

    LQError ViErrors::connectionNotOpen() {
        return protoError(-4, tr("Connection with device is not opened"));
    }

    LQError ViErrors::connectionLost() {
        return protoError(-5, tr("Connection lost"));
    }

    LQError ViErrors::connectionReset() {
        return protoError(-6, tr("Connection was reset by another side"));
    }

    LQError ViErrors::invalidCmdRespFmt() {
        return protoError(-7, tr("Invalid command response format"));
    }

    LQError ViErrors::invalidCmdRespStatus() {
        return protoError(-8, tr("Invalid command response status"));
    }

    LQError ViErrors::insufCmdRespDataSize() {
        return protoError(-9, tr("Insufficient command response data size"));
    }

    LQError ViErrors::noActiveCfgExchange() {
        return protoError(-10, tr("No corresponding active configuration exchange"));
    }

    LQError ViErrors::noValidOpCfg() {
        return protoError(-11, tr("No valid configuration was detected in module"));
    }

    LQError ViErrors::invalidRdCfgBlockDataSize() {
        return protoError(-12, tr("Invalid read configuration block data size"));
    }

    LQError ViErrors::invalidRdCfgCRC() {
        return protoError(-13, tr("Invalid read configuration block data size"));
    }

    LQError ViErrors::opcfgParseMeasTypeMismatch() {
        return protoError(-14, tr("Measurements types mismatch on configuration parse"));
    }

    LQError ViErrors::opcfgWrCheckMismatch() {
        return protoError(-15, tr("Written configuration check data mismatch"));
    }

    LQError ViErrors::noLiveBitChange() {
        return protoError(-16, tr("Livebit change is not detected"));
    }

    LQError ViErrors::dataOverflow()  {
        return protoError(-17, tr("Stream data overflow"));
    }

    LQError ViErrors::dataRecvTout() {
        return protoError(-18, tr("Stream data receive timeout"));
    }

    LQError ViErrors::acqChNotEnabled(int ch_num) {
        return protoError(-19, tr("Data acquisition channel %1 is not enabled in module configuration").arg(ch_num));
    }



    QString ViErrors::devErrorMsg(int code) {
        switch (code) {
            case VI_MODULE_ERR_INVALID_WR_SEQ: return tr("Invalid control sequence");
            case VI_MODULE_ERR_INVALID_WR_SIGN: return tr("Invalid command signature");
            case VI_MODULE_ERR_INVALID_WR_SIZE: return tr("Invalid write sequence size");
            case VI_MODULE_ERR_INVALID_CRC: return tr("Invalid CRC");
            case VI_MODULE_ERR_INVALID_DATA_SIZE: return tr("Invalid command data size");
            case VI_MODULE_ERR_FLASH_WRITE: return tr("Flash write error");
            case VI_MODULE_ERR_INVALID_REG_BOUNDARY: return tr("Invalid Modbus register boundary cross");
            case VI_MODULE_ERR_WF_NOT_PRESENT: return tr("Waveform is not present");
            case VI_MODULE_ERR_WF_OUT_OF_DATA: return tr("Out of waveform data");
            case VI_MODULE_ERR_WF_INVALID_SIZE: return tr("Invalid waveform size requested");
            case VI_MODULE_ERR_WF_NO_DATA: return tr("No waveform data");
            case VI_MODULE_ERR_WF_INVALID_TYPE: return tr("Invalid wavefrom type");
            case VI_MODULE_ERR_WF_UNSUP_BY_MODULE_TYPE: return tr("Waveforms are not supported for this module type");
            case VI_MODULE_ERR_INVALID_CMD_CODE: return tr("Invalid command code");
            case VI_MODULE_ERR_NOT_READY: return tr("Device not ready to execute specified operation");
            case VI_MODULE_ERR_NOT_SUPPORT_IN_CUR_MODE: return tr("Specified operation is not supported in current device mode");
            case VI_MODULE_ERR_CBRD_MEM_NOT_DETECTED: return tr("Carrier board flash memory is not detected");
            case VI_MODULE_ERR_CBRD_MEM_EXCH_ERROR: return tr("Carrier board flash memory exchange error");
            case VI_MODULE_ERR_SPI_MEM_NOT_DETECTED: return tr("Mezzanine flash memory is not detected");
            case VI_MODULE_ERR_SPI_MEM_EXCH_ERROR: return tr("Mezzanine flash memory exchange error");
            case VI_MODULE_ERR_UNSUP_FORMAT_VER: return tr("Unsupported data format version");
            case VI_MODULE_ERR_INVALID_MODULE_TYPE: return tr("Invalid module type");
            case VI_MODULE_ERR_FLASH_DATA_WR_CMP: return tr("Written flash data comparsion error");
            case VI_MODULE_ERR_INVALID_NET_PORT_NUM: return tr("Invalid network port number");
            case VI_MODULE_ERR_NET_PORT_UNSUP_FEATURE: return tr("Unsupported network port feature");
            case VI_MODULE_ERR_SWITCH_NOT_DETECTED: return tr("Ethernet switch mcs is not detected");
            case VI_MODULE_ERR_INVALID_CMD_PARAM: return tr("Invalid command parameter value");
            case VI_MODULE_ERR_OP_NOT_STARTED: return tr("Specified operation is not started");
            case VI_MODULE_ERR_NET_PORT_INIT_ERR: return tr("Network port initialization error");
            case VI_MODULE_ERR_OPCFG_MODIFY_NOT_STARTED: return tr("Operational configuration modification is not started");
            case VI_MODULE_ERR_OPCFG_MODIFY_NOT_FINISHED: return tr("Operational configuration modification is not finished");
            case VI_MODULE_ERR_OPCFG_INVALID_ID: return tr("Invalid operational configuration identification");
            case VI_MODULE_ERR_OPCFG_INVALID_BLOCK_NUM: return tr("Invalid operational configuration block number");
            case VI_MODULE_ERR_OPCFG_SIZE_TOO_LARGE: return tr("Operational configuration size is too large");
            case VI_MODULE_ERR_OPCFG_CRC: return tr("Invalid operational configuration CRC");
            case VI_MODULE_ERR_OPCFG_NOT_VALID: return tr("Operational configuration is not valid");
            case VI_MODULE_ERR_OPCFG_INVALID_DATA_FMT: return tr("Invalid operational configuration data format");
            case VI_MODULE_ERR_OPCFG_UNKNOWN_MEASTYPE: return tr("Unknown operational configuration measurement type");
            default: return tr("Unknown error code %1").arg(code);
        }
    }
}


