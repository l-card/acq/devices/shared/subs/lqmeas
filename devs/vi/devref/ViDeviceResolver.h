#ifndef LQMEAS_VIDEVICERESOLVER_H
#define LQMEAS_VIDEVICERESOLVER_H

#include "lqmeas/devs/resolver/DevicesResolver.h"
namespace LQMeas {
    class ViDeviceResolver : public DevicesResolver {
    public:
        static ViDeviceResolver &resolver();

        QList<const NetAutodiscoveryServiceType *> netAutoDiscSvcTypes() const override;

    protected:
        QList<QSharedPointer<Device> > protGetDevList(const QList<const DeviceRef *> &devRefs, ResolveFlags flags) override;
    private:
        explicit ViDeviceResolver();

        QList<QSharedPointer<Device> > m_devs;
    };
}
#endif // LQMEAS_VIDEVICERESOLVER_H
