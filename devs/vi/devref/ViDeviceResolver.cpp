#include "ViDeviceResolver.h"
#include "ViDeviceRefIpAddr.h"
#include "ViDeviceRefNetSvc.h"
#include "lqmeas/devs/DeviceRef.h"
#include "lqmeas/devs/vi/modules/ViModule.h"
#include "lqmeas/devs/vi/netdiscovery/ViNetAutodiscoveryServiceType.h"
#include "lqmeas/devs/vi/netdiscovery/ViNetAutodiscoveryRecordInfo.h"
#include "lqmeas/devs/vi/connection/mb/ViModuleMbConnection.h"
#include "lqmeas/devs/resolver/netdiscovery/NetAutodiscoveryServerList.h"
#include "lqmeas/devs/resolver/netdiscovery/NetAutodiscoveryRecord.h"
#include "lqmeas_config.h"
#ifdef LQMEAS_DEV_LVIMS
#include "lqmeas/devs/vi/modules/lvims/LViMSNetAutodiscoveryServiceType.h"
#endif
#ifdef LQMEAS_DEV_VIB4
#include "lqmeas/devs/vi/modules/vib4/Vib4NetAutodiscoveryServiceType.h"
#endif

namespace LQMeas {
    ViDeviceResolver &LQMeas::ViDeviceResolver::resolver() {
        static ViDeviceResolver resolver;
        return resolver;
    }

    QList<const NetAutodiscoveryServiceType *> ViDeviceResolver::netAutoDiscSvcTypes() const {
        static const QList<const NetAutodiscoveryServiceType *> list {
#ifdef LQMEAS_DEV_LVIMS
            &LViMSNetAutodiscoveryServiceType::type(),
#endif
#ifdef LQMEAS_DEV_VIB4
            &Vib4NetAutodiscoveryServiceType::type(),
#endif
        };
        return list;
    }

    QList<QSharedPointer<LQMeas::Device> > LQMeas::ViDeviceResolver::protGetDevList(
        const QList<const DeviceRef *> &devRefs, ResolveFlags flags) {

        QList<QSharedPointer<Device> > newDevices;
        QList<QSharedPointer<Device> > oldDevices {m_devs};


        QList<QSharedPointer<NetAutodiscoveryRecord> >  records {NetAutodiscoveryServerList::instance().records()};
        for (const QSharedPointer<NetAutodiscoveryRecord> &rec : records) {
            if (netAutoDiscSvcTypes().contains(&rec->serviceType())) {
                const ViNetAutodiscoveryRecordInfo &viRecInfo {static_cast<const ViNetAutodiscoveryRecordInfo &>(rec->takeInfo())};
                QSharedPointer<Device> fndDev;

                const auto &it {std::find_if(oldDevices.cbegin(), oldDevices.cend(),
                                            [rec](const QSharedPointer<Device> &dev) {
                                                 ViModule *videv {qobject_cast<ViModule*>(dev.data())};
                                                 return videv && videv->netSvcRecordInfo() &&
                                                        videv->netSvcRecordInfo()->instanceName() == rec->instanceName();})};
                if (it != oldDevices.cend()) {
                    fndDev = *it;
                    /* обновляем информацию из записи */
                    ViModule *livmsModule {qobject_cast<ViModule*>(fndDev.data())};
                    if (livmsModule) {
                        livmsModule->updateAutodiscoveryInfo(viRecInfo);
                    }
                    oldDevices.removeOne(fndDev);
                } else {
                    /* если есть ссылка на это устройство, то берем параметры подключения
                     * из ссылки (ссылка может переназначать таймаут подключения) */
                    const ViDeviceRefNetSvc *fndRef = nullptr;
                    for (const DeviceRef *ref : devRefs) {
                        if (ref->refTypeID() == ViDeviceRefNetSvc::typeRefTypeID()) {
                            const ViDeviceRefNetSvc *svcRef {static_cast<const ViDeviceRefNetSvc *>(ref)};
                            if (svcRef->checkNetSvcRecord(viRecInfo)) {
                                fndRef = svcRef;
                                break;
                            }
                        }
                    }
                    fndDev = QSharedPointer<Device>{fndRef ? new ViModule{viRecInfo, *fndRef}
                                                           : new ViModule{viRecInfo, ViModuleMbConnection::OpenParams::defaultTout()}};

                }
                rec->releaseInfo();
                if (fndDev)
                    newDevices.append(fndDev);
            }
        }




        for (const DeviceRef *ref : devRefs) {
            if (!ref->isAutoResolved()) {
                if (ref->refTypeID() == ViDeviceRefIpAddr::typeRefTypeID()) {
                    const ViDeviceRefIpAddr *ipRef {static_cast<const ViDeviceRefIpAddr *>(ref)};
                    QSharedPointer<Device> fndDev;

                    const auto &it {std::find_if(oldDevices.cbegin(), oldDevices.cend(),
                                                [ref](const QSharedPointer<Device> &dev) {
                                                    return ref->checkDevice(*dev) != DeviceRef::DevCheckResult::BadDevice;})};
                    if (it != oldDevices.cend()) {
                        fndDev = *it;
                        oldDevices.removeOne(fndDev);
                    } else {
                        /* проверяем на всякий случай среди новых устройств, что это не
                             * повторная ссылка */
                        bool fnd_newDev {false};
                        if (!fndDev) {
                            const auto &it {std::find_if(newDevices.cbegin(), newDevices.cend(),
                                                        [ref](const QSharedPointer<Device> &dev) {
                                                            return ref->sameDeviceLocation(*dev);})};
                            fnd_newDev = it != newDevices.cend();
                        }

                        if (!fndDev && !fnd_newDev)
                            fndDev = QSharedPointer<Device>{new ViModule{*ipRef}};
                    }

                    if (fndDev)
                        newDevices.append(fndDev);
                }
            }
        }

        m_devs = newDevices;
        return newDevices;
    }

    ViDeviceResolver::ViDeviceResolver() {

    }
}
