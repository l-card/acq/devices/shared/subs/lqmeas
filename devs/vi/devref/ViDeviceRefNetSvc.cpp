#include "ViDeviceRefNetSvc.h"
#include "lqmeas/devs/vi/netdiscovery/ViNetAutodiscoveryRecordInfo.h"
#include "lqmeas/devs/vi/connection/mb/ViModuleMbConnection.h"
#include "lqmeas/devs/vi/modules/ViModule.h"
#include "lqmeas/devs/DeviceInfo.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/devs/DeviceRefMethod.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String& cfgkey_net_svc_group {StdConfigKeys::netSvc()};
    static const QLatin1String& cfgkey_instance_name {StdConfigKeys::instanceName()};
    static const QLatin1String& cfgkey_con_tout {StdConfigKeys::conTout()};

    ViDeviceRefNetSvc::ViDeviceRefNetSvc(const DeviceInfo &devinfo, const QString &instanceName, int conTout) :
        DeviceRef{devinfo, DeviceInterfaces::ethernet(), DeviceRefMethods::netName()},
        m_instanceName{instanceName},
        m_conTout{conTout} {

    }

    ViDeviceRefNetSvc::ViDeviceRefNetSvc(const ViDeviceRefNetSvc &ref) :
        DeviceRef{ref},
        m_instanceName{ref.m_instanceName},
        m_conTout{ref.m_conTout} {

    }

    ViDeviceRefNetSvc::ViDeviceRefNetSvc(const DeviceInfo &devinfo, const ViDeviceRefNetSvc &ref)  :
        DeviceRef{devinfo, DeviceInterfaces::ethernet(), DeviceRefMethods::netName()},
        m_instanceName{ref.m_instanceName},
        m_conTout{ref.m_conTout} {

    }

    const QString &ViDeviceRefNetSvc::typeRefTypeID() {
        static const QString str { QStringLiteral("vi_netsvc") };
        return str;
    }

    int ViDeviceRefNetSvc::defaultConnectionTimeout() {
        return ViModuleMbConnection::OpenParams::defaultTout();
    }

    DeviceRef::DevCheckResult ViDeviceRefNetSvc::checkDevice(const Device &device) const {
        DevCheckResult res {DevCheckResult::BadDevice};
        const ViModule *videv {qobject_cast<const ViModule *>(&device)};
        if (videv && videv->netSvcRecordInfo()) {
            const ViNetAutodiscoveryRecordInfo *rec {videv->netSvcRecordInfo()};
            if (checkNetSvcRecord(*rec)) {
                res = DeviceRef::checkDevice(device);
            }
        }
        return res;
    }

    bool ViDeviceRefNetSvc::sameDeviceLocation(const DeviceRef &ref) const {
        bool same {ref.refTypeID() == typeRefTypeID()};
        if (same) {
            const ViDeviceRefNetSvc &svcRef = static_cast<const ViDeviceRefNetSvc &>(ref);
            same = svcRef.instanceName() == m_instanceName;
        }
        return same;
    }

    bool ViDeviceRefNetSvc::sameDevice(const DeviceRef &ref) const {
        bool same {sameDeviceLocation(ref)};
        if (same) {
            same = DeviceRef::sameDevice(ref);
        }
        return  same;
    }

    void ViDeviceRefNetSvc::save(QJsonObject &refObj) const {
        DeviceRef::save(refObj);

        QJsonObject svcObj;
        svcObj[cfgkey_instance_name] = m_instanceName;
        svcObj[cfgkey_con_tout] = m_conTout;
        refObj[cfgkey_net_svc_group] = svcObj;
    }

    void ViDeviceRefNetSvc::load(const QJsonObject &refObj) {
        DeviceRef::load(refObj);

        const QJsonObject &svcObj {refObj[cfgkey_net_svc_group].toObject()};
        m_instanceName = svcObj[cfgkey_instance_name].toString();
        m_conTout = svcObj[cfgkey_con_tout].toInt();
    }

    DeviceRef *ViDeviceRefNetSvc::clone() const {
        return new ViDeviceRefNetSvc{*this};
    }

    QString ViDeviceRefNetSvc::displayName() const {
        return QString{"%1, %2 '%3'"}.arg(DeviceRef::displayName(),
                                          LQMeas::ViModule::tr("Net Name"),
                                          m_instanceName);
    }

    bool ViDeviceRefNetSvc::checkNetSvcRecord(const ViNetAutodiscoveryRecordInfo &rec) const {
        QString serial {devInfo().serial()};
        return (rec.instanceName() == m_instanceName) &&
               (serial.isEmpty() || (serial == rec.serial()));
    }

    QString ViDeviceRefNetSvc::protKey() const {
        return QString{"%1_netsvc%2"}.arg(DeviceRef::protKey(), m_instanceName);
    }
}
