#include "ViDeviceRefIpAddr.h"
#include "lqmeas/devs/DeviceInterface.h"
#include "lqmeas/devs/DeviceRefMethod.h"
#include "lqmeas/devs/vi/modules/ViModule.h"
#include "lqmeas/devs/vi/connection/mb/ViModuleMbConnection.h"
#include "lqmeas/StdConfigKeys.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String& cfgkey_ip_group  {StdConfigKeys::ip()};
    static const QLatin1String& cfgkey_ipv4_addr {StdConfigKeys::ipv4Addr()};
    static const QLatin1String& cfgkey_con_tout  {StdConfigKeys::conTout()};

    ViDeviceRefIpAddr::ViDeviceRefIpAddr(const DeviceInfo &devinfo, const QHostAddress &ipAddr, int conTout) :
        DeviceRef{devinfo, DeviceInterfaces::ethernet(), DeviceRefMethods::ipAddr()},
        m_ipAddr{ipAddr},
        m_conTout{conTout} {

    }

    ViDeviceRefIpAddr::ViDeviceRefIpAddr(const ViDeviceRefIpAddr &ipRef) :
        DeviceRef{ipRef},
        m_ipAddr{ipRef.m_ipAddr},
        m_conTout{ipRef.m_conTout} {

    }

    ViDeviceRefIpAddr::ViDeviceRefIpAddr(const DeviceInfo &devinfo, const ViDeviceRefIpAddr &ipRef) :
        DeviceRef{devinfo, DeviceInterfaces::ethernet(), DeviceRefMethods::ipAddr()},
        m_ipAddr{ipRef.m_ipAddr},
        m_conTout{ipRef.m_conTout} {

    }

    const QString &ViDeviceRefIpAddr::typeRefTypeID() {
        static const QString str { QStringLiteral("vi_ipaddr") };
        return str;
    }

    int ViDeviceRefIpAddr::defaultConnectionTimeout() {
        return ViModuleMbConnection::OpenParams::defaultTout();
    }

    DeviceRef::DevCheckResult ViDeviceRefIpAddr::checkDevice(const Device &device) const {
        DevCheckResult res = DevCheckResult::BadDevice;
        const ViModule *videv = qobject_cast<const ViModule *>(&device);
        if (videv) {
            if (videv->ipAddrRef()) {
                if (videv->ipAddrRef()->sameDevice(*this)) {
                    res = DeviceRef::checkDevice(device);
                }
            } else {
                QString conAddr {videv->connectionAddr()};
                if (conAddr == ipAddr().toString()) {
                    res = DeviceRef::checkDevice(device);
                }
            }
        }
        return res;
    }

    bool ViDeviceRefIpAddr::sameDeviceLocation(const Device &device) const  {
        bool res {false};
        const ViModule *videv = qobject_cast<const ViModule *>(&device);
        if (videv) {
            if (videv->ipAddrRef()) {
                res = sameDeviceLocation(*videv->ipAddrRef());
            } else {
                QString conAddr {videv->connectionAddr()};
                if (conAddr == ipAddr().toString()) {
                    res = true;
                }
            }
        }
        return res;
    }

    bool ViDeviceRefIpAddr::sameDeviceLocation(const DeviceRef &ref) const {
        bool same = ref.refTypeID() == typeRefTypeID();
        if (same) {
            const ViDeviceRefIpAddr &ipRef = static_cast<const ViDeviceRefIpAddr &>(ref);
            same = (ipRef.ipAddr() == ipAddr());
        }
        return  same;
    }

    bool ViDeviceRefIpAddr::sameDevice(const DeviceRef &ref) const {
        bool same {sameDeviceLocation(ref)};
        if (same) {
            same = DeviceRef::sameDevice(ref);
        }
        return  same;
    }

    void ViDeviceRefIpAddr::save(QJsonObject &refObj) const {
        DeviceRef::save(refObj);

        QJsonObject ipObj;
        ipObj[cfgkey_ipv4_addr] = m_ipAddr.toString();
        ipObj[cfgkey_con_tout] = m_conTout;

        refObj[cfgkey_ip_group] = ipObj;
    }

    void ViDeviceRefIpAddr::load(const QJsonObject &refObj) {
        DeviceRef::load(refObj);

        const QJsonObject &ipObj = refObj[cfgkey_ip_group].toObject();
        m_ipAddr = QHostAddress{ipObj[cfgkey_ipv4_addr].toString()};
        m_conTout = ipObj[cfgkey_con_tout].toInt(defaultConnectionTimeout());
    }

    DeviceRef *ViDeviceRefIpAddr::clone() const {
        return new ViDeviceRefIpAddr{*this};
    }

    QString ViDeviceRefIpAddr::displayName() const {
        return QString("%1, %2 %3").arg(DeviceRef::displayName(),  LQMeas::ViModule::tr("IP"), m_ipAddr.toString());
    }

    QString ViDeviceRefIpAddr::protKey() const {
        return QString("%1_ip%2").arg(DeviceRef::protKey(), m_ipAddr.toString());
    }
}
