#ifndef LQMEAS_VIDEVICEREFNETSVC_H
#define LQMEAS_VIDEVICEREFNETSVC_H

#include "lqmeas/devs/DeviceRef.h"


namespace LQMeas {
    class ViNetAutodiscoveryRecordInfo;

    class ViDeviceRefNetSvc : public DeviceRef {
    public:
        ViDeviceRefNetSvc(const DeviceInfo &devinfo, const QString &instanceName = QString{},
                            int conTout = defaultConnectionTimeout());
        ViDeviceRefNetSvc(const ViDeviceRefNetSvc &ref);
        ViDeviceRefNetSvc(const DeviceInfo &devinfo, const ViDeviceRefNetSvc &ref);


        static const QString &typeRefTypeID();
        QString refTypeID() const override {return typeRefTypeID();}

        const QString &instanceName() const {return m_instanceName;}
        int connectionTimeout() const {return m_conTout;}
        bool serialUsed() const override {return false;}

        void setInstanceName(const QString &name) {m_instanceName = name;}
        void setConnectionTimeout(int toutMs) {m_conTout = toutMs;}

        static int defaultConnectionTimeout();

        DevCheckResult checkDevice(const Device &device) const override;
        bool sameDeviceLocation(const DeviceRef &ref) const override;
        bool sameDevice(const DeviceRef &ref) const override;

        void save(QJsonObject &refObj) const override;
        void load(const QJsonObject &refObj) override;

        DeviceRef *clone() const override;
        QString displayName() const override;

        bool checkNetSvcRecord(const ViNetAutodiscoveryRecordInfo &rec) const;
    protected:
        QString protKey() const override;
    private:
        QString m_instanceName;
        int m_conTout;
    };
}

#endif // LQMEAS_VIDEVICEREFNETSVC_H
