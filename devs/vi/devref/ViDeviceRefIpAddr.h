#ifndef LQMEAS_VIDEVICEREFIPADDR_H
#define LQMEAS_VIDEVICEREFIPADDR_H

#include "lqmeas/devs/DeviceRef.h"
#include <QHostAddress>

namespace LQMeas {
    class ViDeviceRefIpAddr  : public DeviceRef {
    public:
        explicit ViDeviceRefIpAddr(const DeviceInfo &devinfo,
                                     const QHostAddress &ipAddr = QHostAddress{},
                                     int conTout = defaultConnectionTimeout());
        ViDeviceRefIpAddr(const ViDeviceRefIpAddr &ipRef);
        explicit ViDeviceRefIpAddr(const DeviceInfo &devinfo, const ViDeviceRefIpAddr &ipRef);

        static const QString &typeRefTypeID();
        QString refTypeID() const override {return typeRefTypeID();}
        bool serialUsed() const override {return false;}


        const QHostAddress &ipAddr() const {return m_ipAddr;}
        int connectionTimeout() const {return m_conTout;}

        static int defaultConnectionTimeout();

        void setIpAddr(const QHostAddress &addr) { m_ipAddr = addr; }
        void setConnectionTimeout(int toutMs) {m_conTout = toutMs;}

        DevCheckResult checkDevice(const Device &device) const override;
        bool sameDeviceLocation(const Device &device) const override;
        bool sameDeviceLocation(const DeviceRef &ref) const override;
        bool sameDevice(const DeviceRef &ref) const override;
        bool isAutoResolved() const override {return false;}

        void save(QJsonObject &refObj) const override;
        void load(const QJsonObject &refObj) override;

        DeviceRef *clone() const override;
        QString displayName() const override;
    protected:
        QString protKey() const override;
    private:
        QHostAddress m_ipAddr;
        int m_conTout;
    };
}

#endif // LQMEAS_VIDEVICEREFIPADDR_H
