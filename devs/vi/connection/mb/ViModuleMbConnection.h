#ifndef LQMEAS_VIMODULEMBCONNECTION_H
#define LQMEAS_VIMODULEMBCONNECTION_H

#include <QObject>
#include <QString>
#include <QMutex>
#include "lqmeas/devs/vi/protocol/vi_modbus_cmds.h"
#include "lqmeas/devs/vi/protocol/vi_module_state.h"
class LQError;
struct _modbus;
namespace LQMeas {
    class ViModuleInfo;
    class ViModuleConfig;
    class ViAcqChannelState;
    class ViModuleState;
    class ViDoutTypeState;
    class ViDoutChannelType;
    class ViModuleDiagnostic;
    class ViAcqChSetFlagReqData;
    class ViWfReqData;
    class ViDoutResetReqData;
    class ViDoutManualChangeReqData;
    class ViWfInfo;
    class ViWfChInfo;
    class ViWfChDataBlock;
    class ViIfaceSettings;
    class ViTimeStamp;
    class ViTimeClockId;


    class ViModuleMbConnection {
    public:
        class OpenParams {
        public:
            static constexpr quint16 defaultPort() {return 502;}
            static constexpr int defaultTout() {return 3000;}

            explicit OpenParams(const QString &ipAddr = QString{},
                                quint16 port = defaultPort(),
                                int tout = defaultTout()) :
                m_ip_addr{ipAddr}, m_port{port}, m_tout{tout} {
            }

            const QString &ipAddr() const {return m_ip_addr;}
            quint16 port() const {return m_port;}
            unsigned respTout() const {return m_tout;}
        private:
            QString m_ip_addr;
            quint16 m_port;
            int m_tout;
        };

        const QString &connectionAddr() const {return m_open_params.ipAddr();}

        void open(const OpenParams &params, LQError &err);
        void close();
        bool isOpened() const {return m_is_open;}



        void startBootMode(quint16 boot_port, LQError &err);
        void resetRequest(LQError &err);
        void nvConfigWrite(const ViModuleConfig &cfg, LQError &err);
        void nvConfigRead(ViModuleConfig &cfg, LQError &err);
        
        void ifaceSettingsRead(ViIfaceSettings &netset, LQError &err);
        void ifaceSettingsWrite(const ViIfaceSettings &netset, LQError &err);

        ViModuleInfo *readModuleInfo(LQError &err);
        ViTimeClockId *readModuleClockId(LQError &err);
        ViAcqChannelState *readAcqChState(int ch_num, LQError &err);
        ViModuleState *readModuleState(const ViModuleInfo &info, LQError &err);
        ViDoutTypeState *readDoutTypeState(const ViModuleInfo &info, const ViDoutChannelType &chType, LQError &err);
        ViModuleDiagnostic *readDiagnostic(LQError &err);
        ViWfInfo *readWfInfo(const ViModuleInfo &info, quint16 wf_id, LQError &err);
        ViWfChInfo *readWfChInfo(quint16 wf_id, quint16 ch_num, LQError &err);
        ViWfChDataBlock *readWfChData(quint16 wf_id, quint16 ch_num, quint32 offs, quint32 samles_cnt, LQError &err);



        void setAcqChStateFlag(const ViAcqChSetFlagReqData &reqData, LQError &err);
        void wfManualRequest(const ViWfReqData &reqData, quint16 &wf_id, LQError &err);
        void doutReset(const ViDoutResetReqData &reqData, LQError &err);
        void doutManualChange(const ViDoutManualChangeReqData &reqData, LQError &err);


        void execCmd(t_vi_mb_cmd_codes cmd, const QByteArray &txData, QByteArray &rx_data, LQError &err);
        void execCmd(t_vi_mb_cmd_codes cmd, const QByteArray &txData, LQError &err);
    private:
        void privClose();

        void opcfgWriteRaw(const QByteArray &opCfgData, quint16 &cfg_id, LQError &err);
        void opcfgReadRaw(QByteArray &opCfgData, LQError &err);

        void opcfgWriteStart(quint16 &crg_id, LQError &err);
        void opcfgWriteBlock(quint16 cfg_id, quint32 offs, const QByteArray &blockData, LQError &err);
        void opcfgWriteFinish(quint16 cfg_id, quint32 size, quint32 crc, LQError &err);
        void opcfgUpdate(quint16 cfg_id, LQError &err);
        void opcfgStore(quint16 cfg_id, LQError &err);

        void checkOpen(LQError &err);

        ViDoutTypeState *readDoutTypeStateRegs(unsigned addr, unsigned cnt, const ViModuleInfo &info, const ViDoutChannelType &chType, LQError &err);

        void getMbRegsErr(LQError &err);
        void regsRead(unsigned addr, unsigned cnt, quint16 *vals, LQError &err);
        void regsWrite(unsigned addr, unsigned cnt, const quint16 *vals, LQError &err);
        void regsRW(unsigned wr_addr, unsigned wr_cnt, const quint16 *wr_vals,
                    unsigned rd_addr, unsigned rd_cnt, quint16 *rd_vals, LQError &err);

        void regsWriteCheckDevErr(unsigned addr, unsigned cnt, const quint16 *vals, LQError &err);
        void regsReadWriteCheckDevErr(unsigned wr_addr, unsigned wr_cnt, const quint16 *wr_vals,
                                      unsigned rd_addr, unsigned rd_cnt, quint16 *rd_vals,  LQError &err);

        void wfRead(quint16 wf_id, quint16 ch_num, quint32 offs, unsigned addr, quint16 regs, quint16 *rd_vals, LQError &err);
        static void fillAcqChState(quint16 ch_num, const t_vi_acq_ch_state_full &mb_ch_state, ViAcqChannelState &chState);

        static void fillTstmp(const t_vi_tstmp &mb_tstmp, ViTimeStamp &tstmp);

        mutable QMutex m_mb_ctx_lock;
        OpenParams m_open_params;
        bool m_con_lost {false};
        bool m_is_open {false};
        quint16 m_cmd_id {0};

        struct _modbus *m_mb {nullptr};


    };
}

#endif // LQMEAS_VIMODULEMBCONNECTION_H
