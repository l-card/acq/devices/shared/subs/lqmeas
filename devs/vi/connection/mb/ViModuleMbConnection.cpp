#include "ViModuleMbConnection.h"
#include "ViModuleMbErrors.h"
#include "modbus.h"
#include "lqmeas/devs/vi/protocol/vi_modbus_map.h"
#include "lqmeas/devs/vi/protocol/vi_module_dev_faults.h"
#include "lqmeas/devs/vi/ViErrors.h"
#include "lqmeas/devs/vi/config/ViModuleConfigSerializer.h"
#include "lqmeas/devs/vi/ifaceset/ViIfaceSettings.h"
#include "lqmeas/devs/vi/modules/ViModuleInfo.h"
#include "lqmeas/devs/vi/protocol/vi_module_state.h"
#include "lqmeas/devs/vi/protocol/vi_module_info.h"
#include "lqmeas/devs/vi/data/state/ViModuleState.h"
#include "lqmeas/devs/vi/data/state/acq/ViAcqChannelState.h"
#include "lqmeas/devs/vi/data/state/acq/ViMeasState.h"
#include "lqmeas/devs/vi/data/state/dout/ViDoutTypeState.h"
#include "lqmeas/devs/vi/data/state/dout/ViDoutChannelState.h"
#include "lqmeas/devs/vi/config/dout/ViDoutChannelType.h"
#include "lqmeas/devs/vi/data/state/wf/ViWfInfo.h"
#include "lqmeas/devs/vi/data/state/wf/ViWfChInfo.h"
#include "lqmeas/devs/vi/data/state/wf/ViWfChDataBlock.h"
#include "lqmeas/devs/vi/data/state/diagnostic/ViModuleDiagnostic.h"
#include "lqmeas/devs/vi/data/req/ViAcqChSetFlagReqData.h"
#include "lqmeas/devs/vi/data/req/ViWfReqData.h"
#include "lqmeas/devs/vi/data/req/ViDoutResetReqData.h"
#include "lqmeas/devs/vi/data/req/ViDoutManualChangeReqData.h"
#include "fast_crc.h"
#include <memory>

namespace LQMeas {
    static QHostAddress f_ifaceset_conv_to_host_addr(const uint8_t ip[]) {
        return QHostAddress{static_cast<quint32>((ip[0] << 24)
                                                 |  (ip[1] << 16)
                                                 |  (ip[2] <<  8)
                                                 |  (ip[3] <<  0)
                                                 )};
    }

    void f_ifaceset_fill_from_host_addr(const QHostAddress &addr, uint8_t ip[]) {
        const quint32 wrd {addr.toIPv4Address()};
        ip[0] = (wrd >> 24) & 0xFF;
        ip[1] = (wrd >> 16) & 0xFF;
        ip[2] = (wrd >>  8) & 0xFF;
        ip[3] = (wrd >>  0) & 0xFF;
    }



    void ViModuleMbConnection::open(const OpenParams &params, LQError &err) {
        QMutexLocker lock{&m_mb_ctx_lock};
        privClose();
        m_mb = modbus_new_tcp_pi(params.ipAddr().toLocal8Bit(), QString::number(params.port()).toLocal8Bit());
        if (!m_mb) {
            err  = ViErrors::connectionParameters();
        } else {
    #if LIBMODBUS_VERSION_CHECK(3, 1, 0)
            modbus_set_response_timeout(m_mb, params.respTout()/1000,  (params.respTout() % 1000) *1000);
    #else
            struct timeval tv;
            tv.tv_sec = params.respTout()/1000;
            tv.tv_usec = (params.respTout() % 1000) *1000;
            modbus_set_response_timeout(m_mb, &tv);
    #endif
            if (modbus_connect(m_mb) != 0) {
                err = ViErrors::connectionCreate();
            } else {
                m_open_params = params;
                m_is_open = true;
                m_con_lost = false;
            }
        }
    }

    void ViModuleMbConnection::close() {
        QMutexLocker lock{&m_mb_ctx_lock};
        privClose();
    }

    void ViModuleMbConnection::privClose() {
        if (m_is_open) {
            modbus_close(m_mb);
            modbus_free(m_mb);
            m_mb = nullptr;
            m_is_open = false;
            m_con_lost = false;
        }
    }

    void ViModuleMbConnection::startBootMode(quint16 boot_port, LQError &err) {
        uint16_t boot_seq_http[VI_MB_REGS_CNT_BOOT + 1]  = {0x424F, 0x4F53, 0x4D42, 0x5251, 0 };
        boot_seq_http[VI_MB_REGS_CNT_BOOT] = boot_port;
        regsWriteCheckDevErr(VI_MB_REGADDR_START_BOOT, VI_MB_REGS_CNT_BOOT + 1, boot_seq_http, err);
    }

    void ViModuleMbConnection::resetRequest(LQError &err) {
        static const uint8_t rest_seq[VI_MB_CMD_RST_SEQ_LEN]  = VI_MB_CMD_RESET_SEQ_DATA;
        QByteArray rxData;
        execCmd(VI_MB_CMD_CODE_RST, QByteArray{reinterpret_cast<const char *>(rest_seq), sizeof(rest_seq)}, rxData, err);
    }

    void ViModuleMbConnection::nvConfigWrite(const ViModuleConfig &cfg, LQError &err) {
        QByteArray opCfgData;
        ViModuleMbConfigSerializer::serialize(cfg, opCfgData, err);
        if (err.isSuccess()) {
            quint16 cfg_id {0};
            opcfgWriteRaw(opCfgData, cfg_id, err);

            if (err.isSuccess()) {
                opcfgStore(cfg_id, err);
            }

            if (err.isSuccess()) {
                opcfgUpdate(cfg_id, err);
            }

            if (err.isSuccess()) {
                QByteArray rdArray;
                opcfgReadRaw(rdArray, err);

                if (err.isSuccess() && (rdArray != opCfgData)) {
                    err = ViErrors::opcfgWrCheckMismatch();
                }
            }
        }
    }

    void ViModuleMbConnection::nvConfigRead(ViModuleConfig &cfg, LQError &err) {
        QByteArray opCfgData;
        opcfgReadRaw(opCfgData, err);
        if (err.isSuccess()) {
            ViModuleMbConfigSerializer::parse(opCfgData, cfg, err);
        }
    }
    
    void ViModuleMbConnection::ifaceSettingsRead(ViIfaceSettings &netset, LQError &err) {
        t_vi_mb_cmd_data_ifaceset_rd_req params;
        params.fmt = VI_IFACESET_FMT_GEN_DEF;
        params.reserved = 0;
        QByteArray rxData;
        execCmd(VI_MB_CMD_CODE_IFACESET_RD, QByteArray{reinterpret_cast<const char *>(&params), sizeof(params)}, rxData, err);
        if (err.isSuccess()) {
            if (rxData.size() >= sizeof(t_vi_mb_cmd_data_ifaceset_data)) {
                t_vi_mb_cmd_data_ifaceset_data *mb_ifaceset {reinterpret_cast<t_vi_mb_cmd_data_ifaceset_data *>(rxData.data())};
                netset.setDeviceInstanceName(QString::fromUtf8(reinterpret_cast<const char*>(mb_ifaceset->set.devname)));
                netset.setIpAddr(f_ifaceset_conv_to_host_addr(mb_ifaceset->set.ipv4.addr));
                netset.setIpMask(f_ifaceset_conv_to_host_addr(mb_ifaceset->set.ipv4.mask));
                netset.setIpGate(f_ifaceset_conv_to_host_addr(mb_ifaceset->set.ipv4.gate));
                netset.setAutoGetIpParamsEn(mb_ifaceset->set.flags & VI_IFACESET_FLAG_AUTO_IP);
                netset.setMbPort(mb_ifaceset->set.modbus.port);
            }
        }
    }
    
    void ViModuleMbConnection::ifaceSettingsWrite(const ViIfaceSettings &netset, LQError &err) {
        t_vi_mb_cmd_data_ifaceset_data params;
        memset(&params, 0, sizeof(params));
        params.fmt = VI_IFACESET_FMT_GEN_DEF;
        QByteArray utf8_name {netset.deviceInstanceName().toUtf8()};
        memcpy(params.set.devname, utf8_name.data(), qMin(utf8_name.size(), VI_IFACESET_INSTANCE_NAME_SIZE));
        f_ifaceset_fill_from_host_addr(netset.ipAddr(), params.set.ipv4.addr);
        f_ifaceset_fill_from_host_addr(netset.ipMask(), params.set.ipv4.mask);
        f_ifaceset_fill_from_host_addr(netset.ipGate(), params.set.ipv4.gate);
        if (netset.autoGetIpParamsEn()) {
            params.set.flags |= VI_IFACESET_FLAG_AUTO_IP;
        }
        params.set.modbus.port = netset.mbPort();
        execCmd(VI_MB_CMD_CODE_IFACESET_WR, QByteArray{reinterpret_cast<const char *>(&params), sizeof(params)}, err);
        if (err.isSuccess()) {
            execCmd(VI_MB_CMD_CODE_IFACESET_STORE, QByteArray{}, err);
        }
        if (err.isSuccess()) {
            execCmd(VI_MB_CMD_CODE_IFACESET_UPDATE, QByteArray{}, err);
        }
    }

    ViModuleInfo *ViModuleMbConnection::readModuleInfo(LQError &err) {
        ViModuleInfo *minfo {nullptr};
        t_vi_device_devinfo mb_devinfo;
        regsRead(VI_MB_REGADDR_DEVINFO, sizeof(mb_devinfo)/2, reinterpret_cast<quint16 *>(&mb_devinfo), err);
        if (err.isSuccess()) {
            const ViModuleTypeInfo &type {ViModuleTypeInfo::getDevType(
                mb_devinfo.id.module, mb_devinfo.id.modification, (mb_devinfo.id.revision >> 8) & 0xFF, mb_devinfo.id.revision & 0xFF, mb_devinfo.id.system)};
            QStringVersion sw_ver {QString::fromUtf8(mb_devinfo.soft_ver)};
            QString clkid_text;
            if (sw_ver >=  QStringVersion{{0,0,10}}) {
                std::unique_ptr<ViTimeClockId> clkid {readModuleClockId(err)};
                if (err.isSuccess() && clkid) {
                    clkid_text = clkid->text();
                }
            }


            minfo = new ViModuleInfo{type, QString::fromUtf8(mb_devinfo.serial), QString::fromUtf8(mb_devinfo.soft_ver), clkid_text};
        }
        return minfo;
    }

    ViTimeClockId *ViModuleMbConnection::readModuleClockId(LQError &err) {
        ViTimeClockId *clkid {nullptr};
        quint8 clkid_data[VI_CLKID_SIZE];
        regsRead(VI_MB_REGADDR_MODULE_CLK_ID, sizeof(clkid_data)/2, reinterpret_cast<quint16 *>(&clkid_data[0]), err);
        if (err.isSuccess()) {
            clkid = new ViTimeClockId{};
            clkid->setData(clkid_data);
        }
        return clkid;
    }

    ViAcqChannelState *ViModuleMbConnection::readAcqChState(int ch_num, LQError &err) {
        ViAcqChannelState *chState {nullptr};
        t_vi_acq_ch_state_full mb_ch_state;
        regsRead(VI_MB_REGADDR_CH_STATUS(ch_num), sizeof(mb_ch_state)/2, reinterpret_cast<quint16 *>(&mb_ch_state), err);
        if (err.isSuccess()) {
            chState = new ViAcqChannelState{};
            fillAcqChState(ch_num, mb_ch_state, *chState);

        }
        return chState;
    }

    ViModuleState *ViModuleMbConnection::readModuleState(const ViModuleInfo &info, LQError &err) {
        ViModuleState *moduleState {nullptr};
        t_vi_module_state mb_state;
        memset(&mb_state, 0, sizeof(mb_state));
        regsRead(VI_MB_REGADDR_MODULE_STATE, sizeof(mb_state)/2, reinterpret_cast<quint16 *>(&mb_state), err);
        if (err.isSuccess()) {
            moduleState = new ViModuleState{};
            moduleState->m_live_bit = mb_state.state_flags & VI_MODULE_STATE_FLAG_LIVEBIT;
            moduleState->m_has_fault = mb_state.state_flags & VI_MODULE_STATE_FLAG_FAULT;
            moduleState->m_has_warn  = mb_state.state_flags & VI_MODULE_STATE_FLAG_WARN;
            moduleState->m_cfg_id = mb_state.opcfg_id;

            moduleState->m_wf_rdy_cnt = mb_state.wf_rdy_cnt;
            if (info.supportAllRdyWfIds()) {
                for (quint16 i {0}; i < moduleState->m_wf_rdy_cnt; ++i) {
                    moduleState->m_wf_rdy_ids += mb_state.wf_last_id[i];
                }
            } else {
                if (moduleState->m_wf_rdy_cnt > 0) {
                    moduleState->m_wf_rdy_ids += mb_state.wf_last_id[0];
                }
            }

            if (info.supportLocalTimeInfo()) {
                moduleState->m_loc_time_info.m_valid = true;
                moduleState->m_loc_time_info.m_secs = mb_state.loc_time.sec;
                moduleState->m_loc_time_info.m_nsec = mb_state.loc_time.ns;
                moduleState->m_loc_time_info.m_df_ppm = mb_state.loc_time.loc_feq_ppm;
            }

            fillTstmp(mb_state.cur_time, moduleState->m_cur_time);

            moduleState->m_evtStates.resize(VI_OPCFG_EVT_CNT_MAX);
            for (int evt_idx {0}; evt_idx < VI_OPCFG_EVT_CNT_MAX; ++evt_idx) {
                moduleState->m_evtStates[evt_idx] = vi_module_state_evt_get(&mb_state, evt_idx);
            }


            moduleState->m_dinStates.resize(VI_OPCFG_DIN_CNT_MAX);
            for (int din_idx {0}; din_idx < VI_OPCFG_DIN_CNT_MAX; ++din_idx) {
                if (info.supportDinState()) {
                    moduleState->m_dinStates[din_idx] = vi_module_state_din_get(&mb_state, din_idx);
                } else {
                    moduleState->m_dinStates[din_idx] = tribool_z;
                }
            }
        }

        return moduleState;

    }

    ViDoutTypeState *ViModuleMbConnection::readDoutTypeState(const ViModuleInfo &info, const ViDoutChannelType &chType, LQError &err)     {
        ViDoutTypeState *ret {nullptr};
        if (chType == ViDoutChannelTypes::relayLP()) {
            ret = readDoutTypeStateRegs(VI_MB_REGADDR_MODULE_DOUT_LP_STATE, VI_DOUT_LP_STATE_REGS_CNT, info, chType, err);
        } else if (chType == ViDoutChannelTypes::relayHP()) {
            ret = readDoutTypeStateRegs(VI_MB_REGADDR_MODULE_DOUT_HP_STATE, VI_DOUT_HP_STATE_REGS_CNT, info, chType, err);
        }
        return ret;
    }

    ViModuleDiagnostic *ViModuleMbConnection::readDiagnostic(LQError &err) {
        ViModuleDiagnostic *diagnostic {nullptr};
        t_vi_module_diagnostic mb_diagn;
        regsRead(VI_MB_REGADDR_MODULE_DIAGNOSTIC, sizeof(mb_diagn)/2, reinterpret_cast<quint16 *>(&mb_diagn), err);
        if (err.isSuccess()) {
            diagnostic = new ViModuleDiagnostic{};
            for (int reg = 0; reg < VI_DEVFAULTS_STATE_REGS_CNT; ++reg) {
                if (mb_diagn.devfault_errs[reg] != 0) {
                    for (int bitnum {0}; bitnum < 16; ++bitnum) {
                        if (mb_diagn.devfault_errs[reg] & (1UL << bitnum)) {
                            diagnostic->m_devfault_list += ViModuleFault::getByID(ViModuleFaults::all(), VI_DEVFAULT_ERR_CODE(reg * 16 + bitnum + 1), &ViModuleFaults::unknownFault());
                        }
                    }
                }
                if (mb_diagn.devfault_warns[reg] != 0) {
                    for (int bitnum {0}; bitnum < 16; ++bitnum) {
                        if (mb_diagn.devfault_warns[reg] & (1UL << bitnum)) {
                            diagnostic->m_devfault_list += ViModuleFault::getByID(ViModuleFaults::all(), VI_DEVFAULT_WARN_CODE(reg * 16 + bitnum + 1), &ViModuleFaults::unknownWarn());
                        }
                    }
                }
            }

            for (int adc_num = 0; adc_num < VI_DIGAN_ADC_MAX_CNT; ++adc_num) {
                const t_vi_diagn_adc_ch_state *mb_adc_ch {&mb_diagn.diagn_adc[adc_num]};
                if (mb_adc_ch->status & VI_DIGAN_ADC_STATUS_FLD_PRESENT) {
                    ViDiagnosticAdcState adcState;
                    adcState.m_addr = adc_num;
                    adcState.m_present = mb_adc_ch->status & VI_DIGAN_ADC_STATUS_FLD_PRESENT;
                    adcState.m_valid = mb_adc_ch->status & VI_DIGAN_ADC_STATUS_FLD_RDY;
                    adcState.m_range_err = mb_adc_ch->status & VI_DIGAN_ADC_STATUS_FLD_RANGEERR;
                    adcState.m_value = mb_adc_ch->value;
                    diagnostic->m_adc_state_list += adcState;
                }
            }

        }
        return diagnostic;
    }

    ViWfInfo *ViModuleMbConnection::readWfInfo(const ViModuleInfo &info, quint16 wf_id, LQError &err) {
        ViWfInfo *wfInfo {nullptr};
        t_vi_wf_info mb_wf_info;
        wfRead(wf_id, 0, 0, VI_MB_REGADDR_WF_INFO, sizeof(mb_wf_info)/2, reinterpret_cast<quint16 *>(&mb_wf_info), err);
        if (err.isSuccess()) {
            wfInfo = new ViWfInfo{};
            wfInfo->setWfId(mb_wf_info.wf_id);
            wfInfo->setSrcIsUserReq(mb_wf_info.src == VI_WF_SRC_USER_REQ);
            wfInfo->setSrcNum(mb_wf_info.src);
            wfInfo->setSamplesFreq(mb_wf_info.sample_freq);
            if (info.supportWfGmFreq()) {
                wfInfo->setGmSamplesFreq(mb_wf_info.sample_gm_freq);
            } else {
                wfInfo->setGmSamplesFreq(wfInfo->samplesFreq());
            }
            wfInfo->setChMask(mb_wf_info.params.ch_mask);
            wfInfo->setSamplesCnt(mb_wf_info.params.samples_cnt);
            wfInfo->setDelaySamplesCnt(mb_wf_info.params.delay_cnt);
            wfInfo->setWfType(static_cast<ViWfInfo::Type>(mb_wf_info.params.type));
            wfInfo->setFlags(mb_wf_info.params.flags);

            if (info.supportWfTime()) {
                ViTimeStamp ts;
                fillTstmp(mb_wf_info.tstmp, ts);
                wfInfo->setTimeStamp(ts);
            }
        }
        return wfInfo;
    }

    ViWfChInfo *ViModuleMbConnection::readWfChInfo(quint16 wf_id, quint16 ch_num, LQError &err) {
        ViWfChInfo *wfChInfo {nullptr};
        t_vi_wf_ch_info mb_wf_ch_info;
        /** @todo для возможности считать за один раз, сейчас не считываем тут измерения 9-10,
         *  потом можно реализовать дочтение если по количеству их будет больше */
        wfRead(wf_id, ch_num, 0, VI_MB_REGADDR_WF_CH_INFO, (sizeof(mb_wf_ch_info) - 2*sizeof(t_vi_meas_state))/2, reinterpret_cast<quint16 *>(&mb_wf_ch_info), err);
        if (err.isSuccess()) {
            wfChInfo = new ViWfChInfo{};
            wfChInfo->setWfId(mb_wf_ch_info.wf_id);
            wfChInfo->setChNum(mb_wf_ch_info.ch_num);
            wfChInfo->setFlags(mb_wf_ch_info.flags);

            ViAcqChannelConvInfo conv;
            conv.setElec({mb_wf_ch_info.conv_elec.k, mb_wf_ch_info.conv_elec.b});
            conv.setPhys({mb_wf_ch_info.conv_phys.k, mb_wf_ch_info.conv_phys.b});
            wfChInfo->setConv(conv);

            QSharedPointer<LQMeas::ViAcqChannelState> chState{new LQMeas::ViAcqChannelState{}};
            fillAcqChState(wfChInfo->chNum(), mb_wf_ch_info.state, *chState);
            wfChInfo->setChState(chState);
        }
        return wfChInfo;
    }

    ViWfChDataBlock *ViModuleMbConnection::readWfChData(quint16 wf_id, quint16 ch_num, quint32 offs, quint32 samles_cnt, LQError &err) {
        ViWfChDataBlock *wfChData{nullptr};
        QVector<int32_t> samples;
        samples.resize(samles_cnt);

        wfRead(wf_id, ch_num, offs, VI_MB_REGADDR_WF_DATA, samles_cnt * sizeof(samples[0])/2, reinterpret_cast<quint16 *>(samples.data()), err);
        if (err.isSuccess()) {
            wfChData = new ViWfChDataBlock{};
            wfChData->setWfId(wf_id);
            wfChData->setChNum(ch_num);
            wfChData->setOffset(offs);
            wfChData->setSamples(samples);
        }
        return wfChData;
    }

    void ViModuleMbConnection::wfRead(quint16 wf_id, quint16 ch_num, quint32 offs, unsigned int addr, quint16 regs, quint16 *rd_vals, LQError &err)    {
        t_vi_wf_sel sel;
        sel.wf_id = wf_id;
        sel.ch_num = ch_num;
        sel.samples_offs = offs;

        t_vi_wf_info mb_wf_info;
        regsReadWriteCheckDevErr(VI_MB_REGADDR_WF_SEL, sizeof(sel)/2, reinterpret_cast<const quint16 *>(&sel),
                                 addr, regs, rd_vals, err);
    }

    void ViModuleMbConnection::setAcqChStateFlag(const ViAcqChSetFlagReqData &reqData, LQError &err) {
        t_vi_mb_cmd_data_set_acq_ch_flag params;
        params.ch_flag = static_cast<int>(reqData.flag());
        params.ch_mask = reqData.channelMask();
        params.state = reqData.state();
        execCmd(VI_MB_CMD_CODE_SET_ACQ_CH_FLAG, QByteArray{reinterpret_cast<const char *>(&params), sizeof(params)}, err);
    }

    void ViModuleMbConnection::wfManualRequest(const ViWfReqData &reqData, quint16 &wf_id, LQError &err) {
        t_vi_mb_cmd_data_wf_req req;
        memset(&req, 0, sizeof(req));
        req.params.type = VI_WF_TYPE_TIME_WAVE;
        req.params.ch_mask = reqData.channelMask();
        req.params.samples_cnt = reqData.samplesCnt();
        req.params.delay_cnt = reqData.delaySamplesCnt();
        if (reqData.timestamp().isValid()) {
            req.tstmp.flags = reqData.timestamp().flags();
            req.tstmp.utc_offset = reqData.timestamp().flags();
            req.tstmp.fract_ns = reqData.timestamp().nsecFract();
            req.tstmp.ns = reqData.timestamp().nsec();
            req.tstmp.sec = reqData.timestamp().seconds() & 0xFFFFFFFF;
            req.tstmp.hsec = (reqData.timestamp().seconds() >> 32) & 0xFFFF;
        }
        QByteArray rxData;
        execCmd(VI_MB_CMD_CODE_WF_REQ, QByteArray{reinterpret_cast<const char *>(&req), sizeof(req)}, rxData, err);
        if (err.isSuccess()) {
            if (rxData.size() >= sizeof(wf_id)) {
                memcpy(&wf_id, rxData.data(), sizeof(wf_id));
            } else {
                err = ViErrors::insufCmdRespDataSize();
            }
        }
    }

    void ViModuleMbConnection::doutReset(const ViDoutResetReqData &reqData, LQError &err) {
        t_vi_mb_cmd_data_dout_reset params;
        memset(&params, 0, sizeof(params));
        params.ch_lp_mask = reqData.lpChannelMask();
        params.ch_hp_mask = reqData.hpChannelMask();
        execCmd(VI_MB_CMD_CODE_DOUT_RESET, QByteArray{reinterpret_cast<const char *>(&params), sizeof(params)}, err);
    }

    void ViModuleMbConnection::doutManualChange(const ViDoutManualChangeReqData &reqData, LQError &err) {
        t_vi_mb_cmd_data_dout_man_set params;
        memset(&params, 0, sizeof(params));
        params.ch_lp_mask = reqData.lpChannelMask();
        params.ch_lp_values = reqData.lpChannelValues();
        params.ch_hp_mask = reqData.hpChannelMask();
        params.ch_hp_values = reqData.hpChannelValues();
        execCmd(VI_MB_CMD_CODE_DOUT_MANUAL_SET, QByteArray{reinterpret_cast<const char *>(&params), sizeof(params)}, err);
    }

    void ViModuleMbConnection::execCmd(t_vi_mb_cmd_codes cmd, const QByteArray &txData,
                                       QByteArray &rx_data, LQError &err) {
        t_vi_mb_cmd_req_data cmd_req;
        t_vi_mb_cmd_resp_data resp_data;

        memset(&cmd_req, 0, sizeof(cmd_req));
        memset(&resp_data, 0, sizeof(resp_data));
        cmd_req.hdr.sign = VI_MB_CMD_SIGN;
        cmd_req.hdr.id = ++m_cmd_id;
        cmd_req.hdr.cmd = cmd;
        memcpy(cmd_req.data, txData.data(), txData.size());

        regsReadWriteCheckDevErr(VI_MB_REGADDR_CMD_REQ,  (sizeof(cmd_req.hdr) + txData.size() + 1)/2,
                                 reinterpret_cast<const uint16_t *>(&cmd_req),
                                 VI_MB_REGADDR_CMD_STATUS, sizeof(resp_data)/2,
                                 reinterpret_cast<uint16_t *>(&resp_data),
                                 err);
        if (err.isSuccess()) {
            if ((resp_data.hdr.cmd != cmd) || (resp_data.hdr.id != cmd_req.hdr.id)) {
                err = ViErrors::invalidCmdRespFmt();
            } else if (resp_data.hdr.state == VI_MB_CMD_RESP_STATE_ERROR){
                err = ViErrors::devError(resp_data.hdr.err);
            } else if (resp_data.hdr.state != VI_MB_CMD_RESP_STATE_DONE) {
                err = ViErrors::invalidCmdRespStatus();
            } else {
                rx_data = QByteArray{reinterpret_cast<const char *>(resp_data.data), resp_data.hdr.data_size};
            }
        }

    }

    void ViModuleMbConnection::execCmd(t_vi_mb_cmd_codes cmd, const QByteArray &txData, LQError &err) {
        QByteArray rxData;
        execCmd(cmd, txData, rxData, err);
    }

    void ViModuleMbConnection::opcfgWriteRaw(const QByteArray &opCfgData, quint16 &cfg_id, LQError &err) {
        opcfgWriteStart(cfg_id, err);
        if (err.isSuccess()) {
            int size {opCfgData.size()};

            for (int offs{0}; (offs < size) && err.isSuccess(); offs += VI_MB_CMD_OPCFG_BLOCK_LEN_MAX) {
                const int block_len {qMin<int>(size - offs, VI_MB_CMD_OPCFG_BLOCK_LEN_MAX)};
                opcfgWriteBlock(cfg_id, offs, opCfgData.mid(offs, block_len), err);
            }
        }

        if (err.isSuccess()) {
            quint32 cfg_crc {CRC32_Block8(0, reinterpret_cast<const quint8*>(opCfgData.data()), opCfgData.size())};

            opcfgWriteFinish(cfg_id, opCfgData.size(), cfg_crc, err);
        }
    }

    void ViModuleMbConnection::opcfgReadRaw(QByteArray &opCfgData, LQError &err) {
        QByteArray rxData;
        execCmd(VI_MB_CMD_CODE_OPCFG_RD_INFO, QByteArray{}, rxData, err);
        if (err.isSuccess()) {
            if (!rxData.isEmpty()) {
                if (rxData.size() < sizeof(t_vi_opcfg_info)) {
                    err = ViErrors::insufCmdRespDataSize();
                } else {
                    const t_vi_opcfg_info  *cfg_info = reinterpret_cast<const t_vi_opcfg_info*>(rxData.data());
                    opCfgData.reserve(cfg_info->data_size);

                    for (int pos = 0; pos < cfg_info->data_size; pos+= VI_MB_CMD_DATA_SIZE_MAX) {
                        t_vi_mb_cmd_data_opcfg_rd_buf_params params;
                        params.offset = pos;
                        params.size = qMin<uint32_t>(VI_MB_CMD_DATA_SIZE_MAX, cfg_info->data_size - pos);
                        QByteArray blockData;
                        execCmd(VI_MB_CMD_CODE_OPCFG_RD_BUF,
                                QByteArray{reinterpret_cast<const char *>(&params), sizeof(params)},
                                blockData, err);
                        if (err.isSuccess()) {
                            opCfgData += blockData;
                        }
                    }
                }
            } else {
                err = ViErrors::noValidOpCfg();
            }
        }
    }

    void ViModuleMbConnection::opcfgWriteStart(quint16 &crg_id, LQError &err) {
        QByteArray rxData;
        execCmd(VI_MB_CMD_CODE_OPCFG_MODIFY_START, QByteArray{}, rxData, err);
        if (err.isSuccess()) {
            if (rxData.size() < sizeof(crg_id)) {
                err = ViErrors::insufCmdRespDataSize();
            } else {
                memcpy(&crg_id, rxData.data(), sizeof(crg_id));
            }
        }
    }

    void ViModuleMbConnection::opcfgWriteBlock(quint16 cfg_id, quint32 offs, const QByteArray &blockData, LQError &err) {

        t_vi_mb_cmd_opcfg_mod_block_hdr wrhdr;
        wrhdr.cfg_id = cfg_id;
        wrhdr.offs    = offs;


        execCmd(VI_MB_CMD_CODE_OPCFG_MODIFY_WRBLOCK,
                QByteArray{reinterpret_cast<const char *>(&wrhdr), sizeof(wrhdr)} +
                blockData,
                err
                );
    }

    void ViModuleMbConnection::opcfgWriteFinish(quint16 cfg_id, quint32 size, quint32 crc, LQError &err) {
        t_vi_mb_cmd_data_opcfg_mod_finish finishblock;
        memset(&finishblock, 0, sizeof(finishblock));
        finishblock.cfg_id = cfg_id;
        finishblock.size = size;
        finishblock.crc = crc;

        execCmd(VI_MB_CMD_CODE_OPCFG_MODIFY_FINISH,
                QByteArray{reinterpret_cast<const char *>(&finishblock), sizeof(finishblock)},
                err
                );

    }

    void ViModuleMbConnection::opcfgUpdate(quint16 cfg_id, LQError &err) {
        execCmd(VI_MB_CMD_CODE_OPCFG_MODIFY_UPDATE,
                QByteArray{reinterpret_cast<const char *>(&cfg_id), sizeof(cfg_id)},
                err
                );
    }

    void ViModuleMbConnection::opcfgStore(quint16 cfg_id, LQError &err) {
        execCmd(VI_MB_CMD_CODE_OPCFG_MODIFY_STORE,
                QByteArray{reinterpret_cast<const char *>(&cfg_id), sizeof(cfg_id)},
                err
                );
    }

    void ViModuleMbConnection::checkOpen(LQError &err)  {
        if (!m_mb) {
            err = ViErrors::connectionNotOpen();
        } else if (m_con_lost) {
            err = ViErrors::connectionLost();
        }
    }

    ViDoutTypeState *ViModuleMbConnection::readDoutTypeStateRegs(
        unsigned int addr, unsigned int ch_cnt, const ViModuleInfo &info,
        const ViDoutChannelType &chType, LQError &err) {

        ViDoutTypeState *doutsState = nullptr;
        std::unique_ptr<quint16[]> ch_state_regs{new quint16[ch_cnt]};
        regsRead(addr, ch_cnt, ch_state_regs.get(), err);
        if (err.isSuccess()) {
            doutsState = new ViDoutTypeState{chType};
            for (unsigned ch_idx {0}; ch_idx < ch_cnt; ++ch_idx) {
                ViDoutChannelState *chState {new ViDoutChannelState{chType, static_cast<int>(ch_idx)}};
                quint16 ch_state_flags {ch_state_regs[ch_idx]};
                chState->m_failure = flag_to_tribool(ch_state_flags >> VI_OUT_STATE_FLAG_POS_FAILURE);
                chState->m_energized = flag_to_tribool(ch_state_flags >> VI_OUT_STATE_FLAG_POS_ENERGIZED);
                chState->m_out_state = flag_to_tribool(ch_state_flags >> VI_OUT_STATE_FLAG_POS_STATE);
                chState->m_alarm_evt_state = flag_to_tribool(ch_state_flags >> VI_OUT_STATE_FLAG_POS_CTL_EVT);
                chState->m_rst_evt_state = flag_to_tribool(ch_state_flags >> VI_OUT_STATE_FLAG_POS_RST_EVT);

                doutsState->m_chs += chState;
            }
        }

        return doutsState;
    }

    void ViModuleMbConnection::getMbRegsErr(LQError &err) {
#ifdef Q_OS_WIN
        /* на Windows libmodbus не всегда возвращает код ошибки...*/
        DWORD err_code {GetLastError()};
        const bool con_lost {true};
        const bool con_rst {false};
#else
        const int err_code {errno};
        const bool con_lost {err_code == ETIMEDOUT};
        const bool con_rst {err_code == ECONNRESET};
#endif
        if (con_lost)  {
            err = ViErrors::connectionLost();
            m_con_lost = true;
        } else if (con_rst) {
            err = ViErrors::connectionReset();
            m_con_lost = true;
        } else {
            err = ViModuleMbErrors::mbRegsOp(err_code);
        }
    }

    void ViModuleMbConnection::regsRead(unsigned int addr, unsigned int cnt, quint16 *vals, LQError &err) {
        QMutexLocker lock{&m_mb_ctx_lock};
        checkOpen(err);
        if (err.isSuccess() && (modbus_read_input_registers(m_mb, addr - 1, cnt, vals) != cnt)) {
            getMbRegsErr(err);
        }
    }

    void ViModuleMbConnection::regsWrite(unsigned int addr, unsigned int cnt, const quint16 *vals, LQError &err) {
        QMutexLocker lock{&m_mb_ctx_lock};
        checkOpen(err);
        if (err.isSuccess() && (modbus_write_registers(m_mb, addr - 1, cnt, vals) != cnt)) {
            getMbRegsErr(err);
        }
    }

    void ViModuleMbConnection::regsRW(unsigned int wr_addr, unsigned int wr_cnt, const quint16 *wr_vals,
                                         unsigned int rd_addr, unsigned int rd_cnt, quint16 *rd_vals, LQError &err)   {
        QMutexLocker lock{&m_mb_ctx_lock};
        checkOpen(err);
        if (err.isSuccess() && (modbus_write_and_read_registers(
                                    m_mb, wr_addr - 1, wr_cnt, wr_vals, rd_addr - 1, rd_cnt, rd_vals) != rd_cnt)) {
            getMbRegsErr(err);
        }
    }

    void ViModuleMbConnection::regsWriteCheckDevErr(
        unsigned int addr, unsigned int cnt, const quint16 *vals, LQError &err) {

        uint16_t dev_err = 0;
        regsWrite(addr, cnt, vals, err);
        if (!err.isSuccess()) {
            LQError rd_err;
            regsRead(VI_MB_REGADDR_LAST_WR_ERR, 1, &dev_err, rd_err);
            if (rd_err.isSuccess() && (dev_err != 0)) {
                err = ViErrors::devError(dev_err);
            }
        }
    }

    void ViModuleMbConnection::regsReadWriteCheckDevErr(
        unsigned int wr_addr, unsigned int wr_cnt, const quint16 *wr_vals,
        unsigned int rd_addr, unsigned int rd_cnt, quint16 *rd_vals, LQError &err) {

        uint16_t dev_err = 0;
        regsRW(wr_addr, wr_cnt, wr_vals, rd_addr, rd_cnt, rd_vals, err);
        if (!err.isSuccess()) {
            LQError rd_err;
            regsRead(VI_MB_REGADDR_LAST_WR_ERR, 1, &dev_err, rd_err);
            if (rd_err.isSuccess() && (dev_err != 0)) {
                err = ViErrors::devError(dev_err);
            }
        }
    }

    void ViModuleMbConnection::fillAcqChState(quint16 ch_num, const t_vi_acq_ch_state_full &mb_ch_state, ViAcqChannelState &chState) {
        chState.m_ch_num = ch_num;
        chState.fillStatus(mb_ch_state.gen.ch_status);
        chState.m_bypassed      = mb_ch_state.gen.status_flags & VI_ACQ_CH_STATE_FLAG_BYPASS;

        chState.m_alarm_inhibit.setManualState(mb_ch_state.gen.status_flags & VI_ACQ_CH_STATE_FLAG_INH_MAN);
        chState.m_alarm_inhibit.setEventState(mb_ch_state.gen.status_flags & VI_ACQ_CH_STATE_FLAG_INH_EVT);
        chState.m_trip_multiply.setManualState(mb_ch_state.gen.status_flags & VI_ACQ_CH_STATE_FLAG_TM_MAN);
        chState.m_trip_multiply.setEventState(mb_ch_state.gen.status_flags & VI_ACQ_CH_STATE_FLAG_TM_EVT);


        for (int meas_idx {0}; meas_idx < mb_ch_state.gen.meas_cnt; ++meas_idx) {
            const t_vi_meas_state *mb_meas_state = &mb_ch_state.meas[meas_idx];
            ViMeasState *measState {new ViMeasState{}};
            measState->m_meas_num = meas_idx;
            measState->fillMeasType(mb_meas_state->type);
            measState->m_value = mb_meas_state->value;
            measState->m_percent_value = ViMeasState::scaledValue(mb_meas_state->percent_value);
            measState->m_percent_baseline = ViMeasState::scaledValue(mb_meas_state->percent_baseline);
            measState->m_failure = vi_meas_state_failure(mb_meas_state);
            measState->m_alert = vi_meas_state_alert(mb_meas_state);
            measState->m_danger = vi_meas_state_danger(mb_meas_state);
            measState->m_baseline_valid = vi_meas_state_baseline_valid(mb_meas_state);
            measState->m_baseline_exceeded = vi_meas_state_baseline_exceeded(mb_meas_state);
            chState.m_meas_list += measState;
        }
    }

    void ViModuleMbConnection::fillTstmp(const t_vi_tstmp &mb_tstmp, ViTimeStamp &tstmp) {
        tstmp.setIsValid(mb_tstmp.value.flags & VI_TSTMP_FLAG_VALID);
        if (tstmp.isValid()) {
            tstmp.setIsGlobal(mb_tstmp.value.flags & VI_TSTMP_FLAG_PTP_TIMESCALE);
            quint64 secs = ((quint64)mb_tstmp.value.hsec << 32ULL) | mb_tstmp.value.sec;
            tstmp.setSeconds(secs);
            tstmp.setNSec(mb_tstmp.value.ns);
            tstmp.setNSecFract(mb_tstmp.value.fract_ns);
            if (tstmp.isGlobal()) {
                if (mb_tstmp.value.flags & VI_TSTMP_FLAG_UTC_OFFS_VALID) {
                   secs -= mb_tstmp.value.utc_offset;
                }
                quint32 msec = tstmp.nsec() / 1000000;
                tstmp.setUtcTime(QDateTime::fromMSecsSinceEpoch(secs * 1000 + msec, Qt::TimeSpec::UTC));
            }

            ViTimeClockId clkid;
            clkid.setData(mb_tstmp.clk_id);
            tstmp.setClockId(clkid);
            tstmp.setFlags(mb_tstmp.value.flags);
            tstmp.setUtcOffset(mb_tstmp.value.utc_offset);
        }
    }
}

