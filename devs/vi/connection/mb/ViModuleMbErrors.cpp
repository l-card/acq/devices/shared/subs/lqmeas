#include "ViModuleMbErrors.h"
#include "modbus.h"

namespace LQMeas {
    LQError ViModuleMbErrors::mbRegsOp(int err) {
        return mbError(-1, tr("Modbus register operation error")  + QString(" (%1)").arg(modbus_strerror(err)));
    }

    LQError ViModuleMbErrors::mbError(int code, const QString &msg) {
        static const QString err_type {QStringLiteral("vi_con_mb")};
        return LQError{code, msg, err_type};
    }
}
