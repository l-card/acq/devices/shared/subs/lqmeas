#ifndef LQMEAS_VIMODULEMBERRORS_H
#define LQMEAS_VIMODULEMBERRORS_H

#include "LQError.h"
#include <QObject>


namespace LQMeas {
    class ViModuleMbErrors  : public QObject {
        Q_OBJECT
    public:

        static LQError mbRegsOp(int err);
    private:
        static LQError mbError(int code, const QString &msg);
    };
}

#endif // LQMEAS_VIMODULEMBERRORS_H
