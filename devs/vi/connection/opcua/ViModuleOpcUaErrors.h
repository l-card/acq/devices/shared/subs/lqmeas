#ifndef LQMEAS_VIMODULEOPCUAERRORS_H
#define LQMEAS_VIMODULEOPCUAERRORS_H


#include "LQError.h"
#include <QObject>


namespace LQMeas {
    class ViModuleOpcUaErrors  : public QObject {
        Q_OBJECT
    public:

        static LQError acqChParamsRead(quint32 status_code);
        static LQError acqChSubscriptionCreate(quint32 status_code);
        static LQError acqChMonItemsCreate(quint32 status_code);
        static LQError acqChDataRecv(quint32 status_code);
    private:

        static LQError opcStatusError(int code, quint32 status_code, const QString &msg);
        static LQError opcError(int code, const QString &msg);
    };
}


#endif // VIMODULEOPCUAERRORS_H
