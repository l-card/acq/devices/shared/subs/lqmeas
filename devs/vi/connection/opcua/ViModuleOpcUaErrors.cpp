#include "ViModuleOpcUaErrors.h"
#include "open62541/types.h"

namespace LQMeas {
    LQError ViModuleOpcUaErrors::acqChParamsRead(quint32 status_code) {
        return opcStatusError(-1, status_code, tr("Acquisition channel paramaeters read request failed"));
    }

    LQError ViModuleOpcUaErrors::acqChSubscriptionCreate(quint32 status_code)  {
        return opcStatusError(-2, status_code, tr("Acquisition channels raw data subscription create request failed"));
    }

    LQError ViModuleOpcUaErrors::acqChMonItemsCreate(quint32 status_code) {
        return opcStatusError(-3, status_code, tr("Acquisition channels monitored items create request failed"));
    }

    LQError ViModuleOpcUaErrors::acqChDataRecv(quint32 status_code) {
        return opcStatusError(-4, status_code, tr("Acquisition channels data receive error"));
    }

    LQError ViModuleOpcUaErrors::opcStatusError(int code, quint32 status_code, const QString &msg) {
        return opcError(code, tr("%1. Status 0x%2: %3").arg(msg).arg(status_code, 8, 16, QChar('0')).arg(QString::fromUtf8(UA_StatusCode_name(status_code))));
    }

    LQError ViModuleOpcUaErrors::opcError(int code, const QString &msg) {
        static const QString err_type {QStringLiteral("vi_con_opcua")};
        return LQError{code, msg, err_type};
    }
}
