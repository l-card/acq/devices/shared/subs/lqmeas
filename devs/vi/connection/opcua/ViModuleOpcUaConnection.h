#ifndef VIMODULEOPCUACONNECTION_H
#define VIMODULEOPCUACONNECTION_H

#include <QString>
#include <QList>
#include <QVector>
#include <QMutex>
#include <open62541/client_highlevel.h>
#include "lqmeas/devs/vi/data/state/acq/ViAcqChannelStreamInfo.h"
#include "lqmeas/devs/vi/data/state/ViTimeStamp.h"
#include "LQError.h"

namespace LQMeas {
    class ViModuleOpcUaConnection {
    public:
        class OpenParams {
        public:
            static quint16 defaultPort();

            explicit OpenParams(const QString &ipAddr = QString{},
                                quint16 port = defaultPort()) :
                m_ip_addr{ipAddr}, m_port{port} {
            }

            const QString &ipAddr() const {return m_ip_addr;}
            quint16 port() const {return m_port;}

        private:
            QString m_ip_addr;
            quint16 m_port;
        };

        ViModuleOpcUaConnection();
        ~ViModuleOpcUaConnection();

        void open(const OpenParams &params, LQError &err);
        void close();

        void acqStreamReadStart(quint32 adc_ch_mask, quint32 block_time_ms,
                                QList<ViAcqChannelStreamInfo> &streamChsInfo, LQError &err);
        void acqStreamReadStop(LQError &err);
        void acqStreamProgress(int tout);

        void acqStreamGetData(int ch_idx, int32_t *data, int size,
                              unsigned tout, int &recvd_size, ViTimeStamp *lastTs, LQError &err);
    private:


        static void acqStreamDataHandler(UA_Client *client, UA_UInt32 subId, void *subContext,
                                    UA_UInt32 monId, void *monContext, UA_DataValue *value);


        struct StreamChCtx {
            ViAcqChannelStreamInfo info;
            UA_UInt32 monItemId;
            bool isActive;
            int cur_samples_cnt {0};
            int samples_put_pos {0};
            QVector<qint32> samples;
            QVector<ViTimeStamp> tstmps;
            QList<int> ov_idx_list;
            int err_idx {0};
            LQError err;
            QMutex mutex;
        };

        QList<StreamChCtx *> m_chs;


        static const int max_ch_cnt = 4;

        struct UA_Client *m_ctx {nullptr};
        bool m_is_opened {false};
        UA_UInt32 m_sub_id;
        const int m_buf_time_ms = 5000;

    };
}

#endif // VIMODULEOPCUACONNECTION_H
