#include "ViModuleOpcUaConnection.h"
#include "ViModuleOpcUaErrors.h"
#include <open62541/client_config_default.h>
#include <open62541/client_highlevel.h>
#include <open62541/plugin/log_stdout.h>
#include <open62541/client_subscriptions.h>
#include "lqmeas/devs/vi/protocol/vi_opcua_map.h"
#include "lqmeas/devs/vi/ViErrors.h"
#include <QElapsedTimer>
#include <memory>

/* разница в секундах между отсчетом времени OpcUA и UnixTime */
#define OPCUA_TSTMP_SEC_TO_UNIX_EPOCH        11644473600LL
#define OPCUA_TSTMP_TICK_PER_SEC                10000000

#define VI_INT_NODEID(intval) UA_NODEID_NUMERIC(VI_OPCUA_NODEID_NS, intval)

namespace LQMeas {

    quint16 ViModuleOpcUaConnection::OpenParams::defaultPort() {return VI_OPCUA_TCP_DEFAULT_PORT;}

    ViModuleOpcUaConnection::ViModuleOpcUaConnection() {
        m_ctx = UA_Client_new();
        UA_ClientConfig *cfg = UA_Client_getConfig(m_ctx);
        UA_ClientConfig_setDefault(cfg);
    }

    ViModuleOpcUaConnection::~ViModuleOpcUaConnection()  {
        close();
        UA_Client_delete(m_ctx);
    }

    void ViModuleOpcUaConnection::open(const OpenParams &params, LQError &err) {
        QString conStr = QString{"opc.tcp://%1:%2"}.arg(params.ipAddr()).arg(params.port());
        UA_StatusCode retval = UA_Client_connect(m_ctx, conStr.toUtf8().data());
        if(retval != UA_STATUSCODE_GOOD) {
            err = ViErrors::connectionCreate();
        } else {
            m_is_opened = true;
        }
    }

    void ViModuleOpcUaConnection::close() {
        if (m_is_opened) {
            UA_Client_disconnect(m_ctx);
            m_is_opened = false;
        }
    }

    void ViModuleOpcUaConnection::acqStreamReadStart(quint32 adc_ch_mask, quint32 block_time_ms,
                                                     QList<ViAcqChannelStreamInfo> &streamChsInfo, LQError &err) {
        int ch_cnt = 0;

        UA_MonitoredItemCreateRequest items[max_ch_cnt];
        UA_UInt32 newMonitoredItemIds[max_ch_cnt];
        UA_Client_DataChangeNotificationCallback callbacks[max_ch_cnt];
        UA_Client_DeleteMonitoredItemCallback deleteCallbacks[max_ch_cnt];
        void *contexts[max_ch_cnt];



        for (int ch_num = 0; ch_num < max_ch_cnt; ++ch_num) {
            if (adc_ch_mask & (1 << ch_num)) {
                UA_ReadRequest request;
                UA_ReadRequest_init(&request);
                UA_ReadValueId ids[7];
                int id_pos = 0;
                UA_ReadValueId_init(&ids[id_pos]);
                ids[id_pos].attributeId = UA_ATTRIBUTEID_VALUE;
                ids[id_pos++].nodeId = VI_INT_NODEID(VI_OPCUA_NODEID_ACQ_CH_RAW_EN(ch_num));
                UA_ReadValueId_init(&ids[id_pos]);
                ids[id_pos].attributeId = UA_ATTRIBUTEID_VALUE;
                ids[id_pos++].nodeId = VI_INT_NODEID(VI_OPCUA_NODEID_ACQ_CH_RAW_FREQ(ch_num));
                UA_ReadValueId_init(&ids[id_pos]);
                ids[id_pos].attributeId = UA_ATTRIBUTEID_VALUE;
                ids[id_pos++].nodeId = VI_INT_NODEID(VI_OPCUA_NODEID_ACQ_CH_RAW_FREQ_GM(ch_num));
                UA_ReadValueId_init(&ids[id_pos]);
                ids[id_pos].attributeId = UA_ATTRIBUTEID_VALUE;
                ids[id_pos++].nodeId = VI_INT_NODEID(VI_OPCUA_NODEID_ACQ_CH_RAW_K_E(ch_num));
                UA_ReadValueId_init(&ids[id_pos]);
                ids[id_pos].attributeId = UA_ATTRIBUTEID_VALUE;
                ids[id_pos++].nodeId = VI_INT_NODEID(VI_OPCUA_NODEID_ACQ_CH_RAW_B_E(ch_num));
                UA_ReadValueId_init(&ids[id_pos]);
                ids[id_pos].attributeId = UA_ATTRIBUTEID_VALUE;
                ids[id_pos++].nodeId = VI_INT_NODEID(VI_OPCUA_NODEID_ACQ_CH_RAW_K_P(ch_num));
                UA_ReadValueId_init(&ids[id_pos]);
                ids[id_pos].attributeId = UA_ATTRIBUTEID_VALUE;
                ids[id_pos++].nodeId = VI_INT_NODEID(VI_OPCUA_NODEID_ACQ_CH_RAW_B_P(ch_num));

                request.nodesToRead = ids;
                request.nodesToReadSize = id_pos;

                UA_ReadResponse response = UA_Client_Service_read(m_ctx, request);
                if (response.responseHeader.serviceResult != UA_STATUSCODE_GOOD) {
                    err = ViModuleOpcUaErrors::acqChParamsRead(response.responseHeader.serviceResult);
                } else {
                    int res_pos = 0;
                    bool en = *(UA_Boolean*)response.results[res_pos++].value.data;
                    double freq_loc = *(UA_Double *)response.results[res_pos++].value.data;
                    double freq_gm = *(UA_Double *)response.results[res_pos++].value.data;
                    double k_e = *(UA_Double*)response.results[res_pos++].value.data;
                    double b_e = *(UA_Double *)response.results[res_pos++].value.data;
                    double k_p = *(UA_Double *)response.results[res_pos++].value.data;
                    double b_p = *(UA_Double *)response.results[res_pos++].value.data;

                    if (!en) {
                        err = ViErrors::acqChNotEnabled(ch_num);
                    } else {
                        quint32 ch_block_size = block_time_ms * freq_loc / 1000;
                        if (ch_block_size > VI_OPCUA_ACQ_CH_RAW_DATA_BLOCK_SIZE_MAX) {
                            ch_block_size = VI_OPCUA_ACQ_CH_RAW_DATA_BLOCK_SIZE_MAX;
                        }
                        quint32 ch_block_time_ms = static_cast<quint32>(static_cast<double>(ch_block_size) * 1000 / freq_loc + 0.5);



                        StreamChCtx *chCtx {new StreamChCtx{}};                        
                        ViAcqChannelConvInfo conv;
                        conv.setElec(ViAcqConvCoefs{k_e, b_e});
                        conv.setPhys(ViAcqConvCoefs{k_p, b_p});

                        chCtx->info.setChNum(ch_num);
                        chCtx->info.setNominalFreq(freq_loc);
                        chCtx->info.setGmFreq(freq_gm);
                        chCtx->info.setConv(conv);
                        chCtx->info.setBlockTimeMs(ch_block_time_ms);
                        chCtx->info.setBlockSamplesCnt(ch_block_size);

                        const UA_NodeId adcRawNodeId = VI_INT_NODEID(VI_OPCUA_NODEID_ACQ_CH_RAW_DATA(ch_num));
                        items[ch_cnt] = UA_MonitoredItemCreateRequest_default(adcRawNodeId);
                        items[ch_cnt].monitoringMode = UA_MONITORINGMODE_REPORTING;
                        items[ch_cnt].requestedParameters.samplingInterval = ch_block_time_ms;
                        items[ch_cnt].requestedParameters.queueSize = qMax(4U, static_cast<UA_UInt32>(m_buf_time_ms/ch_block_time_ms));
                        items[ch_cnt].requestedParameters.discardOldest = false;

                        contexts[ch_cnt] = chCtx;
                        callbacks[ch_cnt] = acqStreamDataHandler;
                        deleteCallbacks[ch_cnt] = nullptr;

                        unsigned ch_blocks_cnt =  m_buf_time_ms / ch_block_time_ms;

                        chCtx->samples.resize(ch_blocks_cnt * ch_block_size);
                        chCtx->tstmps.resize(ch_blocks_cnt);

                        m_chs += chCtx;
                        streamChsInfo += chCtx->info;
                        ++ch_cnt;
                    }
                }
                UA_ReadResponse_clear(&response);
            }
        }

        if (ch_cnt > 0) {
            UA_CreateSubscriptionRequest request = UA_CreateSubscriptionRequest_default();
            request.requestedPublishingInterval = streamChsInfo.at(0).blockTimeMs();
            UA_CreateSubscriptionResponse response = UA_Client_Subscriptions_create(m_ctx, request, this,
                                                                                    NULL, NULL);
            if(response.responseHeader.serviceResult != UA_STATUSCODE_GOOD) {
                err = ViModuleOpcUaErrors::acqChSubscriptionCreate(response.responseHeader.serviceResult);
            } else {
                m_sub_id = response.subscriptionId;

                UA_CreateMonitoredItemsRequest createRequest;
                UA_CreateMonitoredItemsRequest_init(&createRequest);
                createRequest.subscriptionId = m_sub_id;
                createRequest.timestampsToReturn = UA_TIMESTAMPSTORETURN_SOURCE;
                createRequest.itemsToCreate = items;
                createRequest.itemsToCreateSize = ch_cnt;

                UA_CreateMonitoredItemsResponse createResponse =
                    UA_Client_MonitoredItems_createDataChanges(m_ctx, createRequest, contexts,
                                                               callbacks, deleteCallbacks);
                if (createResponse.responseHeader.serviceResult != UA_STATUSCODE_GOOD) {
                    err = ViModuleOpcUaErrors::acqChMonItemsCreate(createResponse.responseHeader.serviceResult);
                } else {
                    for (int i = 0; (i < ch_cnt) && err.isSuccess(); ++i) {
                        if (createResponse.results[i].statusCode == UA_STATUSCODE_GOOD) {
                            m_chs[i]->monItemId = createResponse.results[i].monitoredItemId;
                            m_chs[i]->isActive = true;
                        } else {
                            err = ViModuleOpcUaErrors::acqChMonItemsCreate(createResponse.results[i].statusCode);
                        }
                    }

                }
            }
        }
    }

    void ViModuleOpcUaConnection::acqStreamReadStop(LQError &err) {

        UA_DeleteMonitoredItemsRequest deleteRequest;
        std::unique_ptr<UA_UInt32[]> monIds {new UA_UInt32[m_chs.size()]};
        for (int i = 0; i < m_chs.size(); ++i) {
            monIds[i] = m_chs.at(i)->monItemId;
        }
        UA_DeleteMonitoredItemsRequest_init(&deleteRequest);
        deleteRequest.subscriptionId = m_sub_id;
        deleteRequest.monitoredItemIds = monIds.get();
        deleteRequest.monitoredItemIdsSize = m_chs.size();

        UA_DeleteMonitoredItemsResponse deleteResponse =
            UA_Client_MonitoredItems_delete(m_ctx, deleteRequest);


        UA_DeleteMonitoredItemsResponse_clear(&deleteResponse);

        UA_Client_Subscriptions_deleteSingle(m_ctx, m_sub_id);
        qDeleteAll(m_chs);
        m_chs.clear();
    }

    void ViModuleOpcUaConnection::acqStreamProgress(int tout)  {
        UA_Client_run_iterate(m_ctx, tout);
    }

    void ViModuleOpcUaConnection::acqStreamGetData(int ch_idx, int32_t *data, int size, unsigned int tout,
                                                   int &recvd_size, ViTimeStamp *lastTs, LQError &err) {
        StreamChCtx *chCtx {m_chs.at(ch_idx)};


        QElapsedTimer tmr;
        tmr.start();
        bool tout_expired = false;

        if (!chCtx->err.isSuccess()) {
            err = chCtx->err;
        }
        while ((chCtx->cur_samples_cnt < size) && !tout_expired && err.isSuccess()) {
            int rem_time = tout - tmr.elapsed();
            if (rem_time < 1) {
                rem_time = 1;
                tout_expired = true;
            }
            UA_Client_run_iterate(m_ctx, rem_time);
            if (!chCtx->err.isSuccess()) {
                err = chCtx->err;
            }
        }

        chCtx->mutex.lock();
        int cpy_size = qMin(chCtx->cur_samples_cnt, size);
        if (cpy_size > 0) {
            int get_pos  = chCtx->samples_put_pos - chCtx->cur_samples_cnt;
            if (get_pos < 0) {
                get_pos += chCtx->samples.size();
            }

            int end_size = chCtx->samples.size() - get_pos;
            int ts_pos;
            int ts_offs {0};
            if (cpy_size <= end_size) {
                memcpy(data, &chCtx->samples.at(get_pos), cpy_size * sizeof(data[0]));
                ts_pos = (get_pos + cpy_size - 1) / chCtx->info.blockSamplesCnt();
                int block_rem = (get_pos + cpy_size) %  chCtx->info.blockSamplesCnt();
                if (block_rem != 0) {
                    ts_offs =  - (chCtx->info.blockSamplesCnt() - block_rem);
                }
            } else {
                int rem_size = cpy_size - end_size;
                memcpy(data, &chCtx->samples.at(get_pos), end_size * sizeof(data[0]));
                memcpy(&data[end_size], &chCtx->samples.at(0), rem_size * sizeof(data[0]));
                ts_pos = (rem_size - 1) / chCtx->info.blockSamplesCnt();
                int block_rem = rem_size %  chCtx->info.blockSamplesCnt();
                if (block_rem != 0) {
                    ts_offs =  -(chCtx->info.blockSamplesCnt() - block_rem);
                }
            }
            chCtx->cur_samples_cnt -= cpy_size;

            if (ts_pos == 0) {
                ts_pos = chCtx->tstmps.size() - 1;
            } else {
                --ts_pos;
            }

            if (lastTs) {
                ViTimeStamp ts = chCtx->tstmps.at(ts_pos);
                if (ts_offs != 0) {
                    ts.addNs(ts_offs * 1e9/chCtx->info.gmFreq());
                }
                *lastTs = ts;
            }

            if (!chCtx->ov_idx_list.isEmpty()) {
                int ov_idx = chCtx->ov_idx_list.first();
                if (((ov_idx >= get_pos) && (ov_idx < (get_pos + cpy_size)))
                    || ((cpy_size > end_size) && (ov_idx < (cpy_size - end_size)))) {
                    err = ViErrors::dataOverflow();
                }
            }
        }
        chCtx->mutex.unlock();
        recvd_size = cpy_size;
    }

    void ViModuleOpcUaConnection::acqStreamDataHandler(UA_Client *client, UA_UInt32 subId, void *subContext, UA_UInt32 monId, void *monContext, UA_DataValue *value) {
        StreamChCtx *chCtx = reinterpret_cast<StreamChCtx *>(monContext);
        if (value->status & UA_STATUSCODE_BAD) {
            if (chCtx->err.isSuccess()) {
                chCtx->err = ViModuleOpcUaErrors::acqChDataRecv(value->status);
                chCtx->err_idx = chCtx->samples_put_pos;
            }
        } else if (UA_Variant_hasArrayType(&value->value, &UA_TYPES[UA_TYPES_INT32])) {

            QMutexLocker lock{&chCtx->mutex};
            bool overflow = false;
            UA_Int32 *raw_data = (UA_Int32 *) value->value.data;
            int put_size = value->value.arrayLength;            
            int free_size = chCtx->samples.size() - chCtx->cur_samples_cnt;
            if (free_size < put_size) {
                put_size = free_size;
                overflow = true;
            }

            if (put_size > 0) {
                int ts_put_pos = chCtx->samples_put_pos/chCtx->info.blockSamplesCnt();

                ViTimeStamp &ts = chCtx->tstmps[ts_put_pos];
                if (!value->hasSourceTimestamp) {
                    ts.setIsValid(false);
                } else {
                    ts.setIsValid(true);
                    UA_DateTime time = value->sourceTimestamp;

                    ts.setIsGlobal(time > 400ULL * 365 * 24 * 3600 * OPCUA_TSTMP_TICK_PER_SEC);

                    ts.setSeconds(time / OPCUA_TSTMP_TICK_PER_SEC);
                    ts.setNSec((time % OPCUA_TSTMP_TICK_PER_SEC) * (1000000000/OPCUA_TSTMP_TICK_PER_SEC));
                    ts.setNSecFract(0);

                    if (ts.isGlobal()) {

                        ts.setUtcTime(QDateTime::fromMSecsSinceEpoch(time / (OPCUA_TSTMP_TICK_PER_SEC / 1000) - OPCUA_TSTMP_SEC_TO_UNIX_EPOCH*1000));
                        ts.setUtcOffset(37);
                    }
                }


                int end_size = chCtx->samples.size() - chCtx->samples_put_pos;
                if (put_size <= end_size) {
                    memcpy(&chCtx->samples.data()[chCtx->samples_put_pos], raw_data, sizeof(UA_Int32)*put_size);
                    chCtx->samples_put_pos += put_size;
                    if (chCtx->samples_put_pos == chCtx->samples.size()) {
                        chCtx->samples_put_pos = 0;
                    }
                } else {
                    int rem_size = put_size - end_size;
                    memcpy(&chCtx->samples.data()[chCtx->samples_put_pos], raw_data, sizeof(UA_Int32)*end_size);
                    memcpy(&chCtx->samples.data()[0], &raw_data[end_size], sizeof(UA_Int32)*rem_size);
                    chCtx->samples_put_pos = rem_size;
                }

                chCtx->cur_samples_cnt += put_size;
            }

            if ((value->status & UA_STATUSCODE_INFOTYPE_DATAVALUE) && (value->status & UA_STATUSCODE_INFOBITS_OVERFLOW)) {
                overflow = true;
            }

            if (overflow) {
                if (chCtx->ov_idx_list.isEmpty() || (chCtx->ov_idx_list.last() != chCtx->samples_put_pos)) {
                    chCtx->ov_idx_list += chCtx->samples_put_pos;
                }
            }
        }
    }



}
