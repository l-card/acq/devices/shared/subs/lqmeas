#ifndef LQMEAS_VIMODULEINFO_H
#define LQMEAS_VIMODULEINFO_H

#include "lqmeas/devs/DeviceInfo.h"
#include "ViModuleTypeInfo.h"
#include "QStringVersion.h"

namespace LQMeas {
    class ViModuleTypeInfo;
    class ViNetAutodiscoveryRecordInfo;

    class ViModuleInfo : public DeviceInfo {
    public:
        explicit ViModuleInfo(const ViModuleTypeInfo &type = ViModuleTypeInfo::unknownType(),
                              const QString &serial = QString{},
                              const QString &fwVer = QString{},
                              const QString &clkid = QString{});
        explicit ViModuleInfo(const ViNetAutodiscoveryRecordInfo &recInfo);
        ViModuleInfo(const ViModuleInfo &info);

        DeviceInfo *clone() const override {return new ViModuleInfo{*this};}

        const ViModuleTypeInfo &viModuleType() const;
        const QString &mcuFwVersion() const {return m_mcu_fw_version.text();}
        const QString &clkIdText() const {return m_clk_id_text;}

        bool supportDiagnosticInfo() const {return m_mcu_fw_version >= QStringVersion{{0,0,10}};}
        bool supportDinState() const {return m_mcu_fw_version >= QStringVersion{{0,0,10}};}
        bool supportDoutState() const {return m_mcu_fw_version >= QStringVersion{{0,0,10}};}
        bool supportTimeInfo() const  {return m_mcu_fw_version >= QStringVersion{{0,0,10}};}
        bool supportAllRdyWfIds() const  {return m_mcu_fw_version >= QStringVersion{{0,0,13}};}
        bool supportLocalTimeInfo() const  {return m_mcu_fw_version >= QStringVersion{{0,0,13}};}
        bool supportWfTime() const {return m_mcu_fw_version >= QStringVersion{{0,0,13}};}
        bool supportWfGmFreq() const {return m_mcu_fw_version >= QStringVersion{{0,0,13}};}
    protected:
        void protSave(QJsonObject &infoObj) const override;
        void protLoad(const QJsonObject &infoObj) override;
    private:
        QStringVersion m_mcu_fw_version;
        QString m_clk_id_text;
    };
}

#endif // LQMEAS_VIMODULEINFO_H
