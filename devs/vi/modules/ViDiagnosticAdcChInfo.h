#ifndef LQMEAS_VIDIAGNOSTICADCCHINFO_H
#define LQMEAS_VIDIAGNOSTICADCCHINFO_H

#include <QString>
#include <QObject>

namespace LQMeas {
    class Unit;

    class ViDiagnosticAdcChInfo {
    public:
        virtual quint8 addr() const = 0;
        virtual QString name() const = 0;
        virtual const Unit& unit() const = 0;
    };




    class ViDiagnosticAdcStdChsInfo : public QObject {
        Q_OBJECT
    public:
        static const ViDiagnosticAdcChInfo &vref();
        static const ViDiagnosticAdcChInfo &tmcu();
        static const ViDiagnosticAdcChInfo &upow();

        static const ViDiagnosticAdcChInfo &hpRel(int ch_num);
    };

}

#endif // LQMEAS_VIDIAGNOSTICADCCHINFO_H
