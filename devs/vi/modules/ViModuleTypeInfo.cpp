#include "ViModuleTypeInfo.h"
#include "ViDiagnosticAdcChInfo.h"
#include "lqmeas_config.h"
#include "lqmeas/units/std/Voltage.h"
#include "lqmeas/devs/DeviceTypeStdSeries.h"
#include "../protocol/vi_module_types.h"
#include "../protocol/vi_module_opcfg.h"
#include "../config/acq/sensor/ViAcqSensorConType.h"
#ifdef LQMEAS_DEV_LVIMS
#include "lvims/icp/LViMS_ICP_TypeInfo.h"
#include "lvims/nps/LViMS_NPS_TypeInfo.h"
#include "lvims/rel/LViMS_REL_TypeInfo.h"
#include "lvims/switch/LViMS_SWITCH_TypeInfo.h"
#endif
#ifdef LQMEAS_DEV_VIB4
#include "vib4/icp/Vib4_ICP_TypeInfo.h"
#include "vib4/nps/Vib4_NPS_TypeInfo.h"
#include "vib4/rel/Vib4_REL_TypeInfo.h"
#include "vib4/switch/Vib4_SWITCH_TypeInfo.h"
#endif
#include "lqmeas/devs/DeviceInterface.h"
#include <QStringBuilder>

namespace LQMeas {
    ViModuleTypeInfo::ViModuleTypeInfo(const QString &typeGroupName, const t_vi_module_type_descr *type_descr, const t_vi_system_type_descr *sys_descr) :
        m_group_type_name{typeGroupName}, m_type_descr{type_descr}, m_sys_descr{sys_descr} {

    }

    const ViModuleTypeInfo &ViModuleTypeInfo::getDevType(const t_vi_module_type_descr *type_descr, const t_vi_system_type_descr *sys_descr) {
        QList<const DeviceTypeInfo *> fullList  = QList<const DeviceTypeInfo *>{}
#ifdef LQMEAS_DEV_LVIMS
            + LViMS_ICP_TypeInfo::defaultTypeInfo().modificationList()
            + LViMS_NPS_TypeInfo::defaultTypeInfo().modificationList()
            + LViMS_REL_TypeInfo::defaultTypeInfo().modificationList()
            + LViMS_SWITCH_TypeInfo::defaultTypeInfo().modificationList()
#endif
#ifdef LQMEAS_DEV_VIB4
            + Vib4_ICP_TypeInfo::defaultTypeInfo().modificationList()
            + Vib4_NPS_TypeInfo::defaultTypeInfo().modificationList()
            + Vib4_REL_TypeInfo::defaultTypeInfo().modificationList()
            + Vib4_SWITCH_TypeInfo::defaultTypeInfo().modificationList()
#endif
            ;
        ;
        const auto it {std::find_if(fullList.cbegin(), fullList.cend(),
                         [type_descr, sys_descr](const DeviceTypeInfo *type){
            return (static_cast<const ViModuleTypeInfo *>(type)->m_type_descr == type_descr)
                   && (static_cast<const ViModuleTypeInfo *>(type)->m_sys_descr == sys_descr);
        })};
        return it != fullList.cend() ? static_cast<const ViModuleTypeInfo &>(**it) : unknownType();
    }

    const ViModuleTypeInfo &ViModuleTypeInfo::getDevType(quint16 module_id, quint8 modification, quint8 board_type, quint8 rev, quint16 sys_id)  {
        return getDevType(vi_module_type_get_descr(module_id, modification, board_type, rev),
                          vi_system_type_get_descr(sys_id));
    }

    const ViModuleTypeInfo &ViModuleTypeInfo::unknownType() {
        static const ViModuleTypeInfo type{QStringLiteral("Unknown"),
                                              vi_module_type_get_descr(VI_MODULE_TYPE_INVALID, 0, 0, 0),
                                              vi_system_type_default()};
        return type;
    }

    QString ViModuleTypeInfo::deviceTypeName() const {
        return QString::fromUtf8(m_sys_descr->loc_name) %
               QString::fromUtf8(m_sys_descr->loc_name_delim) %
               typeGroupName();
    }

    QString ViModuleTypeInfo::deviceModificationName() const {
        return QString::fromUtf8(m_sys_descr->loc_name) %
               QString::fromUtf8(m_sys_descr->loc_name_delim) %
               QString::fromUtf8(m_type_descr->name);
    }

    QString ViModuleTypeInfo::deviceModificationIdName() const {
        return QString::fromUtf8(m_sys_descr->id_name) %
               QChar('-') %
               QString::fromUtf8(m_type_descr->name);
    }

    const DeviceTypeSeries &ViModuleTypeInfo::deviceTypeSeries() const {
        return DeviceTypeStdSeries::unknown();
    }

    QString ViModuleTypeInfo::hardwareName() const {
        return QString::fromUtf8(m_type_descr->hwname);
    }

    QList<const DeviceTypeInfo *> ViModuleTypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list {
            &unknownType()
        };
        return list;
    }

    bool ViModuleTypeInfo::isCompatible(const DeviceTypeInfo &type) const  {
        const ViModuleTypeInfo *viType {dynamic_cast<const ViModuleTypeInfo *>(&type)};
        return viType && viType->typeGroupName() == typeGroupName();
    }

    int ViModuleTypeInfo::acqChCnt() const {
        return m_type_descr->adc ? m_type_descr->adc->ch_cnt : 0;
    }

    double ViModuleTypeInfo::acqAdcFreq() const {
        return m_type_descr->adc ? m_type_descr->adc->adc_freq : 0;
    }

    double ViModuleTypeInfo::acqAdcMaxValue() const {
        return m_type_descr->adc ? (m_type_descr->adc->range_val > 0) ? m_type_descr->adc->range_val : 0 : 0;
    }

    double ViModuleTypeInfo::acqAdcMinValue() const {
        return m_type_descr->adc ? (m_type_descr->adc->range_val < 0) ? m_type_descr->adc->range_val : 0 : 0;
    }

    bool ViModuleTypeInfo::acqAdcHighFreqSupport() const {
        return (m_type_descr->adc && (m_type_descr->adc->flags & VI_MODULE_FEAT_ADC_FLAG_FAST_MODE));
    }

    const Unit &ViModuleTypeInfo::acqValueUnit() const {
        return LQMeas::Units::Voltage::V();
    }

    QList<const ViAcqSensorConType *> ViModuleTypeInfo::acqSensorConnectionMethods() const {
        QList<const ViAcqSensorConType *> ret;
        if (m_type_descr->adc) {
            for (int i {0}; i < m_type_descr->adc->cont_types_cnt; ++i) {
                if (m_type_descr->adc->con_types[i] == VI_SENSOR_CON_TYPE_ICP) {
                    ret += &ViAcqSensorConTypes::ICP();
                } else if (m_type_descr->adc->con_types[i] == VI_SENSOR_CON_TYPE_VOUT_USUP_POS) {
                    ret += &ViAcqSensorConTypes::VOutPosSup();
                } else if (m_type_descr->adc->con_types[i] == VI_SENSOR_CON_TYPE_VOUT_USUP_NEG) {
                    ret += &ViAcqSensorConTypes::VOutNegSup();
                }
            }
        }
        return ret;
    }

    int ViModuleTypeInfo::doutLpChCnt() const  {
        return m_type_descr->lp_relay_cnt;
    }

    int ViModuleTypeInfo::doutHpChCnt() const {
        return m_type_descr->hp_relay_cnt;
    }

    int ViModuleTypeInfo::dinChCnt() const {
        return m_type_descr->in_discrete_ch_cnt;
    }

    int ViModuleTypeInfo::netPortsCnt() const {
        return m_type_descr->net_info->ports_cnt;
    }

    QList<const ViDiagnosticAdcChInfo *> ViModuleTypeInfo::diagnAdcChs() const  {
        static const QList<const ViDiagnosticAdcChInfo *> stdList {
            &ViDiagnosticAdcStdChsInfo::vref(),
            &ViDiagnosticAdcStdChsInfo::tmcu(),
            &ViDiagnosticAdcStdChsInfo::upow(),
        };
        return stdList;
    }

    QList<const DeviceInterface *> ViModuleTypeInfo::supportedConInterfaces() const {
        static const QList<const DeviceInterface *> ifaces {
            &DeviceInterfaces::ethernet()
        };
        return ifaces;
    }
}

