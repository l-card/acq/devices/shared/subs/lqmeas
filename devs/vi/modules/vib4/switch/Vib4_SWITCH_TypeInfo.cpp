#include "Vib4_SWITCH_TypeInfo.h"
#include "lqmeas/devs/vi/protocol/vi_module_types.h"

namespace LQMeas {
    const Vib4_SWITCH_TypeInfo &Vib4_SWITCH_TypeInfo::defaultTypeInfo() {
        return typeInfo_SWITCH();
    }

    const Vib4_SWITCH_TypeInfo &Vib4_SWITCH_TypeInfo::typeInfo_SWITCH() {
        static const Vib4_SWITCH_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_SWITCH, 0, VI_MODULE_BOARD_TYPE_DEFAULT, 5)};
        return info;
    }

    const Vib4_SWITCH_TypeInfo &Vib4_SWITCH_TypeInfo::typeInfo_SWITCH_v2()  {
        static const Vib4_SWITCH_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_SWITCH, 0, VI_MODULE_BOARD_TYPE_DEFAULT, 3)};
        return info;
    }



    QList<const DeviceTypeInfo *> Vib4_SWITCH_TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list {
            &typeInfo_SWITCH(),
            &typeInfo_SWITCH_v2(),
        };
        return list;
    }

    QList<const DeviceTypeInfo *> Vib4_SWITCH_TypeInfo::visibleModificationList() const {
        static const QList<const DeviceTypeInfo *> list {
            &typeInfo_SWITCH(),
        };
        return list;
    }

    Vib4_SWITCH_TypeInfo::Vib4_SWITCH_TypeInfo(const t_vi_module_type_descr *type_descr) :
        Vib4SysModuleType{QStringLiteral("SWITCH"), type_descr} {

    }
}
