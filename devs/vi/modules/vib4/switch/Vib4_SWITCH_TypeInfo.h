#ifndef LQMEAS_VIB4_SWITCH_TYPEINFO_H
#define LQMEAS_VIB4_SWITCH_TYPEINFO_H

#include "lqmeas/devs/vi/modules/vib4/Vib4SysModuleType.h"

namespace LQMeas {
    class Vib4_SWITCH_TypeInfo : public Vib4SysModuleType {
    public:
        static const Vib4_SWITCH_TypeInfo &defaultTypeInfo();

        static const Vib4_SWITCH_TypeInfo &typeInfo_SWITCH();

        static const Vib4_SWITCH_TypeInfo &typeInfo_SWITCH_v2();

        QList<const DeviceTypeInfo *> modificationList() const override;
        QList<const DeviceTypeInfo *> visibleModificationList() const override;
    private:
        Vib4_SWITCH_TypeInfo(const t_vi_module_type_descr *type_descr);
    };
}


#endif // VIB4_SWITCH_TYPEILQMEAS_VIB4_SWITCH_TYPEINFO_HNFO_H
