#ifndef LQMEAS_VIB4NETAUTODISCOVERYSERVICETYPE_H
#define LQMEAS_VIB4NETAUTODISCOVERYSERVICETYPE_H

#include "lqmeas/devs/vi/netdiscovery/ViNetAutodiscoveryServiceType.h"

namespace LQMeas {
    class Vib4NetAutodiscoveryServiceType : public ViNetAutodiscoveryServiceType {
    public:
        static const ViNetAutodiscoveryServiceType &type();
        static const QString &typeName();

        const QString name() const override { return typeName(); }
        NetAutodiscoveryRecordInfo *createRecord() const override;
    protected:
        Vib4NetAutodiscoveryServiceType();
    };
}

#endif // LQMEAS_VIB4NETAUTODISCOVERYSERVICETYPE_H
