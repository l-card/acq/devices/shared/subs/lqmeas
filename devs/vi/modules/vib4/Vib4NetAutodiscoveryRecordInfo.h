#ifndef LQMEAS_VIB4NETAUTODISCOVERYRECORDINFO_H
#define LQMEAS_VIB4NETAUTODISCOVERYRECORDINFO_H

#include "lqmeas/devs/vi/netdiscovery/ViNetAutodiscoveryRecordInfo.h"

namespace LQMeas {
    class Vib4NetAutodiscoveryRecordInfo : public ViNetAutodiscoveryRecordInfo {
    public:
        explicit Vib4NetAutodiscoveryRecordInfo() {}
        explicit Vib4NetAutodiscoveryRecordInfo(const Vib4NetAutodiscoveryRecordInfo &other) :
            ViNetAutodiscoveryRecordInfo{other} {}

        const NetAutodiscoveryServiceType &serviceType() const override;
        NetAutodiscoveryRecordInfo *clone() const override {
            return new Vib4NetAutodiscoveryRecordInfo{*this};
        }
    };
}

#endif // LQMEAS_VIB4NETAUTODISCOVERYRECORDINFO_H
