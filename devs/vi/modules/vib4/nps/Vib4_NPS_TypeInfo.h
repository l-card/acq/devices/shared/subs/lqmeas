#ifndef LQMEAS_VIB4_NPS_TYPEINFO_H
#define LQMEAS_VIB4_NPS_TYPEINFO_H

#include "lqmeas/devs/vi/modules/vib4/Vib4SysModuleType.h"

namespace LQMeas {
    class Vib4_NPS_TypeInfo : public Vib4SysModuleType {
    public:
        static const Vib4_NPS_TypeInfo &defaultTypeInfo();

        static const Vib4_NPS_TypeInfo &typeInfo_NPS();
        static const Vib4_NPS_TypeInfo &typeInfo_NPS_1();

        static const Vib4_NPS_TypeInfo &typeInfo_NPS_v2();
        static const Vib4_NPS_TypeInfo &typeInfo_NPS_1_v2();

        QList<const DeviceTypeInfo *> modificationList() const override;
        QList<const DeviceTypeInfo *> visibleModificationList() const override;
    private:
        Vib4_NPS_TypeInfo(const t_vi_module_type_descr *type_descr);
    };
};

#endif // LQMEAS_VIB4_NPS_TYPEINFO_H
