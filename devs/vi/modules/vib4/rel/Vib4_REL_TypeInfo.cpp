#include "Vib4_REL_TypeInfo.h"
#include "lqmeas/devs/vi/protocol/vi_module_types.h"
#include "lqmeas/devs/vi/modules/ViDiagnosticAdcChInfo.h"

namespace LQMeas {
    const Vib4_REL_TypeInfo &Vib4_REL_TypeInfo::defaultTypeInfo() {
        return typeInfo_REL();
    }

    const Vib4_REL_TypeInfo &Vib4_REL_TypeInfo::typeInfo_REL() {
        static const Vib4_REL_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_REL, 0, VI_MODULE_BOARD_TYPE_DEFAULT, 4)};
        return info;
    }

    const Vib4_REL_TypeInfo &Vib4_REL_TypeInfo::typeInfo_REL_v2() {
        static const Vib4_REL_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_REL, 0, VI_MODULE_BOARD_TYPE_DEFAULT, 3)};
        return info;
    }

    QList<const DeviceTypeInfo *> Vib4_REL_TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list {
            &typeInfo_REL(),
            &typeInfo_REL_v2(),
        };
        return list;
    }

    QList<const DeviceTypeInfo *> Vib4_REL_TypeInfo::visibleModificationList() const {
        static const QList<const DeviceTypeInfo *> list {
            &typeInfo_REL(),
        };
        return list;
    }

    QList<const ViDiagnosticAdcChInfo *> Vib4_REL_TypeInfo::diagnAdcChs() const {
        return Vib4SysModuleType::diagnAdcChs() + QList<const ViDiagnosticAdcChInfo *> {
                &ViDiagnosticAdcStdChsInfo::hpRel(0),
                &ViDiagnosticAdcStdChsInfo::hpRel(1),
                &ViDiagnosticAdcStdChsInfo::hpRel(2),
                &ViDiagnosticAdcStdChsInfo::hpRel(3)
               };
    }

    Vib4_REL_TypeInfo::Vib4_REL_TypeInfo(const t_vi_module_type_descr *type_descr) :
        Vib4SysModuleType{QStringLiteral("REL"), type_descr} {

    }
}
