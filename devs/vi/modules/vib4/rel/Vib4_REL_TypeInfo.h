#ifndef LQMEAS_VIB4_REL_TYPEINFO_H
#define LQMEAS_VIB4_REL_TYPEINFO_H

#include "lqmeas/devs/vi/modules/vib4/Vib4SysModuleType.h"

namespace LQMeas {
    class Vib4_REL_TypeInfo : public Vib4SysModuleType {
    public:
        static const Vib4_REL_TypeInfo &defaultTypeInfo();

        static const Vib4_REL_TypeInfo &typeInfo_REL();

        static const Vib4_REL_TypeInfo &typeInfo_REL_v2();


        QList<const ViDiagnosticAdcChInfo *> diagnAdcChs() const override;

        QList<const DeviceTypeInfo *> modificationList() const override;
        QList<const DeviceTypeInfo *> visibleModificationList() const override;
    private:
        Vib4_REL_TypeInfo(const t_vi_module_type_descr *type_descr);
    };
}


#endif // LQMEAS_VIB4_REL_TYPEINFO_H
