#include "Vib4NetAutodiscoveryRecordInfo.h"
#include "Vib4NetAutodiscoveryServiceType.h"

namespace LQMeas {
    const NetAutodiscoveryServiceType &Vib4NetAutodiscoveryRecordInfo::serviceType() const {
        return Vib4NetAutodiscoveryServiceType::type();
    }
}
