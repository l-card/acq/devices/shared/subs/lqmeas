#include "Vib4SysModuleType.h"
#include "lqmeas/devs/vi/protocol/vi_system_types.h"
#include "lqmeas/devs/DeviceTypeStdSeries.h"

namespace LQMeas {

    Vib4SysModuleType::Vib4SysModuleType(const QString &moduleGroupType, const t_vi_module_type_descr *type_descr) :
        ViModuleTypeInfo{moduleGroupType, type_descr, vi_system_type_get_descr(VI_SYSTEM_TYPE_VIB4)} {

    }

    const DeviceTypeSeries &Vib4SysModuleType::deviceTypeSeries() const {
        return DeviceTypeStdSeries::vib4();
    }
}
