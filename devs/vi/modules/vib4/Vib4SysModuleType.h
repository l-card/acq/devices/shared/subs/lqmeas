#ifndef LQMEAS_VIB4SYSMODULETYPE_H
#define LQMEAS_VIB4SYSMODULETYPE_H


#include "lqmeas/devs/vi/modules/ViModuleTypeInfo.h"

namespace LQMeas {
    class Vib4SysModuleType : public ViModuleTypeInfo {
    protected:
        Vib4SysModuleType(const QString &moduleGroupType, const t_vi_module_type_descr *type_descr);

        const DeviceTypeSeries &deviceTypeSeries() const override;
    };
}


#endif // LQMEAS_VIB4SYSMODULETYPE_H
