#ifndef LQMEAS_VIB4_ICP_TYPEINFO_H
#define LQMEAS_VIB4_ICP_TYPEINFO_H

#include "lqmeas/devs/vi/modules/vib4/Vib4SysModuleType.h"

namespace LQMeas {
    class Vib4_ICP_TypeInfo : public Vib4SysModuleType {
    public:
        static const Vib4_ICP_TypeInfo &defaultTypeInfo();

        static const Vib4_ICP_TypeInfo &typeInfo_ICP10();
        static const Vib4_ICP_TypeInfo &typeInfo_ICP10_1();
        static const Vib4_ICP_TypeInfo &typeInfo_ICP4();
        static const Vib4_ICP_TypeInfo &typeInfo_ICP4_1();

        static const Vib4_ICP_TypeInfo &typeInfo_ICP10_v2();
        static const Vib4_ICP_TypeInfo &typeInfo_ICP10_1_v2();
        static const Vib4_ICP_TypeInfo &typeInfo_ICP4_v2();
        static const Vib4_ICP_TypeInfo &typeInfo_ICP4_1_v2();

        QList<const DeviceTypeInfo *> modificationList() const override;
        QList<const DeviceTypeInfo *> visibleModificationList() const override;
    private:
        Vib4_ICP_TypeInfo(const t_vi_module_type_descr *type_descr);
    };
}

#endif // LQMEAS_VIB4_ICP_TYPEINFO_H
