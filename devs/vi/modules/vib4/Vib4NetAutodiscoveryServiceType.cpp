#include "Vib4NetAutodiscoveryServiceType.h"
#include "Vib4NetAutodiscoveryRecordInfo.h"
#include "lqmeas/devs/vi/protocol/vi_system_types.h"

namespace LQMeas {
    const ViNetAutodiscoveryServiceType &Vib4NetAutodiscoveryServiceType::type() {
        static const Vib4NetAutodiscoveryServiceType t;
        return t;
    }

    const QString &Vib4NetAutodiscoveryServiceType::typeName() {
        static const QString n {QStringLiteral(VI_SYSTEM_TYPE_VIB4_SVCNAME)};
        return n;
    }

    NetAutodiscoveryRecordInfo *Vib4NetAutodiscoveryServiceType::createRecord() const {
        return new Vib4NetAutodiscoveryRecordInfo{};
    }

    Vib4NetAutodiscoveryServiceType::Vib4NetAutodiscoveryServiceType() {

    }
}
