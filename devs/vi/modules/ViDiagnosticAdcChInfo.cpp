#include "ViDiagnosticAdcChInfo.h"
#include "lqmeas/units/std/Voltage.h"
#include "lqmeas/units/std/Temperature.h"
#include "lqmeas/devs/vi/protocol/vi_module_diagn_adc.h"

namespace LQMeas {
    class ViDiagnosticAdcHpRelChInfo : public ViDiagnosticAdcChInfo {
    public:
        explicit ViDiagnosticAdcHpRelChInfo(int ch_num) : m_ch_num{ch_num} {}

        quint8 addr() const override {return VI_DIGAN_ADC_NUM_REL_STS(m_ch_num);}
        QString name() const override {return ViDiagnosticAdcStdChsInfo::tr("High Power Relay %1").arg(m_ch_num + 1);}
        const Unit& unit() const override {return LQMeas::Units::Voltage::V();}
    private:
        int m_ch_num;
    };

    const ViDiagnosticAdcChInfo &ViDiagnosticAdcStdChsInfo::vref() {
        static const class ViDiagnosticAdcChInfoVRef : public ViDiagnosticAdcChInfo {
        public:
            quint8 addr() const override {return VI_DIGAN_ADC_NUM_VREF;};
            QString name() const override {return tr("Vref");}
            const Unit& unit() const override {return LQMeas::Units::Voltage::V();}
        } info;
        return info;
    }

    const ViDiagnosticAdcChInfo &ViDiagnosticAdcStdChsInfo::tmcu() {
        static const class ViDiagnosticAdcChInfoTMcu : public ViDiagnosticAdcChInfo {
        public:
            quint8 addr() const override {return VI_DIGAN_ADC_NUM_TS;};
            QString name() const override {return tr("Tmcu");}
            const Unit& unit() const override {return LQMeas::Units::Temperature::degreeC();}
        } info;
        return info;
    }

    const ViDiagnosticAdcChInfo &ViDiagnosticAdcStdChsInfo::upow()  {
        static const class ViDiagnosticAdcChInfoUPow : public ViDiagnosticAdcChInfo {
        public:
            quint8 addr() const override {return VI_DIGAN_ADC_NUM_POW;};
            QString name() const override {return tr("Upow");}
            const Unit& unit() const override {return LQMeas::Units::Voltage::V();}
        } info;
        return info;
    }

    const ViDiagnosticAdcChInfo &ViDiagnosticAdcStdChsInfo::hpRel(int ch_num) {
        static const ViDiagnosticAdcHpRelChInfo hp_rels[] = {
            ViDiagnosticAdcHpRelChInfo{0},
            ViDiagnosticAdcHpRelChInfo{1},
            ViDiagnosticAdcHpRelChInfo{2},
            ViDiagnosticAdcHpRelChInfo{3},
            ViDiagnosticAdcHpRelChInfo{4},
            ViDiagnosticAdcHpRelChInfo{5},
            ViDiagnosticAdcHpRelChInfo{6},
            ViDiagnosticAdcHpRelChInfo{7},
        };
        return hp_rels[ch_num];
    }



}
