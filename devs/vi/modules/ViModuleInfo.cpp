#include "ViModuleInfo.h"
#include "ViModuleTypeInfo.h"
#include "lqmeas/StdConfigKeys.h"
#include "lqmeas/devs/vi/netdiscovery/ViNetAutodiscoveryRecordInfo.h"

#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String &cfgkey_board_rev {StdConfigKeys::boardRev()};
    static const QLatin1String &cfgkey_mcu_ver   {StdConfigKeys::mcuVer()};
    static const QLatin1String &cfgkey_clk_id    {StdConfigKeys::clkId()};

    ViModuleInfo::ViModuleInfo(const ViModuleTypeInfo &type, const QString &serial,
                               const QString &fwVer, const QString &clkid) :
        DeviceInfo{type, serial},
        m_mcu_fw_version{fwVer},
        m_clk_id_text{clkid} {


    }

    ViModuleInfo::ViModuleInfo(const ViNetAutodiscoveryRecordInfo &recInfo) :
        DeviceInfo{ViModuleTypeInfo::getDevType(recInfo.moduleTypeID(), recInfo.moduleModID(),
                                                  recInfo.boardTypeId(), recInfo.moduleRev(),
                                                  recInfo.moduleSystemID()), recInfo.serial()},
        m_mcu_fw_version{recInfo.firmwareVersion()},
        m_clk_id_text{recInfo.clkIdText()} {

    }

    ViModuleInfo::ViModuleInfo(const ViModuleInfo &info) :
        DeviceInfo{info},
        m_mcu_fw_version{info.mcuFwVersion()},
        m_clk_id_text{info.clkIdText()} {

    }

    const ViModuleTypeInfo &ViModuleInfo::viModuleType() const {
        return static_cast<const ViModuleTypeInfo &>(type());
    }

    void ViModuleInfo::protSave(QJsonObject &infoObj) const {
        infoObj[cfgkey_mcu_ver] = m_mcu_fw_version.text();
        infoObj[cfgkey_clk_id] = m_clk_id_text;

    }

    void ViModuleInfo::protLoad(const QJsonObject &infoObj) {
        m_mcu_fw_version = QStringVersion{infoObj[cfgkey_mcu_ver].toString()};
        m_clk_id_text = infoObj[cfgkey_clk_id].toString();
    }
}
