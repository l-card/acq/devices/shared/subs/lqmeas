#include "ViModule.h"
#include "LQError.h"
#include "lqmeas/devs/vi/netdiscovery/ViNetAutodiscoveryRecordInfo.h"
#include "lqmeas/devs/vi/config/ViModuleConfig.h"
#include "lqmeas/devs/vi/devref/ViDeviceRefIpAddr.h"
#include "lqmeas/devs/vi/devref/ViDeviceRefNetSvc.h"
#include "lqmeas/devs/vi/modules/ViModuleInfo.h"
#include "lqmeas/devs/vi/connection/mb/ViModuleMbConnection.h"
#include "lqmeas/devs/vi/ViErrors.h"
#include "lqmeas/devs/DeviceInterface.h"


namespace LQMeas {
    ViModule::~ViModule() {

    }

    QSharedPointer<const ViModuleInfo> ViModule::devspecInfo() const {
        return devInfo().staticCast<const ViModuleInfo>();
    }

    bool ViModule::isOpened() const {
        return m_con && m_con->isOpened();
    }

    DeviceRef *ViModule::createRef() const {
        return m_ipAddrRef ? static_cast<DeviceRef*>(new ViDeviceRefIpAddr{*devInfo(), *m_ipAddrRef})
               : m_netSvcRef ? static_cast<DeviceRef*>(new ViDeviceRefNetSvc{*devInfo(), *m_netSvcRef})
               : m_svcRecord ? static_cast<DeviceRef*>(new ViDeviceRefNetSvc{*devInfo(), m_svcRecord->instanceName(),
                                                                                ViModuleMbConnection::OpenParams::defaultTout()})
                             : Device::createRef();
    }

    QString ViModule::displayFullName() const {
        return QString("%1 (%2)").arg(devInfo()->type().deviceModificationName(), connectionString());
    }

    QString ViModule::connectionString() const {
        return m_ipAddrRef ? tr("IP: %1").arg(m_ipAddrRef->ipAddr().toString())
               : m_svcRecord ? tr("Net name: %1").arg(m_svcRecord->instanceName())
                             : QString{};
    }

    QString ViModule::instanceName() const {
        /** @todo  get from connection */
        return m_svcRecord ? m_svcRecord->instanceName() :
                   m_netSvcRef ? m_netSvcRef->instanceName() : QString{};
    }

    QString ViModule::connectionAddr() const {
        QMutexLocker lock{&m_dev_lock};
        return m_con ? m_con->connectionAddr() :
                m_ipAddrRef ? m_ipAddrRef->ipAddr().toString() :
                m_svcRecord ? m_svcRecord->ipv4Addr().toString() : QString{};
    }

    void ViModule::startBootMode(LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        m_con->startBootMode(defaultBootPort(), err);
    }

    void ViModule::resetRequest(LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        m_con->resetRequest(err);
    }
    
    void ViModule::ifaceSettingsRead(ViIfaceSettings &netset, LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        m_con->ifaceSettingsRead(netset, err);
    }
    
    void ViModule::ifaceSettingsWrite(const ViIfaceSettings &netset, LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        m_con->ifaceSettingsWrite(netset, err);
    }

    void ViModule::nvConfigWrite(const DeviceConfig &cfg, LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        m_con->nvConfigWrite(static_cast<const ViModuleConfig &>(cfg), err);
    }

    DeviceConfig *ViModule::nvConfigRead(LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        ViModuleConfig cfg;
        LQError rdError;
        cfg.updateDevice(this);
        m_con->nvConfigRead(cfg, rdError);
        if (rdError != ViErrors::noValidOpCfg()) {
            err = rdError;
        }
        return err.isSuccess() ? cfg.clone(this) : nullptr;
    }

    ViTimeClockId *ViModule::readModuleClockId(LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        return m_con->readModuleClockId(err);
    }

    ViAcqChannelState *ViModule::readViAcqChState(int ch_num, LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        return m_con->readAcqChState(ch_num, err);
    }

    ViModuleState *ViModule::readModuleState(LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        return m_con->readModuleState(*devspecInfo(), err);
    }

    ViDoutTypeState *ViModule::readDoutTypeState(const ViDoutChannelType &chType, LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        return m_con->readDoutTypeState(*devspecInfo(), chType, err);
    }

    ViModuleDiagnostic *ViModule::readDiagnostic(LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        return m_con->readDiagnostic(err);
    }

    ViWfInfo *ViModule::readWfInfo(quint16 wf_id, LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        return m_con->readWfInfo(*devspecInfo(), wf_id, err);
    }

    ViWfChInfo *ViModule::readWfChInfo(quint16 wf_id, quint16 ch_num, LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        return m_con->readWfChInfo(wf_id, ch_num, err);
    }

    ViWfChDataBlock *ViModule::readWfChData(quint16 wf_id, quint16 ch_num, quint32 offs, quint32 samles_cnt, LQError &err)  {
        QMutexLocker lock{&m_dev_lock};
        return m_con->readWfChData(wf_id, ch_num, offs, samles_cnt, err);
    }

    void ViModule::setAcqChStateFlag(const ViAcqChSetFlagReqData &reqData, LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        m_con->setAcqChStateFlag(reqData, err);
    }

    void ViModule::wfManualRequest(const ViWfReqData &reqData, quint16 &wf_id, LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        m_con->wfManualRequest(reqData, wf_id, err);
    }

    void ViModule::doutReset(const ViDoutResetReqData &reqData, LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        m_con->doutReset(reqData, err);
    }

    void ViModule::doutManualChange(const ViDoutManualChangeReqData &reqData, LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        m_con->doutManualChange(reqData, err);
    }

    void ViModule::protOpen(OpenFlags flags, LQError &err) {
        QMutexLocker lock{&m_dev_lock};
        if (!m_con) {
            m_con.reset(new ViModuleMbConnection{});
        }
        ViModuleMbConnection::OpenParams params;
        if (m_ipAddrRef) {
            params = ViModuleMbConnection::OpenParams{m_ipAddrRef->ipAddr().toString(),
                                                         ViModuleMbConnection::OpenParams::defaultPort(),
                                                         m_ipAddrRef->connectionTimeout()};
        } else if (m_svcRecord) {
            params = ViModuleMbConnection::OpenParams{m_svcRecord->ipv4Addr().toString(),
                                                         m_svcRecord->port(),
                                                         m_netSvcRef ? m_netSvcRef->connectionTimeout()
                                                                     : ViModuleMbConnection::OpenParams::defaultTout()};
        }
        m_con->open(params, err);
        if (err.isSuccess()) {
            std::unique_ptr<ViModuleInfo> info {m_con->readModuleInfo(err)};
            if (err.isSuccess() && info) {
                setDeviceInfo(*info);
            }
        }
    }

    void ViModule::protClose(LQError &err) {
        m_con->close();
    }

    void ViModule::protConfigure(const DeviceConfig &cfg, LQError &err) {

    }

    ViModule::ViModule(const ViDeviceRefIpAddr &ipRef) :
        Device {new ViModuleConfig{}, ipRef.devInfo(), DeviceInterfaces::ethernet()},
        m_ipAddrRef{new ViDeviceRefIpAddr{ipRef}} {

    }

    ViModule::ViModule(const ViNetAutodiscoveryRecordInfo &svcRec, int conTout) :
        Device {new ViModuleConfig{}, ViModuleInfo{svcRec}, DeviceInterfaces::ethernet()},
        m_svcRecord{static_cast<ViNetAutodiscoveryRecordInfo *>(svcRec.clone())}  {

    }

    ViModule::ViModule(const ViNetAutodiscoveryRecordInfo &svcRec, const ViDeviceRefNetSvc &netRef) :
        Device {new ViModuleConfig{}, ViModuleInfo{svcRec}, DeviceInterfaces::ethernet()},
        m_netSvcRef{new ViDeviceRefNetSvc{netRef}},
        m_svcRecord{static_cast<ViNetAutodiscoveryRecordInfo *>(svcRec.clone())} {

    }

    void ViModule::updateAutodiscoveryInfo(const ViNetAutodiscoveryRecordInfo &svcRec) {
        QMutexLocker lock{&m_dev_lock};
        m_svcRecord.reset(static_cast<ViNetAutodiscoveryRecordInfo *>(svcRec.clone()));
        setDeviceInfo(ViModuleInfo{svcRec});
    }
}
