#ifndef LQMEAS_VIMODULE_H
#define LQMEAS_VIMODULE_H

#include "lqmeas/devs/Device.h"
#include <memory>

namespace LQMeas {
    class ViNetAutodiscoveryRecordInfo;
    class ViDeviceRefIpAddr;
    class ViDeviceRefNetSvc;
    class ViModuleInfo;
    class ViModuleMbConnection;
    class ViAcqChannelState;
    class ViModuleState;
    class ViModuleDiagnostic;
    class ViAcqChSetFlagReqData;
    class ViDoutTypeState;
    class ViDoutChannelType;
    class ViDoutResetReqData;
    class ViDoutManualChangeReqData;
    class ViWfReqData;    
    class ViWfInfo;
    class ViWfChInfo;
    class ViWfChDataBlock;
    class ViIfaceSettings;
    class ViTimeClockId;

    class ViModule : public Device {
        Q_OBJECT
    public:
        ~ViModule();

        const ViDeviceRefIpAddr *ipAddrRef() const {return m_ipAddrRef.get();}
        const ViNetAutodiscoveryRecordInfo *netSvcRecordInfo() const {return m_svcRecord.get();}

        QSharedPointer<const ViModuleInfo> devspecInfo() const;

        bool isOpened() const override;

        DeviceRef *createRef() const override;
        QString displayFullName() const;
        QString connectionString() const;
        QString instanceName() const;
        QString connectionAddr() const;


        static constexpr quint16 defaultBootPort() {return 80;}

        void startBootMode(LQError &err);
        void resetRequest(LQError &err);

        void ifaceSettingsRead(ViIfaceSettings &netset, LQError &err);
        void ifaceSettingsWrite(const ViIfaceSettings &netset, LQError &err);

        void nvConfigWrite(const DeviceConfig &cfg, LQError &err);
        DeviceConfig *nvConfigRead(LQError &err);

        ViTimeClockId *readModuleClockId(LQError &err);
        ViAcqChannelState *readViAcqChState(int ch_num, LQError &err);
        ViModuleState *readModuleState(LQError &err);
        ViDoutTypeState *readDoutTypeState(const ViDoutChannelType &chType, LQError &err);
        ViModuleDiagnostic *readDiagnostic(LQError &err);
        ViWfInfo *readWfInfo(quint16 wf_id, LQError &err);
        ViWfChInfo *readWfChInfo(quint16 wf_id, quint16 ch_num, LQError &err);
        ViWfChDataBlock *readWfChData(quint16 wf_id, quint16 ch_num, quint32 offs, quint32 samles_cnt, LQError &err);

        void setAcqChStateFlag(const ViAcqChSetFlagReqData &reqData, LQError &err);
        void wfManualRequest(const ViWfReqData &reqData, quint16 &wf_id, LQError &err);
        void doutReset(const ViDoutResetReqData &reqData, LQError &err);
        void doutManualChange(const ViDoutManualChangeReqData &reqData, LQError &err);
    protected:
        void protOpen(OpenFlags flags, LQError &err) override;
        void protClose(LQError &err) override;
        void protConfigure(const DeviceConfig &cfg, LQError &err) override;
    private:


        explicit ViModule(const ViDeviceRefIpAddr &ipRef);
        explicit ViModule(const ViNetAutodiscoveryRecordInfo &svcRec, int conTout);
        explicit ViModule(const ViNetAutodiscoveryRecordInfo &svcRec, const ViDeviceRefNetSvc &netRef);

        void updateAutodiscoveryInfo(const ViNetAutodiscoveryRecordInfo &svcRec);

        std::unique_ptr<ViDeviceRefIpAddr> m_ipAddrRef;
        std::unique_ptr<ViDeviceRefNetSvc> m_netSvcRef;
        std::unique_ptr<ViNetAutodiscoveryRecordInfo> m_svcRecord;

        std::unique_ptr<ViModuleMbConnection> m_con;
        mutable QMutex m_dev_lock;

        friend class ViDeviceResolver;
    };
}

#endif // LQMEAS_VIMODULE_H
