#ifndef LQMEAS_VIMODULETYPEINFO_H
#define LQMEAS_VIMODULETYPEINFO_H

#include "lqmeas/devs/DeviceTypeInfo.h"
#include "lqmeas/devs/vi/protocol/vi_module_types_descr.h"
#include "lqmeas/devs/vi/protocol/vi_system_types_descr.h"

namespace LQMeas {
    class Unit;
    class ViAcqSensorConType;
    class ViDiagnosticAdcChInfo;

    class ViModuleTypeInfo : public DeviceTypeInfo {
    public:
        explicit ViModuleTypeInfo(const QString &typeGroupName,
                                     const t_vi_module_type_descr *type_descr,
                                     const t_vi_system_type_descr *sys_descr);

        static const ViModuleTypeInfo &getDevType(const t_vi_module_type_descr *type_descr, const t_vi_system_type_descr *sys_descr);
        static const ViModuleTypeInfo &getDevType(quint16 module_id, quint8 modification, quint8 board_type, quint8 rev, quint16 sys_id);

        static const ViModuleTypeInfo &unknownType();




        const QString &typeGroupName() const {return m_group_type_name;}
        QString deviceTypeName() const override;
        QString deviceModificationName() const override;
        QString deviceModificationIdName() const;
        const DeviceTypeSeries &deviceTypeSeries() const override;
        QString hardwareName() const;
        QList<const DeviceTypeInfo *> modificationList() const override;
        virtual bool isCompatible(const DeviceTypeInfo &type) const  override;

        int acqChCnt() const;
        double acqAdcFreq() const;
        double acqAdcMaxValue() const;
        double acqAdcMinValue() const;
        bool acqAdcHighFreqSupport() const;
        const Unit &acqValueUnit() const;
        QList<const ViAcqSensorConType *> acqSensorConnectionMethods() const;

        int doutLpChCnt() const;
        int doutHpChCnt() const;
        int dinChCnt() const;

        int netPortsCnt() const;

        virtual QList<const ViDiagnosticAdcChInfo *> diagnAdcChs() const;
        QList<const DeviceInterface *> supportedConInterfaces() const override;
    private:
        const QString m_group_type_name;
        const t_vi_module_type_descr *m_type_descr;
        const t_vi_system_type_descr *m_sys_descr;
    };
}

#endif // LQMEAS_VIMODULETYPEINFO_H
