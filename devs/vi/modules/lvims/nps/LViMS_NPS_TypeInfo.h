#ifndef LQMEAS_LVIMS_NPS_TYPEINFO_H
#define LQMEAS_LVIMS_NPS_TYPEINFO_H

#include "../LViMSSysModuleType.h"

namespace LQMeas {
    class LViMS_NPS_TypeInfo : public LViMSSysModuleType {
    public:
        static const LViMS_NPS_TypeInfo &defaultTypeInfo();

        static const LViMS_NPS_TypeInfo &typeInfo_NPS();
        static const LViMS_NPS_TypeInfo &typeInfo_NPS_1();

        static const LViMS_NPS_TypeInfo &typeInfo_NPS_v2();
        static const LViMS_NPS_TypeInfo &typeInfo_NPS_1_v2();

        QList<const DeviceTypeInfo *> modificationList() const override;
        QList<const DeviceTypeInfo *> visibleModificationList() const override;
    private:
        LViMS_NPS_TypeInfo(const t_vi_module_type_descr *type_descr);
    };
}

#endif // LQMEAS_LVIMS_NPS_TYPEINFO_H
