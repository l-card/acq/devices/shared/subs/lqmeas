#include "LViMS_NPS_TypeInfo.h"
#include "lqmeas/devs/vi/protocol/vi_module_types.h"

namespace LQMeas {
    const LViMS_NPS_TypeInfo &LViMS_NPS_TypeInfo::defaultTypeInfo() {
        return typeInfo_NPS();
    }

    const LViMS_NPS_TypeInfo &LViMS_NPS_TypeInfo::typeInfo_NPS() {
        static const LViMS_NPS_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_NPS, 0, VI_MODULE_BOARD_TYPE_DEFAULT, 4)};
        return info;
    }

    const LViMS_NPS_TypeInfo &LViMS_NPS_TypeInfo::typeInfo_NPS_1() {
        static const LViMS_NPS_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_NPS, 1, VI_MODULE_BOARD_TYPE_V1, 1)};
        return info;
    }

    const LViMS_NPS_TypeInfo &LViMS_NPS_TypeInfo::typeInfo_NPS_v2() {
        static const LViMS_NPS_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_NPS, 0, VI_MODULE_BOARD_TYPE_DEFAULT, 3)};
        return info;
    }

    const LViMS_NPS_TypeInfo &LViMS_NPS_TypeInfo::typeInfo_NPS_1_v2() {
        static const LViMS_NPS_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_NPS, 1, VI_MODULE_BOARD_TYPE_DEFAULT, 3)};
        return info;
    }


    QList<const DeviceTypeInfo *> LViMS_NPS_TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list {
            &typeInfo_NPS(),
            &typeInfo_NPS_1(),
            /* версия до введения общей платы ICP/NPS */
            &typeInfo_NPS_v2(),
            &typeInfo_NPS_1_v2(),
        };
        return list;
    }

    QList<const DeviceTypeInfo *> LViMS_NPS_TypeInfo::visibleModificationList() const {
        static const QList<const DeviceTypeInfo *> list {
            &typeInfo_NPS(),
            &typeInfo_NPS_1(),
        };
        return list;
    }

    LViMS_NPS_TypeInfo::LViMS_NPS_TypeInfo(const t_vi_module_type_descr *type_descr) :
        LViMSSysModuleType{QStringLiteral("NPS"), type_descr} {

    }
}
