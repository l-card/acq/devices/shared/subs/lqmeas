#ifndef LQMEAS_LVIMSNETAUTODISCOVERYRECORDINFO_H
#define LQMEAS_LVIMSNETAUTODISCOVERYRECORDINFO_H

#include "lqmeas/devs/vi/netdiscovery/ViNetAutodiscoveryRecordInfo.h"

namespace LQMeas {
    class LViMSNetAutodiscoveryRecordInfo : public ViNetAutodiscoveryRecordInfo {
    public:
        explicit LViMSNetAutodiscoveryRecordInfo() {}
        explicit LViMSNetAutodiscoveryRecordInfo(const ViNetAutodiscoveryRecordInfo &other) :
            ViNetAutodiscoveryRecordInfo{other} {}

        const NetAutodiscoveryServiceType &serviceType() const override;
        NetAutodiscoveryRecordInfo *clone() const override {
            return new LViMSNetAutodiscoveryRecordInfo{*this};
        }
    };
}

#endif // LQMEAS_LVIMSNETAUTODISCOVERYRECORDINFO_H
