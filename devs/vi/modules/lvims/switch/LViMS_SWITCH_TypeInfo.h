#ifndef LQMEAS_LVIMS_SWITCH_TYPEINFO_H
#define LQMEAS_LVIMS_SWITCH_TYPEINFO_H

#include "../LViMSSysModuleType.h"

namespace LQMeas {
    class LViMS_SWITCH_TypeInfo : public LViMSSysModuleType {
    public:
        static const LViMS_SWITCH_TypeInfo &defaultTypeInfo();

        static const LViMS_SWITCH_TypeInfo &typeInfo_SWITCH();

        static const LViMS_SWITCH_TypeInfo &typeInfo_SWITCH_v2();

        QList<const DeviceTypeInfo *> modificationList() const override;
        QList<const DeviceTypeInfo *> visibleModificationList() const override;
    private:
        LViMS_SWITCH_TypeInfo(const t_vi_module_type_descr *type_descr);
    };
}

#endif // LQMEAS_LVIMS_SWITCH_TYPEINFO_H
