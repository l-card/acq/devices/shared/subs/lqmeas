#include "LViMSSysModuleType.h"
#include "lqmeas/devs/vi/protocol/vi_system_types.h"
#include "lqmeas/devs/DeviceTypeStdSeries.h"

namespace LQMeas {

    LViMSSysModuleType::LViMSSysModuleType(const QString &moduleGroupType, const t_vi_module_type_descr *type_descr) :
        ViModuleTypeInfo{moduleGroupType, type_descr, vi_system_type_get_descr(VI_SYSTEM_TYPE_LVIMS)} {

    }

    const DeviceTypeSeries &LViMSSysModuleType::deviceTypeSeries() const {
        return DeviceTypeStdSeries::lvims();
    }
}
