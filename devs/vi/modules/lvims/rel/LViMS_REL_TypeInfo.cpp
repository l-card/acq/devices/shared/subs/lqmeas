#include "LViMS_REL_TypeInfo.h"
#include "lqmeas/devs/vi/protocol/vi_module_types.h"
#include "lqmeas/devs/vi/modules/ViDiagnosticAdcChInfo.h"

namespace LQMeas {
    const LViMS_REL_TypeInfo &LViMS_REL_TypeInfo::defaultTypeInfo() {
        return typeInfo_REL();
    }

    const LViMS_REL_TypeInfo &LViMS_REL_TypeInfo::typeInfo_REL() {
        static const LViMS_REL_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_REL, 0, VI_MODULE_BOARD_TYPE_DEFAULT, 4)};
        return info;
    }

    const LViMS_REL_TypeInfo &LViMS_REL_TypeInfo::typeInfo_REL_v2() {
        static const LViMS_REL_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_REL, 0, VI_MODULE_BOARD_TYPE_DEFAULT, 3)};
        return info;
    }

    QList<const DeviceTypeInfo *> LViMS_REL_TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list {
            &typeInfo_REL(),
            &typeInfo_REL_v2(),
        };
        return list;
    }

    QList<const DeviceTypeInfo *> LViMS_REL_TypeInfo::visibleModificationList() const {
        static const QList<const DeviceTypeInfo *> list {
            &typeInfo_REL(),
        };
        return list;
    }

    QList<const ViDiagnosticAdcChInfo *> LViMS_REL_TypeInfo::diagnAdcChs() const {
        return LViMSSysModuleType::diagnAdcChs() + QList<const ViDiagnosticAdcChInfo *> {
                   &ViDiagnosticAdcStdChsInfo::hpRel(0),
                   &ViDiagnosticAdcStdChsInfo::hpRel(1),
                   &ViDiagnosticAdcStdChsInfo::hpRel(2),
                   &ViDiagnosticAdcStdChsInfo::hpRel(3)
               };
    }

    LViMS_REL_TypeInfo::LViMS_REL_TypeInfo(const t_vi_module_type_descr *type_descr) :
        LViMSSysModuleType{QStringLiteral("REL"), type_descr} {

    }
}
