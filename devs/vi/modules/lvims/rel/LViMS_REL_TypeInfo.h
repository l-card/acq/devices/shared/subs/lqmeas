#ifndef LQMEAS_LVIMS_REL_TYPEINFO_H
#define LQMEAS_LVIMS_REL_TYPEINFO_H

#include "../LViMSSysModuleType.h"

namespace LQMeas {
    class LViMS_REL_TypeInfo : public LViMSSysModuleType {
    public:
        static const LViMS_REL_TypeInfo &defaultTypeInfo();

        static const LViMS_REL_TypeInfo &typeInfo_REL();

        static const LViMS_REL_TypeInfo &typeInfo_REL_v2();

        QList<const DeviceTypeInfo *> modificationList() const override;
        QList<const DeviceTypeInfo *> visibleModificationList() const override;

        QList<const ViDiagnosticAdcChInfo *> diagnAdcChs() const override;
    private:
        LViMS_REL_TypeInfo(const t_vi_module_type_descr *type_descr);
    };
}

#endif // LQMEAS_LVIMS_REL_TYPEINFO_H
