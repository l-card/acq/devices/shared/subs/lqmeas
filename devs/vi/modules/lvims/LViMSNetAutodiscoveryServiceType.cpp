#include "LViMSNetAutodiscoveryServiceType.h"
#include "LViMSNetAutodiscoveryRecordInfo.h"
#include "lqmeas/devs/vi/protocol/vi_system_types.h"

namespace LQMeas {
    const ViNetAutodiscoveryServiceType &LViMSNetAutodiscoveryServiceType::type() {
        static const LViMSNetAutodiscoveryServiceType t;
        return t;
    }

    const QString &LViMSNetAutodiscoveryServiceType::typeName() {
        static const QString n {QStringLiteral(VI_SYSTEM_TYPE_LVIMS_SVCNAME)};
        return n;
    }

    NetAutodiscoveryRecordInfo *LViMSNetAutodiscoveryServiceType::createRecord() const {
        return new LViMSNetAutodiscoveryRecordInfo{};
    }

    LViMSNetAutodiscoveryServiceType::LViMSNetAutodiscoveryServiceType() {

    }
}
