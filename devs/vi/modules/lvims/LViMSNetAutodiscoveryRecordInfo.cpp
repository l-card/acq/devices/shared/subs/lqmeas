#include "LViMSNetAutodiscoveryRecordInfo.h"
#include "LViMSNetAutodiscoveryServiceType.h"

namespace LQMeas {
    const NetAutodiscoveryServiceType &LViMSNetAutodiscoveryRecordInfo::serviceType() const {
        return LViMSNetAutodiscoveryServiceType::type();
    }
}
