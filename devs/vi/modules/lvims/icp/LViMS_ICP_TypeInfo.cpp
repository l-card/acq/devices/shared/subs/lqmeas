#include "LViMS_ICP_TypeInfo.h"
#include "lqmeas/devs/vi/protocol/vi_module_types.h"

namespace LQMeas {
    const LViMS_ICP_TypeInfo &LViMS_ICP_TypeInfo::defaultTypeInfo() {
        return typeInfo_ICP10_1();
    }


    const LViMS_ICP_TypeInfo &LViMS_ICP_TypeInfo::typeInfo_ICP10() {
        static const LViMS_ICP_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_ICP_10, 0, VI_MODULE_BOARD_TYPE_DEFAULT, 4)};
        return info;
    }

    const LViMS_ICP_TypeInfo &LViMS_ICP_TypeInfo::typeInfo_ICP10_1() {
        static const LViMS_ICP_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_ICP_10, 1, VI_MODULE_BOARD_TYPE_V1, 1)};
        return info;
    }

    const LViMS_ICP_TypeInfo &LViMS_ICP_TypeInfo::typeInfo_ICP4() {
        static const LViMS_ICP_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_ICP_4, 0, VI_MODULE_BOARD_TYPE_V1, 1)};
        return info;
    }

    const LViMS_ICP_TypeInfo &LViMS_ICP_TypeInfo::typeInfo_ICP4_1() {
        static const LViMS_ICP_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_ICP_4, 1, VI_MODULE_BOARD_TYPE_V1, 1)};
        return info;
    }

    const LViMS_ICP_TypeInfo &LViMS_ICP_TypeInfo::typeInfo_ICP10_v2() {
        static const LViMS_ICP_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_ICP_10, 0, VI_MODULE_BOARD_TYPE_DEFAULT, 3)};
        return info;
    }

    const LViMS_ICP_TypeInfo &LViMS_ICP_TypeInfo::typeInfo_ICP10_1_v2() {
        static const LViMS_ICP_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_ICP_10, 1, VI_MODULE_BOARD_TYPE_DEFAULT, 3)};
        return info;
    }

    const LViMS_ICP_TypeInfo &LViMS_ICP_TypeInfo::typeInfo_ICP4_v2() {
        static const LViMS_ICP_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_ICP_4, 0, VI_MODULE_BOARD_TYPE_DEFAULT, 3)};
        return info;
    }

    const LViMS_ICP_TypeInfo &LViMS_ICP_TypeInfo::typeInfo_ICP4_1_v2() {
        static const LViMS_ICP_TypeInfo info{vi_module_type_get_descr(
            VI_MODULE_TYPE_ICP_4, 1, VI_MODULE_BOARD_TYPE_DEFAULT, 3)};
        return info;
    }


    QList<const DeviceTypeInfo *> LViMS_ICP_TypeInfo::modificationList() const {
        static const QList<const DeviceTypeInfo *> list {
            &typeInfo_ICP10(),
            &typeInfo_ICP10_1(),
            &typeInfo_ICP4(),
            &typeInfo_ICP4_1(),
            /* версия до введения общей платы ICP/NPS */
            &typeInfo_ICP10_v2(),
            &typeInfo_ICP10_1_v2(),
            &typeInfo_ICP4_v2(),
            &typeInfo_ICP4_1_v2(),
        };
        return list;
    }

    QList<const DeviceTypeInfo *> LViMS_ICP_TypeInfo::visibleModificationList() const {
        static const QList<const DeviceTypeInfo *> list {
            &typeInfo_ICP10(),
            &typeInfo_ICP10_1(),
            &typeInfo_ICP4(),
            &typeInfo_ICP4_1(),

        };
        return list;
    }

    LViMS_ICP_TypeInfo::LViMS_ICP_TypeInfo(const t_vi_module_type_descr *type_descr) :
        LViMSSysModuleType{QStringLiteral("ICP"), type_descr} {

    }
}
