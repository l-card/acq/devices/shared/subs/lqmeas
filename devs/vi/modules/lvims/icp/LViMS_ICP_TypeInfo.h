#ifndef LQMEAS_LVIMS_ICP_TYPEINFO_H
#define LQMEAS_LVIMS_ICP_TYPEINFO_H

#include "../LViMSSysModuleType.h"

namespace LQMeas {
    class LViMS_ICP_TypeInfo : public LViMSSysModuleType {
    public:
        static const LViMS_ICP_TypeInfo &defaultTypeInfo();

        static const LViMS_ICP_TypeInfo &typeInfo_ICP10();
        static const LViMS_ICP_TypeInfo &typeInfo_ICP10_1();
        static const LViMS_ICP_TypeInfo &typeInfo_ICP4();
        static const LViMS_ICP_TypeInfo &typeInfo_ICP4_1();


        static const LViMS_ICP_TypeInfo &typeInfo_ICP10_v2();
        static const LViMS_ICP_TypeInfo &typeInfo_ICP10_1_v2();
        static const LViMS_ICP_TypeInfo &typeInfo_ICP4_v2();
        static const LViMS_ICP_TypeInfo &typeInfo_ICP4_1_v2();

        QList<const DeviceTypeInfo *> modificationList() const override;
        QList<const DeviceTypeInfo *> visibleModificationList() const override;
    private:
        LViMS_ICP_TypeInfo(const t_vi_module_type_descr *type_descr);
    };
}

#endif // LQMEAS_LVIMS_ICP_TYPEINFO_H
