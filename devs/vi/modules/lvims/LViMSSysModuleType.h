#ifndef LQMEAS_LVIMSSYSMODULETYPE_H
#define LQMEAS_LVIMSSYSMODULETYPE_H

#include "lqmeas/devs/vi/modules/ViModuleTypeInfo.h"

namespace LQMeas {
    class LViMSSysModuleType : public ViModuleTypeInfo {
    protected:
        LViMSSysModuleType(const QString &moduleGroupType, const t_vi_module_type_descr *type_descr);

        const DeviceTypeSeries &deviceTypeSeries() const override;
    };
}

#endif // LQMEAS_LVIMSSYSMODULETYPE_H
