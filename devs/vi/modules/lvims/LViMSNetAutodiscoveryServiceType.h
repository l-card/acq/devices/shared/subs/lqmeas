#ifndef LQMEAS_LVIMSNETAUTODISCOVERYSERVICETYPE_H
#define LQMEAS_LVIMSNETAUTODISCOVERYSERVICETYPE_H

#include "lqmeas/devs/vi/netdiscovery/ViNetAutodiscoveryServiceType.h"

namespace LQMeas {
    class LViMSNetAutodiscoveryServiceType : public ViNetAutodiscoveryServiceType {
    public:
        static const ViNetAutodiscoveryServiceType &type();
        static const QString &typeName();

        const QString name() const override { return typeName(); }
        NetAutodiscoveryRecordInfo *createRecord() const override;
    protected:
        LViMSNetAutodiscoveryServiceType();
    };
}

#endif // LQMEAS_LVIMSNETAUTODISCOVERYSERVICETYPE_H
