#include "ViDoutResetReqData.h"
#include "lqmeas/devs/vi/config/dout/ViDoutChannelType.h"

namespace LQMeas {

    ViDoutResetReqData::ViDoutResetReqData(quint64 lp_chmsk, quint64 hp_chmsk) :
        m_ch_lp_mask{lp_chmsk}, m_ch_hp_mask{hp_chmsk} {

    }

    quint64 ViDoutResetReqData::channelMask(const ViDoutChannelType &type) const {
        return type == ViDoutChannelTypes::relayLP() ? m_ch_lp_mask :
                   type == ViDoutChannelTypes::relayHP() ? m_ch_hp_mask : 0;
    }

    void ViDoutResetReqData::setChannelMask(const ViDoutChannelType &type, quint64 mask) {
        if (type == ViDoutChannelTypes::relayLP()) {
            m_ch_lp_mask = mask;
        } else if (type == ViDoutChannelTypes::relayHP()) {
            m_ch_hp_mask = mask;
        }
    }

}
