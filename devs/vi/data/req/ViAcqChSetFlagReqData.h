#ifndef LQMEAS_VIACQCHSETFLAGREQDATA_H
#define LQMEAS_VIACQCHSETFLAGREQDATA_H

#include <QtGlobal>

namespace LQMeas {
    class ViAcqChSetFlagReqData {
    public:
        enum class FlagType {
            None = 0,
            Bypass = 1,
            AlarmInhibit = 2,
            TripMultiply = 3,
        };

        ViAcqChSetFlagReqData() {}
        ViAcqChSetFlagReqData(FlagType f, bool st, quint32 chmsk) :
            m_flag{f}, m_state{st}, m_ch_mask{chmsk} {}


        FlagType flag() const {return m_flag;}
        bool state() const {return m_state;}
        quint32 channelMask() const {return m_ch_mask;}

        void setFlag(FlagType f) {m_flag = f;}
        void setState(bool st) {m_state = st;}
        void setChannelMask(quint32 mask) {m_ch_mask = mask;}
    private:
        FlagType    m_flag {FlagType::None};
        quint32     m_ch_mask {0};
        bool        m_state {false};
    };
}

#endif // LQMEAS_VIACQCHSETFLAGREQDATA_H
