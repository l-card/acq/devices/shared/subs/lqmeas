#ifndef LQMEAS_VIDOUTRESETREQDATA_H
#define LQMEAS_VIDOUTRESETREQDATA_H


#include <QtGlobal>

namespace LQMeas {
    class ViDoutChannelType;

    class ViDoutResetReqData {
    public:
        ViDoutResetReqData() {}
        ViDoutResetReqData(quint64 lp_chmsk, quint64 hp_chmsk);

        quint64 channelMask(const ViDoutChannelType &type) const;
        void setChannelMask(const ViDoutChannelType &type, quint64 mask);

        quint64 lpChannelMask() const {return m_ch_lp_mask;}
        quint64 hpChannelMask() const {return m_ch_hp_mask;}

        void setLpChannelMask(quint64 mask) {m_ch_lp_mask = mask;}
        void setHpChannelMask(quint64 mask) {m_ch_hp_mask = mask;}

    private:
        quint64 m_ch_lp_mask {0};
        quint64 m_ch_hp_mask {0};
    };
}

#endif // LQMEAS_VIDOUTRESETREQDATA_H
