#ifndef LQMEAS_VIDOUTMANUALCHANGEREQDATA_H
#define LQMEAS_VIDOUTMANUALCHANGEREQDATA_H

#include <QtGlobal>

namespace LQMeas {
    class ViDoutChannelType;

    class ViDoutManualChangeReqData {
    public:
        ViDoutManualChangeReqData() {}
        ViDoutManualChangeReqData(quint64 lp_chmsk, quint64 lp_chvals,
                    quint64 hp_chmsk, quint64 hp_chvals);

        quint64 channelMask(const ViDoutChannelType &type) const;
        quint64 channelValues(const ViDoutChannelType &type) const;
        void setChannelMask(const ViDoutChannelType &type, quint64 mask);
        void setChannelValues(const ViDoutChannelType &type, quint64 vals);

        quint64 lpChannelMask() const {return m_ch_lp_mask;}
        quint64 lpChannelValues() const {return m_ch_lp_values;}
        quint64 hpChannelMask() const {return m_ch_hp_mask;}
        quint64 hpChannelValues() const {return m_ch_hp_values;}

        void setLpChannelMask(quint64 mask) {m_ch_lp_mask = mask;}
        void setLpChannelValues(quint64 vals) {m_ch_lp_values = vals;}
        void setHpChannelMask(quint64 mask) {m_ch_hp_mask = mask;}
        void setHpChannelValues(quint64 vals) {m_ch_hp_values = vals;}
    private:
        quint64 m_ch_lp_mask {0};
        quint64 m_ch_lp_values {0};
        quint64 m_ch_hp_mask {0};
        quint64 m_ch_hp_values {0};
    };
}

#endif // LQMEAS_VIDOUTMANUALCHANGEREQDATA_H
