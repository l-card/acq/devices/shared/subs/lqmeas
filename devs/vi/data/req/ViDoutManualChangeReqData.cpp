#include "ViDoutManualChangeReqData.h"
#include "lqmeas/devs/vi/config/dout/ViDoutChannelType.h"

namespace LQMeas {

    ViDoutManualChangeReqData::ViDoutManualChangeReqData(quint64 lp_chmsk, quint64 lp_chvals, quint64 hp_chmsk, quint64 hp_chvals) :
        m_ch_lp_mask{lp_chmsk}, m_ch_lp_values{lp_chvals},
        m_ch_hp_mask{hp_chmsk}, m_ch_hp_values{hp_chvals}  {

    }

    quint64 ViDoutManualChangeReqData::channelMask(const ViDoutChannelType &type) const {
        return type == ViDoutChannelTypes::relayLP() ? m_ch_lp_mask :
                   type == ViDoutChannelTypes::relayHP() ? m_ch_hp_mask : 0;
    }

    quint64 ViDoutManualChangeReqData::channelValues(const ViDoutChannelType &type) const {
        return type == ViDoutChannelTypes::relayLP() ? m_ch_lp_values :
                   type == ViDoutChannelTypes::relayHP() ? m_ch_hp_values : 0;
    }

    void ViDoutManualChangeReqData::setChannelMask(const ViDoutChannelType &type, quint64 mask) {
        if (type == ViDoutChannelTypes::relayLP()) {
            m_ch_lp_mask = mask;
        } else if (type == ViDoutChannelTypes::relayHP()) {
            m_ch_hp_mask = mask;
        }
    }

    void ViDoutManualChangeReqData::setChannelValues(const ViDoutChannelType &type, quint64 vals) {
        if (type == ViDoutChannelTypes::relayLP()) {
            m_ch_lp_values = vals;
        } else if (type == ViDoutChannelTypes::relayHP()) {
            m_ch_hp_values = vals;
        }
    }

}
