#ifndef LQMEAS_VIWFREQDATA_H
#define LQMEAS_VIWFREQDATA_H

#include <QtGlobal>
#include "../state/ViTimeStamp.h"

namespace LQMeas {
    class ViWfReqData {
    public:
        ViWfReqData() {}
        ViWfReqData(quint32 chmsk, quint32 samples_cnt) :
            m_ch_mask{chmsk}, m_samples_cnt{samples_cnt} {}



        quint32 channelMask() const {return m_ch_mask;}
        quint32 samplesCnt() const {return m_samples_cnt;}
        quint32 delaySamplesCnt() const {return m_delay_samples_cnt;}
        const ViTimeStamp &timestamp() const {return m_tstmp;}

        void setChannelMask(quint32 mask) {m_ch_mask = mask;}
        void setSamplesCnt(quint32 cnt) {m_samples_cnt = cnt;}
        void setDelayCnt(quint32 cnt) {m_delay_samples_cnt = cnt;}
        void setTimestamp(const ViTimeStamp &tstmp) { m_tstmp = tstmp;}

    private:
        quint32     m_ch_mask {0};
        quint32     m_samples_cnt {0};
        quint32     m_delay_samples_cnt {0};
        ViTimeStamp m_tstmp;
    };
}

#endif // LQMEAS_VIWFREQDATA_H
