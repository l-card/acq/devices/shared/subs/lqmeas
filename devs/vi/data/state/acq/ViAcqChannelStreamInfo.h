#ifndef VIACQCHANNELSTREAMINFO_H
#define VIACQCHANNELSTREAMINFO_H

#include "ViAcqChannelConvInfo.h"


namespace LQMeas {
    class ViAcqChannelStreamInfo {
    public:
        quint16 chNum() const {return m_ch_num;}
        double  nominalFreq() const {return m_nom_freq;}
        double  gmFreq() const {return m_gm_freq;}
        const ViAcqChannelConvInfo &conv() const {return m_conv;}

        quint32 blockTimeMs() const {return m_block_time_ms;}
        quint32 blockSamplesCnt() const {return m_block_samples_cnt;}

        void setChNum(quint16 num) {m_ch_num = num;}
        void setNominalFreq(double f) {m_nom_freq = f;}
        void setGmFreq(double f) {m_gm_freq = f;}
        void setConv(const ViAcqChannelConvInfo &conv) { m_conv = conv; }
        void setBlockTimeMs(quint32 ms) {m_block_time_ms = ms;}
        void setBlockSamplesCnt(quint32 cnt) {m_block_samples_cnt = cnt;}

    private:
        quint16 m_ch_num {0};
        double  m_nom_freq {0};
        double  m_gm_freq {0};
        quint32 m_block_time_ms {0};
        quint32 m_block_samples_cnt {0};
        ViAcqChannelConvInfo m_conv;
    };
}

#endif // VIACQCHANNELSTREAMINFO_H
