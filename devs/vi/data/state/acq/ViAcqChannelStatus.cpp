#include "ViAcqChannelStatus.h"
#include "lqmeas/devs/vi/protocol/vi_module_state.h"

namespace LQMeas {
    const ViAcqChannelStatus &ViAcqChannelStatusCodes::unknown() {
        static const class ViAcqChannelStatusNotReady : public ViAcqChannelStatus {
            int id() const override {return -1;}
            QString displayName() const override {return tr("Unknown");}
            bool isFailure() const override {return false;}
            bool isDataValid() const override {return false;}
        } s;
        return s;
    }

    const ViAcqChannelStatus &ViAcqChannelStatusCodes::notReady() {
        static const class ViAcqChannelStatusNotReady : public ViAcqChannelStatus {
            int id() const override {return VI_ACQ_CH_STATUS_NOT_RDY;}
            QString displayName() const override {return tr("Not ready");}
            bool isFailure() const override {return false;}
            bool isDataValid() const override {return false;}
        } s;
        return s;
    }

    const ViAcqChannelStatus &ViAcqChannelStatusCodes::ok() {
        static const class ViAcqChannelStatusOk : public ViAcqChannelStatus {
            int id() const override {return VI_ACQ_CH_STATUS_VALID;}
            QString displayName() const override {return tr("Valid data");}
            bool isFailure() const override {return false;}
            bool isDataValid() const override {return true;}
        } s;
        return s;
    }

    const ViAcqChannelStatus &ViAcqChannelStatusCodes::shortCircuit() {
        static const class ViAcqChannelStatusShort : public ViAcqChannelStatus {
            int id() const override {return VI_ACQ_CH_STATUS_SHORT;}
            QString displayName() const override {return tr("Short circuit");}
            bool isFailure() const override {return true;}
            bool isDataValid() const override {return false;}
        } s;
        return s;
    }

    const ViAcqChannelStatus &ViAcqChannelStatusCodes::openCircuit() {
        static const class ViAcqChannelStatusOpen : public ViAcqChannelStatus {
            int id() const override {return VI_ACQ_CH_STATUS_OPEN;}
            QString displayName() const override {return tr("Open circuit");}
            bool isFailure() const override {return true;}
            bool isDataValid() const override {return false;}
        } s;
        return s;
    }

    const ViAcqChannelStatus &ViAcqChannelStatusCodes::outOfSensorRange() {
        static const class ViAcqChannelStatusOpen : public ViAcqChannelStatus {
            int id() const override {return VI_ACQ_CH_STATUS_OUT_OF_SENS_RANGE;}
            QString displayName() const override {return tr("Out of sensor valid range");}
            bool isFailure() const override {return true;}
            bool isDataValid() const override {return false;}
        } s;
        return s;
    }

    const QList<const ViAcqChannelStatus *> &ViAcqChannelStatusCodes::all() {
        static QList<const ViAcqChannelStatus *> list {
            &notReady(),
            &ok(),
            &shortCircuit(),
            &openCircuit(),
            &outOfSensorRange()
        };
        return list;
    }
}

