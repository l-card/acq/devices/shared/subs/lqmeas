#ifndef LQMEAS_VIMEASSTATE_H
#define LQMEAS_VIMEASSTATE_H

#include <QtGlobal>
#include "lqmeas/devs/vi/protocol/tribool.h"

namespace LQMeas {
    class ViMeasType;

    class ViMeasState {
    public:
        int measurementNumber() const {return m_meas_num;}
        const ViMeasType &measType() const {return *m_meas_type;}
        quint16 typeCode() const {return m_type_code;}
        double value() const {return m_value;}
        double percentValue() const {return m_percent_value;}
        double percentBaseline() const {return m_percent_baseline;}

        t_tribool failure() const {return m_failure;}
        t_tribool alert()  const {return m_alert;}
        t_tribool danger() const {return m_danger;}
        bool  baselineValid() const {return m_baseline_valid;}
        bool baselineExceeded() const {return m_baseline_exceeded;}
    private:
        ViMeasState();

        static double scaledValue(quint16 value);

        void fillMeasType(quint16 type_code);

        int m_meas_num {-1};
        const ViMeasType *m_meas_type;
        quint16 m_type_code {0};
        double m_value {0};
        double m_percent_value {0};
        double m_percent_baseline {0};

        t_tribool m_failure {tribool_z};
        t_tribool m_alert {tribool_z};
        t_tribool m_danger {tribool_z};
        bool m_baseline_valid {false};
        bool m_baseline_exceeded {false};

        friend class ViModuleMbConnection;
    };
}

#endif // LQMEAS_VIMEASSTATE_H
