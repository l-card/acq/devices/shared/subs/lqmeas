#include "ViMeasState.h"
#include "lqmeas/devs/vi/config/acq/meas/types/ViMeasTypeUnknown.h"
#include "lqmeas/devs/vi/config/ViModuleConfigSerializer.h"

namespace LQMeas {
    ViMeasState::ViMeasState() :
        m_meas_type{&ViMeasTypeUnknown::type()} {

    }

    double ViMeasState::scaledValue(quint16 value) {
        return static_cast<double>(value)*100/0xFFFF;
    }

    void ViMeasState::fillMeasType(quint16 type_code) {
        m_type_code = type_code;
        m_meas_type = &ViModuleMbConfigSerializer::getMeasType(type_code);
    }
}
