#include "ViAcqChannelState.h"
#include "ViAcqChannelStatus.h"

namespace LQMeas {
    ViAcqChannelState::ViAcqChannelState() :
        m_ch_status{&ViAcqChannelStatusCodes::notReady()},
        m_ch_status_code{static_cast<quint16>(ViAcqChannelStatusCodes::notReady().id())}{

    }

    void ViAcqChannelState::fillStatus(quint16 status_code) {
        m_ch_status_code = status_code;
        const QList<const ViAcqChannelStatus *> &allStatusList {ViAcqChannelStatusCodes::all()};
        const auto &it {std::find_if(allStatusList.cbegin(), allStatusList.cend(),
                                    [status_code](const ViAcqChannelStatus *status){return status->id() == status_code;})};
        m_ch_status = it != allStatusList.cend() ? *it : &ViAcqChannelStatusCodes::unknown();
    }
}
