#ifndef LQMEAS_VIACQCHANNELSTATUS_H
#define LQMEAS_VIACQCHANNELSTATUS_H

#include "lqmeas/EnumNamedValue.h"

#include "lqmeas/EnumNamedValue.h"
#include <QObject>

namespace LQMeas {
    class ViAcqChannelStatus : public EnumNamedValue<int> {
    public:
        virtual bool isFailure() const = 0;
        virtual bool isDataValid() const = 0;
    };

    class ViAcqChannelStatusCodes : public QObject {
        Q_OBJECT
    public:
        static const ViAcqChannelStatus &unknown();

        static const ViAcqChannelStatus &notReady();
        static const ViAcqChannelStatus &ok();
        static const ViAcqChannelStatus &shortCircuit();
        static const ViAcqChannelStatus &openCircuit();
        static const ViAcqChannelStatus &outOfSensorRange();

        static const QList<const ViAcqChannelStatus *> &all();
    };
}

Q_DECLARE_METATYPE(const LQMeas::ViAcqChannelStatus *);


#endif // LQMEAS_VIACQCHANNELSTATUS_H
