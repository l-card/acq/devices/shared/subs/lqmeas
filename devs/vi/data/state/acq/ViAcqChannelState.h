#ifndef LQMEAS_VIACQCHANNELSTATE_H
#define LQMEAS_VIACQCHANNELSTATE_H


#include <QList>
#include "../ViManEventFlagState.h"

namespace LQMeas {
    class ViMeasState;
    class ViAcqChannelStatus;



    class ViAcqChannelState {
    public:
        ViAcqChannelState();

        int channelNumber() const {return m_ch_num;}
        const ViAcqChannelStatus &channelStatus() const {return *m_ch_status;}
        quint16 channelStatusCode() const {return m_ch_status_code;}
        int measurementCnt() const {return m_meas_list.size();}
        const ViMeasState &measurement(int i) const {return *m_meas_list.at(i);}

        bool bypassed() const {return m_bypassed;}
        const ViManEventFlagState &tripMultiply() const {return m_trip_multiply;}
        const ViManEventFlagState &alarmInhibit() const {return m_alarm_inhibit;}
    private:


        void fillStatus(quint16 status_code);

        int m_ch_num {-1};
        const ViAcqChannelStatus *m_ch_status;
        quint16 m_ch_status_code;
        bool m_bypassed {false};
        ViManEventFlagState m_trip_multiply;
        ViManEventFlagState m_alarm_inhibit;
        QList<ViMeasState *> m_meas_list;

        friend class ViModuleMbConnection;
    };
}

#endif // LQMEAS_VIACQCHANNELSTATE_H
