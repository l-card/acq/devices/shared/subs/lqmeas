#ifndef VIACQCHANNELCONVINFO_H
#define VIACQCHANNELCONVINFO_H


#include <QtGlobal>

namespace LQMeas {
    struct ViAcqConvCoefs {
        ViAcqConvCoefs(double _k = 1., double _b = 0.) : k{_k}, b{_b} {}

        double k {1.};
        double b {0.};
    };

    class ViAcqChannelConvInfo {
    public:
        const ViAcqConvCoefs &elec() const {return m_conv_elec;}
        const ViAcqConvCoefs &phys() const {return m_conv_phys;}

        double toElec(int32_t sample) const {return m_conv_elec.k * sample + m_conv_elec.b;}
        double elecToPhys(double e) const {return m_conv_phys.k * e + m_conv_phys.b;}
        double toPhys(int32_t sample) const {return elecToPhys(toElec(sample));}

        void setElec(const ViAcqConvCoefs &coefs)  {
            m_conv_elec = coefs;
        }
        void setPhys(const ViAcqConvCoefs &coefs)  {
            m_conv_phys = coefs;
        }
    private:
        ViAcqConvCoefs m_conv_elec;
        ViAcqConvCoefs m_conv_phys;
    };
}

#endif // VIACQCHANNELCONVINFO_H
