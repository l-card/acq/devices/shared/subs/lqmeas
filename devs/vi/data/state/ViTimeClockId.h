#ifndef LQMEAS_VITIMECLOCKID_H
#define LQMEAS_VITIMECLOCKID_H

#include <QString>
#include <QByteArray>

namespace LQMeas {
    class ViTimeClockId {
    public:
        ViTimeClockId() {}
        ViTimeClockId(const ViTimeClockId &other) : m_data{other.m_data} {}

        ViTimeClockId &operator=(const ViTimeClockId &other) {
            if (other != *this) {
                m_data = other.m_data;
            }
            return *this;
        }

        static const int size {8};

        const QByteArray &data() const {return m_data;}

        bool operator==(const ViTimeClockId &other) const {
            return  m_data == other.m_data;
        }
        bool operator!=(const ViTimeClockId &other) const {
            return  m_data != other.m_data;
        }

        QString text() const {
            QString ret;
            for (char byte : m_data) {
                ret += QString("%1").arg(static_cast<quint8>(byte), 2, 16, QChar('0')).toUpper();
            }
            return ret;
        }

        bool isValid() const {
            return m_data.size() == size;
        }

        void clear() {
            m_data.clear();
        }

        void setData(const QByteArray &data) {
            m_data = data;
        }

        void setData(const quint8 *data) {
            m_data = QByteArray{reinterpret_cast<const char *>(data), size};
        }
    private:


        QByteArray m_data;
    };
}

#endif // LQMEAS_VITIMECLOCKID_H
