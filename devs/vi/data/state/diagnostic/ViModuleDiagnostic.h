#ifndef LQMEAS_VIMODULEDIAGNOSTIC_H
#define LQMEAS_VIMODULEDIAGNOSTIC_H

#include <QList>
#include "ViDiagnosticAdcState.h"
#include "ViModuleFaults.h"

namespace LQMeas {
    class ViModuleDiagnostic {
    public:
        const QList<const ViModuleFault*> &devfaultList() const {return m_devfault_list;}
        const QList<ViDiagnosticAdcState> &adcStateList() const {return m_adc_state_list;}

    private:
        ViModuleDiagnostic() {}

        QList<const ViModuleFault *> m_devfault_list;
        QList<ViDiagnosticAdcState> m_adc_state_list;

        friend class ViModuleMbConnection;
    };
}

#endif // LQMEAS_VIMODULEDIAGNOSTIC_H
