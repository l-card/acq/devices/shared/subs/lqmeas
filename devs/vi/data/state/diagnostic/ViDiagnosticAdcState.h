#ifndef LQMEAS_VIDIAGNOSTICADCSTATE_H
#define LQMEAS_VIDIAGNOSTICADCSTATE_H

namespace LQMeas {
    class ViDiagnosticAdcState {
    public:
        ViDiagnosticAdcState() {}

        unsigned addr() const {return m_addr;}
        bool isPresent() const {return m_present;}
        bool isValid() const {return m_valid;}
        bool hasRangeErr() const {return m_range_err;}
        float value() const {return m_value;}
    private:
        unsigned m_addr {0};
        bool m_present {false};
        bool m_valid {false};
        bool m_range_err {false};
        float m_value {0};

        friend class ViModuleMbConnection;
    };
}

#endif // LQMEAS_VIDIAGNOSTICADCSTATE_H
