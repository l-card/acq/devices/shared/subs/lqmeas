#ifndef LQMEAS_VIMODULEFAULTS_H
#define LQMEAS_VIMODULEFAULTS_H

#include "lqmeas/EnumNamedValue.h"
#include <QObject>

namespace LQMeas {
    typedef EnumNamedValue<quint8> ViModuleFault;

    class ViModuleFaults : public QObject {
        Q_OBJECT
    public:
        static const ViModuleFault &unknownFault();
        static const ViModuleFault &unknownWarn();

        static const ViModuleFault &clkinit();
        static const ViModuleFault &sdram();
        static const ViModuleFault &ethPhyRegs();
        static const ViModuleFault &ethPhyLb();
        static const ViModuleFault &noMezzFlash();
        static const ViModuleFault &noBoardFlash();
        static const ViModuleFault &noSwitch();
        static const ViModuleFault &switchData();
        static const ViModuleFault &gphyRegs();
        static const ViModuleFault &gphyLb();
        static const ViModuleFault &relayCurrent();
        static const ViModuleFault &overtemp();
        static const ViModuleFault &diagnAdcLdoRdy();
        static const ViModuleFault &diagnAdcCalRdy();

        static const ViModuleFault &usupRange();
        static const ViModuleFault &acqChFault();
        static const ViModuleFault &noBoardInfo();
        static const ViModuleFault &unknowDevtype();
        static const ViModuleFault &noMezzInfo();

        static QList<const ViModuleFault *> all();
    };
}

#endif // LQMEAS_VIMODULEFAULTS_H
