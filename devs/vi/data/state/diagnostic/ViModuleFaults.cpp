#include "ViModuleFaults.h"
#include "lqmeas/devs/vi/protocol/vi_module_dev_faults.h"




namespace LQMeas {
    const ViModuleFault &ViModuleFaults::unknownFault() {
        static const class ViModuleFaultUnkErr : public ViModuleFault {
            virtual quint8 id() const override {return VI_DEVFAULT_ERR_CODE(0);}
            virtual QString displayName() const override {return tr("Unknown device fault");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::unknownWarn()  {
        static const class ViModuleFaultUnkWarn : public ViModuleFault {
            virtual quint8 id() const override {return static_cast<quint8>(VI_DEVFAULT_WARN_CODE(0));}
            virtual QString displayName() const override {return tr("Unknown device warning");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::clkinit() {
        static const class ViModuleFaultClkInit : public ViModuleFault {
            virtual quint8 id() const override {return VI_DEVFAULT_CLK_INIT;}
            virtual QString displayName() const override {return tr("Clock system fault");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::sdram() {
        static const class ViModuleFaultSDRAM : public ViModuleFault {
            virtual quint8 id() const override {return VI_DEVFAULT_SDRAM;}
            virtual QString displayName() const override {return tr("SDRAM fault");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::ethPhyRegs() {
        static const class ViModuleFaultEthPhyRegs : public ViModuleFault {
            virtual quint8 id() const override {return VI_DEVFAULT_ETH_PHYREG;}
            virtual QString displayName() const override {return tr("Eth PHY access");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::ethPhyLb() {
        static const class ViModuleFaultEthPhyLb : public ViModuleFault {
            virtual quint8 id() const override {return VI_DEVFAULT_ETH_LOOPBACK;}
            virtual QString displayName() const override {return tr("Eth PHY loopback");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::noMezzFlash() {
        static const class ViModuleFaultNoMezzFlash : public ViModuleFault {
            virtual quint8 id() const override {return VI_DEVFAULT_NO_SPI_FLASH;}
            virtual QString displayName() const override {return tr("Mezzanine flash not detected");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::noBoardFlash() {
        static const class ViModuleFaultNoBoardFlash : public ViModuleFault {
            virtual quint8 id() const override {return VI_DEVFAULT_NO_OW_FLASH;}
            virtual QString displayName() const override {return tr("Carrier board flash not detected");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::noSwitch() {
        static const class ViModuleFaultNoSwitch : public ViModuleFault {
            virtual quint8 id() const override {return VI_DEVFAULT_NO_SWITCH;}
            virtual QString displayName() const override {return tr("Ethrentet switch IC not detected");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::switchData() {
        static const class ViModuleFaultSwitchData : public ViModuleFault {
            virtual quint8 id() const override {return VI_DEVFAULT_SWITCH_DATA;}
            virtual QString displayName() const override {return tr("Ethrentet switch IC interface");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::gphyRegs() {
        static const class ViModuleFaultGphyAcc : public ViModuleFault {
            virtual quint8 id() const override {return VI_DEVFAULT_GPHY_REGS;}
            virtual QString displayName() const override {return tr("Gigabit PHY access");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::gphyLb() {
        static const class ViModuleFaultGphyLb : public ViModuleFault {
            virtual quint8 id() const override {return VI_DEVFAULT_GPHY_LOOPBACK;}
            virtual QString displayName() const override {return tr("Gigabit PHY loopback");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::relayCurrent() {
        static const class ViModuleFaultRelCurrent : public ViModuleFault {
            virtual quint8 id() const override {return VI_DEVFAULT_RELAY_CURRENT;}
            virtual QString displayName() const override {return tr("Relay current out of range");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::overtemp() {
        static const class ViModuleFaultOvertemp : public ViModuleFault {
            virtual quint8 id() const override {return VI_DEVFAULT_OVERTEMP;}
            virtual QString displayName() const override {return tr("Over-temperature");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::diagnAdcLdoRdy()  {
        static const class ViModuleFaultDiagnAdcLdoRdy : public ViModuleFault {
            virtual quint8 id() const override {return VI_DEVFAULT_DIAGN_ADC_LDORDY;}
            virtual QString displayName() const override {return tr("Diagnostic ADC LDO not ready");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::diagnAdcCalRdy() {
        static const class ViModuleFaultDiagnAdcCalRdy : public ViModuleFault {
            virtual quint8 id() const override {return VI_DEVFAULT_DIAGN_ADC_CALRDY;}
            virtual QString displayName() const override {return tr("Diagnostic ADC calibration not ready");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::usupRange() {
        static const class ViModuleFaultUsupRange : public ViModuleFault {
            virtual quint8 id() const override {return static_cast<quint8>(VI_DEVFAULT_USUP_OUT_OF_RANGE);}
            virtual QString displayName() const override {return tr("Supply voltage out of range");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::acqChFault() {
        static const class ViModuleFaultAcqChFault : public ViModuleFault {
            virtual quint8 id() const override {return static_cast<quint8>(VI_DEVFAULT_ACQ_CH_FAULT);}
            virtual QString displayName() const override {return tr("Acquisition channel fault");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::noBoardInfo() {
        static const class ViModuleFaultNoBoardInfo : public ViModuleFault {
            virtual quint8 id() const override {return static_cast<quint8>(VI_DEVFAULT_OW_FLASH_NO_BRD_INFO);}
            virtual QString displayName() const override {return tr("No carrier board info");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::unknowDevtype() {
        static const class ViModuleFaultUnknDevtype : public ViModuleFault {
            virtual quint8 id() const override {return static_cast<quint8>(VI_DEVFAULT_UNKNOWN_DEVTYPE);}
            virtual QString displayName() const override {return tr("Unknown device type");}
        } f;
        return f;
    }

    const ViModuleFault &ViModuleFaults::noMezzInfo() {
        static const class ViModuleFaultNoMezzInfo : public ViModuleFault {
            virtual quint8 id() const override {return static_cast<quint8>(VI_DEVFAULT_SPI_FLASH_NO_MEZINFO);}
            virtual QString displayName() const override {return tr("No mezzanine board info");}
        } f;
        return f;
    }

    QList<const ViModuleFault *> ViModuleFaults::all() {
        static const QList<const ViModuleFault *> list {
            &clkinit(),
            &sdram(),
            &ethPhyRegs(),
            &ethPhyLb(),
            &noMezzFlash(),
            &noBoardFlash(),
            &noSwitch(),
            &switchData(),
            &gphyRegs(),
            &gphyLb(),
            &relayCurrent(),
            &overtemp(),
            &diagnAdcLdoRdy(),
            &diagnAdcCalRdy(),

            &usupRange(),
            &acqChFault(),
            &noBoardInfo(),
            &unknowDevtype(),
            &noMezzInfo(),
        };
        return list;
    }
}
