#ifndef LQMEAS_VIMANEVENTFLAGSTATE_H
#define LQMEAS_VIMANEVENTFLAGSTATE_H

namespace LQMeas {
    class ViManEventFlagState {
    public:
        bool resultState() const {return m_manual_state || m_event_state;}
        bool manualState() const {return m_manual_state;}
        bool eventState() const {return m_event_state;}

        void setManualState(bool st) {
            m_manual_state = st;
        }
        void setEventState(bool st) {
            m_event_state = st;
        }

    private:
        bool m_manual_state {false};
        bool m_event_state  {false};
    };
}

#endif // LQMEAS_VIMANEVENTFLAGSTATE_H
