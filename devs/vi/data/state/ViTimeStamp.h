#ifndef LQMEAS_VITIMESTAMP_H
#define LQMEAS_VITIMESTAMP_H

#include <QDateTime>
#include "ViTimeClockId.h"

namespace LQMeas {
    class ViTimeStamp {
    public:

        bool isValid() const {return m_is_valid;}
        bool isGlobal() const {return m_is_global;}
        const QDateTime &utcTime() const {return m_utcTime;}
        quint64 seconds() const {return m_sec;}
        quint32 nsec() const {return m_nsec;}
        quint16 nsecFract() const {return m_nsec_fract;}
        quint16 flags() const {return m_flags;}
        quint16 utcOffset() const {return m_utc_offset;}


        const ViTimeClockId &clockId() const {return m_clock_id;}

        void setIsValid(bool valid) {m_is_valid  = valid;}
        void setIsGlobal(bool glob) {m_is_global  = glob;}

        void setSeconds(quint64 sec) {m_sec = sec;}
        void setNSec(quint32 nsec) {m_nsec = nsec;}
        void setNSecFract(quint16 fract) {m_nsec_fract = fract;}
        void setUtcTime(const QDateTime &time) {m_utcTime = time;}
        void setClockId(const ViTimeClockId &clkid) {m_clock_id = clkid;}
        void setFlags(quint16 flags) {m_flags = flags;}
        void setUtcOffset(quint16 offs) {m_utc_offset = offs;}

        void addNs(qint32 ns) {
            if (ns > 0) {
                m_nsec += static_cast<quint32>(ns);
                if (m_nsec > 1000000000) {
                    m_nsec -= 1000000000;
                    m_sec++;
                }
            } else {
                quint32 sub_ns = static_cast<quint32>(-ns);
                if (sub_ns > m_nsec) {
                    --m_sec;
                    m_nsec = 1000000000 - (sub_ns - m_nsec);
                } else {
                    m_nsec -= sub_ns;
                }
            }
        }
    private:
        bool m_is_valid {false};
        bool m_is_global {false};


        quint64 m_sec {0};
        quint32 m_nsec {0};
        quint16 m_nsec_fract {0};
        QDateTime m_utcTime;
        ViTimeClockId m_clock_id;

        quint16 m_flags {0};
        quint16 m_utc_offset {0};
    };
}



#endif // LQMEAS_VITIMESTAMP_H
