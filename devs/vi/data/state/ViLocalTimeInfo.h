#ifndef LQMEAS_VILOCALTIMEINFO_H
#define LQMEAS_VILOCALTIMEINFO_H

#include <QtGlobal>

namespace LQMeas {
    class ViLocalTimeInfo {
    public:
        bool isValid() const {return m_valid;}
        quint32 seconds() const {return m_secs;}
        quint32 nsec() const {return m_nsec;}
        double  dfPpm() const {return m_df_ppm;}

    private:
        bool m_valid {false};
        double m_df_ppm{0};
        quint32 m_secs{0};
        quint32 m_nsec{0};

        friend class ViModuleMbConnection;
    };
}


#endif // LQMEAS_VILOCALTIMEINFO_H
