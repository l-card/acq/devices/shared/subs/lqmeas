#ifndef LQMEAS_VIWFCHINFO_H
#define LQMEAS_VIWFCHINFO_H


#include "lqmeas/devs/vi/data/state/acq/ViAcqChannelConvInfo.h"
#include <QSharedPointer>

namespace LQMeas {
    class ViAcqChannelState;
    struct ViWfConfCoefs {
        double k {1.};
        double b {0.};
    };

    class ViWfChInfo     {
    public:
        ViWfChInfo();
        ~ViWfChInfo();

        quint16 wfID() const {return m_wf_id;}
        quint16 chNum() const {return m_ch_num;}
        bool isEnabled() const;
        const ViAcqChannelConvInfo &conv() const {return m_conv;}

        const LQMeas::ViAcqChannelState &chState() const {return *m_chState;}
        QSharedPointer<const LQMeas::ViAcqChannelState> chStatePtr() const {return m_chState;}


        void setWfId(quint16 id) {m_wf_id = id;}
        void setConv(const ViAcqChannelConvInfo &conv) { m_conv = conv; }
        void setFlags(quint32 f) {m_flags = f;}
        void setChNum(quint16 num) {m_ch_num = num;}
        void setChState(const QSharedPointer<LQMeas::ViAcqChannelState> &state) {m_chState = state;}

    private:
        quint16 m_wf_id {0};
        quint16 m_ch_num {0};
        quint32 m_flags {0};
        ViAcqChannelConvInfo m_conv;

        QSharedPointer<LQMeas::ViAcqChannelState> m_chState;
    };
}


#endif // LQMEAS_VIWFCHINFO_H
