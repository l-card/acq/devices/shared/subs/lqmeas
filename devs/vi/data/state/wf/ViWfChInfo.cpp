#include "ViWfChInfo.h"
#include "lqmeas/devs/vi/data/state/acq/ViAcqChannelState.h"
#include "lqmeas/devs/vi/protocol/vi_module_state.h"

namespace LQMeas {
    ViWfChInfo::ViWfChInfo() {


    }

    ViWfChInfo::~ViWfChInfo() {

    }

    bool ViWfChInfo::isEnabled() const {
        return m_flags & VI_WF_CH_FLAG_ENABLED;
    }

}
