#ifndef LQMEAS_VIWFCHDATABLOCK_H
#define LQMEAS_VIWFCHDATABLOCK_H

#include <QtGlobal>
#include <QVector>

namespace LQMeas {
    class ViWfChDataBlock {
    public:
        static constexpr quint32 maxSamplesCnt() {return 62;}

        ViWfChDataBlock() {}

        quint16 wfID() const {return m_wf_id;}
        quint16 chNum() const {return m_ch_num;}
        quint16 dataOffset() const {return m_offset;}
        const QVector<qint32> &samples() const {return m_samples;}

        void setWfId(quint16 wf_id) { m_wf_id = wf_id;}
        void setChNum(quint16 ch_num) { m_ch_num = ch_num;}
        void setOffset(quint32 offs) {m_offset = offs;}
        void setSamples(const QVector<qint32> &smpls) {m_samples = smpls;}
    private:
        quint16 m_wf_id {0};
        quint16 m_ch_num {0};
        quint32 m_offset {0};
        QVector<qint32> m_samples;
    };
}

#endif // LQMEAS_VIWFCHDATABLOCK_H
