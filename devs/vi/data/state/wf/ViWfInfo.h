#ifndef LQMEAS_VIWFINFO_H
#define LQMEAS_VIWFINFO_H

#include <QtGlobal>
#include "../ViTimeStamp.h"

namespace LQMeas {
    class ViWfInfo {
    public:
        enum class Type {
            Unknown = 0,
            TimeWave = 1
        };

        quint16 wfID() const {return m_wf_id;}
        bool srcIsUserRequest() const {return m_src_is_user_req;}
        quint16 srcNum() const {return m_src;}
        const ViTimeStamp &timestamp() const {return m_wf_timestamp;}
        quint32 channelMask() const {return m_chs_mask;}        
        quint32 samplesCount() const {return m_samples_cnt;}
        quint32 delaySamplesCount() const {return m_delay_samples_cnt;}

        double samplesFreq() const {return m_samples_freq;}
        double gmSamplesFreq() const {return m_gm_samples_freq;}

        bool channelIsEnabled(int ch_num) const {
            return (m_chs_mask & (1 << ch_num)) != 0;
        }
        quint32 channelsCnt() const {
            int ret {0};
            for (int ch_num {0}; ch_num < sizeof(m_chs_mask)*8; ++ch_num) {
                if (channelIsEnabled(ch_num)) {
                    ++ret;
                }
            }
            return ret;
        }
        quint32 totalSamplesCount() const {
            return channelsCnt() * samplesCount();
        }

        void setWfId(quint16 id) {m_wf_id = id;}
        void setWfType(Type type) {m_type = type;}
        void setSrcIsUserReq(bool userreq) {m_src_is_user_req = userreq;}
        void setSrcNum(quint16 srcnum) {m_src = srcnum;}
        void setFlags(quint16 flags) {m_flags = flags;}
        void setChMask(quint32 mask) {m_chs_mask = mask;}
        void setSamplesCnt(quint32 cnt) {m_samples_cnt = cnt;}
        void setDelaySamplesCnt(quint32 cnt) {m_delay_samples_cnt = cnt;}
        void setSamplesFreq(double freq) {m_samples_freq = freq;}
        void setGmSamplesFreq(double freq) {m_gm_samples_freq = freq;}
        void setTimeStamp(const ViTimeStamp &tstmp) {m_wf_timestamp = tstmp;}
    private:
        quint16 m_wf_id {0};
        Type m_type {Type::Unknown};
        bool m_src_is_user_req {false};
        quint16 m_src {0};        
        quint16 m_flags {0};
        quint32 m_chs_mask {0};
        quint32 m_samples_cnt {0};
        quint32 m_delay_samples_cnt {0};
        double  m_samples_freq {0};
        double  m_gm_samples_freq {0};
        ViTimeStamp m_wf_timestamp;
    };
}

#endif // LQMEAS_VIWFINFO_H

