#ifndef LQMEAS_VIMODULESTATE_H
#define LQMEAS_VIMODULESTATE_H

#include <QtGlobal>
#include <QVector>
#include "ViTimeStamp.h"
#include "ViLocalTimeInfo.h"
#include "lqmeas/devs/vi/protocol/tribool.h"

namespace LQMeas {
    class ViModuleState {
    public:


        bool liveBit() const {return m_live_bit;}
        bool hasFailure() const {return m_has_fault;}
        bool hasWarn() const {return m_has_warn;}
        quint16 configID() const {return m_cfg_id;}



        quint16 wfReadyCount() const {return m_wf_rdy_cnt;}
        const QList<quint16> &wfRdyIDs() const {return m_wf_rdy_ids;}

        const ViTimeStamp &curTime() const {return m_cur_time;}
        const ViLocalTimeInfo &locTimeInfo() const {return m_loc_time_info;}

        t_tribool eventState(int evt_num) const {return evt_num < m_evtStates.size() ? m_evtStates[evt_num] : tribool_z;}
        t_tribool dinState(int din_num) const {return din_num < m_dinStates.size() ? m_dinStates[din_num] : tribool_z;}
    private:
        ViModuleState();

        bool m_live_bit {false};
        bool m_has_fault {false};
        bool m_has_warn {false};
        quint16 m_cfg_id {0};
        quint16 m_wf_rdy_cnt {0};
        QList<quint16> m_wf_rdy_ids;

        ViTimeStamp m_cur_time;
        ViLocalTimeInfo m_loc_time_info;

        QVector<t_tribool> m_evtStates;
        QVector<t_tribool> m_dinStates;

        friend class ViModuleMbConnection;
    };
}

#endif // LQMEAS_VIMODULESTATE_H
