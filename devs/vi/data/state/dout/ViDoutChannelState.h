#ifndef LQMEAS_VIDOUTCHANNELSTATE_H
#define LQMEAS_VIDOUTCHANNELSTATE_H


#include "lqmeas/devs/vi/protocol/tribool.h"

namespace LQMeas {
    class ViDoutChannelType;

    class ViDoutChannelState {
    public:
        int channelNumber() const {return m_ch_num;}
        const ViDoutChannelType &channelType() const {return *m_ch_type;}


        t_tribool failure() const {return m_failure;}
        t_tribool outState()  const {return m_out_state;}
        t_tribool energized() const {return m_energized;}
        t_tribool alarmEventState() const {return m_alarm_evt_state;}
        t_tribool resetEventState() const {return m_rst_evt_state;}
    private:
        ViDoutChannelState(const ViDoutChannelType &type, int ch_num);

        const ViDoutChannelType *m_ch_type {nullptr};
        int m_ch_num {-1};


        t_tribool m_failure {tribool_z};
        t_tribool m_out_state {tribool_z};
        t_tribool m_energized {tribool_z};
        t_tribool m_alarm_evt_state {tribool_z};
        t_tribool m_rst_evt_state {tribool_z};

        friend class ViModuleMbConnection;
    };
}

#endif // LQMEAS_VIDOUTCHANNELSTATE_H
