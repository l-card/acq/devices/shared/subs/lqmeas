#include "ViDoutTypeState.h"
#include "ViDoutChannelState.h"

namespace LQMeas {
    ViDoutTypeState::ViDoutTypeState(const ViDoutChannelType &type) : m_ch_type{&type}{

    }

    ViDoutTypeState::~ViDoutTypeState() {
        qDeleteAll(m_chs);
    }
}
