#ifndef LQMEAS_VIDOUTTYPESTATE_H
#define LQMEAS_VIDOUTTYPESTATE_H

#include <QList>

namespace LQMeas {
    class ViDoutChannelType;
    class ViDoutChannelState;

    class ViDoutTypeState {
    public:
        ~ViDoutTypeState();

        const ViDoutChannelType &channelsType() const {return *m_ch_type;}
        const ViDoutChannelState &channelState(int ch_num) const {return *m_chs.at(ch_num);}
        int channelsCnt() const {return m_chs.size();}

    private:
        explicit ViDoutTypeState(const ViDoutChannelType &type);


        const ViDoutChannelType *m_ch_type {nullptr};
        QList<ViDoutChannelState *> m_chs;

        friend class ViModuleMbConnection;
    };

}

#endif // LQMEAS_VIDOUTTYPESTATE_H
