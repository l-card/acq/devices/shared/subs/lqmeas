﻿#include "DeviceInterface.h"

namespace LQMeas {
    const DeviceInterface &DeviceInterfaces::unknown() {
        static const class DeviceInterfaceUnknown : public DeviceInterface {
            QString id() const override {return QStringLiteral("unk");}
            QString displayName() const override {return tr("Unknown");}
        } iface;
        return iface;
    }

    const DeviceInterface &DeviceInterfaces::usb() {
        static const class DeviceInterfaceUSB : public DeviceInterface {
            QString id() const override {return QStringLiteral("usb");}
            QString displayName() const override {return tr("USB");}
        } iface;
        return iface;
    }

    const DeviceInterface &DeviceInterfaces::pci() {
        static const class DeviceInterfacePCI : public DeviceInterface {
            QString id() const override {return QStringLiteral("pci");}
            QString displayName() const override {return tr("PCI");}
        } iface;
        return iface;
    }

    const DeviceInterface &DeviceInterfaces::pcie() {
        static const class DeviceInterfacePCIe : public DeviceInterface {
            QString id() const override {return QStringLiteral("pcie");}
            QString displayName() const override {return tr("PCI-Express");}
        } iface;
        return iface;
    }

    const DeviceInterface &DeviceInterfaces::ethernet() {
        static const class DeviceInterfaceEth : public DeviceInterface {
            QString id() const override {return QStringLiteral("eth");}
            QString displayName() const override {return tr("Ethernet");}
        } iface;
        return iface;
    }

    const DeviceInterface &DeviceInterfaces::ltrModule() {
        static const class DeviceInterfaceLTRModule : public DeviceInterface {
            QString id() const override {return QStringLiteral("ltrmod");}
            QString displayName() const override {return tr("LTR module");}
        } iface;
        return iface;
    }

    const DeviceInterface &DeviceInterfaces::local() {
        static const class DeviceInterfaceLTRModule : public DeviceInterface {
            QString id() const override {return QStringLiteral("local");}
            QString displayName() const override {return tr("Local");}
        } iface;
        return iface;
    }

    const QList<const DeviceInterface *> &DeviceInterfaces::fullList() {
        static const QList<const DeviceInterface *> list {
            &usb(),
            &pci(),
            &pcie(),
            &ethernet(),
            &ltrModule(),
            &local()
        };
        return list;
    }

    const DeviceInterface &DeviceInterfaces::getByID(const QString &id, const DeviceInterface &defaultIface) {
        const QList<const DeviceInterface *> &ifaces = fullList();
        auto it = std::find_if(ifaces.constBegin(), ifaces.constEnd(),
                               [&id](auto iface) {return iface->id() == id;});
        return it == ifaces.constEnd() ? defaultIface : *(*it);
    }

    bool DeviceInterface::operator ==(const DeviceInterface &other) const {
        return id() == other.id();
    }

    bool DeviceInterface::operator !=(const DeviceInterface &other) const {
        return id() != other.id();
    }

}
