#include "Device.h"
#include "DeviceConfig.h"
#include "lqmeas/log/Log.h"
#include "lqmeas/ifaces/out/DevOutSync.h"
#include "lqmeas/ifaces/out/SignalConverter/DevOutDacConverter.h"
#include "lqmeas/misc/LQTypeReg.h"
#include "lqmeas/devs/DeviceRefMethod.h"
#include "DeviceInterface.h"
#include "DeviceTypeInfo.h"
#include "DeviceInfo.h"
#include "DeviceRef.h"


namespace LQMeas {
    Device::~Device() {
    }

    const QSharedPointer<const DeviceInfo> &Device::devInfo() const {
        QMutexLocker lock{&m_info_lock};
        return m_info;
    }

    const DeviceTypeInfo &Device::devTypeInfo() const {
        return devInfo()->type();
    }

    QString Device::name() const {
        return devTypeInfo().deviceTypeName();
    }

    QString Device::modificationName() const {
        return devTypeInfo().deviceModificationName();
    }

    QString Device::serial() const {
        return m_info ? m_info->serial() : QString();
    }

    QString Device::devStr() const {
        QString ret = modificationName();
        if (!serial().isEmpty())
            ret.append(", ").append(serial());
        return ret;
    }

    QString Device::devStrFull() const {
        QString ret = devStr();
        if (!location().isEmpty())
            ret += QString(" (%1)").arg(location());
        return ret;
    }


    void Device::open(LQError &err) {
        open(OpenFlag::None, err);
    }

    void Device::open(OpenFlags flags, LQError &err) {
        if (!isOpened()){
            protOpen(flags, err);
            if (err.isSuccess()) {
                LQMeasLog->detail(tr("Device opened successfully"), this);
            } else {
                LQMeasLog->error(tr("Device open error"), err, this);
            }

            if (!err.isSuccess() && !(flags & OpenFlag::Force)) {
                protClose(err);
            }
        }
    }

    void Device::close(LQError &err) {
        if (isOpened()) {
            if (devOutSync())
                devOutSync()->outSyncGenStop(err);
            protClose(err);
            if (err.isSuccess()) {
                LQMeasLog->detail(tr("Device closed successfully"), this);
            } else {
                LQMeasLog->error(tr("Device close error"), err, this);
            }
        }
    }

    void Device::close() {
        LQError err;
        close(err);
    }

    void Device::setParentConfig(const QSharedPointer<const DeviceConfig> &cfg) {
        QMutexLocker lock{&m_cfg_lock};
        m_parentCfg = cfg;
        if (m_cfg) {
            m_cfg->setParentConfig(m_parentCfg.data());
        }
    }

    void Device::configure(const DeviceConfig &cfg, LQError &err) {
        QSharedPointer<DeviceConfig> setCfg {cfg.clone()};
        {
            QMutexLocker lock{&m_cfg_lock};

            if (m_parentCfg)
                setCfg->setParentConfig(m_parentCfg.data());
            setCfg->updateDevice(this);
            setCfg->update();

            protConfigure(*setCfg, err);

            if (err.isSuccess()) {
                m_cfg = setCfg;
                if (devOutSync()) {
                    devOutSync()->outSyncUpdateConfig(setCfg->outSyncConfig());
                }
            }
        }

        if (err.isSuccess()) {
            LQMeasLog->detail(tr("Device configuration done successfully"), this);
            Q_EMIT configChanged(m_cfg);
        } else {
            LQMeasLog->error(tr("Device configuration error"), err, this);
        }
    }

    void Device::sendOpProgess(OpStatus status, QString descr, int done, int size, const LQError &err) {
        Q_EMIT opProgress(status, descr, done, size, err);
    }

    Device::Device(DeviceConfig *defaultCfg, const DeviceInfo &info,
                   const DeviceInterface &iface, QObject *parent) :
        QObject{parent}, m_cfg{defaultCfg}, m_info{info.clone()}, m_iface{&iface} {

        if (m_cfg)
            m_cfg->update();
        LQTYPE_REGISTER(LQError);
        LQTYPE_REGISTER(QSharedPointer<const DeviceConfig>);
    }

    void Device::setDeviceInfo(const DeviceInfo &info)  {
        QMutexLocker lock{&m_info_lock};
        m_info = QSharedPointer<const DeviceInfo>{info.clone()};
        if (m_cfg)
            m_cfg->updateDevTypeInfo(&m_info->type());
        if (devOutSync())
            devOutSync()->outSyncUpdateInfo(info);
        if (devOutDacConverter()) {
            devOutDacConverter()->updateInfo(info);
        }
    }

    DeviceRef *Device::createRef() const {
        return new DeviceRef{*devInfo(), iface(), DeviceRefMethods::serialNumber()};
    }

    bool Device::devInfoValid() const {
        QMutexLocker lock{&m_info_lock};
        return m_info && !m_info->serial().isEmpty();
    }
}
