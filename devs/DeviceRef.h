#ifndef DEVICEREF_H
#define DEVICEREF_H

#include <QString>
#include <memory>
class QJsonObject;

namespace LQMeas {
    class DeviceRefMethod;
    class DeviceInterface;
    class DeviceInfo;
    class Device;

    class DeviceRef {
    public:
        enum class DevCheckResult {
            BadDevice,
            GoodDevice,
            OpenRequired
        };

        enum class Flag {
            None        = 0x00000000,
        };
        Q_DECLARE_FLAGS(Flags, Flag)


        DeviceRef(const DeviceInfo &info, const DeviceInterface &iface,
                 const DeviceRefMethod &refMethod,
                  Flags flags = Flag::None,
                  const DeviceRef *parentRef = nullptr);
        virtual ~DeviceRef();

        virtual DeviceRef *clone() const {return new DeviceRef{*this};}

        virtual DevCheckResult checkDevice(const Device &device) const;
        /* проверка, указывают ли обе ссылки на одно и то же устройство */
        virtual bool sameDevice(const DeviceRef &ref) const;
        /* проверка, указывают ли обе ссылки на устройство с одинаковым
         * расположением */
        virtual bool sameDeviceLocation(const Device &device) const;
        virtual bool sameDeviceLocation(const DeviceRef &ref) const;
        /* используется ли для распознавания устройства серийный номер */
        virtual bool serialUsed() const {return true;}
        /* признак, что устройство, соответствующее данной ссылке, может быть
         * обнаружено автоматически и явно не обязательно передавать ее в
         * DevicesResolver::getDevList() */
        virtual bool isAutoResolved() const {return true;}
        /* ID, определяющий тип класса ссылки на устройство.
         * Унаследованные классы должны переопределять этот тип */
        virtual QString refTypeID() const {return defaultRefTypeID();}

        const DeviceInfo &devInfo() const {return *m_info;}
        const DeviceInterface &iface() const {return *m_iface;}
        const DeviceRefMethod &refMethod() const {return *m_refMethod;}
        const DeviceRef *parentRef() const {return m_parent;}
        virtual int parentPos() const {return 0;}

        void setSerial(const QString &serial);
        void setInfo(const DeviceInfo &info);
        void setIface(const DeviceInterface &iface);

        void setParentRef(const DeviceRef &ref);
        void clearParentRef();

        virtual void save(QJsonObject &refObj) const;
        virtual void load(const QJsonObject &refObj);

        /* Ключ со строкой, уникально идентифицирующей устройство.
           По умолчанию основан на модификции, интерфейсе и серийном номере.
           Может быть преопределен с помощью метода protKey()
           (Сам метод key дополнительно исключает спец. символы из
            результата key() и не может переопределяться) */
        QString key() const;
        /* Имя, отображающее информацию о ссылке */
        virtual QString displayName() const;

        static void saveBaseInfo(QJsonObject &refObj, const QString &dev_type,
                                 const QString &dev_mod, const QString &dev_serial);
        static QString loadDeviceModification(const QJsonObject &refObj);
        static QString loadRefTypeID(const QJsonObject &refObj);
        static QJsonObject loadParentRef(const QJsonObject &refObj);

        static QString defaultRefTypeID() {return QStringLiteral("default");}
    protected:
        DeviceRef(const DeviceRef &ref);

        virtual QString protKey() const;
        virtual bool checkDevIface() const {return true;}

    private:        
        DeviceInfo *m_info;
        const DeviceInterface *m_iface;
        const DeviceRefMethod *m_refMethod;
        Flags m_flags;
        const DeviceRef *m_parent;
    };

    Q_DECLARE_OPERATORS_FOR_FLAGS(DeviceRef::Flags)
}

#endif // DEVICEREF_H
