#ifndef LQMEAS_DEVICECONFIGIFACE_H
#define LQMEAS_DEVICECONFIGIFACE_H

#include "DeviceConfigItem.h"


namespace LQMeas {
    class DeviceConfig;

    class DeviceConfigIface : public DeviceConfigItem {
        Q_OBJECT
    public:

        virtual ~DeviceConfigIface();

        void init(DeviceConfig *cfg);

        DeviceConfig *devConfig() const {return  m_devCfg;}
    protected:
        explicit DeviceConfigIface(QObject *parent = nullptr);

        virtual QString cfgIfaceName() const = 0;

        virtual void protInit(DeviceConfig *cfg) {Q_UNUSED(cfg) }
        virtual void protUpdateParentConfig(const DeviceConfig *parentCfg) {Q_UNUSED(parentCfg)}
    private:
        friend class DeviceConfig;
        DeviceConfig *m_devCfg {nullptr};
    };
}

#endif // LQMEAS_DEVICECONFIGIFACE_H
