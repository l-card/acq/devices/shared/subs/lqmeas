#ifndef LQMEAS_DEVFRAMERECEIVER_H
#define LQMEAS_DEVFRAMERECEIVER_H


#include <QVector>
#include "Device.h"
#include "lqmeas/log/Log.h"

namespace LQMeas {
    template < class TypeDevice, class TypeWord> class DeviceFrameReceiver {
    public:
        explicit DeviceFrameReceiver(TypeDevice *dev);

        LQError getFrames(unsigned size, unsigned tout, TypeWord **frame, unsigned *recvd_size);
        void setFrameSize(int size);

        int frameSize() const {return m_frame_size;}
    private:
        TypeDevice *m_dev;
        QVector<TypeWord>  m_last_wrds;
        int m_last_wrds_cnt {0};
        int m_frame_size {1};
        int m_last_wrds_offs {0};
    };

    template < class TypeDevice, class TypeWord>
        DeviceFrameReceiver<TypeDevice, TypeWord>::DeviceFrameReceiver(TypeDevice *dev) : m_dev{dev} {
    }

    template < typename TypeDevice, typename TypeWord>
        LQError DeviceFrameReceiver<TypeDevice, TypeWord>::getFrames(
                unsigned size, unsigned tout, TypeWord **frame, unsigned *recvd_size) {

        LQError err;
        int recvd;
        if (size > static_cast<unsigned>(m_last_wrds.size()))
            m_last_wrds.resize(size);

        if ((m_last_wrds_cnt != 0) && (m_last_wrds_offs != 0))  {
            memmove(m_last_wrds.data(), &m_last_wrds.data()[m_last_wrds_offs],
                    m_last_wrds_cnt*sizeof(m_last_wrds[0]));
        }

        m_dev->rawWordsReceive(&m_last_wrds.data()[m_last_wrds_cnt],
                               size - m_last_wrds_cnt, tout, recvd, err);
        if (err.isSuccess()) {
            unsigned tail_size;
            recvd += m_last_wrds_cnt;

            tail_size = recvd % m_frame_size;
            recvd -= tail_size;

            if (recvd_size != nullptr) {
                *recvd_size = recvd;
            }


            if (frame != nullptr)
                *frame = m_last_wrds.data();
            m_last_wrds_cnt = tail_size;
            m_last_wrds_offs = recvd;
        } else {
            LQMeasLog->error(Device::tr("Data receive error"), err, m_dev);
        }

        return err;
    }

    template < typename TypeDevice, typename TypeWord>
        void DeviceFrameReceiver<TypeDevice, TypeWord>::setFrameSize(int size) {

        m_frame_size = size;
        m_last_wrds_cnt = 0;
        m_last_wrds_offs = 0;
    }
}
#endif // LQMEAS_DEVFRAMERECEIVER_H
