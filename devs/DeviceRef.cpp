#include "DeviceRef.h"
#include "DeviceInterface.h"
#include "lqmeas/devs/DeviceInfo.h"
#include "lqmeas/devs/DeviceTypeInfo.h"
#include "lqmeas/devs/DeviceRefMethod.h"
#include "lqmeas/devs/Device.h"
#include <QJsonObject>

namespace LQMeas {
    static const QLatin1String cfgkey_reftype{"RefType"};
    static const QLatin1String cfgkey_refmethod{"RefMethod"};
    static const QLatin1String cfgkey_deviface{"DevIface"};
    static const QLatin1String cfgkey_flags{"Flags"};
    static const QLatin1String cfgkey_devinfo{"DevInfo"};
    static const QLatin1String cfgkey_parent_dev{"Parent"};


    DeviceRef::DeviceRef(const DeviceInfo &info, const DeviceInterface &iface,
                         const DeviceRefMethod &refMethod,
                         Flags flags, const DeviceRef *parent) :
        m_info{info.clone()},
        m_iface{&iface},
        m_refMethod{&refMethod},
        m_flags{flags},
        m_parent{parent ? parent->clone() : nullptr} {

    }

    DeviceRef::DeviceRef(const DeviceRef &ref) :
          m_info{ref.m_info->clone()},
          m_iface{ref.m_iface},
          m_refMethod{ref.m_refMethod},
          m_flags{ref.m_flags},
          m_parent{ref.m_parent ? ref.m_parent->clone() : nullptr} {

    }



    DeviceRef::~DeviceRef() {
        delete m_info;
        delete m_parent;
    }

    DeviceRef::DevCheckResult DeviceRef::checkDevice(const Device &device) const  {
        DevCheckResult ret = DevCheckResult::GoodDevice;
        if (!m_info->type().isCompatible(device.devTypeInfo())) {
            ret = DevCheckResult::BadDevice;
        } else {
            if (!m_info->serial().isEmpty()) {
                if (device.serial().isEmpty() && !device.isOpened()) {
                    ret = DevCheckResult::OpenRequired;
                } else if (device.serial() != m_info->serial()) {
                    ret = DevCheckResult::BadDevice;
                }
            }
        }

        if ((ret == DevCheckResult::GoodDevice)
            && checkDevIface()
            && (iface() != DeviceInterfaces::unknown())) {
            if ((device.iface() == DeviceInterfaces::unknown()) && !device.isOpened()) {
                ret = DevCheckResult::OpenRequired;
            } else if (device.iface() != iface()) {
                ret = DevCheckResult::BadDevice;
            }
        }
        return ret;
    }

    bool DeviceRef::sameDevice(const DeviceRef &ref) const {
        bool ok = refTypeID() == ref.refTypeID();
        if (ok && checkDevIface()) {
            /* если интерфейсы обоих ссылок известны, то должны совпадать */
            ok = (ref.iface() == iface()) ||
                    (iface() == DeviceInterfaces::unknown()) ||
                    (ref.iface() == DeviceInterfaces::unknown());
        }
        if (ok)
            ok = m_info->sameDevice(ref.devInfo());
        if (ok && m_parent)
            ok = !ref.m_parent || m_parent->sameDevice(*ref.m_parent);

        return ok;
    }

    bool DeviceRef::sameDeviceLocation(const Device &device) const  {
        std::unique_ptr<DeviceRef> ref {device.createRef()};
        return sameDeviceLocation(*ref);
    }

    bool DeviceRef::sameDeviceLocation(const DeviceRef &ref) const {
        return sameDevice(ref);
    }

    void DeviceRef::setSerial(const QString &serial) {
        m_info->setSerial(serial);
    }

    void DeviceRef::setInfo(const DeviceInfo &info) {
        delete m_info;
        m_info = info.clone();
    }

    void DeviceRef::setIface(const DeviceInterface &iface) {
        m_iface = &iface;
    }

    void DeviceRef::setParentRef(const DeviceRef &ref) {
        delete m_parent;
        m_parent = ref.clone();
    }

    void DeviceRef::clearParentRef() {
        delete  m_parent;
        m_parent = nullptr;
    }

    void DeviceRef::save(QJsonObject &refObj) const {
        refObj[cfgkey_reftype] = refTypeID();
        refObj[cfgkey_deviface] = m_iface->id();
        refObj[cfgkey_refmethod] = m_refMethod->id();
        refObj[cfgkey_flags] = static_cast<int>(m_flags);

        QJsonObject infoObj = refObj[cfgkey_devinfo].toObject();
        m_info->save(infoObj);
        refObj[cfgkey_devinfo] = infoObj;
        if (m_parent) {
            QJsonObject parObj;
            m_parent->save(parObj);
            refObj[cfgkey_parent_dev] = parObj;
        }
    }

    void DeviceRef::load(const QJsonObject &refObj) {
        m_iface = &DeviceInterfaces::getByID(refObj[cfgkey_deviface].toString(), *m_iface);
        m_refMethod = &DeviceRefMethods::getByID(refObj[cfgkey_refmethod].toString(), *m_refMethod);
        m_flags = static_cast<Flags>(refObj[cfgkey_flags].toInt());
        m_info->load(refObj[cfgkey_devinfo].toObject());
    }

    QString DeviceRef::key() const {
        return protKey().replace('/', '-');
    }

    QString DeviceRef::displayName() const {
        QString ret {devInfo().type().deviceModificationName()};
        const QString &serial {devInfo().serial()};
        if (!serial.isEmpty())
            ret += QString{" (%1)"}.arg(serial);
        return ret;
    }

    QString DeviceRef::protKey() const {
        QString ret = QString("%1_%2").arg(devInfo().type().deviceModificationName(), iface().id());
        if (!devInfo().serial().isEmpty()) {
            ret += QString("_%1").arg(devInfo().serial());
        }
        return ret;
    }

    void DeviceRef::saveBaseInfo(QJsonObject &refObj, const QString &dev_type, const QString &dev_mod, const QString &dev_serial) {
        QJsonObject infoObj {refObj[cfgkey_devinfo].toObject()};
        DeviceInfo::saveBaseInfo(infoObj, dev_type, dev_mod, dev_serial);
        refObj[cfgkey_devinfo] = infoObj;
    }

    QString DeviceRef::loadDeviceModification(const QJsonObject &refObj) {
        return DeviceInfo::loadDeviceModification(refObj[cfgkey_devinfo].toObject());
    }

    QString DeviceRef::loadRefTypeID(const QJsonObject &refObj) {
        return refObj[cfgkey_reftype].toString(defaultRefTypeID());
    }

    QJsonObject DeviceRef::loadParentRef(const QJsonObject &refObj) {
        return refObj[cfgkey_parent_dev].toObject();
    }
}
