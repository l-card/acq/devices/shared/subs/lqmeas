#ifndef DEVICEREFMETHOD_H
#define DEVICEREFMETHOD_H

#include "lqmeas/EnumNamedValue.h"
#include <QObject>

namespace LQMeas {
    class  DeviceRefMethod : public EnumNamedValue<QString> {
    public:
        bool operator ==(const DeviceRefMethod &other) const;
        bool operator !=(const DeviceRefMethod &other) const;
    };

    class DeviceRefMethods : public QObject {
        Q_OBJECT
    public:
        static const DeviceRefMethod &unknown();
        static const DeviceRefMethod &serialNumber();
        static const DeviceRefMethod &ipAddr();
        static const DeviceRefMethod &netName();
        static const DeviceRefMethod &crateSlot();

        static const QList<const DeviceRefMethod *> &fullList();
        static const DeviceRefMethod &getByID(const QString &id, const DeviceRefMethod &defaultMethod = unknown());
    };
}

#endif // DEVICEREFMETHOD_H
