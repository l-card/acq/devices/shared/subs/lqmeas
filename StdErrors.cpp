#include "StdErrors.h"

namespace LQMeas {
    LQError StdErrors::error(int code, const QString &msg) {
        static const QString err_type {QStringLiteral("lqmeas")};
        return LQError{code, msg, err_type};
    }

    LQError StdErrors::DeviceUnsupConfig()  {
        return error(-1, tr("This configuration is not supported by device"));
    }

    LQError StdErrors::SendBusy() {
        return error(-2, tr("Data sending in progress. No free space for new data"));
    }

    LQError StdErrors::StreamStopWait() {
        return error(-3, tr("Stream completion timeout"));
    }

    LQError StdErrors::RecvInsufficientData() {
        return error(-4, tr("Received less data than requested"));
    }

    LQError StdErrors::InvalidAdcConfig() {
        return error(-5, tr("Invalid data acquisition parameters"));
    }

    LQError StdErrors::FeatureNotImplemented() {
        return error(-6, tr("Feature is not implemented!"));
    }

    LQError StdErrors::OutSignalPointCount() {
        return error(-7, tr("Output signal requires too many points"));
    }

    LQError StdErrors::OutGenRunning() {
        return error(-7, tr("Output generation already running"));
    }

    LQError StdErrors::InvalidConfigType() {
        return error(-8, tr("Invalid configuration type"));
    }

    LQError StdErrors::DataProcess() {
        return error(-9, tr("Data processing error"));
    }

    LQError StdErrors::UnsupOutSyncRamMode() {
        return error(-10, tr("Unsupported synchronous output generation mode"));
    }

    LQError StdErrors::DeviceDisconnected() {
        return error(-11, tr("Specified device was diconnected"));
    }

    LQError StdErrors::NoChannelEnabled() {
        return error(-12, tr("No one channel is enabled"));
    }

    LQError StdErrors::UnsupportedSignalType() {
        return error(-13, tr("Specified signal type is not supported in current mode"));
    }

    LQError StdErrors::UnsupportedTEDSNodeFamily(quint8 family) {
        return error(-14, tr("TEDS Node is found but TEDS chip is not supported by software (family 0x%1)").arg(family, 2, 16, QChar('0')));
    }

    LQError StdErrors::DevChNotInTEDSAccessMode() {
        return error(-15, tr("Specified device channel is not in TEDS data access mode"));
    }

    LQError StdErrors::TareInvalidOp() {
        return error(-16, tr("Specified invalid tare operation"));
    }

    LQError StdErrors::TareInvalidPoint() {
        return error(-17, tr("Specified invalid tare point"));
    }

    LQError StdErrors::RecvNoData() {
        return error(-18, tr("No module data was received"));
    }

    LQError StdErrors::UnknownUnit(const QString &id) {
        return error(-19, tr("Unknown unit type (%1)").arg(id));
    }

    LQError StdErrors::OutGenState() {
        return error(-20, tr("Invalid operation for current output generation state"));
    }

    LQError StdErrors::OutGenStartDoneTimeout() {
        return error(-21, tr("Generation start waiting timeout"));
    }

    LQError StdErrors::OutGenSigUpdateDoneTimeout() {
        return error(-22, tr("Generated signal update waiting timeout"));
    }

    LQError StdErrors::UnsupportedFeature() {
        return error(-23, tr("Requested feature is not supported"));
    }

    LQError StdErrors::DeviceIsNotOpen() {
        return error(-24, tr("No valid device connection"));
    }
}



